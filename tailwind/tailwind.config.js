/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./templates/*.{html,js,tpl}","./index.php","./modules/*.php","./modules/manage/*.php","./modules/events/*.php"],
  darkMode: 'class',
  theme: {
    extend: {
      padding:{
        'alerts': '16px',
        'modal': '16px',
        'input-y': '12px',
        'input-x': '16px',
        'badge-y': '4px',
        'badge-x': '6px',
      },
      margin:{
        'modal': '16px',
        'modal-y': '40px',
        'modal-x': '48px',
      },
      borderWidth:{
        'alerts': '1px',
      },
      borderRadius: {
        'buttons': '20px',
        'input': '8px',
        'action-button': '8px',
        'content-box': '8px',
        'badge': '2px',
      },
      fontSize: {
        'modal-header': '24px',
        'input-fontsize': '14px',
        'form-descr-fontsize': '10px',
        'badge-fontsize': '10px',
        'footer-links': '14px',
      },
      boxShadow: {
        'modal': '0 2px 24px 0 rgba(41, 82, 255, 0.25)',
        'container-small': '0 8px 2px 0 rgba(41, 82, 255, 0.15)',
        'container-big': '0 2px 24px 0 rgba(41, 82, 255, 0.25)',
        'header': '0 1px rgba(44, 44, 44, 0.2)',
        'header-dark': '0 1px rgba(255, 255, 255, 0.2)',
        'footer': '0 -1px rgba(44, 44, 44, 0.2)',
        'footer-dark': '0 -1px rgba(255, 255, 255, 0.2)',
      },
      colors: {
        'main-color':{
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'page': {//main body background
          DEFAULT: '#f3f8ff',
          dark: '#151515',
        },
        'container': {//header, content container
          DEFAULT: '#ffffff',
          dark: '#101216',
        },
        'content-back': {//from figma modal background
          DEFAULT:'#ffffff',
          dark: '#242424',
        },
        'modal-page-back': {
          DEFAULT:'rgba(208,222,245,0.6)',
          dark: 'rgba(16,18,22,0.6)',
        },
        'modal-back': {
          DEFAULT: '#ffffff',
          dark: '#242424',
        },
        'header-menu': {//text color
          DEFAULT: '#2c2c2c',
          dark: '#ffffff',
        },
        'header-menu-back': {//active button background
          DEFAULT: '#D0DEF5',
          dark: '#232D51',
        },
        'ncounter': {//notify counter text
          DEFAULT: '#ffffff',
          dark: '#2B2B2B',
        },
        'ncounter-back': {//notify counter back
          DEFAULT: '#C93CCC',
          dark: '#C93CCC',
        },
        'nbar':{//navigation bar in toggle menu (mobile)
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'nbar-back':{//on hover
          DEFAULT: '#F3F8FF',
          dark: '#333333',
        },
        /* buttons */
        'action-button-text':{
          DEFAULT: '#ffffff',
          dark: '#2B2B2B',
        },
        'action-button-back':{
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'action-button-hover-back':{
          DEFAULT: '#496EF1',
          dark: '#7590F3',
        },
        'action-button-active-back':{
          DEFAULT: '#11319F',
          dark: '#7590F3',
        },
        'action-button-disabled-back':{
          DEFAULT: '#979797',
          dark: '#929292',
        },

        'neutral-button-text':{
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'neutral-button-disabled-text':{
          DEFAULT: '#979797',
          dark: '#929292',
        },
        'neutral-button-back':{
          DEFAULT: '#BBD1F6',
          dark: '#BBD1F6',
        },
        'neutral-button-hover-back':{
          DEFAULT: '#D0DEF5',
          dark: '#D0DEF5',
        },
        'neutral-button-active-back':{
          DEFAULT: '#BBD1F6',
          dark: '#BBD1F6',
        },
        'neutral-button-disabled-back':{
          DEFAULT: '#D9D9D9',
          dark: '#333333',
        },

        'inline-button':{
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'inline-button-hover':{
          DEFAULT: '#496EF1',
          dark: '#7590F3',
        },
        'inline-button-active':{
          DEFAULT: '#11319F',
          dark: '#7590F3',
        },
        'inline-button-disabled':{
          DEFAULT: '#979797',
          dark: '#929292',
        },

        /* links colors*/
        'link-color':{
          DEFAULT: '#2C2C2C',
          dark: '#EEEEEE',
        },
        'link-hover-color':{
          DEFAULT: '#496EF1',
          dark: '#7590F3',
        },
        'link-underline-color':{
          DEFAULT: '#496EF1',
          dark: '#7590F3',
        },

        /* countdown circle*/
        'circle-border':{
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'circle-back':{
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'circle-text':{
          DEFAULT: '#EEEEEE',
          dark: '#2C2C2C',
        },

        'speaker-company':{
          DEFAULT: '#496EF1',
          dark: '#7590F3',
        },
        'speaker-addon-link':{
          DEFAULT: '#496EF1',
          dark: '#7590F3',
        },
        'speaker-addon-hover-link':{
          DEFAULT: '#EEEEEE',
          dark: '#2C2C2C',
        },
        'speaker-addon-back':{
          DEFAULT: 'white',
          dark: 'transparent',
        },
        'speaker-addon-hover-back':{
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },

        /* alerts */
        'attention-back':{
          DEFAULT: '#F3F8FF',
          dark: '#101216',
        },
        'attention-text':{
          DEFAULT: '#2C2C2C',
          dark: '#EEEEEE',
        },
        'attention-stroke':{
          DEFAULT: '#829CF6',
          dark: '#232D51',
        },
        'attention-item':{
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'attention-item-hover-text':{
          DEFAULT: '#ffffff',
          dark: '#2B2B2B',
        },

        'success-back':{
          DEFAULT: '#F5FFF8',
          dark: '#28351B',
        },
        'success-text':{
          DEFAULT: '#2C2C2C',
          dark: '#EEEEEE',
        },
        'success-stroke':{
          DEFAULT: '#C1FFBC',
          dark: '#0D6215',
        },
        'success-item':{
          DEFAULT: '#20BF86',
          dark: '#50D16C',
        },
        'success-item-hover-text':{
          DEFAULT: '#ffffff',
          dark: '#2B2B2B',
        },

        'negative-back':{
          DEFAULT: '#FFF6F5',
          dark: '#351B1B',
        },
        'negative-text':{
          DEFAULT: '#2C2C2C',
          dark: '#EEEEEE',
        },
        'negative-stroke':{
          DEFAULT: '#FFBCBC',
          dark: '#620D0D',
        },
        'negative-item':{
          DEFAULT: '#CC453C',
          dark: '#D15951',
        },
        'negative-item-hover-text':{
          DEFAULT: '#ffffff',
          dark: '#2B2B2B',
        },

        'tab': {
          DEFAULT: '#979797',
          dark: '#979797',
        },
        'tab-active': {
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },

        'input-back': {
          DEFAULT: '#ffffff',
          dark: '#151515',
        },
        'input-stroke': {
          DEFAULT: '#D9D9D9',
          dark: '#333333',
        },
        'input-back-hover': {
          DEFAULT: '#ffffff',
          dark: '#2B2B2B',
        },
        'input-stroke-hover': {
          DEFAULT: '#979797',
          dark: '#929292',
        },
        'input-back-focus': {
          DEFAULT: '#ffffff',
          dark: '#2B2B2B',
        },
        'input-stroke-focus': {
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'input-back-disabled': {
          DEFAULT: '#979797',
          dark: '#929292',
        },
        'input-stroke-disabled': {
          DEFAULT: '#D9D9D9',
          dark: '#333333',
        },
        'input-back-error': {
          DEFAULT: '#FFF6F5',
          dark: '#351B1B',
        },
        'input-stroke-error': {
          DEFAULT: '#CC453C',
          dark: '#D15951',
        },

        'input-text': {
          DEFAULT: '#2C2C2C',
          dark: '#EEEEEE',
        },
        'input-placeholder': {
          DEFAULT: '#979797',
          dark: '#929292',
        },
        'input-text-focus': {
          DEFAULT: '#2C2C2C',
          dark: '#EEEEEE',
        },
        'input-placeholder-focus': {
          DEFAULT: '#979797',
          dark: '#929292',
        },
        'input-text-disabled': {
          DEFAULT: '#D9D9D9',
          dark: '#333333',
        },
        'input-placeholder-disabled': {
          DEFAULT: '#D9D9D9',
          dark: '#333333',
        },
        'input-text-error': {
          DEFAULT: '#2C2C2C',
          dark: '#EEEEEE',
        },
        'input-placeholder-error': {
          DEFAULT: '#2C2C2C',
          dark: '#EEEEEE',
        },

        'form-descr': {
          DEFAULT: '#979797',
          dark: '#929292',
        },
        'form-descr-tooltip': {
          DEFAULT: '#2952E1',
          dark: '#5672D8',
        },
        'form-descr-error': {
          DEFAULT: '#CC453C',
          dark: '#D15951',
        },

        'badge-back': {
          DEFAULT: '#496EF1',
          dark: '#7590F3',
        },
        'badge-text': {
          DEFAULT: '#F5FFF8',
          dark: '#101216',
        },
        'badge-reverse-stroke': {
          DEFAULT: '#2952E1',
          dark: '#7590F3',
        },
        'badge-reverse-text': {
          DEFAULT: '#2952E1',
          dark: '#7590F3',
        },

        'footer-back': {
          DEFAULT: '#FFFFFF',
          dark: '#151515',
        },
        'footer-text': {
          DEFAULT: '#2B2B2B',
          dark: '#EEEEEE',
        },
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
  safelist: [
    'rounded-none',
    'rounded',
    'rounded-xl',
    'rounded-full',
    'opacity-50',
    'opacity-75',
    'opacity-100',
    'hidden',
  ],
}
