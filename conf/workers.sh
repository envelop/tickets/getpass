#!/bin/bash

#add gitlab-runner user for CI/CD and remove conflict with www-data group
useradd -u 997 gitlab-runner
usermod -aG www-data gitlab-runner

#install nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
source ~/.bashrc
nvm install v14.21.3
npm install -g npx --force
#create symlink for nodejs
ln -s /usr/bin/nodejs /usr/bin/node
rm /usr/bin/npx
ln -s /root/.nvm/versions/node/v14.21.3/bin/npx /usr/bin/npx

chown -R www-data:www-data /var/www/*

#install tailwind
cd /var/www/tailwind
npm install -D tailwindcss
npm install
chmod -R 777 /var/www/tailwind/node_modules

/bin/bash