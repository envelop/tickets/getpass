SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `caption` varchar(100) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=default, 1=admin',
  `update` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'need to update nft list',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT 'last update time',
  `mailing` int(11) NOT NULL DEFAULT 0 COMMENT 'last mailing time',
  `mailing_platform` int(11) NOT NULL DEFAULT 0,
  `mailing_organizers` int(11) NOT NULL DEFAULT 0,
  `mailing_events` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `type_address` (`type`,`address`),
  KEY `update` (`update`),
  KEY `update_time` (`update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `app_auths`;
CREATE TABLE `app_auths` (
  `address` varchar(255) NOT NULL,
  `nonce` int(11) DEFAULT NULL,
  PRIMARY KEY (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(64) NOT NULL,
  `init_ip` varchar(45) NOT NULL DEFAULT '' COMMENT 'ipv6 support',
  `ip` varchar(45) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL,
  `active_time` int(11) NOT NULL DEFAULT 0,
  `notifications` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=none, 1=have unread, 2=need recalc',
  `notifications_count` int(11) NOT NULL DEFAULT 0 COMMENT 'for counter',
  `addresses` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'counter (<255)',
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`),
  KEY `active_time` (`active_time`),
  KEY `addresses` (`addresses`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `auth_addresses`;
CREATE TABLE `auth_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth` (`auth`),
  KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `auth_notifications`;
CREATE TABLE `auth_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth` int(11) NOT NULL,
  `notify` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth` (`auth`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `binded_nft`;
CREATE TABLE `binded_nft` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` int(11) NOT NULL,
  `nft` int(11) NOT NULL,
  `whitelist` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `address_whitelist` (`address`,`whitelist`),
  KEY `whitelist_address` (`whitelist`,`address`),
  KEY `address_nft` (`address`,`nft`),
  KEY `whitelist_nft` (`whitelist`,`nft`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `chains`;
CREATE TABLE `chains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `chain_id` int(11) NOT NULL,
  `short` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL DEFAULT '',
  `ticker` varchar(10) NOT NULL DEFAULT '',
  `api_url` varchar(255) NOT NULL DEFAULT '' COMMENT 'url for api',
  `api_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'type:post,header, prop name, key value',
  `nft_url` varchar(255) NOT NULL DEFAULT '' COMMENT '{contract}, {token_id} for replace',
  `status` tinyint(4) NOT NULL COMMENT '0=enabled,1=disabled,2=hidden',
  `sort` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `type_chain_id` (`type`,`chain_id`),
  KEY `type_status` (`type`,`status`),
  KEY `name` (`name`),
  KEY `type_order` (`type`,`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `chains` (`id`, `type`, `chain_id`, `short`, `name`, `description`, `ticker`, `api_url`, `api_key`, `nft_url`, `status`, `sort`) VALUES
(1,	1,	5,	'goerli',	'Görli Testnet',	'',	'eth',	'',	'',	'https://testnets.opensea.io/assets/goerli/{contract}/{token_id}',	0,	4),
(2,	1,	1,	'ethereum',	'Ethereum Mainnet',	'',	'eth',	'',	'',	'https://opensea.io/assets/ethereum/{contract}/{token_id}',	0,	1),
(3,	1,	137,	'polygon',	'Polygon Mainnet',	'',	'matic',	'',	'',	'https://opensea.io/assets/matic/{contract}/{token_id}',	0,	2),
(4,	1,	56,	'bsc',	'Binance Smart Chain Mainnet',	'',	'bnb',	'',	'',	'https://bscscan.com/token/{contract}?a={token_id}',	0,	3);

DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT '>320x180',
  `thumbnail_cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT '320x180',
  `description` longtext NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=preparing,1=published,2=hidden',
  `time` int(11) NOT NULL DEFAULT 0 COMMENT 'publish time',
  `pin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=pinned',
  PRIMARY KEY (`id`),
  KEY `status_pin` (`status`,`pin`),
  KEY `status_time` (`status`,`time`),
  KEY `url_status` (`url`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `content_tags`;
CREATE TABLE `content_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` int(11) NOT NULL,
  `tag` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content` (`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `error`;
CREATE TABLE `error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `url` varchar(100) NOT NULL COMMENT 'short url',
  `title` varchar(255) NOT NULL COMMENT 'event title',
  `short_description` longtext NOT NULL DEFAULT '' COMMENT 'annotation',
  `description` longtext NOT NULL COMMENT 'about',
  `short_location_description` varchar(255) NOT NULL DEFAULT '' COMMENT 'short location caption',
  `location_description` longtext NOT NULL DEFAULT '' COMMENT 'where',
  `speakers_description` longtext NOT NULL DEFAULT '' COMMENT 'who',
  `tickets_description` longtext NOT NULL DEFAULT '' COMMENT 'ticket',
  `check_in_message` longtext NOT NULL DEFAULT '' COMMENT 'success check in',
  `logo_url` varchar(255) NOT NULL DEFAULT '' COMMENT 'logo image (no resize)',
  `cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT 'fullhd background cover',
  `speakers_cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT 'fullhd background cover',
  `location_cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT 'fullhd background cover',
  `partners_cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT 'fullhd background cover',
  `customize` longtext NOT NULL DEFAULT '[]' COMMENT 'customize json',
  `wizard` longtext NOT NULL DEFAULT '[]' COMMENT 'wizard json',
  `time` int(11) NOT NULL DEFAULT 0 COMMENT 'start datetime (for count down)',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=planning, 1=active, 2=archived, 3=hidden',
  `moderation` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=waiting, 1=approved, 2=rejected',
  `moderation_time` int(11) NOT NULL DEFAULT 0 COMMENT 'decision unixtime',
  `moderation_address` int(11) NOT NULL DEFAULT 0 COMMENT 'decision maker',
  `reject_comment` text NOT NULL DEFAULT '' COMMENT 'comment available for event managers',
  `reject_counter` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'if >=3 there is no opportunity to send request to moderation',
  `update` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'trigger for reverse matching',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT 'last update time',
  `summary_files_size` bigint(20) NOT NULL DEFAULT 0 COMMENT 'summary files size',
  `recalc_files_size_time` int(11) NOT NULL DEFAULT 0 COMMENT 'last recalc in unixtime',
  PRIMARY KEY (`id`),
  KEY `url` (`url`),
  KEY `organizer_status` (`organizer`,`status`),
  KEY `status` (`status`),
  KEY `moderation` (`moderation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_content`;
CREATE TABLE `event_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT '>320x180',
  `thumbnail_cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT '320x180',
  `description` longtext NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT -1,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=preparing,1=published,2=hidden',
  `time` int(11) NOT NULL DEFAULT 0 COMMENT 'publish time',
  `pin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=pinned',
  PRIMARY KEY (`id`),
  KEY `event_status_pin` (`event`,`status`,`pin`),
  KEY `event_status_time` (`event`,`status`,`time`),
  KEY `event_url_status` (`event`,`url`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_content_tags`;
CREATE TABLE `event_content_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `content` int(11) NOT NULL,
  `tag` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content` (`content`),
  KEY `event` (`event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_files`;
CREATE TABLE `event_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `path` varchar(255) NOT NULL COMMENT 'from filesystem root',
  `relative_path` varchar(255) NOT NULL COMMENT 'from www dir',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'from user upload, name',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT 'from user upload, mime',
  `size` bigint(20) NOT NULL DEFAULT 0 COMMENT 'from user upload, bytes',
  `target` int(11) NOT NULL DEFAULT 0 COMMENT '0=none, 1=content, 2=event, 3=speaker, 4=session, 5=partner',
  `target_id` int(11) NOT NULL DEFAULT 0,
  `address` bigint(20) NOT NULL DEFAULT 0 COMMENT 'uploader id',
  `time` int(11) NOT NULL COMMENT 'upload unixtime',
  PRIMARY KEY (`id`),
  KEY `organizer` (`organizer`),
  KEY `event` (`event`),
  KEY `target` (`target`),
  KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_invites`;
CREATE TABLE `event_invites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `address` varchar(100) NOT NULL DEFAULT '',
  `platform` int(11) NOT NULL DEFAULT 0 COMMENT 'for future purpose',
  `internal_id` varchar(100) NOT NULL DEFAULT '' COMMENT 'for future purpose',
  `internal_username` varchar(100) NOT NULL DEFAULT '' COMMENT 'for future purpose',
  `admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag',
  `manager` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag',
  `speaker` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag',
  `sponsor` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag',
  `level` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'participant group level',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'invite claimed (0=waiting,1=claimed,2=expired)',
  `time` int(11) NOT NULL DEFAULT 0 COMMENT 'claiming time',
  PRIMARY KEY (`id`),
  KEY `platform_internal_id_internal_username` (`platform`,`internal_id`,`internal_username`),
  KEY `platform` (`platform`),
  KEY `event_status` (`event`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_levels`;
CREATE TABLE `event_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=guest',
  `name` varchar(100) NOT NULL DEFAULT '',
  `caption` varchar(255) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `event_level` (`event`,`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_locations`;
CREATE TABLE `event_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=enabled,1=disabled',
  PRIMARY KEY (`id`),
  KEY `event_status` (`event`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_partners`;
CREATE TABLE `event_partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `cat` int(11) NOT NULL COMMENT 'category',
  `name` varchar(255) NOT NULL,
  `logo_url` varchar(255) NOT NULL DEFAULT '' COMMENT '>250x250',
  `small_logo_url` varchar(255) NOT NULL DEFAULT '' COMMENT '250x250',
  `caption` varchar(255) NOT NULL DEFAULT '' COMMENT 'optional',
  `link` varchar(255) NOT NULL DEFAULT '' COMMENT 'external link (optional)',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT 'internal page (optional)',
  `description` longtext NOT NULL DEFAULT '' COMMENT 'html (optional)',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=default, 1=starred, 2=hidden',
  `sort` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'pin important partners',
  PRIMARY KEY (`id`),
  KEY `event_cat_status_sort` (`event`,`cat`,`status`,`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_partners_cat`;
CREATE TABLE `event_partners_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `event_sort` (`event`,`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_sessions`;
CREATE TABLE `event_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `time` int(11) NOT NULL COMMENT 'unixtime',
  `duration` int(11) NOT NULL DEFAULT 30 COMMENT 'minutes',
  `endtime` int(11) NOT NULL COMMENT 'unixtime',
  `caption` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL DEFAULT '',
  `location` int(11) NOT NULL DEFAULT 0,
  `cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT '>320x180',
  `thumbnail_cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT '320x180',
  `content` longtext NOT NULL DEFAULT '',
  `embed_content` longtext NOT NULL DEFAULT '',
  `level` tinyint(4) NOT NULL DEFAULT -1,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=default,1=starred',
  PRIMARY KEY (`id`),
  KEY `event_time_status_location` (`event`,`time`,`status`,`location`),
  KEY `event_endtime_status_location` (`event`,`endtime`,`status`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_sessions_speakers`;
CREATE TABLE `event_sessions_speakers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `session` int(11) NOT NULL,
  `speaker` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_session` (`event`,`session`),
  KEY `event_speaker` (`event`,`speaker`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_speakers`;
CREATE TABLE `event_speakers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `user` int(11) NOT NULL DEFAULT 0 COMMENT 'for future purpose: linked user within event, not org',
  `platform` int(11) NOT NULL DEFAULT 0 COMMENT 'for future purpose: search address with linked platform',
  `internal_id` varchar(100) NOT NULL DEFAULT '' COMMENT 'for future purpose',
  `internal_username` varchar(100) NOT NULL DEFAULT '' COMMENT 'for future purpose',
  `photo_url` varchar(255) NOT NULL DEFAULT '' COMMENT '>500x500',
  `small_photo_url` varchar(255) NOT NULL DEFAULT '' COMMENT '250x250',
  `tiny_photo_url` varchar(255) NOT NULL DEFAULT '' COMMENT '60x60',
  `public_name` varchar(100) NOT NULL DEFAULT '',
  `nick_name` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `company` varchar(100) NOT NULL DEFAULT '',
  `position` text NOT NULL DEFAULT '',
  `description` text NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=default,1=starred,2=hidden',
  `sort` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'pin important persons',
  `addon` longtext NOT NULL DEFAULT '' COMMENT 'json addon for social links',
  PRIMARY KEY (`id`),
  KEY `event_status_sort` (`event`,`status`,`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_tags`;
CREATE TABLE `event_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `event_sort` (`event`,`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_users`;
CREATE TABLE `event_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'telegram id',
  `event` int(11) NOT NULL,
  `address` int(11) NOT NULL COMMENT 'linked address',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=guest,1=binded,2=invited',
  `binded` int(11) NOT NULL DEFAULT 0 COMMENT 'id from NFT list',
  `invited` int(11) NOT NULL DEFAULT 0 COMMENT 'id from invites',
  `admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag',
  `manager` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag',
  `speaker` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag',
  `sponsor` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag',
  `level` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'participant group level (int value, not id)',
  `trigger` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'set to 1 if needed worker trigger',
  PRIMARY KEY (`id`),
  KEY `trigger` (`trigger`),
  KEY `event_status` (`event`,`status`),
  KEY `address_event_status` (`address`,`event`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `event_whitelist`;
CREATE TABLE `event_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `chain` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'future purpose, not used, 0=unknown,3=721,4=1155',
  `contract` varchar(42) NOT NULL DEFAULT '' COMMENT 'EVM',
  `token_id` varchar(100) NOT NULL DEFAULT '' COMMENT 'varchar for ERC1155 support',
  `binded_nft` int(11) NOT NULL DEFAULT 0 COMMENT 'nft id if binded',
  `level` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'level preset',
  `time` int(11) NOT NULL DEFAULT 0 COMMENT 'bind/unbind time',
  PRIMARY KEY (`id`),
  KEY `contract_binded` (`contract`,`binded_nft`),
  KEY `event_contract_binded_nft` (`event`,`contract`,`binded_nft`),
  KEY `update_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `fast_auth`;
CREATE TABLE `fast_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(50) NOT NULL DEFAULT '' COMMENT 'app name',
  `auth_id` int(11) NOT NULL COMMENT 'auth id',
  `time` int(11) NOT NULL COMMENT 'unixtime',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'for internal app (0=created, 1=counted, 2=charged?)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL COMMENT 'from filesystem root',
  `relative_path` varchar(255) NOT NULL COMMENT 'from www dir',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'from user upload, name',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT 'from user upload, mime',
  `size` bigint(20) NOT NULL DEFAULT 0 COMMENT 'from user upload, bytes',
  `target` int(11) NOT NULL DEFAULT 0 COMMENT '0=none, 1=content, 2=event',
  `target_id` int(11) NOT NULL DEFAULT 0,
  `address` bigint(20) NOT NULL DEFAULT 0 COMMENT 'uploader id',
  `time` int(11) NOT NULL COMMENT 'upload unixtime',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `linked_platforms`;
CREATE TABLE `linked_platforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` int(11) NOT NULL,
  `platform` int(11) NOT NULL,
  `internal_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'platform user id',
  `internal_username` varchar(100) NOT NULL DEFAULT '' COMMENT 'platform username',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=new, 1=wait approve, 2=approved, 3=rejected, 4=deleted',
  `status_time` int(11) NOT NULL DEFAULT 0,
  `hash` varchar(64) NOT NULL DEFAULT '' COMMENT 'for confirmation',
  `flag1` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'ex: news from platform (as user)',
  `flag2` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'ex: notifications',
  `flag3` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'ex: NFT-ticket activation',
  `flag4` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'ex: news from orgs (as participant)',
  `flag5` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'ex: news from events (as participant)',
  `data` longtext NOT NULL DEFAULT '' COMMENT 'platform metadata',
  `back` varchar(255) NOT NULL DEFAULT '' COMMENT 'back url for redirect',
  PRIMARY KEY (`id`),
  KEY `platform_internal_id` (`platform`,`internal_id`),
  KEY `platform_internal_username` (`platform`,`internal_username`),
  KEY `address_platform` (`address`,`platform`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `mailing_rules`;
CREATE TABLE `mailing_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=platform, 1=organizer, 2=event',
  `target` int(11) NOT NULL DEFAULT 0 COMMENT 'target id',
  `rule` tinyint(4) NOT NULL COMMENT '1=subscribed, 2=unsubscribed',
  `time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `address_type_target` (`address`,`type`,`target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `mail_queue`;
CREATE TABLE `mail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` int(11) NOT NULL DEFAULT 0,
  `to` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'optional',
  `subject` varchar(255) NOT NULL,
  `text` longtext NOT NULL COMMENT 'html by default (striptags for text version)',
  `preheader` text NOT NULL DEFAULT '',
  `headers` longtext NOT NULL DEFAULT '' COMMENT 'in json format (key:value)',
  `time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=waiting, 1=delivered, 2=error',
  `status_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `nft_list`;
CREATE TABLE `nft_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `chain` int(11) NOT NULL DEFAULT 0,
  `address` int(11) NOT NULL,
  `contract` varchar(100) NOT NULL,
  `token_id` varchar(100) NOT NULL,
  `time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0=holded, 1=wait activation, 2=wait deactivation, 3=need update',
  `status_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `type_chain_address` (`type`,`chain`,`address`),
  KEY `type_chain_contract` (`type`,`chain`,`contract`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `notifications_queue`;
CREATE TABLE `notifications_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` int(11) NOT NULL,
  `platform` int(11) NOT NULL DEFAULT 0 COMMENT '0=web, other for platforms integrations',
  `time` int(11) NOT NULL COMMENT 'created',
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=text, 1=html',
  `preset` int(11) NOT NULL DEFAULT 0 COMMENT 'from preset list (not customized)',
  `content` longtext NOT NULL COMMENT 'text, html for standart or json data for preset',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=waiting, 1=delivered, 2=readed, 3=active auth not founded',
  `status_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `address_platform_status` (`address`,`platform`,`status`),
  KEY `address_time` (`address`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `oracle_updates`;
CREATE TABLE `oracle_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chain` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0=waiting,1=finished,2=error',
  `status_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `address_chain_status_time` (`address`,`chain`,`status_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `organizers`;
CREATE TABLE `organizers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL DEFAULT '',
  `events_description` longtext NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=waiting, 1=approved, 2=banned, 3=hidden',
  `time` int(11) NOT NULL DEFAULT 0 COMMENT 'start time',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT 'status update time',
  `summary_files_size` bigint(20) NOT NULL DEFAULT 0 COMMENT 'summary files size + event files size',
  `recalc_files_size_time` int(11) NOT NULL DEFAULT 0 COMMENT 'last recalc in unixtime',
  `subscription_plan` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=free basic, 1=pro, 2=vip',
  `subscription_expiration` int(11) NOT NULL DEFAULT 0 COMMENT 'unixtime',
  `max_upload_files_size` int(11) NOT NULL DEFAULT 0 COMMENT 'rewrite subscription plan limits',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `status_url` (`status`,`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `organizer_addresses`;
CREATE TABLE `organizer_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `caption` varchar(100) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL COMMENT '0=admin, 1=owner',
  PRIMARY KEY (`id`),
  KEY `organizer` (`organizer`),
  KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `organizer_content`;
CREATE TABLE `organizer_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT '>320x180',
  `thumbnail_cover_url` varchar(255) NOT NULL DEFAULT '' COMMENT '320x180',
  `description` longtext NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=preparing,1=published,2=hidden',
  `time` int(11) NOT NULL DEFAULT 0 COMMENT 'publish time',
  `pin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=pinned',
  PRIMARY KEY (`id`),
  KEY `organizer_status_pin` (`organizer`,`status`,`pin`),
  KEY `organizer_status_time` (`organizer`,`status`,`time`),
  KEY `organizer_url_status` (`organizer`,`url`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `organizer_content_tags`;
CREATE TABLE `organizer_content_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `content` int(11) NOT NULL,
  `tag` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content` (`content`),
  KEY `organizer` (`organizer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `organizer_files`;
CREATE TABLE `organizer_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `path` varchar(255) NOT NULL COMMENT 'from filesystem root',
  `relative_path` varchar(255) NOT NULL COMMENT 'from www dir',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'from user upload, name',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT 'from user upload, mime',
  `size` bigint(20) NOT NULL DEFAULT 0 COMMENT 'from user upload, bytes',
  `target` int(11) NOT NULL DEFAULT 0 COMMENT '0=none, 1=content, 2=event',
  `target_id` int(11) NOT NULL DEFAULT 0,
  `address` bigint(20) NOT NULL DEFAULT 0 COMMENT 'uploader id',
  `time` int(11) NOT NULL COMMENT 'upload unixtime',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `address` (`address`),
  KEY `organizer` (`organizer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `organizer_tags`;
CREATE TABLE `organizer_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `organizer_sort` (`organizer`,`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `platforms`;
CREATE TABLE `platforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=disabled, 1=enabled',
  `mailing` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=disabled, 1=enabled',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `platforms` (`id`, `name`, `status`, `mailing`) VALUES
(1,	'Telegram',	1,	1),
(2,	'Email',	1,	1);

DROP TABLE IF EXISTS `presets`;
CREATE TABLE `presets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `presets` (`id`, `name`, `value`) VALUES
(1,	'org_approved',	'✔️ Organizer <a href=\"/@{organizer_url}/\">{organizer_title}</a> has been approved by the platform moderation'),
(2,	'org_rejected',	'❌ Organizer {organizer_title} has been rejected by the platform moderation'),
(3,	'org_admin_added',	'✔️ You have been added to the administration of the organizer <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a>'),
(4,	'org_admin_deleted',	'❌ You have been removed from the administration of the organizer <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a>'),
(5,	'org_new_owner',	'👑 You have been appointed as the new owner of the organizer <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a>'),
(6,	'org_new_event',	'📢 New event created by organizer <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a>: <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a>'),
(7,	'event_user_invited',	'✔️ You have been invited to the event <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a> (by organizer <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a>)'),
(8,	'event_user_deactivation',	'❌ The NFT ticket has been deactivated: <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a>'),
(9,	'event_user_activation',	'🎟️ The NFT ticket has been activated for the event: <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a>'),
(10,	'admin_new_org',	'📢 New organizer <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a> has been added to <a href=\"/admin/organizers/?status=0\" target=\"_blank\">the list for moderation</a>'),
(11,	'admin_new_event',	'📢 New event <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a> from organizer <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a> has been added to <a href=\"/admin/events/?moderation=0\" target=\"_blank\">the list for moderation</a>'),
(12,	'admin_new_request_linked_platform_deletion',	'📢 New <a href=\"/admin/requests/?target=1&status=0\" target=\"_blank\">request to delete linked platform</a> from {address}'),
(13,	'request_1_was_approved',	'✔️ Your request to remove linked platform was approved'),
(14,	'request_1_was_rejected',	'❌ Your request to remove linked platform was rejected'),
(15,	'org_expired_subscription',	'⚠️ <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a> paid subscription has expired, please renew your subscription so you do not encounter file size limitations'),
(16,	'event_was_approved',	'✔️ Event <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a> was approved by moderation'),
(17,	'event_was_rejected',	'❌ Event <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a> was rejected by moderation'),
(18,	'event_was_rejected_with_comment',	'❌ Event <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a> was rejected by moderation with comment:<br>\r\n{comment}'),
(19,	'admin_moderation_event',	'📢 Event <a href=\"/@{organizer_url}/{event_url}/\" target=\"_blank\">{event_title}</a> from organizer <a href=\"/@{organizer_url}/\" target=\"_blank\">{organizer_title}</a> has been requested to <a href=\"/admin/events/?status=1&moderation=!1\" target=\"_blank\">moderation</a>');

DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target` int(11) NOT NULL DEFAULT 0 COMMENT '0=none (test), 1=delete linked platforms',
  `target_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=under review, 1=request approved, 2=request rejected',
  `status_time` int(11) NOT NULL DEFAULT 0 COMMENT 'decision unixtime',
  `status_address` int(11) NOT NULL DEFAULT 0 COMMENT 'decision maker',
  PRIMARY KEY (`id`),
  KEY `time_status` (`time`,`status`),
  KEY `target` (`target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `signer_history`;
CREATE TABLE `signer_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `time` int(11) NOT NULL COMMENT 'unixtime for range of auth (2 min)',
  `nonce` bigint(20) NOT NULL COMMENT 'add noise fro signed data',
  `signature` text NOT NULL DEFAULT '',
  `recovered_pubkey` varchar(255) NOT NULL DEFAULT '',
  `recovered_address` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=created,1=verified,2=error',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `telegram_groups`;
CREATE TABLE `telegram_groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(4) NOT NULL COMMENT '0=group,1=supergroup,2=channel',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=none,1=admin,2=admin with permissions,3=invite created',
  `status_time` int(11) NOT NULL DEFAULT 0,
  `invite_link` varchar(255) NOT NULL DEFAULT '' COMMENT 'from createChatInviteLink',
  `check` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=finished,1=started',
  `check_time` int(11) NOT NULL DEFAULT 0,
  `event` int(11) NOT NULL DEFAULT 0 COMMENT 'event id',
  `filter` longtext NOT NULL DEFAULT '' COMMENT 'filter props for future (serialized)',
  PRIMARY KEY (`id`),
  KEY `status_status_time` (`status`,`status_time`),
  KEY `check_check_time` (`check`,`check_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `telegram_groups_history`;
CREATE TABLE `telegram_groups_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `action` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=decline,1=approve,2=kicked',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `telegram_polls`;
CREATE TABLE `telegram_polls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` varchar(32) NOT NULL DEFAULT '',
  `out_id` int(11) NOT NULL DEFAULT 0,
  `respond` longtext NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `telegram_queue`;
CREATE TABLE `telegram_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `chat_id` varchar(255) NOT NULL DEFAULT '',
  `method` varchar(255) NOT NULL DEFAULT '',
  `data` longtext NOT NULL DEFAULT '',
  `inline_keyboard` longtext NOT NULL DEFAULT '',
  `keyboard` longtext NOT NULL DEFAULT '',
  `reply_message_id` int(11) NOT NULL DEFAULT 0,
  `message_id` int(11) NOT NULL DEFAULT 0 COMMENT 'received message_id from telegram',
  `pin` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'send pinChatMessage',
  `http_status` smallint(6) NOT NULL DEFAULT 0 COMMENT 'only on error',
  `error` longtext NOT NULL DEFAULT '' COMMENT 'of telegram respond without successful result',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `telegram_updates`;
CREATE TABLE `telegram_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offset` int(11) NOT NULL COMMENT 'telegram internal offset',
  `time` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1=processed',
  PRIMARY KEY (`id`),
  KEY `offset` (`offset`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `telegram_users`;
CREATE TABLE `telegram_users` (
  `id` bigint(20) NOT NULL COMMENT 'telegram id',
  `username` varchar(100) NOT NULL DEFAULT '',
  `first_name` varchar(100) NOT NULL DEFAULT '',
  `last_name` varchar(100) NOT NULL DEFAULT '',
  `language_code` varchar(3) NOT NULL DEFAULT '',
  `plugin` varchar(100) NOT NULL COMMENT 'current user plugin',
  `step` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'plugin step for navigation',
  `state` longtext NOT NULL DEFAULT '' COMMENT 'serialized array for plugin',
  `trigger` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'set to 1 if needed worker trigger',
  KEY `trigger` (`trigger`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `types`;
CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `signer` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=disabled,1=internal support',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `types` (`id`, `name`, `signer`) VALUES
(1,	'EVM',	1);