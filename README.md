# GetPass - EMS (Event Management System)

Introducing GetPass, the revolutionary event management system that uses web3 wallet (Metamask/WalletConnect) to allow users to login and manage their events with ease.

With GetPass, organizers can easily configure NFT whitelists for participant entrance and keep track of participants’ attendance in a secure way.

GetPass is a great tool for any organizer looking to streamline their event management process! Link platforms like email and telegram to deliver content wisely, notify your audience with notifications, organize events and blogs, create an event agenda with sessions and speakers as well as a list of partners.

GetPass supports multiple addresses in one session.

## Admin workflow

- Manage addresses
- Manage supported chains
- Configure delivering content platforms (mail & telegram)
- Manage Organizers (approve/ban)
- Manage Events
- Manage Content

## Organizer workflow

- Manage Administration
- Manage Organizer Content
- Manage Events

## Event manager workflow

- Manage participand levels
- Manage invites (team/speaker addresses)
- Manage NFT Whitelist (chain, contract, token id)
- Configure event information
- Manage agenda (speakers, sessions, locations)
- Manage partners
- Manage public event content (for guests with 0 level)
- Manage closed event content (for participants with >=1 level)
- Manage session content (customizable level for gaining access)

## Participant workflow

- Sign-in with web3 address
- Link email, telegram (for notifications and content delivery)
- Visit event page
- Buy NFT ticket
- Check-in to get pass (access to closed content and sessions)

## Technology stack & examples

- PHP-FPM 8.2
- Nginx ([example config](./conf/nginx-platform.web))
- MariaDB (MySQL) ([init dump](./dump/platform.sql))
- Crontab jobs ([example](./conf/crontab))
- Telegram bot (using [Telegam API](https://core.telegram.org/bots/api), workers dir, [BotPlugins](./src/class/BotPlugins/))

## Install

The installation process is easy: clone the repository, edit the environment variables, and then use docker-compose to bring up the containers.

```
git clone https://gitlab.com/envelop/tickets/getpass.git
cd getpass
nano .env
docker-compose build
docker-compose up
```

### Development docker environment (!!!! Compound V2 !!!!)
1. Clone or  pull repo
```bash
git pull
```
2. Create/Edit `.env.local` 

3. Put available db dump into `./dump` folder
4. just Up
```bash
docker compose  -f docker-compose-local.yml up
```
webapp: http://localhost:8080/  
db admin tool: http://localhost:8888/

The web-service will be running on port 8080. Tailwind will be triggered once a minute to rebuild. All standard output from the workers will be logged in the [logs directory](./logs/). If you need to use a landing page, deploy/update the build result (with an index.html entrypoint) to the [landing directory](./landing/).

Open [localhost:8080](http://localhost:8080/) and proceed with the installation. It will automatically import the SQL structures and refresh the page.


### Envelop Stage environment
For load new dump
1. Change folder to that contain sql dump
```bash
scp dump.sql devops@<stage_address>:/home/devops/tickets-getpass-platform/dump
``` 

## Dependencies

- [tailwind css](https://github.com/tailwindlabs/tailwindcss) (css framework, [config example](./tailwind/), `npm install`, `npx tailwindcss -i input.css -o app.css`)
- [cash-js](https://github.com/fabiospampinato/cash)
- [walletconnect sdk](https://github.com/WalletConnect/walletconnect-monorepo/releases/tag/1.7.8)
- [jdenticon](https://github.com/dmester/jdenticon) (for generative avatars from user address string)
- [CKEditor 5 Classic](https://github.com/ckeditor/ckeditor5) (as WYSIWYG)
- [atcb](https://github.com/add2cal/add-to-calendar-button) (js addon, adding event session to calendar)
- [oembed-js](https://github.com/kudago/oembed-all) (js addon with modifications)
- [PHPMailer](https://github.com/PHPMailer/PHPMailer/) (SMTP support)
- [Telegram Auth Widget](https://core.telegram.org/widgets/login)
- [viz-php-lib](https://github.com/VIZ-Blockchain/viz-php-lib)
- php-imagick (for thumbnails)
- GMP (GNU Multiple Precision) — installed as php extension
- BCMath — as GMP alternative

