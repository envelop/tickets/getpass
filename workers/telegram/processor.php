<?php
error_reporting(255);
include('worker_preload.php');
pcntl_signal(SIGINT,'sig_handler');
pcntl_signal(SIGTERM,'sig_handler');
pcntl_signal(SIGQUIT,'sig_handler');
//SIGHUP for restart?

register_shutdown_function('fatal_handler');

if(!$_SERVER['PWD']){
	exit;
}

$pname='worker';
$pid_file=$root_dir.'/'.$pname.'.pid';
$pid=posix_getpid();
$old_pid=false;
if(file_exists($pid_file)){
	$old_pid=file_get_contents($pid_file);
}
if($old_pid){
	$working=posix_getpgid($old_pid);
	if($working){
		print $pname.' already working, kill with SIGINT, PID: '.$old_pid.PHP_EOL;
		posix_kill($old_pid,SIGINT);
	}
	while($working){
		usleep(100000);//check every 0.1 sec
		$working=posix_getpgid($old_pid);
	}
	print $pname.' old PID '.$old_pid.' was stopped, continued...'.PHP_EOL;
	if(file_exists($pid_file)){
		print '['.date('d.m.Y H:i:s').'] SHUTDOWN old pid file: '.$pid_file.PHP_EOL;
		unlink($pid_file);
	}
}
file_put_contents($pid_file,$pid);
print '['.date('d.m.Y H:i:s').'] STARTUP pid file: '.$pid_file.', pid: '.$pid.PHP_EOL;
$work=true;

foreach($options['plugins'] as $plugin_name){
	$plugin='plugin_'.strtolower($plugin_name);
	$plugin_class='BotPlugins\\'.$plugin_name.'Plugin';
	if(class_exists($plugin_class)){
		$$plugin=new $plugin_class();
		$$plugin->load();
	}
}
$plugin_ae_sleep=[];//microtime for next automatic execution for each plugin

$usleep_min_timeout=500000*2;//0.5*3 sec between updates/queues checks
while($work){
	$work_start=microtime(true);

	$media_groups=[];
	$media_groups_id=[];
	$media_groups_data=[];
	$media_groups_plugin=[];
	$trigger=false;

	//check telegram_users for trigger flag
	$q=$db->sql("SELECT * FROM `telegram_users` WHERE `trigger`=1");
	while($user=$db->row($q)){
		$plugin='plugin_'.strtolower($user['plugin']);
		print 'User '.$user['id'].' trigger ['.$user['plugin'].']'.PHP_EOL;
		$resolved=false;
		if(isset($$plugin)){
			if(method_exists($$plugin,'process_user')){
				print 'Trigger process_user for ['.$user['plugin'].']'.PHP_EOL;
				$trigger=true;
				$resolved=$$plugin->process_user();
				$trigger=false;
			}
		}
		if($resolved){
			$db->sql("UPDATE `telegram_users` SET `trigger`=0 WHERE `id`='".$user['id']."'");
		}
	}

	foreach($options['plugins'] as $plugin_name){
		$plugin='plugin_'.strtolower($plugin_name);
		if(isset($$plugin)){
			if(method_exists($$plugin,'automatic_execution')){
				$ae_time=microtime(true);
				if(!isset($plugin_ae_sleep[$plugin])){
					$plugin_ae_sleep[$plugin]=0;
				}
				if($plugin_ae_sleep[$plugin]<$ae_time){
					$plugin_ae_sleep[$plugin]=$$plugin->automatic_execution();
					print 'Automatic execution by ['.$plugin_name.'] '.round(microtime(true) - $ae_time,5).'s'.PHP_EOL;
				}
			}
		}
	}

	$q=$db->sql("SELECT `id`,`data` FROM `telegram_updates` WHERE `status`=0 ORDER BY `id` ASC");
	while($m=$db->row($q)){
		$id=$m['id'];
		$use_plugin=false;
		$user=[];
		$update=unserialize($m['data']);

		$chat_id=$update['message']['chat']['id'];
		$chat_type=$update['message']['chat']['type'];
		$chat_type_id=0;//group by default
		if('supergroup'==$chat_type){
			$chat_type_id=1;
		}
		if('channel'==$chat_type){
			$chat_type_id=2;
		}
		if('private'==$chat_type){
			$chat_type_id=3;
		}
		//print_r($update);
		//https://core.telegram.org/bots/api#update
		if(isset($update['edited_message'])){
			//if need to action, call plugin with update type?
			//just ignore edit message updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['my_chat_member'])){//triggers when bot was deleted or reset, or changed type in group
			//if need to action, call plugin with update type?
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['channel_post'])){//triggers when bot was added to channel and receive new post
			//if need to action, call plugin with update type?
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['edited_channel_post'])){//triggers when bot was added to channel and receive edited post
			//if need to action, call plugin with update type?
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['shipping_query'])){//triggers when bot was invoiced?
			//if need to action, call plugin with update type?
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['pre_checkout_query'])){//triggers when bot was checkout?
			//if need to action, call plugin with update type?
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['chat_member'])){//triggers when bot admin in group and specify "chat_member" in the list of "allowed_updates"
			//if need to action, call plugin with update type?
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['chat_join_request'])){//triggers when bot admin in group and have "can_invite_users" right
			//if need to action, call plugin with update type?
			if(isset($plugin_groups)){
				if(method_exists($plugin_groups,'process_join_request')){
					$chat_id=$update['chat_join_request']['chat']['id'];
					$user_id=$update['chat_join_request']['from']['id'];
					$plugin_groups->process_join_request($chat_id,$user_id);
				}
			}
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['callback_query'])){//triggers when bot get callback from button
			$chat_type=$update['callback_query']['message']['chat']['type'];
			$chat_id=$update['callback_query']['message']['chat']['id'];
			$message_id=$update['callback_query']['message']['message_id'];
			$callback_id=$update['callback_query']['id'];
			if('private'==$chat_type){
				$user=$db->sql_row("SELECT * FROM `telegram_users` WHERE `id`='".$chat_id."'");
				if(!is_null($user)){
					print 'User '.$user['id'].' try callback ['.$user['plugin'].']'.PHP_EOL;
					$plugin='plugin_'.strtolower($user['plugin']);
					if(isset($$plugin)){
						if(method_exists($$plugin,'callback_query')){
							print 'Callback resolved by ['.$plugin.'] '.$id.PHP_EOL;
							$callback=$update['callback_query']['data'];
							$$plugin->callback_query($callback);
						}
					}
				}
			}
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['inline_query'])){//triggers when bot get request from inline chat @mentition
			//if need to action, call plugin with update type?
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['chosen_inline_result'])){//triggers when user select result from inline chat @mentition
			//if need to action, call plugin with update type?
			//just ignore bot reset updates
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['poll_answer'])){
			if(isset($update['poll_answer']['poll_id'])){
				$poll_id=$update['poll_answer']['poll_id'];
				$db->sql("UPDATE `telegram_polls` SET `respond`='".$db->prepare(serialize($update['poll_answer']['option_ids']))."' WHERE `poll_id`='".$poll_id."'");
			}
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['poll'])){
			if(isset($update['poll']['id'])){
				$poll_id=$update['poll']['id'];
				$db->sql("UPDATE `telegram_polls` SET `respond`='".$db->prepare(serialize($update['poll']['options']))."' WHERE `poll_id`='".$poll_id."'");
			}
			$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
		}
		if(isset($update['message'])){
			$message_id=$update['message']['message_id'];
			$message_text=false;
			$message_entities=false;

			$message_animation=false;
			$message_voice=false;
			$message_video=false;
			$message_video_note=false;
			$message_audio=false;
			$message_photo=false;
			$message_sticker=false;
			$message_document=false;
			$message_media_group=false;
			$message_poll=false;
			$message_dice=false;
			$message_game=false;
			$message_contact=false;
			$message_invoice=false;
			$message_location=false;
			$message_venue=false;
			$message_connected_website=false;

			$message_caption=false;
			$message_caption_entities=false;
			$message_parse_mode=false;

			//forwarded exclude other data
			//forwardMessage & copyMessage works only if bot in the original channel
			//thats why ignore it and try get all context from message
			/*
			if(isset($update['message']['forward_from_message_id'])){
				$forward_message=$update['message']['forward_from_message_id'];
				$forward_message_from=$update['message']['forward_from_chat']['id'];
			}
			*/

			if(isset($update['message']['text'])){
				$message_text=$update['message']['text'];
			}
			if(isset($update['message']['entities'])){
				$message_entities=$update['message']['entities'];
			}
			if(isset($update['message']['game'])){
				$message_game=true;
			}
			if(isset($update['message']['location'])){
				$message_location=true;
			}
			if(isset($update['message']['venue'])){
				$message_venue=true;
			}
			if(isset($update['message']['connected_website'])){
				$message_connected_website=$update['message']['connected_website'];
			}
			if(isset($update['message']['invoice'])){
				$message_invoice=true;
			}
			if(isset($update['message']['contact'])){
				$message_contact=true;
			}
			if(isset($update['message']['animation'])){
				$message_animation=$update['message']['animation']['file_id'];
			}
			if(isset($update['message']['voice'])){
				$message_voice=$update['message']['voice']['file_id'];
			}
			if(isset($update['message']['dice'])){
				$message_dice=$update['message']['dice'];
			}
			if(isset($update['message']['video'])){
				$message_video=$update['message']['video']['file_id'];
			}
			if(isset($update['message']['video_note'])){
				$message_video_note=$update['message']['video_note']['file_id'];
			}
			if(isset($update['message']['sticker'])){
				$message_sticker=$update['message']['sticker']['file_id'];
			}
			if(isset($update['message']['document'])){
				$message_document=$update['message']['document']['file_id'];
			}
			if(isset($update['message']['audio'])){
				$message_audio=$update['message']['audio']['file_id'];
			}
			if(isset($update['message']['photo'])){
				if(isset($update['message']['photo'][0])){
					$last_item=count($update['message']['photo'])-1;
					$message_photo=$update['message']['photo'][$last_item]['file_id'];
				}
			}
			if(isset($update['message']['video_note'])){
				$message_video_note=$update['message']['video_note']['file_id'];
			}

			if(isset($update['message']['media_group_id'])){
				$message_media_group=$update['message']['media_group_id'];
			}
			if(isset($update['message']['poll'])){
				$message_poll=[];
				$message_poll['question']=$update['message']['poll']['question'];
				$message_poll['options']=[];
				foreach($update['message']['poll']['options'] as $item){
					$message_poll['options'][]=$item['text'];
				}
				if(isset($update['message']['poll']['type'])){
					$message_poll['type']=$update['message']['poll']['type'];
				}
				if(isset($update['message']['poll']['is_anonymous'])){
					$message_poll['is_anonymous']=$update['message']['poll']['is_anonymous'];
				}
				if(isset($update['message']['poll']['allows_multiple_answers'])){
					$message_poll['allows_multiple_answers']=$update['message']['poll']['allows_multiple_answers'];
				}
				if(isset($update['message']['poll']['correct_option_id'])){
					$message_poll['correct_option_id']=$update['message']['poll']['correct_option_id'];
				}
				if(isset($update['message']['poll']['explanation'])){
					$message_poll['explanation']=$update['message']['poll']['explanation'];
				}
				if(isset($update['message']['poll']['explanation_parse_mode'])){
					$message_poll['explanation_parse_mode']=$update['message']['poll']['explanation_parse_mode'];
				}
				if(isset($update['message']['poll']['explanation_entities'])){
					$message_poll['explanation_entities']=$update['message']['poll']['explanation_entities'];
				}
				if(isset($update['message']['poll']['open_period'])){
					$message_poll['open_period']=$update['message']['poll']['open_period'];
				}
				if(isset($update['message']['poll']['close_date'])){
					$message_poll['close_date']=$update['message']['poll']['close_date'];
				}
			}

			if(isset($update['message']['caption'])){
				$message_caption=$update['message']['caption'];
			}
			if(isset($update['message']['caption_entities'])){
				$message_caption_entities=$update['message']['caption_entities'];
			}
			if(isset($update['message']['parse_mode'])){
				$message_parse_mode=$update['message']['parse_mode'];
			}

			if(isset($update['message']['pinned_message'])){
				//just ignore pinned message
				$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
			}
			else{
				if(1>=$chat_type_id){//group and supergroup
					$ignore_creation=false;
					if(isset($update['message']['migrate_to_chat_id'])){
						$db->sql("UPDATE `telegram_groups` SET `id`='".$db->prepare($update['message']['migrate_to_chat_id'])."' WHERE `id`='".$chat_id."'");
						$ignore_creation=true;
					}
					else
					if(isset($update['message']['migrate_from_chat_id'])){
						$db->sql("UPDATE `telegram_groups` SET `id`='".$chat_id."', `type`='".$chat_type_id."' WHERE `id`='".$db->prepare($update['message']['migrate_from_chat_id'])."'");
						$ignore_creation=true;
					}
					else
					if(isset($update['message']['group_chat_created'])){
						//do nothing, create next
					}

					$group=$db->sql_row("SELECT * FROM `telegram_groups` WHERE `id`='".$chat_id."'");
					if(!$ignore_creation){
						if(null===$group){
							$user_name='';
							if(isset($update['message']['chat']['username'])){
								$user_name=$update['message']['chat']['username'];
							}
							$db->sql("INSERT INTO `telegram_groups` (`id`,`username`,`title`,`type`) VALUES ('".$chat_id."','".$user_name."','".$db->prepare($update['message']['chat']['title'])."','".$chat_type_id."')");
						}
						else{
							if($group['title']!=$update['message']['chat']['title']){
								$db->sql("UPDATE `telegram_groups` SET `title`='".$db->prepare($update['message']['chat']['title'])."', `type`='".$chat_type_id."' WHERE `id`='".$chat_id."'");
							}
						}
					}

					if(false!==$message_text){//triggers only on text
						$user_id=$update['message']['from']['id'];
						$user=$db->sql_row("SELECT * FROM `telegram_users` WHERE `id`='".$user_id."'");
						$status=$plugin_groups->process_update();
						if($status){
							$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
						}
						else{//not processed
							print '['.$use_plugin.'] Error with update:'.PHP_EOL;
							$db->sql("UPDATE `telegram_updates` SET `status`=2 WHERE `id`='".$m['id']."'");
						}
					}
					else{//all not text updates flags as processed
						$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
					}
				}
				if('private'==$chat_type){//personal chat with user
					if(0==$db->table_count('telegram_users',"WHERE `id`='".$chat_id."'")){
						/*
						//old behaviour for init admins (in new version we use database match for permissions)
						$init_admin=0;
						if(in_array($chat_id,$options['init_admins'])){
							$init_admin=1;
						}
						*/
						$user_name='';
						if(isset($update['message']['chat']['username'])){
							$user_name=$update['message']['chat']['username'];
						}
						$user_first_name='';
						if(isset($update['message']['chat']['first_name'])){
							$user_first_name=$update['message']['chat']['first_name'];
						}
						$user_last_name='';
						if(isset($update['message']['chat']['last_name'])){
							$user_last_name=$update['message']['chat']['last_name'];
						}
						$user_language_code='';
						if(isset($update['message']['from'])){
							if(isset($update['message']['from']['language_code'])){
								$user_language_code=$update['message']['from']['language_code'];
							}
						}
						$db->sql("INSERT INTO `telegram_users` (`id`,`plugin`,`username`,`first_name`,`last_name`,`language_code`) VALUES ('".$chat_id."','".$options['init_plugin']."','".$db->prepare($user_name)."','".$db->prepare($user_first_name)."','".$db->prepare($user_last_name)."','".$db->prepare($user_language_code)."')");
						//$user_arr=$db->sql_row("SELECT * FROM `telegram_users` WHERE `id`='".$chat_id."'");
						//$find_invite=$plugin_accounts->check_invite($user_arr);//+ status=1
					}
					$user=$db->sql_row("SELECT * FROM `telegram_users` WHERE `id`='".$chat_id."'");
					if(isset($update['message']['chat']['username'])){
						if($update['message']['chat']['username']!=$user['username']){//user update username, update metadata
							$user_name='';
							if(isset($update['message']['chat']['username'])){
								$user_name=$update['message']['chat']['username'];
								$user['username']=$user_name;
							}
							$user_first_name='';
							if(isset($update['message']['chat']['first_name'])){
								$user_first_name=$update['message']['chat']['first_name'];
								$user['first_name']=$user_first_name;
							}
							$user_last_name='';
							if(isset($update['message']['chat']['last_name'])){
								$user_last_name=$update['message']['chat']['last_name'];
								$user['last_name']=$user_last_name;
							}
							$user_language_code='';
							if(isset($update['message']['from'])){
								if(isset($update['message']['from']['language_code'])){
									$user_language_code=$update['message']['from']['language_code'];
									$user['language_code']=$user_language_code;
								}
							}
							$db->sql("UPDATE `telegram_users` SET `username`='".$db->prepare($user_name)."'".($user_first_name?", `first_name`='".$db->prepare($user_first_name)."'":'').($user_last_name?", `last_name`='".$db->prepare($user_last_name)."'":'').($user_language_code?", `language_code`='".$db->prepare($user_language_code)."'":'')." WHERE `id`='".$user['id']."'");
						}
					}

					$use_plugin=$user['plugin'];

					//check keyboard preset for each active plugin
					$preset_action=false;
					if(false!==$message_text){//only for text message
						foreach($options['plugins'] as $plugin_name){
							$plugin='plugin_'.strtolower($plugin_name);
							if(isset($$plugin)){
								if(method_exists($$plugin,'preset_action')){
									print 'Try preset action ['.$plugin_name.'] '.$id.PHP_EOL;
									$find_preset_action=$$plugin->preset_action();
									//return true if plugin want to prevent execution
									//return false if plugin want to continue (plugin can change user state and use plugin) or nothing changed
									if(true===$find_preset_action){
										$preset_action=true;
										print 'Preset action '.$id.' resolved by ['.$plugin_name.']'.PHP_EOL;
										$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
										break;
									}
								}
							}
						}
					}
					if(false===$preset_action){//if not preset action
						while(false!=$use_plugin){//if know what a plugin
							print $m['id'].' '.$use_plugin.PHP_EOL;
							//check keyboard preset for current user plugin
							$internal_action=false;
							if(false!==$message_text){//only for text message
								$plugin='plugin_'.strtolower($use_plugin);
								if(isset($$plugin)){
									if(method_exists($$plugin,'internal_action')){
										print 'Try internal action ['.$use_plugin.'] '.$id.PHP_EOL;
										$find_internal_action=$$plugin->internal_action();
										//return true if plugin want to prevent execution
										//return false if plugin want to continue (plugin can change user state and use plugin) or nothing changed
										if(true===$find_internal_action){
											$internal_action=true;
											print 'Internal action '.$id.' was resolved'.PHP_EOL;
											$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
											break;
										}
									}
								}
							}
							if(false===$internal_action){//if update not captured by internal action
								if('echo'==$use_plugin){
									$status=$plugin_echo->process_update();
									if($status){
										$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
									}
									else{
										print '['.$use_plugin.'] Error with update:'.PHP_EOL;
										print_r($update);
										exit;
									}
									$use_plugin=false;
								}
								if('index'==$use_plugin){
									$status=$plugin_index->process_update();
									if($status){
										$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
									}
									else{
										print '['.$use_plugin.'] Error with update:'.PHP_EOL;
										print_r($update);
										exit;
									}
									$use_plugin=false;
								}

								if(false!==$use_plugin){
									//if plugin is unknown - set user to default init plugin and trigger it
									print '['.$use_plugin.'] Error with unknown plugin (reset to default)'.PHP_EOL;
									$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$m['id']."'");
									$db->sql("UPDATE `telegram_users` SET `plugin`='".$db->prepare($options['init_plugin'])."', `trigger`=1 WHERE `id`='".$user['id']."'");
									//exit;
								}
							}
						}
					}
				}
			}
		}
	}

	//it just example for echo room, if array $media_groups is filled, sendMediaGroup back to $user_id
	//but need to expand it to support user plugin mediagroup action?
	//need to execute all $media_groups
	foreach($media_groups as $user_id=>$media_group){
		$user=[];
		$user=$db->sql_row("SELECT * FROM `telegram_users` WHERE `id`='".$user_id."'");
		if(!is_null($user)){
			foreach($media_group as $media_group_id=>$media_arr){
				$data=[];
				$data['media']=$media_arr;
				foreach($media_groups_data[$media_group_id] as $attr=>$value){
					$data['media'][0][$attr]=$value;
				}

				$plugin='plugin_'.($media_groups_plugin[$media_group_id]);
				if(isset($$plugin)){
					if(method_exists($$plugin,'process_media_group')){
						$$plugin->process_media_group($user,$data);//plugin resolve media group itself
						print 'Media group '.$media_group_id.' from user'.$user_id.' resolved by ['.$media_groups_plugin[$media_group_id].']'.PHP_EOL;
					}
				}

				if(isset($media_groups_id[$media_group_id])){
					foreach($media_groups_id[$media_group_id] as $update_id){
						$db->sql("UPDATE `telegram_updates` SET `status`=1 WHERE `id`='".$update_id."'");
					}
				}
			}
		}
	}

	if(!file_exists($pid_file)){
		print '['.date('d.m.Y H:i:s').'] SHUTDOWN pid file was deleted: '.$pid_file.PHP_EOL;
		exit;
	}
	pcntl_signal_dispatch();//check any signal come?
	//exit;//temp reboot for each start
	$work_diff=ceil(1000000*(microtime(true) - $work_start));
	usleep($usleep_min_timeout - min($usleep_min_timeout,$work_diff));
}
exit;