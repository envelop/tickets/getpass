<?php
error_reporting(255);
set_include_path('.');
include('worker_preload.php');

pcntl_signal(SIGINT,'sig_handler');
pcntl_signal(SIGTERM,'sig_handler');
pcntl_signal(SIGQUIT,'sig_handler');
//SIGHUP for restart?

register_shutdown_function('fatal_handler');

if(!$_SERVER['PWD']){//only for cron
	exit;
}

$pname='telegram_parser';
$pid_file=$root_dir.'/'.$pname.'.pid';
$pid=posix_getpid();
$old_pid=false;
if(file_exists($pid_file)){
	$old_pid=file_get_contents($pid_file);
}
if($old_pid){
	$working=posix_getpgid($old_pid);
	if($working){
		print $pname.' already working, kill with SIGINT, PID: '.$old_pid.PHP_EOL;
		posix_kill($old_pid,SIGINT);
	}
	while($working){
		usleep(100000);//check every 0.1 sec
		$working=posix_getpgid($old_pid);
	}
	print $pname.' old PID '.$old_pid.' was stopped, continued...'.PHP_EOL;
	if(file_exists($pid_file)){
		print '['.date('d.m.Y H:i:s').'] SHUTDOWN old pid file: '.$pid_file.PHP_EOL;
		unlink($pid_file);
	}
}
file_put_contents($pid_file,$pid);
print '['.date('d.m.Y H:i:s').'] STARTUP pid file: '.$pid_file.', pid: '.$pid.PHP_EOL;
$work=true;

$offset=$db->select_one('telegram_updates','`offset`','ORDER BY `id` DESC');
if(is_null($offset)){
	$offset=0;
}
$usleep_min_timeout=500000*2;//0.5*5 sec between long pooling (lasts min 1 sec, up to 3-4 sec)
while($work){
	$work_start=microtime(true);
	$updates_json=telegram_method('getUpdates',array('offset'=>1+$offset,'timeout'=>1),false,true);
	if($updates_json){
		$updates=json_decode($updates_json,true);
		if(1==$updates['ok']){
			$update_count=0;
			foreach($updates['result'] as $update_state){
				if($update_state['update_id']>$offset){
					$update_count++;
					$offset=(int)$update_state['update_id'];
					unset($update_state['update_id']);
					$db->sql("INSERT INTO `telegram_updates` (`offset`,`time`,`data`) VALUES ('".$offset."','".time()."','".$db->prepare(serialize($update_state))."')");
				}
			}
			if(0<$update_count){
				print '['.date('d.m.Y H:i:s').'] '.$update_count.PHP_EOL;
			}
		}
	}
	if(!file_exists($pid_file)){
		print '['.date('d.m.Y H:i:s').'] SHUTDOWN pid file was deleted: '.$pid_file.PHP_EOL;
		exit;
	}
	pcntl_signal_dispatch();//check any signal come?
	$work_diff=ceil(1000000*(microtime(true) - $work_start));
	usleep($usleep_min_timeout - min($usleep_min_timeout,$work_diff));
}
exit;