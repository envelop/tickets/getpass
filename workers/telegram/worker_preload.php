<?php
if(!isset($_ENV['MYSQL_USER'])){
	$_ENV=$_SERVER;
}
$path_to_platform='/var/www/html';//example: /var/www/html
include($path_to_platform.'/preload.php');
error_reporting(255);//log all errors if telegram change something
$options=[];
$options['bot']=$config['telegram_bot'];//bot username
$options['bot_id']=$config['telegram_bot_id'];//bot tg id from API
$options['api']=$config['telegram_api'];//bot API key
$options['telegam_file_maxsize']=300000;//300Kb
$options['telegam_file_ext_allow']=['gif','png','jpg','webp'];
$options['gate']='https://api.telegram.org/bot'.$options['api'].'/';

$options['plugins']=['Echo','Index','Groups'];
$options['init_plugin']='index';