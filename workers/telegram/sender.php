<?php
error_reporting(255);
set_include_path('.');
include('worker_preload.php');

pcntl_signal(SIGINT,'sig_handler');
pcntl_signal(SIGTERM,'sig_handler');
pcntl_signal(SIGQUIT,'sig_handler');
//SIGHUP for restart?

register_shutdown_function('fatal_handler');

if(!$_SERVER['PWD']){//only for cron
	exit;
}

$pname='telegram_sender';
$pid_file=$root_dir.'/'.$pname.'.pid';
$pid=posix_getpid();
$old_pid=false;
if(file_exists($pid_file)){
	$old_pid=file_get_contents($pid_file);
}
if($old_pid){
	$working=posix_getpgid($old_pid);
	if($working){
		print $pname.' already working, kill with SIGINT, PID: '.$old_pid.PHP_EOL;
		posix_kill($old_pid,SIGINT);
	}
	while($working){
		usleep(100000);//check every 0.1 sec
		$working=posix_getpgid($old_pid);
	}
	print $pname.' old PID '.$old_pid.' was stopped, continued...'.PHP_EOL;
	if(file_exists($pid_file)){
		print '['.date('d.m.Y H:i:s').'] SHUTDOWN old pid file: '.$pid_file.PHP_EOL;
		unlink($pid_file);
	}
}
file_put_contents($pid_file,$pid);
print '['.date('d.m.Y H:i:s').'] STARTUP pid file: '.$pid_file.', pid: '.$pid.PHP_EOL;
$work=true;

$usleep_min_timeout=500000*5;//0.5 sec between new sender checks//*10=5sec
$sender_sleep_timeout=5000;//0.005 sec between every out item (anyway latency for request spend a time ≈0.1s)
$max_global_bottleneck=30;
$max_personal_bottleneck=20;
$max_bottleneck_time=1;//seconds to refresh
while($work){
	$work_start=microtime(true);
	$bottleneck_counter=[];//private chat_type
	$bottleneck_time=[];//private chat_type
	$bottleneck_global_counter=0;
	$bottleneck_global_time=microtime(true);
	$q=$db->sql("SELECT * FROM `telegram_queue` WHERE `status`=0 ORDER BY `id` ASC LIMIT 10");
	while($m=$db->row($q)){
		//calculate global counter and elapsed time
		$bottleneck_global_counter++;
		$bottleneck_elapsed_time=microtime(true) - $bottleneck_global_time;

		if($max_bottleneck_time<$bottleneck_elapsed_time){//refresh
			$bottleneck_global_counter=0;
			$bottleneck_global_time=microtime(true);
		}
		if($max_global_bottleneck<$bottleneck_global_counter){
			print '['.date('d.m.Y H:i:s').'] Bottleneck '.$m['id'].' on global counter '.$bottleneck_global_counter.', elapsed time: '.round($bottleneck_elapsed_time,5).', usleep '.round(1-$bottleneck_elapsed_time,5).'s'.PHP_EOL;

			usleep(max($sender_sleep_timeout,1000000*(1-$bottleneck_elapsed_time)));

			$bottleneck_global_counter=0;
			$bottleneck_global_time=microtime(true);
		}

		//calculate personal counter and elapsed time
		$bottleneck_counter[$m['chat_id']]++;
		if(!isset($bottleneck_time[$m['chat_id']])){
			$bottleneck_time[$m['chat_id']]=microtime(true);
		}
		$bottleneck_counter_elapsed_time=microtime(true)-$bottleneck_time[$m['chat_id']];

		if($max_bottleneck_time<$bottleneck_counter_elapsed_time){//refresh
			$bottleneck_counter[$m['chat_id']]=0;
			$bottleneck_time[$m['chat_id']]=microtime(true);
		}
		if($max_personal_bottleneck<$bottleneck_counter[$m['chat_id']]){
			print '['.date('d.m.Y H:i:s').'] Bottleneck '.$m['id'].' on personal '.$m['chat_id'].' counter '.$bottleneck_counter[$m['chat_id']].', elapsed time: '.round($bottleneck_counter_elapsed_time,5).', usleep '.round(1-$bottleneck_counter_elapsed_time,5).'s'.PHP_EOL;

			usleep(max($sender_sleep_timeout,1000000*(1-$bottleneck_counter_elapsed_time)));

			$bottleneck_counter[$m['chat_id']]=0;
			$bottleneck_time[$m['chat_id']]=microtime(true);
		}

		$data=unserialize($m['data']);
		$inline_keyboard=false;
		if(''!=$m['inline_keyboard']){
			unset($data['reply_markup']);
			$inline_keyboard=unserialize($m['inline_keyboard']);
			$data['reply_markup']['inline_keyboard']=$inline_keyboard;
		}
		if(''!=$m['keyboard']){
			unset($data['reply_markup']);
			$keyboard=unserialize($m['keyboard']);
			$data['reply_markup']['keyboard']=$keyboard;
			$data['reply_markup']['resize_keyboard']=true;
		}
		$success=false;
		$message_id=0;
		$reply_message_id=0;
		$http_status=0;
		$result_json='';
		if(isset($data['reply_to_message_id'])){
			$reply_message_id=$data['reply_to_message_id'];
		}
		if('editMessageText'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendMessage'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('deleteMessage'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
			}
		}
		elseif('sendAnimation'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendVoice'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendDice'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendVideo'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendAudio'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendVideoNote'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendPhoto'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
			else{
				if(isset($result['description'])){
					if('Bad Request: MEDIA_CAPTION_TOO_LONG'==$result['description']){
						unset($data['caption']);
						unset($data['caption_entities']);
						$data['caption']='[error: long caption]';
						list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
						$result=json_decode($result_json,true);
						if(true===$result['ok']){
							$success=true;
							if(isset($result['result'])){
								if(isset($result['result']['message_id'])){
									$message_id=(int)$result['result']['message_id'];
								}
							}
						}
					}
				}
			}
		}
		elseif('answerCallbackQuery'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
			}
		}
		elseif('sendDocument'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendPoll'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
						$pool_id=false;
						if(isset($result['result']['poll'])){
							if(isset($result['result']['poll']['id'])){
								$poll_id=$result['result']['poll']['id'];
								$db->sql("INSERT INTO `polls` (`poll_id`,`out_id`) VALUES ('".$poll_id."','".$m['id']."')");
							}
						}
					}
				}
			}
		}
		elseif('sendSticker'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('editMessageReplyMarkup'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('pinChatMessage'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('sendMediaGroup'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
				if(isset($result['result'])){
					if(isset($result['result']['message_id'])){
						$message_id=(int)$result['result']['message_id'];
					}
				}
			}
		}
		elseif('approveChatJoinRequest'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
			}
		}
		elseif('declineChatJoinRequest'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
			}
		}
		elseif('promoteChatMember'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
			}
		}
		elseif('banChatMember'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
			}
		}
		elseif('unbanChatMember'==$m['method']){
			$data['chat_id']=$m['chat_id'];
			list($http_status,$result_json)=telegram_method_with_status($m['method'],$data);
			$result=json_decode($result_json,true);
			if(true===$result['ok']){
				$success=true;
			}
		}
		else{
			print $m['method'].' method unknown'.PHP_EOL;
			exit;
		}

		if($success){
			/*
			if($m['mailing_queue_id']){
				$db->sql("UPDATE `mailing_queue` SET `counter`=`counter`+1 WHERE `id`='".$m['mailing_queue_id']."'");
			}
			*/
			if($m['pin']){
				telegram_queue($m['chat_id'],'pinChatMessage',['message_id'=>$message_id,'disable_notification'=>($data['disable_notification']?$data['disable_notification']:false)]);
			}
		}
		else{
			/*
			if($m['mailing_queue_id']){
				$db->sql("UPDATE `mailing_queue` SET `errors`=`errors`+1 WHERE `id`='".$m['mailing_queue_id']."'");
			}
			*/
		}

		if(429==$http_status){//bottleneck by status
			print '['.date('d.m.Y H:i:s').'] Bottleneck '.$m['id'].' by status '.$bottleneck_global_counter.', elapsed time: '.round($bottleneck_elapsed_time,5).', usleep '.round(1-$bottleneck_elapsed_time,5).'s'.PHP_EOL;

			usleep(max($sender_sleep_timeout,1000000*(1-$bottleneck_elapsed_time)));

			$bottleneck=false;
			$bottleneck_global_counter=0;
			$bottleneck_global_time=microtime(true);
		}
		else{
			$db->sql("UPDATE `telegram_queue` SET `status`=".($success?1:"2, `http_status`='".$http_status."', `error`='".$db->prepare($result_json)."'")."".($reply_message_id?", `reply_message_id`='".$reply_message_id."'":'')."".($message_id?", `message_id`='".$message_id."'":'')." WHERE `id`='".$m['id']."'");
			//usleep($sender_sleep_timeout);//is not required because telegram api latency ≈0.1s
		}
	}

	if(!file_exists($pid_file)){
		print '['.date('d.m.Y H:i:s').'] SHUTDOWN pid file was deleted: '.$pid_file.PHP_EOL;
		exit;
	}
	pcntl_signal_dispatch();//check any signal come?
	//exit;//temp while small queue
	$work_diff=ceil(1000000*(microtime(true) - $work_start));
	usleep($usleep_min_timeout - min($usleep_min_timeout,$work_diff));

}
exit;