<!DOCTYPE html>
<html class="tinyscroll {theme}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>{title}</title>
	<meta name="description" content="{description}">
	<meta property="og:description" content="{description}">
	<meta name="twitter:description" content="{description}">
	<meta name="viewport" content="width=device-width">

	<link rel="icon" href="/favicon.ico?v2" />
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="GetPass"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="/mstile-310x310.png" />

	{head_addon}
	{script_preset}
	<script>var ltmp={ltmp_json};</script>

	<link rel="stylesheet" href="/app.css?{css_change_time}">

	<script type="text/javascript" src="/cash.min.js"></script>
	<script type="text/javascript" src="/app.js?{script_change_time}"></script>
</head>
<body>

<div class="menu-toggle tinyscroll hidden">
	<div class="container mx-auto pt-0 min-w-auto">
		<div class="header">
			<div class="logo-wrapper">
				<a href="{event_url}" class="logo"><img src="{event_logo_url}" alt="{event_title}" class="{event_logo_class}">{event_logo_addon}</a>
			</div>

			<div class="menu-button-wrapper{menu_button_wrapper_addon}">
				<button class="toggle-menu-action" title="Toggle menu">
					<svg class="fill-current h-4 w-4" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
				</button>
			</div>
		</div>
		<div class="menu-toggle-wrapper">
			<ul>
				{menu}
			</ul>
			<div class="user-badge">
				{theme_mode_badge}
				{attention_badge}
				{notify_badge}
				{user_badge}
			</div>
		</div>
	</div>
</div>

<div class="header-wrapper">
	<div class="header-container container mx-auto h-20">
		<div class="header">
			<div class="logo-wrapper">
				<a href="{event_url}" class="logo"><img src="{event_logo_url}" alt="{event_title}" class="{event_logo_class}">{event_logo_addon}</a>
			</div>

			<div class="menu-button-wrapper{menu_button_wrapper_addon}">
				<button class="toggle-menu-action">
					<svg class="fill-current h-4 w-4" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path></svg>
				</button>
			</div>

			<div class="menu-wrapper">
				<div class="inline-flex grow">
					<ul>
						{menu}
					</ul>
				</div>
				<div class="inline-flex">
					<div class="user-badge">
						<!--<span class="loading"><img src="/loader.svg" class="rotate"></span>-->
						{theme_mode_badge}
						{attention_badge}
						{notify_badge}
						{user_badge}
					</div>
				</div>
			</div>


		</div>
	</div>
</div>

{event_cover}

<div class="main-container{main_container_addon}">
	<div class="content">
		{content}
	</div>
</div>

<!--
<div class="container min-w-max mx-auto pt-10">
	<div class="text-sm p-4 text-gray-500">
		<div class="theme-changer">Change theme to <a class="theme-action cursor-pointer" data-theme="light">Light</a> or <a class="theme-action cursor-pointer" data-theme="dark">Dark</a></div>
		<div class="mt-2">2022&ndash;{year} &copy; <a href="{platform_url}" target="_blank">GetPass.is</a> develop by <a href="https://envelop.is/" target="_blank">Envelop.is</a></div>
	</div>
</div>
-->

<div class="footer-wrapper">
	<div class="footer-container container mx-auto h-20">
		<div class="footer">
			<div class="logo-wrapper">
				<a href="{event_url}" class="logo"><img src="{event_logo_url}" alt="{event_title}" class="{event_logo_class}">{event_logo_addon}</a>
			</div>

			<div class="footer-links">
				Connect with us <a href="mailto:info@envelop.is">info@envelop.is</a><a href="https://gitlab.com/envelop/tickets/getpass" target="_blank">Gitlab</a>
			</div>
		</div>
	</div>
</div>

<div class="debug"></div>

</body>
</html>