<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="x-apple-disable-message-reformatting" />
<title></title>
<meta name="viewport" content="width=device-width">
<link href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;700&display=swap" rel="stylesheet">
<style type="text/css">
html{
line-height:1.5;
-webkit-text-size-adjust:100%;
-moz-tab-size:4;
-o-tab-size:4;
tab-size:4;
font-family:ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
}
body{
margin:0;
margin-top:1em;
margin-bottom:1em;
margin-left:1em;
margin-right:1em;
line-height:inherit;
background:#ffffff;
}
hr{
border:0;
height:2px;
background:#dadada;
margin-top:1em;
margin-bottom:1em;
}
.content{
font-size:1em;
color:#000000;
max-width:600px;
margin:0 auto;
}
.content img{
/*width:100%;*/
width:auto;/*100% broke emoji gmail replacement*/
max-width:600px;
}
.content figure{
max-width:600px;
overflow:auto;
margin:0 auto;
}
a{
color:#3b82f6;
text-decoration:underline;
}
.footer{
text-align:center;
font-size:0.9em;
color:#696969;
}
table{
border:1px double #b3b3b3;
border-collapse:collapse;
border-spacing:0;
height:100%;
width:100%;
}
table th{
text-align:left;
background:#0000000d;
font-weight:bold;
}
table>td{
border:1px solid #bfbfbf;
min-width:15px;
padding:5px;
}
table>th{
border:1px solid #bfbfbf;
min-width:15px;
padding:5px;
}
table>tr>td{
padding-left:15px;
padding-right:15px;
padding-top:10px;
padding-bottom:10px;
text-align:left;
}
table.clear{
border:none;
border-collapse:collapse;
border-spacing:0;
height:100%;
width:100%;
}
table.clear>th{
text-align:left;
background:none;
font-weight:bold;
}
table.clear>td{
border:none;
min-width:20px;
padding:0;
}
table.clear>th{
border:none;
min-width:20px;
padding:0;
}
table.clear>tr>td{
padding:0;
text-align:left;
}
blockquote{
border-left:5px solid #ccc;
font-style:italic;
margin-left:0;
margin-right:0;
overflow:hidden;
padding-left:1.5em;
padding-right:1.5em;
}
.notify-item{
display:block;
border:0;
border-bottom:1px solid #e5e7eb;
padding:1em 0;
}
table.clear tr td.content-cover{
	width:25%;
}
table.clear tr td.content-cover img{
	max-width:200px;
	border:2px solid #e5e7eb;
}
table.clear tr td.content-title{
	font-size:1.4em;
	font-weight:bold;
	padding-top:15px;
	padding-bottom:10px;
}
table.clear tr td.content-description{
	font-size:1em;
	font-weight:normal;
	vertical-align:top;
	text-align:left;
	padding-left:15px;
	width:100%;
}
table.clear tr td.content-description img{
	max-width:385;/*600-200-15*/
}
table.clear tr td.content-body figure{
	overflow-x:auto;
}
</style>
</head>
<body>
{preheader}
<center>
<a href="{event_url}"><img alt="{event_title}" src="{event_url}/getpass-new-logo-teal-black.png"></a>
</center>
<div class="content">
<hr>
{body}
<hr>
<div class="footer">
2022&ndash;{year} &copy; <a href="{platform_url}">GetPass.is</a> develop by <a href="https://envelop.is/">Envelop.is</a>
</div>
</div>
</body>
</html>