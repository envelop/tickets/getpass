function check_minimal_offset(el){
	//check minimal offset for datetime-future inputs and show tooltip
	if(typeof $(el).data('minimal-offset')!='undefined'){
		let minimal_offset=parseInt($(el).data('minimal-offset'));
		let minimal_offset_range=minimal_offset;
		if(minimal_offset_range>60){//1 minute
			minimal_offset_range-=60;//distinct 60 seconds for safety select
		}
		if(minimal_offset_range>60){//2 minute
			minimal_offset_range-=60;//distinct 60 seconds for safety select
		}
		let datetime_unixtime=Date.parse($(el).val())/1000 | 0;
		let now_unixtime=Date.now()/1000 | 0;
		if(datetime_unixtime<(now_unixtime+minimal_offset_range)){
			//$(el).val(get_datetime_now(minimal_offset));
			$(el).parent().find('.additional-tooltip').addClass('show');
			return false;
		}
		else{
			$(el).parent().find('.additional-tooltip').removeClass('show');
			return true;
		}
	}
	return true;
}

function check_manage_form(form){
	let input_unixtime=$(form).find('input[name="unixtime"]');
	if(typeof input_unixtime !== 'undefined'){
		$(input_unixtime).val(Date.parse($(form).find('input[name=datetime]').val())/1000 |0);
	}

	let error_checks=false;
	$(form).find('.datetime-future').each(function(i,el){
		if(typeof $(el).data('minimal-offset')!='undefined'){
			let check_minimal_offset_status=check_minimal_offset(this);
			if(false===check_minimal_offset_status){
				error_checks=true;
			}
		}
	});

	if(error_checks){
		return false;
	}
	return true;
}

function copy_to_clipboard(el,start_callback,end_callback){
	start_callback=typeof start_callback!=='undefined'?start_callback:false;
	end_callback=typeof end_callback!=='undefined'?end_callback:false;
	if(false!==start_callback){
		start_callback(el);
	}
	let text=$(el).data('text');
	if(typeof text === 'undefined'){
		text=$(el).text();
	}

	$('body').append('<input class="text-copy">');
	console.log(text);
	$('.text-copy').val(text);
	$('.text-copy')[0].select();
	$('.text-copy')[0].setSelectionRange(0,99999);
	document.execCommand('copy');
	$('.text-copy').remove();
	if(false!==end_callback){
		setTimeout(function(){
			end_callback(el);
		},1500);//1.5 sec
	}
}

function bind_time_wrapper(){
	$('.time').each(function(i,el){
		console.log($(el).data('old-value'),typeof $(el).data('old-value'));
		if(typeof $(el).data('old-value')==='undefined'){
			$(el).data('old-value',$(el).text());
		}
		let time=$(el).data('time');
		if(typeof time !== 'undefined'){
			let date=new Date(time*1000);
			//get local time format to variable
			let time_str=date.toLocaleString(undefined,{hour12:false});
			$(el).text(time_str);
		}

		$(el).on('click',function(){
			if($(this).text()!=$(this).data('old-value')){
				$(this).text($(this).data('old-value'));
			}
			else{
				let time=$(this).data('time');
				if(typeof time !== 'undefined'){
					let date=new Date(time*1000);
					let time_str=date.toLocaleString(undefined,{hour12:false});
					$(this).text(time_str);
				}
			}
		});
	});
}

function ajax_action(el){
	if(!$(el).hasClass('disabled')){
		$(el).addClass('disabled');
		let link=$(el).prop('href');
		if($(el).data('href').length>0){
			link=$(el).data('href');
		}
		let xhr = new XMLHttpRequest();
		xhr.timeout=5000;//5 seconds
		xhr.overrideMimeType('text/plain');
		xhr.open('GET',link);
		xhr.setRequestHeader('accept','application/json, text/plain, */*');
		xhr.setRequestHeader('content-type','application/json');
		xhr.ontimeout = function() {
			console.log('ajax_action timeout',link);
			$(el).addClass('error');
			$(el).closest('.action-wrapper').append('<p class="text-red-500 text-sm">Server not respond. Please try again later...</p>');
		};
		xhr.onreadystatechange = function() {
			if(4==xhr.readyState && 200==xhr.status){
				let response=xhr.response;
				console.log('ajax_action response',response);
				$(el).removeClass('disabled');
				try{
					response=JSON.parse(response);
					if(typeof response.status !== 'undefined'){
						if(response.status){
							if(typeof response.message !== 'undefined'){
								$(el).closest('.action-wrapper').html('<span class="text-orange-500">'+response.message+'</span>');
							}
						}
					}
				}
				catch(e){
					$(el).closest('.action-wrapper').append('<p class="text-red-500 text-sm">Server not respond. Please try again later...</p>');
				}
			}
			if(4==xhr.readyState && 200!=xhr.status){
				console.log('ajax_action status',xhr.status);
				$(el).removeClass('disabled');
				let error=xhr.status;
				if(404==xhr.status){
					error='Object not found.';
				}
				if(403==xhr.status){
					error='CSRF token is invalid or expired.';
				}
				$(el).closest('.action-wrapper').append('<p class="text-red-500 text-sm">Error: '+error+'</p>');
			}
		};
		xhr.send();
	}
}
var update_user_badge_timer=0;
function update_user_badge(lazy,empty){
	lazy=typeof lazy!=='undefined'?lazy:false;
	empty=typeof empty!=='undefined'?empty:false;
	if(!$('.user-badge').hasClass('waiting')){
		if(0==$('.check-in-autoupdate').length){//if not on check-in page
			$('.user-badge').addClass('waiting');//add waiting class that show loading animation
		}

		let xhr = new XMLHttpRequest();
		xhr.timeout=5000;//5 seconds
		xhr.overrideMimeType('text/plain');
		let event_addon=(typeof event_id!=='undefined'?event_id+'/':'');
		if(empty){
			event_addon='';
		}
		if(typeof manage_event!== 'undefined'){//if manage event page
			event_addon='';
		}
		xhr.open('GET','/ajax/user-badge/'+event_addon+(lazy?'lazy/':''));
		xhr.setRequestHeader('accept','text/html, */*');
		xhr.setRequestHeader('content-type','text/html');
		xhr.ontimeout = function() {
			console.log('update_user_badge timeout',timeout);
			$('.user-badge').removeClass('waiting');
		};
		xhr.onreadystatechange = function() {
			if(4==xhr.readyState && 200==xhr.status){
				console.log('update_user_badge response',xhr.response);
				$('.user-badge').html(xhr.response);
				use_jdenticon();
				update_theme_changer();
				$('.user-badge').removeClass('waiting');
				if(typeof event_id!=='undefined'){
					if($('.user-badge .check-in-status').length>0){
						//clearTimeout(update_user_badge_timer);
						//need update anyway for new notifies indicator
						update_user_badge_timer=setTimeout(function(){
							update_user_badge(lazy);
						},15000);//15 sec
					}
					else{
						update_user_badge_timer=setTimeout(function(){
							update_user_badge(lazy);
						},5000);//5 sec
					}
				}
			}
			if(4==xhr.readyState && 200!=xhr.status){
				console.log('update_user_badge status',xhr.status);
				$('.user-badge').removeClass('waiting');
			}
		};
		xhr.send();
	}
}
function load_content(el){
	if($(el).hasClass('waiting')){
		$(el).removeClass('waiting');
		let session_id=false;
		if($(el).data('session')){
			session_id=$(el).data('session');
		}
		let link=window.location.pathname +'load_content/'+(false!==session_id?session_id:'');
		console.log('load_content',el,link);

		let xhr = new XMLHttpRequest();
		xhr.timeout=5000;//5 seconds
		xhr.overrideMimeType('text/plain');
		xhr.open('GET',link);
		xhr.setRequestHeader('accept','application/json, text/plain, */*');
		xhr.setRequestHeader('content-type','application/json');
		xhr.ontimeout = function() {
			console.log('load_content timeout',link);
			$(el).addClass('error');
			$(el).html('<p>Server not respond. Please try again later...</p>');
		};
		xhr.onreadystatechange = function() {
			if(4==xhr.readyState && 200==xhr.status){
				let response=xhr.response;
				console.log('load_content response',response);
				$(el).addClass('success');
				$(el).html(response);
				oembed_all();
			}
			if(4==xhr.readyState && 200!=xhr.status){
				console.log('load_content status',xhr.status);
				$(el).addClass('error');
				$(el).html('<p>Server not respond. Please try again later...</p>');
			}
		};
		xhr.send();
	}
}

function get_datetime_now(offset){
	offset=typeof offset!=='undefined'?offset:0;//in seconds
	let d=new Date();
	if(0!=offset){
		d=new Date(d.getTime() + offset * 1000);
	}
	let year=d.getFullYear();
	let month=d.getMonth()+1;
	month=("0" + month).slice(-2);
	let day=d.getDate();
	day=("0" + day).slice(-2);
	let hours=d.getHours();
	hours=("0" + hours).slice(-2);
	let minutes=d.getMinutes();
	minutes=("0" + minutes).slice(-2);
	return ""+year+"-"+month+"-"+day+"T"+hours+":"+minutes;
}

function countdown(el) {
	let now = new Date();
	let countdown_time=$(el).data('time');
	let eventDate = new Date(1000*countdown_time);

	let currentTime = now.getTime();
	let eventTime = eventDate.getTime();

	let remTime = eventTime - currentTime;

	let s = Math.floor(remTime / 1000);
	let m = Math.floor(s / 60);
	let h = Math.floor(m / 60);
	let d = Math.floor(h / 24);

	h %= 24;
	m %= 60;
	s %= 60;

	if(0>d){
		d=0;
	}
	if(0>h){
		h=0;
	}
	if(0>m){
		m=0;
	}
	if(0>s){
		s=0;
	}

	h = (h < 10) ? "0" + h : h;
	m = (m < 10) ? "0" + m : m;
	s = (s < 10) ? "0" + s : s;

	$(el).find('.days').text(d);
	$(el).find('.hours').text(h);
	$(el).find('.minutes').text(m);
	$(el).find('.seconds').text(s);

	if('0'==d){
		$(el).find('.days').parent().css('display','none');
	}
	if('00'==h){
		$(el).find('.hours').parent().css('display','none');
	}

	if(remTime<=0){
		$(el).addClass('hidden');
		let content_el=$(el).closest('.content-wrapper');
		if(typeof content_el !== 'undefined'){
			content_el.css('<p>Loading...</p>');
			content_el.addClass('waiting');
			load_content(content_el);
		}
		else{
			if($(el).hasClass('global')){
				window.location.reload();//reload event page
			}
		}
	}
	else{
		setTimeout(countdown,1000,el);
	}
}

let wysiwyg_enabled=false;
function wysiwyg_init(callback){
	if(!wysiwyg_enabled){
		wysiwyg_enabled=true;

		//inject /js/ckeditor/translations/ru.js in head
		let translation = document.createElement('script');
		translation.src = '/js/ckeditor/translations/ru.js';
		translation.type = 'text/javascript';
		document.getElementsByTagName('head')[0].appendChild(translation);

		//inject /js/ckeditor/styles.css in head
		let style = document.createElement('link');
		style.href = '/js/ckeditor/styles.css';
		style.rel = 'stylesheet';
		style.type = 'text/css';
		document.getElementsByTagName('head')[0].appendChild(style);

		//inject /js/ckeditor/ckeditor.js in head
		let script = document.createElement('script');
		script.src = '/js/ckeditor/ckeditor.js?'+(new Date().getTime());
		script.type = 'text/javascript';
		document.getElementsByTagName('head')[0].appendChild(script);

		//check loaded
		let check = setInterval(function() {
			if (typeof ClassicEditor !== 'undefined') {
				clearInterval(check);
				callback();
			}
		},500);
	}
	else{
		callback();
	}
}
class UploadAdapter{
	constructor(loader){
		// The file loader instance to use during the upload.
		this.loader = loader;
	}

	// Starts the upload process.
	upload() {
		return this.loader.file
			.then( file => new Promise( ( resolve, reject ) => {
				this._initRequest();
				this._initListeners( resolve, reject, file );
				this._sendRequest( file );
			} ) );
	}

	// Aborts the upload process.
	abort() {
		if ( this.xhr ) {
			this.xhr.abort();
		}
	}

	// Initializes the XMLHttpRequest object using the URL passed to the constructor.
	_initRequest() {
		const xhr = this.xhr = new XMLHttpRequest();

		// Note that your request may look different. It is up to you and your editor
		// integration to choose the right communication channel. This example uses
		// a POST request with JSON as a data structure but your configuration
		// could be different.
		let link_path_arr=window.location.pathname.split('/');
		let new_link_path_arr=['',link_path_arr[1],link_path_arr[2],link_path_arr[3],'upload_content',''];
		if('admin'==link_path_arr[1]){
			new_link_path_arr=['',link_path_arr[1],link_path_arr[2],'upload_content',''];
		}
		let link=new_link_path_arr.join('/');

		xhr.open('POST',link,true);
		xhr.responseType='json';
	}

	// Initializes XMLHttpRequest listeners.
	_initListeners( resolve, reject, file ) {
		const xhr = this.xhr;
		const loader = this.loader;
		const genericErrorText = `Couldn't upload file: ${ file.name }.`;

		xhr.addEventListener( 'error', () => reject( genericErrorText ) );
		xhr.addEventListener( 'abort', () => reject() );
		xhr.addEventListener( 'load', () => {
			const response = xhr.response;

			// This example assumes the XHR server's "response" object will come with
			// an "error" which has its own "message" that can be passed to reject()
			// in the upload promise.
			//
			// Your integration may handle upload errors in a different way so make sure
			// it is done properly. The reject() function must be called when the upload fails.
			if ( !response || response.error ) {
				return reject( response && response.error ? response.error.message : genericErrorText );
			}

			// If the upload is successful, resolve the upload promise with an object containing
			// at least the "default" URL, pointing to the image on the server.
			// This URL will be used to display the image in the content. Learn more in the
			// UploadAdapter#upload documentation.
			resolve( {
				default: response.url
			} );
		} );

		// Upload progress when it is supported. The file loader has the #uploadTotal and #uploaded
		// properties which are used e.g. to display the upload progress bar in the editor
		// user interface.
		if ( xhr.upload ) {
			xhr.upload.addEventListener( 'progress', evt => {
				if ( evt.lengthComputable ) {
					loader.uploadTotal = evt.total;
					loader.uploaded = evt.loaded;
				}
			} );
		}
	}

	// Prepares the data and sends the request.
	_sendRequest( file ) {
		// Prepare the form data.
		const data = new FormData();

		data.append( 'upload', file );

		// Important note: This is the right place to implement security mechanisms
		// like authentication and CSRF protection. For instance, you can use
		// XMLHttpRequest.setRequestHeader() to set the request headers containing
		// the CSRF token generated earlier by your application.

		// Send the request.
		this.xhr.send( data );
	}
}
function upload_adapter_plugin(editor){
	editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
		return new UploadAdapter(loader);
	};
}

jdenticon_active=false;
function activate_jdenticon(){
	//inject /js/jdenticon.min.js in head
	let script = document.createElement('script');
	script.src = '/js/jdenticon.min.js';
	script.type = 'text/javascript';
	document.getElementsByTagName('head')[0].appendChild(script);

	jdenticon_active=true;
}
function wait_jdenticon(callback){
	if(typeof jdenticon === 'function'){
		callback();
	}
	else{
		setTimeout(function(){
			wait_jdenticon(callback);
		},100);
	}
}
function use_jdenticon(){
	if(false===jdenticon_active){
		activate_jdenticon();
	}
	wait_jdenticon(function(){
		if(typeof jdenticon === 'function'){
			jdenticon();
		}
	});
}

oembed_active=false;
function activate_oembed(){
	//inject /js/oembed.js.js in head
	let script = document.createElement('script');
	script.src = '/js/oembed.js?'+(new Date()).getTime();
	script.type = 'text/javascript';
	document.getElementsByTagName('head')[0].appendChild(script);

	//inject /css/oembed.css in head
	let style = document.createElement('link');
	style.href = '/css/oembed.css';
	style.rel = 'stylesheet';
	style.type = 'text/css';
	document.getElementsByTagName('head')[0].appendChild(style);
	oembed_active=true;
}
function wait_oembed(callback){
	if(typeof Oembed === 'undefined'){
		setTimeout(function(){
			wait_oembed(callback);
		},100);
	}
	else{
		callback();
	}
}
function oembed_all(){
	if($('oembed').length>0){
		if(false===oembed_active){
			activate_oembed();
		}
		wait_oembed(function(){
			$('oembed').each(function(i,el){
				oembed_parent_el=$(el).parent();
				//console.log(el);
				let new_oembed=new Oembed(el,{url:$(el).attr('url')});
				console.log(new_oembed);

				//if content wrapper contains autoplay data flag, add autoplay to iframe (works on event landging page)
				setTimeout(function(){
					oembed_new_el=$(oembed_parent_el).find('iframe');
					if('yes'==$(oembed_new_el).closest('.content-wrapper').data('autoplay')){
						let query_started=$(oembed_new_el).attr('src').indexOf('?')>-1;
						let new_src=$(oembed_new_el).attr('src')+(query_started?'&':'?')+'autoplay=1';
						$(oembed_new_el).attr('src',new_src);
					}
				},1500);
			});
		});
	}
}

let check_in_timer=false;
function check_in_autoupdate(){
	check_in_timer=setTimeout(function(){
		let timer_val=parseInt($('.check-in-autoupdate').find('.timer').html());
		timer_val--;
		if(timer_val<0){
			clearInterval(check_in_timer);
			let link=window.location.pathname+'ajax/status/';
			let xhr = new XMLHttpRequest();
			xhr.timeout=5000;//5 seconds
			xhr.overrideMimeType('application/json');
			xhr.open('GET',link);
			xhr.setRequestHeader('accept','application/json, text/plain, */*');
			xhr.setRequestHeader('content-type','application/json');
			xhr.ontimeout = function() {
				console.log('check-in-autoupdate timeout',link);
				window.location.reload();//reload page maybe server is down?
			};
			xhr.onreadystatechange = function() {
				if(4==xhr.readyState && 200==xhr.status){
					let response=xhr.response;
					console.log('check-in-autoupdate response',link,response);
					let json=JSON.parse(response);
					if(typeof json['status'] !== 'undefined'){
						if(0==json['status']){//not participant
							if(0!=json['on_update']){
								let text='Please wait. We are looking NFT-ticket on your address'+(json['on_update']>1?'es':'')+'';
								$('.check-in-autoupdate').find('.text').html(text);
								$('.check-in-autoupdate').find('.timer').html(5);
								check_in_autoupdate();
							}
							else{
								if(0!=json['wait_update']){
									let text='Too frequent requests to the oracle, we must wait';
									$('.check-in-autoupdate').find('.text').html(text);
									$('.check-in-autoupdate').find('.timer').html(30);
									check_in_autoupdate();
								}
							}
						}
						else{
							let text='You are participant of this event! Reloading page...';
							$('.check-in-autoupdate').find('.text').html(text);
							$('.check-in-autoupdate').find('.timer-text').html('');
							window.location.reload();
						}
					}
					else{
						let text='Server error. Please try again later or contact support.';
						$('.check-in-autoupdate').find('.text').html(text);
						$('.check-in-autoupdate').find('.timer-text').html('');
					}
				}
				if(4==xhr.readyState && 200!=xhr.status){
					console.log('check-in-autoupdate status',xhr.status);
					let text='Server error. Please try again later or contact support.';
					$('.check-in-autoupdate').find('.text').html(text);
					$('.check-in-autoupdate').find('.timer-text').html('');
				}
			};
			xhr.send();
		}
		else{
			$('.check-in-autoupdate').find('.timer').html(timer_val);
			check_in_autoupdate();
		}
	},1000);
}

function set_cookie(key,value,days,path){
	console.log('set_cookie',key,value,days,path);
	path=(typeof path !== 'undefined')?path:'/';
	const d=new Date();
	d.setTime(d.getTime()+(days*24*60*60*1000));
	let expires='expires='+d.toUTCString();
	document.cookie=key+'='+value+';'+expires+';path='+path;
}

function get_cookie(key){
	console.log('get_cookie',key);
	let name=key+"=";
	let ca=document.cookie.split(';');
	for(let i=0;i<ca.length;i++) {
		let c=ca[i];
		while(c.charAt(0)==' '){
			c=c.substring(1);
		}
		if(c.indexOf(name)==0){
			let result=c.substring(name.length,c.length);
			console.log(result);
			return result;
		}
	}
	console.log(false);
	return false;
}

function link_platform_notice_dismiss(el){
	let cookie_name='link_platform_notice_dismiss';
	let cookie_path='/';
	if(typeof event_id !== 'undefined'){
		cookie_name+='_'+event_id;
		//parse window.location.pathname to get 2 first parts
		cookie_path=window.location.pathname.split('/').slice(0,3).join('/')+'/';
	}
	console.log('link_platform_notice_dismiss',cookie_name,cookie_path);
	set_cookie(cookie_name,new Date().getTime(),365,cookie_path);
	$(el).closest('.attention-box').remove();
	$(el).closest('.modal-bg').remove();
}

function modal_show(el,name){
	el=(typeof el !== 'undefined')?el:false;
	name=(typeof name !== 'undefined')?name:false;
	if(el){
		$(el).closest('.modal-bg').removeClass('hide');
		$(el).closest('.modal-wrapper').removeClass('hide');
	}
	if(name){
		$('.modal-bg[data-modal="'+name+'"]').removeClass('hide');
		$('.modal-wrapper[data-modal="'+name+'"]').removeClass('hide');
	}
}

function modal_hide(el){
	$(el).closest('.modal-bg').addClass('hide');
	$(el).closest('.modal-wrapper').addClass('hide');
}

let themes_arr=['light','dark'];
/*
let themes_svg_arr={
	'light':'<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-moon-fill" viewBox="0 0 16 16"><path d="M6 .278a.768.768 0 0 1 .08.858 7.208 7.208 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277.527 0 1.04-.055 1.533-.16a.787.787 0 0 1 .81.316.733.733 0 0 1-.031.893A8.349 8.349 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.752.752 0 0 1 6 .278z"/></svg>',
	'dark':'<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sun-fill" viewBox="0 0 16 16"><path d="M8 12a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z"/></svg>',
};
*/
let default_theme='dark';
let current_theme=default_theme;
function apply_theme(theme){
	let cookie_name='theme';
	let cookie_path='/';
	if(typeof event_id !== 'undefined'){
		cookie_name+='_'+event_id;
		//parse window.location.pathname to get 2 first parts
		cookie_path=window.location.pathname.split('/').slice(0,3).join('/')+'/';
	}
	console.log('apply_theme',theme,cookie_name,cookie_path);
	if(-1==themes_arr.indexOf(theme)){
		theme=default_theme;
	}
	set_cookie(cookie_name,theme,365,cookie_path);
	$('html').removeClass('dark');
	$('html').removeClass('light');
	$('html').addClass(theme);
	current_theme=theme;
	//$('.theme-toggle').html(themes_svg_arr[theme]+'<span class="explainer">Toggle theme</span>');
	$('.theme-toggle').html(ltmp.icons[theme+'-change-theme']+'<span class="explainer">Toggle theme</span>');
	//don't change logo on event pages
	if(typeof event_id === 'undefined'){
		if('light'==theme){
			$('.logo img').attr('src','/getpass-new-logo-light-purple.svg');
		}
		if('dark'==theme){
			$('.logo img').attr('src','/getpass-new-logo-dark-purple.svg');
		}
	}
	else{
		if(!$('.logo img').hasClass('custom-logo')){//not custom logo
			if('light'==theme){
				$('.logo img').attr('src','/getpass-new-logo-light-purple.svg');
			}
			if('dark'==theme){
				$('.logo img').attr('src','/getpass-new-logo-dark-purple.svg');
			}
		}
	}
}

let drag_item=null;
let drag_item_num=0;
function check_sortable_lists(){
	$('.sort-wrapper').each(function(i,el){
		let name=$(el).attr('data-name');
		let value_el=$(el).find('input[name="'+name+'"]');
		if(value_el.length>0){
			let sort_value=[];
			$(el).find('ul li').each(function(i,el){
				$(el).attr('data-num',i);
				sort_value.push($(el).attr('data-value'));
			});
			value_el.val(sort_value.join(','));
		}
	});
}

function sortable_lists(){
	$('.sort-wrapper').each(function(i,el){
		if(!$(el).hasClass('active')){
			$(el).addClass('active');
			let name=$(el).attr('data-name');
			$(el).append('<input type="hidden" name="'+encodeURIComponent(name)+'" value="">');
			$(el).find('ul li').each(function(i,el){
				$(el)[0].draggable=true;
				$(el).on('dragstart',function(e){
					let parent_list=$(this).closest('ul');
					drag_item=this;
					drag_item_num=$(this).attr('data-num');
					$(parent_list).find('li').each(function(i,sub_el){
						if(drag_item != sub_el){
							$(sub_el).addClass('hint');
						}
					});
				});

				$(el).on('dragenter',function(e){
					if(drag_item != this){
						$(this).addClass('active');
					}
				});

				$(el).on('dragleave',function(e){
					$(this).removeClass('active');
				});

				$(el).on('dragend',function(e){
					let parent_list=$(this).closest('ul');
					$(parent_list).find('li').each(function(i,sub_el){
						$(sub_el).removeClass('hint');
						$(sub_el).removeClass('active');
					});
					check_sortable_lists();
				});

				$(el).on('dragover',function(e){
					e.preventDefault();
				});

				$(el).on('drop',function(e){
					e.preventDefault();
					if(drag_item != this){
						//check if item was dragged up
						if($(this).attr('data-num')>drag_item_num){
							console.log('drag up',drag_item_num,$(this).attr('data-num'));
							$(this).after(drag_item);
						}
						else{
							//or down
							console.log('drag down',drag_item_num,$(this).attr('data-num'));
							$(this).before(drag_item);
						}
					}
					drag_item=null;
					drag_item_num=0;
					check_sortable_lists();
				});

				//add support to touch events
				$(el).on('touchstart',function(e){
					let parent_list=$(this).closest('ul');
					drag_item=this;
					drag_item_num=$(this).attr('data-num');
					$(parent_list).find('li').each(function(i,sub_el){
						if(drag_item != sub_el){
							$(sub_el).addClass('hint');
						}
					});
				});

				$(el).on('touchmove',function(e){
					e.preventDefault();
					var changedTouch = e.changedTouches[0];
					var el = document.elementFromPoint(changedTouch.clientX, changedTouch.clientY);
					if(drag_item != el){
						$(el).addClass('active');
					}

					let parent_list=$(el).closest('ul');
					$(parent_list).find('li').each(function(i,sub_el){
						if(sub_el !== el){
							$(sub_el).removeClass('active');
						}
					});
				});

				$(el).on('touchend',function(e){
					e.preventDefault();
					e.stopPropagation();
					var changedTouch = e.changedTouches[0];
					var el = document.elementFromPoint(changedTouch.clientX, changedTouch.clientY);
					let parent_list=$(el).closest('ul');
					$(parent_list).find('li').each(function(i,sub_el){
						$(sub_el).removeClass('hint');
						$(sub_el).removeClass('active');
					});

					if(drag_item != el){
						//check if item was dragged up
						if($(el).attr('data-num')>drag_item_num){
							console.log('drag up',drag_item_num,$(el).attr('data-num'));
							$(el).after(drag_item);
						}
						else{
							//or down
							console.log('drag down',drag_item_num,$(el).attr('data-num'));
							$(el).before(drag_item);
						}
					}
					drag_item=null;
					drag_item_num=0;
					check_sortable_lists();
				});

			});
		}
	});
	check_sortable_lists();
}

let wysiwyg_toolbar=[
	'undo',
	'redo',
	'|',
	'paragraph',
	'heading1',
	'heading2',
	'heading3',
	'|',
	'bold',
	'italic',
	'strikethrough',
	'underline',
	'blockQuote',
	'link',
	'alignment',
	'|',
	'horizontalLine',
	'bulletedList',
	'numberedList',
	'|',
	'imageInsert',
	'insertTable',
	'mediaEmbed',
	'htmlEmbed',
	'|',
	'removeFormat',
	'sourceEditing'
];

let theme_changer=true;
function update_theme_changer(){
	if(typeof event_id !== 'undefined'){
		theme_changer=false;
		if(typeof event_theme_changer !== 'undefined'){
			theme_changer=event_theme_changer;
		}
		if(typeof event_default_theme !== 'undefined'){
			let cookie_name='theme';
			cookie_name+='_'+event_id;
			let find_cookie=get_cookie(cookie_name);
			if(false===find_cookie){
				current_theme=event_default_theme;
			}
			else{
				current_theme=find_cookie;
			}
		}
	}
	else{
		let cookie_name='theme';
		let find_cookie=get_cookie(cookie_name);
		if(false===find_cookie){
			current_theme=default_theme;
		}
		else{
			current_theme=find_cookie;
		}
	}
	apply_theme(current_theme);
	if(theme_changer){
		//hide old theme changer https://gitlab.com/envelop/tickets/getpass/-/issues/21
		//$('.theme-changer').css('display','block');
		$('.theme-toggle').css('display','inline-block');
		$('.menu-toggle .theme-toggle').css('display','block');
	}
	else{
		$('.menu-toggle .theme-toggle').css('display','none');
		//if no theme changer, but default theme is set, apply it
		if(typeof event_default_theme !== 'undefined'){
			apply_theme(event_default_theme);
		}
	}
	$('.theme-action').off('click');
	$('.theme-action').on('click',function(){
		let theme=$(this).data('theme');
		apply_theme(theme);
	});
	$('.theme-toggle').off('click');
	$('.theme-toggle').on('click',function(){
		let theme=current_theme;
		//find theme in themes_arr and get next theme
		let theme_index=themes_arr.indexOf(theme);
		if(theme_index<0){
			theme_index=0;
		}
		theme_index++;
		if(theme_index>=themes_arr.length){
			theme_index=0;
		}
		theme=themes_arr[theme_index];
		apply_theme(theme);
	});
}

$(function(){
	if(typeof Oembed === 'undefined'){
		oembed_active=false;
	}
	else{
		oembed_active=true;
	}
	if(typeof wysiwyg_toolbar_remove !== 'undefined'){
		wysiwyg_toolbar=wysiwyg_toolbar.filter(function(item){
			return wysiwyg_toolbar_remove.indexOf(item) < 0;
		});
	}
	oembed_all();
	$('.activate-wysiwyg-action').on('click', function(){
		let btn=$(this);
		wysiwyg_init(function(){
			let textarea=btn.parent().find('textarea')[0];
			console.log(textarea);
			if(typeof(textarea)!='undefined'){
				ClassicEditor
					.create(textarea,{
						extraPlugins:[upload_adapter_plugin],
						allowedContent:true,
						extraAllowedContent:'*[*]',
						link: {
							decorators: {
								openInNewTab: {
									mode: 'manual',
									label: 'Open in a new tab',
									attributes: {
										target: '_blank',
										rel: 'noopener noreferrer'
									}
								}
							}
						},
						toolbar: wysiwyg_toolbar,
						htmlSupport: {//allow selected html in wysiwyg
							allow: [
								{
									name: 'table',
									classes: ['clear'],
								}
							]
						}
					})
					.then(editor=>{
						console.log( editor );
						btn.addClass('hidden');
					})
					.catch(error=>{
						console.error( error );
					});
			}
		});
	});
	$('.countdown').each(function(i,el){
		countdown(el);
	});
	$('.content-wrapper').each(function(i,el){
		if($(el).hasClass('waiting')){
			load_content(el);
		}
	});
	$('.form-post-by-enter').on('keypress', function(e){
		if(e.which == 13){
			$(this).closest('form')[0].submit();
		}
	});
	$('.toggle-menu-action').on('click', function(){
		if($('.menu-toggle').hasClass('hidden')){
			$('.menu-toggle').removeClass('hidden');
			$('body').addClass('no-scroll');
		}
		else{
			$('.menu-toggle').addClass('hidden');
			$('body').removeClass('no-scroll');
		}
	});
	$('.session-card').each(function(i,el){
		let time_el=$(el).find('.session-time');
		let start_time=time_el.data('start-time');
		let end_time=time_el.data('end-time');
		let start_time_date=new Date(start_time*1000);
		let end_time_date=new Date(end_time*1000);

		let start_date_str='';
		start_date_str+=('0'+start_time_date.getDate()).slice(-2);
		start_date_str+='.';
		start_date_str+=('0'+(start_time_date.getMonth()+1)).slice(-2);
		start_date_str+='.';
		start_date_str+=start_time_date.getFullYear();

		let start_time_str='';
		start_time_str+=('0'+start_time_date.getHours()).slice(-2);
		start_time_str+=':';
		start_time_str+=('0'+start_time_date.getMinutes()).slice(-2);

		let end_time_str='';
		end_time_str+=('0'+end_time_date.getHours()).slice(-2);
		end_time_str+=':';
		end_time_str+=('0'+end_time_date.getMinutes()).slice(-2);

		$(el).find('.session-local-time .session-date').html('Local time: <span class="bold">'+start_date_str+'</span>');
		$(el).find('.session-local-time .session-time-range').html('<span class="bold">'+start_time_str+' &ndash; '+end_time_str+'</span>');
		$(el).find('.session-local-time').css('display','inline-block');
	});
	$('.event-card').each(function(i,el){
		let time_el=$(el).find('.event-time');
		let start_time=time_el.data('start-time');
		let start_time_date=new Date(start_time*1000);

		let start_date_str='';
		start_date_str+=('0'+start_time_date.getDate()).slice(-2);
		start_date_str+='.';
		start_date_str+=('0'+(start_time_date.getMonth()+1)).slice(-2);
		start_date_str+='.';
		start_date_str+=start_time_date.getFullYear();

		let start_time_str='';
		start_time_str+=('0'+start_time_date.getHours()).slice(-2);
		start_time_str+=':';
		start_time_str+=('0'+start_time_date.getMinutes()).slice(-2);

		$(el).find('.event-local-time .event-date').html('Local time: <span class="bold">'+start_date_str+'</span>');
		$(el).find('.event-local-time .event-start-time').html('<span class="bold">'+start_time_str+'</span>');
		$(el).find('.event-local-time').css('display','inline-block');
	});
	$('.content-card').each(function(i,el){
		let datetime_el=$(el).find('.content-time');
		let datetime_val=parseInt(datetime_el.data('datetime'));
		let datetime=new Date(datetime_val*1000);

		let date_str='';
		date_str+=('0'+datetime.getDate()).slice(-2);
		date_str+='.';
		date_str+=('0'+(datetime.getMonth()+1)).slice(-2);
		date_str+='.';
		date_str+=datetime.getFullYear();

		let time_str='';
		time_str+=('0'+datetime.getHours()).slice(-2);
		time_str+=':';
		time_str+=('0'+datetime.getMinutes()).slice(-2);

		$(el).find('.content-local-time .content-datetime').html('Local time: '+date_str+' '+time_str);
		$(el).find('.content-global-time').css('display','none');
		$(el).find('.content-local-time').css('display','inline-block');
	});
	$('.notify-item').each(function(i,el){
		let datetime_el=$(el).find('.notify-time');
		if(datetime_el.length>0){
			let datetime_val=parseInt(datetime_el.data('datetime'));
			let datetime=new Date(datetime_val*1000);

			let date_str='';
			date_str+=('0'+datetime.getDate()).slice(-2);
			date_str+='.';
			date_str+=('0'+(datetime.getMonth()+1)).slice(-2);
			date_str+='.';
			date_str+=datetime.getFullYear();

			let time_str='';
			time_str+=('0'+datetime.getHours()).slice(-2);
			time_str+=':';
			time_str+=('0'+datetime.getMinutes()).slice(-2);

			$(el).find('.notify-local-time .notify-datetime').html('Local time: '+date_str+' '+time_str);
			$(el).find('.notify-global-time').css('display','none');
			$(el).find('.notify-local-time').css('display','block');
		}
	});
	$('input.url-part').on('keypress',function(e){
		if(!e)e=window.event;
		let key=(e.charCode)?e.charCode:((e.keyCode)?e.keyCode:((e.which)?e.which:0));
		let char=String.fromCharCode(key);
		let pattern=/^([a-z0-9\-\.])$/;
		if($(this).hasClass('with-uppercase')){
			pattern=/^([a-zA-Z0-9\-\.])$/;
		}
		if(pattern.test(char)){
			return true;
		}
		else{
			$(this).parent().find('.additional-tooltip').addClass('show');
			return false;
		}
	});
	$('input.url-part').on('paste',function(e){
		if(!e)e=window.event;
		e.preventDefault();
		let paste=(e.clipboardData || window.clipboardData).getData("text");
		let paste_len=paste.length;
		let pattern=/([^a-z0-9\-\.])/g
		if($(this).hasClass('with-uppercase')){
			pattern=/([^a-zA-Z0-9\-\.])/g
		}
		paste=paste.replace(pattern,'');
		if(paste_len!=paste.length){
			$(this).parent().find('.additional-tooltip').addClass('show');
		}

		//insert into input cursor position paste value
		let start=$(this).prop('selectionStart');
		let end=$(this).prop('selectionEnd');
		let val=$(this).val();
		let before=val.substring(0,start);
		let after=val.substring(end);
		$(this).val(before+paste+after);
		$(this).prop('selectionStart',start+paste.length);
		$(this).prop('selectionEnd',start+paste.length);
	});

	$('input.title-part').on('keypress',function(e){
		if(!e)e=window.event;
		let key=(e.charCode)?e.charCode:((e.keyCode)?e.keyCode:((e.which)?e.which:0));
		let char=String.fromCharCode(key);
		let pattern=/^([a-zA-Z0-9\-\.\, \;\:])$/;//а-яёЁА-Я
		if(pattern.test(char)){
			return true;
		}
		else{
			$(this).parent().find('.additional-tooltip').addClass('show');
			return false;
		}
	});
	$('input.title-part').on('paste',function(e){
		if(!e)e=window.event;
		e.preventDefault();
		let paste=(e.clipboardData || window.clipboardData).getData("text");
		let paste_len=paste.length;
		let pattern=/([^a-zA-Z0-9\-\.\, \;\:])/g;//а-яёЁА-Я
		paste=paste.replace(pattern,'');
		if(paste_len!=paste.length){
			$(this).parent().find('.additional-tooltip').addClass('show');
		}

		//insert into input cursor position paste value
		let start=$(this).prop('selectionStart');
		let end=$(this).prop('selectionEnd');
		let val=$(this).val();
		let before=val.substring(0,start);
		let after=val.substring(end);
		$(this).val(before+paste+after);
		$(this).prop('selectionStart',start+paste.length);
		$(this).prop('selectionEnd',start+paste.length);
	});
	if($('.check-in-autoupdate').length>0){
		check_in_autoupdate();
	}
	$('.ajax-action').each(function(i,el){
		let href=$(el).prop('href');
		$(el).data('href',href);
		$(el).prop('href','javascript:void(0)');
		$(el).on('click',function(){
			ajax_action(el);
		});
	});

	//set min datetime for datetime-future inputs
	//it must be before hidden unixtime inputs not cut off min attr
	$('.datetime-future').each(function(i,el){
		$(el).attr('min',get_datetime_now());

		if(typeof $(el).data('minimal-offset')!='undefined'){
			$(el).on('change',function(){
				check_minimal_offset(this);
			});
		}
	});

	$('input[type=hidden]').each(function(i,el){
		if($(el).data('timestamp')){
			let parent_el=$('input[name="'+$(el).data('parent')+'"]');
			let unixtime=parseInt($(el).val());
			if(unixtime>0){
				//set current value attr
				let current_value=new Date(unixtime*1000);
				let current_year=current_value.getFullYear();
				let current_month=current_value.getMonth()+1;
				current_month=("0" + current_month).slice(-2);
				let current_day=current_value.getDate();
				current_day=("0" + current_day).slice(-2);
				let current_hours=current_value.getHours();
				current_hours=("0" + current_hours).slice(-2);
				let current_minutes=current_value.getMinutes();
				current_minutes=("0" + current_minutes).slice(-2);
				let current_value_str=""+current_year+"-"+current_month+"-"+current_day+"T"+current_hours+":"+current_minutes+":00";
				console.log('update datetime value from unixtime',parent_el,current_value_str,'old:',parent_el.val());
				parent_el.val(current_value_str);
				$(el).parent().find('.datetime-current-value').addClass('hidden');

				//rewrite min attr for datetime-future inputs (not cut off min attr from current time)
				if($(parent_el).hasClass('datetime-future')){
					//set min value attr
					let current_unixtime=new Date().getTime()/1000;
					let d=new Date(Math.min(unixtime,current_unixtime)*1000);
					let year=d.getFullYear();
					let month=d.getMonth()+1;
					month=("0" + month).slice(-2);
					let day=d.getDate();
					day=("0" + day).slice(-2);
					let hours=d.getHours();
					hours=("0" + hours).slice(-2);
					let minutes=d.getMinutes();
					minutes=("0" + minutes).slice(-2);
					let new_value=""+year+"-"+month+"-"+day+"T"+hours+":"+minutes;
					$(parent_el).attr('min',new_value);
				}
			}
		}
	});

	if(typeof auto_update_user_badge!== 'undefined'){
		if(auto_update_user_badge){
			if(0==$('.user-badge .check-in-status').length){//if user not checked in
				let lazy_status=false;
				if(typeof auto_update_user_badge_lazy!== 'undefined'){
					lazy_status=auto_update_user_badge_lazy;
				}
				update_user_badge_timer=setTimeout(function(){
					update_user_badge(lazy_status);
				},500);//0.5 sec delay
			}
		}
	}
	else{
		update_user_badge_timer=setTimeout(function(){
			update_user_badge(true);//lazy status
		},500);//0.5 sec delay
	}

	update_theme_changer();

	$('.admin-copy-icon').each(function(i,el){
		$(el).on('click',function(){
			copy_to_clipboard(el,
			function(el){
				$(el).addClass('active');
				//emoji check mark 🗸
				$(el).html('&#x1f5f8;');
			},
			function(el){
				$(el).removeClass('active');
				//emoji clipboard ⧉
				$(el).html('&boxbox;');
			});
		});
	});

	sortable_lists();

	//toggle timer array for each target
	let toggle_timer=[];
	$('.toggle-collapse-action').on('click',function(){
		//need clear timeout to prevent animation collapse after expand
		let target=$(this).data('target');
		let target_el=$('#'+target)[0];
		if(typeof toggle_timer[target]==='undefined'){
			toggle_timer[target]=null;
		}
		clearTimeout(toggle_timer[target]);
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			//need preset max-height in pixels to animate collapse
			target_el.style.maxHeight=target_el.scrollHeight+'px';
			toggle_timer[target]=setTimeout(function(){
				target_el.style.maxHeight='0px';
			},500);
		}
		else{
			$(this).addClass('active');
			target_el.style.maxHeight=target_el.scrollHeight+'px';
			//need set max-height to 100% after animation to prevent overflow on textarea resize
			toggle_timer[target]=setTimeout(function(){
				target_el.style.maxHeight='100%';
			},1500);
		}

	});

	bind_time_wrapper();

	$('.link-platform-notice-dismiss-action').on('click',function(){
		link_platform_notice_dismiss(this);
	});
	$('.modal-hide-action').on('click',function(){
		modal_hide(this,false);
	});
	$('.modal-show-action').on('click',function(){
		let modal_name=$(this).data('modal');
		if(typeof modal_name==='undefined'){
			modal_show(this,false);
		}
		else{
			if(modal_name=='organizers'){
				let link='/ajax/organizers-list/';
				let xhr = new XMLHttpRequest();
				xhr.timeout=5000;//5 seconds
				xhr.overrideMimeType('text/plain');
				xhr.open('GET',link);
				xhr.setRequestHeader('accept','application/json, text/plain, */*');
				xhr.setRequestHeader('content-type','application/json');
				xhr.ontimeout = function() {
					console.log('ajax_action timeout',link);
				};
				xhr.onreadystatechange = function() {
					if(4==xhr.readyState && 200==xhr.status){
						let response=xhr.response;
						console.log('ajax_action response',response);
						if(''!=response){
							$('.modal-wrapper[data-modal="'+modal_name+'"] ul.organizers-list').html(response);
						}
					}
					if(4==xhr.readyState && 200!=xhr.status){
						console.log('ajax_action status',xhr.status);
					}
				};
				xhr.send();
			}
			modal_show(false,modal_name);
		}
	});

	use_jdenticon();//update all jdenticons
});