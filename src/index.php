<?php
error_reporting(255);
include('preload.php');
$t->open('index.tpl','index');

$path='';
if(isset($_SERVER['REQUEST_URI'])){
	$path=$_SERVER['REQUEST_URI'];
}
$_GET=array();
$query_string=false;
if(false!==strpos($path,'?')){
	$query_string=substr($path,1+strpos($path,'?'));
	$path=substr($path,0,strpos($path,'?'));
	$pairs=explode('&',$query_string);
	foreach($pairs as $pair){
		if(false!==strpos($pair,'=')){
			list($name,$value)=explode('=',$pair);
			$_GET[$name]=urldecode($value);
		}
	}
}
$path_ext=false;
if(false!==strrpos($path,'.')){
	$path_ext=substr($path,strrpos($path,'.'));
	$allowed_path_extensions=['.css'];
	if(!in_array($path_ext,$allowed_path_extensions)){
		$path_ext=false;
	}
}
if(!$path_ext){
	if('/'!=substr($path,strlen($path)-1)){
		if($query_string){
			$query_string='?'.$query_string;
		}
		header('location:'.$path.'/'.$query_string);
		exit;
	}
}
$path_array=explode('/',trim($path));

if('o'==$path_array[1]){
	//walletconnect sdk trying to connect not needed rpc for listening block
	//just return empty respond with http status 200
	exit;
}

$replace=[];
$replace['counters']=[];
$script_preset='';
include($root_dir.'/modules/user_session.php');

$content='';
$module_name='index';
if(file_exists($root_dir.'/landing/index.html')){
	$module_name='landing-wrapper';
}
if($path_array[1]){
	$module_name=preg_replace('~^[^a-z0-9\-_]+$~iUs','',$path_array[1]);
	if('@'==$path_array[1][0]){
		//changed from organizers to events_poster https://gitlab.com/envelop/tickets/getpass-land/-/issues/41
		$module_name='events_poster';
	}
}

if('index'==$module_name){
	$module_name='content';
	$path_array[1]='content';
	$path_array[2]='index';
}
include($root_dir.'/modules/prepare.php');
$module_file=$root_dir.'/modules/'.$module_name.'.php';
if(file_exists($module_file)){
	include($module_file);
}
else{
	header('HTTP/1.0 404 Not Found');
	$content.='<h1>404 Not Found</h1>';
	$content.='<p>The page that you have requested could not be found.</p>';
}

$replace['script_preset']='';
if($script_preset){
	$replace['script_preset'].='
<script type="text/javascript">
'.$script_preset.'
</script>';
}


if(is_array($replace['counters'])){
	//each counter add to head addon
	foreach($replace['counters'] as $counter_name=>$counter_code){
		if($counter_code){
			$replace['head_addon'].=$counter_code;
		}
	}
	$replace['counters']='';
}

$replace['content']=$content;
foreach ($replace as $name=>$value){
	$t->assign($name,$value,'index');
}
$render=$t->get('index');

if(false!==strpos(@$_SERVER['HTTP_ACCEPT_ENCODING'],'gzip')){
	header('Content-Encoding: gzip');
	print gzencode($render);
}
else{
	print $render;
}
exit;