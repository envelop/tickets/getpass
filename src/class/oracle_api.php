<?php
error_reporting(255);
use VIZ\JsonRPC;
$oracle_api=new VIZ\JsonRPC();
$oracle_api->set_header('Accept','application/json');

function get_oracle_api(){
	global $config;
	$now=time();
	$timeblock=intval(time() / $config['oracle_timeout']);
	$hash=hash('sha256',$config['oracle_app_name'].$config['oracle_app_key'].$timeblock);
	return $config['oracle_app_id'].'.'.$hash;
}

function discover($oracle_api,$chain_id,$address,$type='721',$page=1,$size=10,$array=[]){
	global $db;
	//https://github.com/ethereum/EIPs/issues/721
	//https://github.com/ethereum/EIPs/issues/1155
	$oracle_url='https://api.envelop.is/discover/'.$type.'/user/'.$chain_id.'/'.strtolower($address).'?page='.$page.'&size='.$size;
	$oracle_api->set_header('Authorization',get_oracle_api());
	$result=$oracle_api->get_url($oracle_url);
	list($header,$result)=$oracle_api->parse_web_result($result);
	if(false===strpos($header," 200 OK\r\n")){
		$db->sql("INSERT INTO `error` (`time`,`name`,`text`) VALUES ('".time()."','discover error from api','".$db->prepare($oracle_url.PHP_EOL.$header.PHP_EOL.$result)."')");
		return $array;
	}
	else{
		$json=json_decode($result,true);
		$array=array_merge($array,$json);
		if($size==count($json)){
			$page++;
			return discover($oracle_api,$chain_id,$address,$type,$page,$size,$array);
		}
		else{
			return $array;
		}
	}
}
function discover_collection($oracle_api,$chain_id,$address,$type='721',$page=1,$size=10,$array=[]){
	global $db;
	//https://github.com/ethereum/EIPs/issues/721
	//https://github.com/ethereum/EIPs/issues/1155
	$oracle_url='https://api.envelop.is/discover/'.$type.'/'.$chain_id.'/'.strtolower($address).'?page='.$page.'&size='.$size;
	$oracle_api->set_header('Authorization',get_oracle_api());
	$result=$oracle_api->get_url($oracle_url);
	list($header,$result)=$oracle_api->parse_web_result($result);
	if(false===strpos($header," 200 OK\r\n")){
		$db->sql("INSERT INTO `error` (`time`,`name`,`text`) VALUES ('".time()."','discover error from api','".$db->prepare($oracle_url.PHP_EOL.$header.PHP_EOL.$result)."')");
		return $array;
	}
	else{
		$json=json_decode($result,true);
		$array=array_merge($array,$json);
		if($size==count($json)){
			$page++;
			return discover_collection($oracle_api,$chain_id,$address,$type,$page,$size,$array);
		}
		else{
			return $array;
		}
	}
}
function discover_nft($oracle_api,$chain_id,$address,$token_id,$type='721',$page=1,$size=10,$array=[]){
	global $db;
	//https://github.com/ethereum/EIPs/issues/721
	//https://github.com/ethereum/EIPs/issues/1155
	$oracle_url='https://api.envelop.is/discover/'.$type.'/'.$chain_id.'/'.strtolower($address).'/'.$token_id.'?page='.$page.'&size='.$size;
	$oracle_api->set_header('Authorization',get_oracle_api());
	$result=$oracle_api->get_url($oracle_url);
	list($header,$result)=$oracle_api->parse_web_result($result);
	if(false===strpos($header," 200 OK\r\n")){
		$db->sql("INSERT INTO `error` (`time`,`name`,`text`) VALUES ('".time()."','discover error from api','".$db->prepare($oracle_url.PHP_EOL.$header.PHP_EOL.$result)."')");
		return $array;
	}
	else{
		$json=json_decode($result,true);
		$array=array_merge($array,$json);
		if($size==count($json)){
			$page++;
			return discover_nft($oracle_api,$chain_id,$address,$token_id,$type,$page,$size,$array);
		}
		else{
			return $array;
		}
	}
}

function normalize_nft_arr($arr,$address,$type=3){
	$result=[];
	$address=strtolower($address);
	foreach($arr as $k=>$v){
		if($v['owner']==$address){
			if(10>strlen($v['token_id'])){
				$v['token_id']=(int)$v['token_id'];
			}
			$item=[];
			$item[]=$v['contract_address'];
			$item[]=$v['token_id'];
			$item[]=$type;
			if($v['is_wnft']){
				$item[]=$v['in_contract_address'];
				$item[]=$v['in_token_id'];
				$item[]=(int)$v['in_asset_type'];
			}

			$ignore=false;
			if(isset($v['balance'])){
				if(0===$v['balance']){
					$ignore=true;
				}
			}
			if(!$ignore){
				$result[]=$item;
			}
		}
	}
	return $result;
}

function normalize_collection_arr($arr,$address,$type=3){
	$result=[];
	$address=strtolower($address);
	foreach($arr as $k=>$v){
		if($v['contract_address']==$address){
			if(0!=$v['token_id']){
				if(10>strlen($v['token_id'])){
					$v['token_id']=(int)$v['token_id'];
				}
				$item=[];
				$item[]=$v['owner'];
				$item[]=$v['token_id'];
				$item[]=$type;

				$ignore=false;
				if(isset($v['balance'])){
					if(0===$v['balance']){
						$ignore=true;
					}
				}
				if(!$ignore){
					$result[]=$item;
				}
			}
		}
	}
	return $result;
}

function get_nft_list($oracle_api,$chain_id,$address){
	$nft_raw_arr=discover($oracle_api,$chain_id,$address);
	$nft_arr_721=normalize_nft_arr($nft_raw_arr,$address,3);//type 3 for 721
	$nft_raw_arr=discover($oracle_api,$chain_id,$address,'1155');
	$nft_arr_1155=normalize_nft_arr($nft_raw_arr,$address,4);//type 4 for 1155
	return array_merge($nft_arr_721,$nft_arr_1155);
}

function test_api($address='0x54eE92EEB9E6A959CDcBb975Bc50F28d8EE28189',$chain_id=1){
	global $oracle_api;
	print get_oracle_api();
	$address=strtolower($address);
	print '<hr>Discover 721 for '.$address.': ';
	print PHP_EOL.'https://api.envelop.is/discover/721/user/'.$chain_id.'/'.$address.'?page=1&size=10'.PHP_EOL;
	$nft_raw_arr=discover($oracle_api,$chain_id,$address);
	print '<hr>Raw NFT array:';
	print '<pre>';
	var_dump($nft_raw_arr);
	print '</pre>';
	$nft_arr=normalize_nft_arr($nft_raw_arr,$address,3);
	print '<hr>Normalized NFT array:';
	print '<pre>';
	print_r($nft_arr);
	print '</pre>';

	//https://rinkeby.etherscan.io/tokens-nft1155/ for tests
	print '<hr>Discover 1155: ';
	$nft_raw_arr2=discover($oracle_api,$chain_id,$address,'1155');
	print '<hr>Raw NFT array:';
	print '<pre>';
	print_r($nft_raw_arr2);
	print '</pre>';
	$nft_arr2=normalize_nft_arr($nft_raw_arr2,$address,4);
	print '<hr>Normalized NFT array:';
	print '<pre>';
	print_r($nft_arr2);
	print '</pre>';

	$full_nft_arr=array_merge($nft_arr,$nft_arr2);
	print '<hr>Merged NFT array:';
	print '<pre>';
	print_r($full_nft_arr);
	print '</pre>';
	exit;
}