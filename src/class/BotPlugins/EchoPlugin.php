<?php
namespace BotPlugins;
class EchoPlugin{
	public function load(){
		print static::class.' loaded'.PHP_EOL;
		return true;
	}
	public function init(){
		return false;
	}
	public function preset_action(){
		return false;
	}
	public function internal_action(){
		global $id,$message_text,$user,$db,$use_plugin;
		if('Return to Index'==$message_text){
			telegram_queue($user['id'],'sendMessage',['text'=>'New location: Index room.']);
			$db->sql("UPDATE `telegram_users` SET `plugin`='index', `step`=0, `state`='', `trigger`=1 WHERE `id`='".$user['id']."'");
			$use_plugin=false;
			return true;
		}
		return false;
	}
	public function process_media_group($user,$data){
		telegram_queue($user['id'],'sendMediaGroup',$data);
	}
	public function process_user(){
		global $user;
		$inline_keyboard=false;
		$keyboard=false;
		$keyboard=[
			[
				['text'=>'Return to Index'],
			],
		];
		$data=['text'=>'Welcome to Echo room!'];
		telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
		return true;
	}
	public function process_update(){
		global $id,
			$user,
			$media_groups,
			$media_groups_id,
			$media_groups_data,
			$media_groups_plugin,
			$message_id,
			$message_text,
			$message_entities,
			$message_animation,
			$message_voice,
			$message_video,
			$message_video_note,
			$message_audio,
			$message_photo,
			$message_sticker,
			$message_document,
			$message_media_group,
			$message_poll,
			$message_dice,
			$message_game,
			$message_contact,
			$message_invoice,
			$message_location,
			$message_venue,
			$message_caption,
			$message_caption_entities,
			$message_parse_mode;

		$return_media_group=false;
		$inline_keyboard=false;
		$keyboard=false;
		/*
		$keyboard=[
			[
				['text'=>'Look at the map'],
				['text'=>'Enter to Calc room'],
			],
		];
		*/
		if($message_text){
			$data=['text'=>$message_text,'reply_to_message_id'=>$message_id];
			if($message_entities){
				$data['entities']=$message_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_game){
			$data=['text'=>'Can not play the game... I am a robot.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_location){
			$data=['text'=>'I am a robot, not a map application.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_venue){
			$data=['text'=>'I am a robot, not a map application.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_contact){
			$data=['text'=>'I am a robot, not a contact book.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_invoice){
			$data=['text'=>'Can not pay the bill... I am a robot, not a bank.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_animation){
			$data=['animation'=>$message_animation,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendAnimation',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_voice){
			$data=['voice'=>$message_voice,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendVoice',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_video){
			if($message_media_group){
				if(!isset($media_groups[$user['id']])){
					$media_groups[$user['id']]=[];
				}
				if(!isset($media_groups[$user['id']][$message_media_group])){
					$media_groups[$user['id']][$message_media_group]=[];
				}
				$media_groups[$user['id']][$message_media_group][]=['type'=>'video','media'=>$message_video];

				if(!isset($media_groups_id[$message_media_group])){
					$media_groups_id[$message_media_group]=[];
				}
				$media_groups_id[$message_media_group][]=$id;

				if(!isset($media_groups_data[$message_media_group])){
					$media_groups_data[$message_media_group]=[];
				}
				$media_groups_plugin[$message_media_group]='echo';
				if($message_caption){
					$media_groups_data[$message_media_group]['caption']=$message_caption;
				}
				if($message_caption_entities){
					$media_groups_data[$message_media_group]['caption_entities']=$message_caption_entities;
				}
				if($message_parse_mode){
					$media_groups_data[$message_media_group]['parse_mode']=$message_parse_mode;
				}
				if($message_id){//reply_to_message_id
					$media_groups_data[$message_media_group]['reply_to_message_id']=$message_id;
				}
				$return_media_group=true;
			}
			else{
				$data=['video'=>$message_video,'reply_to_message_id'=>$message_id];
				if($message_caption){
					$data['caption']=$message_caption;
				}
				if($message_caption_entities){
					$data['caption_entities']=$message_caption_entities;
				}
				if($message_parse_mode){
					$data['parse_mode']=$message_parse_mode;
				}
				telegram_queue($user['id'],'sendVideo',$data,$inline_keyboard,$keyboard);
				return true;
			}
		}
		elseif($message_audio){
			$data=['audio'=>$message_audio,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendAudio',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_video_note){
			$data=['video_note'=>$message_video_note,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendVideoNote',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_photo){
			if($message_media_group){
				if(!isset($media_groups[$user['id']])){
					$media_groups[$user['id']]=[];
				}
				if(!isset($media_groups[$user['id']][$message_media_group])){
					$media_groups[$user['id']][$message_media_group]=[];
				}
				$media_groups[$user['id']][$message_media_group][]=['type'=>'photo','media'=>$message_photo];

				if(!isset($media_groups_id[$message_media_group])){
					$media_groups_id[$message_media_group]=[];
				}
				$media_groups_id[$message_media_group][]=$id;

				if(!isset($media_groups_data[$message_media_group])){
					$media_groups_data[$message_media_group]=[];
				}
				$media_groups_plugin[$message_media_group]='echo';
				if($message_caption){
					$media_groups_data[$message_media_group]['caption']=$message_caption;
				}
				if($message_caption_entities){
					$media_groups_data[$message_media_group]['caption_entities']=$message_caption_entities;
				}
				if($message_parse_mode){
					$media_groups_data[$message_media_group]['parse_mode']=$message_parse_mode;
				}
				if($message_id){//reply_to_message_id
					$media_groups_data[$message_media_group]['reply_to_message_id']=$message_id;
				}
				$return_media_group=true;
			}
			else{
				$data=['photo'=>$message_photo,'reply_to_message_id'=>$message_id];
				if($message_caption){
					$data['caption']=$message_caption;
				}
				if($message_caption_entities){
					$data['caption_entities']=$message_caption_entities;
				}
				if($message_parse_mode){
					$data['parse_mode']=$message_parse_mode;
				}
				telegram_queue($user['id'],'sendPhoto',$data,$inline_keyboard,$keyboard);
				return true;
			}
		}
		elseif($message_sticker){
			$data=['sticker'=>$message_sticker,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendSticker',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_document){
			$data=['document'=>$message_document,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendDocument',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_dice){
			$data=['emoji'=>$message_dice['emoji'],'reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendDice',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif(false!==$message_poll){
			$data=$message_poll;
			$data['reply_to_message_id']=$message_id;
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendPoll',$data,$inline_keyboard,$keyboard);
			return true;
		}
		else{
			$data=['text'=>'Unknown message type, please be more correct.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}

		if($return_media_group){
			return true;
		}
		else{
			return false;
		}
	}
}