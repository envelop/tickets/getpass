<?php
namespace BotPlugins;
class IndexPlugin{
	public function load(){
		print static::class.' loaded'.PHP_EOL;
		return true;
	}
	public function init(){
		return false;
	}
	public function preset_action(){
		return false;
	}
	private function linked_platform_status($linked_platform){
		global $id,$user,$db;
		$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$linked_platform['address']."'");
		$type_arr=$db->sql_row("SELECT * FROM `types` WHERE `id`='".$address_arr['type']."'");
		$text='Address:'.PHP_EOL.$address_arr['address'].' ('.$type_arr['name'].')';
		$text.=PHP_EOL.PHP_EOL.'/toggle_'.$linked_platform['id'].'_1'.PHP_EOL.($linked_platform['flag1']?'✔️':'❌').' — Subscription to GetPass.is updates';
		$text.=PHP_EOL.PHP_EOL.'/toggle_'.$linked_platform['id'].'_2'.PHP_EOL.($linked_platform['flag2']?'✔️':'❌').' — Subscription to address notifications';
		$text.=PHP_EOL.PHP_EOL.'/toggle_'.$linked_platform['id'].'_3'.PHP_EOL.($linked_platform['flag3']?'✔️':'❌').' — Message about the successful activation of the NFT ticket';
		$text.=PHP_EOL.PHP_EOL.'/toggle_'.$linked_platform['id'].'_4'.PHP_EOL.($linked_platform['flag4']?'✔️':'❌').' — Subscription to organizers updates';
		$text.=PHP_EOL.PHP_EOL.'/toggle_'.$linked_platform['id'].'_5'.PHP_EOL.($linked_platform['flag5']?'✔️':'❌').' — Subscription to events updates';
		$text.=PHP_EOL.PHP_EOL.'/unlink_'.$linked_platform['id'].PHP_EOL.'💥 Destroy the link between the address and this Telegram account';
		telegram_queue($user['id'],'sendMessage',['text'=>$text]);
	}
	private function address_mailing_rules($linked_platform){
		global $id,$user,$db,$config;
		$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$linked_platform['address']."'");
		$type_arr=$db->sql_row("SELECT * FROM `types` WHERE `id`='".$address_arr['type']."'");
		$address_str='Address:'.PHP_EOL.$address_arr['address'].' ('.$type_arr['name'].')';
		$mailing_rules_count=$db->table_count('mailing_rules','WHERE `address`='.$address_arr['id'].' AND `type`=1');//organizers
		if($mailing_rules_count){
			$mailing_rules=$db->sql("SELECT * FROM `mailing_rules` WHERE `address`='".$address_arr['id']."' AND `type`=1");//organizers
			$list=[];
			foreach($mailing_rules as $mailing_rule_arr){
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$mailing_rule_arr['target']."'");
				if(1==$mailing_rule_arr['rule']){
					$list[]='/delrule_'.$linked_platform['id'].'_'.$mailing_rule_arr['id'].PHP_EOL.'✔️ Subscribed to <a href="'.$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/">"'.htmlspecialchars($organizer_arr['title']).'"</a>';
				}
				else{
					$list[]='/delrule_'.$linked_platform['id'].'_'.$mailing_rule_arr['id'].PHP_EOL.'❌ Ignoring <a href="'.$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/">"'.htmlspecialchars($organizer_arr['title']).'"</a>';
				}
			}
			telegram_queue($user['id'],'sendMessage',['text'=>$address_str.PHP_EOL.PHP_EOL.'Subscriptions to organizers:'.PHP_EOL.PHP_EOL.implode(PHP_EOL.PHP_EOL,$list),'parse_mode'=>'HTML']);
		}
		else{
			telegram_queue($user['id'],'sendMessage',['text'=>$address_str.PHP_EOL.PHP_EOL.'No subscriptions to organizers.']);
		}
		$mailing_rules_count=$db->table_count('mailing_rules','WHERE `address`='.$address_arr['id'].' AND `type`=2');//events
		if($mailing_rules_count){
			$mailing_rules=$db->sql("SELECT * FROM `mailing_rules` WHERE `address`='".$address_arr['id']."' AND `type`=2");//events
			$list=[];
			foreach($mailing_rules as $mailing_rule_arr){
				$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$mailing_rule_arr['target']."'");
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");
				if(1==$mailing_rule_arr['rule']){
					$list[]='/delrule_'.$linked_platform['id'].'_'.$mailing_rule_arr['id'].PHP_EOL.'✔️ Subscribed to <a href="'.$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/">"'.htmlspecialchars($event_arr['title']).'"</a>';
				}
				else{
					$list[]='/delrule_'.$linked_platform['id'].'_'.$mailing_rule_arr['id'].PHP_EOL.'❌ Ignoring <a href="'.$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/">"'.htmlspecialchars($event_arr['title']).'"</a>';
				}
			}
			telegram_queue($user['id'],'sendMessage',['text'=>$address_str.PHP_EOL.PHP_EOL.'Subscriptions to events:'.PHP_EOL.PHP_EOL.implode(PHP_EOL.PHP_EOL,$list),'parse_mode'=>'HTML']);
		}
		else{
			telegram_queue($user['id'],'sendMessage',['text'=>$address_str.PHP_EOL.PHP_EOL.'No subscriptions to events.']);
		}
	}
	public function internal_action(){
		global $id,$message_text,$user,$db,$use_plugin;
		if('/'==$message_text[0]){
			$command_str=substr($message_text,1);
			$command_arr=explode('_',$command_str);
			if(count($command_arr)>0){
				if('delrule'==$command_arr[0]){
					$linked_platform_id=(int)$command_arr[1];
					$linked_platform=$db->sql_row("SELECT * FROM `linked_platforms` WHERE `id`='".$linked_platform_id."'");
					if(null!==$linked_platform){
						if($linked_platform['internal_id']==$user['id']){
							$mailing_rule_id=(int)$command_arr[2];
							$db->sql("DELETE FROM `mailing_rules` WHERE `address`='".$linked_platform['address']."' AND `id`='".$mailing_rule_id."'");
							telegram_queue($user['id'],'sendMessage',['text'=>'Subscription deleted successfully.']);
								return true;
						}
					}
				}
				if('unlink'==$command_arr[0]){
					$linked_platform_id=(int)$command_arr[1];
					$linked_platform=$db->sql_row("SELECT * FROM `linked_platforms` WHERE `id`='".$linked_platform_id."'");
					if(null!==$linked_platform){
						if($linked_platform['internal_id']==$user['id']){
							$db->sql("UPDATE `linked_platforms` SET `status`=4, `status_time`='".time()."' WHERE `id`='".$linked_platform['id']."'");
							telegram_queue($user['id'],'sendMessage',['text'=>'Link destroyed successfully.']);
							return true;
						}
					}
				}
				if('toggle'==$command_arr[0]){
					$linked_platform_id=(int)$command_arr[1];
					$flag=(int)$command_arr[2];
					$linked_platform=$db->sql_row("SELECT * FROM `linked_platforms` WHERE `id`='".$linked_platform_id."'");
					if(null!==$linked_platform){
						if($linked_platform['internal_id']==$user['id']){
							if(isset($linked_platform['flag'.$flag])){
								$linked_platform['flag'.$flag]=($linked_platform['flag'.$flag]?0:1);
								$db->sql("UPDATE `linked_platforms` SET `flag".$flag."`='".$linked_platform['flag'.$flag]."' WHERE `id`='".$linked_platform['id']."'");
								$this->linked_platform_status($linked_platform);
								return true;
							}
						}
					}
				}
			}
		}
		if('Status'==$message_text){
			$linked_platforms_count=$db->table_count('linked_platforms',"WHERE `platform`='1' AND `internal_id`='".$user['id']."' AND `status`='2'");
			if($linked_platforms_count){
				telegram_queue($user['id'],'sendMessage',['text'=>'You have '.$linked_platforms_count.' linked addresses.']);
				$linked_platforms=$db->sql("SELECT * FROM `linked_platforms` WHERE `platform`='1' AND `internal_id`='".$user['id']."' AND `status`='2'");
				foreach($linked_platforms as $linked_platform){
					$this->linked_platform_status($linked_platform);
				}
			}
			else{
				telegram_queue($user['id'],'sendMessage',['text'=>'You have no linked addresses.']);
			}
			return true;
		}
		if('Subscriptions'==$message_text){
			$linked_platforms_count=$db->table_count('linked_platforms',"WHERE `platform`='1' AND `internal_id`='".$user['id']."' AND `status`='2'");
			if($linked_platforms_count){
				$linked_platforms=$db->sql("SELECT * FROM `linked_platforms` WHERE `platform`='1' AND `internal_id`='".$user['id']."' AND `status`='2'");
				foreach($linked_platforms as $linked_platform){
					$this->address_mailing_rules($linked_platform);
				}
			}
			else{
				telegram_queue($user['id'],'sendMessage',['text'=>'You have no linked addresses.']);
			}
			return true;
		}
		/*
		if('Enter to Calc room'==$message_text){
			telegram_queue($user['id'],'sendMessage',['text'=>'New location: Calc room.']);
			$db->sql("UPDATE `telegram_users` SET `plugin`='calc', `step`=0, `state`='', `trigger`=1 WHERE `id`='".$user['id']."'");
			$use_plugin=false;
			return true;
		}
		if('Look at the map'==$message_text){
			telegram_queue($user['id'],'sendMessage',['text'=>'Looking at the map.']);
			$db->sql("UPDATE `telegram_users` SET `plugin`='map', `step`=0, `state`='', `trigger`=1 WHERE `id`='".$user['id']."'");
			$use_plugin=false;
			return true;
		}
		*/
		return false;
	}
	public function process_media_group($user,$data){
		telegram_queue($user['id'],'sendMediaGroup',$data);
	}
	public function process_user(){
		global $user;
		$inline_keyboard=false;
		$keyboard=false;
		$keyboard=[
			[
				['text'=>'Status'],
				['text'=>'Subscriptions'],
			],
		];
		$data=['text'=>'Welcome to GetPass.is bot. Please, select the action:'];
		telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
		return true;
	}
	public function process_update(){
		global $db,
			$id,
			$user,
			$media_groups,
			$media_groups_id,
			$media_groups_data,
			$media_groups_plugin,
			$message_id,
			$message_text,
			$message_entities,
			$message_animation,
			$message_voice,
			$message_video,
			$message_video_note,
			$message_audio,
			$message_photo,
			$message_sticker,
			$message_document,
			$message_media_group,
			$message_poll,
			$message_dice,
			$message_game,
			$message_contact,
			$message_invoice,
			$message_location,
			$message_venue,
			$message_caption,
			$message_caption_entities,
			$message_parse_mode,
			$message_connected_website;

		$return_media_group=false;
		$inline_keyboard=false;
		$keyboard=false;
		$keyboard=[
			[
				['text'=>'Status'],
				['text'=>'Subscriptions'],
			],
		];
		if($message_text){
			$data=['text'=>$message_text,'reply_to_message_id'=>$message_id];
			if($message_entities){
				$data['entities']=$message_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		if($message_connected_website){
			$db->sql("UPDATE `telegram_users` SET `trigger`=1 WHERE `id`='".$user['id']."'");
			return true;
		}
		/*
		if($message_text){
			$data=['text'=>$message_text,'reply_to_message_id'=>$message_id];
			if($message_entities){
				$data['entities']=$message_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_game){
			$data=['text'=>'Can not play the game... I am a robot.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_location){
			$data=['text'=>'I am a robot, not a map application.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_venue){
			$data=['text'=>'I am a robot, not a map application.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_contact){
			$data=['text'=>'I am a robot, not a contact book.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_invoice){
			$data=['text'=>'Can not pay the bill... I am a robot, not a bank.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_animation){
			$data=['animation'=>$message_animation,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendAnimation',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_voice){
			$data=['voice'=>$message_voice,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendVoice',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_video){
			if($message_media_group){
				if(!isset($media_groups[$user['id']])){
					$media_groups[$user['id']]=[];
				}
				if(!isset($media_groups[$user['id']][$message_media_group])){
					$media_groups[$user['id']][$message_media_group]=[];
				}
				$media_groups[$user['id']][$message_media_group][]=['type'=>'video','media'=>$message_video];

				if(!isset($media_groups_id[$message_media_group])){
					$media_groups_id[$message_media_group]=[];
				}
				$media_groups_id[$message_media_group][]=$id;

				if(!isset($media_groups_data[$message_media_group])){
					$media_groups_data[$message_media_group]=[];
				}
				$media_groups_plugin[$message_media_group]='echo';
				if($message_caption){
					$media_groups_data[$message_media_group]['caption']=$message_caption;
				}
				if($message_caption_entities){
					$media_groups_data[$message_media_group]['caption_entities']=$message_caption_entities;
				}
				if($message_parse_mode){
					$media_groups_data[$message_media_group]['parse_mode']=$message_parse_mode;
				}
				if($message_id){//reply_to_message_id
					$media_groups_data[$message_media_group]['reply_to_message_id']=$message_id;
				}
				$return_media_group=true;
			}
			else{
				$data=['video'=>$message_video,'reply_to_message_id'=>$message_id];
				if($message_caption){
					$data['caption']=$message_caption;
				}
				if($message_caption_entities){
					$data['caption_entities']=$message_caption_entities;
				}
				if($message_parse_mode){
					$data['parse_mode']=$message_parse_mode;
				}
				telegram_queue($user['id'],'sendVideo',$data,$inline_keyboard,$keyboard);
				return true;
			}
		}
		elseif($message_audio){
			$data=['audio'=>$message_audio,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendAudio',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_video_note){
			$data=['video_note'=>$message_video_note,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendVideoNote',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_photo){
			if($message_media_group){
				if(!isset($media_groups[$user['id']])){
					$media_groups[$user['id']]=[];
				}
				if(!isset($media_groups[$user['id']][$message_media_group])){
					$media_groups[$user['id']][$message_media_group]=[];
				}
				$media_groups[$user['id']][$message_media_group][]=['type'=>'photo','media'=>$message_photo];

				if(!isset($media_groups_id[$message_media_group])){
					$media_groups_id[$message_media_group]=[];
				}
				$media_groups_id[$message_media_group][]=$id;

				if(!isset($media_groups_data[$message_media_group])){
					$media_groups_data[$message_media_group]=[];
				}
				$media_groups_plugin[$message_media_group]='echo';
				if($message_caption){
					$media_groups_data[$message_media_group]['caption']=$message_caption;
				}
				if($message_caption_entities){
					$media_groups_data[$message_media_group]['caption_entities']=$message_caption_entities;
				}
				if($message_parse_mode){
					$media_groups_data[$message_media_group]['parse_mode']=$message_parse_mode;
				}
				if($message_id){//reply_to_message_id
					$media_groups_data[$message_media_group]['reply_to_message_id']=$message_id;
				}
				$return_media_group=true;
			}
			else{
				$data=['photo'=>$message_photo,'reply_to_message_id'=>$message_id];
				if($message_caption){
					$data['caption']=$message_caption;
				}
				if($message_caption_entities){
					$data['caption_entities']=$message_caption_entities;
				}
				if($message_parse_mode){
					$data['parse_mode']=$message_parse_mode;
				}
				telegram_queue($user['id'],'sendPhoto',$data,$inline_keyboard,$keyboard);
				return true;
			}
		}
		elseif($message_sticker){
			$data=['sticker'=>$message_sticker,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendSticker',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_document){
			$data=['document'=>$message_document,'reply_to_message_id'=>$message_id];
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendDocument',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif($message_dice){
			$data=['emoji'=>$message_dice['emoji'],'reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendDice',$data,$inline_keyboard,$keyboard);
			return true;
		}
		elseif(false!==$message_poll){
			$data=$message_poll;
			$data['reply_to_message_id']=$message_id;
			if($message_caption){
				$data['caption']=$message_caption;
			}
			if($message_caption_entities){
				$data['caption_entities']=$message_caption_entities;
			}
			if($message_parse_mode){
				$data['parse_mode']=$message_parse_mode;
			}
			telegram_queue($user['id'],'sendPoll',$data,$inline_keyboard,$keyboard);
			return true;
		}
		else{
			$data=['text'=>'Unknown message type, please be more correct.','reply_to_message_id'=>$message_id];
			telegram_queue($user['id'],'sendMessage',$data,$inline_keyboard,$keyboard);
			return true;
		}
		*/

		if($return_media_group){
			return true;
		}
		else{
			return false;
		}
	}
}