<?php
namespace BotPlugins;
class GroupsPlugin{
	public function load(){
		print static::class.' loaded'.PHP_EOL;
		return true;
	}
	public function init(){
		return false;
	}
	public function internal_action(){
		return false;
	}
	public function process_user(){
		return true;
	}
	public function process_join_request($chat_id,$user_id){
		global $db,$config;
		$group_arr=$db->sql_row("SELECT * FROM `telegram_groups` WHERE `id` = '".$chat_id."'");
		if(null!==$group_arr){
			if($group_arr['status']==3){
				$filter=[];
				if(''!=$group_arr['filter']){
					$filter=json_decode($group_arr['filter'],true);
				}

				if($group_arr['event']){
					$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$group_arr['event']."'");
					$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");

					$user_addresses=[];
					$user_linked_addresses=$db->sql("SELECT * FROM `linked_platforms` WHERE `platform`='1' AND `internal_id`='".$user_id."' AND `status`=2");
					foreach($user_linked_addresses as $user_linked_address){
						$user_addresses[]=$user_linked_address['address'];
					}

					$allowed=false;
					$allow_event_manage=false;
					$allow_event_manage_status=0;//none, 1=manager, 2=admin
					foreach($user_addresses as $user_address){
						$user_address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$user_address."'");
						if(1==$user_address_arr['status']){//platform admin
							$allowed=true;
							$allow_event_manage=true;
							$allow_event_manage_status=2;
							break;
						}

						$organizer_address_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_arr['id']."' AND `address`='".$user_address."'");
						if(null!=$organizer_address_arr){//organizer admin
							$allowed=true;
							$allow_event_manage=true;
							$allow_event_manage_status=2;
							break;
						}

						$event_address_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_arr['id']."' AND `address`='".$user_address."'");
						if(null!=$event_address_arr){
							if(0!=$event_address_arr['status']){//not guest (binded or invited)
								$allowed=true;
							}

							//free access to any admin or manager
							if($event_address_arr['admin']){
								$allowed=true;
								$allow_event_manage=true;
								$allow_event_manage_status=2;
								break;
							}
							if($event_address_arr['manager']){
								$allowed=true;
								$allow_event_manage=true;
								$allow_event_manage_status=1;
								break;
							}

							//firstly checking user level
							if(isset($filter['level'])){
								if(0==$filter['level']){
									$allowed=true;
								}
								else{
									if($filter['level']<=$event_address_arr['level']){
										$allowed=true;
									}
									else{
										$allowed=false;
									}
								}
							}
							else{
								$allowed=true;
							}

							//secondly checking flags
							$allowed_by_flag=false;
							if(isset($filter['admin'])){
								if(1==$filter['admin']){
									if(1==$event_address_arr['admin']){
										$allowed=true;
										$allowed_by_flag=true;
									}
									else{
										$allowed=false;
									}
								}
							}
							if(isset($filter['manager'])){
								if(1==$filter['manager']){
									if(1==$event_address_arr['manager']){
										$allowed=true;
										$allowed_by_flag=true;
									}
									else{
										if(!$allowed_by_flag){
											$allowed=false;
										}
									}
								}
							}
							if(isset($filter['speaker'])){
								if(1==$filter['speaker']){
									if(1==$event_address_arr['speaker']){
										$allowed=true;
									}
									else{
										if(!$allowed_by_flag){
											$allowed=false;
										}
									}
								}
							}
						}
					}

					if($allowed){
						telegram_queue($chat_id,'unbanChatMember',['user_id'=>$user_id,'only_if_banned'=>true]);
						telegram_queue($chat_id,'approveChatJoinRequest',['user_id'=>$user_id]);
						$db->sql("INSERT INTO `telegram_groups_history` (`time`,`group_id`,`user_id`,`action`) VALUES ('".time()."','".$chat_id."','".$user_id."','1')");
						if(2==$allow_event_manage_status/* || in_array($user_id,$config['init_admins'])*/){
							telegram_queue($chat_id,'promoteChatMember',['user_id'=>$user_id,'can_promote_members'=>true,'can_invite_users'=>true,'can_pin_messages'=>true,'can_restrict_members'=>true,'can_manage_video_chats'=>true,'can_change_info'=>true,'can_delete_messages'=>true,'']);
						}
						elseif(1==$allow_event_manage_status){
							telegram_queue($chat_id,'promoteChatMember',['user_id'=>$user_id,'can_pin_messages'=>true,'can_restrict_members'=>true,'can_manage_video_chats'=>true,'can_change_info'=>true,'can_delete_messages'=>true,'']);
						}
					}
					else{
						telegram_queue($chat_id,'declineChatJoinRequest',['user_id'=>$user_id]);
						$db->sql("INSERT INTO `telegram_groups_history` (`time`,`group_id`,`user_id`,`action`) VALUES ('".time()."','".$chat_id."','".$user_id."','0')");
					}
					return true;
				}
			}
		}
	}
	public function process_update(){
		global $config,$options,$group,$user,$message_text,$db;
		if(false!==$message_text){
			if('/'==$message_text[0]){
				list($status,$sender_json)=telegram_method_with_status('getChatMember',['chat_id'=>$group['id'],'user_id'=>$user['id']],true);
				//print $sender_json;
				$group_admin=false;
				if(200==$status){
					$sender_arr=json_decode($sender_json,true);
					if('creator'==$sender_arr['result']['status'] || 'administrator'==$sender_arr['result']['status']){
						$group_admin=true;
					}
					else{
						$group_admin=false;
					}
				}
				else{
					$group_admin=false;
				}
				//print PHP_EOL.'$group_admin='.$group_admin;

				if($group_admin){
					$group_arr=$db->sql_row("SELECT * FROM `telegram_groups` WHERE `id`='".$group['id']."'");
					//print PHP_EOL.'$group_arr=';
					//print_r($group_arr);

					$user_addresses=[];
					$user_linked_addresses=$db->sql("SELECT * FROM `linked_platforms` WHERE `platform`='1' AND `internal_id`='".$user['id']."' AND `status`=2");
					foreach($user_linked_addresses as $user_linked_address){
						$user_addresses[]=$user_linked_address['address'];
						//print PHP_EOL.'find address '.$user_linked_address['address'];
					}

					$allow_event_manage=false;
					if($group_arr['event']){
						$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$group_arr['event']."'");
						$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");

						foreach($user_addresses as $user_address){
							$user_address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$user_address."'");
							if(1==$user_address_arr['status']){//platform admin
								$allow_event_manage=true;
								break;
							}

							$organizer_address_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_arr['id']."' AND `address`='".$user_address."'");
							if(null!=$organizer_address_arr){//organizer admin
								$allow_event_manage=true;
								break;
							}

							$event_address_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_arr['id']."' AND `address`='".$user_address."' AND (`admin`=1 OR `manager`=1)");
							if(null!=$event_address_arr){//event admin or manager
								$allow_event_manage=true;
								break;
							}
						}
					}
					else{
						$allow_event_manage=true;
					}

					//print PHP_EOL.'$allow_event_manage='.$allow_event_manage;

					if(!$allow_event_manage){
						//just ignore
						//telegram_queue($group['id'],'sendMessage',['text'=>'You are not allowed to manage this event chat.']);
						return true;
					}
					else{
						$message_text.='_';
						$message_text_clear=str_replace('@'.$options['bot'],'',$message_text);
						$command=explode('_',$message_text_clear);
						//print PHP_EOL.'$command=';
						//print_r($command);
						if('/filter'==$command[0]){
							if('status'==$command[1]){
								$filter=[];
								if($group_arr['filter']){
									$filter=json_decode($group_arr['filter'],true);
								}
								$filter_status_str='Filter conditions:'.PHP_EOL;
								if(isset($filter['level'])){
									$filter_status_str.='level => '.$filter['level'];
								}
								if(isset($filter['manager'])){
									$filter_status_str.='manager = '.$filter['manager'];
								}
								if(isset($filter['speaker'])){
									$filter_status_str.='speaker = '.$filter['speaker'];
								}
								if(isset($filter['admin'])){
									$filter_status_str.='admin = '.$filter['admin'];
								}
								if(0==count($filter)){
									$filter_status_str.='none (only NFT holders)';
								}
								$filter_status_str.=PHP_EOL.PHP_EOL.'Change filter:'.PHP_EOL.'/filter_clear (only NFT holders)';
								$levels=$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$group_arr['event']."' ORDER BY `level` ASC");
								foreach($levels as $level){
									$filter_status_str.=PHP_EOL.'/filter_level_'.$level['level'].' ('.$level['name'].')';
								}
								telegram_queue($group['id'],'sendMessage',['text'=>$filter_status_str]);
							}
							if('clear'==$command[1]){
								$db->sql("UPDATE `telegram_groups` SET `filter`='' WHERE `id`='".$group['id']."'");
								telegram_queue($group['id'],'sendMessage',['text'=>'Group filter was cleared.']);
							}
							if('level'==$command[1]){
								$filter=['level'=>(int)$command[2]];
								$db->sql("UPDATE `telegram_groups` SET `filter`='".$db->prepare(json_encode($filter))."' WHERE `id`='".$group['id']."'");
								telegram_queue($group['id'],'sendMessage',['text'=>'Group filter set to only for group level >= '.(int)$command[2].'.']);
							}
							if('speaker'==$command[1]){
								$filter=['speaker'=>1];
								$db->sql("UPDATE `telegram_groups` SET `filter`='".$db->prepare(json_encode($filter))."' WHERE `id`='".$group['id']."'");
								telegram_queue($group['id'],'sendMessage',['text'=>'Group filter set to only for speakers.']);
							}
							if('manager'==$command[1]){
								$filter=['manager'=>1];
								$db->sql("UPDATE `telegram_groups` SET `filter`='".$db->prepare(json_encode($filter))."' WHERE `id`='".$group['id']."'");
								telegram_queue($group['id'],'sendMessage',['text'=>'Group filter set to only for managers.']);
							}
							if('admin'==$command[1]){
								$filter=['admin'=>1];
								$db->sql("UPDATE `telegram_groups` SET `filter`='".$db->prepare(json_encode($filter))."' WHERE `id`='".$group['id']."'");
								telegram_queue($group['id'],'sendMessage',['text'=>'Group filter set to only for admins.']);
							}
						}
						if('/event'==$command[0]){
							$event_id=(int)$command[1];
							$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."'");
							if(1==$event_arr['status']){//active
								$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");

								$allow_event_manage=false;
								foreach($user_addresses as $user_address){
									$user_address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$user_address."'");
									if(1==$user_address_arr['status']){//platform admin
										$allow_event_manage=true;
										break;
									}

									$organizer_address_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_arr['id']."' AND `address`='".$user_address."'");
									if(null!=$organizer_address_arr){//organizer admin
										$allow_event_manage=true;
										break;
									}

									$event_address_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_arr['id']."' AND `address`='".$user_address."' AND (`admin`=1 OR `manager`=1)");
									if(null!=$event_address_arr){//event admin or manager
										$allow_event_manage=true;
										break;
									}
								}

								if($allow_event_manage){
									$db->sql("UPDATE `telegram_groups` SET `event`='".$db->prepare($event_arr['id'])."' WHERE `id`='".$group['id']."'");
									telegram_queue($group['id'],'sendMessage',['text'=>'Group event set to <a href="'.$config['platform_url'].'/@'.$organizer_arr['url'].'/'.$event_arr['url'].'/">'.$event_arr['title'].'</a>.','parse_mode'=>'HTML']);
								}
								else{
									telegram_queue($group['id'],'sendMessage',['text'=>'You are not allowed to manage this event.']);
								}
							}
							else{
								telegram_queue($group['id'],'sendMessage',['text'=>'Event is not active.']);
							}
						}
						if(0===strpos($message_text,'/start@'.$options['bot'])){//bot mention in start command
							telegram_queue($group['id'],'sendMessage',['text'=>'Hello Group! Checking bot status...']);
							list($status,$status_json)=telegram_method_with_status('getChatMember',['chat_id'=>$group['id'],'user_id'=>$options['bot_id']]);
							$status_arr=json_decode($status_json,true);
							if(200==$status){
								print_r($status_arr);
								if(0==$group['status']){
									if('administrator'==$status_arr['result']['status']){
										$db->sql("UPDATE `telegram_groups` SET `status`='1', `status_time`='".time()."' WHERE `id`='".$group['id']."'");
										$group['status']=1;
									}
									else{
										telegram_queue($group['id'],'sendMessage',['text'=>'Bot is not administrator in this group! Provide administrator rights to bot with invite permissions and try again.']);
									}
								}
								if(1==$group['status']){
									if($status_arr['result']['can_invite_users']){
										$db->sql("UPDATE `telegram_groups` SET `status`='2', `status_time`='".time()."' WHERE `id`='".$group['id']."'");
										$group['status']=2;
									}
									else{
										telegram_queue($group['id'],'sendMessage',['text'=>'Bot administrator in this group! Provide invite permissions and try again.']);
									}
								}
								if(2==$group['status']){
									if(''==$group['invite_link']){
										list($status,$invite_json)=telegram_method_with_status('createChatInviteLink',['chat_id'=>$group['id'],'creates_join_request'=>true]);
										$invite_arr=json_decode($invite_json,true);
										print_r($invite_arr);
										if(200==$status){
											$db->sql("UPDATE `telegram_groups` SET  `status`='3', `status_time`='".time()."', `invite_link`='".$invite_arr['result']['invite_link']."' WHERE `id`='".$group['id']."'");
											$group['invite_link']=$invite_arr['result']['invite_link'];
											$group['status']=3;
										}
										else{
											telegram_queue($group['id'],'sendMessage',['text'=>'Bot can not create new invite, please check permissions, group status and try again.']);
										}
									}
									else{
										$db->sql("UPDATE `telegram_groups` SET  `status`='3', `status_time`='".time()."' WHERE `id`='".$group['id']."'");
										$group['status']=3;
										telegram_queue($group['id'],'sendMessage',['text'=>'Bot already have invite link to this group: '.$group['invite_link']]);
									}
								}
								if(3==$group['status']){
									telegram_queue($group['id'],'sendMessage',['text'=>'Invite link to this group: '.$group['invite_link'].PHP_EOL.'Join requests will be accepted automatically for participants who hold NFT from Whitelist (check /filter_status to configure conditions).']);

									if(0==$group['event']){
										$events_list='';
										$events_list_arr=[];
										foreach($user_addresses as $user_address){
											//$events_list.=PHP_EOL.'get user orgs '.$user_address;
											$organizer_addresses=$db->sql("SELECT * FROM `organizer_addresses` WHERE `address`='".$user_address."'");
											foreach($organizer_addresses as $organizer_address_arr){//organizers admin
												//$events_list.=PHP_EOL.'get events for org '.$organizer_address_arr['organizer'];
												$events=$db->sql("SELECT `id` FROM `events` WHERE `organizer`='".$organizer_address_arr['organizer']."' AND `status`='1' ORDER BY `id` DESC");
												foreach($events as $event_arr){
													$events_list_arr[$event_arr['id']]=true;
												}
											}

											$events_address=$db->sql("SELECT * FROM `event_users` WHERE `address`='".$user_address."' AND (`admin`=1 OR `manager`=1)");
											foreach($events_address as $event_address){//event admin or manager
												$events_list_arr[$event_address['event']]=true;
											}
										}

										foreach($events_list_arr as $event_id=>$event_status){
											$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."'");
											if(null!=$event_arr){
												if(1==$event_arr['status']){//active
													$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");
													$events_list.=PHP_EOL.'/event_'.$event_arr['id'].' <a href="'.$config['platform_url'].'/@'.$organizer_arr['url'].'/'.$event_arr['url'].'/">'.$event_arr['title'].'</a> from '.$organizer_arr['title'].'';
												}
											}
										}
										if(''==$events_list){
											$events_list.=PHP_EOL.'No active events';
										}

										telegram_queue($group['id'],'sendMessage',['text'=>'Group event not set. Please select event from list:'.$events_list,'parse_mode'=>'HTML']);
									}
									else{
										$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$group['event']."'");
										if(null!=$event_arr){
											if(1==$event_arr['status']){//active
												$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");
												telegram_queue($group['id'],'sendMessage',['text'=>'Group event set to <a href="'.$config['platform_url'].'/@'.$organizer_arr['url'].'/'.$event_arr['url'].'/">'.$event_arr['title'].'</a> from '.$organizer_arr['title'].'','parse_mode'=>'HTML']);
											}
											else{
												telegram_queue($group['id'],'sendMessage',['text'=>'Group event not set. Event is not active.']);
												$db->sql("UPDATE `groups` SET `event`='0' WHERE `id`='".$group['id']."'");
											}
										}
										else{
											telegram_queue($group['id'],'sendMessage',['text'=>'Group event not set. Event not found.']);
											$db->sql("UPDATE `groups` SET `event`='0' WHERE `id`='".$group['id']."'");
										}
									}
								}
							}
						}
					}
				}
			}
			return true;
		}
		return false;
	}
}