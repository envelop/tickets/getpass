let signer_type='1';
let signer_container=$('.signer_'+signer_type);
let chains=null;
let wc=null;
let selected_provider=false;
let wc_provider=false;
let current_address=false;
let current_chain=false;
let check_result=false;
let check_count=0;
let message=gen_message();

let is_safari=navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
	navigator.userAgent &&
	navigator.userAgent.indexOf('CriOS') == -1 &&
	navigator.userAgent.indexOf('FxiOS') == -1;

let loading_html='<div class="loading-status"><button class="btn" disabled><img src="/loader.svg" class="rotate"> Loading&hellip;</button></div>';
function provider_button(name,caption){
	return '<div><button class="btn connect-'+name+'-action"><img src="/'+name+'.svg"> '+caption+'</button></div>';
}
function gen_message(){
	return platform_str+'\nsession:'+auth_session+'\ntimestamp:'+(new Date().getTime()/1000 | 0)+'\nnonce:'+Math.random().toString().slice(8);
}
function init_providers(){
	if(typeof window.WalletConnectSDK !== 'undefined'){
		if(window.WalletConnectSDK.default){
			wc=new window.WalletConnectSDK.default();
		}
	}
}
function check_providers(){
	console.log('check_providers',selected_provider);
	if(check_result){
		signer_container.find('.connect-buttons').html('');
		return;
	}
	if(false===selected_provider){
		signer_container.find('.connect-status').html('');
		signer_container.find('.sign-status').html('');

		signer_container.find('.connect-buttons').html(loading_html);
		let providers_html='';
		if(typeof window.ethereum !== 'undefined'){
			if(window.ethereum.isMetaMask){
				providers_html+=provider_button('metamask','Metamask');
			}
		}
		if(null !== wc){
			providers_html+=provider_button('walletconnect','WalletConnect');
		}
		if(is_safari){
			providers_html+='<center class="center-notice">Notice: Safari does not support Metamask, please use Chrome/Firefox with the Metamask extension to simplify the process.</center>';
		}
		signer_container.find('.connect-buttons').html(providers_html);
	}
}
function bind_provider(name){
	console.log('bind_provider',name);
	if(false===selected_provider){
		selected_provider=name;
		if('metamask'==selected_provider){
			ethereum.on('accountsChanged',account_change_event);
			ethereum.on('chainChanged',chain_change_event);
			ethereum.on('close',()=>{
				current_address=false;
				current_chain=false;
				selected_provider=false;
				check_providers();
			});
			ethereum.on('disconnect',()=>{
				current_address=false;
				current_chain=false;
				selected_provider=false;
				check_providers();
			});
		}
		if('walletconnect'==selected_provider){
			wc_provider.on('accountsChanged',account_change_event);
			wc_provider.on('chainChanged',chain_change_event);
			wc_provider.on('close',()=>{
				current_address=false;
				current_chain=false;
				selected_provider=false;
				check_providers();
			});
			wc_provider.on('disconnect',()=>{
				current_address=false;
				current_chain=false;
				selected_provider=false;
				check_providers();
			});
		}
	}
}
function update_chain_status(){
	if(0<signer_container.find('.connect-status .chain-status').length){
		signer_container.find('.connect-status .chain-status .value').html('...');
		let find=false;
		for(let i in chains){
			if(current_chain==chains[i].chain_id){
				signer_container.find('.connect-status .chain-status .value').html(chains[i].name/*+ ' (0x'+chains[i].chain_id+')'*/);
				find=true;
				break;
			}
		}
		if(!find){
			signer_container.find('.connect-status .chain-status .value').html('Unknown ('+current_chain+')');
		}
	}
}
function check_connect(){
	console.log('check_connect',selected_provider);
	if(false===selected_provider){
		signer_container.find('.connect-status').html(loading_html);
	}
	else{
		if(false!==current_address){
			let connect_html='';
			connect_html+='<div class="wallet-status my-2"><strong>Address:</strong> <span class="value eclipse-overflow">'+current_address+'</span></div>';
			connect_html+='<div class="chain-status my-2"><strong>Chain id:</strong> <span class="value">&hellip;</span></div>';
			signer_container.find('.connect-status').html(connect_html);
			update_chain_status();
			prepare_sign();
		}
		else{
			//clear all and connect again?
			check_providers();
		}
	}
}
function prepare_sign(){
	if(false===selected_provider){
		signer_container.find('.connect-status').html('');
	}
	else{
		let return_html='';
		if(typeof return_url !== 'undefined'){
			if(''!=return_url){
				return_html+='<div class="return-button" style="display:none">';
				if(typeof return_qr !== 'undefined'){
					if(true===return_qr){
						return_html+=`<div class="qr" style="display:none"><img alt="QR code for scan"></div>`;
					}
				}
				return_html+='<a class="btn btn-yellow" href="'+encodeURI(return_url)+'"><span class="arrow">&larr;</span> Return</a>';
				return_html+='</div>';
			}
		}
		signer_container.find('.sign-status').html(`
		<div class="action-button">
			<button class="btn sign-action">Sign Proof</button>
		</div>
		<div class="loading-status" style="display:none">
			<button class="btn return-wallet-action" disabled><img src="/loader.svg" class="rotate"> Please return to the wallet&hellip;</button>
		</div>
		<div class="error-status red" style="display:none">
			<strong>❌ Error:</strong> <span class="value">User refused to sign a message.</span>
		</div>
		<div class="fail-status red" style="display:none">
			<strong>❌ Error:</strong> <span class="value">Signature does not match the address.</span>
		</div>
		<div class="success-status" style="display:none">
			<strong>✔️ Success!</strong> <span class="value">Please return to <a href="/profile/">profile page</a> or <a href="/organizers/">explore organizers</a> and events.</span>
		</div>`+return_html);
		signer_container.find('.sign-status').addClass('active');

		//force click to sign-action button
		let event=document.createEvent('MouseEvents');
		event.initEvent('click',true,true);
		signer_container.find('.sign-action')[0].dispatchEvent(event);
	}
}
function account_change_event(accounts){
	console.log('account_change_event',accounts);
	if(accounts.length === 0) {
		console.log('Please connect to Provider.');
		current_address=false;
		current_chain=false;
		selected_provider=false;
		check_providers();
	}
	else
	if(accounts[0] !== current_address) {
		current_address=accounts[0];
		signer_container.find('.connect-buttons').html('');
		check_connect();
	}
}
function chain_change_event(chain){
	console.log('chain_change_event',chain);
	current_chain=parseInt(chain,16);
	update_chain_status();
}
function toHexString(byteArray) {
	return Array.prototype.map.call(byteArray, function(byte) {
	return ('0' + (byte & 0xFF).toString(16)).slice(-2);
	}).join('');
}
function check_signature(signature,callback){
	let xhr = new XMLHttpRequest();
	xhr.timeout=5000;
	xhr.overrideMimeType('text/plain');
	xhr.open('POST','/login/');
	xhr.setRequestHeader('accept','application/json, text/plain, */*');
	xhr.setRequestHeader('content-type','application/x-www-form-urlencoded');
	xhr.ontimeout=function(){
		console.log('check_signature timeout error');
	};
	xhr.onreadystatechange=function(){
		if(4==xhr.readyState && 200==xhr.status){
			try{
				let json=JSON.parse(xhr.response);
				if(typeof json.result !== 'undefined'){
					console.log('check_signature response json',xhr.response,json);
					callback(json.result);
				}
				else{
					callback(false);
				}
			}
			catch(e){
				console.log('check_signature response json error',xhr.response,e);
				callback(false);
			}
		}
		if(4==xhr.readyState && 200!=xhr.status){
			callback(false);
		}
	};
	let post_data='';
	post_data+='type='+signer_type;
	post_data+='&message='+message;
	post_data+='&signature='+signature;
	xhr.send(post_data);
	//$('.debug').html(post_data);
}
function actions(e){
	if(!e)e=window.event;
	//console.log(e);
	if('mousedown'==e.type){
		return;
	}
	let target=e.target || e.srcElement;
	if($(target).hasClass('return-wallet-action')){
		if('walletconnect'==selected_provider){
			let link=document.createElement('a');
			link.setAttribute('href','wc:'+wc_provider.wc.handshakeTopic+'@1?');
			if(document.createEvent){
				var event=document.createEvent('MouseEvents');
				event.initEvent('click',true,true);
				link.dispatchEvent(event);
			}
			else {
				link.click();
			}
		}
	}
	if($(target).hasClass('sign-action')){
		message=gen_message();
		signer_container.find('.sign-status .action-button').css('display','none');
		signer_container.find('.sign-status .error-status').css('display','none');
		signer_container.find('.sign-status .fail-status').css('display','none');
		signer_container.find('.sign-status .success-status').css('display','none');
		signer_container.find('.sign-status .return-button').css('display','none');

		signer_container.find('.sign-status .loading-status').css('display','block');

		let te=new TextEncoder('utf-8');
		let msg='0x'+toHexString(te.encode(message));
		if('metamask'==selected_provider){
			ethereum.request({method:'personal_sign',params:[msg,current_address]})
			.then((signature)=>{
				console.log('personal_sign',signature);
				check_count++;
				check_signature(signature,function(result){
					if(true===result){
						if('function'==typeof update_user_badge){
							update_user_badge();
						}
						signer_container.find('.sign-status .fail-status').css('display','none');
						signer_container.find('.sign-status .loading-status').css('display','none');
						signer_container.find('.sign-status .success-status').css('display','block');
						signer_container.find('.sign-status .return-button').css('display','block');
						if(typeof return_qr !== 'undefined'){
							if(return_qr){
								signer_container.find('.sign-status .return-button .qr img').prop('src','qr');
								signer_container.find('.sign-status .return-button .qr').css('display','block');
							}
						}
						if(typeof back_url !== 'undefined'){
							if(back_url){
								window.location=back_url;
							}
						}
						check_result=true;
					}
					else{
						signer_container.find('.sign-status .loading-status').css('display','none');
						signer_container.find('.sign-status .action-button').css('display','block');
						signer_container.find('.sign-status .fail-status').css('display','block');
					}
				});

			}).catch(error => {
				signer_container.find('.sign-status .loading-status').css('display','none');
				signer_container.find('.sign-status .action-button').css('display','block');
				signer_container.find('.sign-status .error-status').css('display','block');
				console.error(error);
			});
		}
		if('walletconnect'==selected_provider){
			wc_provider.request({method:'personal_sign',params:[msg,current_address]})
			.then((signature)=>{
				console.log('personal_sign',signature);
				check_count++;
				check_signature(signature,function(result){
					if(true===result){
						if('function'==typeof update_user_badge){
							update_user_badge();
						}
						signer_container.find('.sign-status .fail-status').css('display','none');
						signer_container.find('.sign-status .loading-status').css('display','none');
						signer_container.find('.sign-status .success-status').css('display','block');
						signer_container.find('.sign-status .return-button').css('display','block');
						if(typeof back_url !== 'undefined'){
							if(back_url){
								window.location=back_url;
							}
						}
						check_result=true;
					}
					else{
						signer_container.find('.sign-status .loading-status').css('display','none');
						signer_container.find('.sign-status .action-button').css('display','block');
						signer_container.find('.sign-status .fail-status').css('display','block');
					}
				});

			}).catch(error => {
				signer_container.find('.sign-status .loading-status').css('display','none');
				signer_container.find('.sign-status .action-button').css('display','block');
				signer_container.find('.sign-status .error-status').css('display','block');
				console.error(error);
			});
		}
	}
	if($(target).hasClass('connect-metamask-action')){
		signer_container.find('.connect-buttons .btn').prop('disabled','disabled');
		$(target).addClass('btn-yellow');

		ethereum.request({method:'eth_requestAccounts'})
		.then((accounts)=>{
			bind_provider('metamask');
			ethereum.request({method:'eth_chainId'}).then(chain_change_event).catch((err)=>{console.log(err)});
			account_change_event(accounts);
		})
		.catch((err)=>{
			if(err.code === 4001){
				// EIP-1193 userRejectedRequest error
				// If this happens, the user rejected the connection request.
				console.log('Please connect to MetaMask.');
			}
			else{
				console.error(err);
			}
			check_providers();
		});

		e.preventDefault();
		return;
	}

	if($(target).hasClass('connect-walletconnect-action')){
		signer_container.find('.connect-buttons .btn').prop('disabled','disabled');
		$(target).addClass('btn-yellow');

		wc.connect().then((result)=>{
			console.log('wc.connect result',result);
			if(!result){
				console.error('WalletConnect provider error');
				current_address=false;
				current_chain=false;
				selected_provider=false;
				check_providers();
			}
			/*
			rpc: {
				1: "https://rpcurl/like/infura/for/each/chainid",
			},
			*/
			wc_provider=wc.getWeb3Provider({rpc:'no sense',});
			console.log('wc_provider',wc_provider);
			wc_provider.enable().then((result)=>{
				console.log('wc_provider.enable() result',result);
				wc_provider._blockTracker.removeAllListeners();//prevent endless sync with Error: PollingBlockTracker - encountered an error while attempting to update latest block
				bind_provider('walletconnect');
				account_change_event(wc_provider.accounts);
				chain_change_event(wc_provider.chainId);
			}).catch((err)=>{
				console.log('wc_provider.enable() error',err);
				check_providers();
			});
		}).catch((err)=>{
			console.log('wc.connect error',err);
			check_providers();
		});

		e.preventDefault();
		return;
	}
}
async function get_json_file(path) {
	const res = await fetch(path);
	const json = await res.json();
	return json;
}
//https://github.com/fabiospampinato/cash
$(async function(){
	delete localStorage.walletconnect;
	chains=await get_json_file("/js/chains_1.json");
	init_providers();
	check_providers();

	document.addEventListener('mousedown',actions,false);
	document.addEventListener('touchstart',actions,false);
	document.addEventListener('click',actions,false);
	document.addEventListener('tap',actions,false);
})