<?php
error_reporting(0);
$config=[];
$config['db_host']='mariadb';
$config['db_login']=$_ENV['MYSQL_USER'];//mysql user
$config['db_password']=$_ENV['MYSQL_PASSWORD'];//mysql password
$config['db_base']=$_ENV['MYSQL_DATABASE'];//mysql database

$config['server_timezone']='Etc/GMT';

$config['platform_domain']=$_ENV['APP_DOMAIN'];
$config['platform_url']='https://'.$config['platform_domain'];
$config['platform_themes']=[
	'light',
	'dark',
];
$config['default_platform_theme']='dark';

$config['organizer_moderation']=false;//set status for new organizers (true=0 waiting, false=1 auto approved)
$config['login_auto_create_organizer']=true;//auto create organizer for new users
$config['event_moderation']=true;//set moderation for new events (true=0 waiting, false=1 auto approved)
$config['event_moderation_max_rejects']=3;//count of rejects for not allow request moderation
$config['link_platform_removal_moderation']=true;//set moderation for remove link to platform (true=create new request to admin, false=auto approved without moderation)
$config['default_wizard']=['manage'=>['wizard','dashboard','event'],'dismiss'=>false,'finished'=>[],'step'=>1];//for events

$config['landing_title']='NFT 2.0 for events';
$config['landing_descr']='GetPass is a service based on the Envelop protocol and oracle to help event organisers mint and check NFT tickets, distribute wNFT gifts and do many other useful things';

$config['platform_title']='GetPass is events platform';
$config['platform_descr']='Event management system by DAO Envelop. NFT tickets and accesses.';

$config['oracle_app_id']=$_ENV['ORACLE_APP_ID'];//oracle app id (from envelop oracle)
$config['oracle_app_name']=$_ENV['ORACLE_APP_NAME'];//oracle app name (from envelop oracle)
$config['oracle_app_key']=$_ENV['ORACLE_APP_KEY'];//oracle app key (from envelop oracle)
$config['oracle_timeout']=300;

$config['auth_activity_status']=180*24*60*60;//days for auth activity status (search for create notifications links)

$config['auth_activity_for_clear_notifications']=30*24*60*60;//check auth activity for clear notifications
$config['auth_notification_max_limit']=1000;//remove old notifications more than 1000

$config['recalc_event_files']=4*60*60;//time for recalc organizer files (4 hours)
$config['recalc_organizer_files']=4*60*60;//time for recalc organizer files (4 hours)

//for future purposes (working only limit for upload files size)
$config['subscription_plans']=[
	0=>[
		'name'=>'Basic',
		'max_upload_files_size'=>1024*1024*100,//100MB for free plan
		'max_events_count'=>5,
		'max_content_count'=>100,
	],
	1=>[
		'name'=>'PRO',
		'max_upload_files_size'=>1024*1024*1000,//1000MB
		'max_events_count'=>20,
		'max_content_count'=>1000,
	],
	2=>[
		'name'=>'VIP',
		'max_upload_files_size'=>1024*1024*10000,//10000MB
		'max_events_count'=>999999,//unlimited
		'max_content_count'=>999999,//unlimited
	],
];

//manuals links and text
$config['manual_video_icon']='<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-btn-fill" viewBox="0 0 16 16"><path d="M0 12V4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2zm6.79-6.907A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"/></svg>';
$config['manual_admin_caption']='Video manual';
$config['manual_admin_link']='https://www.youtube.com/watch?v=DurEmmf3xyE';
$config['manual_organizer_caption']='Video manual';
$config['manual_organizer_link']='';
$config['manual_event_caption']='Video manual';
$config['manual_event_link']='';

$config['csrf_expiration']=600;//10 minutes

$config['mail_smtp_host']=$_ENV['SMTP_APP_HOST'];//smtp host (sendinblue for example)
$config['mail_smtp_port']=$_ENV['SMTP_APP_PORT'];//smtp port
$config['mail_smtp_username']=$_ENV['SMTP_APP_USER'];//smtp username
$config['mail_smtp_password']=$_ENV['SMTP_APP_PASS'];//smtp password
$config['mail_from_title']=$_ENV['SMTP_APP_TITLE'];

$config['telegram_bot']=$_ENV['TELEGRAM_BOT_USERNAME'];;//telegram bot username (without @)
$config['telegram_bot_id']=$_ENV['TELEGRAM_BOT_ID'];;//telegram bot id (from api)
$config['telegram_api']=$_ENV['TELEGRAM_BOT_KEY'];//telegram bot api key (from @botfather)

$apps_arr=[
	'mintapp'=>'https://app.getpass.is/fastauth/%ID%/%KEY%/',
	'stage-mintapp'=>'https://stage.app.getpass.is/fastauth/%ID%/%KEY%/',
];
$app_secret_arr=[
	'mintapp'=>'EXAMPLE_KEY',
	'stage-mintapp'=>'EXAMPLE_STAGE_KEY',
];

$config['admin_type_addresses']=[//preset admin type addresses
	1=>[//EVM/Ethereum
		$_ENV['EVM_ADMIN_ADDRESS'],//admin address
	]
];

$ip='';
if(isset($_SERVER['REMOTE_ADDR'])){
	$ip=$_SERVER['REMOTE_ADDR'];
}
if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])){
	$ip=$_SERVER['HTTP_CF_CONNECTING_IP'];
}
if(isset($_SERVER['HTTP_X_REAL_IP'])){
	$ip=$_SERVER['HTTP_X_REAL_IP'];
}
if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
}

$root_dir='.';
if(isset($_SERVER['DOCUMENT_ROOT'])){
	$root_dir=$_SERVER['DOCUMENT_ROOT'];
}
if(isset($_SERVER['PWD'])){//from cron
	$root_dir=substr(__FILE__,0,strrpos(__FILE__,'/'));
}
set_include_path($root_dir);