<?php
ob_start();
$replace['title']='My events | '.$replace['title'];
print '<h1>My events</h1>';
print '<hr class="my-4">';
if(0==$auth['addresses']){
	print '<p>None addresses was found in current session. Please use <a href="/login/?back_url=/my-events/">Login page</a>.</p>';
}
else{
	print '<div class="text-content">';
	if($auth['default_address_id']){
		print '
		<div class="mb-4">
			<a href="/add-event/" class="action-btn">Create event</a>
		</div>';
	}
	if(0<$auth['organizers']){
		$events_count=0;
		foreach($auth['organizers_list'] as $organizer_id=>$organizer_status){
			$events_arr=$db->sql("SELECT * FROM `events` WHERE `organizer`='".$db->prepare($organizer_id)."' AND `status`<'3' ORDER BY `id` DESC");//0 - planning/draft, 1 - active/published, 2 - archived, 3 - deleted/hidden
			foreach($events_arr as $event_arr){
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($event_arr['organizer'])."'");
				if(3!=$organizer_arr['status']){//hidden
					$events_count++;
					print '<p class="mb-4">';
					print '<a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/manage/" target="_blank">'.htmlspecialchars($event_arr['title']).'</a>';
					/*
					//https://gitlab.com/envelop/tickets/getpass/-/issues/19#note_1390596817
					if(1!=$event_arr['status']){//for not active events
						print ' <span class="'.$events_status_style_arr[$event_arr['status']].'">('.htmlspecialchars($events_status_arr[$event_arr['status']]).')</span>';
					}
					if(1!=$event_arr['moderation']){//not approved
						print ' <span class="'.$events_moderation_style_arr[$event_arr['moderation']].'">('.htmlspecialchars($events_moderation_arr[$event_arr['moderation']]).')</span>';
					}
					*/
					print ' by ';
					print '<a href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/" target="_blank">'.htmlspecialchars($organizer_arr['title']).'</a>';
					if(1!=$organizer_arr['status']){//for not approved organizers
						print ' <span class="'.$organizers_status_arr_class[$organizer_arr['status']].'">('.htmlspecialchars($organizers_status_arr[$organizer_arr['status']]).')</span>';
					}
					//print ' <a class="violet-btn" href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/manage/">Manage event</a>';
					//print ' <a class="violet-btn" href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/">Manage organizer'.(1==$organizer_status?' (owner)':'').'</a>';
					print '</p>';
				}
			}
		}
		if(0==$events_count){
			print '<p>None events was found in current session. Please use <a href="/add-event/">Add event page</a></p>';
		}
	}
	else{
		print '<p>None organizers was found in current session. Please use <a href="/add-organizer/">Add organizer page</a></p>';
	}
	print '<hr>';
	print '<div class="my-4">
	<a href="/profile/" class="reverse-btn">&larr; Back to profile</a>
	</div>';
	print '</div>';
}
$content=ob_get_contents();
ob_end_clean();