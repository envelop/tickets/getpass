<?php
ob_start();
$replace['title']='Create organizer | '.$replace['title'];
//https://gitlab.com/envelop/tickets/getpass/-/issues/16
$video_manual='https://www.youtube.com/@envelop';
print '<div class="admin-title-wrapper">';
print '<h1>Create organizer</h1>';
if($video_manual){
	print '<div class="button-wrapper">';
	print '<a href="'.$video_manual.'" target="_blank" class="action-btn">'.$ltmp['icons']['video'].' '.$config['manual_video_caption'].'</a>';
	print '</div>';
}
print '</div>';
print '<hr class="my-4">';
print '<div class="text-content">';
if($auth['default_address_id']){
	if($config['organizer_moderation']){
		print '<p>Fill the form and send request for organizer approve.</p>';
	}
	print '<p>Owner address: <a href="/profile/'.$auth['default_address_id'].'/">'.$auth_address_arr[$auth['default_address_id']]['address'].'</a>.</p>';
	if(isset($_POST['title'])){
		$title=trim($_POST['title']);
		if(0==$db->table_count('organizers',"WHERE `title`='".$db->prepare($title)."'")){
			$url=trim($_POST['url']);
			//remove all unsopperted symbols from url
			$url=preg_replace('~[^a-zA-Z0-9\-\.]+~iUs','',$url);
			$url=preg_replace('/\.+/','.',$url);
			if(0==$db->table_count('organizers',"WHERE `url`='".$db->prepare($url)."'")){
				$errors=[];
				if(''==$title){
					$errors[]='Title is empty';
				}
				$description=trim($_POST['description']);
				if(''==$description){
					$errors[]='Description is empty';
				}
				$events_description=trim($_POST['events_description']);
				if(''==$events_description){
					$errors[]='Events description is empty';
				}

				if(0==count($errors)){
					$status=$config['organizer_moderation']?0:1;//0 - waiting, 1 - approved
					$db->sql("INSERT INTO `organizers` SET
						`url`='".$db->prepare($url)."',
						`title`='".$db->prepare($title)."',
						`description`='".$db->prepare($description)."',
						`events_description`='".$db->prepare($events_description)."',
						`status`='".$status."',
						`time`='".time()."',
						`update_time`='".time()."'
					");
					$organizer_id=$db->last_id();

					$db->sql("INSERT INTO `organizer_addresses` SET
						`organizer`='".$db->prepare($organizer_id)."',
						`address`='".$db->prepare($auth['default_address_id'])."',
						`status`='1'
					");
					//https://gitlab.com/envelop/tickets/getpass/-/issues/17
					//$notice_text='The organizer <a href="/@'.htmlspecialchars($url).'/">"'.htmlspecialchars($title).'"</a> has been created'.(0==$status?' and sent for moderation approval':'').'.';
					$notice_text='Your organizer has been created. You can now create events. Each event undergoes post-moderation. You can learn about the moderation rules via the <a href="/terms/" target="_blank">link</a>. Enjoy using GetPass';
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>'.$notice_text.'</p>
						<p>You will be redirected to your profile in 5 seconds.</p>
					</div>';

					//redirect to profile
					print '<meta http-equiv="refresh" content="5;url=/profile/">';

					//send notification to all admin if need moderation
					if($config['organizer_moderation']){
						//get all administators and send them notification about new organizer
						$platform_admins=$db->sql("SELECT `id` FROM `addresses` WHERE `status`='1'");
						foreach($platform_admins as $platform_admin){
							add_notify($platform_admin['id'],0,1,'admin_new_org',json_encode(['organizer_title'=>htmlspecialchars($title),'organizer_url'=>htmlspecialchars($url)]));
						}
					}
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
				}
			}
			else{
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Organizer with the same url already exist</p>
				</div>';
			}
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Organizer with the same title already exist</p>
			</div>';
		}
	}
	else{
		print '<form action="" method="POST" class="manage-card">';
		print '<div class="max-w-fit">';
		$object_scheme=[
			'url'=>['type'=>'text','title'=>'URL (Shortname)','required'=>true,'descr'=>'Used for pages /@shortname/ in address bar','class'=>'url-part with-uppercase'],
			'title'=>['type'=>'text','title'=>'Organizer title','required'=>true],
			'description'=>['type'=>'text','title'=>'Organizer description','required'=>true],
			'events_description'=>['type'=>'text','title'=>'Events description','required'=>true],
		];
		print build_form($object_scheme);
		print '</div>';
		print '<input type="submit" value="Create" class="action-btn big">';
		print '</form>';
	}
}
else{
	print '<p>Please <a href="/login/">sign-in</a>, organizer creation linked to wallet address.</p>';
}
print '</div>';
$content=ob_get_contents();
ob_end_clean();