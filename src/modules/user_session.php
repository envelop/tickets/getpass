<?php
$auth=[];
$auth_address_arr=[];

$replace['script_change_time']=$script_change_time;
$replace['css_change_time']=$css_change_time;
$replace['title']=$config['platform_title'];
$replace['description']=$config['platform_descr'];
$replace['year']=date('Y');
$replace['platform_url']=$config['platform_url'];
$replace['event_url']='/';//main platform page
$replace['head_addon']='';
$replace['menu_button_wrapper_addon']='';
$replace['event_title']=$config['platform_title'];

$replace['theme']=$config['default_platform_theme'];
if(isset($_COOKIE['theme'])){
	if(in_array($_COOKIE['theme'],$config['platform_themes'])){
		$replace['theme']=$_COOKIE['theme'];
	}
}
$replace['event_logo_url']='';
if('light'==$replace['theme']){
	$replace['event_logo_url']='/getpass-new-logo-light-purple.svg';
}
if('dark'==$replace['theme']){
	$replace['event_logo_url']='/getpass-new-logo-dark-purple.svg';
}

$replace['event_logo_addon']='';//'<div class="beta">βeta</div>';//'<span>Events</span>';
$replace['event_logo_class']='';
$replace['event_cover']='';
$replace['event_speakers_cover']='';
$replace['main_container_addon']='';//class addon for main-container
$replace['main_container_addon']=' single';//move footer to bottom

$replace['counters']['google']='
<script async src="https://www.googletagmanager.com/gtag/js?id=G-J4W3YR86V1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag(\'js\', new Date());
	gtag(\'config\', \'G-J4W3YR86V1\');
</script>';

$replace['theme_mode_badge']='<a class="theme-toggle cursor-pointer" title="Toggle theme"></a>';
$replace['attention_badge']='';
$replace['notify_badge']='';
$replace['user_badge']='<a href="/login/">Login</a>';

$robot=false;
if($_SERVER['SERVER_ADDR']==$ip){
	$robot=true;//local request
}
if(!$robot){
	//check HTTP_USER_AGENT for robots
	if(isset($_SERVER['HTTP_USER_AGENT'])){
		$robots_arr=[
			'Googlebot',
			'bingbot',
			'YandexBot',
			'DuckDuckBot',
			'Baiduspider',
			'Yahoo!',
		];
		foreach($robots_arr as $robot_name){
			if(false!==strpos($_SERVER['HTTP_USER_AGENT'],$robot_name)){
				$robot=true;
				$auth['id']=0;
				$auth['hash']='robot';
				break;
			}
		}
	}
}

$auth_id=0;
if(!$robot){
	if(isset($_COOKIE['session'])){
		$auth=$db->sql_row("SELECT * FROM `auth` WHERE `hash`='".$db->prepare($_COOKIE['session'])."'");
		$auth_id=$auth['id'];
	}
	else{
		$auth_hash=hash('sha256',$config['platform_domain'].uniqid());
		$db->sql("INSERT INTO `auth` (`hash`,`init_ip`,`time`) VALUES ('".$db->prepare($auth_hash)."','".$db->prepare($ip)."','".time()."')");
		$auth_id=$db->last_id();
		if($auth_id){
			$auth=$db->sql_row("SELECT * FROM `auth` WHERE `id`='".$auth_id."'");
		}
	}
}
$auth['status']=0;//default user
$auth['status_address_id']=0;//change to address id if user is administator
$auth['addresses']=0;//guest user
$auth['default_address_id']=0;//without address
$auth['organizers']=0;//as administration in organizers
$auth['organizers_list']=[];//list of organizers as administator (status)
$auth['organizers_list_address_id']=[];//list of organizers as administator (address id)
$auth['platforms']=0;//linked platforms
$auth['platforms_list']=[];//list of linked platforms
$auth['new_notifications_count']=0;//count of new notifications
if(!$robot){
	if(isset($auth['hash'])){
		setcookie('session',$auth['hash'],time()+60*60*24*365,'/');
		if($auth['init_ip']!=$ip){
			$db->sql("UPDATE `auth` SET `ip`='".$db->prepare($ip)."', `active_time`='".time()."' WHERE `id`='".$auth['id']."'");
		}
		else{
			$db->sql("UPDATE `auth` SET `active_time`='".time()."' WHERE `id`='".$auth['id']."'");
		}

		$auth_addresses=$db->sql("SELECT * FROM `auth_addresses` WHERE `auth`='".$auth['id']."'");
		foreach($auth_addresses as $auth_address){
			$auth_address_arr[$auth_address['address']]=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$auth_address['address']."'");
			if(0==$auth['addresses']){//first address
				$auth['default_address_id']=$auth_address['address'];
				$auth['default_address']=$auth_address_arr[$auth['default_address_id']]['address'];
				$address_caption=substr($auth['default_address'],0,4).'...'.substr($auth['default_address'],-4);
				//calc all curent platforms (1=telegram, 2=email)
				$address_linked_platforms=$db->table_count('linked_platforms',"WHERE `address`='".$auth_address_arr[$auth_address['address']]['id']."' AND `status`='2' AND (`platform`='1' OR `platform`='2')");
				if(0==$address_linked_platforms){//no linked platforms for default address
					$replace['attention_badge']=PHP_EOL.'<a class="attention-button" href="/link-platform/" title="Assign email or telegram for updates">'.$ltmp['icons']['attention'].'<span class="explainer">Assign email or telegram for updates</span></a>';
				}
				//auth notifications can be 0 (none new), 1 (new), 2 (need recalculate)
				//'.(0!=$auth['notifications']?' new':'').' - old version
				$replace['notify_badge']=PHP_EOL.'<a class="notify-button" href="/notifications/" title="Notifications">';
				$replace['notify_badge'].='{notify_counter}';
				$replace['notify_badge'].=$ltmp['icons']['notifications-bell'];
				//$replace['notify_badge'].='<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13.586V10c0-3.217-2.185-5.927-5.145-6.742C13.562 2.52 12.846 2 12 2s-1.562.52-1.855 1.258C7.185 4.074 5 6.783 5 10v3.586l-1.707 1.707A.996.996 0 0 0 3 16v2a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-2a.996.996 0 0 0-.293-.707L19 13.586zM19 17H5v-.586l1.707-1.707A.996.996 0 0 0 7 14v-4c0-2.757 2.243-5 5-5s5 2.243 5 5v4c0 .266.105.52.293.707L19 16.414V17zm-7 5a2.98 2.98 0 0 0 2.818-2H9.182A2.98 2.98 0 0 0 12 22z"></path></svg>';
				$replace['notify_badge'].='<span class="explainer">Notifications</span>';
				$replace['notify_badge'].='</a>';
				$replace['user_badge']='';
				$replace['user_badge'].='<a class="avatar" href="/profile/">';
				$replace['user_badge'].='<svg data-jdenticon-value="'.$auth['default_address'].'" width="80" height="80"  alt="avatar"></svg>';
				$replace['user_badge'].=$address_caption.'</a>';
			}
			$auth['addresses']++;//authed user
			if(1==$auth_address_arr[$auth_address['address']]['status']){
				$auth['status']=1;//admin
				$auth['status_address_id']=$auth_address_arr[$auth_address['address']]['id'];
			}
			$auth_address_arr[$auth_address['address']]['organizers']=[];

			$organizer_addresses=$db->sql("SELECT * FROM `organizer_addresses` WHERE `address`='".$auth_address_arr[$auth_address['address']]['id']."'");
			foreach($organizer_addresses as $organizers_address){
				if(!isset($auth_address_arr[$auth_address['address']]['organizers'][$organizers_address['organizer']])){
					$auth_address_arr[$auth_address['address']]['organizers'][$organizers_address['organizer']]=$organizers_address['status'];
					$auth['organizers']++;//authed organizer
					$auth['organizers_list'][$organizers_address['organizer']]=$organizers_address['status'];
					$auth['organizers_list_address_id'][$organizers_address['organizer']]=$auth_address['address'];
				}
				else{
					if(1==$organizers_address['status']){//owner change admin status
						$auth_address_arr[$auth_address['address']]['organizers'][$organizers_address['organizer']]=1;
						$auth['organizers_list'][$organizers_address['organizer']]=1;
						$auth['organizers_list_address_id'][$organizers_address['organizer']]=$auth_address['address'];
					}
				}
			}

			$linked_platforms=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$auth_address_arr[$auth_address['address']]['id']."'");
			foreach($linked_platforms as $linked_platform){
				if(4!=$linked_platform['status']){//if not deleted
					$auth['platforms']++;
					$auth['platforms_list'][$linked_platform['id']]=$linked_platform;
					if(!isset($auth_address_arr[$auth_address['address']]['platforms'])){
						$auth_address_arr[$auth_address['address']]['platforms']=0;
					}
					$auth_address_arr[$auth_address['address']]['platforms']++;
				}
			}
		}
		$notifications_counter='';
		$auth_notifications=$db->sql_row("SELECT count(`an`.`id`) as `count`
		FROM `auth_notifications` as `an` LEFT JOIN `notifications_queue` as `nq` ON `an`.`notify`=`nq`.`id` WHERE `an`.`auth`='".$auth['id']."' AND `status`!=2");
		$auth['new_notifications_count']=$auth_notifications['count'];
		if($auth['new_notifications_count']){
			if($auth['new_notifications_count']>99){
				$auth['new_notifications_count']='99+';
			}
			$notifications_counter='<span class="counter">'.$auth['new_notifications_count'].'</span>';
		}

		$replace['notify_badge']=ltmp($replace['notify_badge'],['notify_counter'=>$notifications_counter]);
	}
	else{
		setcookie('session','',time()-3600,'/');
	}
}

//https://gitlab.com/envelop/tickets/getpass/-/issues/33
$tab_class='';//default3-button
$replace['menu']='';
//Removed https://gitlab.com/envelop/tickets/getpass-land/-/issues/42
//$replace['menu'].='<li><a href="/"'.(''==$path_array[1]?' class="selected"':'').' target="_blank">About platform</a></li>';
//$replace['menu'].='<li><a href="/organizers/"'.('organizers'==$path_array[1]?' class="selected"':'').'>Organizers</a></li>';
if(1==$auth['status']){
	$replace['menu'].='<li><a href="/admin/" class="'.$tab_class.('admin'==$path_array[1]?' selected':'').'">Admin</a></li>';
}