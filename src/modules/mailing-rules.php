<?php
ob_start();
$replace['title']='Mailing rules'.' | '.$replace['title'];
if($auth['default_address_id']){
	if(!$path_array[2]){
		print '<h1>Mailing rules</h1><hr>';
		print '<div class="text-content">';
		print '<p>Each platform has individual mailing settings:</p>';
		print '<p><ul>';
		$platforms=$db->sql("SELECT * FROM `platforms` WHERE `status`='1' AND `mailing`=1");
		foreach($platforms as $platform_arr){
			print '<li><a href="/mailing-rules/'.$platform_arr['id'].'/">'.$platform_arr['name'].'</a></li>';
		}
		print '</ul></p>';
		print '<hr class="my-4">';
		print '<div class="links-list">
		<a href="/profile/" class="action-btn reverse-btn /*navigate*/">&larr; Back to profile</a>
		</div>';
		print '</div>';
	}
	else{
		$platform_id=(int)$path_array[2];
		$platform_arr=$db->sql_row("SELECT * FROM `platforms` WHERE `id`='".$platform_id."' AND `status`='1'");
		if($platform_arr){
			$replace['title']=htmlspecialchars($platform_arr['name']).' | '.$replace['title'];
			print '<h1><a href="/mailing-rules/">Mailing rules</a>: '.htmlspecialchars($platform_arr['name']).'</h1><hr>';
			print '<div class="text-content">';
			if(!isset($path_array[3])){
				$path_array[3]='';
			}
			if($path_array[3]){
				$linked_platform_id=(int)$path_array[3];
				$linked_platform_arr=$db->sql_row("SELECT * FROM `linked_platforms` WHERE `id`='".$linked_platform_id."' AND `platform`='".$platform_id."'");
				if(null!=$linked_platform_arr){
					$check_access=false;
					foreach($auth['platforms_list'] as $check_platform_link){
						if($check_platform_link['id']==$linked_platform_id){
							$check_access=true;
						}
					}
					if($check_access){
						if(1==$linked_platform_arr['platform']){//Telegram
							if($linked_platform_arr['internal_username']){
								$replace['title']='@'.htmlspecialchars($linked_platform_arr['internal_username']).' | '.$replace['title'];
								print '<h2>Configure @'.htmlspecialchars($linked_platform_arr['internal_username']).'</h2>';
							}
							else{
								$replace['title']='#'.htmlspecialchars($linked_platform_arr['internal_id']).' | '.$replace['title'];
								print '<h2>Configure #'.htmlspecialchars($linked_platform_arr['internal_id']).'</h2>';
							}
						}
						elseif(2==$linked_platform_arr['platform']){//Email
							$replace['title']=htmlspecialchars($linked_platform_arr['internal_username']).' | '.$replace['title'];
							print '<h2>Configure '.htmlspecialchars($linked_platform_arr['internal_username']).'</h2>';
						}
						else{
							print '<h2>Configure #'.$platform_arr['id'].'</h2>';
						}
						print '<p>Linked to address: <a href="/profile/'.$linked_platform_arr['address'].'/">'.$auth_address_arr[$linked_platform_arr['address']]['address'];
						//print ' ('.$types_arr[$auth_address_arr[$linked_platform_arr['address']]['type']]['name'].')</a>';
						print '</p>';
						print '<p>Link status: ';
						print '<span class="'.$linked_platforms_status_style_arr[$linked_platform_arr['status']].'">'.$linked_platforms_status_arr[$linked_platform_arr['status']].'</span>';
						print '</p>';

						if(isset($_GET['toggle_flag'])){
							if(check_csrf()){
								$flag=(int)$_GET['toggle_flag'];
								if(isset($linked_platform_arr['flag'.$flag])){
									$linked_platform_arr['flag'.$flag]=($linked_platform_arr['flag'.$flag]?0:1);
									$db->sql("UPDATE `linked_platforms` SET `flag".$flag."`='".$linked_platform_arr['flag'.$flag]."' WHERE `id`='".$linked_platform_arr['id']."'");
								}
							}
							else{
								print '
								<div class="attention-box" role="alert">
									<p class="font-bold">Error</p>
									<p>CSRF is invalid, try again.</p>
								</div>';
							}
						}
						print '<p><a href="?toggle_flag=1&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($linked_platform_arr['flag1']?'✔️':'❌').' &mdash; Subscription to GetPass.is updates</p>';
						print '<p><a href="?toggle_flag=2&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($linked_platform_arr['flag2']?'✔️':'❌').' &mdash; Subscription to address notifications</p>';
						print '<p><a href="?toggle_flag=3&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($linked_platform_arr['flag3']?'✔️':'❌').' &mdash; Message about the successful activation of the NFT ticket</p>';
						$mailing_rules_count=$db->table_count('mailing_rules','WHERE `address`='.$linked_platform_arr['address'].' AND `type`=1');//organizers
						print '<p><a href="?toggle_flag=4&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($linked_platform_arr['flag4']?'✔️':'❌').' &mdash; Subscription to organizers updates'.($mailing_rules_count?'<br><span class="text-sm text-gray-500">(listed below)</span>':'').'</p>';
						if($mailing_rules_count){
							print '<ul>';
							$mailing_rules=$db->sql("SELECT * FROM `mailing_rules` WHERE `address`='".$linked_platform_arr['address']."' AND `type`=1");//organizers
							foreach($mailing_rules as $mailing_rule_arr){
								$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$mailing_rule_arr['target']."'");
								if(1==$mailing_rule_arr['rule']){
									print '<li><span class="text-green-700"">Subscribed to</span> <a href="/@'.htmlspecialchars($organizer_arr['url']).'/" target="_blank">"'.htmlspecialchars($organizer_arr['title']).'"</a></li>';
								}
								else{
									print '<li><span class="text-red-700">Ignoring</span> <a href="/@'.htmlspecialchars($organizer_arr['url']).'/" target="_blank">"'.htmlspecialchars($organizer_arr['title']).'"</a></li>';
								}
							}
							print '</ul>';
						}
						$mailing_rules_count=$db->table_count('mailing_rules','WHERE `address`='.$linked_platform_arr['address'].' AND `type`=2');//events
						print '<p><a href="?toggle_flag=5&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($linked_platform_arr['flag5']?'✔️':'❌').' &mdash; Subscription to events updates<br><span class="text-sm text-gray-500">(if you are a participant in the event'.($mailing_rules_count?' or from the list below':'').')</span></p>';
						if($mailing_rules_count){
							print '<ul>';
							$mailing_rules=$db->sql("SELECT * FROM `mailing_rules` WHERE `address`='".$linked_platform_arr['address']."' AND `type`=2");//events
							foreach($mailing_rules as $mailing_rule_arr){
								$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$mailing_rule_arr['target']."'");
								$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");
								if(1==$mailing_rule_arr['rule']){
									print '<li><span class="text-green-700"">Subscribed to</span> <a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/" target="_blank">"'.htmlspecialchars($event_arr['title']).'"</a></li>';
								}
								else{
									print '<li><span class="text-red-700">Ignoring</span> <a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/" target="_blank">"'.htmlspecialchars($event_arr['title']).'"</a></li>';
								}
							}
							print '</ul>';
						}
						print '<hr class="my-4">';
						print '<div class="links-list">
						<a href="/profile/" class="action-btn reverse-btn /*navigate*/">&larr; Back to profile</a>
						</div>';

					}
					else{
						print '
						<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>Access denied</p>
						</div>';
					}
				}
				else{
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>Platform link not found</p>
					</div>';
				}
			}
			else{
				$find_platform_link=false;
				foreach($auth['platforms_list'] as $check_platform_link){
					if($check_platform_link['platform']==$platform_id){//selected platform
						if(2==$check_platform_link['status']){//approved status
							if(!$find_platform_link){
								print '<p>Each item has individual mailing settings:</p><ul>';
							}
							$find_platform_link=true;
							foreach($auth_address_arr as $auth_address){
								if($auth_address['id']==$check_platform_link['address']){
									print '<li><a href="/mailing-rules/'.$platform_arr['id'].'/'.$check_platform_link['id'].'/">';
									if(1==$check_platform_link['platform']){//Telegram
										if($check_platform_link['internal_username']){
											print '@'.htmlspecialchars($check_platform_link['internal_username']);
										}
										else{
											print '#'.htmlspecialchars($check_platform_link['internal_id']);
										}
									}
									elseif(2==$check_platform_link['platform']){//Email
										print htmlspecialchars($check_platform_link['internal_username']);
									}
									else{
										print ''.$auth_address['address'];
										//print ' ('.$types_arr[$auth_address['type']]['name'].')';
										if($check_platform_link['internal_username']){
											print ': '.htmlspecialchars($check_platform_link['internal_username']).'';
										}
									}
									print '</li>';
								}
							}
						}
					}
				}
				if($find_platform_link){
					print '</ul>';
				}
				else{
					print '<p>Please <a href="/link-platform/'.$platform_arr['id'].'/">assign "'.htmlspecialchars($platform_arr['name']).'"</a> to current session. This will allow you to receive notifications from the service, subscribe to updates from organizers and events.</p>';
				}
				print '<hr class="my-4">';
				print '<div class="links-list">
				<a href="/profile/" class="action-btn reverse-btn /*navigate*/">&larr; Back to profile</a>
				</div>';
			}
			print '</div>';
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Platform not found</p>
			</div>';
		}
	}
}
else{
	http_response_code(403);
	print '<h1>Access denied</h1>';
	print '<hr class="my-4">';
	print '<div class="text-content">';
	print '<p>Please <a href="/login/?back_url=/mailing-rules/">sign-in</a>, rules assigned to the platforms linked to the crypto address.</p>';
	print '</div>';
}
$content=ob_get_contents();
ob_end_clean();