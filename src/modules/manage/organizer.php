<?php
$replace['counters']='';
ob_start();
if(!isset($allow_manage)){
	http_response_code(404);
	exit;
}
if(false===$allow_manage){
	http_response_code(403);
	exit;
}
$organizer_id=$organizer_arr['id'];

$script_preset.='var manage_organizer=true;';

$types_arr=[];
$types=$db->sql("SELECT * FROM `types`");
foreach($types as $type){
	$types_arr[$type['id']]=$type;
}

$available_organizers=0;
foreach($auth['organizers_list'] as $auth_organizer_id=>$auth_organizer_status){
	$auth_organizer_arr=$db->sql_row("SELECT `id` FROM `organizers` WHERE `id`='".$db->prepare($auth_organizer_id)."' AND `status`<2");//0 (waiting approve) or 1 (approved)
	if(null!=$auth_organizer_arr){
		$available_organizers++;
	}
}

$replace['title']='Manage '.$replace['title'];
$replace['description']=htmlspecialchars($organizer_arr['description']);
print '<div class="admin-title-wrapper">';
print '<h1>Manage <a href="/@'.$organizer_url.'/">'.htmlspecialchars($organizer_arr['title']).'</a>';
if(0<$available_organizers){//have more than one organizer
	if($path_array[3]=='events'){
		if($path_array[4]=='create'){
			print '<a class="action-btn default-button modal-show-action" data-modal="organizers">Change</a>';
		}
	}
}
print '</h1>';
if($config['manual_organizer_link']){
	print '<div class="button-wrapper">';
	print '<a href="'.$config['manual_organizer_link'].'" target="_blank" class="action-btn">'.$ltmp['icons']['video'].' '.$config['manual_organizer_caption'].'</a>';
	print '</div>';
}
print '</div>';
print '<hr>';
print '<div class="my-4">';
print '<ul class="submenu">';
//https://gitlab.com/envelop/tickets/getpass/-/issues/33
$tab_class='';//default3-button
print '<li><a href="/@'.$organizer_url.'/manage/events/" class="'.$tab_class.('events'==$path_array[3]?' selected':'').'">Events</a></li>';
if(1==$allow_manage){//only owner can edit
	print '<li><a href="/@'.$organizer_url.'/manage/edit/" class="'.$tab_class.('edit'==$path_array[3]?' selected':'').'">Organizer</a></li>';
}
print '<li><a href="/@'.$organizer_url.'/manage/administration/" class="'.$tab_class.('administration'==$path_array[3]?' selected':'').'">Administration</a></li>';

print '<li><a href="/@'.$organizer_url.'/manage/tags/" class="'.$tab_class.('tags'==$path_array[3]?' selected':'').'">Tags</a></li>';
print '<li><a href="/@'.$organizer_url.'/manage/content/" class="'.$tab_class.('content'==$path_array[3]?' selected':'').'">Content</a></li>';
print '<li><a href="/@'.$organizer_url.'/manage/files/" class="'.$tab_class.('files'==$path_array[3]?' selected':'').'">Files</a></li>';
print '<li><a href="/@'.$organizer_url.'/manage/stats/" class="'.$tab_class.('stats'==$path_array[3]?' selected':'').'">Stats</a></li>';

print '</ul>';
print '</div>';
print '<hr class="my-4">';

if($limit_upload && count($_FILES)>0){
	print '
	<div class="attention-box" role="alert">
		<p><b>Error!</b> You can not upload files. Please <a href="/@'.$organizer_url.'/manage/files/">remove some files</a> before you upload new ones or subscribe to paid plan.</p>
	</div>';
	$_FILES=[];
}

if(0<$available_organizers){//have more than one organizer
	print '<div class="modal-bg active hide" data-modal="organizers">';
	print '
	<div class="modal-wrapper hide" data-modal="organizers">
		<div class="modal-box">
			<div class="modal-header">
				<h3>Select organizer</h3>';
				print '
<button type="button" class="modal-hide-button modal-hide-action">
	'.$ltmp['icons']['cross'].'
</button>
';
	print '
			</div>
			<div class="modal-body">';
	print '<ul class="organizers-list">';
	//sort organizers by id desc (newer first)
	krsort($auth['organizers_list'],SORT_NUMERIC);
	foreach($auth['organizers_list'] as $auth_organizer_id=>$auth_organizer_status){
		$auth_organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($auth_organizer_id)."'");
		if(null!=$auth_organizer_arr){
			if(2>$auth_organizer_arr['status']){//0 (waiting approve) or 1 (approved)
				print '<li><a href="/@'.$auth_organizer_arr['url'].'/manage/events/create/">'.htmlspecialchars($auth_organizer_arr['title']).'</a></li>';
			}
		}
	}
	print '</ul>';
	print '<p class="italic">Only available organizers is shown here.</p>';
	print '
			</div>
			<div class="modal-footer">
				<div class="grow-white-space">
					<a class="action-btn negative modal-hide-action opacity-75">Dismiss</a>
				</div>
				<a class="action-btn" href="/add-organizer/" target="_blank">Create organizer</a>
			</div>
		</div>
	</div>';
	print '</div>';
}

$tab_descr_arr=[
	'stats'=>'Main statistics of {ORGANIZER_NAME} here.',
	'edit'=>'Information about {ORGANIZER_NAME} displayed on the platform.',
	'administration'=>'Here you can change wallets and assign their roles: administrator or owner.',
	//'events'=>'All events of {ORGANIZER_NAME} here.',
	'tags'=>'This section is used in the blog on the platform, if you have one set up.',
	'content'=>'This section is used in the blog on the platform, if you have one set up.',
	'files'=>'All the files that you uploaded while working with the platform, as well as all the main data about them (upload time, uploader, file size, etc.).'
];
$ltml_tab_descr_arr=['ORGANIZER_NAME'=>$organizer_arr['title']];

if(''==$path_array[3]){
	header('Location: /@'.$organizer_url.'/manage/events/');
	exit;
}
if('stats'==$path_array[3]){
	$replace['title']='Dashboard | '.$replace['title'];
	print '<p>'.ltmp($tab_descr_arr[$path_array[3]],$ltml_tab_descr_arr).'</p>';
	print '<p>Subscription plan: '.$config['subscription_plans'][$organizer_arr['subscription_plan']]['name'].'</p>';
	print '<p>Max upload files size: '.human_readable_filesize($max_organizer_files_size).'</p>';
	print '<p>Administrators count: '.$db->table_count('organizer_addresses',"WHERE `organizer`='".$organizer_id."'").'</p>';
	print '<p>Events count: '.$db->table_count('events',"WHERE `organizer`='".$organizer_id."'").'</p>';
	print '<p>Preparing events: '.$db->table_count('events',"WHERE `status`=0 AND `organizer`='".$organizer_id."'").'</p>';
	print '<p>Active events: '.$db->table_count('events',"WHERE `status`=1 AND `organizer`='".$organizer_id."'").'</p>';
	print '<p>Archived events: '.$db->table_count('events',"WHERE `status`=2 AND `organizer`='".$organizer_id."'").'</p>';
	print '<p>Hidden events: '.$db->table_count('events',"WHERE `status`=3 AND `organizer`='".$organizer_id."'").'</p>';
	print '<hr>';
	print '<p>Content count: '.$db->table_count('organizer_content',"WHERE `organizer`='".$organizer_id."'").'</p>';
	print '<hr>';
	$summary_file_size=0;
	print '<p>Organizer files count: '.$db->table_count('organizer_files',"WHERE `organizer`='".$organizer_id."'").'</p>';
	$organizers_file_size=$db->select_one('organizer_files','SUM(`size`)',"WHERE `organizer`='".$organizer_id."'");
	print '<p>Organizers files size: '.human_readable_filesize($organizers_file_size).'</p>';
	$summary_file_size+=$organizers_file_size;

	print '<p>Events files count: '.$db->table_count('event_files',"WHERE `organizer`='".$organizer_id."'").'</p>';
	$events_file_size=$db->select_one('event_files','SUM(`size`)',"WHERE `organizer`='".$organizer_id."'");
	print '<p>Events files size: '.human_readable_filesize($events_file_size).'</p>';
	$summary_file_size+=$events_file_size;

	print '<p>Summary files size: '.human_readable_filesize($summary_file_size).'</p>';
}
elseif('edit'==$path_array[3]){
	$replace['title']='Edit | '.$replace['title'];
	if(1==$allow_manage){//only owner can edit
		print '<h2>Edit organizer info</h2>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[3]],$ltml_tab_descr_arr).'</p>';
		if(isset($_POST['title'])){
			$errors=[];

			$title=trim($_POST['title']);
			if(''==$title){
				$errors[]='Title is empty';
			}
			if(0!=$db->table_count('organizers',"WHERE `id`!='".$organizer_id."' AND `title`='".$db->prepare($title)."'")){
				$errors[]='Organizer with the same title already exist';
			}

			$description=trim($_POST['description']);
			if(''==$description){
				$errors[]='Description is empty';
			}
			$events_description=trim($_POST['events_description']);
			if(''==$events_description){
				$errors[]='Events description is empty';
			}

			if(0==count($errors)){
				$db->sql("UPDATE `organizers` SET
					`title`='".$db->prepare($title)."',
					`description`='".$db->prepare($description)."',
					`events_description`='".$db->prepare($events_description)."',
					`update_time`='".time()."'
					WHERE `id`='".$organizer_id."'
				");
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Organizer <a href="/@'.$organizer_url.'/manage/">"'.htmlspecialchars($title).'"</a> was updated</p>
				</div>';

				print '<meta http-equiv="refresh" content="5;url=/@'.$organizer_url.'/manage/edit/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="?" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';
			$object_scheme=[//organizer profile
				'url'=>['type'=>'text','title'=>'URL','required'=>true,'descr'=>'Used for /@<b>URL</b>/ page address bar.','editable'=>false],
				'title'=>['type'=>'text','title'=>'Title','required'=>true],
				'description'=>['type'=>'text','title'=>'Description','descr'=>'Text about organizer and mission'],
				'events_description'=>['type'=>'text','title'=>'Events description','descr'=>'Text that will be shown on the top of the events list page'],
			];
			print build_form($object_scheme,$organizer_arr);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Save changes" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	else{
		print '<div class="attention-box" role="alert">
			<p class="font-bold">Error</p>
			<p>You are not allowed to edit this organizer</p>
		</div>';
	}
}
elseif('administration'==$path_array[3]){
	$replace['title']='Administration | '.$replace['title'];
	if(1==$allow_manage){//owner actions
		if('delete_address'==$path_array[4]){
			if(check_csrf()){
				$organizers_address_id=intval($path_array[5]);
				$address_exist_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `id`='".$organizers_address_id."' AND `organizer`='".$organizer_id."'");
				if(null!=$address_exist_arr){
					$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$address_exist_arr['address']."'");
					$db->sql("DELETE FROM `organizer_addresses` WHERE `id`='".$organizers_address_id."'");

					//send notification to deleted admin
					add_notify($address_exist_arr['address'],0,1,'org_admin_deleted',json_encode(['organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url'])]));

					header('Location: /@'.$organizer_url.'/manage/administration/?delete_address=success&address='.urlencode($address_arr['address']));
					exit;
				}
				else{
					header('Location: /@'.$organizer_url.'/manage/administration/?delete_address=error&error='.$organizers_address_id);
					exit;
				}
			}
			else{
				header('Location: /@'.$organizer_url.'/manage/administration/?delete_address=error&error=csrf');
				exit;
			}
		}
		if('make_owner'==$path_array[4]){
			if(check_csrf()){
				$organizers_address_id=intval($path_array[5]);
				$address_exist_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `id`='".$organizers_address_id."' AND `organizer`='".$organizer_id."'");
				if(null!=$address_exist_arr){
					$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$address_exist_arr['address']."'");
					//make all other addresses as admin
					$db->sql("UPDATE `organizer_addresses` SET `status`=0 WHERE `organizer`='".$organizer_id."'");
					//make selected address as owner
					$db->sql("UPDATE `organizer_addresses` SET `status`=1 WHERE `id`='".$organizers_address_id."'");

					//send notification to new owner
					add_notify($address_exist_arr['address'],0,1,'org_new_owner',json_encode(['organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url'])]));

					header('Location: /@'.$organizer_url.'/manage/administration/?make_owner=success&address='.urlencode($address_arr['address']));
					exit;
				}
			}
			else{
				header('Location: /@'.$organizer_url.'/manage/administration/?make_owner=error&error=csrf');
				exit;
			}
		}
	}
	print '<p>'.ltmp($tab_descr_arr[$path_array[3]],$ltml_tab_descr_arr).'</p>';
	if(isset($_GET['make_owner'])){
		if('success'==$_GET['make_owner']){
			print '
			<div class="success-box" role="alert">
				<p class="font-bold">Success</p>
				<p>Address '.htmlspecialchars($_GET['address']).' was updated to organizer owner</p>
			</div>';
		}
		if('error'==$_GET['make_owner']){
			if('csrf'==$_GET['error']){
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
				</div>';
			}
		}
	}
	if(isset($_GET['delete_address'])){
		if('success'==$_GET['delete_address']){
			print '
			<div class="success-box" role="alert">
				<p class="font-bold">Success</p>
				<p>Address '.htmlspecialchars($_GET['address']).' was deleted from organizer administration</p>
			</div>';
		}
		if('error'==$_GET['delete_address']){
			if('csrf'==$_GET['error']){
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
				</div>';
			}
			else{
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Address #'.htmlspecialchars((int)$_GET['error']).' not found</p>
				</div>';
			}
		}
	}
	if(isset($_POST['add_address'])){
		$errors=[];
		$type=intval($_POST['type']);
		if(!isset($types_arr[$type])){
			$errors[]='Invalid type';
		}
		$caption='';
		if(isset($_POST['caption'])){
			$caption=trim($_POST['caption']);
		}
		$address=trim($_POST['address']);
		if(1==$type){//ethereum
			if(!preg_match('/^0x[a-fA-F0-9]{40}$/',$address)){
				$errors[]='Invalid EVM address';
			}
			$address=mb_strtolower($address);
		}
		if(''==$address){
			$errors[]='Address is empty';
		}
		$address_id=false;
		$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `address`='".$db->prepare($address)."' AND `type`='".$type."'");
		if(null!=$address_arr){
			$address_id=$address_arr['id'];
			//check if address is already in organizer
			$organizer_address_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."' AND `address`='".$address_id."'");
			if(null!=$organizer_address_arr){
				$errors[]='Address '.htmlspecialchars($address_arr['address']).' is already in organizer administration';
			}
		}
		else{
			$db->sql("INSERT INTO `addresses` SET
				`type`='".$type."',
				`address`='".$db->prepare($address)."',
				`caption`='".$db->prepare($caption)."',
				`time`='".time()."'
			");
			$address_id=$db->last_id();
		}
		$status=intval($_POST['status']);
		if(!isset($organizer_addresses_status_arr[$status])){
			$errors[]='Invalid status';
		}
		if(1==$status){
			$owner_exist=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."' AND `status`=1");
			if(null!=$owner_exist){
				$errors[]='Owner already exist, there can only be one';
			}
		}
		if(0==count($errors)){
			$db->sql("INSERT INTO `organizer_addresses`
				(`organizer`,`address`,`caption`,`status`)
				VALUES
				('".$organizer_id."','".$address_id."','".$db->prepare($caption)."','".$status."')
			");

			//send notification to added admin
			add_notify($address_id,0,1,'org_admin_added',json_encode(['organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url'])]));

			print '
			<div class="success-box" role="alert">
				<p class="font-bold">Success</p>
				<p>Address '.htmlspecialchars($address).' ('.$types_arr[$type]['name'].' type) was added to organizer <a href="/@'.$organizer_url.'/">'.htmlspecialchars($organizer_arr['title']).'</a> as '.$organizer_addresses_status_arr[$status].'.</p>
			</div>';
		}
		else{
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>'.implode('<br>',$errors).'</p>
			</div>';
		}
	}
	$new_address_type=1;//owner by default
	if(0!=$db->table_count('organizer_addresses',"WHERE `organizer`='".$organizer_id."'")){
		print '<div class="flex flex-col">
		<div class="table-wrapper">
		<div class="py-2 inline-block min-w-full">
		<div class="overflow-hidden">';
		print '<table class="table-auto min-w-full">';
		print '<thead class="bg-white border-b">';
		print '<tr>';
		print '<th>Action</th>';
		print '<th>Type</th>';
		print '<th>Address</th>';
		print '<th>Caption</th>';
		print '<th>Status</th>';
		print '</tr>';
		print '</thead>';
		print '<tbody>';
		$organizer_addresses_arr=$db->sql("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."' ORDER BY `id` ASC");
		foreach($organizer_addresses_arr as $k=>$v){
			print '<tr>';
			print '<td>';
			if(1==$allow_manage){//owner permissions
				if(0==$v['status']){//not owner
					print '<a href="/@'.$organizer_url.'/manage/administration/make_owner/'.$v['id'].'/?'.gen_csrf_param().'" class="toggle-btn mr-4 mb-2">Make owner</a>';
					print '<a href="/@'.$organizer_url.'/manage/administration/delete_address/'.$v['id'].'/?'.gen_csrf_param().'" class="red-btn">Delete</a>';
				}
				else{
					print '&mdash;';
				}
			}
			else{
				print '&mdash;';
			}
			print '</td>';
			$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$v['address']."'");
			print '<td>'.htmlspecialchars($types_arr[$address_arr['type']]['name']).'</td>';
			print '<td>'.htmlspecialchars($address_arr['address']).'</td>';
			print '<td>'.htmlspecialchars($v['caption']).'</td>';
			print '<td class="'.$organizer_addresses_status_arr_class[$v['status']].'">'.$organizer_addresses_status_arr[$v['status']].'</td>';
			print '</tr>';
			if(1==$v['status']){//owner founded
				$new_address_type=0;//admin
			}
		}
		print '</tbody>';
		print '</table>';
		print '</div>';
		print '</div>';
		print '</div>';
		if(1==$new_address_type){
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Attention</p>
				<p>Organizer has no owner address, please add someone</p>
			</div>';
		}
	}
	else{
		print '<div class="attention-box" role="alert">
			<p class="font-bold">Attention</p>
			<p>Organizer has no administrators addresses, please add someone</p>
		</div>';
	}
	if(1==$allow_manage){//owner actions
		print '<form action="?" method="POST" class="manage-card">';
		print '<div class="max-w-fit">';
		print '<p class="font-bold">Add address to administration</p>';
		$object_scheme=[//organizer add address to administration
			'type'=>['type'=>'select','title'=>'Address type','options'=>$types_arr,'caption_template'=>'{id} ({name})','required'=>true],
			'address'=>['type'=>'text','title'=>'Wallet address','required'=>true],
			'caption'=>['type'=>'text','title'=>'Caption','placeholder'=>'Caption (visible memo)'],
			'status'=>['type'=>'select','title'=>'Status','options'=>$organizer_addresses_status_arr,'classes'=>$organizer_addresses_status_arr_class,'default_option'=>$new_address_type,'required'=>true],
		];
		print build_form($object_scheme,false);
		print '</div>';
		print '<div class="grid-wrapper">';
		print '<input type="submit" name="add_address" value="Add administrator" class="action-btn big">';
		print '</div>';
		print '</form>';
	}
}
elseif('events'==$path_array[3]){
	$replace['title']='Events | '.$replace['title'];
	if('upload_content'==$path_array[4]){
		ob_end_clean();
		ob_end_clean();
		$dir='/files/organizers/'.$organizer_arr['id'].'/events/';
		if(!file_exists($root_dir.$dir)){
			mkdir($root_dir.$dir,0777,true);
		}
		$errors=[];
		$files=[];
		$upload_name='upload';
		if(isset($_FILES[$upload_name]) && 0==$_FILES[$upload_name]['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png'];
			$ext=pathinfo($_FILES[$upload_name]['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES[$upload_name]['size']<=1024*1024*2){//check size 2MB
					$upload=md5($_FILES[$upload_name]['name']).'_'.time().'.'.$ext;
					$filename=$dir.$upload;
					move_uploaded_file($_FILES[$upload_name]['tmp_name'],$root_dir.$filename);
					http_response_code(200);
					print json_encode(['url'=>$filename]);

					$files[]=['time'=>time(),'name'=>$_FILES[$upload_name]['name'],'size'=>$_FILES[$upload_name]['size'],'type'=>$_FILES[$upload_name]['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2,'target_id'=>0];//target=event
					foreach($files as $file){
						$db->sql("INSERT INTO `organizer_files` SET `organizer`='".$organizer_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$file['target_id']."',`address`='".$allow_manage_address_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					}
				}
				else{
					$errors[]='Image size must be less than 2MB';
				}
			}
			else{
				$errors[]='Image must be in JPG, JPEG or PNG format';
			}
		}
		else{
			if($limit_upload){
				$errors[]='Image not uploaded, limit reached, please subscribe to paid plan';
			}
			else{
				$errors[]='Image not uploaded';
			}
		}
		if(count($errors)){
			http_response_code(400);
			print json_encode(['error'=>['message'=>implode("\n",$errors)]]);
		}
		exit;
	}
	else
	if('create'==$path_array[4]){
		//new wizard workflow: create event, setup step by step https://gitlab.com/envelop/tickets/getpass/-/issues/8
		$replace['title']='Create event | '.$replace['title'];
		print '<a class="reverse-btn default-button" href="/@'.htmlspecialchars($organizer_url).'/manage/events/">&larr; Back to events</a>';
		print '<h2>Wizard: Create event</h2>';
		if(isset($_POST['url'])){
			$errors=[];
			$files=[];

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			$upload_max_size=ini_get('upload_max_filesize');
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			$url=trim($_POST['url']);
			$url=mb_strtolower($url);
			$url=str_replace(' ','-',$url);
			$url=str_replace('/','',$url);
			$url=ru2lat($url);
			$url=preg_replace('/[^a-z0-9\-\_\.]/','',$url);
			$url=preg_replace('/\-+/','-',$url);
			$url=preg_replace('/\.+/','.',$url);
			if(in_array($url,$reserved_urls)){
				$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
			}
			//check if url is unique
			$find_event_url=$db->sql_row("SELECT * FROM `events` WHERE `organizer`='".$organizer_id."' AND `url`='".$db->prepare($url)."'");
			if(null!=$find_event_url){
				$errors[]='Event URL is not unique';
			}

			$title=trim($_POST['title']);
			$short_description=trim($_POST['short_description']);
			$description=trim($_POST['description']);
			$short_location_description=trim($_POST['short_location_description']);
			$location_description=trim($_POST['location_description']);
			$speakers_description=trim($_POST['speakers_description']);
			$tickets_description=trim($_POST['tickets_description']);
			$check_in_message=trim($_POST['check_in_message']);

			$logo_url='';
			$cover_url='';
			$speakers_cover_url='';
			$location_cover_url='';
			$partners_cover_url='';

			$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$url.'/';
			if(!file_exists($root_dir.$dir)){
				mkdir($root_dir.$dir,0777,true);
			}
			if(isset($_FILES['logo']) && 0==$_FILES['logo']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['logo']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['logo']['size']<=1024*1024*0.5){//check size 500Kb
						$logo=md5($url).'_logo_'.time().'.'.$ext;
						$filename=$dir.$logo;
						move_uploaded_file($_FILES['logo']['tmp_name'],$root_dir.$filename);
						$logo_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>$_FILES['logo']['size'],'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Logo size must be less than 500Kb';
					}
				}
				else{
					$errors[]='&mdash; Logo must be in JPG, JPEG, PNG or SVG format';
				}
			}
			if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
						$cover=md5($url).'_cover_'.time().'.'.$ext;
						$filename=$dir.$cover;
						move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
						$cover_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Cover must be in JPG, JPEG, PNG or SVG format';
				}
			}
			if(isset($_FILES['speakers_cover']) && 0==$_FILES['speakers_cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['speakers_cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['speakers_cover']['size']<=1024*1024*2){//check size 2MB
						$speakers_cover=md5($url).'_speakers_cover_'.time().'.'.$ext;
						$filename=$dir.$speakers_cover;
						move_uploaded_file($_FILES['speakers_cover']['tmp_name'],$root_dir.$filename);
						$speakers_cover_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['speakers_cover']['name'],'size'=>$_FILES['speakers_cover']['size'],'type'=>$_FILES['speakers_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Speakers cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Speakers cover must be in JPG, JPEG, PNG or SVG format';
				}
			}
			if(isset($_FILES['location_cover']) && 0==$_FILES['location_cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['location_cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['location_cover']['size']<=1024*1024*2){//check size 2MB
						$location_cover=md5($url).'_location_cover_'.time().'.'.$ext;
						$filename=$dir.$location_cover;
						move_uploaded_file($_FILES['location_cover']['tmp_name'],$root_dir.$filename);
						$location_cover_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['location_cover']['name'],'size'=>$_FILES['location_cover']['size'],'type'=>$_FILES['location_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Location cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Location cover must be in JPG, JPEG, PNG or SVG format';
				}
			}
			if(isset($_FILES['partners_cover']) && 0==$_FILES['partners_cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['partners_cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['partners_cover']['size']<=1024*1024*2){//check size 2MB
						$partners_cover=md5($url).'_partners_cover_'.time().'.'.$ext;
						$filename=$dir.$partners_cover;
						move_uploaded_file($_FILES['partners_cover']['tmp_name'],$root_dir.$filename);
						$partners_cover_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['partners_cover']['name'],'size'=>$_FILES['partners_cover']['size'],'type'=>$_FILES['partners_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Partners cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Partners cover must be in JPG, JPEG, PNG or SVG format';
				}
			}

			$time=0;
			if(isset($_POST['unixtime'])){
				$time=(int)$_POST['unixtime'];
			}
			//https://gitlab.com/envelop/tickets/getpass/-/issues/20#note_1395411570
			if($time<=(time()+3600-180)){//1 hour (minus 3 minutes for safety) from now
				$errors[]='&mdash; Event time must be at least 1 hour in the future';
			}
			$status=0;
			if(isset($events_status_arr[$_POST['status']])){
				$status=(int)$_POST['status'];
			}
			$moderation=0;
			if(false===$config['event_moderation']){//if moderation is disabled
				$moderation=1;//auto approve
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `events` (`organizer`,`url`,`title`,`short_description`,`description`,`short_location_description`,`location_description`,`speakers_description`,`tickets_description`,`check_in_message`,`logo_url`,`cover_url`,`speakers_cover_url`,`location_cover_url`,`partners_cover_url`,`time`,`status`,`moderation`) VALUES ('".$organizer_id."','".$db->prepare($url)."','".$db->prepare($title)."','".$db->prepare($short_description)."','".$db->prepare($description)."','".$db->prepare($short_location_description)."','".$db->prepare($location_description)."','".$db->prepare($speakers_description)."','".$db->prepare($tickets_description)."','".$db->prepare($check_in_message)."','".$db->prepare($logo_url)."','".$db->prepare($cover_url)."','".$db->prepare($speakers_cover_url)."','".$db->prepare($location_cover_url)."','".$db->prepare($partners_cover_url)."','".$time."','".$status."','".$moderation."')");
				$event_id=$db->last_id();

				//add files to database
				foreach($files as $file){
					//fix https://gitlab.com/envelop/tickets/getpass/-/issues/4#note_1398988737 (file assigned to event now)
					//$db->sql("INSERT INTO `organizer_files` SET `organizer`='".$organizer_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$event_id."',`address`='".$allow_manage_address_id."'");
					$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."',`event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$event_id."',`address`='".$allow_manage_address_id."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
				}

				//get all organizer administators and send them notification about new event
				$org_administation=$db->sql("SELECT `address` FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."'");
				foreach($org_administation as $org_administator){
					add_notify($org_administator['address'],0,1,'org_new_event',json_encode([
						'organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url']),
						'event_title'=>htmlspecialchars($title),
						'event_url'=>htmlspecialchars($url)
					]));
				}

				/* disable notifications about new events, they created on moderation request * /
				//auto inform platform admins about new event
				if(!$config['event_moderation']){//if moderation is disabled
					//get all admins and send them notification about new event
					$platform_admins=$db->sql("SELECT `id` FROM `addresses` WHERE `status`='1'");
					foreach($platform_admins as $platform_admin){
						add_notify($platform_admin['id'],0,1,'admin_new_event',json_encode([
							'organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url']),
							'event_title'=>htmlspecialchars($title),
							'event_url'=>htmlspecialchars($url)
						]));
					}
				}
				//not inform platform admins about new event, wait for moderation request by organizer
				if($config['event_moderation']){//if moderation is enabled
					//get all admins and send them notification about new event
					$platform_admins=$db->sql("SELECT `id` FROM `addresses` WHERE `status`='1'");
					foreach($platform_admins as $platform_admin){
						add_notify($platform_admin['id'],0,1,'admin_new_event',json_encode([
							'organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url']),
							'event_title'=>htmlspecialchars($title),
							'event_url'=>htmlspecialchars($url)
						]));
					}
				}
				/**/

				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Event <a href="/@'.htmlspecialchars($organizer_url).'/manage/events/edit/'.$event_id.'/">'.htmlspecialchars($url).'</a> was created for organizer <a href="/@'.htmlspecialchars($organizer_url).'/manage/">'.htmlspecialchars($organizer_arr['url']).'</a></p>
					<p>Please continue to <a href="/@'.htmlspecialchars($organizer_url).'/'.htmlspecialchars($url).'/manage/wizard/">event wizard</a></p>
				</div>';
				//print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/manage/events/">';
				//redirecto to wizard
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.htmlspecialchars($url).'/manage/wizard/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

				//deleting temporarily uploaded files
				foreach($files as $file){
					unlink($file['path']);
				}
			}
		}
		else{
			print '<p class="wizard-description">On this step, you will create the description of your event. You will provide details such as the event name, date, time, location, and a brief overview of what the event is about. You can also add any additional information or instructions for attendees. This step sets the foundation for your event and helps you establish the basic details that will be used throughout the event creation process.</p>';
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="return check_manage_form(this);">';
			print '<div class="max-w-fit">';
			$object_scheme=[//org create event
				'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/<b>URL</b>/ page address bar.','required'=>true],
				'title'=>['type'=>'text','title'=>'Title','class'=>'title-part','required'=>true],
				'short_description'=>['type'=>'textarea','title'=>'Short description','descr'=>'Annotation for preview & seo'],

				'logo'=>[
					'type'=>'image',
					'title'=>'🖼️ Logo image',
					'placeholder'=>'Logo',
					'object_type'=>'event',
					'file_path'=>'logo_url',
					'descr'=>'The recommended sizes are <b>300x60</b> pixels, 500Kb, please do not exceed them.<br>For best quality use <b>svg</b> or <b>png</b> with transparent background.'
				],

				'collapse_main_part'=>['type'=>'start-collapse','title'=>'Main section'],
				'cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Cover image',
					'placeholder'=>'Cover',
					'object_type'=>'event',
					'file_path'=>'cover_url',
					'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
				],
				'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'Description (annotation)','wysiwyg'=>true],
				['type'=>'end-collapse'],

				'collapse_location_part'=>['type'=>'start-collapse','title'=>'Location section'],
				'short_location_description'=>['type'=>'text','title'=>'Short location description','descr'=>'For event preview card'],
				'location_cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Location cover image',
					'placeholder'=>'Location cover',
					'object_type'=>'event',
					'file_path'=>'location_cover_url',
					'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
				],
				'location_description'=>['type'=>'textarea','title'=>'📄 Location description','placeholder'=>'Location description','wysiwyg'=>true],
				['type'=>'end-collapse'],

				'collapse_speakers_part'=>['type'=>'start-collapse','title'=>'Speakers section'],
				'speakers_cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Speakers cover image',
					'placeholder'=>'Speakers cover',
					'object_type'=>'event',
					'file_path'=>'speakers_cover_url',
					'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
				],
				'speakers_description'=>['type'=>'textarea','title'=>'📄 Speakers description','placeholder'=>'Speakers description','wysiwyg'=>true],
				['type'=>'end-collapse'],

				'collapse_tickets_part'=>['type'=>'start-collapse','title'=>'Tickets section'],
				'tickets_description'=>['type'=>'textarea','title'=>'📄 Tickets description','placeholder'=>'Tickets description','wysiwyg'=>true],
				'check_in_message'=>['type'=>'textarea','title'=>'📄 Check-in message','placeholder'=>'Check-in message','wysiwyg'=>true],
				['type'=>'end-collapse'],

				'collapse_partners_part'=>['type'=>'start-collapse','title'=>'Partners section'],
				'partners_cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Partners cover image',
					'placeholder'=>'Partners cover',
					'object_type'=>'event',
					'file_path'=>'partners_cover_url',
					'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
				],
				['type'=>'end-collapse'],

				['type'=>'hr'],
				'datetime'=>[
					'key'=>'time',
					'type'=>'datetime',
					'local'=>true,//-local
					'future'=>true,
					'minimal-offset'=>(3600),//1 hour
					'title'=>'Start date time (by your local time)',
					'timestamp'=>'unixtime',
					'value'=>0,
				],
				'status'=>[
					'type'=>'select',
					'title'=>'Status',
					'options'=>$events_status_arr,
					'default_option'=>0,
					'classes'=>$events_status_style_arr,
				],
			];
			print build_form($object_scheme,false);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create event" class="action-btn big">';
			print '</div>';
			print '<p class="wizard-description">After creating event you can set up agenda, add speakers, partners, etc.</p>';
			print '<p class="wizard-description">You will be redirected to next wizard step for customizing event.</p>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[4]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/events/">&larr; Back to events</a>';
		print '<h2>Edit event</h2>';
		$id=(int)$path_array[5];
		if($id){
			$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$id."' AND `organizer`='".$organizer_id."'");
			if(null!=$event_arr){
				//edit event was deleted from organizer admin panel
				//https://gitlab.com/envelop/tickets/getpass-land/-/issues/21
				header('location: /@'.$organizer_url.'/'.$event_arr['url'].'/manage/event/');
				exit;
				/*
				if(isset($_POST['url'])){
					$errors=[];
					$files=[];
					$remove_files=[];

					//https://www.php.net/manual/ru/features.file-upload.errors.php
					$upload_max_size=ini_get('upload_max_filesize');
					foreach($_FILES as $file_key=>$file_data){
						switch($file_data['error']){
							case UPLOAD_ERR_INI_SIZE:
								$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
								break;
							case UPLOAD_ERR_FORM_SIZE:
								$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
								break;
						}
					}

					$url=trim($_POST['url']);
					$url=mb_strtolower($url);
					$url=str_replace(' ','-',$url);
					$url=str_replace('/','',$url);
					$url=ru2lat($url);
					$url=preg_replace('/[^a-z0-9\-\_\.]/','',$url);
					$url=preg_replace('/\-+/','-',$url);
					$url=preg_replace('/\.+/','.',$url);
					if(in_array($url,$reserved_urls)){
						$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
					}
					//check if url is unique
					$find_event_url=$db->sql_row("SELECT * FROM `events` WHERE `organizer`='".$organizer_id."' AND `url`='".$db->prepare($url)."' AND `id`!='".$id."'");
					if(null!=$find_event_url){
						$errors[]='Event URL is not unique';
					}

					$title=trim($_POST['title']);
					$short_description=trim($_POST['short_description']);
					$description=trim($_POST['description']);
					$short_location_description=trim($_POST['short_location_description']);
					$location_description=trim($_POST['location_description']);
					$speakers_description=trim($_POST['speakers_description']);
					$tickets_description=trim($_POST['tickets_description']);
					$check_in_message=trim($_POST['check_in_message']);

					$logo_url=$event_arr['logo_url'];
					$cover_url=$event_arr['cover_url'];
					$speakers_cover_url=$event_arr['speakers_cover_url'];
					$location_cover_url=$event_arr['location_cover_url'];
					$partners_cover_url=$event_arr['partners_cover_url'];

					$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$url.'/';
					if(isset($_FILES['logo']) && 0==$_FILES['logo']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['logo']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['logo']['size']<=1024*1024*0.5){//check size 500Kb
								$logo=md5($url).'_logo_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename=$dir.$logo;
								move_uploaded_file($_FILES['logo']['tmp_name'],$root_dir.$filename);
								$logo_url=$filename;

								$remove_files[]=$event_arr['logo_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>$_FILES['logo']['size'],'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Logo size must be less than 500Kb';
							}
						}
						else{
							$errors[]='&mdash; Logo must be in JPG, JPEG, PNG or SVG format';
						}
					}
					if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
								$cover=md5($url).'_cover_'.time().'.'.$ext;
								$filename='/files/events/'.$cover;
								move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
								$cover_url=$filename;

								$remove_files[]=$event_arr['cover_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Cover size must be less than 2MB';
							}
						}
						else{
							$errors[]='&mdash; Cover must be in JPG, JPEG, PNG or SVG format';
						}
					}
					if(isset($_FILES['speakers_cover']) && 0==$_FILES['speakers_cover']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['speakers_cover']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['speakers_cover']['size']<=1024*1024*2){//check size 2MB
								$speakers_cover=md5($url).'_speakers_cover_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename=$dir.$speakers_cover;
								move_uploaded_file($_FILES['speakers_cover']['tmp_name'],$root_dir.$filename);
								$speakers_cover_url=$filename;

								$remove_files[]=$event_arr['speakers_cover_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['speakers_cover']['name'],'size'=>$_FILES['speakers_cover']['size'],'type'=>$_FILES['speakers_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Speakers cover size must be less than 2MB';
							}
						}
						else{
							$errors[]='&mdash; Speakers cover must be in JPG, JPEG, PNG or SVG format';
						}
					}
					if(isset($_FILES['location_cover']) && 0==$_FILES['location_cover']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['location_cover']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['location_cover']['size']<=1024*1024*2){//check size 2MB
								$location_cover=md5($url).'_location_cover_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename=$dir.$location_cover;
								move_uploaded_file($_FILES['location_cover']['tmp_name'],$root_dir.$filename);
								$location_cover_url=$filename;

								$remove_files[]=$event_arr['location_cover_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['location_cover']['name'],'size'=>$_FILES['location_cover']['size'],'type'=>$_FILES['location_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Location cover size must be less than 2MB';
							}
						}
						else{
							$errors[]='&mdash; Location cover must be in JPG, JPEG, PNG or SVG format';
						}
					}
					if(isset($_FILES['partners_cover']) && 0==$_FILES['partners_cover']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['partners_cover']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['partners_cover']['size']<=1024*1024*2){//check size 2MB
								$partners_cover=md5($url).'_partners_cover_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename=$dir.$partners_cover;
								move_uploaded_file($_FILES['partners_cover']['tmp_name'],$root_dir.$filename);
								$partners_cover_url=$filename;

								$remove_files[]=$event_arr['partners_cover_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['partners_cover']['name'],'size'=>$_FILES['partners_cover']['size'],'type'=>$_FILES['partners_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Partners cover size must be less than 2MB';
							}
						}
						else{
							$errors[]='&mdash; Partners cover must be in JPG, JPEG, PNG or SVG format';
						}
					}

					$time=0;
					if(isset($_POST['unixtime'])){
						$time=(int)$_POST['unixtime'];
					}
					$status=0;
					if(isset($events_status_arr[$_POST['status']])){
						$status=(int)$_POST['status'];
					}

					if(0==count($errors)){
						$db->sql("UPDATE `events` SET `url`='".$db->prepare($url)."',`title`='".$db->prepare($title)."',`short_description`='".$db->prepare($short_description)."',`description`='".$db->prepare($description)."',`short_location_description`='".$db->prepare($short_location_description)."',`location_description`='".$db->prepare($location_description)."',`speakers_description`='".$db->prepare($speakers_description)."',`speakers_description`='".$db->prepare($speakers_description)."',`tickets_description`='".$db->prepare($tickets_description)."',`check_in_message`='".$db->prepare($check_in_message)."',`time`='".$time."',`status`='".$status."',`logo_url`='".$db->prepare($logo_url)."',`cover_url`='".$db->prepare($cover_url)."',`speakers_cover_url`='".$db->prepare($speakers_cover_url)."',`location_cover_url`='".$db->prepare($location_cover_url)."',`partners_cover_url`='".$db->prepare($partners_cover_url)."' WHERE `id`='".$id."'");
						print '
						<div class="success-box" role="alert">
							<p class="font-bold">Success</p>
							<p>Event <a href="/@'.htmlspecialchars($organizer_url).'/manage/events/edit/'.$id.'/">"'.htmlspecialchars($url).'"</a> was updated</p>
						</div>';

						print '<meta http-equiv="refresh" content="5;url=/@'.$organizer_url.'/manage/events/">';

						//deleting old files
						foreach($remove_files as $file){
							unlink($root_dir.$file);
							$db->sql("DELETE FROM `organizer_files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
						}
						//add files to database
						foreach($files as $file){
							$db->sql("INSERT INTO `organizer_files` SET `organizer`='".$organizer_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$id."',`address`='".$allow_manage_address_id."'");
						}
					}
					else{
						print '
						<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>'.implode('<br>',$errors).'</p>
						</div>';
						print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

						//deleting temporarily uploaded files
						foreach($files as $file){
							unlink($file['path']);
						}
					}
				}
				else{
					print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
					print '<div class="max-w-fit">';
					$object_scheme=[//org edit event
						'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/<b>URL</b>/ page address bar.','required'=>true],
						'title'=>['type'=>'text','title'=>'Title','class'=>'title-part','required'=>true],
						'short_description'=>['type'=>'textarea','title'=>'Short description','descr'=>'Annotation for preview & seo'],

						'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'Description (annotation)','wysiwyg'=>true],

						'short_location_description'=>['type'=>'text','title'=>'Short location description','descr'=>'For event preview card'],
						'location_description'=>['type'=>'textarea','title'=>'📄 Location description','placeholder'=>'Location description','wysiwyg'=>true],

						'speakers_description'=>['type'=>'textarea','title'=>'📄 Speakers description','placeholder'=>'Speakers description','wysiwyg'=>true],
						'tickets_description'=>['type'=>'textarea','title'=>'📄 Tickets description','placeholder'=>'Tickets description','wysiwyg'=>true],
						'check_in_message'=>['type'=>'textarea','title'=>'📄 Check-in message','placeholder'=>'Check-in message','wysiwyg'=>true],

						'logo'=>[
							'type'=>'image',
							'title'=>'🖼️ Logo image',
							'placeholder'=>'Logo',
							'object_type'=>'event',
							'file_path'=>'logo_url',
							'descr'=>'The recommended sizes are <b>300x60</b> pixels, 500Kb, please do not exceed them.<br>For best quality use <b>svg</b> or <b>png</b> with transparent background.'
						],
						'sep1'=>['type'=>'hr'],
						'cover'=>[
							'type'=>'image',
							'title'=>'🖼️ Cover image',
							'placeholder'=>'Cover',
							'object_type'=>'event',
							'file_path'=>'cover_url',
							'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
						],
						'speakers_cover'=>[
							'type'=>'image',
							'title'=>'🖼️ Speakers cover image',
							'placeholder'=>'Speakers cover',
							'object_type'=>'event',
							'file_path'=>'speakers_cover_url',
							'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
						],
						'location_cover'=>[
							'type'=>'image',
							'title'=>'🖼️ Location cover image',
							'placeholder'=>'Location cover',
							'object_type'=>'event',
							'file_path'=>'location_cover_url',
							'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
						],
						'partners_cover'=>[
							'type'=>'image',
							'title'=>'🖼️ Partners cover image',
							'placeholder'=>'Partners cover',
							'object_type'=>'event',
							'file_path'=>'partners_cover_url',
							'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
						],
						'sep2'=>['type'=>'hr'],
						'datetime'=>[
							'key'=>'time',
							'type'=>'datetime',
							'local'=>true,//-local
							'title'=>'Start date time (by your local time)',
							'timestamp'=>'unixtime',
							'value'=>0
						],
						'status'=>[
							'type'=>'select',
							'title'=>'Status',
							'options'=>$events_status_arr,
							'default_option'=>0,
							'classes'=>$events_status_style_arr,
						],
					];
					print build_form($object_scheme,$event_arr);
					print '</div>';
					print '<input type="submit" value="Update event" class="action-btn big">';
					print '</form>';
				}
				*/
			}
			else{
				print '<h2 class="text-red-600">Event not found</h2>';
				print '<p>Return to events list and try again.</p>';
			}
		}
		else{
			print '<h2 class="text-red-600">Event not found</h2>';
			print '<p>Return to events list and try again.</p>';
		}
	}
	else{
		print '<div class="my-4">';
		print '<a href="/@'.$organizer_url.'/manage/events/create/" class="action-btn">Create event</a>';
		print '</div>';
		if(isset($tab_descr_arr[$path_array[3]])){
			print '<p>'.ltmp($tab_descr_arr[$path_array[3]],$ltml_tab_descr_arr).'</p>';
		}
		$filter_status=false;
		if(isset($_GET['status'])){
			$filter_status=intval($_GET['status']);
			if(''==$_GET['status']){
				$filter_status=false;
			}
		}
		print '<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">';
				print '<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
				foreach($events_status_arr as $status_id=>$status_name){
					print '<option value="'.$status_id.'"'.($filter_status===$status_id?' selected':'').' class="'.$events_status_style_arr[$status_id].'">'.$status_name.'</option>';
				}
			print '</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Action</th>';

			print '<th>URL</th>';
			print '<th>Title</th>';

			print '<th>Description</th>';
			print '<th>Location descr.</th>';
			print '<th>Speakers descr.</th>';

			print '<th>Start</th>';

			print '<th>Status</th>';
			if($config['event_moderation']){
				print '<th>Moderation</td>';
			}
			print '<th>Sessions</th>';
			print '<th>Files size</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]="`organizer`='".$organizer_id."'";
		if(false!==$filter_status){
			$sql_addon[]="`status`='$filter_status'";
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}

		$per_page=50;
		$count=$db->table_count('events',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$events=$db->sql("SELECT * FROM `events`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($events as $event){
			print '<tr>';
			print '<td>';
			print '<a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event['url']).'/manage/" class="violet-btn mr-4 mb-2">Manage</a>';
			print '</td>';

			print '<td class="whitespace-nowrap">
				<a href="/@'.$organizer_url.'/'.htmlspecialchars($event['url']).'/" target="_blank">'.htmlspecialchars($event['url']).'</a>
				<a class="admin-copy-icon" data-text="'.$config['platform_url'].'/@'.$organizer_url.'/'.htmlspecialchars($event['url']).'">⧉</a>
			</td>';
			print '<td>'.htmlspecialchars($event['title']).'</td>';

			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($event['description']).'</div></td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($event['location_description']).'</div></td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($event['speakers_description']).'</div></td>';

			if($event['time']){
				print '<td><span class="time" data-time="'.$event['time'].'">'.date('d.m.Y H:i',$event['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}

			print '<td class="'.$events_status_style_arr[$event['status']].'">'.htmlspecialchars($events_status_arr[$event['status']]).'</td>';
			if($config['event_moderation']){
				print '<td class="'.$events_moderation_style_arr[$event['moderation']].'">'.htmlspecialchars($events_moderation_arr[$event['moderation']]).'</td>';
			}
			$sessions_count=$db->table_count('event_sessions',"WHERE `event`='".$event['id']."'");
			print '<td><a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.$event['url'].'/manage/sessions/">'.$sessions_count.'</a></td>';

			print '<td class="whitespace-nowrap">'.human_readable_filesize($event['summary_files_size']).'</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('delete_file'==$path_array[3]){
	ob_end_clean();
	ob_end_clean();
	$object_type=$path_array[4];
	$object_id=(int)$path_array[5];
	if('event'==$object_type){
		if(check_csrf()){
			$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$object_id."' AND `organizer`='".$organizer_arr['id']."'");
			if(null!==$event_arr){
				$file_type=$db->prepare($path_array[6]);

				$event_files_arr=array('logo_url','cover_url','speakers_cover_url','location_cover_url','partners_cover_url');

				$result=['status'=>false];
				if(in_array($file_type,$event_files_arr)){
					if(file_exists($root_dir.$event_arr[$file_type])){
						$free_file_size=filesize($root_dir.$event_arr[$file_type]);
						unlink($root_dir.$event_arr[$file_type]);
						$db->sql("DELETE FROM `organizer_files` WHERE `path`='".$db->prepare($root_dir.$event_arr[$file_type])."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					$db->sql("UPDATE `events` SET `".$file_type."`='' WHERE `id`='".$object_id."'");
					$result=['status'=>true,'message'=>'✓ File successfully deleted'];
				}
				if($result['status']){
					print json_encode($result);
					exit;
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	if('content'==$object_type){
		if(check_csrf()){
			$content_arr=$db->sql_row("SELECT * FROM `organizer_content` WHERE `id`='".$object_id."' AND `organizer`='".$organizer_arr['id']."'");
			if(null!==$content_arr){
				$result=['status'=>false];
				$object_key=$db->prepare($path_array[6]);
				if(isset($content_arr[$object_key])){
					if($content_arr[$object_key]){
						if(file_exists($root_dir.$content_arr[$object_key])){
							$free_file_size=filesize($root_dir.$content_arr[$object_key]);
							unlink($root_dir.$content_arr[$object_key]);
							$db->sql("DELETE FROM `organizer_files` WHERE `path`='".$db->prepare($root_dir.$content_arr[$object_key])."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
						$db->sql("UPDATE `organizer_content` SET `".$object_key."`='' WHERE `id`='".$object_id."'");
						$result=['status'=>true,'message'=>'✓ File successfully deleted'];
					}
				}
				$search_thumbnail_key='thumbnail_'.$object_key;
				if(isset($content_arr[$search_thumbnail_key])){
					if($content_arr[$search_thumbnail_key]){
						if(file_exists($root_dir.$content_arr[$search_thumbnail_key])){
							$free_file_size=filesize($root_dir.$content_arr[$search_thumbnail_key]);
							unlink($root_dir.$content_arr[$search_thumbnail_key]);
							$db->sql("DELETE FROM `organizer_files` WHERE `path`='".$db->prepare($root_dir.$content_arr[$search_thumbnail_key])."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
						$db->sql("UPDATE `organizer_content` SET `".$search_thumbnail_key."`='' WHERE `id`='".$object_id."'");
						$result=['status'=>true,'message'=>'✓ File & thumbnail successfully deleted'];
					}
				}
				if($result['status']){
					print json_encode($result);
					exit;
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	header('HTTP/1.0 404 Not Found');
	exit;
}
elseif('content'==$path_array[3]){
	$replace['title']='Content | '.$replace['title'];
	$script_preset.='var wysiwyg_toolbar_remove=["heading1"];';
	if('upload_content'==$path_array[4]){
		ob_end_clean();
		ob_end_clean();
		$dir='/files/organizers/'.$organizer_arr['id'].'/content/';
		if(!file_exists($root_dir.$dir)){
			mkdir($root_dir.$dir,0777,true);
		}
		$errors=[];
		$files=[];
		$upload_name='upload';
		if(isset($_FILES[$upload_name]) && 0==$_FILES[$upload_name]['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png'];
			$ext=pathinfo($_FILES[$upload_name]['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES[$upload_name]['size']<=1024*1024*2){//check size 2MB
					$upload=md5($_FILES[$upload_name]['name']).'_'.time().'.'.$ext;
					$filename=$dir.$upload;
					move_uploaded_file($_FILES[$upload_name]['tmp_name'],$root_dir.$filename);
					http_response_code(200);
					print json_encode(['url'=>$filename]);

					$files[]=['time'=>time(),'name'=>$_FILES[$upload_name]['name'],'size'=>$_FILES[$upload_name]['size'],'type'=>$_FILES[$upload_name]['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1,'target_id'=>0];//target=content
					foreach($files as $file){
						$db->sql("INSERT INTO `organizer_files` SET `organizer`='".$organizer_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$file['target_id']."',`address`='".$allow_manage_address_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					}
				}
				else{
					$errors[]='Image size must be less than 2MB';
				}
			}
			else{
				$errors[]='Image must be in JPG, JPEG or PNG format';
			}
		}
		else{
			if($limit_upload){
				$errors[]='Image not uploaded, limit reached, please subscribe to paid plan';
			}
			else{
				$errors[]='Image not uploaded';
			}
		}
		if(count($errors)){
			http_response_code(400);
			print json_encode(['error'=>['message'=>implode("\n",$errors)]]);
		}
		exit;
	}
	if('delete'==$path_array[4]){
		$content_id=(int)$path_array[5];
		$content_arr=$db->sql_row("SELECT * FROM `organizer_content` WHERE `organizer`='".$organizer_id."' AND `id`='".$content_id."'");
		if(3==$content_arr['status']){
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content is banned</p>
			</div>';
		}
		else
		if(null!=$content_arr){
			if(isset($_POST['approve'])){
				if(''!=$content_arr['cover_url']){
					if(file_exists($root_dir.$content_arr['cover_url'])){
						$free_file_size=filesize($root_dir.$content_arr['cover_url']);
						unlink($root_dir.$content_arr['cover_url']);
						$db->sql("DELETE FROM `organizer_files` WHERE `path`='".$db->prepare($root_dir.$content_arr['cover_url'])."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
				}
				if(''!=$content_arr['thumbnail_cover_url']){
					if(file_exists($root_dir.$content_arr['thumbnail_cover_url'])){
						$free_file_size=filesize($root_dir.$content_arr['thumbnail_cover_url']);
						unlink($root_dir.$content_arr['thumbnail_cover_url']);
						$db->sql("DELETE FROM `organizer_files` WHERE `path`='".$db->prepare($root_dir.$content_arr['thumbnail_cover_url'])."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
				}
				$db->sql("DELETE FROM `organizer_content` WHERE `id`='".$content_id."'");
				$db->sql("DELETE FROM `organizer_content_tags` WHERE `content`='".$content_id."'");
				header('Location:/@'.$organizer_url.'/manage/content/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/content/">&larr; Back to content list</a>';
				print '<h2 class="text-red-600">Remove content #'.$content_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Content will be removed from the database.</p>
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove content" class="red-btn">';
				print '</form>';

			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/content/">&larr; Back to content list</a>';
			print '<h2 class="text-red-600">Content not found</h2>';
			print '<p>Return to content list and try again.</p>';
		}
	}
	elseif('create'==$path_array[4]){
		unset($content_status_arr[3]);//remove banned status for organizer administrator
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/content/">&larr; Back to content list</a>';
		print '<h2>Create content</h2>';
		if(isset($_POST['url'])){
			$errors=[];
			$files=[];
			$url=trim($_POST['url']);
			$url=mb_strtolower($url);
			$url=preg_replace('/\.+/','.',$url);
			if(0!=$db->table_count('organizer_content',"WHERE `organizer`='".$organizer_id."' AND `url`='".$db->prepare($url)."'")){
				$errors[]='URL "'.htmlspecialchars($url).'" is already in use';
			}
			if(in_array($url,$reserved_urls)){
				$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
			}

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			$upload_max_size=ini_get('upload_max_filesize');
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			$cover_url='';
			$thumbnail_cover_url='';

			$dir='/files/organizers/'.$organizer_arr['id'].'/content/';
			if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png'];
				$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
						$cover=md5($_FILES['cover']['name']).'_'.time().'.'.$ext;
						if(!file_exists($root_dir.$dir)){
							mkdir($root_dir.$dir,0777,true);
						}
						$filename=$dir.$cover;
						move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
						$cover_url=$filename;
						//make small thumbnail 250x250 from cover
						$thumbnail_cover=md5($_FILES['cover']['name']).'_'.time().'_thumbnail.'.$ext;
						$thumbnail_filename=$dir.$thumbnail_cover;
						$thumbnail_cover_url=$thumbnail_filename;
						$thumbnail_cover_path=$root_dir.$thumbnail_filename;
						$thumbnail_cover_obj=new Imagick($root_dir.$filename);
						$thumbnail_cover_obj->thumbnailImage(250,250,true);
						$thumbnail_cover_obj->writeImage($thumbnail_cover_path);

						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1];//target=content
						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>filesize($root_dir.$thumbnail_filename),'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$thumbnail_filename,'relative_path'=>$thumbnail_filename,'target'=>1];//target=content
					}
					else{
						$errors[]='&mdash; Cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Cover must be in JPG, JPEG or PNG format';
				}
			}

			$title=trim($_POST['title']);
			if(''==$title){
				$errors[]='&mdash; Title is required';
			}
			$description=trim($_POST['description']);
			$content=trim($_POST['content']);

			$status=0;
			if($content_status_arr[$_POST['status']]){
				$status=(int)$_POST['status'];
			}
			$publishing_time=(int)$_POST['unixtime'];
			if(0==$status){//if status is planned
				if($publishing_time<time()){//if time is in past
					$publishing_time=0;
					//$errors[]='&mdash; Publishing time must be in future for planned content';
				}
			}
			if(1==$status){//if status is published
				if($publishing_time<time()){//if time is in past
					$publishing_time=time();//set time to now
				}
			}
			$pin=0;
			if(isset($_POST['pin'])){
				if(1==$_POST['pin']){
					$pin=1;
				}
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `organizer_content` (`organizer`,`url`,`title`,
				`cover_url`,`thumbnail_cover_url`,
				`description`,`content`,`status`,`time`,`pin`) VALUES ('".$organizer_id."','".$db->prepare($url)."','".$db->prepare($title)."',
				'".$db->prepare($cover_url)."','".$db->prepare($thumbnail_cover_url)."',
				'".$db->prepare($description)."','".$db->prepare($content)."','".$status."','".$publishing_time."','".$pin."')");

				$content_id=$db->last_id();

				//add files to database
				foreach($files as $file){
					$db->sql("INSERT INTO `organizer_files` SET `organizer`='".$organizer_id."', `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$content_id."',`address`='".$allow_manage_address_id."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
				}

				$db->sql("DELETE FROM `organizer_content_tags` WHERE `organizer`='".$organizer_id."' AND `content`='".$db->prepare($content_id)."'");
				if(isset($_POST['tag'])){
					foreach($_POST['tag'] as $tag_id=>$tag_status){
						if('on'==$tag_status){
							$db->sql("INSERT INTO `organizer_content_tags` (`organizer`,`content`,`tag`) VALUES ('".$organizer_id."','".$db->prepare($content_id)."','".$db->prepare($tag_id)."')");
						}
					}
				}

				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Content <a href="/@'.$organizer_url.'/manage/content/edit/'.$content_id.'/">"'.htmlspecialchars($url).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/manage/content/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

				//deleting temporarily uploaded files
				foreach($files as $file){
					unlink($file['path']);
				}
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
			print '<div class="max-w-fit">';
			$object_scheme=[//org create content
				'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/content/<b>URL</b>/ page address bar.','required'=>true],
				'title'=>['type'=>'text','title'=>'Title','required'=>true],
				'description'=>['type'=>'textarea','title'=>'Description','placeholder'=>'Description (annotation)'],
				'cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Cover image',
					'placeholder'=>'Cover',
					'object_type'=>'content',
					'file_path'=>'cover_url',
					'thumbnail_path'=>'thumbnail_cover_url',
					'descr'=>'The thumbnail will be created automatically with a target 250x250 pixels (1:1 ratio), 2Mb'
				],
				'content'=>['type'=>'textarea','title'=>'📄 Content','placeholder'=>'HTML content','wysiwyg'=>true],
				'sep1'=>['type'=>'hr'],
				'status'=>[
					'type'=>'select',
					'title'=>'Status',
					'options'=>$content_status_arr,
					'default_option'=>0,
					'classes'=>$content_status_style_arr,
				],
				'datetime'=>[
					'type'=>'datetime',
					'local'=>true,//-local
					'future'=>true,
					'title'=>'Publishing time (by your local time)',
					'timestamp'=>'unixtime',
					'value'=>0
				],
				'pin'=>['type'=>'checkbox','title'=>'Pinned content','value'=>1],
				'tag'=>[
					'type'=>'multi-checkbox',
					'title'=>'Tags',
					'options'=>$db->sql('SELECT * FROM `organizer_tags` WHERE `organizer`='.$organizer_id.' ORDER BY `sort` ASC, `url` ASC'),
					'key'=>'id',
					'value'=>'caption',
					'value_function'=>false,
				],
			];
			print build_form($object_scheme,false);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create content" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[4]){
		unset($content_status_arr[3]);//remove banned status for organizer administrator
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/content/">&larr; Back to content list</a>';
		print '<h2>Edit content</h2>';
		$content_id=(int)$path_array[5];
		$content_item=$db->sql_row("SELECT * FROM `organizer_content` WHERE `organizer`='".$organizer_id."' AND `id`='".$content_id."'");
		if(3==$content_item['status']){
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content is banned</p>
			</div>';
			$content_item=null;
		}
		else
		if(null!=$content_item){
			if(isset($_POST['url'])){
				$errors=[];
				$files=[];
				$remove_files=[];

				//https://www.php.net/manual/ru/features.file-upload.errors.php
				$upload_max_size=ini_get('upload_max_filesize');
				foreach($_FILES as $file_key=>$file_data){
					switch($file_data['error']){
						case UPLOAD_ERR_INI_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
							break;
					}
				}

				$cover_url=$content_item['cover_url'];
				$thumbnail_cover_url=$content_item['thumbnail_cover_url'];

				$dir='/files/organizers/'.$organizer_arr['id'].'/content/';
				if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
					//check extensions
					$allowed_exts=['jpg','jpeg','png'];
					$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
					if(in_array($ext,$allowed_exts)){
						if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
							$cover=md5($_FILES['cover']['name']).'_'.time().'.'.$ext;
							if(!file_exists($root_dir.$dir)){
								mkdir($root_dir.$dir,0777,true);
							}
							$filename=$dir.$cover;
							move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
							$cover_url=$filename;
							//make small thumbnail 250x250 from cover
							$thumbnail_cover=md5($_FILES['cover']['name']).'_'.time().'_thumbnail.'.$ext;
							$thumbnail_filename=$dir.$thumbnail_cover;
							$thumbnail_cover_url=$thumbnail_filename;
							$thumbnail_cover_path=$root_dir.$thumbnail_filename;
							$thumbnail_cover_obj=new Imagick($root_dir.$filename);
							$thumbnail_cover_obj->thumbnailImage(250,250,true);
							$thumbnail_cover_obj->writeImage($thumbnail_cover_path);

							$remove_files[]=$content_item['cover_url'];//remove old file
							$remove_files[]=$content_item['thumbnail_cover_url'];//remove old file
							$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1];//target=content
							$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>filesize($root_dir.$thumbnail_filename),'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$thumbnail_filename,'relative_path'=>$thumbnail_filename,'target'=>1];//target=content
						}
						else{
							$errors[]='&mdash; Cover size must be less than 2MB';
						}
					}
					else{
						$errors[]='&mdash; Cover must be in JPG, JPEG or PNG format';
					}
				}

				$url=trim($_POST['url']);
				$url=mb_strtolower($url);
				$url=preg_replace('/\.+/','.',$url);
				if(in_array($url,$reserved_urls)){
					$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
				}
				if(''==$url){
					$errors[]='&mdash; URL is required';
				}
				//check if url is unique
				$check_url=$db->sql_row("SELECT * FROM `organizer_content` WHERE `organizer`='".$organizer_id."' AND `url`='".$url."' AND `id`!='".$content_id."'");
				if(null!=$check_url){
					$errors[]='&mdash; URL is already in use';
				}

				$title=trim($_POST['title']);
				if(''==$title){
					$errors[]='&mdash; Title is required';
				}
				$description=trim($_POST['description']);
				$content=trim($_POST['content']);

				$status=0;
				if($content_status_arr[$_POST['status']]){
					$status=(int)$_POST['status'];
				}
				$publishing_time=(int)$_POST['unixtime'];
				if(0==$status){//if status is planned
					if($publishing_time<time()){//if time is in past
						$publishing_time=0;
						//$errors[]='&mdash; Publishing time must be in future for planned content';
					}
				}
				if(1==$status){//if status is published
					if($publishing_time<time()){//if time is in past
						$publishing_time=time();//set time to now
					}
				}
				$pin=0;
				if(isset($_POST['pin'])){
					if(1==$_POST['pin']){
						$pin=1;
					}
				}

				if(0==count($errors)){
					$db->sql("UPDATE `organizer_content` SET `url`='".$db->prepare($url)."', `title`='".$db->prepare($title)."', `description`='".$db->prepare($description)."', `content`='".$db->prepare($content)."', `cover_url`='".$db->prepare($cover_url)."', `thumbnail_cover_url`='".$db->prepare($thumbnail_cover_url)."', `status`='".$status."', `time`='".$publishing_time."', `pin`='".$pin."' WHERE `id`='".$content_id."'");

					//deleting old files
					foreach($remove_files as $file){
						$free_file_size=filesize($root_dir.$file);
						unlink($root_dir.$file);
						$db->sql("DELETE FROM `organizer_files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					//add files to database
					foreach($files as $file){
						$db->sql("INSERT INTO `organizer_files` SET `organizer`='".$organizer_id."', `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$content_id."',`address`='".$allow_manage_address_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					}

					$db->sql("DELETE FROM `organizer_content_tags` WHERE `content`='".$content_id."'");
					if(isset($_POST['tag'])){
						foreach($_POST['tag'] as $tag_id=>$tag_status){
							if('on'==$tag_status){
								$db->sql("INSERT INTO `organizer_content_tags` (`organizer`,`content`,`tag`) VALUES ('".$organizer_id."','".$db->prepare($content_item['id'])."','".$db->prepare($tag_id)."')");
							}
						}
					}

					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Content <a href="/@'.$organizer_url.'/manage/content/edit/'.$content_item['id'].'/">"'.htmlspecialchars($url).'"</a> was updated</p>
					</div>';

					print '<meta http-equiv="refresh" content="5;url=/@'.$organizer_url.'/manage/content/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

					//deleting temporarily uploaded files
					foreach($files as $file){
						unlink($file['path']);
					}
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
				print '<div class="max-w-fit">';

				$object_scheme=[//org edit content
					'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/content/<b>URL</b>/ page address bar.','required'=>true],
					'title'=>['type'=>'text','title'=>'Title','required'=>true],
					'description'=>['type'=>'textarea','title'=>'Description','placeholder'=>'Description (annotation)'],
					'cover'=>[
						'type'=>'image',
						'title'=>'🖼️ Cover image',
						'placeholder'=>'Cover',
						'object_type'=>'content',
						'file_path'=>'cover_url',
						'thumbnail_path'=>'thumbnail_cover_url',
						'descr'=>'The thumbnail will be created automatically with a target 250x250 pixels (1:1 ratio), 2Mb'
					],
					'content'=>['type'=>'textarea','title'=>'📄 Content','placeholder'=>'HTML content','wysiwyg'=>true],
					'sep1'=>['type'=>'hr'],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$content_status_arr,
						'default_option'=>0,
						'classes'=>$content_status_style_arr,
					],
					'datetime'=>[
						'key'=>'time',
						'type'=>'datetime',
						'local'=>true,//-local
						'future'=>true,
						'title'=>'Publishing time (by your local time)',
						'value_title'=>'Publishing time',
						'timestamp'=>'unixtime',
						'value'=>0
					],
					'pin'=>['type'=>'checkbox','title'=>'Pinned content','value'=>1],
					'tag'=>[
						'type'=>'multi-checkbox',
						'title'=>'Tags',
						'options'=>$db->sql('SELECT * FROM `organizer_tags` WHERE `organizer`='.$organizer_id.' ORDER BY `sort` ASC, `url` ASC'),
						'key'=>'id',
						'value'=>'caption',
						'value_function'=>false,
						'checked_function'=>function($option,$object){
							global $db,$organizer_id;
							return (0<$db->table_count('organizer_content_tags','WHERE `organizer`='.$organizer_id.' AND `tag`='.$option['id'].' AND `content`='.$object['id']));
						},
					],
				];
				print build_form($object_scheme,$content_item);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Update content" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content not found</p>
			</div>';
		}
	}
	else{
		print '<a class="action-btn" href="/@'.$organizer_url.'/manage/content/create/">Create content</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[3]],$ltml_tab_descr_arr).'</p>';

		$filter_tag=false;
		if(isset($_GET['tag'])){
			if(''!==$_GET['tag']){
				$filter_tag=(int)$_GET['tag'];
				if(0==$db->table_count('organizer_tags',"WHERE `organizer`='".$organizer_id."' AND `id`='".$filter_tag."'")){
					$filter_tag=false;
				}
			}
		}

		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($content_status_arr[$filter_status])){
					$filter_status=false;
				}
			}
		}

		$filter_query=false;
		if(isset($_GET['query'])){
			if(''!==$_GET['query']){
				$filter_query=trim($_GET['query']);
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="query" class="form-post-by-enter" placeholder="Search query" value="'.($filter_query?htmlspecialchars($filter_query):'').'">
			</div>
			<div>
				<select name="tag" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_tag?' selected':'').'>Filter by tag</option>';
					$tags=$db->sql('SELECT * FROM `organizer_tags` WHERE `organizer`='.$organizer_id.' ORDER BY `sort` ASC, `url` ASC');
					foreach($tags as $tag){
						print '<option value="'.htmlspecialchars($tag['id']).'"'.($tag['id']==$filter_tag?' selected':'').'>Tag: '.htmlspecialchars($tag['caption']).'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
					foreach($content_status_arr as $content_status_id=>$content_status_caption){
						print '<option value="'.htmlspecialchars($content_status_id).'" class="'.$content_status_style_arr[$content_status_id].'"'.($content_status_id===$filter_status?' selected':'').'>Status: '.htmlspecialchars($content_status_caption).'</option>';
					}
					print '
				</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>URL</th>';
			print '<th>Cover</th>';
			print '<th>Title</th>';
			print '<th>Description</th>';
			print '<th>Content</th>';
			print '<th>Status</th>';
			print '<th>Time</th>';
			print '<th>Pinned</th>';
			print '<th>Tags</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]='`organizer_content`.`organizer`='.$organizer_id;
		if(false!==$filter_status){
			$sql_addon[]='`organizer_content`.`status`='.$filter_status;
		}
		if(false!==$filter_query){
			$sql_addon[]='(`organizer_content`.`url` LIKE \'%'.$filter_query.'%\' OR `organizer_content`.`title` LIKE \'%'.$filter_query.'%\' OR `organizer_content`.`description` LIKE \'%'.$filter_query.'%\')';
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		if(false!==$filter_tag){
			$sql_addon_str=' RIGHT JOIN `organizer_content_tags` ON `organizer_content_tags`.`content`=`organizer_content`.`id` AND `organizer_content_tags`.`tag`='.$filter_tag.' '.$sql_addon_str;
		}
		$per_page=50;
		$count=$db->table_count('organizer_content',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$content=$db->sql("SELECT `organizer_content`.* FROM `organizer_content`".$sql_addon_str." ORDER BY `organizer_content`.`id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($content as $content_item){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/manage/content/delete/'.$content_item['id'].'/" class="red-btn">Delete</a></td>';
			print '<td class="whitespace-nowrap">
				<a href="/@'.$organizer_url.'/manage/content/edit/'.$content_item['id'].'/">'.htmlspecialchars($content_item['url']).'</a>
				<a href="/@'.$organizer_url.'/content/'.htmlspecialchars($content_item['url']).'" target="_blank" class="text-blue-500 !no-underline">⧉</a>
			</td>';

			if($content_item['thumbnail_cover_url']){
				print '<td><img src="'.$content_item['thumbnail_cover_url'].'" class="cover"></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>'.htmlspecialchars($content_item['title']).'</td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($content_item['description']).'</div></td>';

			print '<td>'.((''!=$content_item['content'])||(''!=$content_item['embed_content'])?'✔️':'❌').'</td>';

			print '<td>';
			print '<span class="'.$content_status_style_arr[$content_item['status']].'">';
			print htmlspecialchars($content_status_arr[$content_item['status']]);
			print '</span>';
			print '</td>';
			if($content_item['time']){
				print '<td><span class="time" data-time="'.$content_item['time'].'">'.date('d.m.Y H:i:s',$content_item['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>'.(1==$content_item['pin']?'✔️':'&mdash;').'</td>';//not use ❌ as more negative than &mdash;
			print '<td>';
			$content_tags=$db->sql("SELECT `tag` FROM `organizer_content_tags` WHERE `organizer`='".$organizer_id."' AND `content`=".$content_item['id']);
			foreach($content_tags as $content_tag){
				$tag_arr=$db->sql_row("SELECT * FROM `organizer_tags` WHERE `organizer`='".$organizer_id."' AND `id`=".$content_tag['tag']);
				print ' <a href="/@'.$organizer_url.'/manage/content/?tag='.$tag_arr['id'].'">'.htmlspecialchars($tag_arr['caption']).'</a>';
			}
			print '</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('tags'==$path_array[3]){
	$replace['title']='Tags | '.$replace['title'];
	if('delete'==$path_array[4]){
		$tag_id=intval($path_array[5]);
		$tag_arr=$db->sql_row("SELECT * FROM `organizer_tags` WHERE `organizer`='".$organizer_id."' AND `id`='".$tag_id."'");
		if(null!=$tag_arr){
			if(isset($_POST['approve'])){
				$db->sql("DELETE FROM `organizer_content_tags`WHERE `organizer`='".$organizer_id."' AND `tag`='".$tag_id."'");
				$db->sql("DELETE FROM `organizer_tags` WHERE `id`='".$tag_id."'");
				header('location:/@'.$organizer_url.'/manage/tags/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/tags/">&larr; Back to tags</a>';
				print '<h2 class="text-red-600">Remove tag #'.$tag_arr['id'].'</h2>';
				$affected_content_count=$db->table_count('organizer_content_tags',"WHERE `organizer`='".$organizer_id."' AND `tag`='".$tag_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Tag "<a href="/@'.$organizer_url.'/manage/tags/edit/'.$tag_arr['id'].'/">'.htmlspecialchars($tag_arr['url']).'</a>" will be removed from the database.</p>';
				if($affected_content_count){
					print '<p>Tag will also be removed for '.$affected_content_count.' linked content.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove tag" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/tags/">&larr; Back to tags</a>';
			print '<h2 class="text-red-600">Tag not found</h2>';
			print '<p>Return to organizer tags and try again.</p>';
		}
	}
	elseif('create'==$path_array[4]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/tags/">&larr; Back to tags</a>';
		print '<h2>Create tag</h2>';
		if(isset($_POST['url'])){
			$errors=[];

			$url=trim($_POST['url']);
			$url=mb_strtolower($url);
			$url=preg_replace('/\.+/','.',$url);
			if(!$url){
				$errors[]='URL is required';
			}
			$tag_exist=$db->table_count('organizer_tags',"WHERE `url`='".$db->prepare($url)."' AND `organizer`='".$organizer_id."'");
			if($tag_exist){
				$errors[]='Tag url '.htmlspecialchars($url).' already exists';
			}

			$caption=trim($_POST['caption']);
			if(!$caption){
				$errors[]='Caption is required';
			}
			$tag_exist=$db->table_count('organizer_tags',"WHERE `caption`='".$db->prepare($caption)."' AND `organizer`='".$organizer_id."'");
			if($tag_exist){
				$errors[]='Tag caption '.htmlspecialchars($caption).' already exists';
			}

			$sort=intval($_POST['sort']);

			if(0==count($errors)){
				$db->sql("INSERT INTO `organizer_tags` (`organizer`,`url`,`caption`,`sort`) VALUES ('".$organizer_id."','".$db->prepare($url)."','".$db->prepare($caption)."','".$sort."')");
				$tag_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Tag <a href="/@'.$organizer_url.'/manage/tags/edit/'.$tag_id.'/">"'.htmlspecialchars($url).'"</a> was created</p>
				</div>';

				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/manage/tags/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';
			$object_scheme=[//org content tag
				'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/content/?tag=<b>URL</b> filter page address bar.','required'=>true],
				'caption'=>['type'=>'text','title'=>'Caption','required'=>true],
				'sort'=>['type'=>'number','title'=>'Sort','placeholder'=>'Sort (number)','required'=>true],
			];
			print build_form($object_scheme,false);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create tag" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[4]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/tags/">&larr; Back to tags</a>';
		print '<h2>Edit tag</h2>';
		$tag_id=(int)$path_array[5];
		$tag_arr=$db->sql_row("SELECT * FROM `organizer_tags` WHERE `organizer`='".$organizer_id."' AND `id`='".$tag_id."'");
		if(null!=$tag_arr){
			if(isset($_POST['url'])){
				$errors=[];

				$url=trim($_POST['url']);
				$url=mb_strtolower($url);
				$url=preg_replace('/\.+/','.',$url);
				if(!$url){
					$errors[]='URL is required';
				}
				$tag_exist=$db->table_count('organizer_tags',"WHERE `url`='".$db->prepare($url)."' AND `organizer`='".$organizer_id."' AND `id`!='".$tag_id."'");
				if($tag_exist){
					$errors[]='Tag url '.htmlspecialchars($url).' already exists';
				}

				$caption=trim($_POST['caption']);
				if(!$caption){
					$errors[]='Caption is required';
				}
				$tag_exist=$db->table_count('organizer_tags',"WHERE `caption`='".$db->prepare($caption)."' AND `organizer`='".$organizer_id."' AND `id`!='".$tag_id."'");
				if($tag_exist){
					$errors[]='Tag caption '.htmlspecialchars($caption).' already exists';
				}

				$sort=intval($_POST['sort']);

				if(0==count($errors)){
					$db->sql("UPDATE `organizer_tags` SET
						`url`='".$db->prepare($url)."',
						`caption`='".$db->prepare($caption)."',
						`sort`='".$sort."'
						WHERE `id`='".$tag_id."'");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Tag <a href="/@'.$organizer_url.'/manage/tags/edit/'.$tag_id.'/">"'.htmlspecialchars($url).'"</a> was updated</p>
					</div>';

					print '<meta http-equiv="refresh" content="5;url=/@'.$organizer_url.'/manage/tags/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card">';
				print '<div class="max-w-fit">';
				$object_scheme=[//org content tag
					'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/content/?tag=<b>URL</b> filter page address bar.','required'=>true],
					'caption'=>['type'=>'text','title'=>'Caption','required'=>true],
					'sort'=>['type'=>'number','title'=>'Sort','placeholder'=>'Sort (number)','required'=>true],
				];
				print build_form($object_scheme,$tag_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Tag not found</h2>';
			print '<p>Return to organizer tags and try again.</p>';
		}
	}
	else{
		print '<a href="/@'.$organizer_url.'/manage/tags/create/" class="action-btn">Create tag</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[3]],$ltml_tab_descr_arr).'</p>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Action</th>';
			print '<th>Sort</th>';
			print '<th>URL</th>';
			print '<th>Caption</th>';
			print '<th>Content count</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$tags=$db->sql("SELECT * FROM `organizer_tags` WHERE `organizer`='".$organizer_id."' ORDER BY `sort` ASC");
		foreach($tags as $tag){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/manage/tags/delete/'.$tag['id'].'/" class="red-btn">Delete</a></td>';
			print '<td>'.$tag['sort'].'</td>';
			print '<td><a href="/@'.$organizer_url.'/manage/tags/edit/'.$tag['id'].'/">'.htmlspecialchars($tag['url']).'</a></td>';
			print '<td>'.htmlspecialchars($tag['caption']).'</td>';
			$content_count=$db->table_count('organizer_content_tags',"WHERE `organizer`='".$organizer_id."' AND `tag`='".$tag['id']."'");
			print '<td><a href="/@'.$organizer_url.'/manage/content/?tag='.$tag['id'].'">'.$content_count.'</a></td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}
}
elseif('files'==$path_array[3]){
	error_reporting(255);
	$replace['title']='Files | '.$replace['title'];
	if('upload'==$path_array[4]){
		$upload_max_size=ini_get('upload_max_filesize');
		$replace['title']='Upload | '.$replace['title'];
		if(isset($_POST['custom_upload'])){
			$dir='/files/organizers/'.$organizer_id.'/custom/';
			if(!file_exists($root_dir.$dir)){
				mkdir($root_dir.$dir,0777,true);
			}
			$errors=[];
			$files=[];
			$upload_name='upload';

			if(isset($_FILES[$upload_name]) && 0==$_FILES[$upload_name]['error']){
				//check extensions
				$ext=pathinfo($_FILES[$upload_name]['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$custom_upload_allowed_extensions)){
					if($_FILES[$upload_name]['size']<=1024*1024*(intval($upload_max_size))){//check size $upload_max_size
						$upload=md5($_FILES[$upload_name]['name']).'_'.time().'.'.$ext;
						$filename=$dir.$upload;
						move_uploaded_file($_FILES[$upload_name]['tmp_name'],$root_dir.$filename);
						http_response_code(200);

						$files[]=['time'=>time(),'name'=>$_FILES[$upload_name]['name'],'size'=>$_FILES[$upload_name]['size'],'type'=>$_FILES[$upload_name]['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>0,'target_id'=>0];//target=none
					}
					else{
						$errors[]='File size must be less than '.$upload_max_size;
					}
				}
				else{
					$errors[]='File must be allowed extension';
				}
			}
			if(0==count($_FILES)){
				$errors[]='File not found';
			}

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			if(0==count($errors)){
				print '
				<div class="success-box" role="alert">
				<p class="font-bold">Success</p>';
				foreach($files as $file){
					$db->sql("INSERT INTO `organizer_files` SET `organizer`='".$organizer_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$file['target_id']."',`address`='".$auth['status_address_id']."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					print '<p>File ';
					print '<a href="'.htmlspecialchars($file['relative_path']).'" target="_blank" class="font-bold">';
						print htmlspecialchars(substr($file['relative_path'],strrpos($file['relative_path'],'/')+1));
						print '</a>';
					print ' uploaded successfully.</p>';
				}
				print '
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/manage/files/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
			print '<div class="max-w-fit">';
			print '<p class="text-sm">Allowed extensions: '.implode(', ',$custom_upload_allowed_extensions).'.</p>';
			$object_scheme=[//admin custom upload
				'upload'=>[
					'type'=>'file',
					'title'=>'File',
					'descr'=>'Uploaded file will be public available, so you can share it with anyone<br>Max size: '.$upload_max_size.'',
				],
			];
			print build_form($object_scheme);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" name="custom_upload" value="Upload file" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('delete'==$path_array[4]){
		$replace['title']='Delete | '.$replace['title'];
		$file_arr=$db->sql_row("SELECT * FROM `organizer_files` WHERE `organizer`='".$organizer_id."' AND `id`='".(int)$path_array[5]."' LIMIT 1");
		if($file_arr){
			if(isset($_POST['approve'])){
				$free_file_size=filesize($file_arr['path']);
				@unlink($file_arr['path']);
				$db->sql("DELETE FROM `organizer_files` WHERE `id`='".$file_arr['id']."' LIMIT 1");
				$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
				header('Location: /@'.$organizer_url.'/manage/files/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/files/">&larr; Back to files</a>';
				print '<h2 class="text-red-600">Delete file #'.$file_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Attention</p>
					<p>If you delete this file, it will be lost forever and can not be displayed by linked content.</p>
					<p>Are you sure you want to delete file ';
					print '<a href="'.htmlspecialchars($file_arr['relative_path']).'" target="_blank" class="font-bold">';
					print htmlspecialchars(substr($file_arr['relative_path'],strrpos($file_arr['relative_path'],'/')+1));
					print '</a>';
					print '?</p>
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Yes, delete" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/manage/files/">&larr; Back to files</a>';
			print '<h2 class="text-red-600">File not found</h2>';
			print '<p>Return to files list and try again.</p>';

			header('Location: /@'.$organizer_url.'/manage/files/');
			exit;
		}
	}
	else{
		print '<a class="action-btn" href="/@'.$organizer_url.'/manage/files/upload/">Upload file</a>';
		print '<span class="text-sm">Summary size of organizer files: '.human_readable_filesize($db->select_one('organizer_files','SUM(`size`)',"WHERE `organizer`='".$organizer_id."'")).'</span>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[3]],$ltml_tab_descr_arr).'</p>';

		$filter_target=false;
		if(isset($_GET['target'])){
			if(''!==$_GET['target']){
				$filter_target=(int)$_GET['target'];
				if(!isset($organizer_files_target[$filter_target])){
					$filter_target=false;
				}
			}
		}

		$filter_address=false;
		$filter_address_id=false;
		if(isset($_GET['address'])){
			if(''!==$_GET['address']){
				$filter_address=$_GET['address'];
				$filter_address_id=$db->select_one('addresses','id',"WHERE `address`='".$db->prepare($_GET['address'])."'");
				if(!$filter_address_id){
					$filter_address_id=-1;
				}
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="address" class="form-post-by-enter" placeholder="Uploader address" value="'.($filter_address?htmlspecialchars($filter_address):'').'">
			</div>
			<div>
				<select name="target" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_target?' selected':'').'>Filter by target</option>';
					foreach($organizer_files_target as $target_type=>$target_caption){
						print '<option value="'.$target_type.'"'.($target_type===$filter_target?' selected':'').'>Target: '.htmlspecialchars($target_caption).'</option>';
					}
					print '
				</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>File</th>';
			print '<th>Name</th>';
			print '<th>Size</th>';
			print '<th>Target</th>';
			print '<th>Time</th>';
			print '<th>Uploader</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]="`organizer`='".$organizer_id."'";
		if(false!==$filter_target){
			$sql_addon[]='`target`='.$filter_target;
		}
		if(false!==$filter_address_id){
			$sql_addon[]='`address`='.$filter_address_id;
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		$per_page=50;
		$count=$db->table_count('organizer_files',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$files=$db->sql("SELECT * FROM `organizer_files`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($files as $file_arr){
			print '<tr>';
			print '<td>';
			print '<a href="/@'.$organizer_url.'/manage/files/delete/'.$file_arr['id'].'/" class="red-btn">Delete</a>';
			print '</td>';

			print '<td class="text-sm">';
			print '<a href="'.htmlspecialchars($file_arr['relative_path']).'" target="_blank">';
			print htmlspecialchars(substr($file_arr['relative_path'],strrpos($file_arr['relative_path'],'/')+1));
			print '</a>';
			print '</td>';

			print '<td class="text-sm">'.htmlspecialchars($file_arr['name']).'</td>';
			print '<td class="whitespace-nowrap">'.human_readable_filesize($file_arr['size']).'</td>';

			print '<td>';
			$target_link=false;
			if($file_arr['target_id']){
				if(1==$file_arr['target']){
					$target_link='/@'.$organizer_url.'/manage/content/edit/'.$file_arr['target_id'].'/';
				}
				if(2==$file_arr['target']){
					$target_link='/@'.$organizer_url.'/manage/events/edit/'.$file_arr['target_id'].'/';//will redirected to event edit page
				}
			}
			if($target_link){
				print '<a href="'.htmlspecialchars($target_link).'">';
			}
			print htmlspecialchars($organizer_files_target[$file_arr['target']]);
			if($target_link){
				print '</a>';
			}
			print '</td>';


			if($file_arr['time']){
				print '<td><span class="time" data-time="'.$file_arr['time'].'">'.date('d.m.Y H:i:s',$file_arr['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}

			$uploader='&mdash;';
			if($file_arr['address']){
				$uploader_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$file_arr['address']."'");
				if($uploader_arr){
					$uploader=htmlspecialchars($uploader_arr['address']);
				}
			}
			print '<td>';
			print $uploader;
			print '</td>';

			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
else{
	http_response_code(404);
	print '<h1>'.htmlspecialchars($path_array[3]).'</h1>';
	print '<p>Page not found.</p>';
}
$manage_content=ob_get_contents();
ob_end_clean();