<?php
$replace['counters']='';
ob_start();
if(!isset($event_arr)){
	http_response_code(404);
	exit;
}
if(!isset($allow_event_manage)){
	http_response_code(404);
	exit;
}
if(false===$allow_event_manage){
	http_response_code(403);
	exit;
}
$organizer_id=$organizer_arr['id'];
$event_id=$event_arr['id'];

$script_preset.='var manage_event=true;';

$default_wizard_manage_section=['wizard','dashboard','event'];
$full_wizard_manage_section=['wizard','dashboard','event','customize','levels','users','invites','whitelist','locations','speakers','sessions','content','partners','files'];

$wizard=$config['default_wizard'];
if(isset($event_arr['wizard'])){
	if('[]'!=$event_arr['wizard']){//not empty
		$wizard=json_decode($event_arr['wizard'],true);
	}
}
if(!isset($wizard['step'])){
	$wizard['step']=1;
}
if(!isset($wizard['dismiss'])){
	$wizard['dismiss']=false;
}
if(!isset($wizard['manage'])){
	$wizard['manage']=$default_wizard_manage_section;
}

$levels_arr=[];
$levels=$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `id` ASC");
foreach($levels as $level){
	$levels_arr[$level['id']]=$level;
}

//return original badges
//https://gitlab.com/envelop/tickets/getpass-land/-/issues/2#note_1327406508
$replace['attention_badge']=$replace['original_attention_badge'];
$replace['notify_badge']=$replace['original_notify_badge'];
$replace['user_badge']=$replace['original_user_badge'];

$replace['title']='Manage '.$replace['title'];
print '<div class="admin-title-wrapper">';
print '<h1>Manage <a href="/@'.$organizer_url.'/'.$event_url.'/">'.htmlspecialchars($event_arr['title']).'</a></h1>';
if($config['manual_event_link']){
	print '<div class="button-wrapper">';
	print '<a href="'.$config['manual_event_link'].'" target="_blank" class="action-btn">'.$ltmp['icons']['video'].' '.$config['manual_event_caption'].'</a>';
	print '</div>';
}
print '</div>';
print '<hr>';
print '<div class="my-4">';
print '<ul class="submenu">';
//https://gitlab.com/envelop/tickets/getpass/-/issues/33
$tab_class='';//default3-button
if(in_array('wizard',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/wizard/" class="'.$tab_class.('wizard'==$path_array[4]?' selected':'').'">Wizard</a></li>';
}
if(in_array('event',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/event/" class="'.$tab_class.('event'==$path_array[4]?' selected':'').'">Event</a></li>';
}
if(in_array('customize',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/customize/" class="'.$tab_class.('customize'==$path_array[4]?' selected':'').'">Customize</a></li>';
}
if(in_array('levels',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/" class="'.$tab_class.('levels'==$path_array[4]?' selected':'').'">Levels</a></li>';
}
if(in_array('users',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/users/" class="'.$tab_class.('users'==$path_array[4]?' selected':'').'">Users</a></li>';
}
if(in_array('invites',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/" class="'.$tab_class.('invites'==$path_array[4]?' selected':'').'">Invites</a></li>';
}
if(in_array('whitelist',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/" class="'.$tab_class.('whitelist'==$path_array[4]?' selected':'').'">NFT tickets</a></li>';
}
if(in_array('locations',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/" class="'.$tab_class.('locations'==$path_array[4]?' selected':'').'">Locations</a></li>';
}
if(in_array('speakers',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/" class="'.$tab_class.('speakers'==$path_array[4]?' selected':'').'">Speakers</a></li>';
}
if(in_array('sessions',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/" class="'.$tab_class.('sessions'==$path_array[4]?' selected':'').'">Sessions</a></li>';
}
if(in_array('content',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/" class="'.$tab_class.('tags'==$path_array[4]?' selected':'').'">Tags</a></li>';
}
if(in_array('content',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/" class="'.$tab_class.('content'==$path_array[4]?' selected':'').'">Content</a></li>';
}
if(in_array('partners',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/" class="'.$tab_class.('partners_cat'==$path_array[4]?' selected':'').'">Partners categories</a></li>';
}
if(in_array('partners',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/" class="'.$tab_class.('partners'==$path_array[4]?' selected':'').'">Partners</a></li>';
}
if(in_array('files',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/files/" class="'.$tab_class.('files'==$path_array[4]?' selected':'').'">Files</a></li>';
}
if(in_array('dashboard',$wizard['manage'])){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/stats/" class="'.$tab_class.('stats'==$path_array[4]?' selected':'').'">Stats</a></li>';
}

print '</ul>';
print '</div>';
print '<hr class="my-4">';

if($limit_upload && count($_FILES)>0){
	print '
	<div class="attention-box" role="alert">
		<p><b>Error!</b> You can not upload files. Please <a href="/@'.$organizer_url.'/'.$event_url.'/manage/files/">remove some files</a> before you upload new ones or subscribe to paid plan.</p>
	</div>';
	$_FILES=[];
}

if(''==$path_array[4]){
	if(isset($wizard['dismiss'])){
		if(true===$wizard['dismiss']){//if wizard is completed
			header('location: /@'.$organizer_url.'/'.$event_url.'/manage/event/');
			exit;
		}
	}
	header('location: /@'.$organizer_url.'/'.$event_url.'/manage/wizard/');
	exit;
}
if('wizard'!=$path_array[4]){//not wizard section
	if(isset($wizard['dismiss'])){
		if(false===$wizard['dismiss']){//wizard is not completed
			print '<div class="attention-box" role="alert">';
			print '<p>Please <a href="/@'.$organizer_url.'/'.$event_url.'/manage/wizard/">return to wizard and complete it</a> before you continue event management.</p>';
			print '</div>';
		}
	}
}

$tab_descr_arr=[
	'wizard'=>'This section helps a novice user to understand the event settings on the Getpass platform.',
	'dashboard'=>'Main statistics of {EVENT_NAME} here',
	'event'=>'URL, description and main sections of the event, event date and status (planned, active, etc.)',
	'customize'=>'Fine-tuning settings for the logo, speaker avatars, and other elements on the event page.',
	'levels'=>'Visitor tiers, if you have different categories (primarily applicable to paid events).',
	'users'=>'Event visitors',
	'invites'=>'Special invitations through EVM wallets, without NFT tickets, which you can, for example, give to partners or speakers.',
	'whitelist'=>'NFT/wNFT tickets, which are used for entry (authorization) to the event.',
	'locations'=>'Where the event is held (offline/online; platform used, etc.).',
	'speakers'=>'Your speakers and their descriptions.',
	'sessions'=>'Event sessions, where speakers present (time, presentation topics, etc.).',
	'tags'=>'Tags used in the blog section of the event page on the Getpass platform.',
	'content'=>'Content published in the blog section of the event page on the Getpass platform.',
	'partners_cat'=>'What specific types/kinds of partners can you have? For example, VIP partners (platinum, gold, etc.), standard partners, informational and other partners.',
	'partners'=>'List of partners and their logos.',
	'files'=>'Files used for the event and their data.',
	'moderation'=>'Check moderation status of your event.',
];
$ltml_tab_descr_arr=['ORGANIZER_NAME'=>$organizer_arr['title'],'EVENT_NAME'=>$event_arr['title']];

if('wizard'==$path_array[4]){
	//https://gitlab.com/envelop/tickets/getpass/-/issues/8
	function wizard_final(){
		global $event_arr,$db,$step,$wizard,$wizard_steps;
		//add last sections to manage array
		if(!in_array('customize',$wizard['manage'])){
			$wizard['manage'][]='customize';
		}
		if(!in_array('users',$wizard['manage'])){
			$wizard['manage'][]='users';
		}
		if(!in_array('whitelist',$wizard['manage'])){
			$wizard['manage'][]='whitelist';
		}
		if(!in_array('files',$wizard['manage'])){
			$wizard['manage'][]='files';
		}
		$wizard['result'][$step]=1;
		$wizard['finished'][$step]=1;
		$wizard['dismiss']=true;
		return true;
	}
	$wizard_steps=[
		0=>['title'=>'Event creation'],//was on organizer page, now moved to event page
		1=>[
			'title'=>'Event Options: Opt for Free or Paid Event Access?',
			'description'=>'At this step, you will determine if your event will be free or if there will be a cost associated with attending.',
			'options'=>[
				0=>['caption'=>'Free event','description'=>'Any guest can attend your event without paying for it.'],
				1=>['caption'=>'Paid event','description'=>'Only paid access to your event is available. Guests will be able to purchase tickets and see free content.'],
			],
			'default'=>0,
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				$result=$wizard_steps[$step]['default'];
				if(isset($wizard_steps[$step]['options'][$input])){
					$result=$input;
				}
				if(0==$result){//only free access
					$db->sql("DELETE FROM `event_levels` WHERE `event`='".$event_arr['id']."'");
					$db->sql("INSERT INTO `event_levels` (`event`,`level`,`name`,`caption`) VALUES ('".$event_arr['id']."',0,'Guest','Free access to free event content')");
					if(in_array('levels',$wizard['manage'])){
						//exclude levels from wizard manage array
						$wizard['manage']=array_diff($wizard['manage'],['levels']);
					}
				}
				if(1==$result){//only paid access
					$db->sql("DELETE FROM `event_levels` WHERE `event`='".$event_arr['id']."'");
					$db->sql("INSERT INTO `event_levels` (`event`,`level`,`name`,`caption`) VALUES ('".$event_arr['id']."',0,'Guest','Free access to free event content')");
					$db->sql("INSERT INTO `event_levels` (`event`,`level`,`name`,`caption`) VALUES ('".$event_arr['id']."',1,'Standart','Access to paid event content')");
					if(!in_array('levels',$wizard['manage'])){
						$wizard['manage'][]='levels';
					}
				}
				$wizard['result'][$step]=$result;
				$wizard['finished'][$step]=1;
				return true;
			},
		],
		2=>[
			'title'=>'Event Attendee Levels: only Standard or additional VIP tier?',
			'available_function'=>function(){
				global $wizard;
				if(isset($wizard['finished'][1])){
					if(1==$wizard['result'][1]){//paid
						return true;
					}
				}
				return false;
			},
			'options'=>[
				0=>['caption'=>'Standart attendee level only','description'=>'Only one paid level is available. Guests will be able to purchase tickets to become attendees and get pass to paid content.'],
				1=>['caption'=>'Add VIP attendee level','description'=>'Premium level is available with extra content and bonuses.'],
			],
			'default'=>0,
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				$result=$wizard_steps[$step]['default'];
				if(isset($wizard_steps[$step]['options'][$input])){
					$result=$input;
				}
				if(0==$result){//skip
				}
				if(1==$result){//add VIP level
					$db->sql("DELETE FROM `event_levels` WHERE `event`='".$event_arr['id']."' AND `level`>1");//if there are more than 2 levels (guest and standart), delete them
					$db->sql("INSERT INTO `event_levels` (`event`,`level`,`name`,`caption`) VALUES ('".$event_arr['id']."',2,'VIP','Access to premium content')");
					if(!in_array('levels',$wizard['manage'])){
						$wizard['manage'][]='levels';
					}
				}
				$wizard['result'][$step]=$result;
				$wizard['finished'][$step]=1;
				return true;
			},
		],
		3=>[
			'title'=>'Are there partners of the event, sponsors?',
			'options'=>[
				0=>['caption'=>'No public partners','description'=>'And there\'s no one to thank.'],
				1=>['caption'=>'Of course there are!','description'=>'Media partners, bloggers, sponsors - all are there or in the plans!'],
			],
			'default'=>0,
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				$result=$wizard_steps[$step]['default'];
				if(isset($wizard_steps[$step]['options'][$input])){
					$result=$input;
				}
				if(0==$result){//remove partners categories
					if(in_array('partners',$wizard['manage'])){
						$wizard['manage']=array_diff($wizard['manage'],['partners']);
					}
				}
				if(1==$result){//add partners categories
					$db->sql("INSERT INTO `event_partners_cat` (`event`,`caption`,`sort`) VALUES ('".$event_arr['id']."','Sponsors',1)");
					$db->sql("INSERT INTO `event_partners_cat` (`event`,`caption`,`sort`) VALUES ('".$event_arr['id']."','Partners',2)");
					$db->sql("INSERT INTO `event_partners_cat` (`event`,`caption`,`sort`) VALUES ('".$event_arr['id']."','Media',3)");
					if(!in_array('partners',$wizard['manage'])){
						$wizard['manage'][]='partners';
					}
				}
				$wizard['result'][$step]=$result;
				$wizard['finished'][$step]=1;
				return true;
			},
		],
		4=>[
			'title'=>'Does the event need a content section (blog, news)?',
			'description'=>'You\'ll be able to create little announcements, talk about speakers, and warm up interest in your event..',
			'options'=>[
				0=>['caption'=>'No, We have main event page and that\'s enough'],
				1=>['caption'=>'Yes, We plan to publish content for the event'],
			],
			'default'=>0,
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				$result=$wizard_steps[$step]['default'];
				if(isset($wizard_steps[$step]['options'][$input])){
					$result=$input;
				}
				if(0==$result){
					if(in_array('content',$wizard['manage'])){
						$wizard['manage']=array_diff($wizard['manage'],['content']);
					}
				}
				if(1==$result){//add content tags
					$db->sql("INSERT INTO `event_tags` (`event`,`url`,`caption`,`sort`) VALUES ('".$event_arr['id']."','news','News',1)");
					$db->sql("INSERT INTO `event_tags` (`event`,`url`,`caption`,`sort`) VALUES ('".$event_arr['id']."','blog','Blog',2)");
					$db->sql("INSERT INTO `event_tags` (`event`,`url`,`caption`,`sort`) VALUES ('".$event_arr['id']."','articles','Articles',2)");
					if(1==$wizard['result'][2]){
						$db->sql("INSERT INTO `event_tags` (`event`,`url`,`caption`,`sort`) VALUES ('".$event_arr['id']."','vip','VIP',2)");
					}
					if(1==$wizard['result'][3]){
						$db->sql("INSERT INTO `event_tags` (`event`,`url`,`caption`,`sort`) VALUES ('".$event_arr['id']."','sponsored','Sponsored',2)");
					}
					if(!in_array('content',$wizard['manage'])){
						$wizard['manage'][]='content';
					}
				}
				$wizard['result'][$step]=$result;
				$wizard['finished'][$step]=1;
				return true;
			},
		],
		5=>[
			'title'=>'Are you planning to involve managers in filling the event or give free invitations to speakers/partners?',
			'description'=>'For example: you can create invite for managers and give them access to the event manager. They will be able to fill in the information about the event, add speakers, and manage the event content. Or you can create free invites for speakers and partners, and they will be able to register for the event without paying.',
			'options'=>[
				0=>['caption'=>'No, I\'ll do everything myself, nobody get invited'],
				1=>['caption'=>'Yes, I\'ll invite other peoples (managers, speakers, etc.)'],
			],
			'default'=>0,
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				$result=$wizard_steps[$step]['default'];
				if(isset($wizard_steps[$step]['options'][$input])){
					$result=$input;
				}
				if(0==$result){
					if(in_array('invites',$wizard['manage'])){
						$wizard['manage']=array_diff($wizard['manage'],['invites']);
					}
				}
				if(1==$result){//add invites
					if(!in_array('invites',$wizard['manage'])){
						$wizard['manage'][]='invites';
					}
				}
				$wizard['result'][$step]=$result;
				$wizard['finished'][$step]=1;
				return true;
			},
		],
		6=>[
			'title'=>'Event number of scenes?',
			'description'=>'You can create a single scene event or a multi-scene event. For example, you can create a main scene and a hackaton scene. Or you can create a main scene and a scene for sponsors.',
			'options'=>[
				0=>['caption'=>'Single scene event'],
				1=>['caption'=>'Multi-scene event'],
			],
			'default'=>0,
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				$result=$wizard_steps[$step]['default'];
				if(isset($wizard_steps[$step]['options'][$input])){
					$result=$input;
				}
				if(0==$result){
					$db->sql("DELETE FROM `event_locations` WHERE `event`='".$event_arr['id']."'");
					$db->sql("INSERT INTO `event_locations` (`event`,`caption`,`status`) VALUES ('".$event_arr['id']."','Main stage',0)");
					if(!in_array('locations',$wizard['manage'])){
						$wizard['manage'][]='locations';
					}
					/*
					//removed by request
					if(in_array('locations',$wizard['manage'])){
						$wizard['manage']=array_diff($wizard['manage'],['locations']);
					}
					*/
				}
				if(1==$result){
					if(0==$db->table_count('event_locations','WHERE `event`='.$event_arr['id'].' AND `caption`=\'Main stage\'')){
						$db->sql("INSERT INTO `event_locations` (`event`,`caption`,`status`) VALUES ('".$event_arr['id']."','Main stage',0)");
					}
					if(0==$db->table_count('event_locations','WHERE `event`='.$event_arr['id'].' AND `caption`=\'Hackaton stage\'')){
						$db->sql("INSERT INTO `event_locations` (`event`,`caption`,`status`) VALUES ('".$event_arr['id']."','Hackaton stage',0)");
					}
					if(!in_array('locations',$wizard['manage'])){
						$wizard['manage'][]='locations';
					}
				}
				$wizard['result'][$step]=$result;
				$wizard['finished'][$step]=1;
				return true;
			},
		],
		7=>[
			'title'=>'Do speakers need separate cards with information about them?',
			'description'=>'',
			'options'=>[
				0=>['caption'=>'No, speakers will be displayed on the main event page as text'],
				1=>['caption'=>'Yes, speakers will have their own cards','description'=>'You can assign speaker to specific sessions and they will be displayed on the session page.'],
			],
			'default'=>0,
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				$result=$wizard_steps[$step]['default'];
				if(isset($wizard_steps[$step]['options'][$input])){
					$result=$input;
				}
				if(0==$result){
					if(in_array('speakers',$wizard['manage'])){
						$wizard['manage']=array_diff($wizard['manage'],['speakers']);
					}
				}
				if(1==$result){
					if(!in_array('speakers',$wizard['manage'])){
						$wizard['manage'][]='speakers';
					}
				}
				$wizard['result'][$step]=$result;
				$wizard['finished'][$step]=1;
				return true;
			},
		],
		8=>[
			'title'=>'Are separate sessions of the event planned, agenda, or all in a single stream?',
			'description'=>'All sessions will be displayed on the agenda page. You can detail the agenda by adding a description, speakers to each session, starting and ending time, and location.',
			'options'=>[
				0=>['caption'=>'All in a single stream'],
				1=>['caption'=>'Separate sessions','description'=>'You can create a separate session for each scene.'],
			],
			'default'=>0,
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				$result=$wizard_steps[$step]['default'];
				if(isset($wizard_steps[$step]['options'][$input])){
					$result=$input;
				}
				if(0==$result){
					$db->sql("DELETE FROM `event_sessions` WHERE `event`='".$event_arr['id']."'");
					$db->sql("INSERT INTO `event_sessions` (`event`,`caption`,`time`,`endtime`,`status`) VALUES ('".$event_arr['id']."','Main session','".$event_arr['time']."','".(1800+$event_arr['time'])."',0)");//default 30 min for example
				}
				//add sessions in manage array if not exists
				if(!in_array('sessions',$wizard['manage'])){
					$wizard['manage'][]='sessions';
				}
				$wizard['result'][$step]=$result;
				$wizard['finished'][$step]=1;
				return true;
			},
		],
		9=>[
			'title'=>'Help with NFT tickets minting?',
			'list'=>[
				'<a href="https://appv1.envelop.is/mint/" target="_blank">Mint NFT tickets</a> for the event',
				'Add NFT tickets to the event <a href="%whitelist%" target="_blank">whitelist</a> (use contract address and token id\'s)',
				'Add marketplaces links to <a href="%event%" target="_blank">tickets section</a>',
				'Start marketing campaign ❤️',
			],
			'available_function'=>function(){
				global $wizard;
				if(isset($wizard['finished'][1])){
					if(1==$wizard['result'][1]){//paid
						return true;
					}
				}
				return false;
			},
			'setup_function'=>function($input){
				global $event_arr,$db,$step,$wizard,$wizard_steps;
				wizard_final();
				return true;
			},
		],
	];
	if(isset($_GET['dismiss'])){
		if(1==$_GET['dismiss']){
			$wizard['dismiss']=true;
			$wizard['manage']=$full_wizard_manage_section;//all sections are available
			$db->sql("UPDATE `events` SET `wizard`='".$db->prepare(json_encode($wizard))."' WHERE `id`='".$event_arr['id']."'");
			header('location: /@'.$organizer_url.'/'.$event_url.'/manage/');
			exit;
		}
	}
	/*
	//for tests only
	if(isset($_GET['finish'])){
		if(1==$_GET['finish']){
			$wizard['dismiss']=true;
			$db->sql("UPDATE `events` SET `wizard`='".$db->prepare(json_encode($wizard))."' WHERE `id`='".$event_arr['id']."'");
			header('location: /@'.$organizer_url.'/'.$event_url.'/manage/');
			exit;
		}
	}
	*/
	if(isset($_GET['reset'])){
		if(1==$_GET['reset']){
			$db->sql("UPDATE `events` SET `wizard`='[]' WHERE `id`='".$event_arr['id']."' LIMIT 1");
			header('location: /@'.$organizer_url.'/'.$event_url.'/manage/wizard/');
			exit;
		}
	}
	$step=$wizard['step'];
	$go_direction=1;
	if(isset($_GET['go'])){
		if('back'==$_GET['go']){
			$go_direction=-1;
		}
	}
	if(isset($_GET['step'])){
		$step=(int)$_GET['step'];
	}
	$min_step=1;
	$max_step=count($wizard_steps)-1;
	if($step<1){
		$step=1;
	}
	if($step>$max_step){//max?
		$step=$max_step;
	}
	$replace['title']='Wizard  '.$replace['title'];
	print '<h2><span class="emoji-circle"></span> Wizard</h2>';//🔮
	print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';
	print '<hr>';
	$available=true;
	$finish=false;
	if(isset($wizard_steps[$step]['available_function'])){
		if(is_callable($wizard_steps[$step]['available_function'])){
			$available=$wizard_steps[$step]['available_function']();
		}
	}
	if($available){
		if(isset($_POST['input'])){
			if(isset($wizard_steps[$step]['setup_function'])){
				if(is_callable($wizard_steps[$step]['setup_function'])){
					$wizard_steps[$step]['setup_function']($_POST['input']);
				}
			}
			$wizard['step']=$step+1;
			if($wizard['step']>=$max_step){
				$wizard['step']=$max_step;
				$wizard['dismiss']=true;//finished
			}
			$db->sql("UPDATE `events` SET `wizard`='".$db->prepare(json_encode($wizard))."' WHERE `id`='".$event_arr['id']."'");
			header('location: /@'.$organizer_url.'/'.$event_url.'/manage/wizard/');
			exit;
		}
		print '<h3>'.$wizard_steps[$step]['title'].'</h3>';
		if(isset($wizard_steps[$step]['description'])){
			print '<p>'.$wizard_steps[$step]['description'].'</p>';
		}
		print '<form action="" method="POST" class="manage-card">';
		print '<input type="hidden" name="wizard_step" value="'.$step.'">';
		print '<div class="max-w-fit wizard-form">';
		if(isset($wizard_steps[$step]['options'])){
			$default=$wizard_steps[$step]['default'];
			if(isset($wizard['result'][$step])){
				$default=$wizard['result'][$step];
			}
			foreach($wizard_steps[$step]['options'] as $option_id=>$option_data){
				print '<div class="my-4"><label><input type="radio" name="input" value="'.$option_id.'"'.($default==$option_id?' checked':'').'> &mdash; '.$option_data['caption'].'</label>';
				if(isset($option_data['description'])){
					print '<div><span class="form-description">'.$option_data['description'].'</div>';
				}
				print '</div>';
			}
		}
		if(isset($wizard_steps[$step]['list'])){
			print '<div class="content-wrapper">';
			print '<p>List of actions:</p>';
			print '<ul>';
			foreach($wizard_steps[$step]['list'] as $item){
				$item=str_replace('%event%','/@'.$organizer_url.'/'.$event_url.'/manage/event/',$item);
				$item=str_replace('%whitelist%','/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/',$item);
				print '<li>'.$item.'</li>';
			}
			print '</ul>';
			print '</div>';
		}
		$last=false;
		if($step==$max_step){
			$last=true;
			print '<input type="hidden" name="input" value="1">';
		}
		if($last){
			if($wizard['dismiss']){//already finished, show only text from last step
				$finish=true;
			}
		}
		if(!$finish){
			//https://gitlab.com/envelop/tickets/getpass/-/issues/32
			print '<input type="submit" value="'.($last?'Finish wizard':'Next step').'" class="action default-button selected">';
		}
		print '</div>';
		print '</form>';
	}
	else{
		if($max_step!=$step){
			//skip this step and go to next step
			header('location: /@'.$organizer_url.'/'.$event_url.'/manage/wizard/?step='.($step+$go_direction));
			exit;
		}
		else{//if last step is not available, finish
			$finish=true;
		}
	}

	if($finish){
		wizard_final();
		$db->sql("UPDATE `events` SET `wizard`='".$db->prepare(json_encode($wizard))."' WHERE `id`='".$event_arr['id']."'");

		print '<p>✔️ <b>Thank you for completing the Wizard!</b></p>';
		print '<p>You\'ve taken important steps towards setting up your event. Now, you have the option to further fine-tune your event by selecting the specific sections that require additional customization.</p>';
		print '<p>Please choose the relevant section for further refinement to ensure that your event is tailored to your exact needs.</p>';
	}
	if($min_step!=$step){
		print '<div class="my-4"><a href="?step='.($step-1).'&go=back" class="text-sm action-btn navigate default-button">&larr; Previous step</a></div>';
	}
	/*
	//https://gitlab.com/envelop/tickets/getpass/-/issues/32
	print '<hr class="my-4">';
	if($max_step!=$step){
		print '<a href="?step='.($step+1).'" class="text-sm action-btn navigate default-button">Next step &rarr;</a>';
	}
	*/
	if($finish){
		print '<div><a href="/@'.$organizer_url.'/'.$event_url.'/manage/" class="action default-button selected">Finish</a></div>';
	}
	print '<hr class="my-4">';
	print '<a class="/*action-btn navigate default-button*/ reverse-btn wizard-advanced-mode" href="?dismiss=1">Switch to advanced mode</a>';

	/*
	https://gitlab.com/envelop/tickets/getpass/-/issues/32
	print '<p class="wizard-description">The Wizard is a tool designed to simplify event planning and customization. With its user-friendly interface and advanced features, the module guides users through a series of interactive steps to better understand their event requirements, customize the visual aspects, and identify potential gaps in planning. However, for those who prefer more control, the module also offers an option to skip the guided process and access all features with a single click.</p>';
	print '<p class="wizard-description">Note: Want to skip the guided process? <a href="?dismiss=1">Click here to access all features</a> and customize your event to your liking without the help of the Wizard.</p>';
	print '<p class="wizard-description italic">Also you can reset wizard process by <a href="?reset=1">clicking here</a>.</p>';
	*/
}
else
if('stats'==$path_array[4]){
	$replace['title']='Dashboard | '.$replace['title'];
	print '<p>'.ltmp($tab_descr_arr['dashboard'],$ltml_tab_descr_arr).'</p>';
	print '<p>Users count: '.$db->table_count('event_users',"WHERE `event`='".$event_id."'").'</p>';
	print '<p>Users verified: '.$db->table_count('event_users',"WHERE `event`='".$event_id."' AND `status`>0").'</p>';
	print '<p>Users invited: '.$db->table_count('event_users',"WHERE  `event`='".$event_id."' AND `status`=2").'</p>';
	print '<p>Users binded: '.$db->table_count('event_users',"WHERE  `event`='".$event_id."' AND `status`=1").'</p>';
	print '<p>Admins count: '.$db->table_count('event_users',"WHERE `event`='".$event_id."' AND `admin`=1").'</p>';
	print '<p>Managers count: '.$db->table_count('event_users',"WHERE `event`='".$event_id."' AND `manager`=1").'</p>';
	print '<p>Speakers count: '.$db->table_count('event_users',"WHERE `event`='".$event_id."' AND `speaker`=1").'</p>';
	print '<hr>';
	print '<p>Invites count: '.$db->table_count('event_invites',"WHERE `event`='".$event_id."'").'</p>';
	print '<p>NFT tickets count: '.$db->table_count('event_whitelist',"WHERE `event`='".$event_id."'").'</p>';
	print '<p>NFT Wildcard tickets count: '.$db->table_count('event_whitelist',"WHERE  `event`='".$event_id."' AND `token_id`='*'").'</p>';
	print '<hr>';
	print '<p>Content count: '.$db->table_count('event_content',"WHERE `event`='".$event_id."'").'</p>';
	print '<hr>';
	print '<p>Files count: '.$db->table_count('event_files',"WHERE `event`='".$event_id."'").'</p>';
	$events_file_size=$db->select_one('event_files','SUM(`size`)',"WHERE `event`='".$event_id."'");
	print '<p>Summary files size: '.human_readable_filesize($events_file_size).'</p>';
}
elseif('event'==$path_array[4]){
	$replace['title']='Event | '.$replace['title'];
	print '<h2>Edit event info</h2>';
	print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';
	if(isset($_POST['url'])){
		$errors=[];
		$files=[];
		$remove_files=[];

		$url=trim($_POST['url']);
		$url=mb_strtolower($url);
		$url=str_replace(' ','-',$url);
		$url=str_replace('/','',$url);
		$url=ru2lat($url);
		$url=preg_replace('/[^a-z0-9\-\_\.]/','',$url);
		$url=preg_replace('/\-+/','-',$url);
		$url=preg_replace('/\.+/','.',$url);
		if(in_array($url,$reserved_urls)){
			$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
		}
		//check if url is unique
		$find_event_url=$db->sql_row("SELECT * FROM `events` WHERE `organizer`='".$organizer_id."' AND `url`='".$db->prepare($url)."' AND `id`!='".$event_id."'");
		if(null!=$find_event_url){
			$errors[]='Event URL is not unique for this organizer';
		}

		$title=trim($_POST['title']);
		$short_description=trim($_POST['short_description']);
		$description=trim($_POST['description']);
		$short_location_description=trim($_POST['short_location_description']);
		$speakers_description=trim($_POST['speakers_description']);
		$location_description=trim($_POST['location_description']);
		$tickets_description=trim($_POST['tickets_description']);
		$check_in_message=trim($_POST['check_in_message']);

		$logo_url=$event_arr['logo_url'];
		$cover_url=$event_arr['cover_url'];
		$speakers_cover_url=$event_arr['speakers_cover_url'];
		$location_cover_url=$event_arr['location_cover_url'];
		$partners_cover_url=$event_arr['partners_cover_url'];

		//https://www.php.net/manual/ru/features.file-upload.errors.php
		$upload_max_size=ini_get('upload_max_filesize');
		foreach($_FILES as $file_key=>$file_data){
			switch($file_data['error']){
				case UPLOAD_ERR_INI_SIZE:
					$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
					break;
				case UPLOAD_ERR_FORM_SIZE:
					$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
					break;
			}
		}

		$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$url.'/';
		if(!file_exists($root_dir.$dir)){
			mkdir($root_dir.$dir,0777,true);
		}
		if(isset($_FILES['logo']) && 0==$_FILES['logo']['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png','svg'];
			$ext=pathinfo($_FILES['logo']['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES['logo']['size']<=1024*1024*0.5){//check size 500Kb
					$logo=md5($url).'_logo_'.time().'.'.$ext;
					$filename=$dir.$logo;
					move_uploaded_file($_FILES['logo']['tmp_name'],$root_dir.$filename);
					$logo_url=$filename;

					$remove_files[]=$event_arr['logo_url'];//remove old file
					$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>$_FILES['logo']['size'],'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
				}
				else{
					$errors[]='&mdash; Logo size must be less than 500Kb';
				}
			}
			else{
				$errors[]='&mdash; Logo must be in JPG, JPEG, PNG or SVG format';
			}
		}
		if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png','svg'];
			$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
					$cover=md5($url).'_cover_'.time().'.'.$ext;
					$filename=$dir.$cover;
					move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
					$cover_url=$filename;

					$remove_files[]=$event_arr['cover_url'];//remove old file
					$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
				}
				else{
					$errors[]='&mdash; Cover size must be less than 2MB';
				}
			}
			else{
				$errors[]='&mdash; Cover must be in JPG, JPEG, PNG or SVG format';
			}
		}
		if(isset($_FILES['speakers_cover']) && 0==$_FILES['speakers_cover']['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png','svg'];
			$ext=pathinfo($_FILES['speakers_cover']['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES['speakers_cover']['size']<=1024*1024*2){//check size 2MB
					$speakers_cover=md5($url).'_speakers_cover_'.time().'.'.$ext;
					$filename=$dir.$speakers_cover;
					move_uploaded_file($_FILES['speakers_cover']['tmp_name'],$root_dir.$filename);
					$speakers_cover_url=$filename;

					$remove_files[]=$event_arr['speakers_cover_url'];//remove old file
					$files[]=['time'=>time(),'name'=>$_FILES['speakers_cover']['name'],'size'=>$_FILES['speakers_cover']['size'],'type'=>$_FILES['speakers_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
				}
				else{
					$errors[]='&mdash; Speakers cover size must be less than 2MB';
				}
			}
			else{
				$errors[]='&mdash; Speakers cover must be in JPG, JPEG, PNG or SVG format';
			}
		}
		if(isset($_FILES['location_cover']) && 0==$_FILES['location_cover']['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png','svg'];
			$ext=pathinfo($_FILES['location_cover']['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES['location_cover']['size']<=1024*1024*2){//check size 2MB
					$location_cover=md5($url).'_location_cover_'.time().'.'.$ext;
					$filename=$dir.$location_cover;
					move_uploaded_file($_FILES['location_cover']['tmp_name'],$root_dir.$filename);
					$location_cover_url=$filename;

					$remove_files[]=$event_arr['location_cover_url'];//remove old file
					$files[]=['time'=>time(),'name'=>$_FILES['location_cover']['name'],'size'=>$_FILES['location_cover']['size'],'type'=>$_FILES['location_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
				}
				else{
					$errors[]='&mdash; Location cover size must be less than 2MB';
				}
			}
			else{
				$errors[]='&mdash; Location cover must be in JPG, JPEG, PNG or SVG format';
			}
		}
		if(isset($_FILES['partners_cover']) && 0==$_FILES['partners_cover']['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png','svg'];
			$ext=pathinfo($_FILES['partners_cover']['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES['partners_cover']['size']<=1024*1024*2){//check size 2MB
					$partners_cover=md5($url).'_partners_cover_'.time().'.'.$ext;
					$filename=$dir.$partners_cover;
					move_uploaded_file($_FILES['partners_cover']['tmp_name'],$root_dir.$filename);
					$partners_cover_url=$filename;

					$remove_files[]=$event_arr['partners_cover_url'];//remove old file
					$files[]=['time'=>time(),'name'=>$_FILES['partners_cover']['name'],'size'=>$_FILES['partners_cover']['size'],'type'=>$_FILES['partners_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
				}
				else{
					$errors[]='&mdash; Partners cover size must be less than 2MB';
				}
			}
			else{
				$errors[]='&mdash; Partners cover must be in JPG, JPEG, PNG or SVG format';
			}
		}

		$start_time=0;
		if(isset($_POST['unixtime'])){
			$start_time=(int)$_POST['unixtime'];
		}
		if($start_time!=$event_arr['time']){//time was changed
			//https://gitlab.com/envelop/tickets/getpass/-/issues/20#note_1395411570
			if($start_time<=(time()+3600-180)){//1 hour (minus 3 minutes for safety) from now
				$errors[]='&mdash; Event time must be at least 1 hour in the future';
			}
		}
		$status=0;
		if(isset($events_status_arr[$_POST['status']])){
			$status=(int)$_POST['status'];
		}

		if(1==$status){//event new status is active
			if(1!=$event_arr['moderation']){//event was not approved
				//get all admins and send them notification about event moderation request
				$platform_admins=$db->sql("SELECT `id` FROM `addresses` WHERE `status`='1'");
				foreach($platform_admins as $platform_admin){
					add_notify($platform_admin['id'],0,1,'admin_moderation_event',json_encode([
						'organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url']),
						'event_title'=>htmlspecialchars($title),
						'event_url'=>htmlspecialchars($url)
					]));
				}
			}
		}

		if(0==count($errors)){
			$db->sql("UPDATE `events` SET `url`='".$db->prepare($url)."',`title`='".$db->prepare($title)."',`short_description`='".$db->prepare($short_description)."',`description`='".$db->prepare($description)."',`short_location_description`='".$db->prepare($short_location_description)."',`location_description`='".$db->prepare($location_description)."',`speakers_description`='".$db->prepare($speakers_description)."',`tickets_description`='".$db->prepare($tickets_description)."',`check_in_message`='".$db->prepare($check_in_message)."',`time`='".$start_time."',`status`='".$status."',`logo_url`='".$db->prepare($logo_url)."',`cover_url`='".$db->prepare($cover_url)."',`speakers_cover_url`='".$db->prepare($speakers_cover_url)."',`location_cover_url`='".$db->prepare($location_cover_url)."',`partners_cover_url`='".$db->prepare($partners_cover_url)."' WHERE `id`='".$event_id."'");
			print '
			<div class="success-box" role="alert">
				<p class="font-bold">Success</p>
				<p>Event <a href="/@'.htmlspecialchars($organizer_url).'/'.htmlspecialchars($url).'/manage/event/">"'.htmlspecialchars($url).'"</a> was updated</p>
			</div>';
			print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/event/">';

			//deleting old files
			foreach($remove_files as $file){
				$free_file_size=filesize($root_dir.$file);
				unlink($root_dir.$file);
				if(0!=$db->table_count('event_files',"WHERE `path`='".$db->prepare($root_dir.$file)."'")){
					$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
					$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
				}
				if(0!=$db->table_count('organizer_files',"WHERE `path`='".$db->prepare($root_dir.$file)."'")){
					$db->sql("DELETE FROM `organizer_files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
				}
			}
			//add files to database
			foreach($files as $file){
				$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."',`event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$event_id."',`address`='".$allow_event_manage_address_id."'");
				$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
				$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
			}
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>'.implode('<br>',$errors).'</p>
			</div>';
			print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

			//deleting temporarily uploaded files
			foreach($files as $file){
				unlink($file['path']);
			}
		}
	}
	else{
		print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
		print '<div class="max-w-fit">';
		$object_scheme=[//event edit info
			'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/<b>URL</b>/ page address bar.','required'=>true],
			'title'=>['type'=>'text','title'=>'Title','class'=>'title-part','required'=>true],
			'short_description'=>['type'=>'textarea','title'=>'Short description','descr'=>'Annotation for preview & seo'],

			'logo'=>[
				'type'=>'image',
				'title'=>'🖼️ Logo image',
				'placeholder'=>'Logo',
				'object_type'=>'event',
				'file_path'=>'logo_url',
				'descr'=>'The recommended sizes are <b>60x60</b> pixels, 500Kb, please do not exceed them.<br>For best quality use <b>svg</b> or <b>png</b> with transparent background.'
			],

			'collapse_main_part'=>['type'=>'start-collapse','title'=>'Main section'],
			'cover'=>[
				'type'=>'image',
				'title'=>'🖼️ Cover image',
				'placeholder'=>'Cover',
				'object_type'=>'event',
				'file_path'=>'cover_url',
				'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
			],
			'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'Description (annotation)','wysiwyg'=>true],
			['type'=>'end-collapse'],

			'collapse_location_part'=>['type'=>'start-collapse','title'=>'Location section'],
			'short_location_description'=>['type'=>'text','title'=>'Short location description','descr'=>'For event preview card'],
			'location_cover'=>[
				'type'=>'image',
				'title'=>'🖼️ Location cover image',
				'placeholder'=>'Location cover',
				'object_type'=>'event',
				'file_path'=>'location_cover_url',
				'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
			],
			'location_description'=>['type'=>'textarea','title'=>'📄 Location description','placeholder'=>'Location description','wysiwyg'=>true],
			['type'=>'end-collapse'],

			'collapse_speakers_part'=>['type'=>'start-collapse','title'=>'Speakers section'],
			'speakers_cover'=>[
				'type'=>'image',
				'title'=>'🖼️ Speakers cover image',
				'placeholder'=>'Speakers cover',
				'object_type'=>'event',
				'file_path'=>'speakers_cover_url',
				'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
			],
			'speakers_description'=>['type'=>'textarea','title'=>'📄 Speakers description','placeholder'=>'Speakers description','wysiwyg'=>true],
			['type'=>'end-collapse'],

			'collapse_tickets_part'=>['type'=>'start-collapse','title'=>'Tickets section'],
			'tickets_description'=>['type'=>'textarea','title'=>'📄 Tickets description','placeholder'=>'Tickets description','wysiwyg'=>true],
			'check_in_message'=>['type'=>'textarea','title'=>'📄 Check-in message','placeholder'=>'Check-in message','wysiwyg'=>true],
			['type'=>'end-collapse'],

			'collapse_partners_part'=>['type'=>'start-collapse','title'=>'Partners section'],
			'partners_cover'=>[
				'type'=>'image',
				'title'=>'🖼️ Partners cover image',
				'placeholder'=>'Partners cover',
				'object_type'=>'event',
				'file_path'=>'partners_cover_url',
				'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
			],
			['type'=>'end-collapse'],

			['type'=>'hr'],
			'datetime'=>[
				'key'=>'time',
				'type'=>'datetime',
				'local'=>true,//-local
				'future'=>true,
				'minimal-offset'=>(3600),//1 hour
				'title'=>'Start date time (by your local time)',
				'timestamp'=>'unixtime',
				'value'=>0
			],
			'status'=>[
				'type'=>'select',
				'title'=>'Status',
				'options'=>$events_status_arr,
				'default_option'=>0,
				'classes'=>$events_status_style_arr,
				'descr'=>(1==$event_arr['moderation']?'Event already approved by moderation':'Choose active status to submit event for moderation'),
			],
		];
		print build_form($object_scheme,$event_arr);
		print '</div>';
		print '<div class="grid-wrapper">';
		print '<input type="submit" value="Update event" class="action-btn big">';
		print '</div>';
		print '</form>';
	}
}
elseif('customize'==$path_array[4]){
	$replace['title']='Customize | '.$replace['title'];
	print '<h2>Customize event</h2>';
	print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';
	if(isset($_POST['avatar_style'])){
		$errors=[];
		$new_customize=$customize;

		if(in_array($_POST['logo_style'],array_keys($logo_style))){
			$new_customize['logo_style']=(int)$_POST['logo_style'];
		}
		else{
			$errors[]='Wrong logo style';
		}

		if(in_array($_POST['logo_border'],array_keys($logo_border))){
			$new_customize['logo_border']=(int)$_POST['logo_border'];
		}
		else{
			$errors[]='Wrong logo border';
		}

		if(in_array($_POST['avatar_style'],array_keys($avatar_style))){
			$new_customize['avatar_style']=(int)$_POST['avatar_style'];
		}
		else{
			$errors[]='Wrong avatar style';
		}

		$new_customize['default_theme']=1;
		if(isset($_POST['default_theme'])){
			$_POST['default_theme']=(int)$_POST['default_theme'];
			if(isset($config['platform_themes'][$_POST['default_theme']])){
				$new_customize['default_theme']=$_POST['default_theme'];
			}
		}

		$new_customize['theme_changer']=true;//by default enabled
		if(isset($_POST['theme_changer'])){
			if(1==$_POST['theme_changer']){
				$new_customize['theme_changer']=true;
			}
		}
		else{
			//if not set - disable
			$new_customize['theme_changer']=false;
		}

		$new_customize['template_parts_disabled']=[];
		if(isset($_POST['template_parts_disabled'])){
			foreach($_POST['template_parts_disabled'] as $key=>$value){
				$new_customize['template_parts_disabled'][]=$key;
			}
		}

		$new_customize['template_parts']=$event_template_parts;
		if(isset($_POST['template_parts'])){
			$new_customize['template_parts']=[];
			$parts_arr=explode(',',$_POST['template_parts']);
			foreach($parts_arr as $part){
				if(in_array($part,$event_template_parts)){
					$new_customize['template_parts'][]=$part;
				}
			}
		}

		//customize main cover section
		if(in_array($_POST['cover_background_color'],array_keys($cover_background_color_overlay_options))){
			$new_customize['cover_background_color']=(int)$_POST['cover_background_color'];
		}
		else{
			$errors[]='Wrong cover background color';
		}

		if(in_array($_POST['cover_title_styles'],array_keys($cover_text_styles_options))){
			$new_customize['cover_title_styles']=(int)$_POST['cover_title_styles'];
		}
		else{
			$errors[]='Wrong cover title styles';
		}

		if(in_array($_POST['cover_descr_styles'],array_keys($cover_text_styles_options))){
			$new_customize['cover_descr_styles']=(int)$_POST['cover_descr_styles'];
		}
		else{
			$errors[]='Wrong cover description styles';
		}

		if(in_array($_POST['cover_title_stroke_width'],array_keys($cover_title_stroke_width_options))){
			$new_customize['cover_title_stroke_width']=(int)$_POST['cover_title_stroke_width'];
		}
		else{
			$errors[]='Wrong cover title stroke width';
		}

		if(in_array($_POST['cover_descr_text_opacity'],array_keys($cover_descr_text_opacity_options))){
			$new_customize['cover_descr_text_opacity']=(int)$_POST['cover_descr_text_opacity'];
		}
		else{
			$errors[]='Wrong cover description text opacity';
		}

		if(isset($_POST['cover_title_text'])){
			$new_customize['cover_title_text']=htmlspecialchars($_POST['cover_title_text']);
		}
		if(isset($_POST['cover_descr_text'])){
			$new_customize['cover_descr_text']=htmlspecialchars($_POST['cover_descr_text']);
		}

		//customize speakers cover section
		if(in_array($_POST['speakers_cover_background_color'],array_keys($cover_background_color_overlay_options))){
			$new_customize['speakers_cover_background_color']=(int)$_POST['speakers_cover_background_color'];
		}
		else{
			$errors[]='Wrong speakers cover background color';
		}

		if(in_array($_POST['speakers_cover_title_styles'],array_keys($cover_text_styles_options))){
			$new_customize['speakers_cover_title_styles']=(int)$_POST['speakers_cover_title_styles'];
		}
		else{
			$errors[]='Wrong speakers cover title styles';
		}

		if(in_array($_POST['speakers_cover_descr_styles'],array_keys($cover_text_styles_options))){
			$new_customize['speakers_cover_descr_styles']=(int)$_POST['speakers_cover_descr_styles'];
		}
		else{
			$errors[]='Wrong speakers cover description styles';
		}

		if(in_array($_POST['speakers_cover_title_stroke_width'],array_keys($cover_title_stroke_width_options))){
			$new_customize['speakers_cover_title_stroke_width']=(int)$_POST['speakers_cover_title_stroke_width'];
		}
		else{
			$errors[]='Wrong speakers cover title stroke width';
		}

		if(in_array($_POST['speakers_cover_descr_text_opacity'],array_keys($cover_descr_text_opacity_options))){
			$new_customize['speakers_cover_descr_text_opacity']=(int)$_POST['speakers_cover_descr_text_opacity'];
		}
		else{
			$errors[]='Wrong speakers cover description text opacity';
		}

		if(isset($_POST['speakers_cover_title_text'])){
			$new_customize['speakers_cover_title_text']=htmlspecialchars($_POST['speakers_cover_title_text']);
		}
		if(isset($_POST['speakers_cover_descr_text'])){
			$new_customize['speakers_cover_descr_text']=htmlspecialchars($_POST['speakers_cover_descr_text']);
		}

		//customize location cover section
		if(in_array($_POST['location_cover_background_color'],array_keys($cover_background_color_overlay_options))){
			$new_customize['location_cover_background_color']=(int)$_POST['location_cover_background_color'];
		}
		else{
			$errors[]='Wrong location cover background color';
		}

		if(in_array($_POST['location_cover_title_styles'],array_keys($cover_text_styles_options))){
			$new_customize['location_cover_title_styles']=(int)$_POST['location_cover_title_styles'];
		}
		else{
			$errors[]='Wrong location cover title styles';
		}

		if(in_array($_POST['location_cover_descr_styles'],array_keys($cover_text_styles_options))){
			$new_customize['location_cover_descr_styles']=(int)$_POST['location_cover_descr_styles'];
		}
		else{
			$errors[]='Wrong location cover description styles';
		}

		if(in_array($_POST['location_cover_title_stroke_width'],array_keys($cover_title_stroke_width_options))){
			$new_customize['location_cover_title_stroke_width']=(int)$_POST['location_cover_title_stroke_width'];
		}
		else{
			$errors[]='Wrong location cover title stroke width';
		}

		if(in_array($_POST['location_cover_descr_text_opacity'],array_keys($cover_descr_text_opacity_options))){
			$new_customize['location_cover_descr_text_opacity']=(int)$_POST['location_cover_descr_text_opacity'];
		}
		else{
			$errors[]='Wrong location cover description text opacity';
		}

		if(isset($_POST['location_cover_title_text'])){
			$new_customize['location_cover_title_text']=htmlspecialchars($_POST['location_cover_title_text']);
		}
		if(isset($_POST['location_cover_descr_text'])){
			$new_customize['location_cover_descr_text']=htmlspecialchars($_POST['location_cover_descr_text']);
		}

		//customize partners cover section
		if(in_array($_POST['partners_cover_background_color'],array_keys($cover_background_color_overlay_options))){
			$new_customize['partners_cover_background_color']=(int)$_POST['partners_cover_background_color'];
		}
		else{
			$errors[]='Wrong partners cover background color';
		}

		if(in_array($_POST['partners_cover_title_styles'],array_keys($cover_text_styles_options))){
			$new_customize['partners_cover_title_styles']=(int)$_POST['partners_cover_title_styles'];
		}
		else{
			$errors[]='Wrong partners cover title styles';
		}

		if(in_array($_POST['partners_cover_descr_styles'],array_keys($cover_text_styles_options))){
			$new_customize['partners_cover_descr_styles']=(int)$_POST['partners_cover_descr_styles'];
		}
		else{
			$errors[]='Wrong partners cover description styles';
		}

		if(in_array($_POST['partners_cover_title_stroke_width'],array_keys($cover_title_stroke_width_options))){
			$new_customize['partners_cover_title_stroke_width']=(int)$_POST['partners_cover_title_stroke_width'];
		}
		else{
			$errors[]='Wrong partners cover title stroke width';
		}

		if(in_array($_POST['partners_cover_descr_text_opacity'],array_keys($cover_descr_text_opacity_options))){
			$new_customize['partners_cover_descr_text_opacity']=(int)$_POST['partners_cover_descr_text_opacity'];
		}
		else{
			$errors[]='Wrong partners cover description text opacity';
		}

		if(isset($_POST['partners_cover_title_text'])){
			$new_customize['partners_cover_title_text']=htmlspecialchars($_POST['partners_cover_title_text']);
		}
		if(isset($_POST['partners_cover_descr_text'])){
			$new_customize['partners_cover_descr_text']=htmlspecialchars($_POST['partners_cover_descr_text']);
		}

		if(isset($_POST['link_platform_notice'])){
			$new_customize['link_platform_notice']=htmlspecialchars($_POST['link_platform_notice']);
		}

		$customize_json=json_encode($new_customize);

		if(0==count($errors)){
			$db->sql("UPDATE `events` SET `customize`='".$db->prepare($customize_json)."' WHERE `id`='".$event_id."'");
			print '
			<div class="success-box" role="alert">
				<p class="font-bold">Success</p>
				<p>Event <a href="/@'.htmlspecialchars($organizer_url).'/'.htmlspecialchars($event_url).'/manage/customize/">"'.htmlspecialchars($event_url).'"</a> was customized</p>
			</div>';
			print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/customize/">';
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>'.implode('<br>',$errors).'</p>
			</div>';
			print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
		}
	}
	else{
		print '<form action="" method="POST" class="manage-card">';
		print '<div class="max-w-fit">';

		$new_customize['template_parts']=[];
		if(isset($customize['template_parts'])){
			$new_customize['template_parts']=$customize['template_parts'];
		}
		//add parts that dont exist in customize from default
		foreach($event_template_parts as $part){
			if(!in_array($part,$new_customize['template_parts'])){
				$new_customize['template_parts'][]=$part;
			}
		}
		//remove parts that dont exist in default from customize
		foreach($new_customize['template_parts'] as $key=>$part){
			if(!in_array($part,$event_template_parts)){
				unset($new_customize['template_parts'][$key]);
			}
		}

		$object_scheme=[//event customize
			'logo_style'=>[
				'type'=>'select',
				'title'=>'Logo style',
				'options'=>$logo_style,
				'caption_value'=>'caption',
				'default_option'=>0,
			],
			'logo_border'=>[
				'type'=>'select',
				'title'=>'Logo border',
				'options'=>$logo_border,
				'caption_value'=>'caption',
				'default_option'=>0,
			],
			'avatar_style'=>[
				'type'=>'select',
				'title'=>'Speakers avatar',
				'options'=>$avatar_style,
				'caption_value'=>'caption',
				'default_option'=>0,
			],
			['type'=>'hr'],
			'default_theme'=>['type'=>'select','title'=>'Default theme','options'=>$config['platform_themes'],'default_option'=>0],
			'theme_changer'=>['type'=>'checkbox','title'=>'Allow user to change theme','value'=>true],
			['type'=>'hr'],
			'link_platform_notice'=>['type'=>'select','title'=>'Notice users to link platform','options'=>['Without notice','Attention box notice','Popup notice'],'default_option'=>0],
			['type'=>'hr'],

			'collapse_template_parts'=>['type'=>'start-collapse','title'=>'Template parts'],
			'template_parts_disabled'=>[
				'type'=>'multi-checkbox',
				'title'=>'Disable template parts on event page',
				'options'=>$event_template_parts,
				'value_function'=>function($option){
					return ucwords($option);
				},
				'checked_function'=>function($option,$object){
					return in_array($option,$object['template_parts_disabled']);
				},
			],
			['type'=>'hr'],
			'template_parts'=>[
				'type'=>'sort',
				'title'=>'Sort event template parts',
				'options'=>$new_customize['template_parts'],
				'caption_function'=>function($option){
					return ucwords($option);
				}
			],
			['type'=>'end-collapse'],

			'customize_main_cover'=>['type'=>'start-collapse','title'=>'Customize main cover'],
			'cover_background_color'=>[
				'type'=>'select',
				'title'=>'Background color overlay',
				'options'=>$cover_background_color_overlay_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'cover_title_text'=>[
				'type'=>'textarea',
				'title'=>'Title text',
				'placeholder'=>'Title',
				//'wysiwyg'=>true,//not going well for ui, need strict rules
			],
			'cover_title_styles'=>[
				'type'=>'select',
				'title'=>'Title styles',
				'options'=>$cover_text_styles_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'cover_title_stroke_width'=>[
				'type'=>'select',
				'title'=>'Title stroke width',
				'options'=>$cover_title_stroke_width_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'cover_descr_text'=>[
				'type'=>'textarea',
				'title'=>'Description text',
				'placeholder'=>'Text',
				//'wysiwyg'=>true,//not going well for ui, need strict rules
			],
			'cover_descr_styles'=>[
				'type'=>'select',
				'title'=>'Description styles',
				'options'=>$cover_text_styles_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'cover_descr_text_opacity'=>[
				'type'=>'select',
				'title'=>'Description text opacity',
				'options'=>$cover_descr_text_opacity_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			['type'=>'end-collapse'],

			'customize_speakers_cover'=>['type'=>'start-collapse','title'=>'Customize speakers cover'],
			'speakers_cover_background_color'=>[
				'type'=>'select',
				'title'=>'Background color overlay',
				'options'=>$cover_background_color_overlay_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'speakers_cover_title_text'=>[
				'type'=>'textarea',
				'title'=>'Title text',
				'placeholder'=>'Title',
				//'wysiwyg'=>true,//not going well for ui, need strict rules
			],
			'speakers_cover_title_styles'=>[
				'type'=>'select',
				'title'=>'Title styles',
				'options'=>$cover_text_styles_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'speakers_cover_title_stroke_width'=>[
				'type'=>'select',
				'title'=>'Title stroke width',
				'options'=>$cover_title_stroke_width_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'speakers_cover_descr_text'=>[
				'type'=>'textarea',
				'title'=>'Description text',
				'placeholder'=>'Text',
				//'wysiwyg'=>true,//not going well for ui, need strict rules
			],
			'speakers_cover_descr_styles'=>[
				'type'=>'select',
				'title'=>'Description styles',
				'options'=>$cover_text_styles_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'speakers_cover_descr_text_opacity'=>[
				'type'=>'select',
				'title'=>'Description text opacity',
				'options'=>$cover_descr_text_opacity_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			['type'=>'end-collapse'],

			'customize_location_cover'=>['type'=>'start-collapse','title'=>'Customize location cover'],
			'location_cover_background_color'=>[
				'type'=>'select',
				'title'=>'Background color overlay',
				'options'=>$cover_background_color_overlay_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'location_cover_title_text'=>[
				'type'=>'textarea',
				'title'=>'Title text',
				'placeholder'=>'Title',
				//'wysiwyg'=>true,//not going well for ui, need strict rules
			],
			'location_cover_title_styles'=>[
				'type'=>'select',
				'title'=>'Title styles',
				'options'=>$cover_text_styles_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'location_cover_title_stroke_width'=>[
				'type'=>'select',
				'title'=>'Title stroke width',
				'options'=>$cover_title_stroke_width_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'location_cover_descr_text'=>[
				'type'=>'textarea',
				'title'=>'Description text',
				'placeholder'=>'Text',
				//'wysiwyg'=>true,//not going well for ui, need strict rules
			],
			'location_cover_descr_styles'=>[
				'type'=>'select',
				'title'=>'Description styles',
				'options'=>$cover_text_styles_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'location_cover_descr_text_opacity'=>[
				'type'=>'select',
				'title'=>'Description text opacity',
				'options'=>$cover_descr_text_opacity_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			['type'=>'end-collapse'],

			'customize_partners_cover'=>['type'=>'start-collapse','title'=>'Customize partners cover'],
			'partners_cover_background_color'=>[
				'type'=>'select',
				'title'=>'Background color overlay',
				'options'=>$cover_background_color_overlay_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'partners_cover_title_text'=>[
				'type'=>'textarea',
				'title'=>'Title text',
				'placeholder'=>'Title',
				//'wysiwyg'=>true,//not going well for ui, need strict rules
			],
			'partners_cover_title_styles'=>[
				'type'=>'select',
				'title'=>'Title styles',
				'options'=>$cover_text_styles_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'partners_cover_title_stroke_width'=>[
				'type'=>'select',
				'title'=>'Title stroke width',
				'options'=>$cover_title_stroke_width_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'partners_cover_descr_text'=>[
				'type'=>'textarea',
				'title'=>'Description text',
				'placeholder'=>'Text',
				//'wysiwyg'=>true,//not going well for ui, need strict rules
			],
			'partners_cover_descr_styles'=>[
				'type'=>'select',
				'title'=>'Description styles',
				'options'=>$cover_text_styles_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			'partners_cover_descr_text_opacity'=>[
				'type'=>'select',
				'title'=>'Description text opacity',
				'options'=>$cover_descr_text_opacity_options,
				'caption_value'=>'caption',
				'default_option'=>0
			],
			['type'=>'end-collapse'],
		];
		print build_form($object_scheme,$customize);
		print '</div>';
		print '<div class="grid-wrapper">';
		print '<input type="submit" value="Save" class="action-btn big">';
		print '</div>';
		print '</form>';
	}
}
elseif('delete_file'==$path_array[4]){
	ob_end_clean();
	ob_end_clean();
	$object_type=$path_array[5];
	$object_id=(int)$path_array[6];

	if('speakers'==$object_type){
		if(check_csrf()){
			$speaker_arr=$db->sql_row("SELECT * FROM `event_speakers` WHERE `id`='".$object_id."' AND `event`='".$event_id."'");
			if(null!==$speaker_arr){
				$file_type=$db->prepare($path_array[7]);

				$photo_types_arr=array('photo_url','small_photo_url','tiny_photo_url');
				if(!in_array($file_type,$photo_types_arr)){
					header('HTTP/1.0 403 Forbidden');
					exit;
				}
				$result=['status'=>false];
				foreach($photo_types_arr as $photo_type){
					if(file_exists($root_dir.$speaker_arr[$photo_type])){
						$free_file_size=filesize($root_dir.$speaker_arr[$photo_type]);
						unlink($root_dir.$speaker_arr[$photo_type]);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$speaker_arr[$photo_type])."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					$db->sql("UPDATE `event_speakers` SET `".$photo_type."`='' WHERE `id`='".$object_id."'");
					$result=['status'=>true,'message'=>'✓ File successfully deleted'];
				}
				if($result['status']){
					print json_encode($result);
					exit;
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	if('sessions'==$object_type){
		if(check_csrf()){
			$session_arr=$db->sql_row("SELECT * FROM `event_sessions` WHERE `id`='".$object_id."' AND `event`='".$event_id."'");
			if(null!==$session_arr){
				$file_type=$db->prepare($path_array[7]);

				$photo_types_arr=array('cover_url','thumbnail_cover_url');
				if(!in_array($file_type,$photo_types_arr)){
					header('HTTP/1.0 403 Forbidden');
					exit;
				}
				$result=['status'=>false];
				foreach($photo_types_arr as $photo_type){
					if(file_exists($root_dir.$session_arr[$photo_type])){
						$free_file_size=filesize($root_dir.$session_arr[$photo_type]);
						unlink($root_dir.$session_arr[$photo_type]);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$session_arr[$photo_type])."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					$db->sql("UPDATE `event_sessions` SET `".$photo_type."`='' WHERE `id`='".$object_id."'");
					$result=['status'=>true,'message'=>'✓ File successfully deleted'];
				}
				if($result['status']){
					print json_encode($result);
					exit;
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	if('partners'==$object_type){
		if(check_csrf()){
			$partner_arr=$db->sql_row("SELECT * FROM `event_partners` WHERE `id`='".$object_id."' AND `event`='".$event_id."'");
			if(null!==$partner_arr){
				$file_type=$db->prepare($path_array[7]);

				$photo_types_arr=array('logo_url','small_logo_url');
				if(!in_array($file_type,$photo_types_arr)){
					header('HTTP/1.0 403 Forbidden');
					exit;
				}
				$result=['status'=>false];
				foreach($photo_types_arr as $photo_type){
					if(file_exists($root_dir.$partner_arr[$photo_type])){
						$free_file_size=filesize($root_dir.$partner_arr[$photo_type]);
						unlink($root_dir.$partner_arr[$photo_type]);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$partner_arr[$photo_type])."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					$db->sql("UPDATE `event_partners` SET `".$photo_type."`='' WHERE `id`='".$object_id."'");
					$result=['status'=>true,'message'=>'✓ File successfully deleted'];
				}
				if($result['status']){
					print json_encode($result);
					exit;
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	if('event'==$object_type){
		if(check_csrf()){
			if($object_id==$event_id){
				$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."'");
				if(null!==$event_arr){
					$file_type=$db->prepare($path_array[7]);

					$event_files_arr=array('logo_url','cover_url','speakers_cover_url','location_cover_url','partners_cover_url');

					$result=['status'=>false];
					if(in_array($file_type,$event_files_arr)){
						if(file_exists($root_dir.$event_arr[$file_type])){
							$free_file_size=filesize($root_dir.$event_arr[$file_type]);
							unlink($root_dir.$event_arr[$file_type]);
							$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$event_arr[$file_type])."'");
							$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
						$db->sql("UPDATE `events` SET `".$file_type."`='' WHERE `id`='".$object_id."'");
						$result=['status'=>true,'message'=>'✓ File successfully deleted'];
					}
					if($result['status']){
						print json_encode($result);
						exit;
					}
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	if('content'==$object_type){
		if(check_csrf()){
			$content_arr=$db->sql_row("SELECT * FROM `event_content` WHERE `id`='".$object_id."' AND `event`='".$event_id."'");
			if(null!==$content_arr){
				$result=['status'=>false];
				$object_key=$db->prepare($path_array[7]);
				if(isset($content_arr[$object_key])){
					if($content_arr[$object_key]){
						if(file_exists($root_dir.$content_arr[$object_key])){
							$free_file_size=filesize($root_dir.$content_arr[$object_key]);
							unlink($root_dir.$content_arr[$object_key]);
							$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$content_arr[$object_key])."'");
							$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
						$db->sql("UPDATE `event_content` SET `".$object_key."`='' WHERE `id`='".$object_id."'");
						$result=['status'=>true,'message'=>'✓ File successfully deleted'];
					}
				}
				$search_thumbnail_key='thumbnail_'.$object_key;
				if(isset($content_arr[$search_thumbnail_key])){
					if($content_arr[$search_thumbnail_key]){
						if(file_exists($root_dir.$content_arr[$search_thumbnail_key])){
							$free_file_size=filesize($root_dir.$content_arr[$search_thumbnail_key]);
							unlink($root_dir.$content_arr[$search_thumbnail_key]);
							$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$content_arr[$search_thumbnail_key])."'");
							$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
						$db->sql("UPDATE `event_content` SET `".$search_thumbnail_key."`='' WHERE `id`='".$object_id."'");
						$result=['status'=>true,'message'=>'✓ File & thumbnail successfully deleted'];
					}
				}
				if($result['status']){
					print json_encode($result);
					exit;
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	header('HTTP/1.0 404 Not Found');
	exit;
}
elseif('users'==$path_array[4]){
	$replace['title']='Users | '.$replace['title'];
	$user_id=false;
	if('download'==$path_array['5']){
		if($event_user['admin']&&check_csrf()){
			ob_end_clean();
			ob_end_clean();
			$csv_data='';
			$csv_data.='"Type";"Address";"Admin";"Manager";"Speaker";"Sponsor";"Level";"Status";"Time";';
			$users=$db->sql("SELECT * FROM `event_users` WHERE `event`='".$event_id."' ORDER BY `id` ASC");
			foreach($users as $user){
				$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$user['address']."'");
				$csv_data.=PHP_EOL;
				$csv_data.=csv_prepare($types_arr[$address_arr['type']]['name']).';';
				$csv_data.=csv_prepare($address_arr['address']).';';
				$csv_data.=csv_prepare($user['admin']?'1':'0').';';
				$csv_data.=csv_prepare($user['manager']?'1':'0').';';
				$csv_data.=csv_prepare($user['speaker']?'1':'0').';';
				$csv_data.=csv_prepare($user['sponsor']?'1':'0').';';
				$csv_data.=csv_prepare($user['level']).';';
				$csv_data.=csv_prepare($user_status_arr[$user['status']]).';';
				if(2==$user['status']){//invited
					$invite_arr=$db->sql_row("SELECT * FROM `event_invites` WHERE `id`='".$user['invited']."'");
					$csv_data.=csv_prepare(date('c',$invite_arr['time'])).';';
				}
				if(1==$user['status']){//binded
					$nft_arr=$db->sql_row("SELECT * FROM `nft_list` WHERE `id`='".$db->prepare($user['binded'])."'");
					$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$db->prepare($user['binded'])."'");
					$whitelist_arr=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `id`='".$binded_nft_arr['whitelist']."'");
					$csv_data.=csv_prepare(date('c',$whitelist_arr['time'])).';';
				}
			}
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename=users-'.(date('d-m-Y-H-i')).'.csv');
			header("Pragma: no-cache");
			header("Expires: 0");
			print $csv_data;
			exit;
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
			</div>';
			$path_array['5']='';
		}
	}
	if($path_array[5]){
		$user_id=$db->prepare($path_array[5]);
		$user_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_id."' AND `id`='".$user_id."'");
		if($user_arr){
			print '<h2>User card #'.$user_arr['id'].'</h2>';
			print '<hr>';
			if(isset($_GET['toggle_flag'])){
				if(check_csrf()){
					$flag=$db->prepare($_GET['toggle_flag']);
					if(in_array($flag,$event_flags_arr)){
						$allow=false;
						if($event_user['admin']){
							$allow=true;
						}
						if($event_user['manager'] && $flag=='speaker'){
							$allow=true;
						}
						if($event_user['manager'] && $flag=='sponsor'){
							$allow=true;
						}
						if($allow){
							$db->sql("UPDATE `event_users` SET `".$flag."`='".($user_arr[$flag]?0:1)."' WHERE `event`='".$event_id."' AND `id`='".$user_id."'");
						}
						header('location:/@'.$organizer_url.'/'.$event_url.'/manage/users/'.$user_id.'/');
						exit;
					}
				}
				else{
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
					</div>';
				}
			}
			if(isset($_GET['level'])){
				$level=intval($_GET['level']);
				if($level>100){
					$level=100;
				}
				if($level<0){
					$level=0;
				}
				$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `level`='".$level."'");
				if(null!==$level_arr){
					$db->sql("UPDATE `event_users` SET `level`='".$level."' WHERE `event`='".$event_id."' AND `id`='".$user_id."'");
					header('location:/@'.$organizer_url.'/'.$event_url.'/manage/users/'.$user_id.'/');
					exit;
				}
			}
			print '<div class="card-content">';
			$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$user_arr['address']."'");

			print '<p>Address type: '.htmlspecialchars($types_arr[$address_arr['type']]['name']).'</p>';
			print '<p>Address: '.htmlspecialchars($address_arr['address']).'</p>';
			print '<hr><p>Status: ';
			if(0==$user_arr['status']){
				print '<span class="'.$user_status_style_arr[$user_arr['status']].'">'.$user_status_arr[$user_arr['status']].'</span>';
			}
			if(1==$user_arr['status']){//binded
				$nft_arr=$db->sql_row("SELECT * FROM `nft_list` WHERE `id`='".$db->prepare($user_arr['binded'])."'");
				$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$db->prepare($user_arr['binded'])."'");
				$binded_whitelist_arr=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `id`='".$db->prepare($binded_nft_arr['whitelist'])."'");

				print '<span class="'.$user_status_style_arr[$user_arr['status']].'">'.$user_status_arr[$user_arr['status']].'</span>';
				if($binded_whitelist_arr){
					$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$binded_whitelist_arr['chain']."'");
					print ' by <a href="'.ltmp($chain_arr['nft_url'],['contract'=>$binded_whitelist_arr['contract'],'token_id'=>$binded_whitelist_arr['token_id']]).'" target="_blank" class="text-green-600">'.$binded_whitelist_arr['contract'].' token id '.$binded_whitelist_arr['token_id'].'</a>';
				}
			}
			if(2==$user_arr['status']){//invited
				$invite_arr=$db->sql_row("SELECT * FROM `event_invites` WHERE `id`='".$db->prepare($user_arr['invited'])."'");
				if($invite_arr){
					print ' invited as ';
					if($invite_arr['type']){
						print $invite_arr['address'];
						print ' ('.$types_arr[$invite_arr['type']]['name'].')';
					}
					if($invite_arr['platform']){
						$platform_arr=$db->sql_row("SELECT * FROM `platforms` WHERE `id`='".$invite_arr['platform']."'");
						if($invite_arr['internal_username']){
							print ' '.htmlspecialchars($invite_arr['internal_username']).'';
							print ' ('.htmlspecialchars($invite_arr['internal_id']).')';
						}
						else{
							print ' '.htmlspecialchars($invite_arr['internal_id']).'';
						}
						print ' from '.htmlspecialchars($platform_arr['name']);
					}
				}
			}
			print '<form action="" method="GET">';
			print '<p><b>Level:</b>';
			print '<select class="max-w-fit" name="level" onchange="$(this).closest(\'form\')[0].submit()">';
				foreach($levels_arr as $level_id=>$level_arr){
					print '<option value="'.$level_arr['level'].'"'.($user_arr['level']==$level_arr['level']?' selected':'').'>#'.$level_arr['level'].' - '.htmlspecialchars($level_arr['name']).($level_arr['caption']?': '.$level_arr['caption']:'').'</option>';
				}
			print '</select></p></form>';
			print '<p><b>Flags:</b>';
			$organizer_addon='';
			if(isset($auth['organizers_list'][$organizer_arr['id']])){
				print '<br>✔️ &mdash; Organizer administration';
			}
			if($event_user['admin']){
				print '<br><a href="?toggle_flag=admin&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($user_arr['admin']?'✔️':'❌').' &mdash; Admin'.$organizer_addon;
				print '<br><a href="?toggle_flag=manager&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($user_arr['manager']?'✔️':'❌').' &mdash; Manager';
			}
			else{
				print '<br>'.($user_arr['admin']?'✔️':'❌').' &mdash; Admin'.$organizer_addon;
				print '<br>'.($user_arr['manager']?'✔️':'❌').' &mdash; Manager';
			}
			print '<br><a href="?toggle_flag=speaker&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($user_arr['speaker']?'✔️':'❌').' &mdash; Speaker';
			print '<br><a href="?toggle_flag=sponsor&'.gen_csrf_param().'" class="toggle-btn mr-2">Toggle</a>'.($user_arr['sponsor']?'✔️':'❌').' &mdash; Sponsor';

			$search_linked_speaker=$db->sql_row("SELECT * FROM `event_speakers` WHERE `event`='".$event_id."' AND `user`='".$db->prepare($user_arr['id'])."'");
			if(null!=$search_linked_speaker){
				print ' (<a href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/edit/'.$search_linked_speaker['id'].'/">linked</a>)';
			}

			print '</p>';
			print '</div>';
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/users/">&larr; Back to users</a>';
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>User card not found</p>
			</div>';
			return;
		}
	}
	else{
		if($event_user['admin']){
			print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/users/download/?'.gen_csrf_param().'">Download as .csv</a>';
		}
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		$filter_flag=false;
		if(isset($_GET['flag'])){
			if(''!==$_GET['flag']){
				$filter_flag=$_GET['flag'];
				if(!in_array($filter_flag,$event_flags_arr)){
					$filter_flag=false;
				}
			}
		}

		$filter_level=false;
		if(isset($_GET['level'])){
			if(''!==$_GET['level']){
				$filter_level=$_GET['level'];
				if(0==$db->table_count('event_levels',"WHERE `event`='".$event_id."' AND `level`='".$filter_level."'")){
					$filter_level=false;
				}
			}
		}

		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($user_status_arr[$filter_status])){
					$filter_status=false;
				}
			}
		}

		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="flag" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_flag?' selected':'').'>Filter by flag</option>';
					foreach($event_flags_arr as $flag){
						print '<option value="'.$flag.'"'.($flag===$filter_flag?' selected':'').'>Flag: '.$flag.'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="level" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_level?' selected':'').'>Filter by level</option>';
					foreach($levels_arr as $level_id=>$level_arr){
						print '<option value="'.$level_arr['level'].'"'.($filter_level===$level_arr['level']?' selected':'').'>Level: #'.$level_arr['level'].' - '.htmlspecialchars($level_arr['name']).'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
					foreach($user_status_arr as $user_status_id=>$user_status_name){
						print '<option value="'.$user_status_id.'"'.($filter_status===$user_status_id?' selected':'').' class="'.$user_status_style_arr[$user_status_id].'">Status: '.htmlspecialchars($user_status_name).'</option>';
					}
				print '
				</select>
			</div>
		</div>
		</form>';

		$sql_addon=[];
		$sql_addon[]="`event`='".$event_id."'";
		if(false!==$filter_status){
			$sql_addon[]='`status`='.$filter_status;
		}
		if(false!==$filter_level){
			$sql_addon[]='`level`='.(int)$filter_level;
		}
		if(false!==$filter_flag){
			$sql_addon[]='`'.$filter_flag.'`=1';
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}

		$per_page=50;
		$count=$db->table_count('event_users',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$users=$db->sql("SELECT * FROM `event_users`".$sql_addon_str." ORDER BY `id` ASC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
				<div class="py-2 inline-block min-w-full">
					<div class="overflow-hidden">';
		print '<table class="table-auto min-w-full">';
		print '<thead class="bg-white border-b">';
		print '<tr>';
			print '<th>Address</th>';
			print '<th>Admin</th>';
			print '<th>Manager</th>';
			print '<th>Speaker</th>';
			print '<th>Sponsor</th>';
			print '<th>Level</th>';
			print '<th>Status</th>';
			print '<th>Linked item</th>';
		print '</tr>';
		print '</thead>';
		print '<tbody>';
		foreach($users as $user){
			print '<tr>';
			$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$user['address']."'");
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/users/'.$user['id'].'/">'.htmlspecialchars($address_arr['address']).' ('.($types_arr[$address_arr['type']]['name']).')</a></td>';
			print '<td>'.($user['admin']?'✔️':'').'</td>';
			print '<td>'.($user['manager']?'✔️':'').'</td>';
			print '<td>'.($user['speaker']?'✔️':'').'</td>';
			print '<td>'.($user['sponsor']?'✔️':'').'</td>';
			print '<td>'.$user['level'].'</td>';
			print '<td class="'.$user_status_style_arr[$user['status']].'">'.$user_status_arr[$user['status']].'</td>';
			print '<td>';
			if(2==$user['status']){//invited
				print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/edit/'.$user['invited'].'/">Invite</a>';
			}
			elseif(1==$user['status']){//binded
				$nft_arr=$db->sql_row("SELECT * FROM `nft_list` WHERE `id`='".$db->prepare($user['binded'])."'");
				$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$db->prepare($user['binded'])."'");
				print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/?id='.$binded_nft_arr['whitelist'].'">NFT ticket</a>';
			}
			else{
				print '&mdash;';
			}
			print '</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('levels'==$path_array[4]){
	$replace['title']='Levels | '.$replace['title'];
	if('delete'==$path_array[5]){
		$level_id=intval($path_array[6]);
		$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `id`='".$level_id."'");
		if(null!=$level_arr){
			if(isset($_POST['approve'])){
				$db->sql("DELETE FROM `event_levels` WHERE `id`='".$level_id."'");
				$db->sql("UPDATE `event_users` SET `level`=0 WHERE `event`='".$event_id."' AND `level`='".$level_arr['level']."'");
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/levels/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/">&larr; Back to levels</a>';
				print '<h2 class="text-red-600">Remove level #'.$level_arr['id'].'</h2>';
				$affected_users_count=$db->table_count('event_users',"WHERE `event`='".$event_id."' AND `level`='".$level_arr['level']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Level "<a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/edit/'.$level_arr['id'].'/">'.htmlspecialchars($level_arr['name']).'</a>" will be removed from the database.</p>';
				if($affected_users_count){
					print '<p>Levels will also be reset to zero for '.$affected_users_count.' users.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove level" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/">&larr; Back to levels</a>';
			print '<h2 class="text-red-600">Level not found</h2>';
			print '<p>Return to event levels and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/">&larr; Back to levels</a>';
		print '<h2>Create level</h2>';
		if(isset($_POST['name'])){
			$errors=[];

			$level=intval($_POST['level']);
			if($level<0){
				$errors[]='Negative level';
			}
			if($level>100){
				$level=100;
			}
			$level_exist=$db->table_count('event_levels',"WHERE `level`='".$level."' AND `event`='".$event_id."'");
			if($level_exist){
				$errors[]='Level '.$level.' already exists';
			}

			$name=trim($_POST['name']);
			if(!$name){
				$errors[]='Name is required';
			}
			$level_exist=$db->table_count('event_levels',"WHERE `name`='".$db->prepare($name)."' AND `event`='".$event_id."'");
			if($level_exist){
				$errors[]='Level name '.htmlspecialchars($name).' already exists';
			}

			$caption=trim($_POST['caption']);


			if(0==count($errors)){
				$db->sql("INSERT INTO `event_levels` (`event`,`level`,`name`,`caption`) VALUES ('".$event_id."','".$level."','".$db->prepare($name)."','".$db->prepare($caption)."')");
				$level_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Level <a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/edit/'.$level_id.'/">"'.htmlspecialchars($name).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/levels/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';
			$object_scheme=[//event levels
				'level'=>['type'=>'number','title'=>'Level','descr'=>'(0 [zero] is used to free for all access <b>as a Guest</b>)','placeholder'=>'Level (number)','required'=>true],
				'name'=>['type'=>'text','title'=>'Name','descr'=>'(e.g. Guest, <b>Standart</b>, VIP, Diamond)','required'=>true],
				'caption'=>['type'=>'text','title'=>'Caption','descr'=>'(e.g. VIP access to speakers group)'],
			];
			print build_form($object_scheme,false);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create level" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/">&larr; Back to levels</a>';
		print '<h2>Edit level</h2>';
		$level_id=(int)$path_array[6];
		$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `id`='".$level_id."'");
		if(null!=$level_arr){
			if(isset($_POST['name'])){
				$errors=[];

				$level=intval($_POST['level']);
				if($level<0){
					$errors[]='Negative level';
				}
				if($level>100){
					$level=100;
				}
				if($level_arr['level']!=$level){//check duplicate only if level was changed
					$level_exist=$db->table_count('event_levels',"WHERE `level`='".$level."' AND `event`='".$event_id."' AND `id`!='".$level_id."'");
					if($level_exist){
						$errors[]='Level '.$level.' already exists';
					}
				}

				$name=trim($_POST['name']);
				if(!$name){
					$errors[]='Name is required';
				}
				if($level_arr['name']!=$name){//check duplicate only if name was changed
					$level_exist=$db->table_count('event_levels',"WHERE `name`='".$db->prepare($name)."' AND `event`='".$event_id."' AND `id`!='".$level_id."'");
					if($level_exist){
						$errors[]='Level name '.htmlspecialchars($name).' already exists';
					}
				}

				$caption=trim($_POST['caption']);

				if(0==count($errors)){
					$db->sql("UPDATE `event_levels` SET
						`level`='".$level."',
						`name`='".$db->prepare($name)."',
						`caption`='".$db->prepare($caption)."'
						WHERE `id`='".$level_id."'");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Level <a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/edit/'.$level_id.'/">"'.htmlspecialchars($name).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/levels/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event levels
					'level'=>['type'=>'number','title'=>'Level','descr'=>'(0 [zero] is used to free for all access <b>as a Guest</b>)','placeholder'=>'Level (number)','required'=>true],
					'name'=>['type'=>'text','title'=>'Name','descr'=>'(e.g. Guest, <b>Standart</b>, VIP, Diamond)','required'=>true],
					'caption'=>['type'=>'text','title'=>'Caption','descr'=>'(e.g. VIP access to speakers group)'],
				];
				print build_form($object_scheme,$level_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Level not found</h2>';
			print '<p>Return to event levels and try again.</p>';
		}
	}
	else{
		print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/create/" class="action-btn">Create level</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Action</th>';
			print '<th>Level</th>';
			print '<th>Name</th>';
			print '<th>Caption</th>';
			print '<th>Users count</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$levels=$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `id` ASC");
		foreach($levels as $level){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/delete/'.$level['id'].'/" class="red-btn">Delete</a></td>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/edit/'.$level['id'].'/">'.$level['level'].'</a></td>';
			print '<td>'.htmlspecialchars($level['name']).'</td>';
			print '<td>'.htmlspecialchars($level['caption']).'</td>';
			$user_count=$db->table_count('event_users',"WHERE `event`='".$event_id."' AND `level`='".$level['level']."'");
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/users/?level='.$level['level'].'">'.$user_count.'</a></td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}
}
elseif('invites'==$path_array[4]){
	$replace['title']='Invites | '.$replace['title'];
	$event_flags_permission_arr=$event_flags_arr;
	if(!$event_user['admin']){
		//remove admin flag as it is not allowed to change for non-admins
		$key=array_search('admin',$event_flags_permission_arr);
		if(false!==$key){
			unset($event_flags_permission_arr[$key]);
		}
		//remove manager flag as it is not allowed to change for non-admins
		$key=array_search('manager',$event_flags_permission_arr);
		if(false!==$key){
			unset($event_flags_permission_arr[$key]);
		}
	}

	if('download'==$path_array['5']){
		if($event_user['admin']&&check_csrf()){
			ob_end_clean();
			ob_end_clean();
			$csv_data='';
			$csv_data.='"Invite";"Type";"Address";"Extended";"Admin";"Manager";"Speaker";"Sponsor";"Level";"Status";"Time";';
			$invites=$db->sql("SELECT * FROM `event_invites` WHERE `event`='".$event_id."' ORDER BY `id` ASC");
			foreach($invites as $invite){
				$csv_data.=PHP_EOL;
				if($invite['type']){
					$csv_data.=csv_prepare('Address').';';
					$csv_data.=csv_prepare($types_arr[$invite['type']]['name']).';';
					$csv_data.=csv_prepare($invite['address']).';';
					$csv_data.=csv_prepare("").';';
				}
				if($invite['platform']){
					$csv_data.=csv_prepare('Platform').';';
					$platform_arr=$db->sql_row("SELECT * FROM `platforms` WHERE `id`=".$invite['platform']."");
					$csv_data.=csv_prepare($platform_arr['name']).';';
					$csv_data.=csv_prepare($invite['internal_id']).';';
					$csv_data.=csv_prepare($invite['internal_username']).';';
				}
				$csv_data.=csv_prepare($invite['admin']?'1':'0').';';
				$csv_data.=csv_prepare($invite['manager']?'1':'0').';';
				$csv_data.=csv_prepare($invite['speaker']?'1':'0').';';
				$csv_data.=csv_prepare($invite['sponsor']?'1':'0').';';
				$csv_data.=csv_prepare($invite['level']).';';

				$csv_data.=csv_prepare($invite_status_arr[$invite['status']]).';';
				if($invite['time']){
					$csv_data.=csv_prepare(date('c',$invite['time'])).';';
				}
				else{
					$csv_data.=csv_prepare('').';';
				}
			}
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename=invites-'.(date('d-m-Y-H-i')).'.csv');
			header("Pragma: no-cache");
			header("Expires: 0");
			print $csv_data;
			exit;
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
			</div>';
			$path_array['5']='';
		}
	}
	if('delete'==$path_array[5]){
		$invite_id=intval($path_array[6]);
		$invite_arr=$db->sql_row("SELECT * FROM `event_invites` WHERE `event`='".$event_id."' AND `id`='".$invite_id."'");
		if(null!=$invite_arr){
			if(isset($_POST['approve'])){
				//reset uset status, level, invited status and set trigger flag
				$db->sql("UPDATE `event_users` SET `level`=0, `status`='0', `invited`='0', `trigger`=1 WHERE `event`='".$event_id."' AND `invited`='".$invite_arr['id']."'");

				$db->sql("DELETE FROM `event_invites` WHERE `id`='".$invite_id."'");
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/invites/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/">&larr; Back to invites</a>';
				print '<h2 class="text-red-600">Remove invite #'.$invite_arr['id'].'</h2>';
				$affected_users_count=$db->table_count('event_users',"WHERE `event`='".$event_id."' AND `invited`='".$invite_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Invite <a href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/edit/'.$invite_arr['id'].'/">'.htmlspecialchars($invite_arr['id']).'</a> will be removed from the database.</p>';
				if($affected_users_count){
					print '<p>Status will also be reset to zero for '.$affected_users_count.' users.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove invite" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/">&larr; Back to invites</a>';
			print '<h2 class="text-red-600">Invite not found</h2>';
			print '<p>Return to event invites and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/">&larr; Back to invites</a>';
		print '<h2>Create invite</h2>';
		if(isset($_POST['level'])){
			$errors=[];

			$flags=$_POST['flags'];
			foreach($event_flags_arr as $flag){
				if(isset($flags[$flag])){
					//check if flag is allowed to be set
					if(in_array($flag,$event_flags_permission_arr)){
						if('on'==$flags[$flag]){
							$flags[$flag]=1;
						}
						else{
							$flags[$flag]=0;
						}
					}
					else{
						$flags[$flag]=0;
					}
				}
				else{
					$flags[$flag]=0;
				}
			}

			$level=intval($_POST['level']);
			if($level<0){
				$errors[]='Negative level';
			}
			if($level>100){
				$level=100;
			}
			$level_exist=$db->table_count('event_levels',"WHERE `level`='".$level."' AND `event`='".$event_id."'");
			if(0==$level_exist){
				$errors[]='Level '.$level.' not exist';
			}

			$type=intval($_POST['type']);
			$platform=intval($_POST['platform']);
			if($type){
				if($platform){
					$errors[]='Need select address type OR platform (not both)';
				}
			}

			$address=trim($_POST['address']);
			$internal_id=trim($_POST['internal_id']);
			$internal_username=trim($_POST['internal_username']);

			if($type){
				if(''==$address){
					$errors[]='Address is empty';
				}
				else{
					$address_exist=$db->table_count('event_invites',"WHERE `event`='".$event_id."' AND `type`='".$type."' AND `address`='".$db->prepare($address)."'");
					if($address_exist){
						$errors[]='Invite with this address already exist';
					}
				}
			}
			else{
				if($platform){
					if(''==$internal_id){
						if(''==$internal_username){
							$errors[]='Internal ID OR Username is empty';
						}
						else{
							$internal_username_exist=$db->table_count('event_invites',"WHERE `event`='".$event_id."' AND `platform`='".$platform."' AND `internal_username`='".$db->prepare($internal_username)."'");
							if($internal_username_exist){
								$errors[]='Invite with this platform and internal username already exist';
							}
						}
					}
					else
					if(''==$internal_username){
						if(''==$internal_id){
							$errors[]='Internal ID OR Username is empty';
						}
						else{
							$internal_id_exist=$db->table_count('event_invites',"WHERE `event`='".$event_id."' AND `platform`='".$platform."' AND `internal_id`='".$db->prepare($internal_id)."'");
							if($internal_id_exist){
								$errors[]='Invite with this platform and internal id already exist';
							}
						}
					}
					else{
						//both internal id and username are set
						$internal_username_exist=$db->table_count('event_invites',"WHERE `event`='".$event_id."' AND `platform`='".$platform."' AND `internal_username`='".$db->prepare($internal_username)."'");
						if($internal_username_exist){
							$errors[]='Invite with this platform and internal username already exist';
						}
						$internal_id_exist=$db->table_count('event_invites',"WHERE `event`='".$event_id."' AND `platform`='".$platform."' AND `internal_id`='".$db->prepare($internal_id)."'");
						if($internal_id_exist){
							$errors[]='Invite with this platform and internal id already exist';
						}
					}
				}
				else{
					$errors[]='Need select address type or platform';
				}
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `event_invites` (`event`, `type`, `address`, `platform`, `internal_id`, `internal_username`, `admin`, `manager`, `speaker`, `sponsor`, `level`) VALUES ('".$event_id."', '".$type."', '".$db->prepare($address)."', '".$platform."', '".$db->prepare($internal_id)."', '".$db->prepare($internal_username)."',
				'".$flags['admin']."', '".$flags['manager']."', '".$flags['speaker']."', '".$flags['sponsor']."', '".$level."')");
				$invite_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Invite <a href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/edit/'.$invite_id.'/">'.htmlspecialchars($invite_id).'</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/invites/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';

			$object_scheme=[//event invites
				'type'=>[
					'type'=>'select',
					'title'=>'Address type',
					'optional'=>true,
					'optional_value'=>0,
					'optional_caption'=>'None',
					'options'=>$types_arr,
					'key'=>'id',
					'caption_template'=>'{name}',
					'descr'=>'optional',
					//'required'=>true,
				],
				'address'=>['type'=>'text','title'=>'Address','descr'=>'(if type is set)'],

				'platform'=>[
					'type'=>'select',
					'title'=>'Platform type',
					'optional'=>true,
					'optional_value'=>0,
					'options'=>$db->sql("SELECT * FROM `platforms` ORDER BY `name` ASC"),
					'key'=>'id',
					'caption_value'=>'name',
					'optional_caption'=>'None',
					'descr'=>'optional',
				],
				'internal_id'=>['type'=>'text','title'=>'Internal id','placeholder'=>'Platform internal id','descr'=>'(if platform is set)'],
				'internal_username'=>['type'=>'text','title'=>'Internal username','placeholder'=>'Platform internal username','descr'=>'(if platform is set)'],

				'level'=>[
					'type'=>'select',
					'title'=>'Participant level for invited',
					'options'=>$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `level` ASC"),
					'key'=>'level',
					'caption_function'=>function($option_id,$option_value){
						return $option_value['name'].($option_value['caption']?' ('.$option_value['caption'].')':'');
					},
				],
				'flags'=>[
					'type'=>'multi-checkbox',
					'title'=>'Flags',
					'options'=>$event_flags_permission_arr,
					'checked_function'=>function($option,$object){
						return (1==$object[$option]);
					},
				],
			];
			print build_form($object_scheme,false);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create invite" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/">&larr; Back to invites</a>';
		print '<h2>Edit invite</h2>';
		$invite_id=(int)$path_array[6];
		$invite_arr=$db->sql_row("SELECT * FROM `event_invites` WHERE `event`='".$event_id."' AND `id`='".$invite_id."'");
		if(null!=$invite_arr){
			if(isset($_POST['level'])){
				$errors=[];

				$flags=$_POST['flags'];
				foreach($event_flags_arr as $flag){
					if(isset($flags[$flag])){
						//check if flag is allowed to be set
						if(in_array($flag,$event_flags_permission_arr)){
							if('on'==$flags[$flag]){
								$flags[$flag]=1;
							}
							else{
								$flags[$flag]=0;
							}
						}
						else{
							$flags[$flag]=0;
						}
					}
					else{
						$flags[$flag]=0;
					}
				}

				$level=intval($_POST['level']);
				if($level<0){
					$errors[]='Negative level';
				}
				if($level>100){
					$level=100;
				}
				$level_exist=$db->table_count('event_levels',"WHERE `level`='".$level."' AND `event`='".$event_id."'");
				if(0==$level_exist){
					$errors[]='Level '.$level.' not exist';
				}

				$type=intval($_POST['type']);
				$platform=intval($_POST['platform']);
				if($type){
					if($platform){
						$errors[]='Need select address type OR platform (not both)';
					}
				}

				$address=trim($_POST['address']);
				$internal_id=trim($_POST['internal_id']);
				$internal_username=trim($_POST['internal_username']);

				if($type){
					if(''==$address){
						$errors[]='Address is empty';
					}
				}
				else{
					if($platform){
						if(''==$internal_id){
							if(''==$internal_username){
								$errors[]='Internal ID OR Username is empty';
							}
						}
						if(''==$internal_username){
							if(''==$internal_id){
								$errors[]='Internal ID OR Username is empty';
							}
						}
					}
					else{
						$errors[]='Need select address type or platform';
					}
				}

				if(0==count($errors)){
					$db->sql("UPDATE `event_invites` SET `type`='".$type."',`address`='".$db->prepare($address)."',`platform`='".$platform."',`internal_id`='".$db->prepare($internal_id)."',`internal_username`='".$db->prepare($internal_username)."',`admin`='".$flags['admin']."',`manager`='".$flags['manager']."',`speaker`='".$flags['speaker']."',`level`='".$level."' WHERE `id`='".$invite_id."'");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Invite <a href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/edit/'.$invite_id.'/">'.$invite_id.'</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/invites/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event invites
					'type'=>[
						'type'=>'select',
						'title'=>'Address type',
						'optional'=>true,
						'optional_value'=>0,
						'optional_caption'=>'None',
						'options'=>$types_arr,
						'key'=>'id',
						'caption_template'=>'{name}',
						'descr'=>'optional',
						//'required'=>true,
					],
					'address'=>['type'=>'text','title'=>'Address','descr'=>'(if type is set)'],

					'platform'=>[
						'type'=>'select',
						'title'=>'Platform type',
						'optional'=>true,
						'optional_value'=>0,
						'options'=>$db->sql("SELECT * FROM `platforms` ORDER BY `name` ASC"),
						'key'=>'id',
						'caption_value'=>'name',
						'optional_caption'=>'None',
						'descr'=>'optional',
					],
					'internal_id'=>['type'=>'text','title'=>'Internal id','placeholder'=>'Platform internal id','descr'=>'(if platform is set)'],
					'internal_username'=>['type'=>'text','title'=>'Internal username','placeholder'=>'Platform internal username','descr'=>'(if platform is set)'],

					'level'=>[
						'type'=>'select',
						'title'=>'Participant level for invited',
						'options'=>$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `level` ASC"),
						'key'=>'level',
						'caption_function'=>function($option_id,$option_value){
							return $option_value['name'].($option_value['caption']?' ('.$option_value['caption'].')':'');
						},
					],
					'flags'=>[
						'type'=>'multi-checkbox',
						'title'=>'Flags',
						'options'=>$event_flags_permission_arr,
						'checked_function'=>function($option,$object){
							return (1==$object[$option]);
						},
					],
				];
				print build_form($object_scheme,$invite_arr);
				print '<p>Status: ';
				print '<span class="'.$invite_status_style_arr[$invite_arr['status']].'">';
				print $invite_status_arr[$invite_arr['status']];
				print '</span>';
				print '</p>';
				if($invite_arr['status']>0){
					print '<p>Claimed: <span class="time" data-time="'.$invite_arr['time'].'">'.date('d.m.Y H:i:s',$invite_arr['time']).' GMT</span></p>';
				}

				print '<div class="grid-wrapper">';
				if($invite_arr['status']==0){
					print '<input type="submit" value="Save changes" class="action-btn big">';
				}
				else{
					print '<p>Can\'t edit claimed invite</p>';
					print '<input type="submit" value="Save changes" class="action-btn big" disabled>';
				}
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Invite not found</h2>';
			print '<p>Return to event invites and try again.</p>';
		}
	}
	else{
		print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/create/" class="action-btn">Create invite</a>';
		if($event_user['admin']){
			print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/download/?'.gen_csrf_param().'">Download as .csv</a>';
		}
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';

			print '<th>#</th>';

			print '<th>Address type</th>';
			print '<th>Address</th>';

			print '<th>Platform</th>';
			print '<th>Platform ID</th>';
			print '<th>Platform Username</th>';

			print '<th>Admin</th>';
			print '<th>Manager</th>';
			print '<th>Speaker</th>';
			print '<th>Level</th>';
			print '<th>Status</th>';
			print '<th>Time</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]='`event`='.$event_id;
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}

		$per_page=50;
		$count=$db->table_count('event_invites',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$invites=$db->sql("SELECT * FROM `event_invites`".$sql_addon_str." ORDER BY `id` ASC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($invites as $invite){
			print '<tr>';
			print '<td>';
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/delete/'.$invite['id'].'/" class="red-btn">Delete</a>';
			print '</td>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/invites/edit/'.$invite['id'].'/">Edit</a></td>';

			if($invite['type']){
				print '<td>'.htmlspecialchars($types_arr[$invite['type']]['name']).'</td>';
				print '<td>'.htmlspecialchars($invite['address']).'</td>';
			}
			else{
				print '<td>&mdash;</td>';
				print '<td>&mdash;</td>';
			}

			if($invite['platform']){
				$platform_arr=$db->sql_row("SELECT * FROM `platforms` WHERE `id`=".$invite['platform']."");
				print '<td>'.htmlspecialchars($platform_arr['name']).'</td>';
				if($invite['internal_id']){
					print '<td>'.htmlspecialchars($invite['internal_id']).'</td>';
				}
				else{
					print '<td>&mdash;</td>';
				}
				if($invite['internal_username']){
					print '<td>'.htmlspecialchars($invite['internal_username']).'</td>';
				}
				else{
					print '<td>&mdash;</td>';
				}
			}
			else{
				print '<td>&mdash;</td>';
				print '<td>&mdash;</td>';
				print '<td>&mdash;</td>';
			}

			print '<td>'.($invite['admin']?'✔️':'').'</td>';
			print '<td>'.($invite['manager']?'✔️':'').'</td>';
			print '<td>'.($invite['speaker']?'✔️':'').'</td>';
			print '<td>'.$invite['level'].'</td>';

			print '<td class="'.$invite_status_style_arr[$invite['status']].'">'.$invite_status_arr[$invite['status']].'</td>';
			if($invite['time']){
				print '<td><span class="time" data-time="'.$invite['time'].'">'.date('d.m.Y H:i:s',$invite['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}

			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('whitelist'==$path_array[4]){
	$replace['title']='Whitelist | '.$replace['title'];
	if('download'==$path_array['5']){
		if($event_user['admin']&&check_csrf()){
			ob_end_clean();
			ob_end_clean();
			$csv_data='';
			$csv_data.='"Chain short";"Chain ID";"Contract";"Token ID";"Level";"Binded address";"Time";';
			$whitelist=$db->sql("SELECT * FROM `event_whitelist` WHERE `event`='".$event_id."' ORDER BY `token_id` ASC");
			foreach($whitelist as $whitelist_arr){
				$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`=".$whitelist_arr['chain']);
				$csv_data.=PHP_EOL;
				$csv_data.=csv_prepare($chain_arr['short']).';';
				$csv_data.=csv_prepare($chain_arr['chain_id']).';';
				$csv_data.=csv_prepare($whitelist_arr['contract']).';';
				$csv_data.=csv_prepare($whitelist_arr['token_id']).';';
				$csv_data.=csv_prepare($whitelist_arr['level']).';';
				if($whitelist_arr['binded_nft']){
					$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$db->prepare($whitelist_arr['binded_nft'])."'");
					$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$binded_nft_arr['address']."'");
					$csv_data.=csv_prepare($address_arr['address']).';';
				}
				else{
					$csv_data.=csv_prepare('').';';
				}
				$csv_data.=csv_prepare(date('c',$whitelist_arr['time'])).';';

			}
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename=tickets-'.(date('d-m-Y-H-i')).'.csv');
			header("Pragma: no-cache");
			header("Expires: 0");
			print $csv_data;
			exit;
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
			</div>';
			$path_array['5']='';
		}
	}
	if('delete'==$path_array[5]){
		//deactivation of user and nft
		$whitelist_arr=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `id`='".intval($path_array[6])."'");
		if(null!=$whitelist_arr){
			if(isset($_POST['approve'])){
				//deactivate nft if binded
				$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `whitelist`='".$whitelist_arr['id']."'");
				if(null==$binded_nft_arr){
					//binded not finded, already deactivated, ignore
				}
				else{
					//deactivate event_users
					$db->sql("UPDATE `event_users` SET `level`=0, `status`='0', `binded`='0', `trigger`=1 WHERE `event`='".$whitelist_arr['event']."' AND `binded`='".$whitelist_arr['id']."'");
					//set deactivation for nft_list
					$db->sql("UPDATE `nft_list` SET `status`=2 WHERE `id`='".$binded_nft_arr['nft']."'");
				}
				//delete event whitelist
				$db->sql("DELETE FROM `event_whitelist` WHERE `id`='".$whitelist_arr['id']."'");
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/">&larr; Back to NFT tickets</a>';
				print '<h2 class="text-red-600">Remove NFT ticket #'.htmlspecialchars($whitelist_arr['token_id']).'</h2>';
				$affected_users_count=$db->table_count('binded_nft',"WHERE `whitelist`='".$whitelist_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Whitelist '.htmlspecialchars($whitelist_arr['token_id']).' will be removed from the database.</p>';
				if($affected_users_count){
					print '<p>'.$affected_users_count.' user will be unbinded.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove NFT ticket" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/">&larr; Back to NFT tickets</a>';
			print '<h2 class="text-red-600">NFT ticket not found</h2>';
			print '<p>Return to event NFT tickets and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/">&larr; Back to NFT tickets</a>';
		print '<h2>Add NFT ticket to whitelist</h2>';
		if(isset($_POST['contract'])){
			$error=[];
			$success=[];
			$token_type=3;
			$chain=0;
			$chain_arr=[];
			if(isset($_POST['chain'])){
				$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`=".(int)$_POST['chain']);
				if(null!=$chain_arr){
					$chain=$chain_arr['id'];
				}
			}
			if(0==$chain){
				$error[]='Chain not exists';
			}
			else{
				$contract_valid=false;
				$contract=trim($_POST['contract']);
				if($chain_arr['type']==1){
					$contract=strtolower($contract);
					$contract=str_replace('0x','',$contract);
					$contract='0x'.$contract;
					if(42==strlen($contract)){
						$contract_valid=true;
					}
				}
				if($contract_valid){
					$level=-1;
					if(isset($_POST['level'])){
						$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `level`=".(int)$_POST['level']);
						if(null!=$level_arr){
							$level=(int)$_POST['level'];
						}
					}
					if(-1!=$level){
						$token_id_arr=[];
						$token_id_str='';
						if(isset($_POST['token_id'])){
							$token_id_str=trim($_POST['token_id']);
						}
						$token_id_str=trim($_POST['token_id']);
						if(''!=$token_id_str){
							preg_match_all('/([0-9]+)/is',$token_id_str,$token_id_arr);
							$token_id_arr=$token_id_arr[0];
							if(strpos($token_id_str,'*')!==false){
								$token_id_arr[]='*';
							}
						}

						$count=0;
						$exist=0;
						foreach($token_id_arr as $token_id){
							$whitelist_exists=$db->table_count('event_whitelist',"WHERE `event`='".$event_id."' AND `chain`='".$chain."' AND `contract`='".$db->prepare($contract)."' AND `token_id`='".$db->prepare($token_id)."'");
							if(!$whitelist_exists){
								//need add to whitelist
								$db->sql("INSERT INTO `event_whitelist` (`event`,`chain`,`contract`, `token_id`, `level`) VALUES ('".$event_id."','".$chain."','".$db->prepare($contract)."', '".$db->prepare($token_id)."', ".$level.")");
								$count++;
							}
							else{
								$exist++;
							}
						}
						if($count){
							$success[]='Added '.$count.' NFT ticket to whitelist';
						}
						if($exist){
							$error[]='Already exists '.$exist.' NFT ticket in whitelist';
						}
					}
					else{
						$error[]='Level is not exist';
					}
				}
				else{
					$error[]='Invalid contract address';
				}
			}
			if(count($success)){
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>'.implode('<br>',$success).'</p>
				</div>';
			}
			if(count($error)){
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$error).'</p>
				</div>';
			}
		}
		$levels_count=$db->table_count('event_levels','WHERE `event`='.$event_id);
		if(0==$levels_count){
			//write notice and link to /partners_cat/ create category
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Go to <a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/create/">levels</a> and create at least one visitor tier level.</p>
			</div>';
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';
			$object_scheme=[
				'chain'=>[
					'type'=>'select',
					'title'=>'Chain',
					'required'=>true,
					'options'=>$db->sql("SELECT * FROM `chains` ORDER BY `type` ASC, `sort` ASC"),
					'key'=>'id',
					'caption_function'=>function($option_id,$option_value){
						global $types_arr;
						return $types_arr[$option_value['type']]['name'].': '.$option_value['name'];
					},
				],
				'contract'=>[
					'type'=>'text',
					'title'=>'Contract address',
					'required'=>true,
				],
				'token_id'=>[
					'type'=>'textarea',
					'title'=>'List of token IDs',
					'placeholder'=>'IDs separated by commas, new lines or spaces',
					'descr'=>'You can use * for all tokens (wildcard)',
					'required'=>true,
				],
				'level'=>[
					'type'=>'select',
					'title'=>'Participant level',
					'options'=>$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `level` ASC"),
					'key'=>'level',
					'caption_function'=>function($option_id,$option_value){
						return $option_value['name'].($option_value['caption']?' ('.$option_value['caption'].')':'');
					},
				],
			];
			print build_form($object_scheme,false);
			print '</div>';

			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Add to NFT tickets whitelist" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('import'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/">&larr; Back to NFT tickets</a>';
		print '<h2>Import .CSV to whitelist</h2>';
		if(isset($_POST['chain'])){
			$error=[];
			$success=[];
			$token_type=3;
			$chain=0;
			$chain_arr=[];
			if(isset($_POST['chain'])){
				$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`=".(int)$_POST['chain']);
				if(null!=$chain_arr){
					$chain=$chain_arr['id'];
				}
			}
			if(0==$chain){
				$error[]='Chain not exists';
			}
			else{
				function check_contract_address($contract){
					$contract=strtolower($contract);
					$contract=str_replace('0x','',$contract);
					$contract='0x'.$contract;
					if(42==strlen($contract)){
						return $contract;
					}
					else{
						return false;
					}
				}

				$level=-1;
				if(isset($_POST['level'])){
					$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `level`=".(int)$_POST['level']);
					if(null!=$level_arr){
						$level=(int)$_POST['level'];
					}
				}
				if(-1!=$level){
					$file=$_FILES['file'];
					$file_data=file_get_contents($file['tmp_name']);
					$file_data_arr=explode("\n",$file_data);

					$whitelist_arr=[];
					$current_contract='';
					foreach($file_data_arr as $file_line){
						$file_line=trim($file_line,"\r\n\t ");
						$delimer=',';
						if(strpos($file_line,';')!==false){
							$delimer=';';
						}
						$data_arr=explode($delimer,$file_line);
						foreach($data_arr as $data_str){
							if($data_str){
								if('"'==$data_str[0]){
									$data_str=substr($data_str,1,-1);
								}
								$data_str=str_replace('""','"',$data_str);
								$token_id=false;
								if(preg_match('/^([0-9]+)$/is',$data_str)){
									//only digits
									$token_id=$data_str;
								}
								else{
									if('*'==$data_str){
										$token_id='*';
									}
									//have letters (0x address)
									$contract=check_contract_address($data_str);
									if(false!==$contract){
										$current_contract=$contract;
									}
								}
								if($current_contract){
									if(false!==$token_id){
										$whitelist_arr[$current_contract][]=$token_id;
									}
								}
							}
						}

					}

					$count=0;
					$exist=0;
					foreach($whitelist_arr as $contract=>$token_id_arr){
						foreach($token_id_arr as $token_id){
							$whitelist_exists=$db->table_count('event_whitelist',"WHERE `event`='".$event_id."' AND `chain`='".$chain."' AND `contract`='".$db->prepare($contract)."' AND `token_id`='".$db->prepare($token_id)."'");
							if(!$whitelist_exists){
								//need add to whitelist
								$db->sql("INSERT INTO `event_whitelist` (`event`,`chain`,`contract`, `token_id`, `level`) VALUES ('".$event_id."','".$chain."','".$db->prepare($contract)."', '".$db->prepare($token_id)."', ".$level.")");
								$count++;
							}
							else{
								$exist++;
							}
						}
					}
					if($count){
						$success[]='Added '.$count.' NFT tickets to whitelist';
					}
					if($exist){
						$error[]='Already exists '.$exist.' NFT tickets in whitelist';
					}
				}
				else{
					$error[]='Level is not exist';
				}
			}
			if(count($success)){
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>'.implode('<br>',$success).'</p>
				</div>';
			}
			if(count($error)){
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$error).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		$levels_count=$db->table_count('event_levels','WHERE `event`='.$event_id);
		if(0==$levels_count){
			//write notice and link to /partners_cat/ create category
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Go to <a href="/@'.$organizer_url.'/'.$event_url.'/manage/levels/create/">levels</a> and create at least one visitor tier level.</p>
			</div>';
		}
		else{
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
			print '<div class="max-w-fit">';
			$object_scheme=[
				'chain'=>[
					'type'=>'select',
					'title'=>'Chain',
					'required'=>true,
					'options'=>$db->sql("SELECT * FROM `chains` ORDER BY `type` ASC, `sort` ASC"),
					'key'=>'id',
					'caption_function'=>function($option_id,$option_value){
						global $types_arr;
						return $types_arr[$option_value['type']]['name'].': '.$option_value['name'];
					},
				],
				'file'=>[
					'type'=>'file',
					'title'=>'CSV file',
					'descr'=>'CSV file with columns: contract, token_id<br>You can use ";" or "," as delimer',
					'required'=>true,

				],
				'level'=>[
					'type'=>'select',
					'title'=>'Participant level',
					'options'=>$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `level` ASC"),
					'key'=>'level',
					'caption_function'=>function($option_id,$option_value){
						return $option_value['name'].($option_value['caption']?' ('.$option_value['caption'].')':'');
					},
				],
			];
			print build_form($object_scheme,false);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Import to NFT tickets whitelist" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	else{
		$id=intval($path_array[5]);
		if($id){
			$whitelist=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `event`='".$event_id."' AND `id`='".$id."'");
			if(!$whitelist){
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/">&larr; Back to NFT tickets</a>';
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>NFT ticket card not found</p>
				</div>';
				return;
			}
			print '<h2>NFT ticket card #'.$id.'</h2>';
			print '<hr>';
			print '<div class="card-content">';
			$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`=".$whitelist['chain']);
			print '<p>Chain: '.htmlspecialchars($chain_arr['short']).' ('.$chain_arr['chain_id'].')</p>';
			print '<p>Contract: '.htmlspecialchars($whitelist['contract']).'</p>';
			print '<p>Token id: '.htmlspecialchars($whitelist['token_id']).'</p>';
			$whitelist_status=0;
			$binded_nft_arr=false;
			$event_user_arr=false;
			$address_arr=false;
			if($whitelist['binded_nft']){
				$whitelist_status=1;
				$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `whitelist`=".$whitelist['id']);
				if($binded_nft_arr){
					$event_user_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_id."' AND `address`=".$binded_nft_arr['address']);
					$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`=".$binded_nft_arr['address']);
				}
			}
			print '<p>Status: <span class="'.$whitelist_status_style_arr[$whitelist_status].'">'.htmlspecialchars($whitelist_status_arr[$whitelist_status]).'</span>';
			if($whitelist['binded_nft']){
				print ' by <a href="/@'.$organizer_url.'/'.$event_url.'/manage/users/'.$event_user_arr['id'].'">';
				print htmlspecialchars($address_arr['address']);
				print '</a>';
			}
			print '</p>';
			if($whitelist['time']){
				print '<p>Updated: <span class="time" data-time="'.$whitelist['time'].'">'.date('d.m.Y H:i',$whitelist['time']).' GMT</span></p>';
			}
			if(isset($_POST['level'])){
				if(check_csrf(true)){
					$level=intval($_POST['level']);
					$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `level`='".$level."'");
					if(null!==$level_arr){
						if($event_user_arr){
							$db->sql("UPDATE `event_users` SET `level`='".$level."' WHERE `event`='".$event_id."' AND `id`='".$event_user_arr['id']."'");
						}
						$db->sql("UPDATE `event_whitelist` SET `level`='".$level."' WHERE `event`='".$event_id."' AND `id`='".$whitelist['id']."'");
					}
				}
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/'.$whitelist['id'].'/');
				exit;
			}
			print '<hr><form action="" method="POST">';
			print gen_csrf_form();
			print '<p><b>Level:</b>';
			print '<select class="max-w-fit" name="level" onchange="$(this).closest(\'form\')[0].submit()">';
			foreach($levels_arr as $level_id=>$level_arr){
				print '<option value="'.$level_arr['level'].'"'.($whitelist['level']==$level_arr['level']?' selected':'').'>#'.$level_arr['level'].' - '.htmlspecialchars($level_arr['name']).($level_arr['caption']?': '.$level_arr['caption']:'').'</option>';
			}
			print '</select></p></form>';
			print '</div>';
		}
		else{
			print '<a class="action-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/create/">Create</a>';
			print '<a class="action-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/import/">Import .csv</a>';
			if($event_user['admin']){
				print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/download/?'.gen_csrf_param().'">Download as .csv</a>';
			}
			print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

			$filter_id=false;
			if(isset($_GET['id'])){
				if(''!==$_GET['id']){
					$filter_id=(int)$_GET['id'];
				}
			}

			$filter_status=false;
			if(isset($_GET['status'])){
				if(''!==$_GET['status']){
					$filter_status=(int)$_GET['status'];
				}
			}

			$filter_chain=false;
			if(isset($_GET['chain'])){
				if(''!==$_GET['chain']){
					$filter_chain=(int)$_GET['chain'];
				}
			}

			$filter_contract=false;
			$contracts=$db->sql("SELECT DISTINCT `contract` FROM `event_whitelist` WHERE `event`='".$event_id."' ORDER BY `contract` ASC");
			$contracts_arr=[];
			foreach($contracts as $contract){
				$contracts_arr[]=$contract['contract'];
			}
			if(isset($_GET['contract'])){
				if(''!==$_GET['contract']){
					$filter_contract=$_GET['contract'];
				}
			}
			print '
			<form action="" method="GET">
			<div class="filters-wrapper">
				<div>
					<select name="contract" onchange="$(this).closest(\'form\')[0].submit()">
						<option value=""'.(false===$filter_contract?' selected':'').'>Filter by contract</option>';
						foreach($contracts_arr as $contract){
							print '<option value="'.$contract.'"'.($filter_contract===$contract?' selected':'').'>'.$contract.'</option>';
						}
						print '
					</select>
				</div>
				<div>
					<select name="chain" onchange="$(this).closest(\'form\')[0].submit()">
						<option value=""'.(false===$filter_contract?' selected':'').'>Filter by chain</option>';
						$chains=$db->sql('SELECT * FROM `chains` WHERE `status`=0');//0=enabled
						foreach($chains as $chain){
							print '<option value="'.$chain['id'].'"'.($filter_chain==$chain['id']?' selected':'').'>'.$chain['short'].' #'.$chain['chain_id'].' ('.$types_arr[$chain['type']]['name'].')</option>';
						}
						print '
					</select>
				</div>
				<div>
					<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
						<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>
						';
						foreach($whitelist_status_arr as $status_id=>$status_name){
							print '<option value="'.$status_id.'"'.($filter_status===$status_id?' selected':'').' class="'.$whitelist_status_style_arr[$status_id].'">Status: '.$status_id.' ('.htmlspecialchars($status_name).')</option>';
						}
						print '
					</select>
				</div>
			</div>
			</form>';

			$sql_addon=[];
			$sql_addon[]='`event`='.$event_id;
			if(false!==$filter_id){
				$sql_addon[]='`id`='.(int)$filter_id;
			}
			if(false!==$filter_status){
				$sql_addon[]='`binded_nft`'.($filter_status?'!=0':'=0');
			}
			if(false!==$filter_contract){
				$sql_addon[]='`contract`=\''.$db->prepare($filter_contract).'\'';
			}
			if(false!==$filter_chain){
				$sql_addon[]='`chain`='.(int)$filter_chain;
			}
			$sql_addon_str='';
			if(count($sql_addon)>0){
				$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
			}

			$per_page=50;
			$count=$db->table_count('event_whitelist',$sql_addon_str);
			$pages_count=ceil($count/$per_page);
			$page=1;
			if(isset($_GET['page'])){
				$page=(int)$_GET['page'];
				if($page<1){
					$page=1;
				}
				elseif($page>$pages_count){
					$page=$pages_count;
				}
			}

			$whitelist=$db->sql("SELECT * FROM `event_whitelist`".$sql_addon_str." ORDER BY `id` ASC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
			print '
			<div class="flex flex-col">
				<div class="table-wrapper">
				<div class="py-2 inline-block min-w-full">
				<div class="overflow-hidden">';
				print '<table class="table-auto min-w-full">';
				print '<thead class="bg-white border-b">';
				print '<tr>';
				print '<th>Action</th>';
				print '<th>#</th>';
				print '<th>Chain</th>';
				print '<th>Contract</th>';
				print '<th>Token ID</th>';
				print '<th>Level</th>';
				print '<th>Binded user</th>';
				print '</tr>';
				print '</thead>';
				print '<tbody>';
			foreach($whitelist as $whitelist_item){
				$wildcard=false;
				if($whitelist_item['token_id']=='*'){
					$wildcard=true;
				}
				print '<tr>';
				if($whitelist_item['binded_nft']){
					print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/delete/'.$whitelist_item['id'].'/" class="red-btn">Unbind & Delete</a></td>';
				}
				else{
					print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/delete/'.$whitelist_item['id'].'/" class="red-btn">Delete</a></td>';
				}
				print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/whitelist/'.$whitelist_item['id'].'/">Edit</td>';
				$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`=".$whitelist_item['chain']);
				print '<td>'.htmlspecialchars($chain_arr['short']).' ('.$chain_arr['chain_id'].')</td>';
				print '<td'.($wildcard?' class="text-green-800 font-bold"':'').'>'.htmlspecialchars($whitelist_item['contract']).'</td>';
				print '<td'.($wildcard?' class="text-green-800 font-bold"':'').'>'.$whitelist_item['token_id'].'</td>';
				print '<td>'.$whitelist_item['level'].'</td>';
				if($whitelist_item['binded_nft']){
					//make link to user card by binded property as id
					$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `whitelist`=".$whitelist_item['id']);
					$event_user_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_id."' AND `address`=".$binded_nft_arr['address']);
					$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`=".$binded_nft_arr['address']);
					print '<td class="small-padding"><div class="json-addon-cell tinyscroll larger">'.($whitelist_item['binded_nft']?'<a href="/@'.$organizer_url.'/'.$event_url.'/manage/users/'.$event_user_arr['id'].'">'.$address_arr['address'].'</a>':'&mdash;').'</div></td>';
				}
				else{
					print '<td>&mdash;</td>';
				}
				print '</tr>';
			}
			print '</tbody>';
			print '</table>';
			print '
						</div>
					</div>
				</div>
			</div>';

			print '<div class="pagination">';
			//get string with all GET params except page
			$get_params=[];
			foreach($_GET as $get_param_name=>$get_param_value){
				if('page'!==$get_param_name){
					$get_params[]=$get_param_name.'='.urlencode($get_param_value);
				}
			}
			$get_params_str='';
			if(count($get_params)){
				$get_params_str=implode('&',$get_params);
			}
			if($page>1){
				print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
			}
			for($i=1;$i<=$pages_count;$i++){
				print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
			}
			if($page<$pages_count){
				print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
			}
			print '</div>';
		}
	}
}
elseif('locations'==$path_array[4]){
	$replace['title']='Locations | '.$replace['title'];
	if('delete'==$path_array[5]){
		$location_arr=$db->sql_row("SELECT * FROM `event_locations` WHERE `event`='".$event_id."' AND `id`='".intval($path_array[6])."'");
		if(null!=$location_arr){
			if(isset($_POST['approve'])){
				$db->sql("DELETE FROM `event_locations` WHERE `id`='".$location_arr['id']."'");
				$db->sql("UPDATE `event_sessions` SET `location`=0 WHERE `event`='".$event_id."' AND `location`='".$location_arr['id']."'");
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/locations/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/">&larr; Back to locations</a>';
				print '<h2 class="text-red-600">Remove location #'.$location_arr['id'].'</h2>';
				$affected_sessions_count=$db->table_count('event_sessions',"WHERE `location`='".$location_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Location <a href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/edit/'.$location_arr['id'].'/">"'.htmlspecialchars($location_arr['caption']).'"</a> will be removed from the database.</p>';
				if($affected_sessions_count){
					print '<p>'.$affected_sessions_count.' sessions will be effected.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove location" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/">&larr; Back to locations</a>';
			print '<h2 class="text-red-600">Location not found</h2>';
			print '<p>Return to event locations and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/">&larr; Back to locations</a>';
		print '<h2>Add location</h2>';
		if(isset($_POST['caption'])){
			$caption=trim($_POST['caption']);
			$status=0;
			if(isset($locations_status_arr[$_POST['status']])){
				$status=(int)$_POST['status'];
			}
			if(0==$db->table_count('event_locations',"WHERE `event`='".$event_id."' AND `caption`='".$db->prepare($caption)."'")){
				$db->sql("INSERT INTO `event_locations` (`event`,`caption`,`status`) VALUES ('".$event_id."','".$db->prepare($caption)."','".$status."')");
				$location_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Location <a href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/edit/'.$location_id.'/">"'.htmlspecialchars($caption).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/locations/">';
			}
			else{
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Location with the same caption was found</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		print '<form action="" method="POST" class="manage-card">';
		print '<div class="max-w-fit">';
		$object_scheme=[//event locations
			'caption'=>[
				'type'=>'text',
				'title'=>'Location name',
				'required'=>true,
			],
			'status'=>[
				'type'=>'select',
				'title'=>'Status',
				'options'=>$locations_status_arr,
				'classes'=>$locations_status_style_arr,
				'default_option'=>0,
			],
		];
		print build_form($object_scheme,false);
		print '</div>';
		print '<div class="grid-wrapper">';
		print '<input type="submit" value="Create location" class="action-btn big">';
		print '</div>';
		print '</form>';
	}
	elseif('edit'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/">&larr; Back to locations</a>';
		print '<h2>Edit location</h2>';
		$location_arr=$db->sql_row("SELECT * FROM `event_locations` WHERE `event`='".$event_id."' AND `id`='".(int)$path_array[6]."'");
		if(null!=$location_arr){
			if(isset($_POST['caption'])){
				$caption=trim($_POST['caption']);
				$status=intval($_POST['status']);

				$db->sql("UPDATE `event_locations` SET `caption`='".$db->prepare($caption)."',`status`='".$status."' WHERE `id`='".$location_arr['id']."'");
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Location <a href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/edit/'.$location_arr['id'].'/">"'.htmlspecialchars($caption).'"</a> was updated</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/locations/">';
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event locations
					'caption'=>[
						'type'=>'text',
						'title'=>'Location name',
						'required'=>true,
					],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$locations_status_arr,
						'classes'=>$locations_status_style_arr,
						'default_option'=>0,
						'key'=>'status',
					],
				];
				print build_form($object_scheme,$location_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Location not found</h2>';
			print '<p>Return to locations list and try again.</p>';
		}
	}
	else{
		print '<a class="action-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/create/">Create location</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		$filter_status=false;
		if(isset($_GET['status'])){
			$filter_status=intval($_GET['status']);
			if(''==$_GET['status']){
				$filter_status=false;
			}
		}
		print '<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">';
				print '<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
				foreach($locations_status_arr as $status_id=>$status_name){
					print '<option value="'.$status_id.'"'.($filter_status===$status_id?' selected':'').' class="'.$locations_status_style_arr[$status_id].'">Status: '.htmlspecialchars($status_name).'</option>';
				}
			print '</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Action</th>';
			print '<th>Caption</th>';
			print '<th>Status</th>';
			print '<th>Sessions count</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]='`event`='.$event_id;
		if(false!==$filter_status){
			$sql_addon[]="`status`='$filter_status'";
		}
		$sql_addon_str='';
		if(count($sql_addon)){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		$locations=$db->sql("SELECT * FROM `event_locations`".$sql_addon_str." ORDER BY `id` ASC");
		foreach($locations as $location_arr){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/delete/'.$location_arr['id'].'/" class="red-btn">Delete</a></td>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/edit/'.$location_arr['id'].'/">'.htmlspecialchars($location_arr['caption']).'</a></td>';
			print '<td class="'.$locations_status_style_arr[$location_arr['status']].'">'.htmlspecialchars($locations_status_arr[$location_arr['status']]).'</td>';
			$sessions_count=$db->table_count('event_sessions',"WHERE `location`='".$location_arr['id']."'");
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/?location='.$location_arr['id'].'">'.$sessions_count.'</a></td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}
}
elseif('speakers'==$path_array[4]){
	$replace['title']='Speakers | '.$replace['title'];
	if('delete'==$path_array[5]){
		$speaker_arr=$db->sql_row("SELECT * FROM `event_speakers` WHERE `event`='".$event_id."' AND `id`='".(int)$path_array[6]."'");
		if(null!=$speaker_arr){
			if(isset($_POST['approve'])){
				if(null!=$speaker_arr){
					if(''!=$speaker_arr['photo_url']){
						if(file_exists($root_dir.$speaker_arr['photo_url'])){
							$free_file_size=filesize($root_dir.$speaker_arr['photo_url']);
							unlink($root_dir.$speaker_arr['photo_url']);
							$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$speaker_arr['photo_url'])."'");
							$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
					}
					if(''!=$speaker_arr['small_photo_url']){
						if(file_exists($root_dir.$speaker_arr['small_photo_url'])){
							$free_file_size=filesize($root_dir.$speaker_arr['small_photo_url']);
							unlink($root_dir.$speaker_arr['small_photo_url']);
							$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$speaker_arr['small_photo_url'])."'");
							$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
					}
					if(''!=$speaker_arr['tiny_photo_url']){
						if(file_exists($root_dir.$speaker_arr['tiny_photo_url'])){
							$free_file_size=filesize($root_dir.$speaker_arr['tiny_photo_url']);
							unlink($root_dir.$speaker_arr['tiny_photo_url']);
							$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$speaker_arr['tiny_photo_url'])."'");
							$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
					}
					$db->sql("DELETE FROM `event_speakers` WHERE `id`='".$speaker_arr['id']."'");
					$db->sql("DELETE FROM `event_sessions_speakers` WHERE `speaker`='".$speaker_arr['id']."'");
				}
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/speakers/');
				exit;
			}
			else{
					print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/">&larr; Back to speakers</a>';
					print '<h2 class="text-red-600">Remove speaker #'.$speaker_arr['id'].'</h2>';
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Warning</p>
						<p>Speaker will be removed from the database.</p>
					</div>';
					print '<form action="" method="POST" class="manage-card">';
					print '<input type="submit" name="approve" value="Remove speaker" class="red-btn">';
					print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/">&larr; Back to speakers</a>';
			print '<h2 class="text-red-600">Speaker not found</h2>';
			print '<p>Return to speakers list and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/">&larr; Back to speakers</a>';
		print '<h2>Create speaker</h2>';
		if(isset($_POST['public_name'])){
			$errors=[];
			$files=[];
			$public_name=trim($_POST['public_name']);
			if(0!=$db->table_count('event_speakers',"WHERE `event`='".$event_id."' AND `public_name`='".$db->prepare($public_name)."'")){
				$errors[]='Speaker with the same public name already exist';
			}
			$linked_user=0;
			if(isset($_POST['linked_user'])){
				$linked_user=intval($_POST['linked_user']);
			}

			$photo_url='';
			$small_photo_url='';
			$tiny_photo_url='';

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			$upload_max_size=ini_get('upload_max_filesize');
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/speakers/';
			if(isset($_FILES['photo']) && 0==$_FILES['photo']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png'];
				$ext=pathinfo($_FILES['photo']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['photo']['size']<=1024*1024*2){//check size 2MB
						$photo=md5($public_name).'_'.time().'.'.$ext;
						if(!file_exists($root_dir.$dir)){
							mkdir($root_dir.$dir,0777,true);
						}
						$filename=$dir.$photo;
						move_uploaded_file($_FILES['photo']['tmp_name'],$root_dir.$filename);
						$photo_url=$filename;
						//make small thumbnail 250x250 from photo
						$small_photo=md5($public_name).'_'.time().'_small.'.$ext;
						$small_filename=$dir.$small_photo;
						$small_photo_url=$small_filename;
						$small_photo_path=$root_dir.$small_filename;
						$small_photo_obj=new Imagick($root_dir.$filename);
						$small_photo_obj->thumbnailImage(250,250,true);
						$small_photo_obj->writeImage($small_photo_path);
						//make tiny thumbnail 50x50 from photo
						$tiny_photo=md5($public_name).'_'.time().'_tiny.'.$ext;
						$tiny_filename=$dir.$tiny_photo;
						$tiny_photo_url=$tiny_filename;
						$tiny_photo_path=$root_dir.$tiny_filename;
						$tiny_photo_obj=new Imagick($root_dir.$filename);
						$tiny_photo_obj->thumbnailImage(50,50,true);
						$tiny_photo_obj->writeImage($tiny_photo_path);

						$files[]=['time'=>time(),'name'=>$_FILES['photo']['name'],'size'=>$_FILES['photo']['size'],'type'=>$_FILES['photo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>3];//target=speaker
						$files[]=['time'=>time(),'name'=>$_FILES['photo']['name'],'size'=>filesize($root_dir.$small_filename),'type'=>$_FILES['photo']['type'],'path'=>$root_dir.$small_filename,'relative_path'=>$small_filename,'target'=>3];//target=speaker
						$files[]=['time'=>time(),'name'=>$_FILES['photo']['name'],'size'=>filesize($root_dir.$tiny_filename),'type'=>$_FILES['photo']['type'],'path'=>$root_dir.$tiny_filename,'relative_path'=>$tiny_filename,'target'=>3];//target=speaker
					}
					else{
						$errors[]='&mdash; Photo size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Photo must be in JPG, JPEG or PNG format';
				}
			}

			$nick_name=trim($_POST['nick_name']);
			$title=trim($_POST['title']);
			$company=trim($_POST['company']);
			$position=trim($_POST['position']);
			$description=trim($_POST['description']);

			//link to platform and internal id/username (optional)
			$platform=intval($_POST['platform']);
			$internal_id=trim($_POST['internal_id']);
			$internal_username=trim($_POST['internal_username']);

			$addon_arr=[];
			$addon_arr['twitter']=trim($_POST['addon_twitter'],' @');
			$addon_arr['twitter']=str_replace('https://twitter.com/','',$addon_arr['twitter']);
			$addon_arr['twitter']=trim($_POST['addon_twitter'],' @/');
			if(''==$addon_arr['twitter']){
				unset($addon_arr['twitter']);
			}
			$addon_arr['linkedin']=trim($_POST['addon_linkedin']);
			$addon_arr['linkedin']=str_replace('https://www.linkedin.com/in/','',$addon_arr['linkedin']);
			$addon_arr['linkedin']=trim($_POST['addon_linkedin'],'/');
			if(''==$addon_arr['linkedin']){
				unset($addon_arr['linkedin']);
			}
			$addon_arr['telegram']=trim($_POST['addon_telegram'],' @');
			if(''==$addon_arr['telegram']){
				unset($addon_arr['telegram']);
			}
			$addon_arr['website']=trim($_POST['addon_website']);
			//add protocol if not present
			if($addon_arr['website']){
				if(!preg_match('/^https?:\/\//',$addon_arr['website'])){
					$addon_arr['website']='https://'.$addon_arr['website'];
				}
			}
			else{
				unset($addon_arr['website']);
			}
			$addon=json_encode($addon_arr);

			if(0==count($errors)){
				$db->sql("INSERT INTO `event_speakers` (`event`,`public_name`,`user`,`photo_url`,`small_photo_url`,`tiny_photo_url`,`nick_name`,`title`,`company`,`position`,`description`,`platform`,`internal_id`,`internal_username`,`addon`) VALUES ('".$event_id."','".$db->prepare($public_name)."','".$db->prepare($linked_user)."','".$db->prepare($photo_url)."','".$db->prepare($small_photo_url)."','".$db->prepare($tiny_photo_url)."','".$db->prepare($nick_name)."','".$db->prepare($title)."','".$db->prepare($company)."','".$db->prepare($position)."','".$db->prepare($description)."','".$db->prepare($platform)."','".$db->prepare($internal_id)."','".$db->prepare($internal_username)."','".$db->prepare($addon)."')");

				$speaker_id=$db->last_id();

				//add files to database
				foreach($files as $file){
					$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."', `event`='".$event_id."', `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$speaker_id."',`address`='".$allow_event_manage_address_id."'");
					$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
				}

				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Speaker <a href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/edit/'.$speaker_id.'/">"'.htmlspecialchars($public_name).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/speakers/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

				//deleting temporarily uploaded files
				foreach($files as $file){
					unlink($file['path']);
				}
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
			print '<div class="max-w-fit">';
			$object_scheme=[//event speakers
				'public_name'=>['type'=>'text','title'=>'Public name','required'=>true],
				/*
				//future purpose
				'linked_user'=>[
					'type'=>'select','title'=>'Linked user','descr'=>'(optional)',
					'options'=>$db->sql("SELECT `id`,`address` FROM `event_users` WHERE `event`='".$event_id."' AND `speaker`='1'"),
					'key'=>'id',
					'caption_function'=>function($option_id,$option_value){
						global $db,$types_arr;
						$address_arr=$db->sql_row("SELECT `type`,`address` FROM `addresses` WHERE `id`='".$option_value['address']."'");
						$res=htmlspecialchars($address_arr['address']);
						$res.=' ('.htmlspecialchars($types_arr[$address_arr['type']]['name']).')';
						$user_linked_platform=$db->sql_row("SELECT * FROM `linked_platforms` WHERE `address`='".$user_arr['address']."' AND `platform`=1 LIMIT 1");//only telegram
						if(null!=$user_linked_platform){
							$res.=', Telegram: #'.$user_linked_platform['internal_id'];
							if($user_linked_platform['internal_username']){
								$res.=' @'.htmlspecialchars($user_linked_platform['internal_username']);
							}
						}
						return $res;
					},
				],
				*/
				/*
				//future purpose
				'platform'=>['type'=>'select','title'=>'Link to platform','descr'=>'(optional)','options'=>$db->sql("SELECT * FROM `platforms`"),'key'=>'id','caption_value'=>'name','optional'=>true,'optional_value'=>'0','optional_caption'=>'No link'],
				'internal_id'=>['type'=>'text','title'=>'Platform internal ID'],
				'internal_username'=>['type'=>'text','title'=>'Platform internal @username'],
				*/
				'photo'=>['type'=>'image','title'=>'🖼️ Photo image','descr'=>'The minimum recommended size - 500x500 pixels (1:1 ratio, square), 2Mb'],
				'nick_name'=>['type'=>'text','title'=>'Nick name'],
				'title'=>['type'=>'text','title'=>'Title'],
				'company'=>['type'=>'text','title'=>'Company'],
				'position'=>['type'=>'text','title'=>'Position'],
				'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'About speaker','wysiwyg'=>true],
				'addon'=>['type'=>'h3','title'=>'Additional info'],
				'addon_linkedin'=>['type'=>'text','title'=>'LinkedIn','descr'=>'public URL to profile: https://www.linkedin.com/in/<b>URL</b>/','json_data'=>'addon'],
				'addon_twitter'=>['type'=>'text','title'=>'Twitter','descr'=>'public <b>username</b>','json_data'=>'addon'],
				'addon_telegram'=>['type'=>'text','title'=>'Telegram','descr'=>'public username for channel or group','json_data'=>'addon'],
				'addon_website'=>['type'=>'text','title'=>'Website','descr'=>'external link: <b>https://...</b>','json_data'=>'addon'],
			];
			print build_form($object_scheme,false);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create speaker" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/">&larr; Back to speakers</a>';
		print '<h2>Edit speaker</h2>';
		$speaker_arr=$db->sql_row("SELECT * FROM `event_speakers` WHERE `event`='".$event_id."' AND `id`='".(int)$path_array[6]."'");
		if(null!=$speaker_arr){
			if(isset($_POST['public_name'])){
				$errors=[];
				$files=[];
				$remove_files=[];
				$public_name=trim($_POST['public_name']);

				$photo_url=$speaker_arr['photo_url'];
				$small_photo_url=$speaker_arr['small_photo_url'];
				$tiny_photo_url=$speaker_arr['tiny_photo_url'];

				//https://www.php.net/manual/ru/features.file-upload.errors.php
				$upload_max_size=ini_get('upload_max_filesize');
				foreach($_FILES as $file_key=>$file_data){
					switch($file_data['error']){
						case UPLOAD_ERR_INI_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
							break;
					}
				}

				$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/speakers/';
				if(isset($_FILES['photo']) && 0==$_FILES['photo']['error']){
					//check extensions
					$allowed_exts=['jpg','jpeg','png'];
					$ext=pathinfo($_FILES['photo']['name'],PATHINFO_EXTENSION);
					if(in_array($ext,$allowed_exts)){
						if($_FILES['photo']['size']<=1024*1024*2){//check size 2MB
							$photo=md5($public_name).'_'.time().'.'.$ext;
							if(!file_exists($root_dir.$dir)){
								mkdir($root_dir.$dir,0777,true);
							}
							$filename=$dir.$photo;
							move_uploaded_file($_FILES['photo']['tmp_name'],$root_dir.$filename);
							$photo_url=$filename;
							//make small thumbnail 250x250 from photo
							$small_photo=md5($public_name).'_'.time().'_small.'.$ext;
							$small_filename=$dir.$small_photo;
							$small_photo_url=$small_filename;
							$small_photo_path=$root_dir.$small_filename;
							$small_photo_obj=new Imagick($root_dir.$filename);
							$small_photo_obj->thumbnailImage(250,250,true);
							$small_photo_obj->writeImage($small_photo_path);
							//make tiny thumbnail 50x50 from photo
							$tiny_photo=md5($public_name).'_'.time().'_tiny.'.$ext;
							$tiny_filename=$dir.$tiny_photo;
							$tiny_photo_url=$tiny_filename;
							$tiny_photo_path=$root_dir.$tiny_filename;
							$tiny_photo_obj=new Imagick($root_dir.$filename);
							$tiny_photo_obj->thumbnailImage(50,50,true);
							$tiny_photo_obj->writeImage($tiny_photo_path);

							$remove_files[]=$speaker_arr['photo_url'];
							$remove_files[]=$speaker_arr['small_photo_url'];
							$remove_files[]=$speaker_arr['tiny_photo_url'];
							$files[]=['time'=>time(),'name'=>$_FILES['photo']['name'],'size'=>$_FILES['photo']['size'],'type'=>$_FILES['photo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>3];//target=speaker
							$files[]=['time'=>time(),'name'=>$_FILES['photo']['name'],'size'=>filesize($root_dir.$small_filename),'type'=>$_FILES['photo']['type'],'path'=>$root_dir.$small_filename,'relative_path'=>$small_filename,'target'=>3];//target=speaker
							$files[]=['time'=>time(),'name'=>$_FILES['photo']['name'],'size'=>filesize($root_dir.$tiny_filename),'type'=>$_FILES['photo']['type'],'path'=>$root_dir.$tiny_filename,'relative_path'=>$tiny_filename,'target'=>3];//target=speaker
						}
						else{
							$errors[]='&mdash; Photo size must be less than 2MB';
						}
					}
					else{
						$errors[]='&mdash; Photo must be in JPG, JPEG or PNG format';
					}
					$db->sql("UPDATE `event_speakers` SET `photo_url`='".$db->prepare($photo_url)."',`small_photo_url`='".$db->prepare($small_photo_url)."',`tiny_photo_url`='".$db->prepare($tiny_photo_url)."' WHERE `id`='".$speaker_arr['id']."'");
					//deleting old files
					foreach($remove_files as $file){
						$free_file_size=filesize($root_dir.$file);
						unlink($root_dir.$file);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					//add files to database
					foreach($files as $file){
						$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."', `event`='".$event_id."', `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$speaker_arr['id']."',`address`='".$allow_event_manage_address_id."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					}
				}

				$nick_name=trim($_POST['nick_name']);
				$title=trim($_POST['title']);
				$company=trim($_POST['company']);
				$position=trim($_POST['position']);
				$description=trim($_POST['description']);
				$status=intval($_POST['status']);
				$sort=intval($_POST['sort']);

				//link to platform and internal id/username (optional)
				$platform=intval($_POST['platform']);
				$internal_id=trim($_POST['internal_id']);
				$internal_username=trim($_POST['internal_username']);

				$addon_arr=[];
				if($speaker_arr['addon']){
					$addon_arr=json_decode($speaker_arr['addon'],true);
				}
				$addon_arr['twitter']=trim($_POST['addon_twitter'],' @');
				$addon_arr['twitter']=str_replace('https://twitter.com/','',$addon_arr['twitter']);
				$addon_arr['twitter']=trim($_POST['addon_twitter'],' @/');
				if(''==$addon_arr['twitter']){
					unset($addon_arr['twitter']);
				}
				$addon_arr['linkedin']=trim($_POST['addon_linkedin']);
				$addon_arr['linkedin']=str_replace('https://www.linkedin.com/in/','',$addon_arr['linkedin']);
				$addon_arr['linkedin']=trim($_POST['addon_linkedin'],'/');
				if(''==$addon_arr['linkedin']){
					unset($addon_arr['linkedin']);
				}
				$addon_arr['telegram']=trim($_POST['addon_telegram'],' @');
				if(''==$addon_arr['telegram']){
					unset($addon_arr['telegram']);
				}
				$addon_arr['website']=trim($_POST['addon_website']);
				//add protocol if not present
				if($addon_arr['website']){
					if(!preg_match('/^https?:\/\//',$addon_arr['website'])){
						$addon_arr['website']='https://'.$addon_arr['website'];
					}
				}
				else{
					unset($addon_arr['website']);
				}
				$addon=json_encode($addon_arr);

				if(0==count($errors)){
					$db->sql("UPDATE `event_speakers` SET
					`platform`='".$platform."',
					`internal_id`='".$db->prepare($internal_id)."', `internal_username`='".$db->prepare($internal_username)."',
					`public_name`='".$db->prepare($public_name)."', `nick_name`='".$db->prepare($nick_name)."',
					`title`='".$db->prepare($title)."',
					`company`='".$db->prepare($company)."', `position`='".$db->prepare($position)."',
					`description`='".$db->prepare($description)."',
					`status`='".$db->prepare($status)."', `sort`='".$db->prepare($sort)."',
					`addon`='".$db->prepare($addon)."' WHERE `id`='".$speaker_arr['id']."'");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Speaker <a href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/edit/'.$speaker_arr['id'].'/">"'.htmlspecialchars($public_name).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/speakers/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event edit speakers
					'public_name'=>['type'=>'text','title'=>'Public name','required'=>true],
					/*
					//future purpose
					'linked_user'=>[
						'type'=>'select','title'=>'Linked user','descr'=>'(optional)',
						'options'=>$db->sql("SELECT `id`,`address` FROM `event_users` WHERE `event`='".$event_id."' AND `speaker`='1'"),
						'key'=>'id',
						'caption_function'=>function($option_id,$option_value){
							global $db,$types_arr;
							$address_arr=$db->sql_row("SELECT `type`,`address` FROM `addresses` WHERE `id`='".$option_value['address']."'");
							$res=htmlspecialchars($address_arr['address']);
							$res.=' ('.htmlspecialchars($types_arr[$address_arr['type']]['name']).')';
							$user_linked_platform=$db->sql_row("SELECT * FROM `linked_platforms` WHERE `address`='".$user_arr['address']."' AND `platform`=1 LIMIT 1");//only telegram
							if(null!=$user_linked_platform){
								$res.=', Telegram: #'.$user_linked_platform['internal_id'];
								if($user_linked_platform['internal_username']){
									$res.=' @'.htmlspecialchars($user_linked_platform['internal_username']);
								}
							}
							return $res;
						},
					],
					*/
					/*
					//future purpose
					'platform'=>['type'=>'select','title'=>'Link to platform','descr'=>'(optional)','options'=>$db->sql("SELECT * FROM `platforms`"),'key'=>'id','caption_value'=>'name','optional'=>true,'optional_value'=>'0','optional_caption'=>'No link'],
					'internal_id'=>['type'=>'text','title'=>'Platform internal ID'],
					'internal_username'=>['type'=>'text','title'=>'Platform internal @username'],
					*/
					'photo'=>[
						'type'=>'image',
						'title'=>'🖼️ Photo image',
						'descr'=>'The minimum recommended size - 500x500 pixels (1:1 ratio, square), 2Mb',
						'object_type'=>'speakers',
						'file_path'=>'photo_url',
						'thumbnail_path'=>'small_photo_url',
					],
					'nick_name'=>['type'=>'text','title'=>'Nick name'],
					'title'=>['type'=>'text','title'=>'Title'],
					'company'=>['type'=>'text','title'=>'Company'],
					'position'=>['type'=>'text','title'=>'Position'],
					'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'About speaker','wysiwyg'=>true],
					'sp1'=>['type'=>'hr'],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$speakers_status_arr,
						'default_option'=>$speakers_status_default_id,
						'classes'=>$speakers_status_style_arr,
					],
					'sort'=>['type'=>'number','title'=>'Sort order','min'=>0,'max'=>10],
					'addon'=>['type'=>'h3','title'=>'Additional info'],
					'addon_linkedin'=>['type'=>'text','title'=>'LinkedIn','descr'=>'public URL to profile: https://www.linkedin.com/in/<b>URL</b>/','json_data'=>'addon'],
					'addon_twitter'=>['type'=>'text','title'=>'Twitter','descr'=>'public <b>username</b>','json_data'=>'addon'],
					'addon_telegram'=>['type'=>'text','title'=>'Telegram','descr'=>'public username for channel or group','json_data'=>'addon'],
					'addon_website'=>['type'=>'text','title'=>'Website','descr'=>'external link: <b>https://...</b>','json_data'=>'addon'],
				];
				print build_form($object_scheme,$speaker_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';
				print '<h2 class="mt-6">Linked sessions</h2>';
				if(0!=$db->table_count('event_sessions_speakers',"WHERE `event`='".$event_id."' AND `speaker`='".$speaker_arr['id']."'")){
					print '<div class="flex flex-col">
					<div class="table-wrapper">
					<div class="py-2 inline-block min-w-full">
					<div class="overflow-hidden">';
					print '<table class="table-auto min-w-full">';
					print '<thead class="bg-white border-b">';
					print '<tr>';
					print '<th>Start time</th>';
					print '<th>Duration</th>';
					print '<th>Session</th>';
					print '<th>Status</th>';
					print '</tr>';
					print '</thead>';
					print '<tbody>';
					$sessions=$db->sql("SELECT `es`.*
					FROM `event_sessions_speakers` as `ess`
					INNER JOIN `event_sessions` as `es` ON `ess`.`event`='".$event_id."' AND `ess`.`session`=`es`.`id` WHERE `ess`.`speaker`='".$speaker_arr['id']."' ORDER BY `es`.`endtime` DESC, `es`.`time` ASC");
					foreach($sessions as $session_item){
						print '<tr>';
						print '<td><span class="time" data-time="'.$session_item['time'].'">'.date('d.m.Y H:i',$session_item['time']).' GMT</span></td>';
						print '<td>'.$session_item['duration'].'</td>';
						print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/edit/'.$session_item['id'].'/" target="_blank">'.htmlspecialchars($session_item['caption']).'</a></td>';
						print '<td>'.($session_item['endtime']>time()?'Scheduled':'<span class="text-gray-500">Finished</span>').'</td>';
						print '</tr>';
					}
					print '</tbody>';
					print '</table>';
					print '</div></div></div></div>';
				}
				else{
					print '<p>No sessions linked to this speaker.</p>';
				}
			}
		}
		else{
			print '<h2 class="text-red-600">Speaker not found</h2>';
			print '<p>Return to speakers list and try again.</p>';
		}
	}
	else{
		print '<a class="action-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/create/">Create speaker</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		$filter_caption=false;
		if(isset($_GET['caption'])){
			if(''!==$_GET['caption']){
				$filter_caption=trim($_GET['caption']);
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="caption" class="form-post-by-enter" placeholder="Search" value="'.($filter_caption?htmlspecialchars($filter_caption):'').'">
			</div>';
		print '</div>';
		print '</form>';

		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>Photo</th>';
			print '<th>Public name</th>';
			print '<th>Nick name</th>';
			print '<th>Title</th>';
			print '<th>Company</th>';
			print '<th>Position</th>';
			print '<th>Status</th>';
			print '<th>Sort</th>';
			print '<th>Addon</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]="`event`='".$event_id."'";
		if(false!==$filter_caption){
			$sql_addon[]='`public_name` LIKE \'%'.$filter_caption.'%\' OR `nick_name` LIKE \'%'.$filter_caption.'%\' OR `title` LIKE \'%'.$filter_caption.'%\' OR `company` LIKE \'%'.$filter_caption.'%\' OR `position` LIKE \'%'.$filter_caption.'%\'';
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}

		$speakers=$db->sql("SELECT * FROM `event_speakers`".$sql_addon_str." ORDER BY `id` ASC");
		foreach($speakers as $speaker){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/delete/'.$speaker['id'].'/" class="red-btn">Delete</a></td>';
			if($speaker['tiny_photo_url']){
				print '<td><img src="'.$speaker['tiny_photo_url'].'" class="avatar"></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td class="whitespace-nowrap">
				<a href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/edit/'.$speaker['id'].'/">'.htmlspecialchars($speaker['public_name']).'</a>
				<a href="/@'.$organizer_url.'/'.$event_url.'/speakers/'.htmlspecialchars($speaker['id']).'" target="_blank" class="text-blue-500 !no-underline">⧉</a>
			</td>';
			print '<td>'.htmlspecialchars($speaker['nick_name']).'</td>';
			print '<td>'.htmlspecialchars($speaker['title']).'</td>';
			print '<td>'.htmlspecialchars($speaker['company']).'</td>';
			print '<td>'.htmlspecialchars($speaker['position']).'</td>';

			print '<td>';
			print '<span class="'.$speakers_status_style_arr[$speaker['status']].'">';
			print htmlspecialchars($speakers_status_arr[$speaker['status']]);
			print '</span>';
			print '</td>';
			print '<td>'.htmlspecialchars($speaker['sort']).'</td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($speaker['addon']).'</div></td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}
}
elseif('moderation'==$path_array[4]){
	$replace['title']='Moderation | '.$replace['title'];
	print '<h2>Moderation</h2>';
	if(isset($_POST['event_moderation'])){
		if(1!=$event_arr['moderation']){//not approved
			if(($config['event_moderation_max_rejects']-1)<$event_arr['reject_counter']){
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Event permanently rejected. You can not create new moderation request.</p>
				</div>';
			}
			else{
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Event sent to moderation.</p>
				</div>';

				//send event to moderation again (change status to 1 - active event)
				$db->sql("UPDATE `events` SET `status`=1 WHERE `id`='".$event_arr['id']."'");

				//get all admins and send them notification about event moderation request
				$platform_admins=$db->sql("SELECT `id` FROM `addresses` WHERE `status`='1'");
				foreach($platform_admins as $platform_admin){
					add_notify($platform_admin['id'],0,1,'admin_moderation_event',json_encode([
						'organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url']),
						'event_title'=>htmlspecialchars($title),
						'event_url'=>htmlspecialchars($url)
					]));
				}
			}
		}
		else{
			print '
			<div class="success-box" role="alert">
				<p class="font-bold">Success</p>
				<p>Event already approved.</p>
			</div>';
		}
	}
	else{
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';
		print '<p>Moderation status: <span class="'.$events_moderation_style_arr[$event_arr['moderation']].'">'.$events_moderation_arr[$event_arr['moderation']].'</span></p>';
		if(0!=$event_arr['reject_counter']){
			print '<p>Reject counter: '.$event_arr['reject_counter'].'</p>';
			if(''!=$event_arr['reject_comment']){
				print '<p>Reject comment:<br>'.nl2br($event_arr['reject_comment']).'</p>';
			}
		}
		if(0==$event_arr['moderation']){
			if(1!=$event_arr['status']){
				print '<p>You can send event to moderation by changing Status to "Active" on <a href="/@'.$organizer_url.'/'.$event_url.'/manage/event/">Event page</a> when event is ready.</p>';
				print '<p>Or press button below to send event to moderation right now.</p>';
			}
			print '<form action="" method="POST">';
			print '<div class="grid-wrapper">';
			print '<input type="submit" name="event_moderation" value="Send event to moderation" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
		if(1==$event_arr['moderation']){
			print '<p><b>Event successfully approved by moderator. Enjoy!</b></p>';
		}
		if(2==$event_arr['moderation']){
			if(($config['event_moderation_max_rejects']-1)<$event_arr['reject_counter']){
				print '<p>Event is permanently rejected.</p>';
			}
			else{
				print '<p><b>Event rejected by moderator. Please fix issues and send event to moderation again.</b></p>';
				$last_try=false;
				if(($config['event_moderation_max_rejects']-1)==$event_arr['reject_counter']){
					$last_try=true;
					print '<p>Event will be permanently halted if not met conditions after next moderation request.</p>';
				}
				print '<form action="" method="POST">';
				print '<div class="grid-wrapper">';
				print '<input type="submit" name="event_moderation" value="Send fixed event to moderation'.($last_try?' (last try)':'').'" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
	}
}
elseif('sessions'==$path_array[4]){
	$replace['title']='Sessions | '.$replace['title'];
	if('delete'==$path_array[5]){
		$session_id=(int)$path_array[6];
		$session_arr=$db->sql_row("SELECT * FROM `event_sessions` WHERE `event`='".$event_id."' AND `id`='".$session_id."'");
		if(null!=$session_arr){
			if(isset($_POST['approve'])){
				if(''!=$session_arr['cover_url']){
					if(file_exists($root_dir.$session_arr['cover_url'])){
						$free_file_size=filesize($root_dir.$session_arr['cover_url']);
						unlink($root_dir.$session_arr['cover_url']);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$session_arr['cover_url'])."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
				}
				if(''!=$session_arr['thumbnail_cover_url']){
					if(file_exists($root_dir.$session_arr['thumbnail_cover_url'])){
						$free_file_size=filesize($root_dir.$session_arr['thumbnail_cover_url']);
						unlink($root_dir.$session_arr['thumbnail_cover_url']);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$session_arr['thumbnail_cover_url'])."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
				}
				$db->sql("DELETE FROM `event_sessions` WHERE `id`='".$session_id."'");
				$db->sql("DELETE FROM `event_sessions_speakers` WHERE `session`='".$session_id."'");
				header('Location:/@'.$organizer_url.'/'.$event_url.'/manage/sessions/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/">&larr; Back to sessions</a>';
				print '<h2 class="text-red-600">Remove session #'.$session_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Session will be removed from the database.</p>
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove session" class="red-btn">';
				print '</form>';

			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/">&larr; Back to sessions</a>';
			print '<h2 class="text-red-600">Session not found</h2>';
			print '<p>Return to sessions list and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/">&larr; Back to sessions</a>';
		print '<h2>Create session</h2>';
		if(isset($_POST['caption'])){
			$errors=[];
			$files=[];
			$caption=trim($_POST['caption']);
			if(0!=$db->table_count('event_sessions',"WHERE `event`='".$event_id."' AND `caption`='".$db->prepare($caption)."'")){
				$errors[]='Session with the same caption already exist';
			}
			$cover_url='';
			$thumbnail_cover_url='';

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			$upload_max_size=ini_get('upload_max_filesize');
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/sessions/';
			if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png'];
				$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
						$cover=md5($caption).'_'.time().'.'.$ext;
						if(!file_exists($root_dir.$dir)){
							mkdir($root_dir.$dir,0777,true);
						}
						$filename=$dir.$cover;
						move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
						$cover_url=$filename;
						//make small thumbnail 250x250 from cover
						$thumbnail_cover=md5($caption).'_'.time().'_thumbnail.'.$ext;
						$thumbnail_filename=$dir.$thumbnail_cover;
						$thumbnail_cover_url=$thumbnail_filename;
						$thumbnail_cover_path=$root_dir.$thumbnail_filename;
						$thumbnail_cover_obj=new Imagick($root_dir.$filename);
						$thumbnail_cover_obj->thumbnailImage(250,250,true);
						$thumbnail_cover_obj->writeImage($thumbnail_cover_path);

						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>4];//target=session
						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>filesize($root_dir.$thumbnail_filename),'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$thumbnail_filename,'relative_path'=>$thumbnail_filename,'target'=>4];//target=session
					}
					else{
						$errors[]='&mdash; Cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Cover must be in JPG, JPEG or PNG format';
				}
			}

			$description=trim($_POST['description']);
			$location=(int)$_POST['location'];
			$start_time=(int)$_POST['unixtime'];
			$duration=(int)$_POST['duration'];//minutes
			$endtime=$start_time + 60*$duration;

			$content=trim($_POST['content']);
			$embed_content=trim($_POST['embed_content']);
			$level=intval($_POST['level']);
			if($level<0){
				$errors[]='Negative level';
			}
			else{
				$level_exist=$db->table_count('event_levels',"WHERE `level`='".$level."' AND `event`='".$event_id."'");
				if(0==$level_exist){
					$errors[]='Level '.$level.' not exist';
				}
			}

			$status=0;
			if($sessions_status_arr[$_POST['status']]){
				$status=(int)$_POST['status'];
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `event_sessions` (`event`,`time`,`duration`,`endtime`,`caption`,`description`,`location`,`cover_url`,`thumbnail_cover_url`,`content`,`embed_content`,`level`,`status`) VALUES ('".$event_id."','".$db->prepare($start_time)."','".$db->prepare($duration)."','".$db->prepare($endtime)."','".$db->prepare($caption)."','".$db->prepare($description)."','".$db->prepare($location)."','".$db->prepare($cover_url)."','".$db->prepare($thumbnail_cover_url)."','".$db->prepare($content)."','".$db->prepare($embed_content)."','".$level."','".$status."')");

				$session_id=$db->last_id();

				//add files to database
				foreach($files as $file){
					$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."', `event`='".$event_id."', `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$session_id."',`address`='".$allow_event_manage_address_id."'");
					$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
				}

				$db->sql("DELETE FROM `event_sessions_speakers` WHERE `event`='".$event_id."' AND `session`='".$db->prepare($session_id)."'");
				if(isset($_POST['speaker'])){
					foreach($_POST['speaker'] as $speaker_id=>$speaker_status){
						if('on'==$speaker_status){
							$db->sql("INSERT INTO `event_sessions_speakers` (`event`,`session`,`speaker`) VALUES ('".$event_id."','".$db->prepare($session_id)."','".$db->prepare($speaker_id)."')");
						}
					}
				}

				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Session <a href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/edit/'.$session_id.'/">"'.htmlspecialchars($caption).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/sessions/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

				//deleting temporarily uploaded files
				foreach($files as $file){
					unlink($file['path']);
				}
			}
		}
		else{
			$locations_count=$db->table_count('event_locations','WHERE `event`='.$event_id);
			if(0==$locations_count){
				//write notice and link to /partners_cat/ create category
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Go to <a href="/@'.$organizer_url.'/'.$event_url.'/manage/locations/create/">locations</a> and create at least one location where the event will take place.</p>
				</div>';
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event edit session
					'caption'=>['type'=>'text','title'=>'Caption','required'=>true],
					'description'=>['type'=>'textarea','title'=>'Description'],
					'cover'=>[
						'type'=>'image',
						'title'=>'🖼️ Cover image',
						'descr'=>'The minimum recommended size - 320x180 pixels (16:9 ratio), 2Mb',
						'object_type'=>'sessions',
						'file_path'=>'cover_url',
						'thumbnail_path'=>'thumbnail_cover_url	',
					],
					'datetime'=>[
						'type'=>'datetime',
						'key'=>'time',
						'local'=>true,//-local
						'future'=>true,
						'title'=>'Start date time (by your local time)',
						'timestamp'=>'unixtime',
						'value_title'=>'Start date time',
					],
					'duration'=>[
						'type'=>'number',
						'title'=>'Duration (minutes)',
						'min'=>1,
						'max'=>3600,
						'required'=>true,
					],
					'location'=>[
						'type'=>'select',
						'title'=>'Location',
						'options'=>$db->sql('SELECT * FROM `event_locations` WHERE `event`='.$event_id.' AND `status`=0 ORDER BY `caption` ASC'),
						'key'=>'id',
						'caption_value'=>'caption',
						'optional'=>true,
						'optional_value'=>'0',
						'optional_caption'=>'(none)',
					],
					'hr1'=>['type'=>'hr'],
					'content'=>['type'=>'textarea','title'=>'📄 Content for participiants','placeholder'=>'HTML content','wysiwyg'=>true],
					'embed_content'=>['type'=>'textarea','title'=>'📄 Embed content (html)','placeholder'=>'Embed HTML'],
					'level'=>[
						'type'=>'select',
						'title'=>'Content is available to participants from the level',
						'options'=>$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `level` ASC"),
						'key'=>'level',
						'caption_template'=>'{name} {caption}',
						'caption_function'=>function($option_id,$option_value){
							return $option_value['name'].($option_value['caption']?' ('.$option_value['caption'].')':'');
						},
					],
					'hr2'=>['type'=>'hr'],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$sessions_status_arr,
						//'default_option'=>$sessions_status_id,
						'classes'=>$sessions_status_style_arr,
					],
					'speaker'=>[
						'type'=>'multi-checkbox',
						'title'=>'Speakers',
						'options'=>$db->sql('SELECT * FROM `event_speakers` WHERE `event`='.$event_id.' AND `status`!=2 ORDER BY `status` DESC, `public_name` ASC'),
						'key'=>'id',
						'value'=>'caption',
						'value_function'=>function($option){
							global $db,$event_id;
							return (1==$option['status']?'⭐ ':'').htmlspecialchars($option['public_name']);
						},
					],
				];
				print build_form($object_scheme,false);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Create session" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
	}
	elseif('edit'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/">&larr; Back to sessions</a>';
		print '<h2>Edit session</h2>';
		$session_id=(int)$path_array[6];
		$session_item=$db->sql_row("SELECT * FROM `event_sessions` WHERE `event`='".$event_id."' AND `id`='".$session_id."'");
		if(null!=$session_item){
			if(isset($_POST['caption'])){
				$errors=[];
				$files=[];
				$remove_files=[];
				$cover_url=$session_item['cover_url'];
				$thumbnail_cover_url=$session_item['thumbnail_cover_url'];

				//https://www.php.net/manual/ru/features.file-upload.errors.php
				$upload_max_size=ini_get('upload_max_filesize');
				foreach($_FILES as $file_key=>$file_data){
					switch($file_data['error']){
						case UPLOAD_ERR_INI_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
							break;
					}
				}

				$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/sessions/';
				if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
					//check extensions
					$allowed_exts=['jpg','jpeg','png'];
					$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
					if(in_array($ext,$allowed_exts)){
						if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
							$cover=md5($caption).'_'.time().'.'.$ext;
							if(!file_exists($root_dir.$dir)){
								mkdir($root_dir.$dir,0777,true);
							}
							$filename=$dir.$cover;
							move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
							$cover_url=$filename;
							//make small thumbnail 250x250 from cover
							$thumbnail_cover=md5($caption).'_'.time().'_thumbnail.'.$ext;
							$thumbnail_filename=$dir.$thumbnail_cover;
							$thumbnail_cover_url=$thumbnail_filename;
							$thumbnail_cover_path=$root_dir.$thumbnail_filename;
							$thumbnail_cover_obj=new Imagick($root_dir.$filename);
							$thumbnail_cover_obj->thumbnailImage(250,250,true);
							$thumbnail_cover_obj->writeImage($thumbnail_cover_path);

							$remove_files[]=$session_item['cover_url'];
							$remove_files[]=$session_item['thumbnail_cover_url'];
							$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>4];//target=session
							$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>filesize($root_dir.$thumbnail_filename),'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$thumbnail_filename,'relative_path'=>$thumbnail_filename,'target'=>4];//target=session
						}
						else{
							$errors[]='&mdash; Cover size must be less than 2MB';
						}
					}
					else{
						$errors[]='&mdash; Cover must be in JPG, JPEG or PNG format';
					}
				}

				$caption=trim($_POST['caption']);
				$description=trim($_POST['description']);
				$location=(int)$_POST['location'];
				$start_time=(int)$_POST['unixtime'];
				$duration=(int)$_POST['duration'];//minutes
				$endtime=$start_time + 60*$duration;

				$content=trim($_POST['content']);
				$embed_content=trim($_POST['embed_content']);
				$level=intval($_POST['level']);
				if($level<0){
					$errors[]='Negative level';
				}
				else{
					$level_exist=$db->table_count('event_levels',"WHERE `level`='".$level."' AND `event`='".$event_id."'");
					if(0==$level_exist){
						$errors[]='Level '.$level.' not exist';
					}
				}

				$status=0;
				if($sessions_status_arr[$_POST['status']]){
					$status=(int)$_POST['status'];
				}

				if(0==count($errors)){
					$db->sql("UPDATE `event_sessions` SET `caption`='".$db->prepare($caption)."', `description`='".$db->prepare($description)."', `location`='".$location."', `time`='".$start_time."', `endtime`='".$endtime."', `duration`='".$duration."', `content`='".$db->prepare($content)."', `embed_content`='".$db->prepare($embed_content)."', `level`='".$level."', `status`='".$status."', `cover_url`='".$db->prepare($cover_url)."', `thumbnail_cover_url`='".$db->prepare($thumbnail_cover_url)."' WHERE `id`='".$session_id."'");

					//deleting old files
					foreach($remove_files as $file){
						$free_file_size=filesize($root_dir.$file);
						unlink($root_dir.$file);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					//add files to database
					foreach($files as $file){
						$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."',`event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$session_id."',`address`='".$allow_event_manage_address_id."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					}

					$db->sql("DELETE FROM `event_sessions_speakers` WHERE `session`='".$session_id."'");
					foreach($_POST['speaker'] as $speaker_id=>$speaker_status){
						if('on'==$speaker_status){
							$db->sql("INSERT INTO `event_sessions_speakers` (`event`,`session`,`speaker`) VALUES ('".$event_id."','".$db->prepare($session_item['id'])."','".$db->prepare($speaker_id)."')");
						}
					}

					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Session <a href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/edit/'.$session_item['id'].'/">"'.htmlspecialchars($caption).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/sessions/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

					//deleting temporarily uploaded files
					foreach($files as $file){
						unlink($file['path']);
					}
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event edit session
					'caption'=>['type'=>'text','title'=>'Caption','required'=>true],
					'description'=>['type'=>'textarea','title'=>'Description'],
					'cover'=>[
						'type'=>'image',
						'title'=>'🖼️ Cover image',
						'descr'=>'The minimum recommended size - 320x180 pixels (16:9 ratio), 2Mb',
						'object_type'=>'sessions',
						'file_path'=>'cover_url',
						'thumbnail_path'=>'thumbnail_cover_url	',
					],
					'datetime'=>[
						'type'=>'datetime',
						'key'=>'time',
						'local'=>true,//-local
						'future'=>true,
						'title'=>'Start date time (by your local time)',
						'timestamp'=>'unixtime',
						'value_title'=>'Start date time',
						'value'=>0,
					],
					'duration'=>[
						'type'=>'number',
						'title'=>'Duration (minutes)',
						'min'=>1,
						'max'=>3600,
						'required'=>true,
					],
					'location'=>[
						'type'=>'select',
						'title'=>'Location',
						'options'=>$db->sql('SELECT * FROM `event_locations` WHERE `event`='.$event_id.' AND `status`=0 ORDER BY `caption` ASC'),
						'key'=>'id',
						'caption_value'=>'caption',
						'optional'=>true,
						'optional_value'=>'0',
						'optional_caption'=>'(none)',
					],
					'hr1'=>['type'=>'hr'],
					'content'=>['type'=>'textarea','title'=>'📄 Content for participiants','placeholder'=>'HTML content','wysiwyg'=>true],
					'embed_content'=>['type'=>'textarea','title'=>'📄 Embed content (html)','placeholder'=>'Embed HTML'],
					'level'=>[
						'type'=>'select',
						'title'=>'Content is available to participants from the level',
						'options'=>$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `level` ASC"),
						'key'=>'level',
						'caption_template'=>'{name} {caption}',
						'caption_function'=>function($option_id,$option_value){
							return $option_value['name'].($option_value['caption']?' ('.$option_value['caption'].')':'');
						},
					],
					'hr2'=>['type'=>'hr'],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$sessions_status_arr,
						//'default_option'=>$sessions_status_id,
						'classes'=>$sessions_status_style_arr,
					],
					'speaker'=>[
						'type'=>'multi-checkbox',
						'title'=>'Speakers',
						'options'=>$db->sql('SELECT * FROM `event_speakers` WHERE `event`='.$event_id.' AND `status`!=2 ORDER BY `status` DESC, `public_name` ASC'),
						'key'=>'id',
						'value'=>'caption',
						'value_function'=>function($option){
							global $db,$event_id;
							return (1==$option['status']?'⭐ ':'').htmlspecialchars($option['public_name']);
						},
						'checked_function'=>function($option,$object){
							global $db,$event_id;
							return (0<$db->table_count('event_sessions_speakers',"WHERE `session`='".$object['id']."' AND `speaker`='".$option['id']."'"));
						},
					],
				];
				print build_form($object_scheme,$session_item);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Update session" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Session not found</p>
			</div>';
			print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
		}
	}
	else{
		print '<a class="action-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/create/">Create session</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		$filter_location=false;
		if(isset($_GET['location'])){
			if(''!==$_GET['location']){
				$filter_location=(int)$_GET['location'];
				if(0==$db->table_count('event_locations',"WHERE `event`='".$event_id."' AND `id`='".$filter_location."'")){
					$filter_location=false;
				}
			}
		}

		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($sessions_status_arr[$filter_status])){
					$filter_status=false;
				}
			}
		}

		$filter_caption=false;
		if(isset($_GET['caption'])){
			if(''!==$_GET['caption']){
				$filter_caption=trim($_GET['caption']);
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="caption" class="form-post-by-enter" placeholder="Filter by caption" value="'.($filter_caption?htmlspecialchars($filter_caption):'').'">
			</div>
			<div>
				<select name="location" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_location?' selected':'').'>Filter by location</option>';
					$locations=$db->sql('SELECT * FROM `event_locations` WHERE `event`='.$event_id.' AND `status`=0 ORDER BY `caption` ASC');
					foreach($locations as $location){
						print '<option value="'.htmlspecialchars($location['id']).'" class="'.$locations_status_style_arr[$location['status']].'"'.($location['id']==$filter_location?' selected':'').'>Location: '.htmlspecialchars($location['caption']).'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
					foreach($sessions_status_arr as $sessions_status_id=>$sessions_status_caption){
						print '<option value="'.htmlspecialchars($sessions_status_id).'" class="'.$sessions_status_style_arr[$sessions_status_id].'"'.($sessions_status_id==$filter_status?' selected':'').'>Status: '.htmlspecialchars($sessions_status_caption).'</option>';
					}
					print '
				</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>Cover</th>';
			print '<th>Caption</th>';
			print '<th>Description</th>';
			print '<th>Time</th>';
			print '<th>Duration</th>';
			print '<th>Location</th>';
			print '<th>Content</th>';
			print '<th>Level</th>';
			print '<th>Status</th>';
			print '<th>Speakers</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]='`event`='.$event_id;
		if(false!==$filter_status){
			$sql_addon[]='`status`='.$filter_status;
		}
		if(false!==$filter_location){
			$sql_addon[]='`location`='.$filter_location;
		}
		if(false!==$filter_caption){
			$sql_addon[]='`caption` LIKE \'%'.$filter_caption.'%\'';
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}

		$per_page=50;
		$count=$db->table_count('event_sessions',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$sessions=$db->sql("SELECT * FROM `event_sessions`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($sessions as $session_item){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/delete/'.$session_item['id'].'/" class="red-btn">Delete</a></td>';
			if($session_item['thumbnail_cover_url']){
				print '<td><img src="'.$session_item['thumbnail_cover_url'].'" class="cover"></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>
				<a href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/edit/'.$session_item['id'].'/">'.htmlspecialchars($session_item['caption']).'</a>
				<a href="/@'.$organizer_url.'/'.$event_url.'/sessions/'.htmlspecialchars($session_item['id']).'" target="_blank" class="text-blue-500 !no-underline">⧉</a>
			</td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($session_item['description']).'</div></td>';
			print '<td><span class="time" data-time="'.$session_item['time'].'">'.date('d.m.Y H:i',$session_item['time']).' GMT</span></td>';
			print '<td>'.$session_item['duration'].'</td>';
			$location_arr=$db->sql_row('SELECT * FROM `event_locations` WHERE `id`='.$session_item['location'].'');
			if(null!=$location_arr){
				print '<td>'.htmlspecialchars($location_arr['caption']).'</td>';
			}
			else{
				print '<td>&mdash;</td>';
			}

			print '<td>'.((''!=$session_item['content'])||(''!=$session_item['embed_content'])?'✔️':'❌').'</td>';
			print '<td>'.$session_item['level'].'</td>';

			print '<td>';
			print '<span class="'.$sessions_status_style_arr[$session_item['status']].'">';
			print htmlspecialchars($sessions_status_arr[$session_item['status']]);
			print '</span>';
			print '</td>';
			$speakers_count=$db->table_count('event_sessions_speakers',"WHERE `session`=".$session_item['id']);
			print '<td>'.$speakers_count.'</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('content'==$path_array[4]){
	$script_preset.='var wysiwyg_toolbar_remove=["heading1"];';
	$replace['title']='Content | '.$replace['title'];
	if('delete'==$path_array[5]){
		$content_id=(int)$path_array[6];
		$content_arr=$db->sql_row("SELECT * FROM `event_content` WHERE `event`='".$event_id."' AND `id`='".$content_id."'");
		if(3==$content_arr['status']){
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content is banned</p>
			</div>';
		}
		else
		if(null!=$content_arr){
			if(isset($_POST['approve'])){
				if(''!=$content_arr['cover_url']){
					if(file_exists($root_dir.$content_arr['cover_url'])){
						$free_file_size=filesize($root_dir.$content_arr['cover_url']);
						unlink($root_dir.$content_arr['cover_url']);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$content_arr['cover_url'])."'");$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
				}
				if(''!=$content_arr['thumbnail_cover_url']){
					if(file_exists($root_dir.$content_arr['thumbnail_cover_url'])){
						$free_file_size=filesize($root_dir.$content_arr['thumbnail_cover_url']);
						unlink($root_dir.$content_arr['thumbnail_cover_url']);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$content_arr['thumbnail_cover_url'])."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
				}
				$db->sql("DELETE FROM `event_content` WHERE `id`='".$content_id."'");
				$db->sql("DELETE FROM `event_content_tags` WHERE `content`='".$content_id."'");
				header('Location:/@'.$organizer_url.'/'.$event_url.'/manage/content/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/content/">&larr; Back to content list</a>';
				print '<h2 class="text-red-600">Remove content #'.$content_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Content will be removed from the database.</p>
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove content" class="red-btn">';
				print '</form>';

			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/content/">&larr; Back to content list</a>';
			print '<h2 class="text-red-600">Content not found</h2>';
			print '<p>Return to content list and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		unset($content_status_arr[3]);//remove banned status for event manager
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/content/">&larr; Back to content list</a>';
		print '<h2>Create content</h2>';
		if(isset($_POST['url'])){
			$errors=[];
			$files=[];
			$url=trim($_POST['url']);
			$url=mb_strtolower($url);
			$url=preg_replace('/\.+/','.',$url);
			if(0!=$db->table_count('event_content',"WHERE `event`='".$event_id."' AND `url`='".$db->prepare($url)."'")){
				$errors[]='Content with the same URL already exist';
			}
			if(in_array($url,$reserved_urls)){
				$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
			}

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			$upload_max_size=ini_get('upload_max_filesize');
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			$cover_url='';
			$thumbnail_cover_url='';

			$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/content/';
			if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png'];
				$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
						$cover=md5($_FILES['cover']['name']).'_'.time().'.'.$ext;
						if(!file_exists($root_dir.$dir)){
							mkdir($root_dir.$dir,0777,true);
						}
						$filename=$dir.$cover;
						move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
						$cover_url=$filename;
						//make small thumbnail 250x250 from cover
						$thumbnail_cover=md5($_FILES['cover']['name']).'_'.time().'_thumbnail.'.$ext;
						$thumbnail_filename=$dir.$thumbnail_cover;
						$thumbnail_cover_url=$thumbnail_filename;
						$thumbnail_cover_path=$root_dir.$thumbnail_filename;
						$thumbnail_cover_obj=new Imagick($root_dir.$filename);
						$thumbnail_cover_obj->thumbnailImage(250,250,true);
						$thumbnail_cover_obj->writeImage($thumbnail_cover_path);

						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1];//target=content
						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>filesize($root_dir.$thumbnail_filename),'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$thumbnail_filename,'relative_path'=>$thumbnail_filename,'target'=>1];//target=content
					}
					else{
						$errors[]='&mdash; Cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Cover must be in JPG, JPEG or PNG format';
				}
			}

			$title=trim($_POST['title']);
			if(''==$title){
				$errors[]='&mdash; Title is required';
			}
			$description=trim($_POST['description']);
			$content=trim($_POST['content']);

			$level=intval($_POST['level']);
			if($level<0){
				$errors[]='Negative level';
			}
			else{
				$level_exist=$db->table_count('event_levels',"WHERE `level`='".$level."' AND `event`='".$event_id."'");
				if(0==$level_exist){
					$errors[]='Level '.$level.' not exist';
				}
			}

			$status=0;
			if($content_status_arr[$_POST['status']]){
				$status=(int)$_POST['status'];
			}
			$publishing_time=(int)$_POST['unixtime'];
			if(0==$status){//if status is planned
				if($publishing_time<time()){//if time is in past
					$publishing_time=0;
					//$errors[]='&mdash; Publishing time must be in future for planned content';
				}
			}
			if(1==$status){//if status is published
				if($publishing_time<time()){//if time is in past
					$publishing_time=time();//set time to now
				}
			}
			$pin=0;
			if(isset($_POST['pin'])){
				if(1==$_POST['pin']){
					$pin=1;
				}
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `event_content` (`event`,`url`,`title`,
				`cover_url`,`thumbnail_cover_url`,
				`description`,`content`,`level`,`status`,`time`,`pin`) VALUES ('".$event_id."','".$db->prepare($url)."','".$db->prepare($title)."',
				'".$db->prepare($cover_url)."','".$db->prepare($thumbnail_cover_url)."',
				'".$db->prepare($description)."','".$db->prepare($content)."','".$level."','".$status."','".$publishing_time."','".$pin."')");

				$content_id=$db->last_id();

				//add files to database
				foreach($files as $file){
					$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."',`event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$content_id."',`address`='".$allow_event_manage_address_id."'");
					$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
				}

				$db->sql("DELETE FROM `event_content_tags` WHERE `event`='".$event_id."' AND `content`='".$db->prepare($content_id)."'");
				if(isset($_POST['tag'])){
					foreach($_POST['tag'] as $tag_id=>$tag_status){
						if('on'==$tag_status){
							$db->sql("INSERT INTO `event_content_tags` (`event`,`content`,`tag`) VALUES ('".$event_id."','".$db->prepare($content_id)."','".$db->prepare($tag_id)."')");
						}
					}
				}

				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Content <a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/edit/'.$content_id.'/">"'.htmlspecialchars($url).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/content/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

				//deleting temporarily uploaded files
				foreach($files as $file){
					unlink($file['path']);
				}
			}
		}
		else{
			$tags_count=$db->table_count('event_tags','WHERE `event`='.$event_id);
			if(0==$tags_count){
				//write notice and link to /partners_cat/ create category
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Go to <a href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/create/">tags</a> and create at least one tag.
				</div>';
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event create content
					'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/'.$event_url.'/content/<b>URL</b>/ page address bar.','required'=>true],
					'title'=>['type'=>'text','title'=>'Title','required'=>true],
					'description'=>['type'=>'textarea','title'=>'Description','placeholder'=>'Description (annotation)'],
					'cover'=>[
						'type'=>'image',
						'title'=>'🖼️ Cover image',
						'placeholder'=>'Cover',
						'object_type'=>'content',
						'file_path'=>'cover_url',
						'thumbnail_path'=>'thumbnail_cover_url',
						'descr'=>'The thumbnail will be created automatically with a target 250x250 pixels (1:1 ratio), 2Mb'
					],
					'content'=>['type'=>'textarea','title'=>'📄 Content for participiants','placeholder'=>'HTML content','wysiwyg'=>true],
					'level'=>[
						'type'=>'select',
						'title'=>'Participant level',
						'options'=>$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `level` ASC"),
						'key'=>'level',
						'caption_template'=>'{name} {caption}',
						'caption_function'=>function($option_id,$option_value){
							return $option_value['name'].($option_value['caption']?' ('.$option_value['caption'].')':'');
						},
					],
					'sep1'=>['type'=>'hr'],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$content_status_arr,
						'default_option'=>0,
						'classes'=>$content_status_style_arr,
					],
					'datetime'=>[
						'type'=>'datetime',
						'local'=>true,//-local
						'future'=>true,
						'title'=>'Publishing time (by your local time)',
						'timestamp'=>'unixtime',
						'value'=>0
					],
					'pin'=>['type'=>'checkbox','title'=>'Pinned content','value'=>1],
					'tag'=>[
						'type'=>'multi-checkbox',
						'title'=>'Tags',
						'options'=>$db->sql('SELECT * FROM `event_tags` WHERE `event`='.$event_id.' ORDER BY `sort` ASC, `url` ASC'),
						'key'=>'id',
						'value'=>'caption',
						'value_function'=>false,
					],
				];
				print build_form($object_scheme,false);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Create content" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
	}
	elseif('edit'==$path_array[5]){
		unset($content_status_arr[3]);//remove banned status for event manager
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/content/">&larr; Back to content list</a>';
		print '<h2>Edit content</h2>';
		$content_id=(int)$path_array[6];
		$content_item=$db->sql_row("SELECT * FROM `event_content` WHERE `event`='".$event_id."' AND `id`='".$content_id."'");
		if(3==$content_item['status']){
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content is banned</p>
			</div>';
			$content_item=null;
		}
		else
		if(null!=$content_item){
			if(isset($_POST['url'])){
				$errors=[];
				$files=[];
				$remove_files=[];
				$cover_url=$content_item['cover_url'];
				$thumbnail_cover_url=$content_item['thumbnail_cover_url'];

				//https://www.php.net/manual/ru/features.file-upload.errors.php
				$upload_max_size=ini_get('upload_max_filesize');
				foreach($_FILES as $file_key=>$file_data){
					switch($file_data['error']){
						case UPLOAD_ERR_INI_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
							break;
					}
				}

				$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/content/';
				if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
					//check extensions
					$allowed_exts=['jpg','jpeg','png'];
					$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
					if(in_array($ext,$allowed_exts)){
						if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
							$cover=md5($_FILES['cover']['name']).'_'.time().'.'.$ext;
							if(!file_exists($root_dir.$dir)){
								mkdir($root_dir.$dir,0777,true);
							}
							$filename=$dir.$cover;
							move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
							$cover_url=$filename;
							//make small thumbnail 250x250 from cover
							$thumbnail_cover=md5($_FILES['cover']['name']).'_'.time().'_thumbnail.'.$ext;
							$thumbnail_filename=$dir.$thumbnail_cover;
							$thumbnail_cover_url=$thumbnail_filename;
							$thumbnail_cover_path=$root_dir.$thumbnail_filename;
							$thumbnail_cover_obj=new Imagick($root_dir.$filename);
							$thumbnail_cover_obj->thumbnailImage(250,250,true);
							$thumbnail_cover_obj->writeImage($thumbnail_cover_path);

							$remove_files[]=$content_item['cover_url'];
							$remove_files[]=$content_item['thumbnail_cover_url'];
							$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1];//target=content
							$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>filesize($root_dir.$thumbnail_filename),'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$thumbnail_filename,'relative_path'=>$thumbnail_filename,'target'=>1];//target=content
						}
						else{
							$errors[]='&mdash; Cover size must be less than 2MB';
						}
					}
					else{
						$errors[]='&mdash; Cover must be in JPG, JPEG or PNG format';
					}
				}

				$url=trim($_POST['url']);
				$url=mb_strtolower($url);
				$url=preg_replace('/\.+/','.',$url);
				if(in_array($url,$reserved_urls)){
					$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
				}
				if(''==$url){
					$errors[]='&mdash; URL is required';
				}
				//check if url is unique
				$check_url=$db->sql_row("SELECT * FROM `event_content` WHERE `event`='".$event_id."' AND `url`='".$url."' AND `id`!='".$content_id."'");
				if(null!=$check_url){
					$errors[]='&mdash; URL is already in use';
				}

				$title=trim($_POST['title']);
				if(''==$title){
					$errors[]='&mdash; Title is required';
				}
				$description=trim($_POST['description']);

				$content=trim($_POST['content']);
				$level=intval($_POST['level']);
				if($level<0){
					$errors[]='Negative level';
				}
				else{
					$level_exist=$db->table_count('event_levels',"WHERE `level`='".$level."' AND `event`='".$event_id."'");
					if(0==$level_exist){
						$errors[]='Level '.$level.' not exist';
					}
				}

				$status=0;
				if($content_status_arr[$_POST['status']]){
					$status=(int)$_POST['status'];
				}
				$publishing_time=(int)$_POST['unixtime'];
				if(0==$status){//if status is planned
					if($publishing_time<time()){//if time is in past
						$publishing_time=0;
						//$errors[]='&mdash; Publishing time must be in future for planned content';
					}
				}
				if(1==$status){//if status is published
					if($publishing_time<time()){//if time is in past
						$publishing_time=time();//set time to now
					}
				}
				$pin=0;
				if(isset($_POST['pin'])){
					if(1==$_POST['pin']){
						$pin=1;
					}
				}

				if(0==count($errors)){
					$db->sql("UPDATE `event_content` SET `url`='".$db->prepare($url)."', `title`='".$db->prepare($title)."', `description`='".$db->prepare($description)."', `content`='".$db->prepare($content)."', `cover_url`='".$db->prepare($cover_url)."', `thumbnail_cover_url`='".$db->prepare($thumbnail_cover_url)."', `level`='".$level."', `status`='".$status."', `time`='".$publishing_time."', `pin`='".$pin."' WHERE `id`='".$content_id."'");

					//deleting old files
					foreach($remove_files as $file){
						$free_file_size=filesize($root_dir.$file);
						unlink($root_dir.$file);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					//add files to database
					foreach($files as $file){
						$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."',`event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$content_id."',`address`='".$allow_event_manage_address_id."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					}

					$db->sql("DELETE FROM `event_content_tags` WHERE `content`='".$content_id."'");
					if(isset($_POST['tag'])){
						foreach($_POST['tag'] as $tag_id=>$tag_status){
							if('on'==$tag_status){
								$db->sql("INSERT INTO `event_content_tags` (`event`,`content`,`tag`) VALUES ('".$event_id."','".$db->prepare($content_item['id'])."','".$db->prepare($tag_id)."')");
							}
						}
					}

					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Content <a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/edit/'.$content_item['id'].'/">"'.htmlspecialchars($url).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/content/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

					//deleting temporarily uploaded files
					foreach($files as $file){
						unlink($file['path']);
					}
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event create content
					'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/'.$event_url.'/content/<b>URL</b>/ page address bar.','required'=>true],
					'title'=>['type'=>'text','title'=>'Title','required'=>true],
					'description'=>['type'=>'textarea','title'=>'Description','placeholder'=>'Description (annotation)'],
					'cover'=>[
						'type'=>'image',
						'title'=>'🖼️ Cover image',
						'placeholder'=>'Cover',
						'object_type'=>'content',
						'file_path'=>'cover_url',
						'thumbnail_path'=>'thumbnail_cover_url',
						'descr'=>'The thumbnail will be created automatically with a target 250x250 pixels (1:1 ratio), 2Mb'
					],
					'content'=>['type'=>'textarea','title'=>'📄 Content for participiants','placeholder'=>'HTML content','wysiwyg'=>true],
					'level'=>[
						'type'=>'select',
						'title'=>'Participant level',
						'options'=>$db->sql("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' ORDER BY `level` ASC"),
						'key'=>'level',
						'caption_template'=>'{name} {caption}',
						'caption_function'=>function($option_id,$option_value){
							return $option_value['name'].($option_value['caption']?' ('.$option_value['caption'].')':'');
						},
					],
					'sep1'=>['type'=>'hr'],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$content_status_arr,
						'default_option'=>0,
						'classes'=>$content_status_style_arr,
					],
					'datetime'=>[
						'type'=>'datetime',
						'key'=>'time',
						'local'=>true,//-local
						'future'=>true,
						'title'=>'Publishing time (by your local time)',
						'timestamp'=>'unixtime',
						'value_title'=>'Publishing time',
						'timestamp'=>'unixtime',
						'value'=>0
					],
					'pin'=>['type'=>'checkbox','title'=>'Pinned content','value'=>1],
					'tag'=>[
						'type'=>'multi-checkbox',
						'title'=>'Tags',
						'options'=>$db->sql('SELECT * FROM `event_tags` WHERE `event`='.$event_id.' ORDER BY `sort` ASC, `url` ASC'),
						'key'=>'id',
						'value'=>'caption',
						'value_function'=>false,
						'checked_function'=>function($option,$object){
							global $db,$event_id;
							return (0<$db->table_count('event_content_tags','WHERE `event`='.$event_id.' AND `tag`='.$option['id'].' AND `content`='.$object['id']));
						},
					],
				];
				print build_form($object_scheme,$content_item);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Update content" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content not found</p>
			</div>';
		}
	}
	else{
		print '<a class="action-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/content/create/">Create content</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		$filter_tag=false;
		if(isset($_GET['tag'])){
			if(''!==$_GET['tag']){
				$filter_tag=(int)$_GET['tag'];
				if(0==$db->table_count('event_tags',"WHERE `event`='".$event_id."' AND `id`='".$filter_tag."'")){
					$filter_tag=false;
				}
			}
		}

		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($content_status_arr[$filter_status])){
					$filter_status=false;
				}
			}
		}

		$filter_query=false;
		if(isset($_GET['query'])){
			if(''!==$_GET['query']){
				$filter_query=trim($_GET['query']);
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="query" class="form-post-by-enter" placeholder="Search query" value="'.($filter_query?htmlspecialchars($filter_query):'').'">
			</div>
			<div>
				<select name="tag" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_tag?' selected':'').'>Filter by tag</option>';
					$tags=$db->sql('SELECT * FROM `event_tags` WHERE `event`='.$event_id.' ORDER BY `sort` ASC, `url` ASC');
					foreach($tags as $tag){
						print '<option value="'.htmlspecialchars($tag['id']).'"'.($tag['id']==$filter_tag?' selected':'').'>Tag: '.htmlspecialchars($tag['caption']).'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
					foreach($content_status_arr as $content_status_id=>$content_status_caption){
						print '<option value="'.htmlspecialchars($content_status_id).'" class="'.$content_status_style_arr[$content_status_id].'"'.($content_status_id===$filter_status?' selected':'').'>Status: '.htmlspecialchars($content_status_caption).'</option>';
					}
					print '
				</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>URL</th>';
			print '<th>Cover</th>';
			print '<th>Title</th>';
			print '<th>Description</th>';
			print '<th>Content</th>';
			print '<th>Level</th>';
			print '<th>Status</th>';
			print '<th>Time</th>';
			print '<th>Pinned</th>';
			print '<th>Tags</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]='`event_content`.`event`='.$event_id;
		if(false!==$filter_status){
			$sql_addon[]='`event_content`.`status`='.$filter_status;
		}
		if(false!==$filter_query){
			$sql_addon[]='(`event_content`.`url` LIKE \'%'.$filter_query.'%\' OR `event_content`.`title` LIKE \'%'.$filter_query.'%\' OR `event_content`.`description` LIKE \'%'.$filter_query.'%\')';
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		if(false!==$filter_tag){
			$sql_addon_str=' RIGHT JOIN `event_content_tags` ON `event_content_tags`.`content`=`event_content`.`id` AND `event_content_tags`.`tag`='.$filter_tag.' '.$sql_addon_str;
		}
		$per_page=50;
		$count=$db->table_count('event_content',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$content=$db->sql("SELECT `event_content`.* FROM `event_content`".$sql_addon_str." ORDER BY `event_content`.`id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($content as $content_item){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/delete/'.$content_item['id'].'/" class="red-btn">Delete</a></td>';
			print '<td class="whitespace-nowrap">
				<a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/edit/'.$content_item['id'].'/">'.htmlspecialchars($content_item['url']).'</a>
				<a href="/@'.$organizer_url.'/'.$event_url.'/content/'.htmlspecialchars($content_item['url']).'" target="_blank" class="text-blue-500 !no-underline">⧉</a>
			</td>';

			if($content_item['thumbnail_cover_url']){
				print '<td><img src="'.$content_item['thumbnail_cover_url'].'" class="cover"></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>'.htmlspecialchars($content_item['title']).'</td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($content_item['description']).'</div></td>';

			print '<td>'.((''!=$content_item['content'])||(''!=$content_item['embed_content'])?'✔️':'❌').'</td>';
			print '<td>'.$content_item['level'].'</td>';

			print '<td>';
			print '<span class="'.$content_status_style_arr[$content_item['status']].'">';
			print htmlspecialchars($content_status_arr[$content_item['status']]);
			print '</span>';
			print '</td>';
			if($content_item['time']){
				print '<td><span class="time" data-time="'.$content_item['time'].'">'.date('d.m.Y H:i:s',$content_item['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>'.(1==$content_item['pin']?'✔️':'&mdash;').'</td>';//not use ❌ as more negative than &mdash;
			print '<td>';
			$content_tags=$db->sql("SELECT `tag` FROM `event_content_tags` WHERE `event`='".$event_id."' AND `content`=".$content_item['id']);
			foreach($content_tags as $content_tag){
				$tag_arr=$db->sql_row("SELECT * FROM `event_tags` WHERE `event`='".$event_id."' AND `id`=".$content_tag['tag']);
				print ' <a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/?tag='.$tag_arr['id'].'">'.htmlspecialchars($tag_arr['caption']).'</a>';
			}
			print '</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('upload_content'==$path_array[4]){
	ob_end_clean();
	ob_end_clean();
	ob_end_clean();
	$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/content/';
	if(!file_exists($root_dir.$dir)){
		mkdir($root_dir.$dir,0777,true);
	}
	$errors=[];
	$files=[];
	$upload_name='upload';
	if(isset($_FILES[$upload_name]) && 0==$_FILES[$upload_name]['error']){
		//check extensions
		$allowed_exts=['jpg','jpeg','png'];
		$ext=pathinfo($_FILES[$upload_name]['name'],PATHINFO_EXTENSION);
		if(in_array($ext,$allowed_exts)){
			if($_FILES[$upload_name]['size']<=1024*1024*2){//check size 2MB
				$upload=md5($_FILES[$upload_name]['name']).'_'.time().'.'.$ext;
				$filename=$dir.$upload;
				move_uploaded_file($_FILES[$upload_name]['tmp_name'],$root_dir.$filename);
				http_response_code(200);
				print json_encode(['url'=>$filename]);
				$files[]=['time'=>time(),'name'=>$_FILES[$upload_name]['name'],'size'=>$_FILES[$upload_name]['size'],'type'=>$_FILES[$upload_name]['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1,'target_id'=>0];//target=content
				foreach($files as $file){
					$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."',`event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$file['target_id']."',`address`='".$allow_event_manage_address_id."'");
					$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
				}
			}
			else{
				$errors[]='Image size must be less than 2MB';
			}
		}
		else{
			$errors[]='Image must be in JPG, JPEG or PNG format';
		}
	}
	else{
		if($limit_upload){
			$errors[]='Image not uploaded, limit reached, please subscribe to paid plan';
		}
		else{
			$errors[]='Image not uploaded';
		}
	}
	if(count($errors)){
		http_response_code(400);
		print json_encode(['error'=>['message'=>implode("\n",$errors)]]);
	}
	exit;
}
elseif('tags'==$path_array[4]){
	$replace['title']='Tags | '.$replace['title'];
	if('delete'==$path_array[5]){
		$tag_id=intval($path_array[6]);
		$tag_arr=$db->sql_row("SELECT * FROM `event_tags` WHERE `event`='".$event_id."' AND `id`='".$tag_id."'");
		if(null!=$tag_arr){
			if(isset($_POST['approve'])){
				$db->sql("DELETE FROM `event_content_tags`WHERE `event`='".$event_id."' AND `tag`='".$tag_id."'");
				$db->sql("DELETE FROM `event_tags` WHERE `id`='".$tag_id."'");
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/tags/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/">&larr; Back to tags</a>';
				print '<h2 class="text-red-600">Remove tag #'.$tag_arr['id'].'</h2>';
				$affected_content_count=$db->table_count('event_content_tags',"WHERE `event`='".$event_id."' AND `tag`='".$tag_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Tag "<a href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/edit/'.$tag_arr['id'].'/">'.htmlspecialchars($tag_arr['url']).'</a>" will be removed from the database.</p>';
				if($affected_content_count){
					print '<p>Tag will also be removed for '.$affected_content_count.' linked content.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove tag" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/">&larr; Back to tags</a>';
			print '<h2 class="text-red-600">Tag not found</h2>';
			print '<p>Return to event tags and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/">&larr; Back to tags</a>';
		print '<h2>Create tag</h2>';
		if(isset($_POST['url'])){
			$errors=[];

			$url=trim($_POST['url']);
			$url=mb_strtolower($url);
			$url=preg_replace('/\.+/','.',$url);
			if(!$url){
				$errors[]='URL is required';
			}
			$tag_exist=$db->table_count('event_tags',"WHERE `url`='".$db->prepare($url)."' AND `event`='".$event_id."'");
			if($tag_exist){
				$errors[]='Tag url '.htmlspecialchars($url).' already exists';
			}

			$caption=trim($_POST['caption']);
			if(!$caption){
				$errors[]='Caption is required';
			}
			$tag_exist=$db->table_count('event_tags',"WHERE `caption`='".$db->prepare($caption)."' AND `event`='".$event_id."'");
			if($tag_exist){
				$errors[]='Tag caption '.htmlspecialchars($caption).' already exists';
			}

			$sort=intval($_POST['sort']);

			if(0==count($errors)){
				$db->sql("INSERT INTO `event_tags` (`event`,`url`,`caption`,`sort`) VALUES ('".$event_id."','".$db->prepare($url)."','".$db->prepare($caption)."','".$sort."')");
				$tag_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Tag <a href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/edit/'.$tag_id.'/">"'.htmlspecialchars($url).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/tags/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';
			$object_scheme=[//event content tag
				'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/'.$event_url.'/content/?tag=<b>URL</b> filter page address bar.','required'=>true],
				'caption'=>['type'=>'text','title'=>'Caption','required'=>true],
				'sort'=>['type'=>'number','title'=>'Sort','placeholder'=>'Sort (number)','required'=>true],
			];
			print build_form($object_scheme,false);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create tag" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/">&larr; Back to tags</a>';
		print '<h2>Edit tag</h2>';
		$tag_id=(int)$path_array[6];
		$tag_arr=$db->sql_row("SELECT * FROM `event_tags` WHERE `event`='".$event_id."' AND `id`='".$tag_id."'");
		if(null!=$tag_arr){
			if(isset($_POST['url'])){
				$errors=[];

				$url=trim($_POST['url']);
				$url=mb_strtolower($url);
				$url=preg_replace('/\.+/','.',$url);
				if(!$url){
					$errors[]='URL is required';
				}
				$tag_exist=$db->table_count('event_tags',"WHERE `url`='".$db->prepare($url)."' AND `event`='".$event_id."' AND `id`!='".$tag_id."'");
				if($tag_exist){
					$errors[]='Tag url '.htmlspecialchars($url).' already exists';
				}

				$caption=trim($_POST['caption']);
				if(!$caption){
					$errors[]='Caption is required';
				}
				$tag_exist=$db->table_count('event_tags',"WHERE `caption`='".$db->prepare($caption)."' AND `event`='".$event_id."' AND `id`!='".$tag_id."'");
				if($tag_exist){
					$errors[]='Tag caption '.htmlspecialchars($caption).' already exists';
				}

				$sort=intval($_POST['sort']);

				if(0==count($errors)){
					$db->sql("UPDATE `event_tags` SET
						`url`='".$db->prepare($url)."',
						`caption`='".$db->prepare($caption)."',
						`sort`='".$sort."'
						WHERE `id`='".$tag_id."'");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Tag <a href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/edit/'.$tag_id.'/">"'.htmlspecialchars($url).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/tags/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event content tag
					'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/'.$event_url.'/content/?tag=<b>URL</b> filter page address bar.','required'=>true],
					'caption'=>['type'=>'text','title'=>'Caption','required'=>true],
					'sort'=>['type'=>'number','title'=>'Sort','placeholder'=>'Sort (number)','required'=>true],
				];
				print build_form($object_scheme,$tag_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Tag not found</h2>';
			print '<p>Return to event tags and try again.</p>';
		}
	}
	else{
		print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/create/" class="action-btn">Create tag</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Action</th>';
			print '<th>Sort</th>';
			print '<th>URL</th>';
			print '<th>Caption</th>';
			print '<th>Content count</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$tags=$db->sql("SELECT * FROM `event_tags` WHERE `event`='".$event_id."' ORDER BY `sort` ASC");
		foreach($tags as $tag){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/delete/'.$tag['id'].'/" class="red-btn">Delete</a></td>';
			print '<td>'.$tag['sort'].'</td>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/tags/edit/'.$tag['id'].'/">'.htmlspecialchars($tag['url']).'</a></td>';
			print '<td>'.htmlspecialchars($tag['caption']).'</td>';
			$content_count=$db->table_count('event_content_tags',"WHERE `event`='".$event_id."' AND `tag`='".$tag['id']."'");
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/?tag='.$tag['id'].'">'.$content_count.'</a></td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}
}
elseif('partners_cat'==$path_array[4]){
	$replace['title']='Partners categories | '.$replace['title'];
	if('delete'==$path_array[5]){
		$cat_id=intval($path_array[6]);
		$cat_arr=$db->sql_row("SELECT * FROM `event_partners_cat` WHERE `event`='".$event_id."' AND `id`='".$cat_id."'");
		if(null!=$cat_arr){
			if(isset($_POST['approve'])){
				$db->sql("UPDATE `event_partners` SET `cat`=0 WHERE `event`='".$event_id."' AND `cat`='".$cat_id."'");
				$db->sql("DELETE FROM `event_partners_cat` WHERE `id`='".$cat_id."'");
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/">&larr; Back to categories</a>';
				print '<h2 class="text-red-600">Remove partners category #'.$cat_arr['id'].'</h2>';
				$affected_partners_count=$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `cat`='".$cat_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Category "<a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/edit/'.$cat_arr['id'].'/">'.htmlspecialchars($cat_arr['caption']).'</a>" will be removed from the database.</p>';
				if($affected_partners_count){
					print '<p>Category will also be removed for '.$affected_partners_count.' partners.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove category" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/">&larr; Back to categories</a>';
			print '<h2 class="text-red-600">Category not found</h2>';
			print '<p>Return to partners categories and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/">&larr; Back to categories</a>';
		print '<h2>Create category</h2>';
		if(isset($_POST['caption'])){
			$errors=[];
			$caption=trim($_POST['caption']);
			if(!$caption){
				$errors[]='Caption is required';
			}
			$cat_exist=$db->table_count('event_partners_cat',"WHERE `caption`='".$db->prepare($caption)."' AND `event`='".$event_id."'");
			if($cat_exist){
				$errors[]='Tag caption '.htmlspecialchars($caption).' already exists';
			}

			$sort=intval($_POST['sort']);
			if(''==$_POST['sort']){//not set
				$last_sort=$db->select_one('event_partners_cat','sort',"WHERE `event`='".$event_id."' ORDER BY `sort` DESC");
				$sort=$last_sort+1;
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `event_partners_cat` (`event`,`caption`,`sort`) VALUES ('".$event_id."','".$db->prepare($caption)."','".$sort."')");
				$cat_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Category <a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/edit/'.$cat_id.'/">"'.htmlspecialchars($caption).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';
			$object_scheme=[//event partners category
				'caption'=>[
					'type'=>'text',
					'title'=>'Caption',
					'required'=>true,
				],
				'sort'=>[
					'type'=>'number',
					'title'=>'Sort',
					'min'=>0,
					'max'=>100,
				],

			];
			print build_form($object_scheme);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create category" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/">&larr; Back to categories</a>';
		print '<h2>Edit category</h2>';
		$cat_id=(int)$path_array[6];
		$cat_arr=$db->sql_row("SELECT * FROM `event_partners_cat` WHERE `event`='".$event_id."' AND `id`='".$cat_id."'");
		if(null!=$cat_arr){
			if(isset($_POST['caption'])){
				$errors=[];
				$caption=trim($_POST['caption']);
				if(!$caption){
					$errors[]='Caption is required';
				}
				$category_exist=$db->table_count('event_partners_cat',"WHERE `caption`='".$db->prepare($caption)."' AND `event`='".$event_id."' AND `id`!='".$cat_id."'");
				if($category_exist){
					$errors[]='Category caption '.htmlspecialchars($caption).' already exists';
				}

				$sort=intval($_POST['sort']);
				if(''==$_POST['sort']){//not set
					$last_sort=$db->select_one('event_partners_cat','sort',"WHERE `event`='".$event_id."' ORDER BY `sort` DESC");
					$sort=$last_sort+1;
				}

				if(0==count($errors)){
					$db->sql("UPDATE `event_partners_cat` SET
						`caption`='".$db->prepare($caption)."',
						`sort`='".$sort."'
						WHERE `id`='".$cat_id."'");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Category <a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/edit/'.$cat_id.'/">"'.htmlspecialchars($caption).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event partners category
					'caption'=>[
						'type'=>'text',
						'title'=>'Caption',
						'required'=>true,
					],
					'sort'=>[
						'type'=>'number',
						'title'=>'Sort',
						'min'=>0,
						'max'=>100,
					],

				];
				print build_form($object_scheme,$cat_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Category not found</h2>';
			print '<p>Return to partners categories and try again.</p>';
		}
	}
	else{
		print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/create/" class="action-btn">Create category</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Action</th>';
			print '<th>Sort</th>';
			print '<th>Caption</th>';
			print '<th>Partners count</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$cats=$db->sql("SELECT * FROM `event_partners_cat` WHERE `event`='".$event_id."' ORDER BY `sort` ASC");
		foreach($cats as $cat){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/delete/'.$cat['id'].'/" class="red-btn">Delete</a></td>';
			print '<td>'.$cat['sort'].'</td>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/edit/'.$cat['id'].'/">'.htmlspecialchars($cat['caption']).'</a></td>';
			$partners_count=$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `cat`='".$cat['id']."'");
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/?cat='.$cat['id'].'">'.$partners_count.'</a></td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}
}
elseif('partners'==$path_array[4]){
	$replace['title']='Partners | '.$replace['title'];
	if('delete'==$path_array[5]){
		$partner_arr=$db->sql_row("SELECT * FROM `event_partners` WHERE `event`='".$event_id."' AND `id`='".(int)$path_array[6]."'");
		if(null!=$partner_arr){
			if(isset($_POST['approve'])){
				if(null!=$partner_arr){
					if(''!=$partner_arr['logo_url']){
						if(file_exists($root_dir.$partner_arr['logo_url'])){
							$free_file_size=filesize($root_dir.$partner_arr['logo_url']);
							unlink($root_dir.$partner_arr['logo_url']);
							$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$partner_arr['logo_url'])."'");
							$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
					}
					if(''!=$partner_arr['small_logo_url']){
						if(file_exists($root_dir.$partner_arr['small_logo_url'])){
							$free_file_size=filesize($root_dir.$partner_arr['small_logo_url']);
							unlink($root_dir.$partner_arr['small_logo_url']);
							$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$partner_arr['small_logo_url'])."'");
							$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
							$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
						}
					}
					$db->sql("DELETE FROM `event_partners` WHERE `id`='".$partner_arr['id']."'");
				}
				header('location:/@'.$organizer_url.'/'.$event_url.'/manage/partners/');
				exit;
			}
			else{
					print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/">&larr; Back to partners</a>';
					print '<h2 class="text-red-600">Remove partner #'.$partner_arr['id'].'</h2>';
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Warning</p>
						<p>Partner will be removed from the database.</p>
					</div>';
					print '<form action="" method="POST" class="manage-card">';
					print '<input type="submit" name="approve" value="Remove partner" class="red-btn">';
					print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/">&larr; Back to partners</a>';
			print '<h2 class="text-red-600">Partner not found</h2>';
			print '<p>Return to partners list and try again.</p>';
		}
	}
	elseif('create'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/">&larr; Back to partners</a>';
		print '<h2>Create partner</h2>';
		if(isset($_POST['name'])){
			$errors=[];
			$files=[];
			$cat=(int)$_POST['cat'];
			if(0==$db->table_count('event_partners_cat',"WHERE `event`='".$event_id."' AND `id`='".$cat."'")){
				$errors[]='Category not found';
			}
			$name=trim($_POST['name']);
			if(0==$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `name`='".$db->prepare($name)."'")){
				//https://www.php.net/manual/ru/features.file-upload.errors.php
				$upload_max_size=ini_get('upload_max_filesize');
				foreach($_FILES as $file_key=>$file_data){
					switch($file_data['error']){
						case UPLOAD_ERR_INI_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
							break;
					}
				}

				$logo_url='';
				$small_logo_url='';

				$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/partners/';
				if(isset($_FILES['logo']) && 0==$_FILES['logo']['error']){
					//check extensions
					$allowed_exts=['jpg','jpeg','png'];
					$ext=pathinfo($_FILES['logo']['name'],PATHINFO_EXTENSION);
					if(in_array($ext,$allowed_exts)){
						if($_FILES['logo']['size']<=1024*1024*2){//check size 2MB
							$logo=md5($name).'_'.time().'.'.$ext;
							if(!file_exists($root_dir.$dir)){
								mkdir($root_dir.$dir,0777,true);
							}
							$filename=$dir.$logo;
							move_uploaded_file($_FILES['logo']['tmp_name'],$root_dir.$filename);
							$logo_url=$filename;
							//make small thumbnail 250x250 from logo
							$small_logo=md5($name).'_'.time().'_small.'.$ext;
							$small_filename=$dir.$small_logo;
							$small_logo_url=$small_filename;
							$small_logo_path=$root_dir.$small_filename;
							$small_logo_obj=new Imagick($root_dir.$filename);
							$small_logo_obj->thumbnailImage(250,250,true);
							$small_logo_obj->writeImage($small_logo_path);

							$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>$_FILES['logo']['size'],'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>5];//target=partner
							$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>filesize($root_dir.$small_filename),'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$small_filename,'relative_path'=>$small_filename,'target'=>5];//target=partner
						}
						else{
							$errors[]='&mdash; Logo size must be less than 2MB';
						}
					}
					else{
						$errors[]='&mdash; Logo must be in JPG, JPEG or PNG format';
					}
				}

				$caption=trim($_POST['caption']);
				$link=trim($_POST['link']);
				$url=trim($_POST['url']);
				$url=mb_strtolower($url);
				$url=preg_replace('/\.+/','.',$url);
				$description=trim($_POST['description']);
				$status=(int)$_POST['status'];
				$sort=(int)$_POST['sort'];

				if(0==count($errors)){
					$db->sql("INSERT INTO `event_partners` (`event`,`cat`,`name`,`logo_url`,`small_logo_url`,`caption`,`link`,`url`,`description`,`status`,`sort`) VALUES ('".$event_id."','".$db->prepare($cat)."','".$db->prepare($name)."','".$db->prepare($logo_url)."','".$db->prepare($small_logo_url)."','".$db->prepare($caption)."','".$db->prepare($link)."','".$db->prepare($url)."','".$db->prepare($description)."','".$db->prepare($status)."','".$db->prepare($sort)."')");

					$partner_id=$db->last_id();

					//add files to database
					foreach($files as $file){
						$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."',`event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$partner_id."',`address`='".$allow_event_manage_address_id."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					}

					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Partner <a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/edit/'.$partner_id.'/">"'.htmlspecialchars($name).'"</a> was created</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/partners/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Partner with the same name already exist</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			$partners_cat_count=$db->table_count('event_partners_cat','WHERE `event`='.$event_id);
			if(0==$partners_cat_count){
				//write notice and link to /partners_cat/ create category
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Go to <a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/create/">partners categories</a> and create at least one category.</p>
				</div>';
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
				print '<div class="max-w-fit">';
				$object_scheme=[//event partners
					'cat'=>['type'=>'select','title'=>'Category','required'=>true,'options'=>$db->sql("SELECT * FROM `event_partners_cat` WHERE `event`='".$event_id."' ORDER BY `sort` ASC"),'key'=>'id','caption_value'=>'caption'],
					'name'=>['type'=>'text','title'=>'Name','required'=>true],
					'logo'=>[
						'type'=>'image',
						'title'=>'🖼️ Logo image',
						'placeholder'=>'Logo',
						'object_type'=>'partners',
						'file_path'=>'logo_url',
						'thumbnail_path'=>'small_logo_url',
						'descr'=>'All partners logos should be in the same size and ratio<br>For example: 500x500px (1:1 ratio), 320x240 (4:3 ratio), max 2Mb',
					],
					'caption'=>['type'=>'text','title'=>'Caption','descr'=>'Under logo image'],
					'link'=>['type'=>'text','title'=>'Link','placeholder'=>'Link (optional)','descr'=>'Optional, external link to partner site (<b>https://</b>)'],
					'hr1'=>['type'=>'hr'],
					'url'=>['type'=>'text','title'=>'URL','placeholder'=>'URL','class'=>'url-part','descr'=>'Optional, used for /@'.$organizer_url.'/'.$event_url.'/partners/<b>URL</b>/ internal partner page address bar.'],
					'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'About partner','wysiwyg'=>true,'descr'=>'Optional for internal partner page.',],
					'hr2'=>['type'=>'hr'],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$partners_status_arr,
						'default_option'=>0,
						'classes'=>$partners_status_style_arr,
					],
					'sort'=>['type'=>'number','title'=>'Sort priority','min'=>0,'max'=>10],
				];
				print build_form($object_scheme);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Create partner" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
	}
	elseif('edit'==$path_array[5]){
		print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/">&larr; Back to partners</a>';
		print '<h2>Edit partner</h2>';
		$partner_arr=$db->sql_row("SELECT * FROM `event_partners` WHERE `event`='".$event_id."' AND `id`='".(int)$path_array[6]."'");
		if(null!=$partner_arr){
			if(isset($_POST['name'])){
				$errors=[];
				$files=[];
				$remove_files=[];
				$name=trim($_POST['name']);

				$cat=(int)$_POST['cat'];
				if(0==$db->table_count('event_partners_cat',"WHERE `event`='".$event_id."' AND `id`='".$cat."'")){
					$errors[]='Category not found';
				}

				//https://www.php.net/manual/ru/features.file-upload.errors.php
				$upload_max_size=ini_get('upload_max_filesize');
				foreach($_FILES as $file_key=>$file_data){
					switch($file_data['error']){
						case UPLOAD_ERR_INI_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
							break;
					}
				}

				$logo_url=$partner_arr['logo_url'];
				$small_logo_url=$partner_arr['small_logo_url'];

				$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$event_url.'/partners/';
				if(isset($_FILES['logo']) && 0==$_FILES['logo']['error']){
					//check extensions
					$allowed_exts=['jpg','jpeg','png'];
					$ext=pathinfo($_FILES['logo']['name'],PATHINFO_EXTENSION);
					if(in_array($ext,$allowed_exts)){
						if($_FILES['logo']['size']<=1024*1024*2){//check size 2MB
							$logo=md5($name).'_'.time().'.'.$ext;
							if(!file_exists($root_dir.$dir)){
								mkdir($root_dir.$dir,0777,true);
							}
							$filename=$dir.$logo;
							move_uploaded_file($_FILES['logo']['tmp_name'],$root_dir.$filename);
							$logo_url=$filename;
							//make small thumbnail 250x250 from logo
							$small_logo=md5($name).'_'.time().'_small.'.$ext;
							$small_filename=$dir.$small_logo;
							$small_logo_url=$small_filename;
							$small_logo_path=$root_dir.$small_filename;
							$small_logo_obj=new Imagick($root_dir.$filename);
							$small_logo_obj->thumbnailImage(250,250,true);
							$small_logo_obj->writeImage($small_logo_path);

							$remove_files[]=$partner_arr['logo_url'];
							$remove_files[]=$partner_arr['small_logo_url'];
							$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>$_FILES['logo']['size'],'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>5];//target=partner
							$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>filesize($root_dir.$small_filename),'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$small_filename,'relative_path'=>$small_filename,'target'=>5];//target=partner
						}
						else{
							$errors[]='&mdash; Logo size must be less than 2MB';
						}
					}
					else{
						$errors[]='&mdash; Logo must be in JPG, JPEG or PNG format';
					}
					$db->sql("UPDATE `event_partners` SET `logo_url`='".$db->prepare($logo_url)."',`small_logo_url`='".$db->prepare($small_logo_url)."' WHERE `id`='".$partner_arr['id']."'");
					//deleting old files
					foreach($remove_files as $file){
						$free_file_size=filesize($root_dir.$file);
						unlink($root_dir.$file);
						$db->sql("DELETE FROM `event_files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
					}
					//add files to database
					foreach($files as $file){
						$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."',`event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$partner_arr['id']."',`address`='".$allow_event_manage_address_id."'");
						$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
						$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					}
				}

				$caption=trim($_POST['caption']);
				$link=trim($_POST['link']);
				$url=trim($_POST['url']);
				$url=mb_strtolower($url);
				$url=preg_replace('/\.+/','.',$url);
				$description=trim($_POST['description']);
				$status=(int)$_POST['status'];
				$sort=(int)$_POST['sort'];

				if(0==count($errors)){
					$db->sql("UPDATE `event_partners` SET
					`cat`='".$cat."',
					`name`='".$db->prepare($name)."',
					`caption`='".$db->prepare($caption)."',
					`link`='".$db->prepare($link)."',
					`url`='".$db->prepare($url)."',
					`description`='".$db->prepare($description)."',
					`status`='".$db->prepare($status)."', `sort`='".$db->prepare($sort)."'
					WHERE `id`='".$partner_arr['id']."'");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Partner <a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/edit/'.$partner_arr['id'].'/">"'.htmlspecialchars($name).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/partners/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
				print '<div class="max-w-fit">';
					if(0==$db->table_count('event_partners_cat',"WHERE `event`='".$event_id."'")){
						print '<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>You must <a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners_cat/">create at least one category</a> before adding partners</p>
						</div>';
					}
					$object_scheme=[//event edit partners
						'cat'=>['type'=>'select','title'=>'Category','required'=>true,'options'=>$db->sql("SELECT * FROM `event_partners_cat` WHERE `event`='".$event_id."' ORDER BY `sort` ASC"),'key'=>'id','caption_value'=>'caption','optional'=>true,'optional_value'=>'0','optional_caption'=>'(none)'],
						'name'=>['type'=>'text','title'=>'Name','required'=>true],
						'logo'=>[
							'type'=>'image',
							'title'=>'🖼️ Logo image',
							'placeholder'=>'Logo',
							'object_type'=>'partners',
							'file_path'=>'logo_url',
							'thumbnail_path'=>'small_logo_url',
							'descr'=>'All partners logos should be in the same size and ratio<br>For example: 500x500px (1:1 ratio), 320x240 (4:3 ratio), max 2Mb',
						],
						'caption'=>['type'=>'text','title'=>'Caption','descr'=>'Under logo image'],
						'link'=>['type'=>'text','title'=>'Link','placeholder'=>'Link (optional)','descr'=>'Optional, external link to partner site (<b>https://</b>)'],
						'hr1'=>['type'=>'hr'],
						'url'=>['type'=>'text','title'=>'URL','placeholder'=>'URL','class'=>'url-part','descr'=>'Optional, used for /@'.$organizer_url.'/'.$event_url.'/partners/<b>URL</b>/ internal partner page address bar.'],
						'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'About partner','wysiwyg'=>true,'descr'=>'Optional for internal partner page.',],
						'hr2'=>['type'=>'hr'],
						'status'=>[
							'type'=>'select',
							'title'=>'Status',
							'options'=>$partners_status_arr,
							'default_option'=>0,
							'classes'=>$partners_status_style_arr,
						],
						'sort'=>['type'=>'number','title'=>'Sort priority','min'=>0,'max'=>10],
					];
					print build_form($object_scheme,$partner_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Partner not found</h2>';
			print '<p>Return to partners list and try again.</p>';
		}
	}
	else{
		print '<a class="action-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/create/">Create partner</a>';
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		$filter_name=false;
		if(isset($_GET['name'])){
			if(''!==$_GET['name']){
				$filter_name=trim($_GET['name']);
			}
		}

		$filter_cat=false;
		if(isset($_GET['cat'])){
			if(''!==$_GET['cat']){
				$filter_cat=(int)$_GET['cat'];
				if(0==$db->table_count('event_partners_cat',"WHERE `event`='".$event_id."' AND `id`='".$filter_cat."'")){
					$filter_cat=false;
				}
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="name" class="form-post-by-enter" placeholder="Search" value="'.($filter_name?htmlspecialchars($filter_name):'').'">
			</div>
			<div>
				<select name="cat" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_cat?' selected':'').'>Filter by category</option>';
					$cats=$db->sql('SELECT * FROM `event_partners_cat` WHERE `event`='.$event_id.' ORDER BY `sort` ASC');
					foreach($cats as $cat){
						print '<option value="'.htmlspecialchars($cat['id']).'"'.($cat['id']==$filter_cat?' selected':'').'>Category: '.htmlspecialchars($cat['caption']).'</option>';
					}
					print '
				</select>
			</div>';

		print '</div>';
		print '</form>';

		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>Category</th>';
			print '<th>Name</th>';
			print '<th>Logo</th>';
			print '<th>Caption</th>';

			print '<th>Link</th>';
			print '<th>URL</th>';
			print '<th>Description</th>';
			print '<th>Status</th>';
			print '<th>Sort</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]="`event`='".$event_id."'";
		if(false!==$filter_name){
			$sql_addon[]='(`name` LIKE \'%'.$filter_name.'%\' OR `caption` LIKE \'%'.$filter_name.'%\' OR `description` LIKE \'%'.$filter_name.'%\' OR `url` LIKE \'%'.$filter_name.'%\')';
		}
		if(false!==$filter_cat){
			$sql_addon[]='`cat`='.$filter_cat;
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}

		$partners=$db->sql("SELECT * FROM `event_partners`".$sql_addon_str." ORDER BY `sort` ASC, `name` ASC");
		foreach($partners as $partner){
			print '<tr>';
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/delete/'.$partner['id'].'/" class="red-btn">Delete</a></td>';
			if($partner['cat']){
				$cat_arr=$db->sql_row('SELECT * FROM `event_partners_cat` WHERE `event`='.$event_id.' AND `id`='.$partner['cat']);
				print '<td>'.htmlspecialchars($cat_arr['caption']).'</td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td><a href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/edit/'.$partner['id'].'/">'.htmlspecialchars($partner['name']).'</a></td>';
			if($partner['logo_url']){
				print '<td><img src="'.$partner['small_logo_url'].'" class="logo-preview"></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>'.htmlspecialchars($partner['caption']).'</td>';

			print '<td class="whitespace-nowrap">'.htmlspecialchars($partner['link']);
			if($partner['link']){
				print ' <a href="'.htmlspecialchars(str_replace('javascript:','',$partner['link'])).'" target="_blank" class="text-blue-500 !no-underline">⧉</a>';
			}
			print '</td>';
			print '<td class="whitespace-nowrap">'.htmlspecialchars($partner['url']);
			if($partner['url']){
				print ' <a href="/@'.$organizer_url.'/'.$event_url.'/partners/'.htmlspecialchars($partner['url']).'/" target="_blank" class="text-blue-500 !no-underline">⧉</a>';
			}
			print '</td>';
			print '<td>'.($partner['description']?'✔️':'&mdash;').'</td>';

			print '<td>';
			print '<span class="'.$partners_status_style_arr[$partner['status']].'">';
			print htmlspecialchars($partners_status_arr[$partner['status']]);
			print '</span>';
			print '</td>';
			print '<td>'.htmlspecialchars($partner['sort']).'</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}
}
elseif('files'==$path_array[4]){
	error_reporting(255);
	$replace['title']='Files | '.$replace['title'];
	if('upload'==$path_array[5]){
		$upload_max_size=ini_get('upload_max_filesize');
		$replace['title']='Upload | '.$replace['title'];
		if(isset($_POST['custom_upload'])){
			$dir='/files/organizers/'.$organizer_id.'/events/'.$event_url.'/custom/';
			if(!file_exists($root_dir.$dir)){
				mkdir($root_dir.$dir,0777,true);
			}
			$errors=[];
			$files=[];
			$upload_name='upload';

			if(isset($_FILES[$upload_name]) && 0==$_FILES[$upload_name]['error']){
				//check extensions
				$ext=pathinfo($_FILES[$upload_name]['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$custom_upload_allowed_extensions)){
					if($_FILES[$upload_name]['size']<=1024*1024*(intval($upload_max_size))){//check size $upload_max_size
						$upload=md5($_FILES[$upload_name]['name']).'_'.time().'.'.$ext;
						$filename=$dir.$upload;
						move_uploaded_file($_FILES[$upload_name]['tmp_name'],$root_dir.$filename);
						http_response_code(200);

						$files[]=['time'=>time(),'name'=>$_FILES[$upload_name]['name'],'size'=>$_FILES[$upload_name]['size'],'type'=>$_FILES[$upload_name]['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>0,'target_id'=>0];//target=none
					}
					else{
						$errors[]='File size must be less than '.$upload_max_size;
					}
				}
				else{
					$errors[]='File must be allowed extension';
				}
			}
			if(0==count($_FILES)){
				$errors[]='File not found';
			}

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			if(0==count($errors)){
				print '
				<div class="success-box" role="alert">
				<p class="font-bold">Success</p>';
				foreach($files as $file){
					$db->sql("INSERT INTO `event_files` SET `organizer`='".$organizer_id."', `event`='".$event_id."',`time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$file['target_id']."',`address`='".$auth['status_address_id']."'");
					$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$event_id."'");
					$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`+".(int)$file['size']." WHERE `id`='".$organizer_id."'");
					print '<p>File ';
					print '<a href="'.htmlspecialchars($file['relative_path']).'" target="_blank" class="font-bold">';
						print htmlspecialchars(substr($file['relative_path'],strrpos($file['relative_path'],'/')+1));
						print '</a>';
					print ' uploaded successfully.</p>';
				}
				print '
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/@'.$organizer_url.'/'.$event_url.'/manage/files/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
			print '<div class="max-w-fit">';
			print '<p class="text-sm">Allowed extensions: '.implode(', ',$custom_upload_allowed_extensions).'.</p>';
			$object_scheme=[//admin custom upload
				'upload'=>[
					'type'=>'file',
					'title'=>'File',
					'descr'=>'Uploaded file will be public available, so you can share it with anyone<br>Max size: '.$upload_max_size.'',
				],
			];
			print build_form($object_scheme);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" name="custom_upload" value="Upload file" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('delete'==$path_array[5]){
		$replace['title']='Delete | '.$replace['title'];
		$file_arr=$db->sql_row("SELECT * FROM `event_files` WHERE `event`='".$event_id."' AND `id`='".(int)$path_array[6]."' LIMIT 1");
		if($file_arr){
			if(isset($_POST['approve'])){
				$free_file_size=filesize($file_arr['path']);
				@unlink($file_arr['path']);
				$db->sql("DELETE FROM `event_files` WHERE `id`='".$file_arr['id']."' LIMIT 1");
				$db->sql("UPDATE `events` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$event_id."'");
				$db->sql("UPDATE `organizers` SET `summary_files_size`=`summary_files_size`-".(int)$free_file_size." WHERE `id`='".$organizer_id."'");
				header('Location: /@'.$organizer_url.'/'.$event_url.'/manage/files/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/files/">&larr; Back to files</a>';
				print '<h2 class="text-red-600">Delete file #'.$file_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Attention</p>
					<p>If you delete this file, it will be lost forever and can not be displayed by linked content.</p>
					<p>Are you sure you want to delete file ';
					print '<a href="'.htmlspecialchars($file_arr['relative_path']).'" target="_blank" class="font-bold">';
					print htmlspecialchars(substr($file_arr['relative_path'],strrpos($file_arr['relative_path'],'/')+1));
					print '</a>';
					print '?</p>
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Yes, delete" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/@'.$organizer_url.'/'.$event_url.'/manage/files/">&larr; Back to files</a>';
			print '<h2 class="text-red-600">File not found</h2>';
			print '<p>Return to files list and try again.</p>';

			header('Location: /@'.$organizer_url.'/'.$event_url.'/manage/files/');
			exit;
		}
	}
	else{
		print '<a class="action-btn" href="/@'.$organizer_url.'/'.$event_url.'/manage/files/upload/">Upload file</a>';
		print '<span class="text-sm">Summary size of event files: '.human_readable_filesize($db->select_one('event_files','SUM(`size`)',"WHERE `event`='".$event_id."'")).'</span>';
		$filter_target=false;
		if(isset($_GET['target'])){
			if(''!==$_GET['target']){
				$filter_target=(int)$_GET['target'];
				if(!isset($event_files_target[$filter_target])){
					$filter_target=false;
				}
			}
		}
		print '<p>'.ltmp($tab_descr_arr[$path_array[4]],$ltml_tab_descr_arr).'</p>';

		$filter_address=false;
		$filter_address_id=false;
		if(isset($_GET['address'])){
			if(''!==$_GET['address']){
				$filter_address=$_GET['address'];
				$filter_address_id=$db->select_one('addresses','id',"WHERE `address`='".$db->prepare($_GET['address'])."'");
				if(!$filter_address_id){
					$filter_address_id=-1;
				}
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="address" class="form-post-by-enter" placeholder="Uploader address" value="'.($filter_address?htmlspecialchars($filter_address):'').'">
			</div>
			<div>
				<select name="target" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_target?' selected':'').'>Filter by target</option>';
					foreach($event_files_target as $target_type=>$target_caption){
						print '<option value="'.$target_type.'"'.($target_type===$filter_target?' selected':'').'>Target: '.htmlspecialchars($target_caption).'</option>';
					}
					print '
				</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>File</th>';
			print '<th>Name</th>';
			print '<th>Size</th>';
			print '<th>Target</th>';
			print '<th>Time</th>';
			print '<th>Uploader</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		$sql_addon[]="`event`='".$event_id."'";
		if(false!==$filter_target){
			$sql_addon[]='`target`='.$filter_target;
		}
		if(false!==$filter_address_id){
			$sql_addon[]='`address`='.$filter_address_id;
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		$per_page=50;
		$count=$db->table_count('event_files',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$files=$db->sql("SELECT * FROM `event_files`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($files as $file_arr){
			print '<tr>';
			print '<td>';
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/files/delete/'.$file_arr['id'].'/" class="red-btn">Delete</a>';
			print '</td>';

			print '<td class="text-sm">';
			print '<a href="'.htmlspecialchars($file_arr['relative_path']).'" target="_blank">';
			print htmlspecialchars(substr($file_arr['relative_path'],strrpos($file_arr['relative_path'],'/')+1));
			print '</a>';
			print '</td>';

			print '<td class="text-sm">'.htmlspecialchars($file_arr['name']).'</td>';
			print '<td class="whitespace-nowrap">'.human_readable_filesize($file_arr['size']).'</td>';

			print '<td>';
			$target_link=false;
			if($file_arr['target_id']){
				if(1==$file_arr['target']){
					$target_link='/@'.$organizer_url.'/'.$event_url.'/manage/content/edit/'.$file_arr['target_id'].'/';
				}
				if(2==$file_arr['target']){
					$target_link='/@'.$organizer_url.'/'.$event_url.'/manage/event/';
				}
				if(3==$file_arr['target']){
					$target_link='/@'.$organizer_url.'/'.$event_url.'/manage/speakers/edit/'.$file_arr['target_id'].'/';
				}
				if(4==$file_arr['target']){
					$target_link='/@'.$organizer_url.'/'.$event_url.'/manage/sessions/edit/'.$file_arr['target_id'].'/';
				}
				if(5==$file_arr['target']){
					$target_link='/@'.$organizer_url.'/'.$event_url.'/manage/partners/edit/'.$file_arr['target_id'].'/';
				}
			}
			if($target_link){
				print '<a href="'.htmlspecialchars($target_link).'">';
			}
			print htmlspecialchars($event_files_target[$file_arr['target']]);
			if($target_link){
				print '</a>';
			}
			print '</td>';


			if($file_arr['time']){
				print '<td><span class="time" data-time="'.$file_arr['time'].'">'.date('d.m.Y H:i:s',$file_arr['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}

			$uploader='&mdash;';
			if($file_arr['address']){
				$uploader_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$file_arr['address']."'");
				if($uploader_arr){
					$uploader=htmlspecialchars($uploader_arr['address']);
				}
			}
			print '<td>';
			print $uploader;
			print '</td>';

			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
else{
	http_response_code(404);
	print '<h1>'.htmlspecialchars($path_array[4]).'</h1>';
	print '<p>Page not found.</p>';
}
$manage_content=ob_get_contents();
ob_end_clean();