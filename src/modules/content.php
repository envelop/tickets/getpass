<?php
ob_start();
if(!isset($path_array[2])){
	$path_array[2]='';
}
if(''==$path_array[2]){
	$filter_tag=false;
	$filter_tag_arr=null;
	if(isset($_GET['tag'])){
		if(''!==$_GET['tag']){
			$filter_tag=$_GET['tag'];
			$filter_tag_arr=$db->sql_row('SELECT * FROM `tags` WHERE `url`=\''.$db->prepare($_GET['tag']).'\'');
			if(null==$filter_tag_arr){
				$filter_tag=false;
			}
			else{
				$filter_tag=$filter_tag_arr['id'];
			}
		}
	}

	print '<h1>Platform blog'.(null!=$filter_tag_arr?' #'.htmlspecialchars($filter_tag_arr['url']).'':'').'</h1>';

	print '<hr class="my-4">';
	if(null!=$filter_tag_arr){
		print '<a class="reverse-btn" href="/content/">&larr; Back to blog</a>';
	}
	$sql_addon=[];
	if(1==$auth['status']){//admin
		$sql_addon[]="`content`.`status`<='1'";//only planned & published
	}
	else{
		$sql_addon[]="`content`.`status`='1'";//only published
	}

	$sql_addon_str='';
	if(count($sql_addon)>0){
		$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
	}

	if(false!==$filter_tag){
		$sql_addon_str=' RIGHT JOIN `content_tags` ON `content_tags`.`content`=`content`.`id` AND `content_tags`.`tag`='.$filter_tag.' '.$sql_addon_str;
	}

	$per_page=10;
	$count=$db->table_count('content',$sql_addon_str);
	$pages_count=ceil($count/$per_page);
	$page=1;
	if(isset($_GET['page'])){
		$page=(int)$_GET['page'];
		if($page<1){
			$page=1;
		}
		elseif($page>$pages_count){
			$page=$pages_count;
		}
	}

	$content_counter=0;
	$content=$db->sql("SELECT `content`.* FROM `content` ".$sql_addon_str." ORDER BY `content`.`time` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
	foreach($content as $content_item){
		$content_counter++;

		$content_html=clear_html_tags($content_item['content']);
		$pinned=(1==$content_item['pin']);

		$tags=$db->sql("SELECT `tag` FROM `content_tags` WHERE `content`='".$content_item['id']."'");
		$tags_str='';
		foreach($tags as $tag){
			$tag_arr=$db->sql_row("SELECT * FROM `tags` WHERE `id`='".$tag['tag']."'");
			$tags_str.='<a class="badge bg-tag" href="/content/?tag='.$tag_arr['url'].'">'.$tag_arr['caption'].'</a> ';
		}
		$tags_str=trim($tags_str);

		print '<div class="content-card'.(0==$content_item['status']?' draft':'').'">';
			if($content_item['cover_url']){
				print '<div class="content-cover mb-4 md:mr-6">';
				print '<a href="/content/'.$content_item['url'].'/">';
				print '<img src="'.$content_item['thumbnail_cover_url'].'" alt="'.htmlspecialchars($content_item['title']).'">';
				print '</a>';
				print '</div>';
			}
			print '<div class="content-info">';
				print '<div class="content-data">';
				print '<div class="content-title">';
				print '<a href="/content/'.$content_item['url'].'/">'.htmlspecialchars($content_item['title']).'</a>';
					if($pinned){
						print '<span class="content-pinned" title="Pinned content">'.$ltmp['icons']['starred'].'</span>';
					}
				print '</div>';
				print '<div class="content-description">'.htmlspecialchars($content_item['description']).'</div>';
				print '</div>';
				print '<div class="content-time" data-datetime="'.$content_item['time'].'">';
				print '<div class="content-global-time">
				<span class="content-datetime">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>
				</div>';
				print '<div class="content-local-time">
				<span class="content-datetime"></span>
				</div>';
				print '<div class="content-tags">'.$tags_str.'</div>';
				print '</div>';
			print '</div>';
		print '</div>';
	}
	if(0==$content_counter){
		print '<p>No content was found</p>';
	}

	print '<div class="pagination">';
	//get string with all GET params except page
	$get_params=[];
	foreach($_GET as $get_param_name=>$get_param_value){
		if('page'!==$get_param_name){
			$get_params[]=$get_param_name.'='.urlencode($get_param_value);
		}
	}
	$get_params_str='';
	if(count($get_params)){
		$get_params_str=implode('&',$get_params);
	}
	if($page>1){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
	}
	for($i=1;$i<=$pages_count;$i++){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
	}
	if($page<$pages_count){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
	}
	print '</div>';
}
else{
	$content_url=urldecode($path_array[2]);
	$sql_addon_str='';
	if(1==$auth['status']){//admin
		$sql_addon_str=" AND `status`<='1'";//only planned & published
	}
	else{
		$sql_addon_str=" AND `status`='1'";//only published
	}
	$content_item=$db->sql_row("SELECT * FROM `content` WHERE `url`='".$db->prepare($content_url)."'".$sql_addon_str);
	if(null!==$content_item){
		$clear_content=clear_html_tags($content_item['content']);
		$pinned=(1==$content_item['pin']);

		$tags=$db->sql("SELECT `tag` FROM `content_tags` WHERE `content`='".$content_item['id']."'");
		$tags_str='';
		foreach($tags as $tag){
			$tag_arr=$db->sql_row("SELECT * FROM `tags` WHERE `id`='".$tag['tag']."'");
			$tags_str.='<a class="badge bg-tag" href="/content/?tag='.$tag_arr['url'].'">'.$tag_arr['caption'].'</a> ';
		}
		$tags_str=trim($tags_str);

		if(0==$content_item['status']){
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Notice</p>';
			if($content_item['time']){
				print '<p>This content is planned to be published at <span class="time" data-time="'.$content_item['time'].'">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>. <a href="/admin/content/edit/'.$content_item['id'].'/">Click to edit.</a></p>';
			}
			else{
				print '<p>This content is in drafts, you can schedule it to be published. <a href="/admin/content/edit/'.$content_item['id'].'/">Click to edit.</a></p>';
			}
			print '</div>';
		}

		$replace['title']=htmlspecialchars($content_item['title']).' | '.$replace['title'];
		if(''!=$content_item['description']){
			$replace['description']=$content_item['description'];
			$replace['description']=strip_tags($replace['description']);
			$replace['description']=str_replace(array("\r\n","\n","\r"),' ',$replace['description']);
			$replace['description']=preg_replace('/\s+/',' ', $replace['description']);
			$replace['description']=substr($replace['description'],0,160);
			$replace['description'].='...';
			$replace['description']=htmlspecialchars($replace['description']);
		}

		$replace['head_addon'].='<meta property="og:title" content="'.htmlspecialchars($content_item['title']).'">';
		$replace['head_addon'].='<meta property="og:description" content="'.htmlspecialchars($content_item['description']).'">';
		if(''!=$content_item['cover_url']){
			$replace['head_addon'].='<meta property="og:image" content="'.$config['platform_url'].$content_item['cover_url'].'">';
		}
		$replace['head_addon'].='<meta property="og:url" content="'.$config['platform_url'].'/'.$content_item['url'].'/">';
		$replace['head_addon'].='<meta property="og:type" content="article">';
		$replace['head_addon'].='<meta property="og:site_name" content="'.$replace['title'].'">';
		$replace['head_addon'].='<meta property="article:published_time" content="'.date('c',$content_item['time']).'">';
		$replace['head_addon'].='<meta property="article:modified_time" content="'.date('c',$content_item['time']).'">';
		$replace['head_addon'].='<meta property="article:section" content="Blog">';
		$replace['head_addon'].='<meta property="og:locale" content="en_US">';
		$replace['head_addon'].='<meta property="og:locale:alternate" content="ru_RU">';

		$replace['head_addon'].='<meta property="article:tag" content="">';

		print '<h1>'.htmlspecialchars($content_item['title']);
		if($pinned){
			print '<span class="content-pinned" title="Pinned content">'.$ltmp['icons']['starred'].'</span>';
		}
		print '</h1>';
		print '<div class="content-card single article">';
		print '<div class="content-wrapper">';
		print $clear_content;
		print '</div>';
		print '<div class="content-info">';
			print '<div class="content-time" data-datetime="'.$content_item['time'].'">';
			print '<div class="content-global-time">
			<span class="content-datetime">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>
			</div>';
			print '<div class="content-local-time">
			<span class="content-datetime"></span>
			</div>';
			print '<div class="content-tags">'.$tags_str.'</div>';
			print '</div>';
		print '</div>';
		print '</div>';

		print '<hr class="my-4">';
		print '<a class="reverse-btn" href="/content/">&larr; Back to blog</a>';
	}
	else{
		header("HTTP/1.0 404 Not Found");
		print '<h1>404 Not Found</h1>';
		print '<p>Content not found, please return to <a href="/content/">platform blog</a>.</p>';
	}
}
$content=ob_get_contents();
ob_end_clean();