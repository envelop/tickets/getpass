<?php
set_time_limit(0);
//script work time near 1 minute with sleep between cycles
$work_time=0;//45;//60 minus 15 seconds//0 for one cycle
$sleep_time=10;//sleep time in seconds between cycles
$start_time=time();

$lock_fp=fopen($root_dir.'/deliver_content_cron.lock','w');
if(!flock($lock_fp,LOCK_EX|LOCK_NB)){
	exit;
}

//Note: if adding new platform check conditions for $linked_platforms_count

while(true){
	//every 1 hour take addresses and form platform content updates from time range
	$new_content_time_range=1*60*60;//1 hour
	$new_content_offset=time()-$new_content_time_range;
	$mailing_addresses=$db->sql("SELECT * FROM `addresses` WHERE `mailing_platform`<'".($new_content_offset)."'");
	foreach($mailing_addresses as $address_item){
		$address_mailing_offset=$address_item['mailing_platform'];
		$db->sql("UPDATE `addresses` SET `mailing_platform`='".time()."' WHERE `id`='".$address_item['id']."'");
		$linked_platforms_count=$db->table_count('linked_platforms','WHERE `address`="'.$address_item['id'].'" AND (`platform`=2 OR `platform`=1) AND `status`=2');
		if(!$linked_platforms_count){//no linked platforms
			continue;//ignore this address
		}

		$new_content_count=$db->table_count('content','WHERE `status`=1 AND `time`>"'.($address_mailing_offset).'"');
		if(!$new_content_count){//no new notifications
			//error_log('platform no content for '.$address_item['id']);
			continue;//ignore this address
		}

		$address_str=htmlspecialchars($address_item['address']);
		//$address_str.=' ('.$types_arr[$address_item['type']]['name'].')';
		$username='%username%';
		if(isset($platform_data['name'])){
			$username=htmlspecialchars($platform_data['name']);
		}

		$subject=$new_content_count.' new updates from the platform';

		$body='';
		$altbody='';
		$links=[];
		$new_content=$db->sql("SELECT * FROM `content` WHERE `status`=1 AND `time`>".($address_mailing_offset)." ORDER BY `time` ASC");
		foreach($new_content as $content_arr){
			if($new_content_count==1){
				$links[]=$config['platform_url'].'/content/'.$content_arr['url'].'/';
				$subject=htmlspecialchars($content_arr['title']);
				$altbody.=strip_tags($content_arr['title']).PHP_EOL.strip_tags($content_arr['content']).PHP_EOL.PHP_EOL;
				$body.='<table class="clear"><tr><td class="content-title"><a href="'.($config['platform_url'].'/content/'.$content_arr['url'].'/').'">'.htmlspecialchars($content_arr['title']).'</a></td></tr><tr>';
				//ignore $content_arr['thumbnail_cover_url'] - it for preview only
				$body.='<td class="content-body">'.prepare_mail_body(clear_html_tags($content_arr['content'])).'</td>';
				$body.='</tr></table>';
			}
			else{
				$links[]=$config['platform_url'].'/content/'.$content_arr['url'].'/';
				$altbody.=strip_tags($content_arr['title']).PHP_EOL.strip_tags($content_arr['description']).PHP_EOL.PHP_EOL;
				$body.='<table class="clear" style="margin-bottom:25px;"><tr><td colspan="2" class="content-title"><a href="'.($config['platform_url'].'/content/'.$content_arr['url'].'/').'">'.htmlspecialchars($content_arr['title']).'</a></td></tr><tr>';
				if($content_arr['thumbnail_cover_url']){
					$body.='<td class="content-cover"><a href="'.($config['platform_url'].'/content/'.$content_arr['url'].'/').'"><img src="'.$config['platform_url'].$content_arr['thumbnail_cover_url'].'" alt="cover"></a></td>';
					$body.='<td class="content-description">'.htmlspecialchars(strip_tags($content_arr['description'])).'<br><a href="'.($config['platform_url'].'/content/'.$content_arr['url'].'/').'">Read more...</a></td>';
				}
				else{
					$body.='<td class="content-description" style="padding-left:0" colspan="2">'.htmlspecialchars(strip_tags($content_arr['description'])).'<br><a href="'.($config['platform_url'].'/content/'.$content_arr['url'].'/').'">Read more...</a></td>';
				}
				$body.='</tr></table>';
			}
		}
		$body.='<p class="notify-item"><a href="'.($config['platform_url'].'/content/').'">View all publications</a></p>';

		//check linked platforms for email (id=2) to send email with list of new content
		$linked_platforms_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$address_item['id']."' AND `platform`=2 AND `status`=2 AND `flag1`=1");//flag1 - news from platform (as user)
		foreach($linked_platforms_arr as $linked_platform){
			$platform_data=json_decode($linked_platform['data'],true);
			if(isset($platform_data['name'])){
				$username=htmlspecialchars($platform_data['name']);
			}
			$body_addon='<br><br><font style="color:#666;">You got it by active subscription at the address below.<br><br>You can change mailing rules on the <a href="'.$config['platform_url'].'/mailing-rules/'.$linked_platform['platform'].'/'.$linked_platform['id'].'/">configuration page</a>.<br>Address: '.$address_str.'</font>';

			new_mail($linked_platform['internal_username'],$username,$subject,$body.$body_addon);
		}
		//check linked platforms for telegram (id=1) to send with list of new content
		$linked_platforms=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$address_item['id']."' AND `platform`=1 AND `status`=2 AND `flag1`=1");//flag1 - news from platform (as user)
		foreach($linked_platforms as $linked_platform){
			foreach($links as $link){
				telegram_queue($linked_platform['internal_id'],'sendMessage',['text'=>$link]);
			}
		}
	}

	//every 1 hour take addresses and form organizers content updates from time range, check mailing rules, implode all organizers content and add mail to queue
	$new_content_time_range=1*60*60;//1 hour
	$new_content_offset=time()-$new_content_time_range;
	$mailing_addresses=$db->sql("SELECT * FROM `addresses` WHERE `mailing_organizers`<'".($new_content_offset)."'");
	foreach($mailing_addresses as $address_item){
		$address_mailing_offset=$address_item['mailing_organizers'];
		$db->sql("UPDATE `addresses` SET `mailing_organizers`='".time()."' WHERE `id`='".$address_item['id']."'");
		$linked_platforms_count=$db->table_count('linked_platforms','WHERE `address`="'.$address_item['id'].'" AND (`platform`=2 OR `platform`=1) AND `status`=2');
		if(!$linked_platforms_count){//no linked platforms
			continue;//ignore this address
		}

		$content_counter=0;
		$subject='';
		$body='';
		$altbody='';
		$links=[];
		//get subscribtions to organizers
		$mailing_rules=$db->sql("SELECT * FROM `mailing_rules` WHERE `address`='".$address_item['id']."' AND `type`=1 AND `rule`=1");//type organizer, rule subscribed
		foreach($mailing_rules as $mailing_rule){
			$new_content_count=$db->table_count('organizer_content','WHERE `organizer`='.$mailing_rule['target'].' AND `status`=1 AND `time`>"'.($address_mailing_offset).'"');
			if(!$new_content_count){//no new notifications
				//error_log('organizers no content for '.$address_item['id']);
				continue;//ignore this address
			}

			$address_str=htmlspecialchars($address_item['address']);
			//$address_str.=' ('.$types_arr[$address_item['type']]['name'].')';
			$username='%username%';
			if(isset($platform_data['name'])){
				$username=htmlspecialchars($platform_data['name']);
			}

			$new_content=$db->sql("SELECT * FROM `organizer_content` WHERE `organizer`='".$mailing_rule['target']."' AND `status`=1 AND `time`>".($address_mailing_offset)." ORDER BY `time` ASC");
			foreach($new_content as $content_arr){
				$content_counter++;
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$content_arr['organizer']."'");
				if($new_content_count==1){
					$links[]=$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/content/'.$content_arr['url'].'/';
					$subject=htmlspecialchars($organizer_arr['title']).': '.htmlspecialchars($content_arr['title']);
					$altbody.=strip_tags($content_arr['title']).PHP_EOL.strip_tags($content_arr['content']).PHP_EOL.PHP_EOL;
					$body.='<table class="clear"><tr><td class="content-title">'.htmlspecialchars($organizer_arr['title']).': <a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/content/'.$content_arr['url'].'/').'">'.htmlspecialchars($content_arr['title']).'</a></td></tr><tr>';
					//ignore $content_arr['thumbnail_cover_url'] - it for preview only
					$body.='<td class="content-body">'.prepare_mail_body(clear_html_tags($content_arr['content'])).'</td>';
					$body.='</tr></table>';
				}
				else{
					$links[]=$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/content/'.$content_arr['url'].'/';
					$altbody.=strip_tags($content_arr['title']).PHP_EOL.strip_tags($content_arr['description']).PHP_EOL.PHP_EOL;
					$body.='<table class="clear" style="margin-bottom:25px;"><tr><td colspan="2" class="content-title">'.htmlspecialchars($organizer_arr['title']).': <a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/content/'.$content_arr['url'].'/').'">'.htmlspecialchars($content_arr['title']).'</a></td></tr><tr>';
					if($content_arr['thumbnail_cover_url']){
						$body.='<td class="content-cover"><a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/content/'.$content_arr['url'].'/').'"><img src="'.$config['platform_url'].$content_arr['thumbnail_cover_url'].'" alt="cover"></a></td>';
						$body.='<td class="content-description">'.htmlspecialchars(strip_tags($content_arr['description'])).'<br><a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/content/'.$content_arr['url'].'/').'">Read more...</a></td>';
					}
					else{
						$body.='<td class="content-description" style="padding-left:0" colspan="2">'.htmlspecialchars(strip_tags($content_arr['description'])).'<br><a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/content/'.$content_arr['url'].'/').'">Read more...</a></td>';
					}
					$body.='</tr></table>';
				}
			}
		}
		if($content_counter){
			if(1!=$content_counter){
				$subject=$content_counter.' updates from organizers';
			}
			//check linked platforms for email (id=2) to send with list of new content
			$linked_platforms_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$address_item['id']."' AND `platform`=2 AND `status`=2 AND `flag4`=1");//flag4 - news from organizers
			foreach($linked_platforms_arr as $linked_platform){
				$platform_data=json_decode($linked_platform['data'],true);
				if(isset($platform_data['name'])){
					$username=htmlspecialchars($platform_data['name']);
				}
				$body_addon='<br><br><font style="color:#666;">You got it by active subscription at the address below.<br><br>You can change mailing rules on the <a href="'.$config['platform_url'].'/mailing-rules/'.$linked_platform['platform'].'/'.$linked_platform['id'].'/">configuration page</a>.<br>Address: '.$address_str.'</font>';

				new_mail($linked_platform['internal_username'],$username,$subject,$body.$body_addon);
			}
			//check linked platforms for telegram (id=1) to send messages with links to new content
			$linked_platforms=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$address_item['id']."' AND `platform`=1 AND `status`=2 AND `flag4`=1");//flag4 - news from organizers
			foreach($linked_platforms as $linked_platform){
				foreach($links as $link){
					telegram_queue($linked_platform['internal_id'],'sendMessage',['text'=>$link]);
				}
			}
		}
	}

	//every 1 hour take addresses and form events content updates from time range, check mailing rules, explode all event content to separate mails and add them to queue
	$new_content_time_range=1*60*60;//1 hour
	$new_content_offset=time()-$new_content_time_range;
	$mailing_addresses=$db->sql("SELECT * FROM `addresses` WHERE `mailing_events`<'".($new_content_offset)."'");
	foreach($mailing_addresses as $address_item){
		$address_mailing_offset=$address_item['mailing_events'];
		$db->sql("UPDATE `addresses` SET `mailing_events`='".time()."' WHERE `id`='".$address_item['id']."'");
		$linked_platforms_count=$db->table_count('linked_platforms','WHERE `address`="'.$address_item['id'].'" AND (`platform`=2 OR `platform`=1) AND `status`=2');
		if(!$linked_platforms_count){//no linked platforms
			continue;//ignore this address
		}

		$events_list=[];

		//get subscriptions to events
		$mailing_rules=$db->sql("SELECT * FROM `mailing_rules` WHERE `address`='".$address_item['id']."' AND `type`=2 AND `rule`=1");//type event, rule subscribed
		foreach($mailing_rules as $mailing_rule){
			$events_list[$mailing_rule['target']]=0;
		}

		//add events_list if address is user in event (check in in past)
		$events_user=$db->sql("SELECT `event`,`level` FROM `event_users` WHERE `address`='".$address_item['id']."'");
		foreach($events_user as $event_user){
			$events_list[$event_user['event']]=$event_user['level'];
		}

		//remove unsubscribed events
		$mailing_rules=$db->sql("SELECT * FROM `mailing_rules` WHERE `address`='".$address_item['id']."' AND `type`=2 AND `rule`=2");//type event, rule unsubscribed
		foreach($mailing_rules as $mailing_rule){
			//remove from list
			unset($events_list[$mailing_rule['target']]);
		}

		foreach($events_list as $event_id=>$event_user_level){
			//get event data
			$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."' AND `status`=1");//only active events
			if(!$event_arr){
				continue;
			}
			$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."' AND `status`=1");//only approved organizers
			if(!$organizer_arr){
				continue;
			}

			$content_counter=0;
			$subject='';
			$body='';
			$altbody='';
			$links=[];

			$new_content_count=$db->table_count('event_content','WHERE `event`='.$event_id.' AND `status`=1 AND `time`>"'.($address_mailing_offset).'"');
			if(!$new_content_count){//no new notifications
				//error_log('event no content for '.$address_item['id']);
				continue;//ignore this address
			}

			$address_str=htmlspecialchars($address_item['address']);
			//$address_str.=' ('.$types_arr[$address_item['type']]['name'].')';
			$username='%username%';
			if(isset($platform_data['name'])){
				$username=htmlspecialchars($platform_data['name']);
			}

			$new_content=$db->sql("SELECT * FROM `event_content` WHERE `event`='".$event_id."' AND `status`=1 AND `time`>".($address_mailing_offset)." ORDER BY `time` ASC");
			foreach($new_content as $content_arr){
				$is_allowed=false;

				if(0==$content_arr['level']){//public for guests
					$is_allowed=true;
				}
				else{
					if($content_arr['level']<=$event_user_level){
						$is_allowed=true;
					}
				}

				if($is_allowed){
					$content_counter++;
					if($new_content_count==1){
						$links[]=$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/content/'.$content_arr['url'].'/';
						$subject=htmlspecialchars($event_arr['title']).': '.htmlspecialchars($content_arr['title']);
						$altbody.=strip_tags($content_arr['title']).PHP_EOL.strip_tags($content_arr['content']).PHP_EOL.PHP_EOL;
						$body.='<table class="clear"><tr><td class="content-title">'.htmlspecialchars($event_arr['title']).': <a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/content/'.$content_arr['url'].'/').'">'.htmlspecialchars($content_arr['title']).'</a></td></tr><tr>';
						//ignore $content_arr['thumbnail_cover_url'] - it for preview only
						$body.='<td class="content-body">'.prepare_mail_body(clear_html_tags($content_arr['content'])).'</td>';
						$body.='</tr></table>';
					}
					else{
						$links[]=$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/content/'.$content_arr['url'].'/';
						$altbody.=strip_tags($content_arr['title']).PHP_EOL.strip_tags($content_arr['description']).PHP_EOL.PHP_EOL;
						$body.='<table class="clear" style="margin-bottom:25px;"><tr><td colspan="2" class="content-title">'.htmlspecialchars($organizer_arr['title']).': <a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/content/'.$content_arr['url'].'/').'">'.htmlspecialchars($content_arr['title']).'</a></td></tr><tr>';
						if($content_arr['thumbnail_cover_url']){
							$body.='<td class="content-cover"><a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/content/'.$content_arr['url'].'/').'"><img src="'.$config['platform_url'].$content_arr['thumbnail_cover_url'].'" alt="cover"></a></td>';
							$body.='<td class="content-description">'.htmlspecialchars(strip_tags($content_arr['description'])).'<br><a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/content/'.$content_arr['url'].'/').'">Read more...</a></td>';
						}
						else{
							$body.='<td class="content-description" style="padding-left:0" colspan="2">'.htmlspecialchars(strip_tags($content_arr['description'])).'<br><a href="'.($config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/content/'.$content_arr['url'].'/').'">Read more...</a></td>';
						}
						$body.='</tr></table>';
					}
				}
			}

			if($content_counter){
				if(1!=$content_counter){
					$subject=$content_counter.' updates from '.htmlspecialchars($event_arr['title']);
				}
				//check linked platforms for email (id=2) to send with list of new content
				$linked_platforms_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$address_item['id']."' AND `platform`=2 AND `status`=2 AND `flag5`=1");//flag5 - news from events
				foreach($linked_platforms_arr as $linked_platform){
					$platform_data=json_decode($linked_platform['data'],true);
					if(isset($platform_data['name'])){
						$username=htmlspecialchars($platform_data['name']);
					}
					$body_addon='';
					$body_addon.='<br><br><font style="color:#444;">Event <a href="'.$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/">'.htmlspecialchars($event_arr['title']).'</a> organized by <a href="'.$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/">'.htmlspecialchars($organizer_arr['title']).'</a></font>';
					$body_addon.='<br><br><font style="color:#666;">You got it by active subscription at the address below.<br><br>You can change mailing rules on the <a href="'.$config['platform_url'].'/mailing-rules/'.$linked_platform['platform'].'/'.$linked_platform['id'].'/">configuration page</a>.<br>Address: '.$address_str.'</font>';

					new_mail($linked_platform['internal_username'],$username,$subject,$body.$body_addon);
				}
				//check linked platforms for telegram (id=1) to send messages with links to new content
				$linked_platforms=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$address_item['id']."' AND `platform`=1 AND `status`=2 AND `flag5`=1");//flag5 - news from events
				foreach($linked_platforms as $linked_platform){
					foreach($links as $link){
						telegram_queue($linked_platform['internal_id'],'sendMessage',['text'=>$link]);
					}
				}
			}
		}
	}
	//check work time
	$now_time=time();
	if($work_time <= $now_time-$start_time){
		break;
	}
	sleep($sleep_time);
}
//close lock file
flock($lock_fp,LOCK_UN);
fclose($lock_fp);
exit;