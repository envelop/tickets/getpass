<?php
//reserved urls for prevent url fraud
$reserved_urls=[
	'preload',
	'prepare',
	'index',
	'login',
	'logout',
	'events',
	'admin',
	'profile',
	'organizer',
	'organizers',
	'about',
	'ajax',
	'api',
	'add-organizer',
	'notifications',
	'manage',
	'subscribe',
	'unsubscribe',
	'partners',
	'user_session',
];

$types_arr=[];
$types=$db->sql("SELECT * FROM `types`");
foreach($types as $type){
	$types_arr[$type['id']]=$type;
}

$chains_status_arr=[
	0=>'Enabled',
	1=>'Disabled',
	2=>'Hidden',
];
$chains_status_arr_class=[
	0=>'text-green-500',
	1=>'text-red-500',
	2=>'text-gray-500',
];

$oracle_updates_status=[
	0=>'Pending',
	1=>'Success',
	2=>'Error',
];
$oracle_updates_status_class=[
	0=>'text-gray-500',
	1=>'text-green-500',
	2=>'text-red-500',
];

$addresses_status_arr=[
	0=>'Default',
	1=>'Admin',
];
$addresses_status_arr_class=[
	0=>'text-gray-500',
	1=>'text-green-500',
];
$addresses_update_arr=[
	0=>'Halted',
	1=>'Scheduled',
	2=>'Forced',
];
$addresses_update_arr_class=[
	0=>'text-gray-500',
	1=>'text-green-500',
	2=>'text-orange-500',
];

$nft_status=[
	0=>'Holded',
	1=>'Activation',
	2=>'Deactivation',
	3=>'Pending update',
];
$nft_status_class=[
	0=>'text-gray-500',
	1=>'text-green-500',
	2=>'text-red-500',
	3=>'text-orange-500',
];

$organizers_status_arr=[
	0=>'Waiting',
	1=>'Approved',
	2=>'Banned',
	3=>'Hidden',
];
$organizers_status_arr_class=[
	0=>'text-orange-500',
	1=>'text-green-500',
	2=>'text-red-500',
	3=>'text-gray-500',
];

$organizer_addresses_status_arr=[
	0=>'Admin',
	1=>'Owner',
];
$organizer_addresses_status_arr_class=[
	0=>'text-gray-500',
	1=>'text-green-500',
];

$events_status_arr=[
	0=>'Planning',
	1=>'Active',
	2=>'Archived',
	3=>'Hidden',
];
$events_status_style_arr=[
	0=>'text-yellow-500',
	1=>'text-green-500',
	2=>'text-orange-500',
	3=>'text-gray-500',
];
$events_moderation_arr=[
	0=>'Waiting',
	1=>'Approved',
	2=>'Rejected',
];
$events_moderation_style_arr=[
	0=>'text-yellow-600',
	1=>'text-green-600',
	2=>'text-orange-600',
];

$user_status_arr=[
	0=>'Guest',
	1=>'Binded',
	2=>'Invited',
];
$user_status_style_arr=[
	0=>'text-gray-600',
	1=>'text-orange-600',
	2=>'text-green-600',
];
$type_token_type_arr=[
	1=>[//EVM
		3=>'ERC-721',
		4=>'ERC-1155'
	],
];

$invite_status_arr=[
	0=>'Pending',
	1=>'Accepted',
];
$invite_status_style_arr=[
	0=>'text-gray-600',
	1=>'text-green-600',
];

$sessions_status_arr=[
	0=>'Default',
	1=>'Starred',
	2=>'Hidden',
];
$sessions_status_style_arr=[
	0=>'text-green-600',
	1=>'text-yellow-600',
	2=>'text-gray-600',
];

$locations_status_arr=[
	0=>'Enabled',
	1=>'Disabled',
];
$locations_status_style_arr=[
	0=>'text-green-600',
	1=>'text-gray-600',
];

$whitelist_status_arr=[
	'0'=>'Not binded',
	'1'=>'Binded',
];
$whitelist_status_style_arr=[
	0=>'text-gray-600',
	1=>'text-green-600',
];

$speakers_status_arr=[
	0=>'Active',
	1=>'Starred',
	2=>'Hidden',
];
$speakers_status_default_id=0;
$speakers_status_style_arr=[
	0=>'text-green-500',
	1=>'text-yellow-500',
	2=>'text-gray-500',
];

$content_status_arr=[
	0=>'Planned',
	1=>'Published',
	2=>'Hidden',
	3=>'Banned',
];
$content_status_style_arr=[
	0=>'text-yellow-500',
	1=>'text-green-500',
	2=>'text-gray-500',
	3=>'text-red-500',
];

$linked_platforms_status_arr=[
	0=>'Wait for delivery',
	1=>'Wait for confirmation',
	2=>'Confirmed',
	3=>'Rejected',
	4=>'Deleted'
];
$linked_platforms_status_style_arr=[
	0=>'text-gray-400',
	1=>'text-orange-500',
	2=>'text-green-600',
	3=>'text-red-500',
	4=>'text-red-300'
];

$partners_status_arr=[
	0=>'Active',
	1=>'Starred',
	2=>'Hidden',
];
$partners_status_style_arr=[
	0=>'text-green-500',
	1=>'text-yellow-500',
	2=>'text-gray-500',
];

$requests_status_arr=[
	0=>'Waiting',
	1=>'Approved',
	2=>'Rejected',
];
$requests_status_style_arr=[
	0=>'text-orange-500',
	1=>'text-green-600',
	2=>'text-red-500',
];

$event_flags_arr=['admin','manager','speaker','sponsor'];
$event_template_parts=['main','speakers','location','tickets','timer','partners','blog'];

$admin_files_target=[
	'0'=>'None',
	'1'=>'Content',
	'2'=>'Event',
];
$organizer_files_target=[
	'0'=>'None',
	'1'=>'Content',
	'2'=>'Event',
];
$event_files_target=[
	'0'=>'None',
	'1'=>'Content',
	'2'=>'Event',
	'3'=>'Speaker',
	'4'=>'Session',
	'5'=>'Partner',
];
$custom_upload_allowed_extensions=[
	'jpg',
	'jpeg',
	'png',
	'gif',
	'webp',
	'pdf',
	'zip',
	'rar',
	'7z',
	'gz',
	'tar',
	'bz2',
	'xz',
	'csv',
	'txt',
	'rtf',
	'odt',
	'doc',
	'docx',
	'xls',
	'xlsx',
	'ppt',
	'pptx',
	'pps',
	'ppsx',
	'odp',
	'odg',
	'odc',
	'odf',
	'odb',
	'odm',
	'ogg',
	'mp3',
	'mp4',
	'wav',
	'webm',
	'avi',
	'mov',
	'mkv',
	'wmv',
	'flv',
];

//event customize options
$logo_style=[
	0=>['caption'=>'Square (not rounded)','class_addon'=>'!rounded-none'],
	1=>['caption'=>'Square (small rounded)','class_addon'=>'!rounded'],
	2=>['caption'=>'Square (large rounded)','class_addon'=>'!rounded-xl'],
	3=>['caption'=>'Circle (default, max rounded)','class_addon'=>'!rounded-full'],
];

$logo_border=[
	0=>['caption'=>'None','class_addon'=>''],
	1=>['caption'=>'White stroke','class_addon'=>'!border-2 !border-white'],
	2=>['caption'=>'Black stroke','class_addon'=>'!border-2 !border-black'],
	3=>['caption'=>'White semi-transparent stroke','class_addon'=>'!border-2 !border-white/50'],
	4=>['caption'=>'Black semi-transparent stroke','class_addon'=>'!border-2 !border-black/50'],
];

$avatar_style=[
	0=>['caption'=>'Circle (default, max rounded)','class_addon'=>'!rounded-full'],
	1=>['caption'=>'Square (not rounded)','class_addon'=>'!rounded-none'],
	2=>['caption'=>'Square (small rounded)','class_addon'=>'!rounded'],
	3=>['caption'=>'Square (large rounded)','class_addon'=>'!rounded-xl'],
];

$cover_background_color_overlay_options=[
	0=>['caption'=>'None','style_addon'=>'none'],
	1=>['caption'=>'Semi-transparent white (25%)','style_addon'=>'linear-gradient(rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0.25))'],
	2=>['caption'=>'Semi-transparent white (50%)','style_addon'=>'linear-gradient(rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5))'],
	3=>['caption'=>'Semi-transparent white (75%)','style_addon'=>'linear-gradient(rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0.75))'],
	4=>['caption'=>'Semi-transparent black (25%)','style_addon'=>'linear-gradient(rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25))'],
	5=>['caption'=>'Semi-transparent black (50%)','style_addon'=>'linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5))'],
	6=>['caption'=>'Semi-transparent black (75%)','style_addon'=>'linear-gradient(rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0.75))'],
];

$cover_text_styles_options=[
	0=>['caption'=>'White text, black stroke','style_addon'=>'color:white !important;-webkit-text-stroke-color:#000000;'],
	1=>['caption'=>'Black text, white stroke','style_addon'=>'color:black !important;-webkit-text-stroke-color:#ffffff;'],
];
$cover_title_stroke_width_options=[
	0=>['caption'=>'None','style_addon'=>''],
	1=>['caption'=>'1px','style_addon'=>'text-shadow: rgb(0, 0, 0) 1px 0px 0px, rgb(0, 0, 0) 0.540302px 0.841471px 0px, rgb(0, 0, 0) -0.416147px 0.909297px 0px, rgb(0, 0, 0) -0.989992px 0.14112px 0px, rgb(0, 0, 0) -0.653644px -0.756802px 0px, rgb(0, 0, 0) 0.283662px -0.958924px 0px, rgb(0, 0, 0) 0.96017px -0.279415px 0px;'],
	2=>['caption'=>'2px','style_addon'=>'text-shadow: rgb(0, 0, 0) 2px 0px 0px, rgb(0, 0, 0) 1.75517px 0.958851px 0px, rgb(0, 0, 0) 1.0806px 1.68294px 0px, rgb(0, 0, 0) 0.141474px 1.99499px 0px, rgb(0, 0, 0) -0.832294px 1.81859px 0px, rgb(0, 0, 0) -1.60229px 1.19694px 0px, rgb(0, 0, 0) -1.97998px 0.28224px 0px, rgb(0, 0, 0) -1.87291px -0.701566px 0px, rgb(0, 0, 0) -1.30729px -1.5136px 0px, rgb(0, 0, 0) -0.421592px -1.95506px 0px, rgb(0, 0, 0) 0.567324px -1.91785px 0px, rgb(0, 0, 0) 1.41734px -1.41108px 0px, rgb(0, 0, 0) 1.92034px -0.558831px 0px;'],
	3=>['caption'=>'3px','style_addon'=>'text-shadow: rgb(0, 0, 0) 3px 0px 0px, rgb(0, 0, 0) 2.83487px 0.981584px 0px, rgb(0, 0, 0) 2.35766px 1.85511px 0px, rgb(0, 0, 0) 1.62091px 2.52441px 0px, rgb(0, 0, 0) 0.705713px 2.91581px 0px, rgb(0, 0, 0) -0.287171px 2.98622px 0px, rgb(0, 0, 0) -1.24844px 2.72789px 0px, rgb(0, 0, 0) -2.07227px 2.16926px 0px, rgb(0, 0, 0) -2.66798px 1.37182px 0px, rgb(0, 0, 0) -2.96998px 0.42336px 0px, rgb(0, 0, 0) -2.94502px -0.571704px 0px, rgb(0, 0, 0) -2.59586px -1.50383px 0px, rgb(0, 0, 0) -1.96093px -2.27041px 0px, rgb(0, 0, 0) -1.11013px -2.78704px 0px, rgb(0, 0, 0) -0.137119px -2.99686px 0px, rgb(0, 0, 0) 0.850987px -2.87677px 0px, rgb(0, 0, 0) 1.74541px -2.43999px 0px, rgb(0, 0, 0) 2.44769px -1.73459px 0px, rgb(0, 0, 0) 2.88051px -0.838247px 0px;'],
	4=>['caption'=>'4px','style_addon'=>'text-shadow: rgb(0, 0, 0) 4px 0px 0px, rgb(0, 0, 0) 3.87565px 0.989616px 0px, rgb(0, 0, 0) 3.51033px 1.9177px 0px, rgb(0, 0, 0) 2.92676px 2.72656px 0px, rgb(0, 0, 0) 2.16121px 3.36588px 0px, rgb(0, 0, 0) 1.26129px 3.79594px 0px, rgb(0, 0, 0) 0.282949px 3.98998px 0px, rgb(0, 0, 0) -0.712984px 3.93594px 0px, rgb(0, 0, 0) -1.66459px 3.63719px 0px, rgb(0, 0, 0) -2.51269px 3.11229px 0px, rgb(0, 0, 0) -3.20457px 2.39389px 0px, rgb(0, 0, 0) -3.69721px 1.52664px 0px, rgb(0, 0, 0) -3.95997px 0.56448px 0px, rgb(0, 0, 0) -3.97652px -0.432781px 0px, rgb(0, 0, 0) -3.74583px -1.40313px 0px, rgb(0, 0, 0) -3.28224px -2.28625px 0px, rgb(0, 0, 0) -2.61457px -3.02721px 0px, rgb(0, 0, 0) -1.78435px -3.57996px 0px, rgb(0, 0, 0) -0.843183px -3.91012px 0px, rgb(0, 0, 0) 0.150409px -3.99717px 0px, rgb(0, 0, 0) 1.13465px -3.8357px 0px, rgb(0, 0, 0) 2.04834px -3.43574px 0px, rgb(0, 0, 0) 2.83468px -2.82216px 0px, rgb(0, 0, 0) 3.44477px -2.03312px 0px, rgb(0, 0, 0) 3.84068px -1.11766px 0px, rgb(0, 0, 0) 3.9978px -0.132717px 0px;'],
	5=>['caption'=>'5px','style_addon'=>'text-shadow: rgb(0, 0, 0) 5px 0px 0px, rgb(0, 0, 0) 4.90033px 0.993347px 0px, rgb(0, 0, 0) 4.60531px 1.94709px 0px, rgb(0, 0, 0) 4.12668px 2.82321px 0px, rgb(0, 0, 0) 3.48353px 3.58678px 0px, rgb(0, 0, 0) 2.70151px 4.20736px 0px, rgb(0, 0, 0) 1.81179px 4.6602px 0px, rgb(0, 0, 0) 0.849836px 4.92725px 0px, rgb(0, 0, 0) -0.145998px 4.99787px 0px, rgb(0, 0, 0) -1.13601px 4.86924px 0px, rgb(0, 0, 0) -2.08073px 4.54649px 0px, rgb(0, 0, 0) -2.94251px 4.04248px 0px, rgb(0, 0, 0) -3.68697px 3.37732px 0px, rgb(0, 0, 0) -4.28444px 2.57751px 0px, rgb(0, 0, 0) -4.71111px 1.67494px 0px, rgb(0, 0, 0) -4.94996px 0.7056px 0px, rgb(0, 0, 0) -4.99147px -0.291871px 0px, rgb(0, 0, 0) -4.83399px -1.27771px 0px, rgb(0, 0, 0) -4.48379px -2.2126px 0px, rgb(0, 0, 0) -3.95484px -3.05929px 0px, rgb(0, 0, 0) -3.26822px -3.78401px 0px, rgb(0, 0, 0) -2.4513px -4.35788px 0px, rgb(0, 0, 0) -1.53666px -4.75801px 0px, rgb(0, 0, 0) -0.560763px -4.96845px 0px, rgb(0, 0, 0) 0.437495px -4.98082px 0px, rgb(0, 0, 0) 1.41831px -4.79462px 0px, rgb(0, 0, 0) 2.34258px -4.41727px 0px, rgb(0, 0, 0) 3.17346px -3.86382px 0px, rgb(0, 0, 0) 3.87783px -3.15633px 0px, rgb(0, 0, 0) 4.4276px -2.32301px 0px, rgb(0, 0, 0) 4.80085px -1.39708px 0px, rgb(0, 0, 0) 4.98271px -0.415447px 0px;'],
];
$cover_descr_text_opacity_options=[
	0=>['caption'=>'None (100%)','style_addon'=>'opacity:1;'],
	1=>['caption'=>'90%','style_addon'=>'opacity:0.9;'],
	2=>['caption'=>'80%','style_addon'=>'opacity:0.8;'],
	3=>['caption'=>'70%','style_addon'=>'opacity:0.7;'],
	4=>['caption'=>'60%','style_addon'=>'opacity:0.6;'],
	5=>['caption'=>'50%','style_addon'=>'opacity:0.5;'],
	6=>['caption'=>'40%','style_addon'=>'opacity:0.4;'],
	7=>['caption'=>'30%','style_addon'=>'opacity:0.3;'],
	8=>['caption'=>'20%','style_addon'=>'opacity:0.2;'],
	9=>['caption'=>'10%','style_addon'=>'opacity:0.1;'],
];

function human_readable_filesize($bytes){
	$units=['B','Kb','Mb','Gb'];
	for($i=0;$bytes>=1024;$i++){
		$bytes/=1024;
	}
	return round($bytes,2).' '.$units[$i];
}

function event_user_badge($event_id,$event_url=false,$organizer_url=false){
	global $db,$auth,$auth_address_arr,$ltmp;

	if(false===$event_url){
		$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."'");
		$event_url=$event_arr['url'];
	}
	if(false===$organizer_url){
		$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");
		$organizer_url=$organizer_arr['url'];
	}

	$user_badge='';
	if(0==$auth['addresses']){
		$user_badge='<a href="/login/?back=/@'.$organizer_url.'/'.$event_url.'/">Login</a>';
	}
	else{
		$user_badge='<a href="/@'.$organizer_url.'/'.$event_url.'/profile/">Check in</a>';
		$find=false;
		$allow_event_manage=false;
		foreach($auth_address_arr as $auth_address_item){
			$search_event_user=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_id."' AND `address`='".$auth_address_item['id']."' AND `level`!=0");
			if(null!=$search_event_user){
				if($search_event_user['admin']){
					$allow_event_manage=true;
					$find=false;//if previous user not admin, but current is - need update user badge
				}
				if($search_event_user['manager']){
					$allow_event_manage=true;
					$find=false;//if previous user not manager, but current is - need update user badge
				}
				if(!$find){
					$address_caption=substr($auth_address_item['address'],0,4).'...'.substr($auth_address_item['address'],-4);
					$user_badge='';
					if(0!=$search_event_user['level']){
						$user_badge.='<a class="check-in-status" title="Successful event registration">'.$ltmp['icons']['check-broken'].'<span class="explainer">Successful event registration</a></a>';
					}
					$user_badge.='<a class="avatar" href="/@'.$organizer_url.'/'.$event_url.'/profile/">';
					$user_badge.='<svg data-jdenticon-value="'.$auth_address_item['address'].'" width="80" height="80"  alt="avatar"></svg>';
					$user_badge.=$address_caption.'</a>';
					$find=true;
				}
				if($allow_event_manage){
					break;//have permission to manage event, stop search
				}
			}
		}
		if(!$find){
			$user_badge='<a href="/@'.$organizer_url.'/'.$event_url.'/profile/">Check in</a>';
		}
	}
	return PHP_EOL.$user_badge;
}

function prepare_mail_body($text){
	//parse oembed url and replace it with html
	$text=parse_oembed($text);
	$text=expand_internal_link($text);
	return $text;
}

function parse_oembed($text){
	$pattern = '/<oembed url="(.*)"><\/oembed>/';
	preg_match_all($pattern, $text, $matches);
	if(!empty($matches[1])){
		foreach($matches[1] as $url){
			$embed_code=get_oembed_code($url);
			if($embed_code){
				$text=str_replace('<oembed url="'.$url.'"></oembed>',$embed_code,$text);
			}
		}
	}
	return $text;
}

function get_oembed_code($url){
	$url=urldecode($url);
	//parse url and get domain
	$code='<a href="'.htmlspecialchars($url).'">'.htmlspecialchars($url).'</a>';
	return $code;
}

function expand_internal_link($text){
	global $config;
	$text=str_replace('//','::doll//',$text);//prevent url without protocol
	$text=str_replace('href="/','href="'.$config['platform_url'].'/',$text);
	$text=str_replace('href=\'/','href=\''.$config['platform_url'].'/',$text);
	$text=str_replace('src="/','src="'.$config['platform_url'].'/',$text);
	$text=str_replace('src=\'/','src=\''.$config['platform_url'].'/',$text);
	$text=str_replace('::doll//','//',$text);
	return $text;
}
// looking html and clear all tags, styles, classes by rules
function clear_html_tags($text){
	$allowed_attr_arr=array('href','rel','target','src','url','alt','max-width','width','max-height','height','style','id','class','colspan','rowspan','frameborder','allowfullscreen');
	$allowed_style_arr=array('text-align','width','font-size','color');
	$allowed_class_arr=array('image','media','table','clear');
	$denied_tags=array('script');
	$denied_href_arr=array('javascript:');
	preg_match_all('~<(.[^>]*)>~iUs',$text,$matches);
	foreach($matches[1] as $match_k=>$match){
		$full_match=$matches[0][$match_k];
		$closing=false;
		$tag_name=$match;
		if(false!==strpos($match,' ')){
			$tag_name=substr($match,0,strpos($match,' '));
		}
		if('/'==$tag_name[0]){
			$closing=true;
			$tag_name=substr($tag_name,1);
		}
		if(in_array($tag_name,$denied_tags)){
			$full_match='';
		}
		else{
			preg_match_all('~(.[^= ]*)="(.[^">]*)"~iUs',$match,$attr_arr);
			$attr_values=array();
			$attr_values_num=0;
			foreach($attr_arr[2] as $k=>$attr_value){
				$replace=$attr_arr[0][$k];
				$attr_values[$attr_values_num]=$attr_value;
				$replace2=str_replace($attr_value,'%ATTRVALUE'.$attr_values_num.'%',$replace);
				$match=str_replace_first($replace,$replace2,$match);
				$attr_values_num++;
			}
			preg_match_all('~(.[^= ]*)=(.[^ >]*) ~iUs',$match.' ',$attr_arr);
			foreach($attr_arr[1] as $attr_k=>$attr){
				$attr_arr[2][$attr_k]=trim($attr_arr[2][$attr_k],'" \'');
				foreach($attr_values as $k=>$attr_value){
					$attr_arr[2][$attr_k]=str_replace('%ATTRVALUE'.$k.'%',$attr_value,$attr_arr[2][$attr_k]);
					$attr_arr[0][$attr_k]=str_replace('%ATTRVALUE'.$k.'%',$attr_value,$attr_arr[0][$attr_k]);
				}
				$attr_arr[0][$attr_k]=trim($attr_arr[0][$attr_k]);
				$attr=trim($attr,'" \'');
				if(!in_array($attr,$allowed_attr_arr)){
					$change=true;
					if('iframe'==$tag_name){
						if('height'==$attr){
							$change=false;
						}
						if('frameborder'==$attr){
							$change=false;
						}
						if('allowfullscreen'==$attr){
							$change=false;
						}
					}
					if($change){
						$full_match=str_replace($attr_arr[0][$attr_k],'',$full_match);
					}
				}
				if('style'==$attr){
					$full_styles=$attr_arr[2][$attr_k];
					if(strpos($full_styles,';')===false){
						$full_styles.=';';
					}
					$styles_arr=explode(';',$full_styles);
					foreach($styles_arr as $style_k=>$style){
						if($style){
							$style_arr=explode(':',$style);
							$style_arr[0]=trim($style_arr[0]);
							$style_arr[1]=trim($style_arr[1]);
							if(!in_array($style_arr[0],$allowed_style_arr)){
								unset($styles_arr[$style_k]);
							}
						}
						else{
							unset($styles_arr[$style_k]);
						}
					}
					$full_styles=implode(';',$styles_arr);
					$full_match=str_replace($attr_arr[2][$attr_k],$full_styles,$full_match);
				}
				if('class'==$attr){
					$full_classes=$attr_arr[2][$attr_k];
					$classes_arr=explode(' ',$full_classes);
					foreach($classes_arr as $class_k=>$class){
						if($class){
							$class=trim($class);
							if(!in_array($class,$allowed_class_arr)){
								unset($classes_arr[$class_k]);
							}
						}
					}
					$full_classes=implode(' ',$classes_arr);
					if($attr_arr[2][$attr_k]!=$full_classes){
						$full_match=str_replace($attr_arr[2][$attr_k],$full_classes,$full_match);
					}
				}
				if('href'==$attr){
					$full_link=$attr_arr[2][$attr_k];
					foreach($denied_href_arr as $denied_href){
						if(strpos($full_link,$denied_href)!==false){
							$full_match=str_replace($attr_arr[2][$attr_k],str_replace($denied_href,'',$full_link),$full_match);
						}
					}
				}
			}
			preg_match_all('~(.[^= ]*)=""~iUs',$full_match,$attr_arr);
			foreach($attr_arr[0] as $free_attr){
				$full_match=str_replace($free_attr,'',$full_match);
			}
		}
		$text=str_replace($matches[0][$match_k],$full_match,$text);
	}
	return $text;
}

function clear_html_tag($html,$tag){//remove single tag from html
	preg_match_all('~<'.$tag.'(.*)>~iUs',$html,$matches);
	foreach($matches[0] as $k=>$v){
		$html=str_replace($v,'',$html);
	}
	return $html;
}

function sort_by_value_length($a,$b){//used in array sorting for replacing longest matches first
	return strlen($b)-strlen($a);
}

function str_replace_first($search,$replace,$subject){
	$pos=strpos($subject, $search);
	if($pos!==false){
		return substr_replace($subject, $replace, $pos, strlen($search));
	}
	return $subject;
}

function add_notify($address,$platform,$type=0,$preset=0,$content=''){
	global $db;
	if(0!==$preset){
		if(is_string($preset)){
			$preset=(int)$db->select_one('presets','id',"WHERE `name`='".$db->prepare($preset)."'");
		}
	}

	if(0==$platform){//web
		$db->sql("INSERT INTO `notifications_queue` (`address`,`platform`,`type`,`preset`,`content`,`time`,`status`) VALUES ('".$address."','".$platform."','".$type."','".$preset."','".$db->prepare($content)."','".time()."','0')");
		$notify_id=$db->last_id();
		return $notify_id;
	}
	return false;
}

function render_notify_preview($notification){
	global $db;
	$result='';
	if(0==$notification['platform']){//web
		if(0==$notification['type']){//text
			if(0==$notification['preset']){//preset not selected
				$result=$notification['content'];
			}
			else{
				$preset_value=$db->select_one('presets','value',"WHERE `id`='".$notification['preset']."'");
				$result_data=json_decode($notification['content'],true);
				$result=ltmp($preset_value,$result_data);
			}
		}
		if(1==$notification['type']){//html
			if(0==$notification['preset']){//preset not selected
				$result=strip_tags($notification['content']);
			}
			else{
				$preset_value=$db->select_one('presets','value',"WHERE `id`='".$notification['preset']."'");
				$result_data=json_decode($notification['content'],true);
				$result=ltmp($preset_value,$result_data);
				$result=strip_tags($result);
			}
		}
	}
	//crop result
	if(strlen($result)>200){
		$result=substr($result,0,200).'...';
	}
	$result=nl2br($result);
	return $result;
}

function render_notify($notification,$external=false){
	global $db,$config;
	$result='';
	if(0==$notification['platform']){//web
		if(0==$notification['type']){//text
			if(0==$notification['preset']){//preset not selected
				$result=$notification['content'];
			}
			else{
				$preset_value=$db->select_one('presets','value',"WHERE `id`='".$notification['preset']."'");
				$result_data=json_decode($notification['content'],true);
				$result=ltmp($preset_value,$result_data);
				$result=nl2br($result);
			}
		}
		if(1==$notification['type']){//html
			if(0==$notification['preset']){//preset not selected
				$result=clear_html_tags($notification['content']);
			}
			else{
				$preset_value=$db->select_one('presets','value',"WHERE `id`='".$notification['preset']."'");
				$result_data=json_decode($notification['content'],true);
				if(isset($result_data['comment'])){//special case for comments (example: event_was_rejected_with_comment)
					$result_data['comment']=nl2br($result_data['comment']);
				}
				$result=ltmp($preset_value,$result_data);
			}
		}
	}
	if($external){
		$result=str_replace('href="/','href="'.$config['platform_url'].'/',$result);
	}
	return $result;
}

function gen_csrf(){
	global $auth,$ip;
	if($auth['id']){
		$time=time();
		return $time.':'.md5($ip.$time.$auth['hash']);
	}
	return '';
}

function gen_csrf_param(){
	global $auth,$ip;
	return 'csrf='.gen_csrf();
}

function gen_csrf_form(){
	global $auth,$ip;
	return '<input type="hidden" name="csrf" value="'.gen_csrf().'">';
}

function check_csrf($post=false){
	global $auth,$ip,$config;
	if($auth['id']){
		$csrf_str='';
		if(true===$post){
			if(isset($_POST['csrf'])){
				$csrf_str=$_POST['csrf'];
			}
		}
		else{
			if(isset($_GET['csrf'])){
				$csrf_str=$_GET['csrf'];
			}
		}
		if(''==$csrf_str){
			return false;
		}
		$csrf_arr=explode(':',$csrf_str);
		if($csrf_arr[0]<(time()-$config['csrf_expiration'])){//csrf expired
			return false;
		}
		if($csrf_arr[1]==md5($ip.$csrf_arr[0].$auth['hash'])){
			return true;
		}
	}
	return false;
}

function new_mail($to,$name,$subject,$text,$preheader='',$headers=false){
	global $db;
	$db->sql("INSERT INTO `mail_queue` (`to`,`name`,`subject`,`text`,`preheader`,`headers`,`time`) VALUES ('".$db->prepare($to)."','".$db->prepare($name)."','".$db->prepare($subject)."','".$db->prepare($text)."','".$db->prepare($preheader)."','".(false!==$headers?$db->prepare(json_encode($headers)):'')."','".time()."')");
}

function csv_prepare($string){
	if((''.intval($string))===$string){//strict number
		return $string;
	}
	else{
		$string=str_replace('"','""',$string);
		$string=str_replace("\n",'\n',$string);
		$string=str_replace("\r",'',$string);
		return '"'.$string.'"';
	}
}

function build_form($scheme,$object=false){
	global $organizer_url,$event_url,$event_arr;
	$result='';
	$collapse=false;
	foreach($scheme as $item_name=>$item){
		if($collapse){//collapse not working with grid
		}
		else{
			//$result.='<div class="grid-wrapper">';
		}
		if(!isset($item['editable'])){
			$item['editable']=true;
		}
		if(!isset($item['required'])){
			$item['required']=false;
		}
		if(!isset($item['title'])){
			$item['title']='';
		}
		if(!isset($item['descr'])){
			$item['descr']='';
		}
		if(!isset($item['placeholder'])){
			$item['placeholder']='';
		}
		$json=[];
		if(isset($item['json_data'])){
			if(false!==$object){
				if(isset($object[$item['json_data']])){
					$json=json_decode($object[$item['json_data']],true);
				}
			}
		}
		$result.=PHP_EOL;
		if('start-collapse'==$item['type']){
			$collapse=true;//remove wrapper for full grid div
			$result.='<div class="my-4">';//fix next closing wrapper div with margin by Y-axis
			$result.='<button class="collapsible toggle-collapse-action" data-target="'.$item_name.'" onclick="return false;">'.$item['title'].'</button>';
			$result.='<div class="collapse-wrapper" id="'.$item_name.'">';
		}
		if(!$collapse){//not in collapse wrapper
			$result.='<div class="grid-wrapper">';
		}
		if('hr'==$item['type']){
			$result.='<hr class="grid-full-wrapper">';
		}
		if('h3'==$item['type']){
			$result.='<h3>'.$item['title'].'</h3>';
		}
		if('checkbox'==$item['type']){
			$result.='<div class="grid-full-wrapper">';
			$result.='<label><input type="checkbox" name="'.$item_name.'" value="'.$item['value'].'"'.(isset($object[$item_name])?($item['value']==$object[$item_name]?' checked':''):'').'> &mdash; '.$item['title'].'</label>';
			$result.='</div>';
		}
		if('multi-checkbox'==$item['type']){
			$result.='<div class="grid-full-wrapper">';
			$result.=$item['title'].':<br>';
			$result.='<div class="multi-checkbox overflow-y-auto max-h-40">';
			foreach($item['options'] as $option){
				$option_type='array';
				if(is_string($option)){
					$option_type='string';
				}

				$caption='';
				if('string'==$option_type){
					$caption=$option;
				}
				if(isset($item['value'])){
					if(isset($option[$item['value']])){
						$caption=$option[$item['value']];
					}
				}
				if(isset($item['value_function'])){
					if(is_callable($item['value_function'])){
						$caption=$item['value_function']($option);
					}
				}

				$value='';
				if('string'==$option_type){
					$value=$option;
				}
				if('array'==$option_type){
					if(isset($option[$item['key']])){
						$value=$option[$item['key']];
					}
				}

				$checked=false;
				if(isset($item['checked_function'])){
					if(is_callable($item['checked_function'])){
						$checked=$item['checked_function']($option,$object);
					}
				}
				$result.='<label><input type="checkbox" name="'.$item_name.'['.$value.']"'.($checked?' checked':'').'> &mdash; '.htmlspecialchars($caption).'</label>';
			}
			$result.='</div>';
			$result.='</div>';
		}
		if('sort'==$item['type']){
			$result.='<div class="grid-full-wrapper sort-wrapper" data-name="'.$item_name.'">';
			$result.=$item['title'].':<br>';
			$result.='<ul>';
			foreach($item['options'] as $option){
				$value=$option;
				if(isset($item['value_function'])){
					if(is_callable($item['value_function'])){
						$value=$item['value_function']($option);
					}
				}
				$caption=$option;
				if(isset($item['caption_function'])){
					if(is_callable($item['caption_function'])){
						$caption=$item['caption_function']($option);
					}
				}
				$result.='<li data-value="'.$value.'">'.$caption.'</li>';
			}
			$result.='</ul>';
			if($item['descr']){
				$result.='<div><span class="form-description">'.$item['descr'].'</span></div>';
			}
			$result.='</div>';
		}
		if('datetime'==$item['type']){
			$key=$item_name;
			if(isset($item['key'])){
				$key=$item['key'];
			}
			if(!isset($item['local'])){
				$item['local']=false;
			}
			if(!isset($item['future'])){
				$item['future']=false;
			}
			$result.='<div class="grid-full-wrapper">';
			if(false!==$object){
				if(isset($object[$key])){
					$value_str='&mdash;';
					if($object[$key]){
						$value_str=date('d.m.Y H:i',$object[$key]).' GMT';
					}
					if(!isset($item['value_title'])){
						$item['value_title']='Current value';
					}
					$result.='<div class="datetime-current-value">'.$item['value_title'].': '.$value_str.'</div>';
				}
			}
			$result.=$item['title'].':<br>';
			$minimal_offset=false;
			if(isset($item['minimal-offset'])){
				$minimal_offset=(int)$item['minimal-offset'];
			}
			$result.='<input type="datetime'.($item['local']?'-local':'').'" name="'.$item_name.'"'.($item['future']?' class="datetime-future"':'').($minimal_offset?' data-minimal-offset="'.$minimal_offset.'"':'').' max="9999-12-31T23:59">';
			if(isset($item['timestamp'])){
				if(isset($item['value'])){
					$value=$item['value'];
				}
				if(isset($object[$key])){
					$value=(int)$object[$key];
				}
				$result.='<input type="hidden" name="'.$item['timestamp'].'" value="'.$value.'"'.(isset($item['timestamp'])?' data-timestamp="true" data-parent="'.$item_name.'"':'').'>';
			}
			if($item['descr']){
				$result.='<div><span class="form-description">'.$item['descr'].'</span></div>';
			}
			if(false===$object){//new object (create)
				if(false!==$item['future']){
					$result.='<div><span class="form-description">Date must be in the future</span></div>';
					if($minimal_offset){
						$result.='<div class="additional-tooltip">At least '.ceil($minimal_offset/60).' minutes from now</div>';
					}
				}
			}
			$result.='</div>';
		}
		if('file'==$item['type']){
			$result.='<div class="grid-full-wrapper">';
			if(false!==$object){
				$file_path='';
				if(isset($object[$item_name])){
					$file_path=$object[$item_name];
				}
				if(isset($item['file_path'])){
					$file_path=$object[$item['file_path']];
				}
				$result.='<div class="mt-4 action-wrapper">';
				$result.='File: <a href="'.htmlspecialchars($file_path).'" class="wrap-long-text" target="_blank">'.htmlspecialchars($file_path).'</a>';
				if($event_url){
					$result.=' <a class="red-btn ajax-action" href="/@'.$organizer_url.'/'.$event_url.'/manage/delete_file/'.$item['object_type'].'/'.$object['id'].'/'.$item['file_path'].'/?'.gen_csrf_param().'">Delete file</a>';
				}
				else{
					if($organizer_url){
						$result.=' <a class="red-btn ajax-action" href="/@'.$organizer_url.'/manage/delete_file/'.$item['object_type'].'/'.$object['id'].'/'.$item['file_path'].'/?'.gen_csrf_param().'">Delete file</a>';
					}
					else{
						$result.=' <a class="red-btn ajax-action" href="/admin/delete_file/'.$item['object_type'].'/'.$object['id'].'/'.$item['file_path'].'/?'.gen_csrf_param().'">Delete file</a>';
					}
				}
				$result.='</div>';
			}
			$result.=$item['title'].':<br>';
			$result.='<input type="file" name="'.$item_name.'" placeholder="'.($item['placeholder']?htmlspecialchars($item['placeholder']):htmlspecialchars($item['title'])).'">';
			if($item['descr']){
				$result.='<div><span class="form-description">'.$item['descr'].'</span></div>';
			}
			$result.='</div>';
		}
		if('image'==$item['type']){
			$result.='<div class="grid-full-wrapper">';
			if(false!==$object){
				$file_path='';
				if(isset($object[$item_name])){
					$file_path=$object[$item_name];
				}
				if(isset($item['file_path'])){
					$file_path=$object[$item['file_path']];
				}
				if($file_path){
					$thumbnail_path=$file_path;
					if(isset($object['thumbnail_'.$item_name])){
						$thumbnail_path=$object['thumbnail_'.$item_name];
					}
					if(isset($item['thumbnail_path'])){
						if(isset($object[$item['thumbnail_path']])){
							$thumbnail_path=$object[$item['thumbnail_path']];
						}
					}
					$result.='<div class="grid-full-wrapper action-wrapper">';
					$result.='<a href="'.htmlspecialchars($file_path).'" target="_blank"><img src="'.htmlspecialchars($thumbnail_path).'" alt="'.$item['placeholder'].'" class="image-type"></a>';
					if($event_url){
						$result.='<a class="red-btn image-sub-button ajax-action" href="/@'.$organizer_url.'/'.$event_url.'/manage/delete_file/'.$item['object_type'].'/'.$object['id'].'/'.$item['file_path'].'/?'.gen_csrf_param().'">Delete file</a>';
					}
					else{
						if($organizer_url){
							$result.='<a class="red-btn image-sub-button ajax-action" href="/@'.$organizer_url.'/manage/delete_file/'.$item['object_type'].'/'.$object['id'].'/'.$item['file_path'].'/?'.gen_csrf_param().'">Delete file</a>';
						}
						else{
							$result.='<a class="red-btn image-sub-button ajax-action" href="/admin/delete_file/'.$item['object_type'].'/'.$object['id'].'/'.$item['file_path'].'/?'.gen_csrf_param().'">Delete file</a>';
						}
					}
					$result.='</div>';
				}
			}
			$result.=$item['title'].':<br>';
			$result.='<input type="file" name="'.$item_name.'" placeholder="'.($item['placeholder']?htmlspecialchars($item['placeholder']):htmlspecialchars($item['title'])).'">';
			if($item['descr']){
				$result.='<div><span class="form-description">'.$item['descr'].'</span></div>';
			}
			$result.='</div>';
		}
		if('text'==$item['type']){
			$class='';
			if(isset($item['class'])){
				$class=$item['class'];
			}
			$value='';
			if(isset($object[$item_name])){
				$value=htmlspecialchars($object[$item_name]);
			}
			$clear_item_name=str_replace('addon_','',$item_name);
			if(isset($json[$clear_item_name])){
				$value=htmlspecialchars($json[$clear_item_name]);
			}
			$result.='<div class="grid-full-wrapper">';
			$result.='<input type="text" name="'.$item_name.'" placeholder="'.($item['placeholder']?htmlspecialchars($item['placeholder']):htmlspecialchars($item['title'])).'" value="'.$value.'"'.($class?' class="'.$class.'"':'').''.($item['required']?' required':'').''.(false===$item['editable']?' disabled':'').'>';
			if(false!==$object){//if not edit mode
				if($item['title']){
					$result.=' &mdash; '.$item['title'];
				}
			}
			if($item['descr']){
				$result.='<div><span class="form-description">'.$item['descr'].'</span></div>';
			}
			if(isset($item['class'])){
				if(false!==strpos($item['class'],'url-part')){
					$tooltip='Allowed characters: a-z, 0-9, "-."';
					if(false!==strpos($item['class'],'with-uppercase')){
						$tooltip='Allowed characters: a-z, A-Z, 0-9, "-."';
					}
					$result.='<div><span class="additional-tooltip">'.htmlspecialchars($tooltip).'</span></div>';
				}
				if(false!==strpos($item['class'],'title-part')){
					$tooltip='Allowed characters: a-z, A-Z, 0-9, "-. ,;:"';
					$result.='<div><span class="additional-tooltip">'.htmlspecialchars($tooltip).'</span></div>';
				}
			}
			$result.='</div>';
		}
		if('textarea'==$item['type']){
			if(!isset($item['wysiwyg'])){
				$item['wysiwyg']=false;
			}
			$result.='<div class="grid-full-wrapper '.($item['wysiwyg']?'!my-8':'').'">';
			$result.=$item['title'].':<br>';
			if($item['wysiwyg']){
				$result.='<div class="wysiwyg-wrapper">';
			}
			$result.='<textarea name="'.$item_name.'" placeholder="'.($item['placeholder']?htmlspecialchars($item['placeholder']):htmlspecialchars($item['title'])).'">'.(isset($object[$item_name])?htmlspecialchars($object[$item_name]):'').'</textarea>';
			if($item['wysiwyg']){
				$result.='<br><a class="small-btn activate-wysiwyg-action">Activate editor</a>';
				$result.='</div>';
			}
			if($item['descr']){
				$result.='<div><span class="form-description">'.$item['descr'].'</span></div>';
			}
			$result.='</div>';
		}
		if('number'==$item['type']){
			$result.='<div class="grid-full-wrapper">';
			$min=false;
			$max=false;
			if(isset($item['min'])){
				$min=(int)$item['min'];
			}
			if(isset($item['max'])){
				$max=(int)$item['max'];
			}
			$result.='<input type="number" name="'.$item_name.'" placeholder="'.($item['placeholder']?htmlspecialchars($item['placeholder']):htmlspecialchars($item['title'])).'" value="'.(isset($object[$item_name])?htmlspecialchars($object[$item_name]):'').'"'.($item['required']?' required':'').($min?' min="'.$min.'"':'').($max?' max="'.$max.'"':'').(false===$item['editable']?' disabled':'').'>';
			if(false!==$object){//if not edit mode
				if($item['title']){
					$result.=' &mdash; '.$item['title'];
				}
			}
			if($item['descr']){
				$result.='<div><span class="form-description">'.$item['descr'].'</span></div>';
			}
			$result.='</div>';
		}
		if('select'==$item['type']){
			//'type'=>['type'=>'select','title'=>'Address type','options'=>$types_arr,'caption_template'=>'{id} ({name})','required'=>true,'value'=>$new_address_type],

			$optional=false;
			if(isset($item['optional'])){
				$optional=$item['optional'];
			}
			$optional_value=-1;
			if(isset($item['optional_value'])){
				$optional_value=$item['optional_value'];
			}
			$optional_caption='[!] None selected';
			if(isset($item['optional_caption'])){
				$optional_caption=$item['optional_caption'];
			}

			$result.='<div class="grid-full-wrapper">'.$item['title'].':<br>';
			$result.='<select name="'.$item_name.'"'.($item['required']?' required':'').''.(false===$item['editable']?' disabled':'').'>';
			if($optional){
				$result.='<option value="'.$optional_value.'">'.htmlspecialchars($optional_caption).'</option>';
			}
			$items=0;
			foreach($item['options'] as $option_id=>$option_value){
				$items++;
				$class='';
				if(isset($item['classes'])){
					if(is_array($item['classes'])){
						if(isset($item['classes'][$option_id])){
							$class=htmlspecialchars($item['classes'][$option_id]);
						}
					}
				}
				$caption=$option_id;
				if(is_array($option_value)){
					if(isset($item['caption_template'])){
						$caption=ltmp($item['caption_template'],$option_value);
					}
					elseif(isset($item['caption_value'])){
						if(isset($option_value[$item['caption_value']])){
							$caption=$option_value[$item['caption_value']];
						}
					}
					else{
						if(isset($option_value['id'])){
							$caption=$option_value['id'];
						}
					}
				}
				else{
					$caption=$option_value;
				}

				$value=$option_id;
				if(isset($item['key'])){
					if(isset($option_value[$item['key']])){
						$value=$option_value[$item['key']];
					}
				}

				if(!isset($item['default_option'])){
					$item['default_option']=false;
				}
				$selected_option=false;
				if(isset($item['default_option'])){
					if($item['default_option']==$value){
						$selected_option=true;
					}
				}
				if(false!==$object){
					//need check item in object, for reset selected option, if not found then will be used default_option
					if(isset($object[$item_name])){
						$selected_option=false;//expect find selected in foreach
						if($object[$item_name]==$value){
							$selected_option=true;
						}
					}
				}
				if(isset($item['caption_function'])){
					if(is_callable($item['caption_function'])){
						$caption=$item['caption_function']($option_id,$option_value);
					}
				}
				$result.='<option value="'.$value.'"'.($class?' class="'.$class.'"':'').''.($selected_option?' selected':'').'>'.htmlspecialchars($caption).'</option>';
			}
			if(0==$items){
				$result.='<option value="-1">[!] None items</option>';
			}
			$result.='</select>';
			if($item['descr']){
				$result.='<div><span class="form-description">'.$item['descr'].'</span></div>';
			}
			$result.='</div>';
		}
		if(!$collapse){//not in collapse wrapper
			$result.='</div>';
		}
		if('end-collapse'==$item['type']){
			$collapse=false;
			$result.='</div>';
			$result.='</div>';
		}
		//close grid wrapper
		//$result.='</div>';
	}
	return $result;
}