<?php
//landing wrapper for https://gitlab.com/envelop/tickets/getpass-land
//fast changes with support images and css
if(!isset($path_array[2])){
	$path_array[2]='';
}
if('style.css'==$path_array[2]){
	//css
	header('Content-type: text/css');
	$css=file_get_contents($root_dir.'/landing/css/styles.min.css');
	$css=str_replace('url("../','url("/landing/',$css);
	$css=str_replace('url(../','url(/landing/',$css);
	print $css;
}
elseif('scripts.bundle.js'==$path_array[2]){
	//js
	header('Content-type: application/javascript');
	$js=file_get_contents($root_dir.'/landing/js/scripts.bundle.js');
	print $js;
}
else{
	$replace['head_addon']='';
	//each counter add to head addon
	foreach($replace['counters'] as $counter_name=>$counter_code){
		if($counter_code){
			$replace['head_addon'].=$counter_code;
		}
	}
	//html
	$html=file_get_contents($root_dir.'/landing/index.html');
	$html=str_replace('src="../','src="/landing/',$html);
	$html=str_replace('href="../','href="/landing/',$html);
	$html=str_replace('src="pics/','src="/landing/pics/',$html);
	$html=str_replace('href="pics/','href="/landing/pics/',$html);
	$html=str_replace('src="js/','src="/landing/js/',$html);
	$html=str_replace('href="js/','href="/landing/js/',$html);
	$html=str_replace('src="css/','src="/landing/css/',$html);
	$html=str_replace('href="css/','href="/landing/css/',$html);
	$html=str_replace('</head>',$replace['head_addon'].'</head>',$html);

	//change logo to new
	$html=str_replace('<img src="/landing/pics/getpass-logo.svg" alt="">','<img src="/getpass-new-logo-teal-white.svg" alt="">',$html);

	//https://gitlab.com/envelop/tickets/getpass-land/-/issues/18
	if($auth['addresses']>0){
		$html=str_replace('<a class="btn" href="https://getpass.is/login/" target="_blank"> <img src="/landing/pics/i-login.svg" alt="#"><span>Login</span></a>','<a class="btn" href="https://getpass.is/profile/" target="_blank"> <img src="/landing/pics/i-login.svg" alt="#"><span>Profile</span></a>',$html);
	}
	else{
		$html=str_replace('<a class="btn" href="https://getpass.is/login/" target="_blank"> <img src="/landing/pics/i-login.svg" alt="#"><span>Login</span></a>','<a class="btn" href="https://getpass.is/login/" target="_blank"> <img src="/landing/pics/i-login.svg" alt="#"><span>Login</span></a>',$html);
	}
	$search='<div class="old-price"> <span>0.005 DAI</span></div>';
	$newstr='<div class="old-price"> <span>100 DAI</span></div>';
	$pos=strpos($html,$search);
	if(false!==$pos){
		$html=substr_replace($html,$newstr,$pos,strlen($search));
	}

	$search='<div class="old-price"> <span>0.005 DAI</span></div>';
	$newstr='<div class="old-price"> <span>10 DAI</span></div>';
	$pos=strpos($html,$search);
	if(false!==$pos){
		$html=substr_replace($html,$newstr,$pos,strlen($search));
	}

	$html=str_replace('<button class="btn btn-outline">Buy for 0.002 DAI</button>','<button class="btn btn-outline">Unavailable now</button>',$html);

	$html=str_replace('<title> </title>','<title>'.htmlspecialchars($config['landing_title']).'</title>',$html);
	$html=str_replace('<meta name="description" content="Create new pattern events using programmable assets such as wrap NFT">','<meta name="description" content="'.htmlspecialchars($config['landing_descr']).'">',$html);
	$html=str_replace('</head>','
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@Envelop_project">
<meta name="twitter:creator" content="@Envelop_project">
<meta name="twitter:title" content="'.htmlspecialchars($config['landing_title']).'">
<meta name="twitter:description" content="'.htmlspecialchars($config['landing_descr']).'">
<meta name="twitter:image" content="/meta-image.jpg">
</head>',$html);
	print $html;
}
exit;