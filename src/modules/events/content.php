<?php
ob_start();
$replace['title']='Blog'.' | '.$replace['title'];

if($path_array[4]){
	if('subscribe'==$path_array[4]){
		if($auth['default_address_id']){
			if(check_csrf()){
				$mailing_rule_arr=$db->sql_row("SELECT * FROM `mailing_rules` WHERE `address`='".$auth['default_address_id']."' AND `type`='2' AND `target`='".$event_id."' LIMIT 1");
				if(null===$mailing_rule_arr){
					$db->sql("INSERT INTO `mailing_rules` (`type`,`target`,`address`,`rule`,`time`) VALUES ('2','".$event_id."','".$auth['default_address_id']."','1','".time()."')");
				}
				else{
					$db->sql("UPDATE `mailing_rules` SET `rule`='1', `time`='".time()."' WHERE `id`='".$mailing_rule_arr['id']."'");
				}
				header('Location: /@'.$organizer_url.'/'.$event_url.'/content/?success=subscribe');
			}
			else{
				header('Location: /@'.$organizer_url.'/'.$event_url.'/content/?error=csrf');
			}
		}
		else{
			header('Location: /@'.$organizer_url.'/'.$event_url.'/content/');
		}
	}
	else
	if('unsubscribe'==$path_array[4]){
		if($auth['default_address_id']){
			if(check_csrf()){
				$mailing_rule_arr=$db->sql_row("SELECT * FROM `mailing_rules` WHERE `address`='".$auth['default_address_id']."' AND `type`='2' AND `target`='".$event_id."' LIMIT 1");
				if(null===$mailing_rule_arr){
					$db->sql("INSERT INTO `mailing_rules` (`type`,`target`,`address`,`rule`,`time`) VALUES ('2','".$event_id."','".$auth['default_address_id']."','2','".time()."')");
				}
				else{
					$db->sql("UPDATE `mailing_rules` SET `rule`='2', `time`='".time()."' WHERE `id`='".$mailing_rule_arr['id']."'");
				}
				header('Location: /@'.$organizer_url.'/'.$event_url.'/content/?success=unsubscribe');
			}
			else{
				header('Location: /@'.$organizer_url.'/'.$event_url.'/content/?error=csrf');
			}
		}
		else{
			header('Location: /@'.$organizer_url.'/'.$event_url.'/content/');
		}
	}
	else{
		$content_url=urldecode($path_array[4]);
		if($allow_event_manage){//event management
			$sql_addon_str=" AND `status`<='1'";//only planned & published
		}
		else{
			$sql_addon_str=" AND `status`='1'";//only published
		}

		$content_item=$db->sql_row("SELECT * FROM `event_content` WHERE `event`='".$event_id."' AND `url`='".$db->prepare($content_url)."'".$sql_addon_str);
		if(null!==$content_item){
			//set meta tags
			$replace['title']=htmlspecialchars($content_item['title']).' | '.$replace['title'];
			if(''!=$content_item['description']){
				$replace['description']=$content_item['description'];
				$replace['description']=strip_tags($replace['description']);
				$replace['description']=str_replace(array("\r\n","\n","\r"),' ',$replace['description']);
				$replace['description']=preg_replace('/\s+/',' ', $replace['description']);
				$replace['description']=substr($replace['description'],0,160);
				$replace['description'].='...';
				$replace['description']=htmlspecialchars($replace['description']);
			}

			$replace['head_addon'].=PHP_EOL.'<meta prefix="og:http://ogp.me/ns#">';
			$replace['head_addon'].=PHP_EOL.'<meta property="og:title" content="'.htmlspecialchars($content_item['title']).'">';
			$replace['head_addon'].=PHP_EOL.'<meta property="og:description" content="'.htmlspecialchars($content_item['description']).'">';
			if(''!=$content_item['cover_url']){
				$replace['head_addon'].=PHP_EOL.'<meta property="og:image" content="'.$config['platform_url'].$content_item['cover_url'].'">';
			}
			$replace['head_addon'].=PHP_EOL.'<meta property="og:url" content="'.$config['platform_url'].'/@'.$organizer_url.'/'.$event_url.'/content/'.$content_item['url'].'/">';
			$replace['head_addon'].=PHP_EOL.'<meta property="og:type" content="article">';
			$replace['head_addon'].=PHP_EOL.'<meta property="og:site_name" content="'.$config['platform_domain'].'">';
			$replace['head_addon'].=PHP_EOL.'<meta property="article:published_time" content="'.date('c',$content_item['time']).'">';
			$replace['head_addon'].=PHP_EOL.'<meta property="article:modified_time" content="'.date('c',$content_item['time']).'">';
			$replace['head_addon'].=PHP_EOL.'<meta property="article:section" content="Blog">';
			$replace['head_addon'].=PHP_EOL.'<meta property="og:locale" content="en_US">';
			$replace['head_addon'].=PHP_EOL.'<meta property="og:locale:alternate" content="ru_RU">';
			$replace['head_addon'].=PHP_EOL.'<meta name="twitter:card" content="summary"></meta>';


			//$replace['head_addon'].='<meta property="article:tag" content="">';

			if(0==$content_item['status']){
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Notice</p>';
				if($content_item['time']){
					print '<p>This content is planned to be published at <span class="time" data-time="'.$content_item['time'].'">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>. <a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/edit/'.$content_item['id'].'/">Click to edit.</a></p>';
				}
				else{
					print '<p>This content is in drafts, you can schedule it to be published. <a href="/@'.$organizer_url.'/'.$event_url.'/manage/content/edit/'.$content_item['id'].'/">Click to edit.</a></p>';
				}
				print '</div>';
			}

			$is_allowed=false;
			if($allow_event_manage){
				$is_allowed=true;
			}
			if(isset($event_user['level'])){
				if($content_item['level']<=$event_user['level']){
					$is_allowed=true;
				}
			}
			else{
				if(0==$content_item['level']){//public for guests
					$is_allowed=true;
				}
			}

			if(!$is_allowed){
				//http_response_code(403);
				print '<h1>'.htmlspecialchars($content_item['title']).'</h1>';
				$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `level`='".$content_item['level']."'");

				print '<div class="attention-box" role="alert">
					<p class="font-bold">Access denied</p>';
					if($level_arr){
						print '<p>Content only for participants with level: '.htmlspecialchars($level_arr['caption']).' ('.htmlspecialchars($level_arr['name']).'), please buy the ticket or return to <a href="/@'.$organizer_url.'/'.$event_url.'/content/">event blog</a>.</p>';
					}
				print '
				</div>';

				if($event_arr['tickets_description']){
					$clear_tickets_description=clear_html_tags($event_arr['tickets_description']);
					print '<div class="content-wrapper">';
					print $clear_tickets_description;
					print '</div>';
				}
			}
			else{
				$clear_content=clear_html_tags($content_item['content']);
				$pinned=(1==$content_item['pin']);

				$tags=$db->sql("SELECT `tag` FROM `event_content_tags` WHERE `event`='".$event_id."' AND `content`='".$content_item['id']."'");
				$tags_str='';
				foreach($tags as $tag){
					$tag_arr=$db->sql_row("SELECT * FROM `event_tags` WHERE `event`='".$event_id."' AND `id`='".$tag['tag']."'");
					$tags_str.='<a class="badge bg-tag" href="/@'.$organizer_url.'/'.$event_url.'/content/?tag='.$tag_arr['url'].'">'.$tag_arr['caption'].'</a> ';
				}
				$tags_str=trim($tags_str);

				print '<h1>'.htmlspecialchars($content_item['title']);
				if($pinned){
					print '<span class="content-pinned" title="Pinned content">'.$ltmp['icons']['starred'].'</span>';
				}
				print '</h1>';
				print '<div class="content-card article single">';
				print '<div class="content-wrapper">';
				print $clear_content;
				print '</div>';
				print '<div class="content-info">';
					print '<div class="content-time" data-datetime="'.$content_item['time'].'">';
					print '<div class="content-global-time">
					<span class="content-datetime">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>
					</div>';
					print '<div class="content-local-time">
					<span class="content-datetime"></span>
					</div>';
					print '</div>';
					print '<div class="content-tags">'.$tags_str.'</div>';
				print '</div>';
				print '</div>';
				print '<hr class="my-4">';
				print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/content/">&larr; Back to blog</a>';
			}
		}
		else{
			header("HTTP/1.0 404 Not Found");
			print '<h1>404 Not Found</h1>';
			print '<p>Content not found, please return to <a href="/@'.$organizer_url.'/'.$event_url.'/content/">event blog</a>.</p>';
		}
	}
}
else{
	$filter_tag=false;
	$filter_tag_arr=null;
	if(isset($_GET['tag'])){
		if(''!==$_GET['tag']){
			$filter_tag=$_GET['tag'];
			$filter_tag_arr=$db->sql_row('SELECT * FROM `event_tags` WHERE `event`='.$event_id.' AND `url`=\''.$db->prepare($_GET['tag']).'\'');
			if(null==$filter_tag_arr){
				$filter_tag=false;
			}
			else{
				$filter_tag=$filter_tag_arr['id'];
			}
		}
	}

	print '<h1>Blog'.(null!=$filter_tag_arr?' #'.htmlspecialchars($filter_tag_arr['url']).'':'').'</h1>';
	print '<hr class="my-4">';
	if(null!=$filter_tag_arr){
		print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/content/">&larr; Back to blog</a>';
	}
	if($auth['default_address_id']){
		$mailing_rule_arr=$db->sql_row("SELECT * FROM `mailing_rules` WHERE `address`='".$auth['default_address_id']."' AND `type`='2' AND `target`='".$event_id."'");
		$mailing_rule=0;
		if(isset($mailing_rule_arr['rule'])){
			$mailing_rule=$mailing_rule_arr['rule'];
		}
		if(1!=$mailing_rule){
			print '<a class="action-btn subscribe-action-button" href="/@'.$organizer_url.'/'.$event_url.'/content/subscribe/?'.gen_csrf_param().'">'.$ltmp['icons']['subscribe'].' Subscribe</a>';
		}
		if(2!=$mailing_rule){
			print '<a class="action-btn redo unsubscribe-action-button" href="/@'.$organizer_url.'/'.$event_url.'/content/unsubscribe/?'.gen_csrf_param().'">'.$ltmp['icons']['unsubscribe'].' Unsubscribe</a>';
		}
		if(isset($_GET['error'])){
			if('csrf'==$_GET['error']){
				print '
				<div class="attention-box" role="alert">
					<p>⚠️ CSRF is invalid, try again.</p>
				</div>';
			}
		}
		if(isset($_GET['success'])){
			if('subscribe'==$_GET['success']){
				print '
				<div class="success-box" role="alert">
					<p><i class="icon positive">'.$ltmp['icons']['circle-check'].'</i> You have successfully subscribed to this event.</p>
				</div>';
			}
			if('unsubscribe'==$_GET['success']){
				print '
				<div class="attention-box" role="alert">
					<p>You have successfully unsubscribed from this event.</p>
				</div>';
			}
		}
	}

	$sql_addon=[];
	$sql_addon[]="`event_content`.`event`='".$event_id."'";
	if($allow_event_manage){//event management
		$sql_addon[]="`event_content`.`status`<='1'";//only planned & published
	}
	else{
		$sql_addon[]="`event_content`.`status`='1'";//only published
	}

	$sql_addon_str='';
	if(count($sql_addon)>0){
		$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
	}

	if(false!==$filter_tag){
		$sql_addon_str=' RIGHT JOIN `event_content_tags` ON `event_content_tags`.`content`=`event_content`.`id` AND `event_content_tags`.`tag`='.$filter_tag.' '.$sql_addon_str;
	}

	$per_page=10;
	$count=$db->table_count('event_content',$sql_addon_str);
	$pages_count=ceil($count/$per_page);
	$page=1;
	if(isset($_GET['page'])){
		$page=(int)$_GET['page'];
		if($page<1){
			$page=1;
		}
		elseif($page>$pages_count){
			$page=$pages_count;
		}
	}

	$content_counter=0;
	$content=$db->sql("SELECT `event_content`.* FROM `event_content` ".$sql_addon_str." ORDER BY `event_content`.`time` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
	foreach($content as $content_item){
		$content_counter++;

		$content_html=clear_html_tags($content_item['content']);
		$pinned=(1==$content_item['pin']);

		$tags=$db->sql("SELECT `tag` FROM `event_content_tags` WHERE `event`='".$event_id."' AND `content`='".$content_item['id']."'");
		$tags_str='';
		foreach($tags as $tag){
			$tag_arr=$db->sql_row("SELECT * FROM `event_tags` WHERE `event`='".$event_id."' AND `id`='".$tag['tag']."'");
			$tags_str.='<a class="badge bg-tag" href="/@'.$organizer_url.'/'.$event_url.'/content/?tag='.$tag_arr['url'].'">'.$tag_arr['caption'].'</a> ';
		}
		$tags_str=trim($tags_str);

		print '<div class="content-card'.(0==$content_item['status']?' draft':'').'">';
			if($content_item['cover_url']){
				print '<div class="content-cover mb-4 md:mr-6">';
				print '<a href="/@'.$organizer_url.'/'.$event_url.'/content/'.$content_item['url'].'/">';
				print '<img src="'.$content_item['thumbnail_cover_url'].'" alt="'.htmlspecialchars($content_item['title']).'">';
				print '</a>';
				print '</div>';
			}
			print '<div class="content-info">';
				print '<div class="content-data">';
					print '<div class="content-title">';
					print '<a href="/@'.$organizer_url.'/'.$event_url.'/content/'.$content_item['url'].'/">'.htmlspecialchars($content_item['title']).'</a>';
						if($pinned){
							print '<span class="content-pinned" title="Pinned content">'.$ltmp['icons']['starred'].'</span>';
						}
					print '</div>';
					print '<div class="content-description">'.htmlspecialchars($content_item['description']).'</div>';
				print '</div>';
				print '<div class="content-time" data-datetime="'.$content_item['time'].'">';
				print '<div class="content-global-time">
				<span class="content-datetime">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>
				</div>';
				print '<div class="content-local-time">
				<span class="content-datetime"></span>
				</div>';
				print '</div>';
				print '<div class="content-tags">'.$tags_str.'</div>';
			print '</div>';
		print '</div>';
	}
	if(0==$content_counter){
		print '<p>No content was found</p>';
	}

	print '<div class="pagination">';
	//get string with all GET params except page
	$get_params=[];
	foreach($_GET as $get_param_name=>$get_param_value){
		if('page'!==$get_param_name){
			$get_params[]=$get_param_name.'='.urlencode($get_param_value);
		}
	}
	$get_params_str='';
	if(count($get_params)){
		$get_params_str=implode('&',$get_params);
	}
	if($page>1){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
	}
	for($i=1;$i<=$pages_count;$i++){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
	}
	if($page<$pages_count){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
	}
	print '</div>';
}
$event_module_content=ob_get_contents();
ob_end_clean();