<?php
ob_start();
$replace['title']='Agenda'.' | '.$replace['title'];

$time=0;
$endtime=0;
if(isset($_GET['time'])){
	$time=$_GET['time'];
	$endtime=$time+60*60*24;//one day
}

$time_arr=[];
$sessions_first=$db->sql_row("SELECT * FROM `event_sessions` WHERE `event`='".$event_id."' AND `status`!='2' ORDER BY `time` ASC LIMIT 1");
if(null!=$sessions_first){
	$sessions_last=$db->sql_row("SELECT * FROM `event_sessions` WHERE `event`='".$event_id."' AND `status`!='2' ORDER BY `time` DESC LIMIT 1");
	$date_first=date('d.m.Y',$sessions_first['time']);
	$date_last=date('d.m.Y',$sessions_last['time']);
	$date_first_arr=explode('.',$date_first);
	$date_last_arr=explode('.',$date_last);
	$time_first=mktime(0,0,0,$date_first_arr[1],$date_first_arr[0],$date_first_arr[2]);
	$time_last=mktime(0,0,0,$date_last_arr[1],$date_last_arr[0],$date_last_arr[2]);
	for($i=$time_first;$i<=$time_last;$i+=60*60*24){
		$sessions_count=$db->table_count('event_sessions',"WHERE `event`='".$event_id."' AND `status`!='2' AND `time`>='".$i."' AND `time`<'".($i+60*60*24)."'");
		if($sessions_count){
			$time_arr[$i]=date('d.m.Y',$i);
		}
	}
}
print '<h1>Agenda</h1>';
print '<hr class="my-4">';
print '<div class="my-4">';
print '<ul class="submenu">';
print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/agenda/"'.(0==$time?' class="selected"':'').'>All days</a></li>';
foreach($time_arr as $unixtime => $date_str){
	print '<li><a href="/@'.$organizer_url.'/'.$event_url.'/agenda/?time='.$unixtime.'"'.($unixtime==$time?' class="selected"':'').'>'.$date_str.'</a></li>';
}
print '</ul>';
print '</div>';

print '<hr class="my-4">';

$filter_location=0;
if(isset($_GET['location'])){
	$_GET['location']=(int)$_GET['location'];
	if($db->table_count('event_locations',"WHERE `event`='".$event_id."' AND `id`='".$_GET['location']."'")){
		$filter_location=$_GET['location'];
	}
}
$filter_location_arr=[];
$filter_location_arr[0]='All locations';
$locations=$db->sql("SELECT * FROM `event_locations` WHERE `event`='".$event_id."' AND `status`!=1 ORDER BY `caption` ASC");
foreach($locations as $location){
	$filter_location_arr[$location['id']]=$location['caption'];
}
$filter_caption=false;
if(isset($_GET['caption'])){
	if(''!==$_GET['caption']){
		$filter_caption=trim($_GET['caption']);
	}
}

print '
<form action="" method="GET">
<div class="filters-wrapper">';
if($time){
	print '<input type="hidden" name="time" value="'.$time.'">';
}
print '
	<div>
		<input name="caption" class="form-post-by-enter" placeholder="Search" value="'.($filter_caption?htmlspecialchars($filter_caption):'').'">
	</div>';
if(1<count($filter_location_arr)){
	print '
		<div>
			<select name="location" onchange="$(this).closest(\'form\')[0].submit()">';
	foreach($filter_location_arr as $location_id => $location_name){
		print '<option value="'.$location_id.'"'.($location_id==$filter_location?' selected':'').'>'.htmlspecialchars($location_name).'</option>';
	}
	print '
	</select>
</div>';
}
print '
</div>
</form>';

print '<hr class="my-4">';

$sql_addon=[];
if($time){
	$sql_addon[]="`time`>='".$time."'";
	$sql_addon[]="`time`<'".$endtime."'";
}
if($filter_location){
	$sql_addon[]="`location`='".$filter_location."'";
}
if($filter_caption){
	$sql_addon[]="`caption` LIKE '%".$db->prepare($filter_caption)."%'";
}
$sql_addon_str='';
if(count($sql_addon)){
	$sql_addon_str=' AND '.implode(' AND ',$sql_addon);
}

$last_date='';
$sessions_counter=0;
$sessions=$db->sql("SELECT * FROM `event_sessions` WHERE `event`='".$event_id."' AND `status`!=2".$sql_addon_str." ORDER BY `time` ASC");
foreach($sessions as $session_item){
	$session_date=date('d.m.Y',$session_item['time']);
	if($session_date!=$last_date){
		if($last_date){
			print '</div>';
		}
		print '<h2>'.$session_date.'</h2>';
		print '<div class="my-4">';
		$last_date=$session_date;
	}
	$sessions_counter++;
	$location=$db->sql_row("SELECT * FROM `event_locations` WHERE `event`='".$event_id."' AND `id`='".$session_item['location']."'");
	$starred=(1==$session_item['status']);
	print '<div class="session-card">';
		if($session_item['cover_url']){
			print '<div class="session-cover mb-4 md:mr-6">';
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/sessions/'.$session_item['id'].'/">';
			print '<img src="'.$session_item['thumbnail_cover_url'].'" alt="'.htmlspecialchars($session_item['caption']).'">';
			print '</a>';
			print '</div>';
		}
		print '<div class="session-info">';
			print '<div class="session-time" data-start-time="'.$session_item['time'].'" data-end-time="'.$session_item['endtime'].'" data-duration="'.$session_item['duration'].'">';
				print '<div class="session-global-time">';
					print '<span class="session-date"><span class="bold">'.date('d.m.Y',$session_item['time']).'</span> GMT</span>';
					print '<span class="session-time-range"><span class="bold">'.date('H:i',$session_item['time']).' &ndash; '.date('H:i',$session_item['endtime']).'</span>';
				print '</div>';
				print '<div class="session-local-time">';
					print '
					<span class="session-date"></span>
					<span class="session-time-range"><span class="bold"> &ndash; </span></span>';
				print '</div>';
				print '<span class="session-duration">('.$session_item['duration'].' Min)</span></span>';
			print '</div>';
			print '<div class="session-info">';
			print '<div class="session-caption">';
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/sessions/'.$session_item['id'].'/">'.htmlspecialchars($session_item['caption']).'</a>';
				if($starred){
					print '<span class="session-starred" title="Starred session">'.$ltmp['icons']['starred'].'</span>';
				}
				print '</div>';
				//print '<div class="session-description">'.htmlspecialchars($session_item['description']).'</div>';
				if($location){
					if($location['caption']){
						print '<div class="session-location">
						'.$ltmp['icons']['location'].'
						'.$location['caption'].'</div>';
					}
				}
			print '</div>';
			print '<div class="session-speakers-list-scroll-wrapper">';
			print '<div class="session-speakers-list">';
			$speakers=$db->sql("SELECT `es`.*
				FROM `event_sessions_speakers` as `ess`
				INNER JOIN `event_speakers` as `es`
				ON `es`.`event`='".$event_id."' AND `es`.`status`!=2 AND `ess`.`speaker`=`es`.`id`
				WHERE `ess`.`session`='".$session_item['id']."'
				ORDER BY `es`.`status` DESC, `es`.`sort` ASC, `es`.`public_name` ASC");
			while($speaker=$db->row($speakers)){
				$public_name_url=ru2lat($speaker['public_name']);
				$public_name_url=str_replace(' ','-',$public_name_url);
				$public_name_url=preg_replace('~[^a-z0-9\-_]+~iUs','',$public_name_url);
				$public_name_url=preg_replace('~\-+~iUs','-',$public_name_url);
				$public_name_url=preg_replace('~\-+$~iUs','',$public_name_url);
				$public_name_url=preg_replace('~^-+~iUs','',$public_name_url);
				$public_name_url=strtolower($public_name_url);

				$speaker_starred=(1==$speaker['status']);

				print '<a class="speaker" href="/@'.$organizer_url.'/'.$event_url.'/speakers/'.$speaker['id'].'-'.$public_name_url.'/">';
					if($speaker['photo_url']){
						print '<div class="speaker-photo'.(isset($customize['avatar_style'])?' '.$avatar_style[$customize['avatar_style']]['class_addon']:'').'"><img src="'.$speaker['small_photo_url'].'" alt="speaker"></div>';
					}
					else{
						print '<div class="speaker-photo'.(isset($customize['avatar_style'])?' '.$avatar_style[$customize['avatar_style']]['class_addon']:'').'"><img src="/speaker.svg" alt="speaker"></div>';
					}

					print '<div class="speaker-short-info">';
						print '<div class="speaker-public-name">'.$speaker['public_name'];
						if($speaker_starred){
							print '<span class="speaker-starred" title="Starred speaker">'.$ltmp['icons']['starred'].'</span>';
						}
						print '</div>';
						print '<div class="speaker-nick-name">'.$speaker['nick_name'].'</div>';
						print '<div class="speaker-title">'.$speaker['title'].'</div>';
						print '<div class="speaker-company">'.$speaker['company'].'</div>';
					print '</div>';
				print '</a>';
			}
			print '</div>';
			print '</div>';
		print '</div>';
	print '</div>';
}
if(0==$sessions_counter){
	print '<p>No active sessions.</p>';
}
$event_module_content=ob_get_contents();
ob_end_clean();