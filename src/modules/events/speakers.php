<?php
ob_start();
$replace['title']='Speakers'.' | '.$replace['title'];

if($path_array[4]){
	$search_speaker_id=(int)$path_array[4];
	$speaker=$db->sql_row("SELECT * FROM `event_speakers` WHERE `event`='".$event_id."' AND `id`='".$search_speaker_id."' AND `status`!=2");
	if(null!==$speaker){
		$public_name_url=ru2lat($speaker['public_name']);
		$public_name_url=str_replace(' ','-',$public_name_url);
		$public_name_url=preg_replace('~[^a-z0-9\-_]+~iUs','',$public_name_url);
		$public_name_url=preg_replace('~\-+~iUs','-',$public_name_url);
		$public_name_url=preg_replace('~\-+$~iUs','',$public_name_url);
		$public_name_url=preg_replace('~^-+~iUs','',$public_name_url);
		$public_name_url=strtolower($public_name_url);

		$speaker_starred=(1==$speaker['status']);

		if(($speaker['id'].'-'.$public_name_url)!=$path_array[4]){
			header('Location: /@'.$organizer_url.'/'.$event_url.'/speakers/'.$speaker['id'].'-'.$public_name_url.'/');
			exit;
		}
		$replace['title']=htmlspecialchars($speaker['public_name']).' | '.$replace['title'];
		$replace['description']=$speaker['public_name'];
		if($speaker['title']){
			$replace['description'].=' ('.$speaker['title'].')';
		}
		if($speaker['company']){
			$replace['description'].=' from '.$speaker['company'];
		}
		if($speaker['description']){
			$replace['description'].=' - '.$speaker['description'];
		}
		$replace['description']=strip_tags($replace['description']);
		$replace['description']=str_replace(array("\r\n","\n","\r"),' ',$replace['description']);
		$replace['description']=preg_replace('/\s+/', ' ', $replace['description']);
		$replace['description']=substr($replace['description'],0,160);
		$replace['description'].='...';
		$replace['description']=htmlspecialchars($replace['description']);

		print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/speakers/">&larr; Back to speakers</a>';
		if($allow_event_manage){
			print '<a class="action-btn configure" href="/@'.$organizer_url.'/'.$event_url.'/manage/speakers/edit/'.$speaker['id'].'/">Edit speaker</a>';
		}
		print '<div class="speaker-card">';
		print '<div class="speaker-photo mb-4 md:mr-6'.(isset($customize['avatar_style'])?' '.$avatar_style[$customize['avatar_style']]['class_addon']:'').'">';
		if($speaker['photo_url']){
			print '<a href="'.$speaker['photo_url'].'" target="_blank"><img src="'.$speaker['small_photo_url'].'" alt="'.htmlspecialchars($speaker['public_name']).'"></a>';
		}
		else{
			print '<img src="/speaker.svg" width="150" alt="'.$speaker['public_name'].'">';
		}
		print '</div>';
		print '<div class="speaker-info">';

		$speaker_addon=false;
		if($speaker['addon']){
			$speaker_addon_arr=json_decode($speaker['addon'],true);
			if(isset($speaker_addon_arr['linkedin'])){
				$speaker_addon.='<div class="speaker-addon-linkedin"><a href="https://linkedin.com/in/'.htmlspecialchars($speaker_addon_arr['linkedin']).'/" target="_blank">
				'.$ltmp['icons']['linkedin'].'
				</a></div>';
			}
			if(isset($speaker_addon_arr['twitter'])){
				$speaker_addon.='<div class="speaker-addon-twitter"><a href="https://twitter.com/'.htmlspecialchars($speaker_addon_arr['twitter']).'" target="_blank">
				'.$ltmp['icons']['twitter'].'
				</a></div>';
			}
			if(isset($speaker_addon_arr['telegram'])){
				$speaker_addon.='<div class="speaker-addon-telegram"><a href="https://t.me/'.htmlspecialchars($speaker_addon_arr['telegram']).'" target="_blank">
				'.$ltmp['icons']['telegram'].'
				</a></div>';
			}
			if(isset($speaker_addon_arr['website'])){
				$speaker_addon.='<div class="speaker-addon-website"><a href="'.htmlspecialchars($speaker_addon_arr['website']).'" target="_blank">
				'.$ltmp['icons']['website'].'
				</a></div>';
			}
		}
		print '<div class="session-info-wrapper">';
		print '<h1 class="speaker-public-name">'.$speaker['public_name'];
		if($speaker_starred){
			print '<span class="speaker-starred" title="Starred speaker">'.$ltmp['icons']['starred'].'</span>';
		}
		print '</h1>';
		if(false!==$speaker_addon){
			print '<div class="speaker-addons">';
			print $speaker_addon;
			print '</div>';
		}
		print '</div>';
		if($speaker['nick_name']){
			print '<div class="speaker-nick-name">'.htmlspecialchars($speaker['nick_name']).'</div>';
		}
		if($speaker['title']){
			print '<div class="speaker-title">'.htmlspecialchars($speaker['title']).'</div>';
		}
		if($speaker['company']){
			print '<div class="speaker-company">'.htmlspecialchars($speaker['company']).'</div>';
		}
		if($speaker['position']){
			print '<div class="speaker-position">'.htmlspecialchars($speaker['position']).'</div>';
		}
		if($speaker['description']){
			print '<div class="speaker-description">';
			print '<div class="content-wrapper simple">';
			$clear_speaker_description=clear_html_tags($speaker['description']);
			print $clear_speaker_description;
			print '</div>';
			print '</div>';
		}
		print '</div>';
		print '</div>';

		if(0!=$db->table_count('event_sessions_speakers',"WHERE `event`='".$event_id."' AND `speaker`='".$speaker['id']."'")){
			print '<hr>';
			print '<h2>Sessions</h2>';
			$sessions=$db->sql("SELECT `es`.*
			FROM `event_sessions_speakers` as `ess`
			INNER JOIN `event_sessions` as `es`
				ON `es`.`event`='".$event_id."' AND `es`.`status`!=2 AND `ess`.`session`=`es`.`id`
			WHERE `ess`.`speaker`='".$speaker['id']."'
			ORDER BY `es`.`endtime` DESC, `es`.`time` ASC");
			foreach($sessions as $session_item){
				$location=$db->sql_row("SELECT * FROM `event_locations` WHERE `event`='".$event_id."' AND `id`='".$session_item['location']."'");
				$starred=$session_item['status'];
				print '<div class="session-card">';
					if($session_item['cover_url']){
						print '<div class="session-cover mb-4 md:mr-6">';
						print '<a href="/@'.$organizer_url.'/'.$event_url.'/sessions/'.$session_item['id'].'/">';
						print '<img src="'.$session_item['thumbnail_cover_url'].'" alt="'.htmlspecialchars($session_item['caption']).'">';
						print '</a>';
						print '</div>';
					}
					print '<div class="session-info">';
						print '<div class="session-time" data-start-time="'.$session_item['time'].'" data-end-time="'.$session_item['endtime'].'" data-duration="'.$session_item['duration'].'">';
							print '<div class="session-global-time">';
								print '<span class="session-date"><span class="bold">'.date('d.m.Y',$session_item['time']).'</span> GMT</span>';
								print '<span class="session-time-range"><span class="bold">'.date('H:i',$session_item['time']).' &ndash; '.date('H:i',$session_item['endtime']).'</span>';
							print '</div>';
							print '<div class="session-local-time">';
								print '
								<span class="session-date"></span>
								<span class="session-time-range"><span class="bold"> &ndash; </span></span>';
							print '</div>';
							print '<span class="session-duration">('.$session_item['duration'].' Min)</span></span>';
						print '</div>';
						print '<div class="session-info">';
							print '<div class="session-caption">';
							print '<a href="/@'.$organizer_url.'/'.$event_url.'/sessions/'.$session_item['id'].'/">'.htmlspecialchars($session_item['caption']).'</a>';
							if($starred){
								print '<span class="session-starred" title="Starred session">'.$ltmp['icons']['starred'].'</span>';
							}
							print '</div>';
							print '<div class="session-description">'.htmlspecialchars($session_item['description']).'</div>';
							if($location){
								print '<div class="session-location">
							'.$ltmp['icons']['location'].'
							'.$location['caption'].'</div>';
							}
						print '</div>';
					print '</div>';
				print '</div>';
			}
		}

	}
	else{
		header("HTTP/1.0 404 Not Found");
		print '<h1>404 Not Found</h1>';
		print '<p>Speaker not found, please return to <a href="/@'.$organizer_url.'/'.$event_url.'/speakers/">our speakers</a> list.</p>';
	}
}
else{
	print '<h1 class="center">Our Speakers</h1>';
	print '<hr class="my-4">';

	$filter_company=false;
	if(isset($_GET['company'])){
		if(''!==$_GET['company']){
			if($db->table_count('event_speakers',"WHERE `event`='".$event_id."' AND `status`!=2 AND `company`='".$db->prepare($_GET['company'])."'")){
				$filter_company=$db->prepare($_GET['company']);
			}
		}
	}
	$filter_companies_arr=[];
	$companies=$db->sql("SELECT DISTINCT(`company`) as `company` FROM `event_speakers` WHERE `event`='".$event_id."' AND `status`!=2 ORDER BY `company` ASC");
	foreach($companies as $company){
		$filter_companies_arr[]=$company['company'];
	}
	$filter_name=false;
	if(isset($_GET['name'])){
		if(''!==$_GET['name']){
			$filter_name=trim($_GET['name']);
		}
	}

	print '
	<form action="" method="GET">
	<div class="filters-wrapper">';
	print '
		<div>
			<input name="name" class="form-post-by-enter" placeholder="Search" value="'.($filter_name?htmlspecialchars($filter_name):'').'">
		</div>';
	print '
		<div>
			<select name="company" onchange="$(this).closest(\'form\')[0].submit()">';
	print '<option value=""'.(false==$filter_company?' selected':'').'>Filter by company</option>';
	foreach($filter_companies_arr as $company){
		print '<option value="'.htmlspecialchars($company).'"'.($company==$filter_company?' selected':'').'>'.htmlspecialchars($company).'</option>';
	}
				print '
			</select>
		</div>';
	print '
	</div>
	</form>';

	print '<hr class="my-4">';

	$sql_addon=[];
	if($filter_company){
		$sql_addon[]="`company`='".$filter_company."'";
	}
	if($filter_name){
		$sql_addon[]="`public_name` LIKE '%".$db->prepare($filter_name)."%'";
	}
	$sql_addon_str='';
	if(count($sql_addon)){
		$sql_addon_str=' AND '.implode(' AND ',$sql_addon);
	}

	print '<div class="speakers-list center">';
	$speakers=$db->sql("SELECT * FROM `event_speakers` WHERE `event`='".$event_id."' AND `status`!=2".$sql_addon_str." ORDER BY `status` DESC, `sort` DESC, `public_name` ASC");
	while($speaker=$db->row($speakers)){
		$public_name_url=ru2lat($speaker['public_name']);
		$public_name_url=str_replace(' ','-',$public_name_url);
		$public_name_url=preg_replace('~[^a-z0-9\-_]+~iUs','',$public_name_url);
		$public_name_url=preg_replace('~\-+~iUs','-',$public_name_url);
		$public_name_url=preg_replace('~\-+$~iUs','',$public_name_url);
		$public_name_url=preg_replace('~^-+~iUs','',$public_name_url);
		$public_name_url=strtolower($public_name_url);

		$speaker_starred=(1==$speaker['status']);

		print '<a class="speaker" href="/@'.$organizer_url.'/'.$event_url.'/speakers/'.$speaker['id'].'-'.$public_name_url.'/">';
		if($speaker['photo_url']){
			print '<div class="speaker-photo'.(isset($customize['avatar_style'])?' '.$avatar_style[$customize['avatar_style']]['class_addon']:'').'"><img src="'.$speaker['small_photo_url'].'" alt="speaker"></div>';
		}
		else{
			print '<div class="speaker-photo'.(isset($customize['avatar_style'])?' '.$avatar_style[$customize['avatar_style']]['class_addon']:'').'"><img src="/speaker.svg" alt="speaker"></div>';
		}
		print '<div class="speaker-public-name">'.$speaker['public_name'];
		if($speaker_starred){
			print '<span class="speaker-starred" title="Starred speaker">'.$ltmp['icons']['starred'].'</span>';
		}
		print '</div>';
		print '<div class="speaker-nick-name">'.$speaker['nick_name'].'</div>';
		print '<div class="speaker-title">'.$speaker['title'].'</div>';
		print '<div class="speaker-company">'.$speaker['company'].'</div>';
		print '</a>';
		print PHP_EOL;
	}
	print PHP_EOL;
	print '</div>';
}
$event_module_content=ob_get_contents();
ob_end_clean();