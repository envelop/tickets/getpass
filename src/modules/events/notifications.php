<?php
ob_start();
$replace['title']='Notifications | '.$replace['title'];

if($path_array[4]){
	if('mark-as-read'==$path_array[4]){
		if(check_csrf()){
			$query="SELECT `nq`.* FROM `auth_notifications` as `an` RIGHT JOIN `notifications_queue` as `nq` ON `nq`.`id`=`an`.`notify` AND `nq`.`status`=1 WHERE `an`.`auth`='".$auth['id']."'";
			$notifications=$db->sql($query);
			foreach($notifications as $notification){
				$db->sql("UPDATE `notifications_queue` SET `status`='2', `status_time`='".time()."' WHERE `id`='".$notification['id']."'");
			}
			$db->sql("UPDATE `auth` SET `notifications`=0 WHERE `id`='".$auth['id']."'");
			header('Location: /@'.$organizer_url.'/'.$event_url.'/notifications/');
		}
		else{
			header('HTTP/1.0 403 Forbidden');
		}
		exit;
	}
	else{
		$notify_id=(int)$path_array[4];
		//check if current auth is allowed to see this notification
		$auth_notification_arr=$db->sql_row("SELECT * FROM `auth_notifications` WHERE `auth`='".$auth['id']."' AND `notify`='".$notify_id."'");
		if(null!==$auth_notification_arr){
			$notification=$db->sql_row("SELECT * FROM `notifications_queue` WHERE `id`='".$notify_id."'");
			if($notification){
				print '<h1>Notification</h1>';
				print '<hr class="my-4">';

				//mark notification as read
				$db->sql("UPDATE `notifications_queue` SET `status`='2', `status_time`='".time()."' WHERE `id`='".$notification['id']."'");

				//mark auth for check notification status
				$db->sql("UPDATE `auth` SET `notifications`='2' WHERE `id`='".$auth['id']."'");

				print '<div class="notify-item notify-view text-content">';
				print render_notify($notification);

				$recipient_address=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$notification['address']."'");
				print '<div class="notify-recipient">';
				print '<a href="#" class="avatar"><svg data-jdenticon-value="'.$recipient_address['address'].'" width="80" height="80"  alt="avatar"></svg></a>';
				print $recipient_address['address'];
				//print ' ('.$types_arr[$recipient_address['type']]['name'].')';
				print '</div>';

				print '<div class="notify-time" data-datetime="'.$notification['time'].'">';
					print '<div class="notify-global-time">
					<span class="notify-datetime">'.date('d.m.Y H:i',$notification['time']).' GMT</span>
					</div>';
					print '<div class="notify-local-time">
					<span class="notify-datetime"></span>
					</div>';
				print '</div>';

				print '</div>';
			}
		}
		else{
			header('Location: /@'.$organizer_url.'/'.$event_url.'/notifications/');
			exit;
		}
		print '<div class="my-4">
		<a href="/@'.$organizer_url.'/'.$event_url.'/notifications/" class="reverse-btn">&larr; Back to notifications list</a>
		</div>';
	}
}
else{
	print '<h1>Notifications</h1>';
	print '<hr>';
	if(0==$auth['addresses']){
		print '<p>None addresses found in current session. Please use <a href="/login/?back_url=/@'.$organizer_url.'/'.$event_url.'/notifications/">Login page</a>.</p>';
	}
	else{
		$only_new=false;
		if(isset($_GET['new'])){
			if($_GET['new']){
				$only_new=true;
			}
		}

		print '<div class="my-4 links-list">';
		if(!$only_new){
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/notifications/?new=1" class="reverse-btn">Show only new</a>';
		}
		else{
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/notifications/" class="reverse-btn">Show all</a>';
		}
		if(1==$auth['notifications']){//has new notifications
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/notifications/mark-as-read/?'.gen_csrf_param().'" class="action-btn">Mark as read</a>';
		}
		else{
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/notifications/mark-as-read/?'.gen_csrf_param().'" class="action-btn disabled">Mark as read</a>';
		}
		print '</div>';

		$only_address=false;
		if(isset($_GET['address'])){
			if($_GET['address']){
				foreach($auth_address_arr as $auth_address){
					if($auth_address['id']==(int)$_GET['address']){
						$only_address=$auth_address['id'];
						break;
					}
				}
			}
		}

		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="address" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$only_address?' selected':'').'>Filter by address</option>';
					foreach($auth_address_arr as $auth_address){
						print '<option value="'.$auth_address['id'].'"'.($only_address==$auth_address['id']?' selected':'').'>'.htmlspecialchars($auth_address['address']);
						//print ' ('.$types_arr[$auth_address['type']]['name'].')';
						print '</option>';
					}
					print '
				</select>
			</div>';
		print '</div>';
		print '</form>';

		$pagination=true;
		$sql_addon=[];
		$sql_addon[]="`nq`.`platform`='0'";//only web notifications
		if($only_new){
			$sql_addon[]="`nq`.`status`!='2'";//only not readed notifications
			$pagination=false;
		}
		if($only_address){
			$sql_addon[]="`nq`.`address`='".$only_address."'";//only notifications for selected address
			$pagination=false;
		}
		$sql_addon_str='';
		if(count($sql_addon)){
			$sql_addon_str=' AND '.implode(' AND ',$sql_addon);
		}

		$per_page=25;
		$count=$db->table_count('auth_notifications',"WHERE `auth`='".$auth['id']."'");
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}
		$query="SELECT `nq`.* FROM `auth_notifications` as `an` RIGHT JOIN `notifications_queue` as `nq` ON `nq`.`id`=`an`.`notify`".$sql_addon_str." WHERE `an`.`auth`='".$auth['id']."' ORDER BY `an`.`id` DESC";
		if($pagination){
			$query.=" LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page);
		}
		$notifications=$db->sql($query);
		print '<div class="text-content">';
		foreach($notifications as $notification){
			print '<a href="/@'.$organizer_url.'/'.$event_url.'/notifications/'.$notification['id'].'/" class="notify-item'.(2==$notification['status']?' viewed':'').'">';
			print render_notify_preview($notification);
			print '</a>';
		}
		print '</div>';
		if($pagination){
			print '<div class="pagination">';
			//get string with all GET params except page
			$get_params=[];
			foreach($_GET as $get_param_name=>$get_param_value){
				if('page'!==$get_param_name){
					$get_params[]=$get_param_name.'='.urlencode($get_param_value);
				}
			}
			$get_params_str='';
			if(count($get_params)){
				$get_params_str=implode('&',$get_params);
			}
			if($page>1){
				print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
			}
			for($i=1;$i<=$pages_count;$i++){
				print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
			}
			if($page<$pages_count){
				print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
			}
			print '</div>';
		}
	}
}
$event_module_content=ob_get_contents();
ob_end_clean();