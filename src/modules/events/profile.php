<?php
ob_start();
//unset($event_user['id']);
if('ajax'==$path_array[4]){
	ob_end_clean();
	ob_end_clean();
	header('Content-Type: application/json');
	if('status'==$path_array[5]){
		$status_arr=[
			'status'=>0,
			'on_update'=>0,
			'wait_update'=>0,
		];
		if(isset($event_user['id'])){
			$status_arr['status']=(int)$event_user['status'];
			$status_arr['status_caption']=$user_status_arr[$event_user['status']];
			$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$event_user['address']."'");
			$status_arr['address']=$address_arr['address'];
			$status_arr['level']=$event_user['level'];
			$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `level`='".$event_user['level']."'");
			$status_arr['level_name']=$level_arr['name'];
			$status_arr['level_caption']=$level_arr['caption'];

			$status_arr['admin']=$event_user['admin'];
			$status_arr['manager']=$event_user['manager'];
			$status_arr['speaker']=$event_user['speaker'];
			$status_arr['sponsor']=$event_user['sponsor'];
		}
		else{
			$address_update_timeout=(300)-1;//5 min
			//check last addresses update, if allowed to update, show request update link
			$addresses_on_update=0;
			$addresses_wait_update=0;
			foreach($auth_address_arr as $address_arr){
				if(0==$address_arr['update']){
					if(time()>($address_arr['update_time']+$address_update_timeout)){
						//update=2 for forced update (ignore last update time)
						$db->sql("UPDATE `addresses` SET `update`='2' WHERE `id`='".$address_arr['id']."'");
						$addresses_on_update++;
					}
					else{
						$addresses_wait_update++;
					}
				}
				else{
					$addresses_on_update++;
				}
			}
			$status_arr['on_update']=$addresses_on_update;
			$status_arr['wait_update']=$addresses_wait_update;
		}
		print json_encode($status_arr);
	}
	exit;
}

$replace['title']='Participant profile'.' | '.$replace['title'];
print '<h1>Participant profile</h1>';
print '<div class="text-content">';
if(isset($event_user['id'])){
	$event_user_address=$auth_address_arr[$event_user['address']];
	print '<div class="mb-4 address-item">';
	print '<a href="#" class="avatar"><svg data-jdenticon-value="'.$event_user_address['address'].'" width="80" height="80" alt="avatar"></svg></a>';
	print '<a href="/profile/'.$event_user_address['id'].'/">'.$event_user_address['address'].'</a>';
	//print ' ('.$types_arr[$event_user_address['type']]['name'].')';
	print '</div>';
	print '<p>Level: ';
	$level_arr=$db->sql_row("SELECT * FROM `event_levels` WHERE `event`='".$event_id."' AND `level`='".$event_user['level']."'");
	print $level_arr['name'];
	if($level_arr['caption']){
		print ' &mdash; '.$level_arr['caption'].'';
	}
	print '</p>';
	print '<p>Status: ';
	print '<span class="mr-4 '.$user_status_style_arr[$event_user['status']].'">';
	print $user_status_arr[$event_user['status']];
	print '</span>';
	if(1==$event_user['admin']){
		print ' <span class="badge">+Admin</span>';
	}
	if(1==$event_user['manager']){
		print ' <span class="badge">+Manager</span>';
	}
	if(1==$event_user['speaker']){
		print ' <span class="badge">+Speaker</span>';
	}
	if(1==$event_user['sponsor']){
		print ' <span class="badge">+Sponsor</span>';
	}
	print '</p>';
	if(1==$event_user['status']){//binded
		$nft_arr=$db->sql_row("SELECT * FROM `nft_list` WHERE `id`='".$event_user['binded']."'");
		if(null!=$nft_arr){
			if($event_user['address']==$nft_arr['address']){
				print '<p>Binded with NFT: ';
				$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$nft_arr['chain']."'");
				$nft_url='';
				if($chain_arr['nft_url']){
					$nft_url=str_replace('{contract}',$nft_arr['contract'],$chain_arr['nft_url']);
					$nft_url=str_replace('{token_id}',$nft_arr['token_id'],$nft_url);
				}
				if($nft_url){
					print '<a href="'.$nft_url.'" target="_blank">'.$nft_arr['contract'].' ('.$nft_arr['token_id'].')</a>';
				}
				else{
					print $nft_arr['contract'].' ('.$nft_arr['token_id'].')';
				}
				print '</p>';
			}
		}
	}

	if($event_arr['check_in_message']){
		$clear_check_in_message=clear_html_tags($event_arr['check_in_message']);
		print '<div class="content-wrapper">';
		print $clear_check_in_message;
		print '</div>';
	}
}

if(!isset($event_user['id'])){
	//print exists addresses list
	if(0==$auth['addresses']){
		print '<p>You are not participant. Please use <a href="/login/?back=/@'.$organizer_url.'/'.$event_url.'/profile/">Login page</a> to connect address.</p>';
	}
	else{
		print '<p>You are not participant.</p>';
		print '<p>You can add new address by <a href="/login/?back=/@'.$organizer_url.'/'.$event_url.'/profile/">Login page</a>.</p>';
		print '<div class="text-content">';
		foreach($auth_address_arr as $auth_address){
			print '<div class="mb-4 address-item">';
			print '<a href="#" class="avatar"><svg data-jdenticon-value="'.$auth_address['address'].'" width="80" height="80"  alt="avatar"></svg></a>';
			print '<a class="'.($auth['default_address']==$auth_address['address']?' font-semibold':'').'" href="/profile/'.$auth_address['id'].'/">'.$auth_address['address'].'</a>';
			//print ' ('.$types_arr[$auth_address['type']]['name'].')';
			print '</div>';
		}
		print '</div>';

		print '<div class="check-in-autoupdate"><img src="/loader.svg" class="rotate"><span class="text">Please wait. We are looking NFT-ticket on your address'.($auth['addresses']>1?'es':'').'</span> <span class="timer-text">(update in <span class="timer">5</span> sec)...</span></div>';
	}
	/*
	//old check-in with manual address oracle update request
	$address_update_timeout=300;//5 min
	//check last addresses update, if allowed to update, show request update link
	$addresses_for_update=0;
	foreach($auth_address_arr as $address_arr){
		if(0==$address_arr['update']){
			if(time()>($address_arr['update_time']+$address_update_timeout)){
				$addresses_for_update++;
			}
		}
	}

	if('request_update'==$path_array[4]){
		print '
		<div class="success-box" role="alert">
			<p class="font-bold">Success</p>
			<p>Addresses requested for profile update.</p>';

		foreach($auth_address_arr as $address_arr){
			if(0==$address_arr['update']){
				if(time()>($address_arr['update_time']+$address_update_timeout)){
					//update=2 for forced update (ignore last update time)
					$db->sql("UPDATE `addresses` SET `update`='2' WHERE `id`='".$address_arr['id']."'");
					print '<p>&mdash; '.$address_arr['address'].' ('.$types_arr[$address_arr['type']]['name'].');</p>';
				}
			}
		}
		print '
		</div>';
		$addresses_for_update=0;
	}

	if($addresses_for_update){
		print '<div class="my-4">';
		print '<a class="action-btn'.(isset($event_user['id'])?' opacity-50':'').'" href="/@'.$organizer_url.'/'.$event_url.'/profile/request_update/">Request profile update</a>';
		print '</div>';
	}
	*/

	//print event nft tickets advertisement here
	if($event_arr['tickets_description']){
		$clear_tickets_description=clear_html_tags($event_arr['tickets_description']);
		print '<div class="content-wrapper">';
		print $clear_tickets_description;
		print '</div>';
	}
}
if(0!=$auth['addresses']){
	print '<hr class="my-4">';
	print '<div class="links-list">
	<a href="/logout/?'.gen_csrf_param().'&back=/@'.$organizer_url.'/'.$event_url.'/profile/" class="action-btn negative /*default2-button*/">Logout from current session</a>
	</div>';
}

print '</div>';

$event_module_content=ob_get_contents();
ob_end_clean();