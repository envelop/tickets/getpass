<?php
ob_start();
$replace['title']='Sessions'.' | '.$replace['title'];

$replace['head_addon'].='<link rel="stylesheet" href="/css/atcb.css">';
$replace['head_addon'].='<script src="/js/atcb.js" async defer></script>';

$replace['head_addon'].='<link rel="stylesheet" href="/css/oembed.css">';
$replace['head_addon'].='<script src="/js/oembed.js"></script>';

if($path_array[4]){
	$search_session_id=(int)$path_array[4];
	$session_item=$db->sql_row("SELECT * FROM `event_sessions` WHERE `event`='".$event_id."' AND `id`='".$search_session_id."' AND `status`!=2");
	if(null!==$session_item){
		if('load_content'==$path_array[5]){
			ob_end_clean();
			ob_end_clean();
			$content='';
			ob_start();
			$is_allowed=false;
			if($allow_event_manage){
				$is_allowed=true;
			}
			if(isset($event_user['level'])){
				if($session_item['level']<=$event_user['level']){
					$is_allowed=true;
				}
			}
			else{
				if(0==$session_item['level']){//public for guests
					$is_allowed=true;
				}
			}
			if($is_allowed){
				$session_content='';
				$clear_content=clear_html_tags($session_item['content']);
				$clear_embed_content=clear_html_tags($session_item['embed_content']);
				$clear_embed_content='<figure class="media">'.$clear_embed_content.'</figure>';
				if($clear_content){
					$session_content.=$clear_content;
				}
				if($clear_embed_content){
					$session_content.=$clear_embed_content;
				}
				if(''!=$session_content){
					print $session_content;
				}
				else{
					print '<p>Session content is empty. Please, contact event organizer.</p>';
				}
			}
			else{
				print '<p>Access denied.</p>';
				if($event_arr['tickets_description']){
					$clear_tickets_description=clear_html_tags($event_arr['tickets_description']);
					print $clear_tickets_description;
				}
			}
			exit;
		}
		$replace['title']=htmlspecialchars($session_item['caption']).' | '.$replace['title'];
		$replace['description']=$session_item['description'];
		$replace['description']=strip_tags($replace['description']);
		$replace['description']=str_replace(array("\r\n","\n","\r"),' ',$replace['description']);
		$replace['description']=preg_replace('/\s+/',' ', $replace['description']);
		$replace['description']=substr($replace['description'],0,160);
		$replace['description'].='...';
		$replace['description']=htmlspecialchars($replace['description']);

		$location_caption='';
		$location=$db->sql_row("SELECT * FROM `event_locations` WHERE `event`='".$event_id."' AND `id`='".$session_item['location']."'");
		if($location){
			$location_caption=$location['caption'];
		}

		$string_description='';
		$string_description.='[url]'.$config['platform_url'].'/@'.$organizer_url.'/'.$event_url.'/sessions/'.$session_item['id'].'/[/url]\n';
		$string_description.=htmlspecialchars($session_item['description']);
		$string_description=str_replace(array("\r\n","\n","\r"),'\n',$string_description);
		$string_description=str_replace('\n','<br>',$string_description);

		print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/sessions/">&larr; Back to sessions</a>';
		if($allow_event_manage){
			print '<a class="action-btn configure" href="/@'.$organizer_url.'/'.$event_url.'/manage/sessions/edit/'.$session_item['id'].'/">Edit session</a>';
		}
		print '<a class="action-btn atcb-action-button">'.$ltmp['icons']['calendar-add'].' Add to calendar</a>
		<script type="application/javascript">
		const atcb_config={
			"name":"'.htmlspecialchars($session_item['caption']).'",
			"description":"'.$string_description.'",
			"startDate":"'.date('Y-m-d',$session_item['time']).'",
			"endDate":"'.date('Y-m-d',$session_item['endtime']).'",
			"startTime":"'.date('H:i',$session_item['time']).'",
			"endTime":"'.date('H:i',$session_item['endtime']).'",
			"location":"'.htmlspecialchars($location_caption).'",
			"options":[
				"Google",
				"Apple",
				"iCal",
				"Microsoft365",
				"MicrosoftTeams",
				"Outlook.com",
				"Yahoo"
			],
			"timeZone":"Etc/UTC",
			"iCalFileName":"Reminder-Event",
			"trigger":"click",
			"inline":true,"listStyle":"modal",
		};
		let atcb_button=$(".atcb-action-button");
		atcb_button.on("click",function(){
			atcb_action(atcb_config,atcb_button[0]);
		});
		</script>
		';
		$starred=(1==$session_item['status']);
		print '<div class="session-card single">';
			if($session_item['cover_url']){
				print '<div class="session-cover mb-4 md:mr-6">';
				print '<a href="'.$session_item['cover_url'].'" target="_blank">';
				print '<img src="'.$session_item['thumbnail_cover_url'].'" alt="'.htmlspecialchars($session_item['caption']).'">';
				print '</a>';
				print '</div>';
			}
			print '<div class="session-info">';
				print '<div class="session-time" data-start-time="'.$session_item['time'].'" data-end-time="'.$session_item['endtime'].'" data-duration="'.$session_item['duration'].'">';
					print '<div class="session-global-time">';
						print '<span class="session-date"><span class="bold">'.date('d.m.Y',$session_item['time']).'</span> GMT</span>';
						print '<span class="session-time-range"><span class="bold">'.date('H:i',$session_item['time']).' &ndash; '.date('H:i',$session_item['endtime']).'</span>';
					print '</div>';
					print '<div class="session-local-time">';
						print '
						<span class="session-date"></span>
						<span class="session-time-range"><span class="bold"> &ndash; </span></span>';
					print '</div>';
					print '<span class="session-duration">('.$session_item['duration'].' Min)</span></span>';
				print '</div>';
				print '<div class="session-info">';
				print '<div class="session-caption">';
				print '<span class="session-title">'.htmlspecialchars($session_item['caption']).'</span>';
					if($starred){
						print '<span class="session-starred" title="Starred session">'.$ltmp['icons']['starred'].'</span>';
					}
					print '</div>';
					print '<div class="session-description">'.htmlspecialchars($session_item['description']).'</div>';
					if(''!=$location_caption){
						print '<div class="session-location">
						'.$ltmp['icons']['location'].'
						'.$location_caption.'</div>';
					}
				print '</div>';
			print '</div>';
		print '</div>';

		print '<h2 class="center">Speakers</h2>';
		print '<hr class="my-4">';
		print '<div class="speakers-list center">';
		$speakers=$db->sql("SELECT `es`.*
			FROM `event_sessions_speakers` as `ess`
			INNER JOIN `event_speakers` as `es`
			ON `es`.`event`='".$event_id."' AND `es`.`status`!=2 AND `ess`.`speaker`=`es`.`id`
			WHERE `ess`.`session`='".$session_item['id']."'
			ORDER BY `es`.`status` DESC, `es`.`sort` ASC, `es`.`public_name` ASC");
		while($speaker=$db->row($speakers)){
			$public_name_url=ru2lat($speaker['public_name']);
			$public_name_url=str_replace(' ','-',$public_name_url);
			$public_name_url=preg_replace('~[^a-z0-9\-_]+~iUs','',$public_name_url);
			$public_name_url=preg_replace('~\-+~iUs','-',$public_name_url);
			$public_name_url=preg_replace('~\-+$~iUs','',$public_name_url);
			$public_name_url=preg_replace('~^-+~iUs','',$public_name_url);
			$public_name_url=strtolower($public_name_url);

			$speaker_starred=(1==$speaker['status']);

			print '<a class="speaker" href="/@'.$organizer_url.'/'.$event_url.'/speakers/'.$speaker['id'].'-'.$public_name_url.'/">';
			if($speaker['photo_url']){
				print '<div class="speaker-photo'.(isset($customize['avatar_style'])?' '.$avatar_style[$customize['avatar_style']]['class_addon']:'').'"><img src="'.$speaker['small_photo_url'].'" alt="speaker"></div>';
			}
			else{
				print '<div class="speaker-photo'.(isset($customize['avatar_style'])?' '.$avatar_style[$customize['avatar_style']]['class_addon']:'').'"><img src="/speaker.svg" alt="speaker"></div>';
			}
			print '<div class="speaker-public-name">'.$speaker['public_name'];
			if($speaker_starred){
				print '<span class="speaker-starred" title="Starred speaker">'.$ltmp['icons']['starred'].'</span>';
			}
			print '</div>';
			print '<div class="speaker-nick-name">'.$speaker['nick_name'].'</div>';
			print '<div class="speaker-title">'.$speaker['title'].'</div>';
			print '<div class="speaker-company">'.$speaker['company'].'</div>';
			print '</a>';
		}
		print '</div>';

		if($session_item['time']>time()){
			print '<div class="content-wrapper">';
				print '<div class="countdown" data-time="'.$session_item['time'].'">';
					print '<div class="circle">';
						print '<div class="countdown-value days"></div>';
						print '<div class="countdown-label">Days</div>';
					print '</div>';
					print '<div class="circle">';
						print '<div class="countdown-value hours"></div>';
						print '<div class="countdown-label">Hours</div>';
					print '</div>';
					print '<div class="circle">';
						print '<div class="countdown-value minutes"></div>';
						print '<div class="countdown-label">Minutes</div>';
					print '</div>';
					print '<div class="circle">';
						print '<div class="countdown-value seconds"></div>';
						print '<div class="countdown-label">Seconds</div>';
					print '</div>';
				print '</div>';
			print '</div>';
		}
		else{
			print '<div class="content-wrapper waiting"><p>Loading...</p></div>';
		}
	}
	else{
		header("HTTP/1.0 404 Not Found");
		print '<h1>404 Not Found</h1>';
		print '<p>Session not found, please return to <a href="/@'.$organizer_url.'/'.$event_url.'/sessions/">sessions</a> list.</p>';
	}
}
else{
	print '<h1>Upcoming sessions</h1>';
	print '<hr class="my-4">';
	print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/agenda/">&larr; Back to agenda</a>';
	$sessions_counter=0;
	$sessions=$db->sql("SELECT `es`.*
		FROM `event_sessions` as `es`
		WHERE `es`.`event`='".$event_id."' AND `es`.`endtime`>'".time()."' AND `es`.`status`!=2
		ORDER BY `es`.`endtime` ASC, `es`.`time` ASC");
	foreach($sessions as $session_item){
		$sessions_counter++;

		$location_caption='';
		$location=$db->sql_row("SELECT * FROM `event_locations` WHERE `event`='".$event_id."' AND `id`='".$session_item['location']."'");
		if($location){
			$location_caption=$location['caption'];
		}

		$starred=(1==$session_item['status']);
		print '<div class="session-card">';
			if($session_item['cover_url']){
				print '<div class="session-cover mb-4 md:mr-6">';
				print '<a href="/@'.$organizer_url.'/'.$event_url.'/sessions/'.$session_item['id'].'/">';
				print '<img src="'.$session_item['thumbnail_cover_url'].'" alt="'.htmlspecialchars($session_item['caption']).'">';
				print '</a>';
				print '</div>';
			}
			print '<div class="session-info">';
				print '<div class="session-time" data-start-time="'.$session_item['time'].'" data-end-time="'.$session_item['endtime'].'" data-duration="'.$session_item['duration'].'">';
					print '<div class="session-global-time">';
						print '<span class="session-date"><span class="bold">'.date('d.m.Y',$session_item['time']).'</span> GMT</span>';
						print '<span class="session-time-range"><span class="bold">'.date('H:i',$session_item['time']).' &ndash; '.date('H:i',$session_item['endtime']).'</span>';
					print '</div>';
					print '<div class="session-local-time">';
						print '
						<span class="session-date"></span>
						<span class="session-time-range"><span class="bold"> &ndash; </span></span>';
					print '</div>';
					print '<span class="session-duration">('.$session_item['duration'].' Min)</span></span>';
				print '</div>';
				print '<div class="session-info">';
				print '<div class="session-caption">';
				print '<a href="/@'.$organizer_url.'/'.$event_url.'/sessions/'.$session_item['id'].'/">'.htmlspecialchars($session_item['caption']).'</a>';
					if($starred){
						print '<span class="session-starred" title="Starred session">'.$ltmp['icons']['starred'].'</span>';
					}
					print '</div>';
					print '<div class="session-description">'.htmlspecialchars($session_item['description']).'</div>';
					if(''!=$location_caption){
						print '<div class="session-location">
						'.$ltmp['icons']['location'].'
						'.$location_caption.'</div>';
					}
				print '</div>';
			print '</div>';
		print '</div>';
	}
	if(0==$sessions_counter){
		print '<p>No upcoming sessions</p>';
	}
}
$event_module_content=ob_get_contents();
ob_end_clean();