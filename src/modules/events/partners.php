<?php
ob_start();
$replace['title']='Partners'.' | '.$replace['title'];
$partners_count=$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `status`!=2");
if($partners_count){
	if($path_array[4]){
		$partner=$db->sql_row("SELECT * FROM `event_partners` WHERE `event`='".$event_id."' AND `url`='".$db->prepare(urldecode($path_array[4]))."' AND `status`!=2");
		if(null!==$partner){
			$partner_starred=(1==$partner['status']);

			$replace['title']=htmlspecialchars($partner['name']).' | '.$replace['title'];
			$replace['description']=$partner['name'];
			if($partner['caption']){
				$replace['description'].=' ('.$partner['caption'].')';
			}
			if($partner['description']){
				$replace['description'].=' - '.$partner['description'];
			}
			$replace['description']=strip_tags($replace['description']);
			$replace['description']=str_replace(array("\r\n","\n","\r"),' ',$replace['description']);
			$replace['description']=preg_replace('/\s+/', ' ', $replace['description']);
			$replace['description']=substr($replace['description'],0,160);
			$replace['description'].='...';
			$replace['description']=htmlspecialchars($replace['description']);

			print '<h1>'.htmlspecialchars($partner['name']);
			if($partner['link']){
				print '<a href="'.htmlspecialchars(str_replace('javascript:','',$partner['link'])).'" class="text-blue-500 font-normal !no-underline ml-2" target="_blank">⧉</a>';
			}
			if($partner_starred){
				print '<span class="partner-starred ml-2" title="Starred partner">'.$ltmp['icons']['starred'].'</span>';
			}
			print '</h1>';

			print '<hr class="my-4">';

			if($partner['logo_url']){
				$replace['head_addon'].='<meta property="og:image" content="'.$config['platform_url'].$partner['logo_url'].'">';
				/*
				print '<div class="partner-logo">';
				print '<img src="'.htmlspecialchars($partner['logo_url']).'" alt="'.htmlspecialchars($partner['name']).'">';
				print '</div>';
				*/
			}
			$clear_description=clear_html_tags($partner['description']);
			if($clear_description){
				print '<div class="content-card article single">';
					print '<div class="content-wrapper">';
					print $clear_description;
					print '</div>';
				print '</div>';
			}
			print '<hr class="my-4">';
			print '<a class="reverse-btn" href="/@'.$organizer_url.'/'.$event_url.'/partners/">&larr; Back to partners</a>';
			if($allow_event_manage){
				print '<a class="action-btn configure" href="/@'.$organizer_url.'/'.$event_url.'/manage/partners/edit/'.$partner['id'].'/">Edit partner</a>';
			}
		}
		else{
			http_response_code(404);
			print '<h1>404 Not Found</h1>';
			print '<p>Partner was not found, please return to <a href="/@'.$organizer_url.'/'.$event_url.'/partners/">partners list</a>.</p>';
		}
	}
	else{
		print '<h1>Partners</h1>';
		print '<hr class="my-4">';
		$first=true;
		$partners_cats=$db->sql("SELECT * FROM `event_partners_cat` WHERE `event`='".$event_id."' ORDER BY `sort` ASC");
		foreach($partners_cats as $partners_cat){
			$partners_cat_count=$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `status`!=2 AND `cat`='".$partners_cat['id']."'");
			if($partners_cat_count){
				if(!$first){
					print '<hr class="my-4">';
				}
				$first=false;
				print '<h2 class="partners-category">'.htmlspecialchars($partners_cat['caption']).'</h2>';
				print '<div class="partners-list">';
				$partners=$db->sql("SELECT * FROM `event_partners` WHERE `event`='".$event_id."' AND `cat`='".$partners_cat['id']."' AND `status`!=2 ORDER BY `sort` ASC");
				foreach($partners as $partner){
					print '<div class="partner-item">';
					$external=true;
						if($partner['url']){
							$external=false;
							$partner['new_url']='/@'.$organizer_url.'/'.$event_url.'/partners/'.$partner['url'].'/';
						}
						else{
							$partner['new_url']=$partner['link'];
						}
					print '<a href="'.htmlspecialchars($partner['new_url']).'"'.($external?' target="_blank"':'').'>';
					if($partner['small_logo_url']){
						print '<img src="'.htmlspecialchars($partner['small_logo_url']).'" alt="'.htmlspecialchars($partner['name']).'">';
					}
					else{
						print htmlspecialchars($partner['name']);
					}
					if($partner['caption']){
						print '<div class="partner-caption">'.htmlspecialchars($partner['caption']).'</div>';
					}
					print '</a>';
					print '</div>';
				}
				print '</div>';
			}
		}

		//check if any partner without category
		if(0<$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `status`!=2 AND `cat`='0'")){
			print '<hr class="my-4"><div class="partners-list">';
			$partners=$db->sql("SELECT * FROM `event_partners` WHERE `event`='".$event_id."' AND `cat`='0' AND `status`!=2 ORDER BY `sort` ASC");
			foreach($partners as $partner){
				print '<div class="partner-item">';
				$external=true;
					if($partner['url']){
						$external=false;
						$partner['new_url']='/@'.$organizer_url.'/'.$event_url.'/partners/'.$partner['url'].'/';
					}
					else{
						$partner['new_url']=$partner['link'];
					}
				print '<a href="'.htmlspecialchars($partner['new_url']).'"'.($external?' target="_blank"':'').'>';
				if($partner['small_logo_url']){
					print '<img src="'.htmlspecialchars($partner['small_logo_url']).'" alt="'.htmlspecialchars($partner['name']).'">';
				}
				else{
					print htmlspecialchars($partner['name']);
				}
				if($partner['caption']){
					print '<div class="partner-caption">'.htmlspecialchars($partner['caption']).'</div>';
				}
				print '</a>';
				print '</div>';
			}
			print '</div>';
		}
	}
}
else{
	http_response_code(404);
	print '<h1>404 Not Found</h1>';
	print '<p>Partners was not found, please return to <a href="/@'.$organizer_url.'/'.$event_url.'/">event page</a>.</p>';
}
$event_module_content=ob_get_contents();
ob_end_clean();