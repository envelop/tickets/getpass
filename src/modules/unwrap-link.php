<?php
//for oembed js support
//check referer
if(!isset($_SERVER['HTTP_REFERER'])){
	http_response_code(403);
	exit;
}
else{
	//check referer is on the same domain
	if(!preg_match('/^https?:\/\/'.$_SERVER['HTTP_HOST'].'/',$_SERVER['HTTP_REFERER'])){
		http_response_code(403);
		exit;
	}
}

$link=$query_string;
//check link starts on http/https
if(!preg_match('/^https?:\/\//',$link)){
	$link='https://'.$link;
}
$response=$api->get_url($link);
$find=false;
$attempts=0;
while(!$find && $attempts<5){
	$attempts++;
	list($header,$result)=$api->parse_web_result($response);
	//check if link is a redirect
	if(preg_match('/^HTTP\/1\.[01] 3\d\d/',$response)){
		//get the location
		preg_match('/Location: (.*)/',$response,$matches);
		$link=$matches[1];
		//check link starts on http/https
		if(!preg_match('/^https?:\/\//',$link)){
			$link='https://'.$link;
		}
		$response=$api->get_url($link);
	}
	else{
		$find=true;
	}
}
if($find){
	print trim($link," \n\r\t");
}
else{
	http_response_code(404);
}
exit;