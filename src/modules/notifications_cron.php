<?php
set_time_limit(0);
//script work time near 1 minute with sleep between cycles
$work_time=50;//60 minus 10 seconds
$sleep_time=10;//sleep time in seconds between cycles
$start_time=time();

$lock_fp=fopen($root_dir.'/notifications_cron.lock','w');
if(!flock($lock_fp,LOCK_EX|LOCK_NB)){
	exit;
}

while(true){
	//look for notifications_queue with status=0 and search each address in auth_addresses
	//if found, add notification to auth_notifications with notify id and update status=1
	//if not found, update status=3
	$notifications=$db->sql("SELECT * FROM `notifications_queue` WHERE `status`='0'");
	foreach($notifications as $notification){
		$founded=0;
		$auth_addresses=$db->sql("SELECT * FROM `auth_addresses` WHERE `address`='".$notification['address']."'");
		foreach($auth_addresses as $auth_address){
			$auth_arr=$db->sql_row("SELECT * FROM `auth` WHERE `id`='".$auth_address['auth']."'");
			//check auth for activity status or ignore it
			if($auth_arr['active_time']>time()-$config['auth_activity_status']){
				$founded++;
				$db->sql("INSERT INTO `auth_notifications` (`auth`,`notify`) VALUES ('".$auth_address['auth']."','".$notification['id']."')");
				$db->sql("UPDATE `auth` SET `notifications`=1 WHERE `id`='".$auth_address['auth']."'");
			}
		}
		if($founded){
			$db->sql("UPDATE `notifications_queue` SET `status`='1' WHERE `id`='".$notification['id']."'");
		}
		else{
			$db->sql("UPDATE `notifications_queue` SET `status`='3' WHERE `id`='".$notification['id']."'");
		}

		//check linked platforms for telegram (id=1) to send an immediate notification
		$linked_platforms=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$notification['address']."' AND `platform`=1 AND `status`=2 AND `flag2`=1");//flag2=1 - send notifications
		foreach($linked_platforms as $linked_platform){
			$notify_html=render_notify($notification,true);
			telegram_queue($linked_platform['internal_id'],'sendMessage',['text'=>$notify_html,'parse_mode'=>'HTML']);
		}
	}

	//look for auth with notifications=2 and check any unread notifications
	$auths=$db->sql("SELECT * FROM `auth` WHERE `notifications`='2'");
	foreach($auths as $auth){
		$auth_notifications=$db->sql_row("SELECT COUNT(`nq`.`id`) as `count` FROM `auth_notifications` as `an` RIGHT JOIN `notifications_queue` as `nq` ON `nq`.`id`=`an`.`notify` AND `nq`.`status`=1 WHERE `an`.`auth`='".$auth['id']."' ORDER BY `an`.`id` DESC");
		if(0!=$auth_notifications['count']){
			$db->sql("UPDATE `auth` SET `notifications`=1 WHERE `id`='".$auth['id']."'");
		}
		else{
			$db->sql("UPDATE `auth` SET `notifications`=0 WHERE `id`='".$auth['id']."'");
		}
	}

	//look for auth with notifications=1 and active in last month and remove old notifications
	$auths=$db->sql("SELECT * FROM `auth` WHERE `notifications`='1' AND `active_time`>'".(time()-$config['auth_activity_for_clear_notifications'])."'");
	foreach($auths as $auth){
		$auth_notifications_count=$db->table_count('auth_notifications','WHERE `auth`="'.$auth['id'].'"');
		if($auth_notifications_count>$config['auth_notification_max_limit']){
			$auth_notifications_last=$db->sql_row("SELECT * FROM `auth_notifications` WHERE `auth`='".$auth['id']."' ORDER BY `id` DESC LIMIT 1 OFFSET ".($config['auth_notification_max_limit']));
			if(null!=$auth_notifications_last){
				//remove old notifications more than limit
				$db->sql("DELETE FROM `auth_notifications` WHERE `auth`='".$auth['id']."' AND `id`<='".$auth_notifications_last['id']."'");
			}
		}
	}

	//check work time
	$now_time=time();
	if($work_time <= $now_time-$start_time){
		break;
	}
	sleep($sleep_time);
}
//close lock file
flock($lock_fp,LOCK_UN);
fclose($lock_fp);
exit;