<?php
ob_start();
$replace['title']='Event tickets | '.$replace['title'];
print '<h1>Event tickets</h1>';
print '<hr>';
$filter=false;//all events
if(isset($_GET['filter'])){
	$filter=(int)$_GET['filter'];
	if(1!=$filter){//active events
		$filter=2;//archive events
	}
}
print '<div class="my-4"><ul class="submenu">';
print '<li><a href="/event-tickets/"'.(false===$filter?' class="selected"':'').'>All events</a></li>';
print '<li><a href="/event-tickets/?filter=1"'.(1==$filter?' class="selected"':'').'>Active events</a></li>';
print '<li><a href="/event-tickets/?filter=2"'.(2==$filter?' class="selected"':'').'>Archived events</a></li>';
print '</ul></div>';
print '<hr class="my-4">';

$tickets_counter=0;
$tickets_binded_counter=0;
$tickets_invited_counter=0;
$event_tickets_counter_arr=[];
$event_tickets_arr=[];
$event_tickets_sort_id_arr=[];//id
$event_tickets_sort_time_arr=[];//start time

//we need build navigation for each address from session, because we can have different events/tickets for each address
foreach($auth_address_arr as $auth_address){
	$sql_addon=[];
	$sql_addon[]="`address`='".$db->prepare($auth_address['id'])."'";
	$sql_addon[]='(`status`=1 OR `status`=2)';//invited OR binded
	$sql_addon_str='';
	if(count($sql_addon)){
		$sql_addon_str='WHERE '.implode(' AND ',$sql_addon);
	}
	$find_event_users=$db->sql("SELECT * FROM `event_users`".$sql_addon_str);
	foreach($find_event_users as $find_event_user){
		$sql_filter='(`status`=1 OR `status`=2)';//all, active and archive
		if(false!==$filter){
			$sql_filter="`status`='".$db->prepare($filter)."'";
		}
		$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$db->prepare($find_event_user['event'])."' AND ".$sql_filter." AND `moderation`='1'");//approved events with active/archive status (from filter)
		/*
		if(1!=$event_arr['moderation']){//approved
			$event_arr=null;
		}
		if(1!=$event_arr['status']){//active
			$event_arr=null;
		}
		*/
		if(null!==$event_arr){
			//count tickets
			$tickets_counter++;
			if(1==$find_event_user['status']){
				$tickets_binded_counter++;
			}
			elseif(2==$find_event_user['status']){
				$tickets_invited_counter++;
			}
			if(!isset($event_tickets_counter_arr[$find_event_user['event']])){
				$event_tickets_counter_arr[$find_event_user['event']]=0;
			}
			$event_tickets_counter_arr[$find_event_user['event']]++;

			$event_tickets_sort_id_arr[]=$event_arr['id'];
			$event_tickets_sort_time_arr[]=$event_arr['time'];
			$event_tickets_arr[]=$event_arr;
		}
	}
}
if($tickets_counter){
	//sort events by time desc
	array_multisort($event_tickets_sort_time_arr, SORT_DESC, $event_tickets_sort_id_arr, SORT_DESC, $event_tickets_arr);

	//remove offset from array
	$per_page=20;
	$count=$tickets_counter;
	$pages_count=ceil($count/$per_page);
	$page=1;
	if(isset($_GET['page'])){
		$page=(int)$_GET['page'];
		if($page<1){
			$page=1;
		}
		elseif($page>$pages_count){
			$page=$pages_count;
		}
	}
	for($i=0;$i<($page-1)*$per_page;$i++){
		array_shift($event_tickets_arr);
	}

	print '<div class="text-content mb-8">';
	$counter=0;
	foreach($event_tickets_arr as $event_id=>$event_arr){
		$counter++;
		$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($event_arr['organizer'])."'");
		//add opacity for archived events
		print '<p class="mb-4'.(2==$event_arr['status']?' opacity-50 hover:opacity-75':'').'"><a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/" target="_blank">'.htmlspecialchars($event_arr['title']).'</a> event by <a href="/@'.htmlspecialchars($organizer_arr['url']).'/" target="_blank">'.htmlspecialchars($organizer_arr['title']).'</a>'.(2==$event_arr['status']?' (archived)':'').'</p>';
		if($counter>=$per_page){
			break;
		}
	}
	print '</div>';

	print '<div class="pagination">';
	//get string with all GET params except page
	$get_params=[];
	foreach($_GET as $get_param_name=>$get_param_value){
		if('page'!==$get_param_name){
			$get_params[]=$get_param_name.'='.urlencode($get_param_value);
		}
	}
	$get_params_str='';
	if(count($get_params)){
		$get_params_str=implode('&',$get_params);
	}
	if($page>1){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
	}
	for($i=1;$i<=$pages_count;$i++){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
	}
	if($page<$pages_count){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
	}
	print '</div>';
}
else{
	print '<p class="text-content">No event tickets found</p>';
}
print '<hr class="my-4">';
print '<div class="max-w-[100vw]">
<a href="/profile/" class="action-btn reverse-btn /*navigate*/">&larr; Back to profile</a>
</div>';
$content=ob_get_contents();
ob_end_clean();