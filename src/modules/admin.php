<?php
if(1!=$auth['status']){//if not admin
	http_response_code(403);
	header('location:/');
	exit;
}
$replace['counters']='';
$replace['title']='Admin '.$replace['title'];
ob_start();
$organizer_url=false;
$event_url=false;
$addresses_cache_arr=[];
function get_address($address_str){
	global $addresses_cache_arr,$db;
	if(isset($addresses_cache_arr[$address_str])){
		return $addresses_cache_arr[$address_str];
	}
	$address_str=strtolower($address_str);
	$address_arr=$db->sql_row("SELECT `address` FROM `addresses` WHERE `id`='".$db->prepare($address_str)."' LIMIT 1");
	if(null!==$address_arr){
		$addresses_cache_arr[$address_str]=$address_arr['address'];
		return $address_arr['address'];
	}
	return '';
}
$organizers_cache_arr=[];
function get_organizer($organizer_id){
	global $organizers_cache_arr,$db;
	if(isset($organizers_cache_arr[$organizer_id])){
		return $organizers_cache_arr[$organizer_id];
	}
	$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".(int)$organizer_id."' LIMIT 1");
	if(null!==$organizer_arr){
		$organizers_cache_arr[$organizer_id]=$organizer_arr;
		return $organizers_cache_arr[$organizer_id];
	}
	return false;
}
print '<div class="admin-title-wrapper">';
print '<h1>Admin</h1>';
if($config['manual_admin_link']){
	print '<div class="button-wrapper">';
	print '<a href="'.$config['manual_admin_link'].'" target="_blank" class="action-btn">'.$ltmp['icons']['video'].' '.$config['manual_admin_caption'].'</a>';
	print '</div>';
}
print '</div>';
print '<hr>';
print '<div class="my-4">';
print '<ul class="submenu">';
//https://gitlab.com/envelop/tickets/getpass/-/issues/33
$tab_class='';//default3-button
print '<li><a href="/admin/events/" class="'.$tab_class.('events'==$path_array[2]?' selected':'').'">Events</a></li>';
print '<li><a href="/admin/types/" class="'.$tab_class.('types'==$path_array[2]?' selected':'').'">Types</a></li>';
print '<li><a href="/admin/chains/" class="'.$tab_class.('chains'==$path_array[2]?' selected':'').'">Chains</a></li>';
print '<li><a href="/admin/addresses/" class="'.$tab_class.('addresses'==$path_array[2]?' selected':'').'">Addresses</a></li>';
print '<li><a href="/admin/oracle_updates/" class="'.$tab_class.('oracle_updates'==$path_array[2]?' selected':'').'">Oracle Updates</a></li>';

print '<li><a href="/admin/nft_list/" class="'.$tab_class.('nft_list'==$path_array[2]?' selected':'').'">NFT List</a></li>';
print '<li><a href="/admin/organizers/" class="'.$tab_class.('organizers'==$path_array[2]?' selected':'').'">Organizers</a></li>';

print '<li><a href="/admin/tags/" class="'.$tab_class.('tags'==$path_array[2]?' selected':'').'">Tags</a></li>';
print '<li><a href="/admin/content/" class="'.$tab_class.('content'==$path_array[2]?' selected':'').'">Content</a></li>';

if($config['link_platform_removal_moderation']){
	print '<li><a href="/admin/requests/" class="'.$tab_class.('requests'==$path_array[2]?' selected':'').'">Requests</a></li>';
}

print '<li><a href="/admin/files/" class="'.$tab_class.('files'==$path_array[2]?' selected':'').'">Files</a></li>';
print '<li><a href="/admin/stats/" class="'.$tab_class.('stats'==$path_array[2]?' selected':'').'">Stats</a></li>';

print '</ul>';
print '</div>';
print '<hr class="my-4">';
if(''==$path_array[2]){
	header('location:/admin/events/');
	exit;
}
if('stats'==$path_array[2]){
	$replace['title']='Dashboard | '.$replace['title'];
	print '<p>Addresses count: '.$db->table_count('addresses').'</p>';
	print '<p>Auth sessions (24h): '.$db->table_count('auth',"WHERE `active_time`>'".(time()-3600*24)."'").'</p>';
	print '<p>Auth sessions: '.$db->table_count('auth').'</p>';
	print '<hr>';
	print '<p>Chains count: '.$db->table_count('chains').'</p>';
	print '<p>Countable NFT: '.$db->table_count('nft_list').'</p>';
	print '<p>Oracle Updates: '.$db->table_count('oracle_updates').'</p>';
	print '<p>Oracle Updates (24h): '.$db->table_count('oracle_updates',"WHERE `status_time`>'".(time()-3600*24)."'").'</p>';
	print '<hr>';
	print '<p>Organizers approved: '.$db->table_count('organizers',"WHERE `status`=1").'</p>';
	$organizers_on_moderation=$db->table_count('organizers',"WHERE `status`=0");
	if($config['organizer_moderation']||$organizers_on_moderation){
		print '<p>Organizers <a href="/admin/organizers/?status=0">on moderation</a>: '.$organizers_on_moderation.'</p>';
	}
	print '<hr>';
	print '<p>Events approved: '.$db->table_count('events',"WHERE `moderation`=1").'</p>';
	$events_on_moderation=$db->table_count('events',"WHERE `moderation`=0");
	if($config['organizer_moderation']||$organizers_on_moderation){
		print '<p>Events <a href="/admin/events/?moderation=0">on moderation</a>: '.$events_on_moderation.'</p>';
	}
	print '<hr>';
	$summary_file_size=0;
	print '<p>Admin files count: '.$db->table_count('files').'</p>';
	$admin_file_size=$db->select_one('files','SUM(`size`)');
	print '<p>Admin files size: '.human_readable_filesize($admin_file_size).'</p>';
	$summary_file_size+=$admin_file_size;

	print '<p>Organizers files count: '.$db->table_count('organizer_files').'</p>';
	$organizers_file_size=$db->select_one('organizer_files','SUM(`size`)');
	print '<p>Organizers files size: '.human_readable_filesize($organizers_file_size).'</p>';
	$summary_file_size+=$organizers_file_size;

	print '<p>Events files count: '.$db->table_count('event_files').'</p>';
	$events_file_size=$db->select_one('event_files','SUM(`size`)');
	print '<p>Events files size: '.human_readable_filesize($events_file_size).'</p>';
	$summary_file_size+=$events_file_size;

	print '<p>Summary files size: '.human_readable_filesize($summary_file_size).'</p>';
}
elseif('types'==$path_array[2]){
	$replace['title']='Types | '.$replace['title'];
	print '
	<div class="flex flex-col">
		<div class="table-wrapper">
		<div class="py-2 inline-block min-w-full">
		<div class="overflow-hidden">';
		print '<table class="table-auto min-w-full">';
		print '<thead class="bg-white border-b">';
		print '<tr>';
		print '<th>ID</th>';
		print '<th>Name</th>';
		print '<th>Signer support</th>';
		print '<th>Chains count</th>';
		print '<th>Addresses count</th>';
		print '<th>NFT count</th>';
		print '</tr>';
		print '</thead>';
		print '<tbody>';
	$types=$db->sql("SELECT * FROM `types` ORDER BY `id` ASC");
	foreach($types as $type){
		print '<tr>';
		print '<td>'.$type['id'].'</td>';
		print '<td>'.htmlspecialchars($type['name']).'</td>';
		print '<td>'.(1==$type['signer']?'<span class="text-green-500">Yes</span>':'None').'</td>';
		$chains_count=$db->table_count('chains',"WHERE `type`='".$type['id']."'");
		print '<td><a href="/admin/chains/?type='.$type['id'].'">'.$chains_count.'</a></td>';
		$addresses_count=$db->table_count('addresses',"WHERE `type`='".$type['id']."'");
		print '<td><a href="/admin/addresses/?type='.$type['id'].'">'.$addresses_count.'</a></td>';
		$nft_list_count=$db->table_count('nft_list',"WHERE `type`='".$type['id']."'");
		print '<td><a href="/admin/nft_list/?type='.$type['id'].'">'.$nft_list_count.'</a></td>';
		print '</tr>';
	}
	print '</tbody>';
	print '</table>';
	print '
				</div>
			</div>
		</div>
	</div>';
}
elseif('chains'==$path_array[2]){
	$replace['title']='Chains | '.$replace['title'];
	if('delete'==$path_array[3]){
		$chain_id=intval($path_array[4]);
		$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$chain_id."'");
		if(null!=$chain_arr){
			if(isset($_POST['approve'])){
				$db->sql("DELETE FROM `chains` WHERE `id`='".$chain_id."'");
				$db->sql("DELETE FROM `nft_list` WHERE `chain`='".$chain_id."'");
				$db->sql("DELETE FROM `oracle_updates` WHERE `chain`='".$chain_id."'");
				header('location:/admin/chains/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/chains/">&larr; Back to chains</a>';
				print '<h2 class="text-red-600">Remove chain #'.$chain_arr['id'].'</h2>';
				$nft_list_count=$db->table_count('nft_list',"WHERE `chain`='".$chain_arr['id']."'");
				$oracle_updates_count=$db->table_count('oracle_updates',"WHERE `chain`='".$chain_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Chain '.htmlspecialchars($chain_arr['name']).' will be removed from the database.</p>';
				if($nft_list_count){
					print '<p>Also will be deleted: '.$nft_list_count.' items from NFT List.</p>';
				}
				if($oracle_updates_count){
					print '<p>Also will be deleted: '.$oracle_updates_count.' items from Oracle Updates.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove chain" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/chains/">&larr; Back to chains</a>';
			print '<h2 class="text-red-600">Chain not found</h2>';
			print '<p>Return to chains list and try again.</p>';
		}
	}
	elseif('create'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/chains/">&larr; Back to chains</a>';
		print '<h2>Create chain</h2>';
		if(isset($_POST['name'])){
			$errors=[];
			$name=trim($_POST['name']);
			if(0!=$db->table_count('chains',"WHERE `name`='".$db->prepare($name)."'")){
				$errors[]='Chain with this name already exists';
			}
			if(''==$name){
				$errors[]='Name is empty';
			}
			$short=trim($_POST['short']);
			if(''==$short){
				$errors[]='Short name is empty';
			}

			$type=intval($_POST['type']);
			if(!isset($types_arr[$type])){
				$errors[]='Invalid type';
			}
			$chain_id=intval($_POST['chain_id']);
			if(!$chain_id){
				$errors[]='Invalid chain ID';
			}
			else{
				$chain_id_exist=$db->table_count('chains',"WHERE `chain_id`='".$chain_id."'");
				if($chain_id_exist){
					$errors[]='Chain ID already exists';
				}
			}
			$description=trim($_POST['description']);
			$ticker=trim($_POST['ticker']);

			$nft_url=trim($_POST['nft_url']);
			if($nft_url){
				if(false===strpos($nft_url,'{contract}')){
					$errors[]='NFT URL must contain {contract} placeholder';
				}
				if(false===strpos($nft_url,'{token_id}')){
					$errors[]='NFT URL must contain {token_id} placeholder';
				}
			}
			$status=intval($_POST['status']);
			if(!isset($chains_status_arr[$status])){
				$errors[]='Invalid status';
			}
			$sort=0;
			if(isset($_POST['sort'])){
				$sort=intval($_POST['sort']);
			}


			if(0==count($errors)){
				$db->sql("INSERT INTO `chains` (`name`,`short`,`type`,`chain_id`,`description`,`ticker`,`nft_url`,`status`,`sort`) VALUES ('".$db->prepare($name)."','".$db->prepare($short)."','".$type."','".$chain_id."','".$db->prepare($description)."','".$db->prepare($ticker)."','".$db->prepare($nft_url)."','".$status."','".$sort."')");
				$chain_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Chain <a href="/admin/chains/edit/'.$chain_id.'/">"'.htmlspecialchars($name).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/admin/chains/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';
			$object_scheme=[//admin create chain
				'name'=>['type'=>'text','title'=>'Name','required'=>true],
				'short'=>['type'=>'text','title'=>'Short name','required'=>true],
				'description'=>['type'=>'text','title'=>'Description'],
				'ticker'=>['type'=>'text','title'=>'Ticker'],
				'nft_url'=>['type'=>'text','title'=>'NFT URL','descr'=>'Use {contract} and {token_id} placeholders in url'],
				'chain_id'=>['type'=>'number','title'=>'Chain ID','required'=>true],
				'sort'=>['type'=>'number','title'=>'Sort order','min'=>0],
				'type'=>['type'=>'select','title'=>'Type','options'=>$types_arr,'caption_function'=>function($option_id,$option_value){return $option_id.' ('.$option_value['name'].')';},'required'=>true],
				'status'=>['type'=>'select','title'=>'Status','options'=>$chains_status_arr,'classes'=>$chains_status_arr_class,'required'=>true],
			];
			print build_form($object_scheme);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create chain" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/chains/">&larr; Back to chains</a>';
		print '<h2>Edit chain</h2>';
		$id=(int)$path_array[4];
		if($id){
			$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$id."'");
			if(null!=$chain_arr){
				if(isset($_POST['name'])){
					$errors=[];

					$type=intval($_POST['type']);
					if(!isset($types_arr[$type])){
						$errors[]='Invalid type';
					}
					$chain_id=intval($_POST['chain_id']);
					if(!$chain_id){
						$errors[]='Invalid chain ID';
					}
					else{
						$chain_id_exist=$db->table_count('chains',"WHERE `chain_id`='".$chain_id."' AND `id`!='".$id."'");
						if($chain_id_exist){
							$errors[]='Chain ID already exists';
						}
					}
					$short=trim($_POST['short']);
					$name=trim($_POST['name']);
					if(!$name){
						$errors[]='Name is required';
					}
					$description=trim($_POST['description']);
					$ticker=trim($_POST['ticker']);

					$nft_url=trim($_POST['nft_url']);
					if($nft_url){
						if(false===strpos($nft_url,'{contract}')){
							$errors[]='NFT URL must contain {contract} placeholder';
						}
						if(false===strpos($nft_url,'{token_id}')){
							$errors[]='NFT URL must contain {token_id} placeholder';
						}
					}
					$status=intval($_POST['status']);
					if(!isset($chains_status_arr[$status])){
						$errors[]='Invalid status';
					}
					$sort=intval($_POST['sort']);

					if(0==count($errors)){
						$db->sql("UPDATE `chains` SET
							`type`='".$type."',
							`chain_id`='".$chain_id."',
							`short`='".$db->prepare($short)."',
							`name`='".$db->prepare($name)."',
							`description`='".$db->prepare($description)."',
							`ticker`='".$db->prepare($ticker)."',
							`nft_url`='".$db->prepare($nft_url)."',
							`status`='".$status."',
							`sort`='".$sort."'
							WHERE `id`='".$id."'
						");
						print '
						<div class="success-box" role="alert">
							<p class="font-bold">Success</p>
							<p>Chain <a href="/admin/chains/edit/'.$id.'/">"'.htmlspecialchars($name).'"</a> was updated</p>
						</div>';
						print '<meta http-equiv="refresh" content="5; url=/admin/chains/">';
					}
					else{
						print '<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>'.implode('<br>',$errors).'</p>
						</div>';
						print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
					}
				}
				else{
					print '<form action="" method="POST" class="manage-card">';
					print '<div class="max-w-fit">';
						print '<div class="my-4"><input type="text" name="name" value="'.htmlspecialchars($chain_arr['name']).'" placeholder="Name" required> &mdash; Name</div>';
						print '<div class="my-4"><input type="text" name="short" value="'.htmlspecialchars($chain_arr['short']).'" placeholder="Short name" required> &mdash; Short name</div>';
						print '<div class="my-4"><input type="text" name="description" value="'.htmlspecialchars($chain_arr['description']).'" placeholder="Description"> &mdash; Description</div>';
						print '<div class="my-4"><input type="text" name="ticker" value="'.htmlspecialchars($chain_arr['ticker']).'" placeholder="Ticker"> &mdash; Ticker</div>';
						print '<div class="my-4"><input type="text" name="nft_url" value="'.htmlspecialchars($chain_arr['nft_url']).'" placeholder="NFT URL"> &mdash; NFT URL</div>';
						print '<div class="my-4"><input type="number" name="chain_id" value="'.htmlspecialchars($chain_arr['chain_id']).'" placeholder="Chain ID" required> &mdash; Chain ID</div>';


						print '<div class="my-4"><input type="number" name="sort" value="'.htmlspecialchars($chain_arr['sort']).'" placeholder="Sort order"> &mdash; Sort order</div>';
						print '<div class="my-4">Type:<br><select name="type" required>';
							foreach($types_arr as $type_id=>$type_arr){
								print '<option value="'.$type_id.'" '.($type_id==$chain_arr['type']?'selected':'').'>'.$type_id.' ('.htmlspecialchars($type_arr['name']).')</option>';
							}
						print '</select></div>';
						print '<div class="my-4">Status:<br><select name="status" required>';
							foreach($chains_status_arr as $k=>$v){
								print '<option value="'.$k.'" '.($k==$chain_arr['status']?'selected':'').' class="'.$chains_status_arr_class[$k].'">'.$v.'</option>';
							}
						print '</select></div>';
					print '</div>';
					print '<div class="grid-wrapper">';
					print '<input type="submit" value="Save changes" class="action-btn big">';
					print '</div>';
					print '</form>';
				}
			}
			else{
				print '<h2 class="text-red-600">Chain not found</h2>';
				print '<p>Return to chains list and try again.</p>';
			}
		}
		else{
			print '<h2 class="text-red-600">Chain not found</h2>';
			print '<p>Return to chains list and try again.</p>';
		}
	}
	else{
		print '<a class="action-btn" href="/admin/chains/create/">Create chain</a>';
		$filter_query=false;
		if(isset($_GET['query'])){
			if(''!==$_GET['query']){
				$filter_query=trim($_GET['query']);
			}
		}
		$filter_type=false;
		if(isset($_GET['type'])){
			if(''!==$_GET['type']){
				$filter_type=(int)$_GET['type'];
				if(!isset($types_arr[$filter_type])){
					$filter_type=false;
				}
			}
		}

		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="type" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_type?' selected':'').'>Filter by type</option>';
					foreach($types_arr as $type_id=>$type_arr){
						print '<option value="'.$type_id.'"'.($type_id===$filter_type?' selected':'').'>Type: '.$type_id.' ('.htmlspecialchars($type_arr['name']).')</option>';
					}
					print '
				</select>
			</div>
			<div>
				<input name="query" class="form-post-by-enter" placeholder="Search" value="'.($filter_query?htmlspecialchars($filter_query):'').'">
			</div>';
		print '</div>';
		print '</form>';

		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
		print '<table class="table-auto min-w-full">';
		print '<thead class="bg-white border-b">';
		print '<tr>';
		print '<th>Actions</th>';
		print '<th>ID</th>';
		print '<th>Name</th>';
		print '<th>Short</th>';
		print '<th>Chain ID</th>';
		print '<th>Type</th>';
		print '<th>Status</th>';
		print '<th>NFT count</th>';
		print '<th>Oracle updates count</th>';
		print '<th>Sort</th>';
		print '</tr>';
		print '</thead>';
		print '<tbody>';
		$sql_addon=[];
		if($filter_query){
			$sql_addon[]="`name` LIKE '%".$db->prepare($filter_query)."%' OR `short` LIKE '%".$db->prepare($filter_query)."%' OR `chain_id` LIKE '%".$db->prepare($filter_query)."%'";
		}
		if($filter_type){
			$sql_addon[]="`type`='".$db->prepare($filter_type)."'";
		}
		$sql_addon_str='';
		if(count($sql_addon)){
			$sql_addon_str='WHERE '.implode(' AND ',$sql_addon);
		}
		$chains_arr=$db->sql("SELECT * FROM `chains`".$sql_addon_str." ORDER BY `sort` ASC");
		foreach($chains_arr as $chain_arr){
			print '<tr>';
			print '<td><a href="/admin/chains/delete/'.$chain_arr['id'].'/" class="red-btn">Delete</a></td>';
			print '<td>'.$chain_arr['id'].'</td>';
			print '<td><a href="/admin/chains/edit/'.$chain_arr['id'].'/">'.htmlspecialchars($chain_arr['name']).'</a></td>';
			print '<td>'.htmlspecialchars($chain_arr['short']).'</td>';
			print '<td>'.$chain_arr['chain_id'].'</td>';
			print '<td>'.$chain_arr['type'].' ('.htmlspecialchars($types_arr[$chain_arr['type']]['name']).')</td>';
			print '<td class="'.$chains_status_arr_class[$chain_arr['status']].'">'.$chains_status_arr[$chain_arr['status']].'</td>';
			$nft_list_count=$db->table_count('nft_list',"WHERE `chain`='".$chain_arr['id']."'");
			print '<td><a href="/admin/nft_list/?chain='.$chain_arr['id'].'">'.$nft_list_count.'</a></td>';
			$oracle_updates_count=$db->table_count('oracle_updates',"WHERE `chain`='".$chain_arr['id']."'");
			print '<td><a href="/admin/oracle_updates/?chain='.$chain_arr['id'].'">'.$oracle_updates_count.'</a></td>';
			print '<td>'.$chain_arr['sort'].'</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}

}
elseif('organizers'==$path_array[2]){
	$replace['title']='Organizers | '.$replace['title'];
	if('approve'==$path_array[3]){
		$organizer_id=intval($path_array[4]);
		$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$organizer_id."'");
		if(null!=$organizer_arr){
			if(isset($_POST['approve'])){

				//get all administators and send them notification about approved organizer
				$org_administation=$db->sql("SELECT `address` FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."'");
				foreach($org_administation as $org_administator){
					add_notify($org_administator['address'],0,1,'org_approved',json_encode(['organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url'])]));
				}

				$db->sql("UPDATE `organizers` SET `status`=1 WHERE `id`='".$organizer_id."'");
				header('location:/admin/organizers/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/organizers/">&larr; Back to organizers</a>';
				print '<h2 class="text-green-600">Approve organizer #'.$organizer_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Attention</p>
					<p>Organizer <a href="/admin/organizers/edit/'.$organizer_arr['id'].'/">"'.htmlspecialchars($organizer_arr['title']).'"</a> will be set as approved and will be able to create events and content on site.</p>
					<p>It will be available for public view.</p>';
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Approve organizer for public actions (events, content)" class="green-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/organizers/">&larr; Back to organizers</a>';
			print '<h2 class="text-red-600">Organizer not found</h2>';
			print '<p>Return to organizers list and try again.</p>';
		}
	}
	elseif('reject'==$path_array[3]){
		$organizer_id=intval($path_array[4]);
		$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$organizer_id."'");
		if(null!=$organizer_arr){
			if(isset($_POST['approve'])){

				//get all administators and send them notification about rejected organizer
				$org_administation=$db->sql("SELECT `address` FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."'");
				foreach($org_administation as $org_administator){
					add_notify($org_administator['address'],0,1,'org_rejected',json_encode(['organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url'])]));
				}

				$db->sql("UPDATE `organizers` SET `status`=2 WHERE `id`='".$organizer_id."'");
				header('location:/admin/organizers/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/organizers/">&larr; Back to organizers</a>';
				print '<h2 class="text-red-600">Reject organizer #'.$organizer_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Attention</p>
					<p>Organizer <a href="/admin/organizers/edit/'.$organizer_arr['id'].'/">"'.htmlspecialchars($organizer_arr['title']).'"</a> will be set as rejected/banned and can not create new events and content on site.</p>
					<p>Organizer events and content will be not available for public view.</p>';
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Ban organizer from taking new actions" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/organizers/">&larr; Back to organizers</a>';
			print '<h2 class="text-red-600">Organizer not found</h2>';
			print '<p>Return to organizers list and try again.</p>';
		}
	}
	elseif('delete'==$path_array[3]){
		$organizer_id=intval($path_array[4]);
		$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$organizer_id."'");
		if(null!=$organizer_arr){
			if(isset($_POST['approve'])){
				$db->sql("UPDATE `organizers` SET `status`=3 WHERE `id`='".$organizer_id."'");
				header('location:/admin/organizers/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/organizers/">&larr; Back to organizers</a>';
				print '<h2 class="text-red-600">Remove organizer #'.$organizer_arr['id'].'</h2>';
				$organizers_events_count=$db->table_count('events',"WHERE `organizer`='".$organizer_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Organizer <a href="/admin/organizers/edit/'.$organizer_arr['id'].'/">"'.htmlspecialchars($organizer_arr['title']).'"</a> will be set as hidden in database.</p>
					<p>It was turned off and not be available for view.</p>';
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Hide organizer and all events ('.$organizers_events_count.')" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/organizers/">&larr; Back to organizers</a>';
			print '<h2 class="text-red-600">Organizer not found</h2>';
			print '<p>Return to organizers list and try again.</p>';
		}
	}
	elseif('create'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/organizers/">&larr; Back to organizers</a>';
		print '<h2>Create organizer</h2>';
		if(isset($_POST['title'])){
			$errors=[];
			$title=trim($_POST['title']);
			if(0!=$db->table_count('organizers',"WHERE `title`='".$db->prepare($title)."'")){
				$errors[]='Organizer with this title already exists';
			}

			$url=trim($_POST['url']);
			//remove all unsopperted symbols from url
			$url=preg_replace('~[^a-zA-Z0-9\-\.]+~iUs','',$url);
			$url=preg_replace('/\.+/','.',$url);
			if(0!=$db->table_count('organizers',"WHERE `url`='".$db->prepare($url)."'")){
				$errors[]='Organizer with this URL already exists';
			}
			if(in_array($url,$reserved_urls)){
				$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
			}

			if(''==$title){
				$errors[]='Title is empty';
			}
			if(0!=$db->table_count('organizers',"WHERE `title`='".$db->prepare($title)."'")){
				$errors[]='Organizer with the same title already exist';
			}

			$description=trim($_POST['description']);
			if(''==$description){
				$errors[]='Description is empty';
			}
			$events_description=trim($_POST['events_description']);
			if(''==$events_description){
				$errors[]='Events description is empty';
			}

			$status=intval($_POST['status']);
			if(!isset($organizers_status_arr[$status])){
				$errors[]='Invalid status';
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `organizers` SET
					`url`='".$db->prepare($url)."',
					`title`='".$db->prepare($title)."',
					`description`='".$db->prepare($description)."',
					`events_description`='".$db->prepare($events_description)."',
					`status`='".$status."',
					`time`='".time()."',
					`update_time`='".time()."'
				");
				$organizer_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Organizer <a href="/admin/organizers/edit/'.$organizer_id.'/">"'.htmlspecialchars($title).'"</a> was created.</p>
					<p>Add admin addresses for it.</p>
				</div>';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';

			$object_scheme=[//admin create organizer profile
				'url'=>['type'=>'text','title'=>'URL','required'=>true,'descr'=>'Used for /@<b>URL</b>/ page address bar.','class'=>'url-part with-uppercase'],
				'title'=>['type'=>'text','title'=>'Title','required'=>true],
				'description'=>['type'=>'text','title'=>'Description','descr'=>'Text about organizer and mission'],
				'events_description'=>['type'=>'text','title'=>'Events description','descr'=>'Text that will be shown on the top of the events list page'],
				'status'=>['type'=>'select','title'=>'Status','options'=>$organizers_status_arr,'classes'=>$organizers_status_arr_class,'required'=>true],
			];
			print build_form($object_scheme);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create organizer" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/organizers/">&larr; Back to organizers</a>';
		$organizer_id=intval($path_array[4]);
		$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$organizer_id."'");
		if(null!=$organizer_arr){
			print '<h2>Edit organizer #'.$organizer_arr['id'].'</h2>';
			if(isset($_POST['title'])){
				$errors=[];

				$url=trim($_POST['url']);
				//remove all unsopperted symbols from url
				$url=preg_replace('~[^a-zA-Z0-9\-\.]+~iUs','',$url);
				$url=preg_replace('/\.+/','.',$url);
				if(in_array($url,$reserved_urls)){
					$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
				}
				if(0!=$db->table_count('organizers',"WHERE `id`!='".$organizer_id."' AND `url`='".$db->prepare($url)."'")){
					$errors[]='Organizer with the same URL already exist';
				}
				if(''==$url){
					$errors[]='URL is empty';
				}

				$title=trim($_POST['title']);
				if(''==$title){
					$errors[]='Title is empty';
				}
				if(0!=$db->table_count('organizers',"WHERE `id`!='".$organizer_id."' AND `title`='".$db->prepare($title)."'")){
					$errors[]='Organizer with the same title already exist';
				}

				$description=trim($_POST['description']);
				if(''==$description){
					$errors[]='Description is empty';
				}
				$events_description=trim($_POST['events_description']);
				if(''==$events_description){
					$errors[]='Events description is empty';
				}

				$status=intval($_POST['status']);
				if(!isset($organizers_status_arr[$status])){
					$errors[]='Invalid status';
				}

				$subscription_plan=intval($_POST['subscription_plan']);
				if(!isset($config['subscription_plans'][$subscription_plan])){
					$errors[]='Invalid subscription plan';
				}

				$subscription_expiration=intval($_POST['unixtime']);
				if(0>$subscription_expiration){
					$subscription_expiration=0;
				}
				if(0!=$subscription_expiration){
					if($subscription_expiration<time()){
						$errors[]='Invalid subscription expiration';
					}
				}

				$max_upload_files_size=intval($_POST['max_upload_files_size']);

				if(0==count($errors)){
					$db->sql("UPDATE `organizers` SET
						`url`='".$db->prepare($url)."',
						`title`='".$db->prepare($title)."',
						`description`='".$db->prepare($description)."',
						`events_description`='".$db->prepare($events_description)."',
						`status`='".$status."',
						`subscription_plan`='".$subscription_plan."',
						`subscription_expiration`='".$subscription_expiration."',
						`max_upload_files_size`='".$max_upload_files_size."',
						`update_time`='".time()."'
						WHERE `id`='".$organizer_id."'
					");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Organizer <a href="/admin/organizers/edit/'.$organizer_id.'/">"'.htmlspecialchars($title).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/admin/organizers/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="?" method="POST" class="manage-card" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=subscription_expiration]\').val())/1000);return true;">';
				print '<div class="max-w-fit">';
				$object_scheme=[//admin edit organizer profile
					'url'=>['type'=>'text','title'=>'URL','required'=>true,'descr'=>'Used for /@<b>URL</b>/ page address bar.','class'=>'url-part with-uppercase'],
					'title'=>['type'=>'text','title'=>'Title','required'=>true],
					'description'=>['type'=>'text','title'=>'Description','descr'=>'Text about organizer and mission'],
					'events_description'=>['type'=>'text','title'=>'Events description','descr'=>'Text that will be shown on the top of the events list page'],
					'status'=>['type'=>'select','title'=>'Status','options'=>$organizers_status_arr,'classes'=>$organizers_status_arr_class,'required'=>true],
					['type'=>'hr'],
					'subscription_plan'=>[
						'type'=>'select',
						'options'=>$config['subscription_plans'],
						'title'=>'Subscription plan',
						'default_option'=>0,
						'caption_function'=>function($option_id,$option_data){
							return htmlspecialchars($option_data['name']);
						}
					],
					'subscription_expiration'=>[
						'type'=>'datetime',
						//'key'=>'time',
						'local'=>true,//-local
						'future'=>true,
						'title'=>'Subscription plan expiration date',
						'timestamp'=>'unixtime',
						'value'=>0
					],
					'max_upload_files_size'=>['type'=>'number','title'=>'Max upload files size','placeholder'=>'Optional (in bytes)','descr'=>'If empty - default value will be used'],
				];
				print build_form($object_scheme,$organizer_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';

				print '<h2 class="mt-6">Administration</h2>';
				if('delete_address'==$path_array[5]){
					if(check_csrf()){
						$organizers_address_id=intval($path_array[6]);
						$address_exist_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `id`='".$organizers_address_id."' AND `organizer`='".$organizer_id."'");
						if(null!=$address_exist_arr){
							$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$address_exist_arr['address']."'");
							$db->sql("DELETE FROM `organizer_addresses` WHERE `id`='".$organizers_address_id."'");

							//send notification to deleted admin
							add_notify($address_exist_arr['address'],0,1,'org_admin_deleted',json_encode(['organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url'])]));

							header('Location: /admin/organizers/edit/'.$organizer_id.'/?delete_address=success&address='.urlencode($address_arr['address']));
							exit;
						}
						else{
							header('Location: /admin/organizers/edit/'.$organizer_id.'/?delete_address=error&error='.$organizers_address_id);
							exit;
						}
					}
					else{
						header('Location: /admin/organizers/edit/'.$organizer_id.'/?delete_address=error&error=csrf');
						exit;
					}
				}
				if('make_owner'==$path_array[5]){
					if(check_csrf()){
						$organizers_address_id=intval($path_array[6]);
						$address_exist_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `id`='".$organizers_address_id."' AND `organizer`='".$organizer_id."'");
						if(null!=$address_exist_arr){
							$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$address_exist_arr['address']."'");
							//make all other addresses as admin
							$db->sql("UPDATE `organizer_addresses` SET `status`=0 WHERE `organizer`='".$organizer_id."'");
							//make selected address as owner
							$db->sql("UPDATE `organizer_addresses` SET `status`=1 WHERE `id`='".$organizers_address_id."'");

							//send notification to new owner
							add_notify($address_exist_arr['address'],0,1,'org_new_owner',json_encode(['organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url'])]));

							header('Location: /admin/organizers/edit/'.$organizer_id.'/?make_owner=success&address='.urlencode($address_arr['address']));
							exit;
						}
					}
					else{
						header('Location: /admin/organizers/edit/'.$organizer_id.'/?make_owner=error&error=csrf');
						exit;
					}
				}
				if(isset($_GET['make_owner'])){
					if('success'==$_GET['make_owner']){
						print '
						<div class="success-box" role="alert">
							<p class="font-bold">Success</p>
							<p>Address '.htmlspecialchars($_GET['address']).' was updated to organizer owner</p>
						</div>';
					}
					if('error'==$_GET['make_owner']){
						if('csrf'==$_GET['error']){
							print '
							<div class="attention-box" role="alert">
								<p class="font-bold">Error</p>
								<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
							</div>';
						}
					}
				}
				if(isset($_GET['delete_address'])){
					if('success'==$_GET['delete_address']){
						print '
						<div class="success-box" role="alert">
							<p class="font-bold">Success</p>
							<p>Address '.htmlspecialchars($_GET['address']).' was deleted from organizer administration</p>
						</div>';
					}
					if('error'==$_GET['delete_address']){
						if('csrf'==$_GET['error']){
							print '
							<div class="attention-box" role="alert">
								<p class="font-bold">Error</p>
								<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
							</div>';
						}
						else{
							print '
							<div class="attention-box" role="alert">
								<p class="font-bold">Error</p>
								<p>Address #'.htmlspecialchars((int)$_GET['error']).' not found</p>
							</div>';
						}
					}
				}
				if(isset($_POST['add_address'])){
					$errors=[];
					$type=intval($_POST['type']);
					if(!isset($types_arr[$type])){
						$errors[]='Invalid type';
					}
					$caption='';
					if(isset($_POST['caption'])){
						$caption=trim($_POST['caption']);
					}
					$address=trim($_POST['address']);
					if(1==$type){//ethereum
						if(!preg_match('/^0x[a-fA-F0-9]{40}$/',$address)){
							$errors[]='Invalid EVM address';
						}
						$address=mb_strtolower($address);
					}
					if(''==$address){
						$errors[]='Address is empty';
					}
					$address_id=false;
					$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `address`='".$db->prepare($address)."' AND `type`='".$type."'");
					if(null!=$address_arr){
						$address_id=$address_arr['id'];
						//check if address is already in organizer
						$organizer_address_arr=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."' AND `address`='".$address_id."'");
						if(null!=$organizer_address_arr){
							$errors[]='Address '.htmlspecialchars($address_arr['address']).' is already in organizer administration';
						}
					}
					else{
						$db->sql("INSERT INTO `addresses` SET
							`type`='".$type."',
							`address`='".$db->prepare($address)."',
							`caption`='".$db->prepare($caption)."',
							`time`='".time()."'
						");
						$address_id=$db->last_id();
					}
					$status=intval($_POST['status']);
					if(!isset($organizer_addresses_status_arr[$status])){
						$errors[]='Invalid status';
					}
					if(1==$status){
						$owner_exist=$db->sql_row("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."' AND `status`=1");
						if(null!=$owner_exist){
							$errors[]='Owner already exist, there can only be one';
						}
					}
					if(0==count($errors)){
						$db->sql("INSERT INTO `organizer_addresses`
							(`organizer`,`address`,`caption`,`status`)
							VALUES
							('".$organizer_id."','".$address_id."','".$db->prepare($caption)."','".$status."')
						");

						//send notification to added admin
						add_notify($address_id,0,1,'org_admin_added',json_encode(['organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url'])]));

						print '
						<div class="success-box" role="alert">
							<p class="font-bold">Success</p>
							<p>Address <a href="/admin/address/edit/'.$address_id.'/">'.htmlspecialchars($address).'</a> ('.$types_arr[$type]['name'].' type)  was added to organizer <a href="/admin/organizers/edit/'.$organizer_id.'/">'.htmlspecialchars($organizer_arr['title']).'</a> as '.$organizer_addresses_status_arr[$status].'.</p>
						</div>';
					}
					else{
						print '<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>'.implode('<br>',$errors).'</p>
						</div>';
						print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
					}
				}
				$new_address_type=1;//owner by default
				if(0!=$db->table_count('organizer_addresses',"WHERE `organizer`='".$organizer_id."'")){
					print '<div class="flex flex-col">
					<div class="table-wrapper">
					<div class="py-2 inline-block min-w-full">
					<div class="overflow-hidden">';
					print '<table class="table-auto min-w-full">';
					print '<thead class="bg-white border-b">';
					print '<tr>';
					print '<th>Action</th>';
					print '<th>Type</th>';
					print '<th>Address</th>';
					print '<th>Caption</th>';
					print '<th>Status</th>';
					print '</tr>';
					print '</thead>';
					print '<tbody>';
					$organizer_addresses_arr=$db->sql("SELECT * FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."' ORDER BY `id` ASC");
					foreach($organizer_addresses_arr as $k=>$v){
						print '<tr>';
						print '<td>';
						if(0==$v['status']){//not owner
							print '<a href="/admin/organizers/edit/'.$organizer_id.'/make_owner/'.$v['id'].'/?'.gen_csrf_param().'" class="toggle-btn mr-4 mb-2">Make owner</a>';
							print '<a href="/admin/organizers/edit/'.$organizer_id.'/delete_address/'.$v['id'].'/?'.gen_csrf_param().'" class="red-btn">Delete</a>';
						}
						else{
							print '&mdash;';
						}
						print '</td>';
						$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$v['address']."'");
						print '<td>'.htmlspecialchars($types_arr[$address_arr['type']]['name']).'</td>';
						print '<td>'.htmlspecialchars($address_arr['address']).'</td>';
						print '<td>'.htmlspecialchars($v['caption']).'</td>';
						print '<td class="'.$organizer_addresses_status_arr_class[$v['status']].'">'.$organizer_addresses_status_arr[$v['status']].'</td>';
						print '</tr>';
						if(1==$v['status']){//owner founded
							$new_address_type=0;//admin
						}
					}
					print '</tbody>';
					print '</table>';
					print '</div>';
					print '</div>';
					print '</div>';
					if(1==$new_address_type){
						print '<div class="attention-box" role="alert">
							<p class="font-bold">Attention</p>
							<p>Organizer has no owner address, please add someone</p>
						</div>';
					}
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Attention</p>
						<p>Organizer has no administrators addresses, please add someone</p>
					</div>';
				}
				print '<form action="?" method="POST" class="manage-card">';
				print '<div class="max-w-fit">';
				print '<p class="font-bold">Add address to administration</p>';
				$object_scheme=[//organizer add address to administration
					'type'=>['type'=>'select','title'=>'Address type','options'=>$types_arr,'caption_template'=>'{id} ({name})','required'=>true],
					'address'=>['type'=>'text','title'=>'Wallet address','required'=>true],
					'caption'=>['type'=>'text','title'=>'Caption','placeholder'=>'Caption (visible memo)'],
					'status'=>['type'=>'select','title'=>'Status','options'=>$organizer_addresses_status_arr,'classes'=>$organizer_addresses_status_arr_class,'default_option'=>$new_address_type,'required'=>true],
				];
				print build_form($object_scheme,false);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" name="add_address" value="Add administrator" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Organizer not found</h2>';
			print '<p>Return to organizers list and try again.</p>';
		}
	}
	else{
		print '<a class="action-btn" href="/admin/organizers/create/">Create organizer</a>';
		$filter_query=false;
		if(isset($_GET['query'])){
			if(''!==$_GET['query']){
				$filter_query=trim($_GET['query']);
			}
		}
		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($organizers_status_arr[$filter_status])){
					$filter_status=false;
				}
			}
		}

		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
					foreach($organizers_status_arr as $k=>$v){
						print '<option value="'.$k.'"'.($k===$filter_status?' selected':'').' class="'.$organizers_status_arr_class[$k].'">Status: '.$v.'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<input name="query" class="form-post-by-enter" placeholder="Search" value="'.($filter_query?htmlspecialchars($filter_query):'').'">
			</div>';
		print '</div>';
		print '</form>';

		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
		print '<table class="table-auto min-w-full">';
		print '<thead class="bg-white border-b">';
		print '<tr>';
		print '<th>Actions</th>';

		print '<th>URL</th>';
		print '<th>Title</th>';
		print '<th>Description</th>';
		print '<th>Events description</th>';
		print '<th>Status</th>';
		print '<th>Reg time</th>';
		print '<th>Update time</th>';

		print '<th>Subscription plan</th>';
		print '<th>Subscription expiration</th>';

		print '<th>Events</th>';
		print '<th>Summary Files size</th>';
		print '</tr>';
		print '</thead>';
		print '<tbody>';
		$sql_addon=[];
		if($filter_query){
			$sql_addon[]="(`url` LIKE '%".$db->prepare($filter_query)."%' OR `title` LIKE '%".$db->prepare($filter_query)."%' OR `description` LIKE '%".$db->prepare($filter_query)."%')";
		}
		if(false!==$filter_status){
			$sql_addon[]="`status`='".$db->prepare($filter_status)."'";
		}
		$sql_addon_str='';
		if(count($sql_addon)){
			$sql_addon_str='WHERE '.implode(' AND ',$sql_addon);
		}

		$per_page=50;
		$count=$db->table_count('organizers',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$organizers_arr=$db->sql("SELECT * FROM `organizers`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($organizers_arr as $organizer_arr){
			print '<tr>';
			print '<td>';
			if(0==$organizer_arr['status']){
				print '<a href="/admin/organizers/approve/'.$organizer_arr['id'].'/" class="green-btn mr-4 mb-2">Approve</a>';
				print '<a href="/admin/organizers/reject/'.$organizer_arr['id'].'/" class="red-btn mr-4 mb-2">Reject</a>';
			}
			if(3!=$organizer_arr['status']){//if not hidden
				print '<a href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/" class="violet-btn mr-4 mb-2">Manage</a>';
				print '<a href="/admin/organizers/delete/'.$organizer_arr['id'].'/" class="red-btn mb-2">Hide</a>';
			}
			print '</td>';
			print '<td class="whitespace-nowrap">
				<a href="/admin/organizers/edit/'.$organizer_arr['id'].'/">'.htmlspecialchars($organizer_arr['url']).'</a>
				<a href="/@'.htmlspecialchars($organizer_arr['url']).'/" target="_blank" class="text-blue-500 !no-underline">⧉</a>
			</td>';
			print '<td>'.htmlspecialchars($organizer_arr['title']).'</td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($organizer_arr['description']).'</div></td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($organizer_arr['events_description']).'</div></td>';
			print '<td class="'.$organizers_status_arr_class[$organizer_arr['status']].'">'.$organizers_status_arr[$organizer_arr['status']].'</td>';
			print '<td><span class="time" data-time="'.$organizer_arr['time'].'">'.date('d.m.Y H:i:s',$organizer_arr['time']).'</span></td>';
			print '<td><span class="time" data-time="'.$organizer_arr['update_time'].'">'.date('d.m.Y H:i:s',$organizer_arr['update_time']).'</span></td>';


			print '<td>';
			print $config['subscription_plans'][$organizer_arr['subscription_plan']]['name'];
			print '</td>';
			print '<td>';
			if(0==$organizer_arr['subscription_expiration']){
				print '&mdash;';
			}
			else{
				print '<span class="time" data-time="'.$organizer_arr['subscription_expiration'].'">'.date('d.m.Y H:i:s',$organizer_arr['subscription_expiration']).'</span>';
			}
			print '</td>';


			$events_count=$db->table_count('events',"WHERE `organizer`='".$organizer_arr['id']."'");

			print '<td><a href="/admin/events/?organizer='.$organizer_arr['url'].'">'.$events_count.'</a></td>';

			print '<td class="whitespace-nowrap">'.human_readable_filesize($organizer_arr['summary_files_size']).'</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('oracle_updates'==$path_array[2]){
	$replace['title']='Oracle Updates | '.$replace['title'];
	$filter_address=false;
	$filter_address_caption='';
	if(isset($_GET['address'])){
		if(''!==$_GET['address']){
			$addresses=$db->sql("SELECT * FROM `addresses` WHERE `address`='".$db->prepare($_GET['address'])."'");
			if($addresses->num_rows){
				$filter_address=[];
				foreach($addresses as $address_arr){
					$filter_address[]=$address_arr['id'];
				}
				if(1==count($filter_address)){
					$filter_address_caption=$_GET['address'];
				}
				elseif(count($filter_address)){
					$filter_address_caption='Addresses: '.count($filter_address);
				}
			}
		}
	}
	$filter_status=false;
	if(isset($_GET['status'])){
		if(''!==$_GET['status']){
			$filter_status=(int)$_GET['status'];
			if(!isset($oracle_updates_status[$filter_status])){
				$filter_status=false;
			}
		}
	}

	print '
	<form action="" method="GET">
	<div class="filters-wrapper">
		<div>
			<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
				<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
				foreach($oracle_updates_status as $oracle_updates_status_id=>$oracle_updates_status_caption){
					print '<option value="'.$oracle_updates_status_id.'"'.($oracle_updates_status_id===$filter_status?' selected':'').'>Status: '.htmlspecialchars($oracle_updates_status_caption).'</option>';
				}
				print '
			</select>
		</div>
		<div>
			<input name="address" class="form-post-by-enter" placeholder="Address query" value="'.htmlspecialchars($filter_address_caption).'">
		</div>';
	print '</div>';
	print '</form>';
	print '
	<div class="flex flex-col">
		<div class="table-wrapper">
		<div class="py-2 inline-block min-w-full">
		<div class="overflow-hidden">';
	print '<table class="table-auto min-w-full">';
	print '<thead class="bg-white border-b">';
	print '<tr>';
	print '<th>ID</th>';
	print '<th>Chain</th>';
	print '<th>Address</th>';
	print '<th>Status</th>';
	print '<th>Status time</th>';
	print '</tr>';
	print '</thead>';
	print '<tbody>';
	$sql_addon=[];
	if($filter_address){
		$sql_addon[]='`address` IN ('.implode(',',$filter_address).')';
	}
	if(false!==$filter_status){
		$sql_addon[]='`status`='.$filter_status;
	}
	$sql_addon_str='';
	if(count($sql_addon)){
		$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
	}
	$per_page=50;
	$count=$db->table_count('oracle_updates',$sql_addon_str);
	$pages_count=ceil($count/$per_page);
	$page=1;
	if(isset($_GET['page'])){
		$page=(int)$_GET['page'];
		if($page<1){
			$page=1;
		}
		elseif($page>$pages_count){
			$page=$pages_count;
		}
	}
	$oracle_updates=$db->sql("SELECT * FROM `oracle_updates`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
	foreach($oracle_updates as $oracle_update){
		print '<tr>';
		print '<td>'.$oracle_update['id'].'</td>';
		$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$oracle_update['chain']."'");
		print '<td><a href="/admin/chains/edit/'.$chain_arr['id'].'/">'.$chain_arr['id'].'</a> ('.htmlspecialchars($chain_arr['short']).' #'.$chain_arr['chain_id'].')</td>';
		print '<td><a href="/admin/addresses/edit/'.$oracle_update['address'].'/">'.$oracle_update['address'].'</a> ('.htmlspecialchars(get_address($oracle_update['address'])).')</td>';
		print '<td class="'.$oracle_updates_status_class[$oracle_update['status']].'">'.$oracle_updates_status[$oracle_update['status']].'</td>';
		print '<td>'.date('d.m.Y H:i:s',$oracle_update['status_time']).'</td>';
		print '</tr>';
	}
	print '</tbody>';
	print '</table>';
	print '
				</div>
			</div>
		</div>
	</div>';

	print '<div class="pagination">';
	//get string with all GET params except page
	$get_params=[];
	foreach($_GET as $get_param_name=>$get_param_value){
		if('page'!==$get_param_name){
			$get_params[]=$get_param_name.'='.urlencode($get_param_value);
		}
	}
	$get_params_str='';
	if(count($get_params)){
		$get_params_str=implode('&',$get_params);
	}
	if($page>1){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
	}
	for($i=1;$i<=$pages_count;$i++){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
	}
	if($page<$pages_count){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
	}
	print '</div>';
}
elseif('nft_list'==$path_array[2]){
	$replace['title']='NFT List | '.$replace['title'];
	if($path_array[3]){
		$id=(int)$path_array[3];
		$nft_arr=$db->sql_row("SELECT * FROM `nft_list` WHERE `id`='".$id."'");
		if(null!=$nft_arr){
			if(0==$nft_arr['status']){
				if(isset($_POST['update'])){
					$db->sql("UPDATE `nft_list` SET `status`='3' WHERE `id`='".$id."'");
					$nft_arr['status']=3;
					print '<div class="success-box" role="alert">
						<p>NFT was added to update queue</p>
					</div>';
				}
			}
			print '<h2>NFT #'.$id.'</h2>';
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="card-content max-w-fit">';

			print '<div class="my-4">Type: '.$nft_arr['type'].' ('.$types_arr[$nft_arr['type']]['name'].')</div>';
			$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$nft_arr['chain']."'");
			print '<div class="my-4">Chain: <a href="/admin/chains/edit/'.$nft_arr['chain'].'/">'.$nft_arr['chain'].'</a> ('.htmlspecialchars($chain_arr['short']).' #'.$chain_arr['chain_id'].')</div>';
			print '<div class="my-4">Address: <a href="/admin/addresses/edit/'.$nft_arr['address'].'/">'.$nft_arr['address'].'</a> ('.htmlspecialchars(get_address($nft_arr['address'])).')</div>';
			print '<div class="my-4">Contract: <a href="/admin/nft_list/?contract='.htmlspecialchars($nft_arr['contract']).'">'.htmlspecialchars($nft_arr['contract']).'</a></div>';
			print '<div class="my-4">Token ID: '.htmlspecialchars($nft_arr['token_id']).'</div>';
			print '<div class="my-4">Status: <span class="'.$nft_status_class[$nft_arr['status']].'">'.$nft_status[$nft_arr['status']].'</span></div>';
			print '<div class="my-4">Status time: '.date('d.m.Y H:i:s',$nft_arr['status_time']).'</div>';

			print '</div>';
			if(0==$nft_arr['status']){
				print '<div class="grid-wrapper">';
				print '<input type="submit" name="update" value="Add to update queue" class="action-btn big">';
				print '</div>';
			}
			print '</form>';
		}
		else{
			print '<h2 class="text-red-600">NFT #'.$id.' not found</h2>';
			print '<p>Return to <a href="/admin/nft_list/">NFT list</a> and try again.</p>';
		}
	}
	else{
		$filter_address=false;
		$filter_address_caption='';
		if(isset($_GET['address'])){
			if(''!==$_GET['address']){
				$addresses=$db->sql("SELECT * FROM `addresses` WHERE `address`='".$db->prepare($_GET['address'])."'");
				if($addresses->num_rows){
					$filter_address=[];
					foreach($addresses as $address_arr){
						$filter_address[]=$address_arr['id'];
					}
					if(1==count($filter_address)){
						$filter_address_caption=$_GET['address'];
					}
					elseif(count($filter_address)){
						$filter_address_caption='Addresses: '.count($filter_address);
					}
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>Address '.htmlspecialchars($_GET['address']).' not found</p>
					</div>';
				}
			}
		}
		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($nft_status[$filter_status])){
					$filter_status=false;
				}
			}
		}
		$filter_contract=false;
		if(isset($_GET['contract'])){
			if(''!==$_GET['contract']){
				$filter_contract=htmlspecialchars($_GET['contract']);
			}
		}
		$filter_token_id=false;
		if(isset($_GET['token_id'])){
			if(''!==$_GET['token_id']){
				$filter_token_id=htmlspecialchars($_GET['token_id']);
			}
		}
		$filter_chain=false;
		if(isset($_GET['chain'])){
			if(''!==$_GET['chain']){
				$chain_arr=$db->sql("SELECT * FROM `chains` WHERE `id`='".$db->prepare($_GET['chain'])."'");
				if(null!==$chain_arr){
					$filter_chain=(int)$_GET['chain'];
				}
				else{
					print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Chain '.htmlspecialchars($_GET['chain']).' not found</p>
					</div>';
				}
			}
		}

		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
					foreach($nft_status as $nft_status_id=>$nft_status_caption){
						print '<option value="'.$nft_status_id.'"'.($nft_status_id===$filter_status?' selected':'').'>Status: '.htmlspecialchars($nft_status_caption).'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="chain" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_chain?' selected':'').'>Filter by chain</option>';
					foreach($types_arr as $type_id=>$type_arr){
						print '<optgroup label="'.$type_id.' ('.htmlspecialchars($type_arr['name']).')">';
						$chains=$db->sql("SELECT * FROM `chains` WHERE `type`='".$db->prepare($type_id)."'");
						foreach($chains as $chain_arr){
							print '<option value="'.$chain_arr['id'].'"'.($chain_arr['id']==$filter_chain?' selected':'').'>Chain: '.htmlspecialchars($chain_arr['name']).' #'.$chain_arr['chain_id'].'</option>';
						}
						print '</optgroup>';
					}
					print '
				</select>
			</div>
			<div>
				<input name="address" class="form-post-by-enter" placeholder="Address query" value="'.htmlspecialchars($filter_address_caption).'">
			</div>
			<div>
				<input name="contract" class="form-post-by-enter" placeholder="Contract (strict)" value="'.htmlspecialchars($filter_contract).'">
			</div>
			<div>
				<input name="token_id" class="form-post-by-enter" placeholder="Token ID (strict)" value="'.htmlspecialchars($filter_token_id).'">
			</div>
		';
		print '</div>';
		print '</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
		print '<table class="table-auto min-w-full">';
		print '<thead class="bg-white border-b">';
		print '<tr>';
		print '<th>ID</th>';
		print '<th>Type</th>';
		print '<th>Chain</th>';
		print '<th>Address</th>';
		print '<th>Contract</th>';
		print '<th>Token ID</th>';
		print '<th>Status</th>';
		print '<th>Status time</th>';
		print '</tr>';
		print '</thead>';
		print '<tbody>';
		$sql_addon=[];
		if($filter_address){
			$sql_addon[]='`address` IN ('.implode(',',$filter_address).')';
		}
		if(false!==$filter_contract){
			$sql_addon[]='`contract`=\''.$db->prepare($filter_contract).'\'';
		}
		if(false!==$filter_token_id){
			$sql_addon[]='`token_id`=\''.$db->prepare($filter_token_id).'\'';
		}
		if(false!==$filter_status){
			$sql_addon[]='`status`='.$filter_status;
		}
		if(false!==$filter_chain){
			$sql_addon[]='`chain`='.$filter_chain;
		}
		$sql_addon_str='';
		if(count($sql_addon)){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		$per_page=50;
		$count=$db->table_count('nft_list',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}
		$nft_list=$db->sql("SELECT * FROM `nft_list`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($nft_list as $nft_arr){
			print '<tr>';
			print '<td><a href="/admin/nft_list/'.$nft_arr['id'].'/">'.$nft_arr['id'].'</a></td>';
			print '<td>'.$nft_arr['type'].' ('.htmlspecialchars($types_arr[$nft_arr['type']]['name']).')</td>';
			$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$nft_arr['chain']."'");
			print '<td><a href="/admin/chains/edit/'.$chain_arr['id'].'/">'.$chain_arr['id'].'</a> ('.htmlspecialchars($chain_arr['short']).' #'.$chain_arr['chain_id'].')</td>';
			print '<td><a href="/admin/addresses/edit/'.$nft_arr['address'].'/">'.$nft_arr['address'].'</a> ('.htmlspecialchars(get_address($nft_arr['address'])).')</td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($nft_arr['contract']).'</div></td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($nft_arr['token_id']).'</div></td>';
			print '<td class="'.$nft_status_class[$nft_arr['status']].'">'.$nft_status[$nft_arr['status']].'</td>';
			print '<td>'.date('d.m.Y H:i:s',$nft_arr['status_time']).'</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('addresses'==$path_array[2]){
	$replace['title']='Addresses | '.$replace['title'];
	/*
	Address structure:
	- id
	- type (from types table)
	- address (0x...)
	- caption (for string search)
	- time (create time)
	- status (1 if admin)
	- update (need to update nft list for all chains same type for this address)
	- update_time
	*/
	if('delete'==$path_array[3]){
		$address_id=intval($path_array[4]);
		$address_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$address_id."'");
		if(null!=$address_arr){
			if(isset($_POST['approve'])){
				$db->sql("DELETE FROM `auth_addresses` WHERE `address`='".$address_id."'");

				//dublicated block (from oracle_cron script) for deactivate all addresses nft from nft_list
				//get nft_list with status 2 (deactivation)
				$nft_list=$db->sql("SELECT * FROM `nft_list` WHERE `address`='".$address_id."'");
				foreach($nft_list as $nft_arr){
					//check nft for binded_nft
					$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$nft_arr['id']."'");
					if(null==$binded_nft_arr){
						//binded not finded, already deactivated, ignore
					}
					else{
						$whitelist_arr=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `id`='".$binded_nft_arr['whitelist']."'");
						if(null!=$whitelist_arr){
							//deactivate event_users
							$db->sql("UPDATE `event_users` SET `status`=0, `binded`=0, `level`=0 WHERE `event`='".$whitelist_arr['event']."' AND `address`='".$binded_nft_arr['address']."'");

							//deactivate event_invites for event_users
							$db->sql("UPDATE `event_invites` SET `status`=0, `time`=".time()." WHERE `event`='".$whitelist_arr['event']."' AND `address`='".$binded_nft_arr['address']."'");

							//deactivate event_whitelist
							$db->sql("UPDATE `event_whitelist` SET `binded_nft`=0 WHERE `id`='".$binded_nft_arr['whitelist']."'");
						}
						/*
						//send notification to linked_platforms for user address
						$linked_platforms_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$binded_nft_arr['address']."'");
						foreach($linked_platforms_arr as $linked_platforms_item){
							//...send notification to platform_queue
						}
						*/
						//binded finded, remove it
						$db->sql("DELETE FROM `binded_nft` WHERE `id`='".$binded_nft_arr['id']."'");
					}
					//remove nft after deactivation
					$db->sql("DELETE FROM `nft_list` WHERE `id`='".$nft_arr['id']."'");
				}
				//remove any binded nft for this address
				$db->sql("DELETE FROM `binded_nft` WHERE `address`='".$address_id."'");
				$db->sql("DELETE FROM `nft_list` WHERE `address`='".$address_id."'");
				$db->sql("DELETE FROM `oracle_updates` WHERE `address`='".$address_id."'");

				//remove linked_platforms for this address (add notification to platform_queue if needed)
				$db->sql("DELETE FROM `linked_platforms` WHERE `address`='".$address_id."'");

				$db->sql("DELETE FROM `addresses` WHERE `id`='".$address_id."'");
				header('location:/admin/addresses/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/addresses/">&larr; Back to addresses</a>';
				print '<h2 class="text-red-600">Remove address #'.$address_arr['id'].'</h2>';
				$nft_list_count=$db->table_count('nft_list',"WHERE `address`='".$address_arr['id']."'");
				$oracle_updates_count=$db->table_count('oracle_updates',"WHERE `address`='".$address_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Address '.htmlspecialchars($address_arr['address']).' will be removed from the database.</p>';
				if($nft_list_count){
					print '<p>Also will be deleted: '.$nft_list_count.' items from NFT List.</p>';
				}
				if($oracle_updates_count){
					print '<p>Also will be deleted: '.$oracle_updates_count.' items from Oracle Updates.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove address" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/addresses/">&larr; Back to addresses</a>';
			print '<h2 class="text-red-600">Address not found</h2>';
			print '<p>Return to addresses list and try again.</p>';
		}
	}
	elseif('edit'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/addresses/">&larr; Back to addresses</a>';
		print '<h2>Edit address</h2>';
		$address_id=(int)$path_array[4];
		if($address_id){
			$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$address_id."'");
			if(null!=$address_arr){
				if(isset($_POST['status'])){
					$errors=[];
					$status=(int)$_POST['status'];
					if(!isset($addresses_status_arr[$status])){
						$errors[]='Invalid status';
					}
					$update=(int)$_POST['update'];
					if(!isset($addresses_update_arr[$update])){
						$errors[]='Invalid update';
					}
					$caption=trim($_POST['caption']);
					if(0==count($errors)){
						$db->sql("UPDATE `addresses` SET
							`caption`='".$db->prepare($caption)."',
							`status`='".$status."',
							`update`='".$update."'
							WHERE `id`='".$address_id."'
						");
						print '
						<div class="success-box" role="alert">
							<p class="font-bold">Success</p>
							<p>Address <a href="/admin/addresses/edit/'.$address_id.'/">"'.htmlspecialchars($address_arr['address']).'"</a> was updated</p>
						</div>';
					}
					else{
						print '<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>'.implode('<br>',$errors).'</p>
						</div>';
						print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
					}
				}
				else{
					print '<form action="" method="POST" class="manage-card">';
					print '<div class="max-w-fit">';
						print '<div class="my-4">Type: '.htmlspecialchars($types_arr[$address_arr['type']]['name']).'</div>';
						print '<div class="my-4">Address: '.htmlspecialchars($address_arr['address']).'</div>';

						print '<div class="my-4"><input type="text" name="caption" placeholder="Caption" value="'.htmlspecialchars($address_arr['caption']).'"> &mdash; Caption (optional)</div>';

						print '<div class="my-4">Added: '.date('d.m.Y H:i',$address_arr['time']).'</div>';
						print '<div class="my-4">Last update: '.date('d.m.Y H:i',$address_arr['update_time']).'</div>';
						print '<div class="my-4">Status:<br><select name="status" required>';
						foreach($addresses_status_arr as $k=>$v){
							print '<option value="'.$k.'" '.($k==$address_arr['status']?'selected':'').' class="'.$addresses_status_arr_class[$k].'">'.$v.'</option>';
						}
						print '</select></div>';
						print '<div class="my-4">Update NFT list:<br><select name="update">';
						foreach($addresses_update_arr as $k=>$v){
							print '<option value="'.$k.'" '.($k==$address_arr['update']?'selected':'').' class="'.$addresses_update_arr_class[$k].'">'.$v.'</option>';
						}
						print '</select></div>';
					print '</div>';
					print '<div class="grid-wrapper">';
					print '<input type="submit" value="Save changes" class="action-btn big">';
					print '</div>';
					print '</form>';

					if(0!=$db->table_count('organizer_addresses',"WHERE `address`='".$address_id."'")){
						print '<h2 class="mt-6">Administrator for organizers</h2>';
						print '<div class="flex flex-col">
						<div class="table-wrapper">
						<div class="py-2 inline-block min-w-full">
						<div class="overflow-hidden">';
						print '<table class="table-auto min-w-full">';
						print '<thead class="bg-white border-b">';
						print '<tr>';
						print '<th>URL</th>';
						print '<th>Title</th>';
						print '<th>Description</th>';
						print '<th>Status</th>';
						print '</tr>';
						print '</thead>';
						print '<tbody>';
						$organizer_addresses_arr=$db->sql("SELECT * FROM `organizer_addresses` WHERE `address`='".$address_id."' ORDER BY `id` ASC");
						foreach($organizer_addresses_arr as $k=>$v){
							print '<tr>';
							$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$v['organizer']."'");

							print '<td class="whitespace-nowrap">
								<a href="/admin/organizers/edit/'.$organizer_arr['id'].'/">'.htmlspecialchars($organizer_arr['url']).'</a>
								<a href="/@'.htmlspecialchars($organizer_arr['url']).'/" target="_blank" class="text-blue-500 !no-underline">⧉</a>
							</td>';
							print '<td>'.htmlspecialchars($organizer_arr['title']).'</td>';
							print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($organizer_arr['description']).'</div></td>';
							print '<td class="'.$organizer_addresses_status_arr_class[$v['status']].'">'.$organizer_addresses_status_arr[$v['status']].'</td>';
							print '</tr>';
						}
						print '</tbody>';
						print '</table>';
						print '</div>';
						print '</div>';
						print '</div>';
					}
				}
			}
			else{
				print '<h2 class="text-red-600">Address not found</h2>';
				print '<p>Return to addresses list and try again.</p>';
			}
		}
		else{
			print '<h2 class="text-red-600">Address not found</h2>';
			print '<p>Return to addresses list and try again.</p>';
		}
	}
	else{
		$filter_query=false;
		if(isset($_GET['query'])){
			if(''!==$_GET['query']){
				$filter_query=trim($_GET['query']);
			}
		}
		$filter_type=false;
		if(isset($_GET['type'])){
			if(''!==$_GET['type']){
				$filter_type=(int)$_GET['type'];
				if(!isset($types_arr[$filter_type])){
					$filter_type=false;
				}
			}
		}
		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($addresses_status_arr[$filter_status])){
					$filter_status=false;
				}
			}
		}

		//add form with filters: by type. by status (select), by caption (string)
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="type" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_type?' selected':'').'>Filter by type</option>';
					foreach($types_arr as $type_id=>$type_arr){
						print '<option value="'.$type_id.'"'.($type_id===$filter_type?' selected':'').'>Type: '.$type_id.' ('.htmlspecialchars($type_arr['name']).')</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">';
				print '<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
				foreach($addresses_status_arr as $status_id=>$status_name){
					print '<option value="'.$status_id.'"'.($filter_status===$status_id?' selected':'').' class="'.$addresses_status_arr_class[$status_id].'">'.$status_name.'</option>';
				}
			print '</select>
			</div>
			<div>
				<input name="query" class="form-post-by-enter" placeholder="Search" value="'.($filter_query?htmlspecialchars($filter_query):'').'">
			</div>';
		print '</div>';
		print '</form>';

		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
		print '<table class="table-auto min-w-full">';
		print '<thead class="bg-white border-b">';
		print '<tr>';
		print '<th>Actions</th>';
		print '<th>ID</th>';
		print '<th>Type</th>';
		print '<th>Address</th>';
		print '<th>Caption</th>';
		print '<th>Added</th>';
		print '<th>Status</th>';
		print '<th>Update</th>';
		print '<th>Last update</th>';
		print '<th>NFT count</th>';
		print '</tr>';
		print '</thead>';
		print '<tbody>';
		$sql_addon=[];
		if($filter_query){
			$sql_addon[]="`address` LIKE '%".$db->prepare($filter_query)."%' OR `caption` LIKE '%".$db->prepare($filter_query)."%'";
		}
		if($filter_type){
			$sql_addon[]="`type`='".$db->prepare($filter_type)."'";
		}
		if(false!==$filter_status){
			$sql_addon[]="`status`='".$db->prepare($filter_status)."'";
		}
		$sql_addon_str='';
		if(count($sql_addon)){
			$sql_addon_str='WHERE '.implode(' AND ',$sql_addon);
		}

		$per_page=50;
		$count=$db->table_count('addresses',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}
		$addresses_arr=$db->sql("SELECT * FROM `addresses`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($addresses_arr as $address_arr){
			print '<tr>';
			print '<td><a href="/admin/addresses/delete/'.$address_arr['id'].'/" class="red-btn">Delete</a></td>';
			print '<td>'.$address_arr['id'].'</td>';
			print '<td>'.$address_arr['type'].' ('.htmlspecialchars($types_arr[$address_arr['type']]['name']).')</td>';
			print '<td><a href="/admin/addresses/edit/'.$address_arr['id'].'/">'.htmlspecialchars($address_arr['address']).'</a></td>';
			print '<td>'.htmlspecialchars($address_arr['caption']).'</td>';
			print '<td>'.date('d.m.Y H:i',$address_arr['time']).'</td>';
			print '<td class="'.$addresses_status_arr_class[$address_arr['status']].'">'.$addresses_status_arr[$address_arr['status']].'</td>';
			$nft_list_count=$db->table_count('nft_list',"WHERE `address`='".$address_arr['id']."'");
			print '<td class="'.$addresses_update_arr_class[$address_arr['update']].'">'.$addresses_update_arr[$address_arr['update']].'</td>';
			print '<td>'.date('d.m.Y H:i',$address_arr['update_time']).'</td>';
			print '<td><a href="/admin/nft_list/?address='.$address_arr['address'].'">'.$nft_list_count.'</a></td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('events'==$path_array[2]){
	$replace['title']='Events | '.$replace['title'];
	if('upload_content'==$path_array[3]){
		ob_end_clean();
		$dir='/files/events/';
		if(!file_exists($root_dir.$dir)){
			mkdir($root_dir.$dir,0777,true);
		}
		$errors=[];
		$files=[];
		$upload_name='upload';
		if(isset($_FILES[$upload_name]) && 0==$_FILES[$upload_name]['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png'];
			$ext=pathinfo($_FILES[$upload_name]['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES[$upload_name]['size']<=1024*1024*2){//check size 2MB
					$upload=md5($_FILES[$upload_name]['name']).'_'.time().'.'.$ext;
					$filename=$dir.$upload;
					move_uploaded_file($_FILES[$upload_name]['tmp_name'],$root_dir.$filename);
					http_response_code(200);
					print json_encode(['url'=>$filename]);

					$files[]=['time'=>time(),'name'=>$_FILES[$upload_name]['name'],'size'=>$_FILES[$upload_name]['size'],'type'=>$_FILES[$upload_name]['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2,'target_id'=>0];//target=event
					foreach($files as $file){
						$db->sql("INSERT INTO `files` SET `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$file['target_id']."',`address`='".$auth['status_address_id']."'");
					}
				}
				else{
					$errors[]='Image size must be less than 2MB';
				}
			}
			else{
				$errors[]='Image must be in JPG, JPEG or PNG format';
			}
		}
		else{
			$errors[]='Image not uploaded';
		}
		if(count($errors)){
			http_response_code(400);
			print json_encode(['error'=>['message'=>implode("\n",$errors)]]);
		}
		exit;
	}
	else
	if('approve'==$path_array[3]){
		$event_id=(int)$path_array[4];
		$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."'");
		if(null!=$event_arr){
			$rejected_permamently=false;
			if(2==$event_arr['moderation']){//already rejected
				if(($config['event_moderation_max_rejects']-1)<$event_arr['reject_counter']){
					$rejected_permamently=true;
				}
			}
			if($rejected_permamently){//already rejected
				print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
				print '<h2 class="text-red-600">Event rejected permamently</h2>';
				if(''!=$event_arr['reject_comment']){
					print '<p>Reject comment:<br>'.nl2br($event_arr['reject_comment']).'</p>';
				}
				print '<p>Return to event list and try again.</p>';
			}
			else{
				if(isset($_POST['approve'])){
					$db->sql("UPDATE `events` SET `moderation`=1, `moderation_time`='".time()."', `moderation_address`='".$auth['status_address_id']."' WHERE `id`='".$event_id."'");

					//get all administators and send them notification about approved event
					$org_administation=$db->sql("SELECT `address` FROM `organizer_addresses` WHERE `organizer`='".$event_arr['organizer']."'");
					foreach($org_administation as $org_administator){
						add_notify($org_administator['address'],0,1,'event_was_approved',json_encode(['organizer_url'=>htmlspecialchars($organizer_arr['url']),'event_url'=>htmlspecialchars($event_arr['url']),'event_title'=>$event_arr['title']]));
					}
					header('location:/admin/events/');
					exit;
				}
				else{
						print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
						print '<h2 class="text-green-600">Approve the event #'.$event_arr['id'].'</h2>';
						print '
						<div class="attention-box" role="alert">
							<p class="font-bold">Warning</p>
							<p>The event will be moderated as approved and can be viewed publicly.</p>
						</div>';
						print '<form action="" method="POST" class="manage-card">';
						print '<input type="submit" name="approve" value="Approve the moderation of the event" class="green-btn">';
						print '</form>';
				}
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
			print '<h2 class="text-red-600">Event not found</h2>';
			print '<p>Return to event list and try again.</p>';
		}
	}
	else
	if('reject'==$path_array[3]){
		$event_id=(int)$path_array[4];
		$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."'");
		if(null!=$event_arr){
			if(1==$event_arr['moderation']){//already approved
				print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
				print '<h2 class="text-red-600">Event already approved</h2>';
				print '<p>Return to event list and try again.</p>';
			}
			else{
				if(isset($_POST['approve'])){
					$reject_comment=$event_arr['reject_comment'];
					if(isset($_POST['reject_comment'])){
						$reject_comment=htmlspecialchars($_POST['reject_comment']);
					}

					$only_update_comment=false;
					if(($config['event_moderation_max_rejects']-1)<$event_arr['reject_counter']){
						$only_update_comment=true;
					}

					if($only_update_comment){
						$db->sql("UPDATE `events` SET `reject_comment`='".$db->prepare($reject_comment)."' WHERE `id`='".$event_id."'");
					}
					else{
						//reset status to 0 (planning) and set moderation to 2 (rejected)
						$db->sql("UPDATE `events` SET `status`=0, `moderation`=2, `moderation_time`='".time()."', `moderation_address`='".$auth['status_address_id']."', `reject_comment`='".$db->prepare($reject_comment)."', `reject_counter`=`reject_counter`+1 WHERE `id`='".$event_id."'");
					}

					$notify_preset_name='event_was_rejected';
					if(''!=$reject_comment){
						$notify_preset_name='event_was_rejected_with_comment';
					}
					$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");
					//get all administators and send them notification about approved event
					$org_administation=$db->sql("SELECT `address` FROM `organizer_addresses` WHERE `organizer`='".$event_arr['organizer']."'");
					foreach($org_administation as $org_administator){
						add_notify($org_administator['address'],0,0/*type 0=text,1=html*/,$notify_preset_name,json_encode(['organizer_url'=>htmlspecialchars($organizer_arr['url']),'event_url'=>htmlspecialchars($event_arr['url']),'event_title'=>$event_arr['title'],'comment'=>$reject_comment]));
					}

					header('location:/admin/events/');
					exit;
				}
				else{
					print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
					print '<h2 class="text-red-600">Reject the event #'.$event_arr['id'].'</h2>';
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Warning</p>
						<p>The event will be moderated as rejected and cannot be viewed publicly.</p>
					</div>';
					print '<form action="" method="POST" class="manage-card">';
					print '<p>Reject counter: '.$event_arr['reject_counter'].'</p>';
					if(($config['event_moderation_max_rejects']-1)==$event_arr['reject_counter']){
						print '<p class="text-red-600">The event will not have the opportunity to be moderated again.</p>';
					}if(($config['event_moderation_max_rejects']-1)<$event_arr['reject_counter']){
						print '<p class="text-red-600">The event not have the opportunity to be moderated.</p>';
					}

					print build_form(['reject_comment'=>['type'=>'textarea','title'=>'Reject comment','descr'=>'Visible to event manager']],$event_arr);

					$button_caption='Reject the moderation of the event';
					if(($config['event_moderation_max_rejects']-1)==$event_arr['reject_counter']){
						$button_caption='Reject the moderation of the event and block the event';
					}
					if(($config['event_moderation_max_rejects']-1)<$event_arr['reject_counter']){
						$button_caption='Update reject comment';
					}
					print '<input type="submit" name="approve" value="'.$button_caption.'" class="red-btn">';
					print '</form>';
				}
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
			print '<h2 class="text-red-600">Event not found</h2>';
			print '<p>Return to event list and try again.</p>';
		}
	}
	else
	if('hide'==$path_array[3]){
		$event_id=(int)$path_array[4];
		$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."'");
		if(null!=$event_arr){
			if(isset($_POST['approve'])){
				$db->sql("UPDATE `events` SET `status`=3 WHERE `id`='".$event_id."'");
				header('location:/admin/events/');
				exit;
			}
			else{
					print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
					print '<h2 class="text-red-600">Hide the event #'.$event_arr['id'].'</h2>';
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Warning</p>
						<p>Event will be hidden, it can be viewed only by platform admin.</p>
					</div>';
					print '<form action="" method="POST" class="manage-card">';
					print '<input type="submit" name="approve" value="Hide the event" class="red-btn">';
					print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
			print '<h2 class="text-red-600">Event not found</h2>';
			print '<p>Return to event list and try again.</p>';
		}
	}
	elseif('create'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
		print '<h2>Add event</h2>';
		if(isset($_POST['url'])){
			$errors=[];
			$files=[];
			$organizer_id=0;
			$organizer_arr=get_organizer((int)$_POST['organizer']);
			if(isset($organizer_arr['id'])){
				$organizer_id=$organizer_arr['id'];
			}
			else{
				$errors[]='Organizer not found';
			}

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			$upload_max_size=ini_get('upload_max_filesize');
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			$url=trim($_POST['url']);
			$url=mb_strtolower($url);
			$url=str_replace(' ','-',$url);
			$url=str_replace('/','',$url);
			$url=ru2lat($url);
			$url=preg_replace('/[^a-z0-9\-\_\.]/','',$url);
			$url=preg_replace('/\-+/','-',$url);
			$url=preg_replace('/\.+/','.',$url);
			if(in_array($url,$reserved_urls)){
				$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
			}
			//check if url is unique
			$find_event_url=$db->sql_row("SELECT * FROM `events` WHERE `organizer`='".$organizer_id."' AND `url`='".$db->prepare($url)."'");
			if(null!=$find_event_url){
				$errors[]='Event URL is not unique for this organizer';
			}

			$title=trim($_POST['title']);
			$short_description=trim($_POST['short_description']);
			$description=trim($_POST['description']);
			$short_location_description=trim($_POST['short_location_description']);
			$location_description=trim($_POST['location_description']);
			$speakers_description=trim($_POST['speakers_description']);
			$tickets_description=trim($_POST['tickets_description']);
			$check_in_message=trim($_POST['check_in_message']);

			$logo_url='';
			$cover_url='';
			$speakers_cover_url='';
			$location_cover_url='';
			$partners_cover_url='';

			$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$url.'/';
			if(!file_exists($root_dir.$dir)){
				mkdir($root_dir.$dir,0777,true);
			}
			if(isset($_FILES['logo']) && 0==$_FILES['logo']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['logo']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['logo']['size']<=1024*1024*1){//check size 1MB
						$logo=md5($url).'_logo_'.time().'.'.$ext;
						$filename=$dir.$logo;
						move_uploaded_file($_FILES['logo']['tmp_name'],$root_dir.$filename);
						$logo_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>$_FILES['logo']['size'],'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Logo size must be less than 1MB';
					}
				}
				else{
					$errors[]='&mdash; Logo must be in JPG, JPEG, PNG or SVG format';
				}
			}
			if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
						$cover=md5($url).'_cover_'.time().'.'.$ext;
						$filename=$dir.$cover;
						move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
						$cover_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Cover must be in JPG, JPEG, PNG or SVG format';
				}
			}
			if(isset($_FILES['speakers_cover']) && 0==$_FILES['speakers_cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['speakers_cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['speakers_cover']['size']<=1024*1024*2){//check size 2MB
						$speakers_cover=md5($url).'_speakers_cover_'.time().'.'.$ext;
						$filename=$dir.$speakers_cover;
						move_uploaded_file($_FILES['speakers_cover']['tmp_name'],$root_dir.$filename);
						$speakers_cover_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['speakers_cover']['name'],'size'=>$_FILES['speakers_cover']['size'],'type'=>$_FILES['speakers_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Speakers cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Speakers cover must be in JPG, JPEG, PNG or SVG format';
				}
			}
			if(isset($_FILES['location_cover']) && 0==$_FILES['location_cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['location_cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['location_cover']['size']<=1024*1024*2){//check size 2MB
						$location_cover=md5($url).'_location_cover_'.time().'.'.$ext;
						$filename=$dir.$location_cover;
						move_uploaded_file($_FILES['location_cover']['tmp_name'],$root_dir.$filename);
						$location_cover_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['location_cover']['name'],'size'=>$_FILES['location_cover']['size'],'type'=>$_FILES['location_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Location cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Location cover must be in JPG, JPEG, PNG or SVG format';
				}
			}
			if(isset($_FILES['partners_cover']) && 0==$_FILES['partners_cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png','svg'];
				$ext=pathinfo($_FILES['partners_cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['partners_cover']['size']<=1024*1024*2){//check size 2MB
						$partners_cover=md5($url).'_partners_cover_'.time().'.'.$ext;
						$filename=$dir.$partners_cover;
						move_uploaded_file($_FILES['partners_cover']['tmp_name'],$root_dir.$filename);
						$partners_cover_url=$filename;

						$files[]=['time'=>time(),'name'=>$_FILES['partners_cover']['name'],'size'=>$_FILES['partners_cover']['size'],'type'=>$_FILES['partners_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
					}
					else{
						$errors[]='&mdash; Partners cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Partners cover must be in JPG, JPEG, PNG or SVG format';
				}
			}

			$time=0;
			if(isset($_POST['unixtime'])){
				$time=(int)$_POST['unixtime'];
			}
			$status=0;
			if(isset($events_status_arr[$_POST['status']])){
				$status=(int)$_POST['status'];
			}
			$moderation=0;
			if(isset($_POST['moderation'])){
				if(isset($events_moderation_arr[$_POST['moderation']])){
					$moderation=(int)$_POST['moderation'];
				}
			}
			if(false===$config['event_moderation']){//if moderation is disabled
				$moderation=1;//auto approve
			}

			if(0!=$db->table_count('events',"WHERE `organizer`='".$organizer_id."' AND `url`='".$db->prepare($url)."'")){
				$errors[]='Event with the same url was found. Please choose another url.';
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `events` (`organizer`,`url`,`title`,`short_description`,`description`,`short_location_description`,`location_description`,`speakers_description`,`tickets_description`,`check_in_message`,`logo_url`,`cover_url`,`speakers_cover_url`,`location_cover_url`,`partners_cover_url`,`time`,`status`,`moderation`) VALUES ('".$organizer_id."','".$db->prepare($url)."','".$db->prepare($title)."','".$db->prepare($short_description)."','".$db->prepare($description)."','".$db->prepare($short_location_description)."','".$db->prepare($location_description)."','".$db->prepare($speakers_description)."','".$db->prepare($tickets_description)."','".$db->prepare($check_in_message)."','".$db->prepare($logo_url)."','".$db->prepare($cover_url)."','".$db->prepare($speakers_cover_url)."','".$db->prepare($location_cover_url)."','".$db->prepare($partners_cover_url)."','".$time."','".$status."','".$moderation."')");
				$event_id=$db->last_id();

				//add files to database
				foreach($files as $file){
					$db->sql("INSERT INTO `files` SET `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$event_id."',`address`='".$auth['status_address_id']."'");
				}

				//get all organizer administators and send them notification about new event
				$org_administation=$db->sql("SELECT `address` FROM `organizer_addresses` WHERE `organizer`='".$organizer_id."'");
				foreach($org_administation as $org_administator){
					add_notify($org_administator['address'],0,1,'org_new_event',json_encode([
						'organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url']),
						'event_title'=>htmlspecialchars($title),
						'event_url'=>htmlspecialchars($url)
					]));
				}

				if($config['event_moderation']){//if moderation is enabled
					//get all admins and send them notification about new event
					$platform_admins=$db->sql("SELECT `id` FROM `addresses` WHERE `status`='1'");
					foreach($platform_admins as $platform_admin){
						add_notify($platform_admin['id'],0,1,'admin_new_event',json_encode([
							'organizer_title'=>htmlspecialchars($organizer_arr['title']),'organizer_url'=>htmlspecialchars($organizer_arr['url']),
							'event_title'=>htmlspecialchars($title),
							'event_url'=>htmlspecialchars($url)
						]));
					}
				}

				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Event <a href="/admin/events/edit/'.$event_id.'/">'.htmlspecialchars($url).'</a> was created for organizer <a href="/admin/organizers/edit/'.$organizer_arr['id'].'/">'.htmlspecialchars($organizer_arr['url']).'</a></p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/admin/events/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

				//deleting temporarily uploaded files
				foreach($files as $file){
					unlink($file['path']);
				}
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
			print '<div class="max-w-fit">';
			$object_scheme=[//admin create event
				'organizer'=>[
					'type'=>'select',
					'title'=>'Organizer',
					'options'=>$db->sql("SELECT `id`,`url`,`title` FROM `organizers` WHERE `status`=1 ORDER BY `url` ASC"),
					'required'=>true,
					'key'=>'id',
					'default_option'=>$event_arr['organizer'],
					'caption_function'=>function($option,$object){
						return htmlspecialchars($object['url']).' ('.htmlspecialchars($object['title']).')';
					}
				],
				'url'=>['type'=>'text','title'=>'URL','class'=>'url-part with-uppercase','descr'=>'Used for /@'.$organizer_url.'/<b>URL</b>/ page address bar.','required'=>true],
				'title'=>['type'=>'text','title'=>'Title','class'=>'title-part','required'=>true],
				'short_description'=>['type'=>'textarea','title'=>'Short description','descr'=>'Annotation for preview & seo'],

				'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'Description (annotation)','wysiwyg'=>true],

				'short_location_description'=>['type'=>'text','title'=>'Short location description','descr'=>'For event preview card'],
				'location_description'=>['type'=>'textarea','title'=>'📄 Location description','placeholder'=>'Location description','wysiwyg'=>true],

				'speakers_description'=>['type'=>'textarea','title'=>'📄 Speakers description','placeholder'=>'Speakers description','wysiwyg'=>true],
				'tickets_description'=>['type'=>'textarea','title'=>'📄 Tickets description','placeholder'=>'Tickets description','wysiwyg'=>true],
				'check_in_message'=>['type'=>'textarea','title'=>'📄 Check-in message','placeholder'=>'Check-in message','wysiwyg'=>true],

				'logo'=>[
					'type'=>'image',
					'title'=>'🖼️ Logo image',
					'placeholder'=>'Logo',
					'object_type'=>'event',
					'file_path'=>'logo_url',
					'descr'=>'The recommended sizes are <b>60x60</b> pixels, 500Kb, please do not exceed them.<br>For best quality use <b>svg</b> or <b>png</b> with transparent background.'
				],
				'sep1'=>['type'=>'hr'],
				'cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Cover image',
					'placeholder'=>'Cover',
					'object_type'=>'event',
					'file_path'=>'cover_url',
					'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
				],
				'speakers_cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Speakers cover image',
					'placeholder'=>'Speakers cover',
					'object_type'=>'event',
					'file_path'=>'speakers_cover_url',
					'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
				],
				'location_cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Location cover image',
					'placeholder'=>'Location cover',
					'object_type'=>'event',
					'file_path'=>'location_cover_url',
					'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
				],
				'partners_cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Partners cover image',
					'placeholder'=>'Partners cover',
					'object_type'=>'event',
					'file_path'=>'partners_cover_url',
					'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
				],
				'sep2'=>['type'=>'hr'],
				'datetime'=>[
					'key'=>'time',
					'type'=>'datetime',
					'local'=>true,//-local
					'future'=>true,
					'title'=>'Start date time (by your local time)',
					'timestamp'=>'unixtime',
					'value'=>0
				],
				'status'=>[
					'type'=>'select',
					'title'=>'Status',
					'options'=>$events_status_arr,
					'default_option'=>0,
					'classes'=>$events_status_style_arr,
				],
				/*
				'moderation'=>[
					'type'=>'select',
					'title'=>'Moderation',
					'options'=>$events_moderation_arr,
					'default_option'=>0,
					'classes'=>$events_moderation_style_arr,
				],
				*/
			];
			if($config['event_moderation']){//if moderation is enabled
				$object_scheme['moderation']=[
					'type'=>'select',
					'title'=>'Moderation',
					'options'=>$events_moderation_arr,
					'default_option'=>0,
					'classes'=>$events_moderation_style_arr,
				];
			}
			print build_form($object_scheme,$event_arr);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create event" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/events/">&larr; Back to events</a>';
		print '<h2>Edit event</h2>';
		$id=(int)$path_array[4];
		if($id){
			$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$id."'");
			if(null!=$event_arr){
				if(isset($_POST['url'])){
					$errors=[];
					$files=[];
					$remove_files=[];
					$organizer_id=0;
					$organizer_arr=get_organizer((int)$_POST['organizer']);
					if(isset($organizer_arr['id'])){
						$organizer_id=$organizer_arr['id'];
					}
					else{
						$errors[]='Organizer not found';
					}

					//https://www.php.net/manual/ru/features.file-upload.errors.php
					$upload_max_size=ini_get('upload_max_filesize');
					foreach($_FILES as $file_key=>$file_data){
						switch($file_data['error']){
							case UPLOAD_ERR_INI_SIZE:
								$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
								break;
							case UPLOAD_ERR_FORM_SIZE:
								$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
								break;
						}
					}

					$url=trim($_POST['url']);
					$url=mb_strtolower($url);
					$url=str_replace(' ','-',$url);
					$url=str_replace('/','',$url);
					$url=ru2lat($url);
					$url=preg_replace('/[^a-z0-9\-\_\.]/','',$url);
					$url=preg_replace('/\-+/','-',$url);
					$url=preg_replace('/\.+/','.',$url);
					if(in_array($url,$reserved_urls)){
						$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
					}
					//check if url is unique
					$find_event_url=$db->sql_row("SELECT * FROM `events` WHERE `organizer`='".$organizer_id."' AND `url`='".$db->prepare($url)."' AND `id`!='".$id."'");
					if(null!=$find_event_url){
						$errors[]='Event URL is not unique for this organizer';
					}

					$title=trim($_POST['title']);
					$short_description=trim($_POST['short_description']);
					$description=trim($_POST['description']);
					$short_location_description=trim($_POST['short_location_description']);
					$location_description=trim($_POST['location_description']);
					$speakers_description=trim($_POST['speakers_description']);
					$tickets_description=trim($_POST['tickets_description']);
					$check_in_message=trim($_POST['check_in_message']);

					$logo_url=$event_arr['logo_url'];
					$cover_url=$event_arr['cover_url'];
					$speakers_cover_url=$event_arr['speakers_cover_url'];
					$location_cover_url=$event_arr['location_cover_url'];
					$partners_cover_url=$event_arr['partners_cover_url'];

					$dir='/files/organizers/'.$organizer_arr['id'].'/events/'.$url.'/';
					if(isset($_FILES['logo']) && 0==$_FILES['logo']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['logo']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['logo']['size']<=1024*1024*1){//check size 2MB
								$logo=md5($url).'_logo_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename=$dir.$logo;
								move_uploaded_file($_FILES['logo']['tmp_name'],$root_dir.$filename);
								$logo_url=$filename;

								$remove_files[]=$event_arr['logo_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['logo']['name'],'size'=>$_FILES['logo']['size'],'type'=>$_FILES['logo']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Logo size must be less than 1MB';
							}
						}
						else{
							$errors[]='&mdash; Logo must be in JPG, JPEG, PNG or SVG format';
						}
					}
					if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
								$cover=md5($url).'_cover_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename='/files/events/'.$cover;
								move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
								$cover_url=$filename;

								$remove_files[]=$event_arr['cover_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Cover size must be less than 2MB';
							}
						}
						else{
							$errors[]='&mdash; Cover must be in JPG, JPEG, PNG or SVG format';
						}
					}
					if(isset($_FILES['speakers_cover']) && 0==$_FILES['speakers_cover']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['speakers_cover']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['speakers_cover']['size']<=1024*1024*2){//check size 2MB
								$speakers_cover=md5($url).'_speakers_cover_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename=$dir.$speakers_cover;
								move_uploaded_file($_FILES['speakers_cover']['tmp_name'],$root_dir.$filename);
								$speakers_cover_url=$filename;

								$remove_files[]=$event_arr['speakers_cover_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['speakers_cover']['name'],'size'=>$_FILES['speakers_cover']['size'],'type'=>$_FILES['speakers_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Speakers cover size must be less than 2MB';
							}
						}
						else{
							$errors[]='&mdash; Speakers cover must be in JPG, JPEG, PNG or SVG format';
						}
					}
					if(isset($_FILES['location_cover']) && 0==$_FILES['location_cover']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['location_cover']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['location_cover']['size']<=1024*1024*2){//check size 2MB
								$location_cover=md5($url).'_location_cover_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename=$dir.$location_cover;
								move_uploaded_file($_FILES['location_cover']['tmp_name'],$root_dir.$filename);
								$location_cover_url=$filename;

								$remove_files[]=$event_arr['location_cover_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['location_cover']['name'],'size'=>$_FILES['location_cover']['size'],'type'=>$_FILES['location_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Location cover size must be less than 2MB';
							}
						}
						else{
							$errors[]='&mdash; Location cover must be in JPG, JPEG, PNG or SVG format';
						}
					}
					if(isset($_FILES['partners_cover']) && 0==$_FILES['partners_cover']['error']){
						//check extensions
						$allowed_exts=['jpg','jpeg','png','svg'];
						$ext=pathinfo($_FILES['partners_cover']['name'],PATHINFO_EXTENSION);
						if(in_array($ext,$allowed_exts)){
							if($_FILES['partners_cover']['size']<=1024*1024*2){//check size 2MB
								$partners_cover=md5($url).'_partners_cover_'.time().'.'.$ext;
								if(!file_exists($root_dir.$dir)){
									mkdir($root_dir.$dir,0777,true);
								}
								$filename=$dir.$partners_cover;
								move_uploaded_file($_FILES['partners_cover']['tmp_name'],$root_dir.$filename);
								$partners_cover_url=$filename;

								$remove_files[]=$event_arr['partners_cover_url'];//remove old file
								$files[]=['time'=>time(),'name'=>$_FILES['partners_cover']['name'],'size'=>$_FILES['partners_cover']['size'],'type'=>$_FILES['partners_cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>2];
							}
							else{
								$errors[]='&mdash; Partners cover size must be less than 2MB';
							}
						}
						else{
							$errors[]='&mdash; Partners cover must be in JPG, JPEG, PNG or SVG format';
						}
					}

					$time=0;
					if(isset($_POST['unixtime'])){
						$time=(int)$_POST['unixtime'];
					}
					$status=0;
					if(isset($events_status_arr[$_POST['status']])){
						$status=(int)$_POST['status'];
					}
					$moderation=0;
					if(isset($_POST['moderation'])){
						if(isset($events_moderation_arr[$_POST['moderation']])){
							$moderation=(int)$_POST['moderation'];
						}
					}
					$reject_comment=$event_arr['reject_comment'];
					if(isset($_POST['reject_comment'])){
						$reject_comment=htmlspecialchars($_POST['reject_comment']);
					}
					/*
					//dont need on edit page
					if(false===$config['event_moderation']){//if moderation is disabled
						$moderation=1;//auto approve
					}
					*/

					if(0==count($errors)){
						$db->sql("UPDATE `events` SET `organizer`='".$organizer_id."', `url`='".$db->prepare($url)."',`title`='".$db->prepare($title)."',`short_description`='".$db->prepare($short_description)."',`description`='".$db->prepare($description)."',`short_location_description`='".$db->prepare($short_location_description)."',`location_description`='".$db->prepare($location_description)."',`speakers_description`='".$db->prepare($speakers_description)."',`tickets_description`='".$db->prepare($tickets_description)."',`check_in_message`='".$db->prepare($check_in_message)."',`time`='".$time."',`status`='".$status."',`logo_url`='".$db->prepare($logo_url)."',`cover_url`='".$db->prepare($cover_url)."',`speakers_cover_url`='".$db->prepare($speakers_cover_url)."',`location_cover_url`='".$db->prepare($location_cover_url)."',`partners_cover_url`='".$db->prepare($partners_cover_url)."',`moderation`='".$moderation."',`reject_comment`='".$db->prepare($reject_comment)."' WHERE `id`='".$id."'");
						print '
						<div class="success-box" role="alert">
							<p class="font-bold">Success</p>
							<p>Event <a href="/admin/events/edit/'.$id.'/">"'.htmlspecialchars($url).'"</a> was updated</p>
						</div>';
						print '<meta http-equiv="refresh" content="5; url=/admin/events/">';

						//deleting old files
						foreach($remove_files as $file){
							unlink($root_dir.$file);
							$db->sql("DELETE FROM `files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
						}
						//add files to database
						foreach($files as $file){
							$db->sql("INSERT INTO `files` SET `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$id."',`address`='".$auth['status_address_id']."'");
						}
					}
					else{
						print '
						<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>'.implode('<br>',$errors).'</p>
						</div>';
						print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

						//deleting temporarily uploaded files
						foreach($files as $file){
							unlink($file['path']);
						}
					}
				}
				else{
					print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
					print '<div class="max-w-fit">';
					$object_scheme=[//admin edit event
						'organizer'=>[
							'type'=>'select',
							'title'=>'Organizer',
							'options'=>$db->sql("SELECT `id`,`url`,`title`,`status` FROM `organizers` ORDER BY `url` ASC"),
							'required'=>true,
							'key'=>'id',
							'default_option'=>$event_arr['organizer'],
							'caption_function'=>function($option,$object){
								return (1==$object['status']?'✔️ ':'').htmlspecialchars($object['url']).' ('.htmlspecialchars($object['title']).')';
							}
						],
						'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /@'.$organizer_url.'/<b>URL</b>/ page address bar.','required'=>true],
						'title'=>['type'=>'text','title'=>'Title','class'=>'title-part','required'=>true],
						'short_description'=>['type'=>'textarea','title'=>'Short description','descr'=>'Annotation for preview & seo'],

						'description'=>['type'=>'textarea','title'=>'📄 Description','placeholder'=>'Description (annotation)','wysiwyg'=>true],

						'short_location_description'=>['type'=>'text','title'=>'Short location description','descr'=>'For event preview card'],
						'location_description'=>['type'=>'textarea','title'=>'📄 Location description','placeholder'=>'Location description','wysiwyg'=>true],

						'speakers_description'=>['type'=>'textarea','title'=>'📄 Speakers description','placeholder'=>'Speakers description','wysiwyg'=>true],
						'tickets_description'=>['type'=>'textarea','title'=>'📄 Tickets description','placeholder'=>'Tickets description','wysiwyg'=>true],
						'check_in_message'=>['type'=>'textarea','title'=>'📄 Check-in message','placeholder'=>'Check-in message','wysiwyg'=>true],

						'logo'=>[
							'type'=>'image',
							'title'=>'🖼️ Logo image',
							'placeholder'=>'Logo',
							'object_type'=>'event',
							'file_path'=>'logo_url',
							'descr'=>'The recommended sizes are <b>300x60</b> pixels, 500Kb, please do not exceed them.<br>For best quality use <b>svg</b> or <b>png</b> with transparent background.'
						],
						'sep1'=>['type'=>'hr'],
						'cover'=>[
							'type'=>'image',
							'title'=>'🖼️ Cover image',
							'placeholder'=>'Cover',
							'object_type'=>'event',
							'file_path'=>'cover_url',
							'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
						],
						'speakers_cover'=>[
							'type'=>'image',
							'title'=>'🖼️ Speakers cover image',
							'placeholder'=>'Speakers cover',
							'object_type'=>'event',
							'file_path'=>'speakers_cover_url',
							'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
						],
						'location_cover'=>[
							'type'=>'image',
							'title'=>'🖼️ Location cover image',
							'placeholder'=>'Location cover',
							'object_type'=>'event',
							'file_path'=>'location_cover_url',
							'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
						],
						'partners_cover'=>[
							'type'=>'image',
							'title'=>'🖼️ Partners cover image',
							'placeholder'=>'Partners cover',
							'object_type'=>'event',
							'file_path'=>'partners_cover_url',
							'descr'=>'The recommended size - <b>1280x720</b> pixels (16:9 ratio), 2Mb'
						],
						'sep2'=>['type'=>'hr'],
						'datetime'=>[
							'key'=>'time',
							'type'=>'datetime',
							'local'=>true,//-local
							'future'=>true,
							'title'=>'Start date time (by your local time)',
							'timestamp'=>'unixtime',
							'value'=>0
						],
						'status'=>[
							'type'=>'select',
							'title'=>'Status',
							'options'=>$events_status_arr,
							'default_option'=>0,
							'classes'=>$events_status_style_arr,
						],
						'moderation'=>[
							'type'=>'select',
							'title'=>'Moderation',
							'options'=>$events_moderation_arr,
							'default_option'=>0,
							'classes'=>$events_moderation_style_arr,
						],
						'reject_comment'=>['type'=>'textarea','title'=>'Reject comment','descr'=>'Visible to event manager if event moderation is rejected'],
					];
					print build_form($object_scheme,$event_arr);
					print '</div>';
					print '<div class="grid-wrapper">';
					print '<input type="submit" value="Update event" class="action-btn big">';
					print '</div>';
					print '</form>';
				}
			}
			else{
				print '<h2 class="text-red-600">Event not found</h2>';
				print '<p>Return to events list and try again.</p>';
			}
		}
		else{
			print '<h2 class="text-red-600">Event not found</h2>';
			print '<p>Return to events list and try again.</p>';
		}
	}
	else{
		print '<a class="action-btn" href="/admin/events/create/">Create event</a>';

		$filter_organizer=false;
		$filter_organizer_id=0;
		$filter_address_caption='';
		if(isset($_GET['organizer'])){
			if(''!==$_GET['organizer']){
				$organizer_arr=$db->sql_row("SELECT `id`,`url` FROM `organizers` WHERE `url`='".$db->prepare($_GET['organizer'])."' OR `title`='".$db->prepare($_GET['organizer'])."'");
				if(null!=$organizer_arr){
					$filter_organizer=$organizer_arr['url'];
					$filter_organizer_id=$organizer_arr['id'];
				}
			}
		}

		$filter_status=false;
		if(isset($_GET['status'])){
			$filter_status=intval($_GET['status']);
			if(''==$_GET['status']){
				$filter_status=false;
			}
		}
		$filter_moderation=false;
		$filter_moderation_reverse=false;
		if(isset($_GET['moderation'])){
			if('!'==$_GET['moderation'][0]){
				$filter_moderation_reverse=true;
				$_GET['moderation']=substr($_GET['moderation'],1);
			}
			$filter_moderation=intval($_GET['moderation']);
			if(''==$_GET['moderation']){
				$filter_moderation=false;
			}
		}
		print '<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="organizer" class="form-post-by-enter" placeholder="Search by organizer" value="'.($filter_organizer?htmlspecialchars($filter_organizer):'').'">
			</div>
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">';
				print '<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
				foreach($events_status_arr as $status_id=>$status_name){
					print '<option value="'.$status_id.'"'.($filter_status===$status_id?' selected':'').' class="'.$events_status_style_arr[$status_id].'">'.$status_name.'</option>';
				}
			print '</select>
			</div>';
		if($config['event_moderation']){
			print '
			<div>
				<select name="moderation" onchange="$(this).closest(\'form\')[0].submit()">';
				print '<option value=""'.(false===$filter_moderation?' selected':'').'>Filter by moderation</option>';
				foreach($events_moderation_arr as $moderation_id=>$moderation_name){
					print '<option value="'.$moderation_id.'"'.($filter_moderation===$moderation_id?' selected':'').' class="'.$events_status_style_arr[$moderation_id].'">'.$moderation_name.'</option>';
				}
				if($filter_moderation_reverse){
					print '<option value="!'.$filter_moderation.'" selected class="'.$events_status_style_arr[$filter_moderation].'">NOT '.$events_moderation_arr[$filter_moderation].'</option>';
				}
			print '</select>
			</div>';
		}
		print '
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Action</th>';

			print '<th>Organizer</th>';
			print '<th>URL</th>';
			print '<th>Title</th>';

			print '<th>Start</th>';

			print '<th>Status</th>';
			if($config['event_moderation']){
				print '<th>Moderation</th>';
				print '<th>Reject comment</th>';
			}
			print '<th>Sessions</th>';
			print '<th>Files size</th>';

			print '<th>Description</th>';
			print '<th>Location descr.</th>';
			print '<th>Speakers descr.</th>';
			print '<th>Tickets descr.</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		if(false!==$filter_status){
			$sql_addon[]="`status`='$filter_status'";
		}
		if(false!==$filter_moderation){
			if($filter_moderation_reverse){
				$sql_addon[]="`moderation`!='$filter_moderation'";
			}
			else{
				$sql_addon[]="`moderation`='$filter_moderation'";
			}
		}
		if(false!==$filter_organizer){
			$sql_addon[]="`organizer`='$filter_organizer_id'";
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}

		$per_page=50;
		$count=$db->table_count('events',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$events=$db->sql("SELECT * FROM `events`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($events as $event_arr){
			$organizer_arr=get_organizer($event_arr['organizer']);
			print '<tr>';
			print '<td>';
			print '<a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/manage/" class="violet-btn mr-4 mb-2">Manage</a>';
			if(1==$event_arr['moderation']){
				print '<a href="/admin/events/hide/'.$event_arr['id'].'/" class="red-btn mr-4 mb-2">Hide event</a>';
			}
			if(1!=$event_arr['moderation']){//not approved
				print '<a href="/admin/events/approve/'.$event_arr['id'].'/" class="green-btn mr-4 mb-2">Approve</a>';
				print '<a href="/admin/events/reject/'.$event_arr['id'].'/" class="red-btn">Reject</a>';
			}
			print '</td>';

			print '<td class="whitespace-nowrap">
				<a href="/admin/organizers/edit/'.$organizer_arr['id'].'/">'.htmlspecialchars($organizer_arr['url']).'</a>
				<a href="/@'.htmlspecialchars($organizer_arr['url']).'/" target="_blank" class="text-blue-500 !no-underline">⧉</a>
			</td>';
			print '<td class="whitespace-nowrap">
				<a href="/admin/events/edit/'.$event_arr['id'].'/">'.htmlspecialchars($event_arr['url']).'</a>
				<a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/" target="_blank" class="text-blue-500 !no-underline">⧉</a>
			</td>';

			print '<td>'.htmlspecialchars($event_arr['title']).'</td>';

			if($event_arr['time']){
				print '<td><span class="time" data-time="'.$event_arr['time'].'">'.date('d.m.Y H:i',$event_arr['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}

			print '<td class="'.$events_status_style_arr[$event_arr['status']].'">'.htmlspecialchars($events_status_arr[$event_arr['status']]).'</td>';
			if($config['event_moderation']){
				print '<td class="'.$events_moderation_style_arr[$event_arr['moderation']].'">'.htmlspecialchars($events_moderation_arr[$event_arr['moderation']]).'</td>';
				if(''!=$event_arr['reject_comment']){
					print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.$event_arr['reject_comment'].'</div></td>';
				}
				else{
					print '<td>&mdash;</td>';
				}
			}
			$sessions_count=$db->table_count('event_sessions',"WHERE `event`='".$event_arr['id']."'");
			print '<td><a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.$event_arr['url'].'/manage/sessions/">'.$sessions_count.'</a></td>';

			print '<td class="whitespace-nowrap">'.human_readable_filesize($event_arr['summary_files_size']).'</td>';

			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($event_arr['description']).'</div></td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($event_arr['location_description']).'</div></td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($event_arr['speakers_description']).'</div></td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($event_arr['tickets_description']).'</div></td>';

			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('delete_file'==$path_array[2]){
	ob_end_clean();
	$object_type=$path_array[3];
	$object_id=(int)$path_array[4];
	if('event'==$object_type){
		if(check_csrf()){
			$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$object_id."'");
			if(null!==$event_arr){
				$file_type=$db->prepare($path_array[5]);

				$event_files_arr=array('logo_url','cover_url','speakers_cover_url','location_cover_url','partners_cover_url');

				$result=['status'=>false];
				if(in_array($file_type,$event_files_arr)){
					if(file_exists($root_dir.$event_arr[$file_type])){
						unlink($root_dir.$event_arr[$file_type]);
						$db->sql("DELETE FROM `files` WHERE `path`='".$db->prepare($root_dir.$event_arr[$file_type])."'");
					}
					$db->sql("UPDATE `events` SET `".$file_type."`='' WHERE `id`='".$object_id."'");
					$result=['status'=>true,'message'=>'✓ File successfully deleted'];

				}
				if($result['status']){
					print json_encode($result);
					exit;
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	if('content'==$object_type){
		if(check_csrf()){
			$content_arr=$db->sql_row("SELECT * FROM `content` WHERE `id`='".$object_id."'");
			if(null!==$content_arr){
				$result=['status'=>false];
				$object_key=$db->prepare($path_array[5]);
				if(isset($content_arr[$object_key])){
					if($content_arr[$object_key]){
						if(file_exists($root_dir.$content_arr[$object_key])){
							unlink($root_dir.$content_arr[$object_key]);
							$db->sql("DELETE FROM `files` WHERE `path`='".$db->prepare($root_dir.$content_arr[$object_key])."'");
						}
						$db->sql("UPDATE `content` SET `".$object_key."`='' WHERE `id`='".$object_id."'");
						$result=['status'=>true,'message'=>'✓ File successfully deleted'];
					}
				}
				$search_thumbnail_key='thumbnail_'.$object_key;
				if(isset($content_arr[$search_thumbnail_key])){
					if($content_arr[$search_thumbnail_key]){
						if(file_exists($root_dir.$content_arr[$search_thumbnail_key])){
							unlink($root_dir.$content_arr[$search_thumbnail_key]);
							$db->sql("DELETE FROM `files` WHERE `path`='".$db->prepare($root_dir.$content_arr[$search_thumbnail_key])."'");
						}
						$db->sql("UPDATE `content` SET `".$search_thumbnail_key."`='' WHERE `id`='".$object_id."'");
						$result=['status'=>true,'message'=>'✓ File & thumbnail successfully deleted'];
					}
				}
				if($result['status']){
					print json_encode($result);
					exit;
				}
			}
		}
		else{
			header('HTTP/1.0 403 Forbidden');
			exit;
		}
	}
	header('HTTP/1.0 404 Not Found');
	exit;
}
elseif('content'==$path_array[2]){
	$replace['title']='Content | '.$replace['title'];
	$script_preset.='var wysiwyg_toolbar_remove=["heading1"];';
	if('upload_content'==$path_array[3]){
		ob_end_clean();
		$dir='/files/content/';
		if(!file_exists($root_dir.$dir)){
			mkdir($root_dir.$dir,0777,true);
		}
		$errors=[];
		$files=[];
		$upload_name='upload';
		if(isset($_FILES[$upload_name]) && 0==$_FILES[$upload_name]['error']){
			//check extensions
			$allowed_exts=['jpg','jpeg','png'];
			$ext=pathinfo($_FILES[$upload_name]['name'],PATHINFO_EXTENSION);
			if(in_array($ext,$allowed_exts)){
				if($_FILES[$upload_name]['size']<=1024*1024*2){//check size 2MB
					$upload=md5($_FILES[$upload_name]['name']).'_'.time().'.'.$ext;
					$filename=$dir.$upload;
					move_uploaded_file($_FILES[$upload_name]['tmp_name'],$root_dir.$filename);
					http_response_code(200);
					print json_encode(['url'=>$filename]);

					$files[]=['time'=>time(),'name'=>$_FILES[$upload_name]['name'],'size'=>$_FILES[$upload_name]['size'],'type'=>$_FILES[$upload_name]['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1,'target_id'=>0];//target=content
					foreach($files as $file){
						$db->sql("INSERT INTO `files` SET `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$file['target_id']."',`address`='".$auth['status_address_id']."'");
					}
				}
				else{
					$errors[]='Image size must be less than 2MB';
				}
			}
			else{
				$errors[]='Image must be in JPG, JPEG or PNG format';
			}
		}
		else{
			$errors[]='Image not uploaded';
		}
		if(count($errors)){
			http_response_code(400);
			print json_encode(['error'=>['message'=>implode("\n",$errors)]]);
		}
		exit;
	}
	if('delete'==$path_array[3]){
		$content_id=(int)$path_array[4];
		$content_arr=$db->sql_row("SELECT * FROM `content` WHERE `id`='".$content_id."'");
		if(3==$content_arr['status']){
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content is banned</p>
			</div>';
		}
		else
		if(null!=$content_arr){
			if(isset($_POST['approve'])){
				if(''!=$content_arr['cover_url']){
					if(file_exists($root_dir.$content_arr['cover_url'])){
						unlink($root_dir.$content_arr['cover_url']);
						$db->sql("DELETE FROM `files` WHERE `path`='".$db->prepare($root_dir.$content_arr['cover_url'])."'");
					}
				}
				if(''!=$content_arr['thumbnail_cover_url']){
					if(file_exists($root_dir.$content_arr['thumbnail_cover_url'])){
						unlink($root_dir.$content_arr['thumbnail_cover_url']);
						$db->sql("DELETE FROM `files` WHERE `path`='".$db->prepare($root_dir.$content_arr['thumbnail_cover_url'])."'");
					}
				}
				$db->sql("DELETE FROM `content` WHERE `id`='".$content_id."'");
				$db->sql("DELETE FROM `content_tags` WHERE `content`='".$content_id."'");
				header('Location:/admin/content/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/content/">&larr; Back to content list</a>';
				print '<h2 class="text-red-600">Remove content #'.$content_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Content will be removed from the database.</p>
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove content" class="red-btn">';
				print '</form>';

			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/content/">&larr; Back to content list</a>';
			print '<h2 class="text-red-600">Content not found</h2>';
			print '<p>Return to content list and try again.</p>';
		}
	}
	elseif('create'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/content/">&larr; Back to content list</a>';
		print '<h2>Create content</h2>';
		if(isset($_POST['url'])){
			$errors=[];
			$files=[];
			$url=trim($_POST['url']);
			$url=mb_strtolower($url);
			$url=preg_replace('/\.+/','.',$url);
			if(0!=$db->table_count('content',"WHERE `url`='".$db->prepare($url)."'")){
				$errors[]='URL "'.htmlspecialchars($url).'" is already in use';
			}
			if('index'!=$url){
				if(in_array($url,$reserved_urls)){
					$errors[]='URL "'.htmlspecialchars($url).'" is reserved by platform';
				}
			}

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			$upload_max_size=ini_get('upload_max_filesize');
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			$cover_url='';
			$thumbnail_cover_url='';

			$dir='/files/content/';
			if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
				//check extensions
				$allowed_exts=['jpg','jpeg','png'];
				$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$allowed_exts)){
					if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
						$cover=md5($_FILES['cover']['name']).'_'.time().'.'.$ext;
						if(!file_exists($root_dir.$dir)){
							mkdir($root_dir.$dir,0777,true);
						}
						$filename=$dir.$cover;
						move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
						$cover_url=$filename;
						//make small thumbnail 250x250 from cover
						$thumbnail_cover=md5($_FILES['cover']['name']).'_'.time().'_thumbnail.'.$ext;
						$thumbnail_filename=$dir.$thumbnail_cover;
						$thumbnail_cover_url=$thumbnail_filename;
						$thumbnail_cover_path=$root_dir.$thumbnail_filename;
						$thumbnail_cover_obj=new Imagick($root_dir.$filename);
						$thumbnail_cover_obj->thumbnailImage(250,250,true);
						$thumbnail_cover_obj->writeImage($thumbnail_cover_path);

						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1];//target=content
						$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>filesize($root_dir.$thumbnail_filename),'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$thumbnail_filename,'relative_path'=>$thumbnail_filename,'target'=>1];//target=content
					}
					else{
						$errors[]='&mdash; Cover size must be less than 2MB';
					}
				}
				else{
					$errors[]='&mdash; Cover must be in JPG, JPEG or PNG format';
				}
			}

			$title=trim($_POST['title']);
			if(''==$title){
				$errors[]='&mdash; Title is required';
			}
			$description=trim($_POST['description']);
			$content=trim($_POST['content']);

			$status=0;
			if($content_status_arr[$_POST['status']]){
				$status=(int)$_POST['status'];
			}
			$publishing_time=(int)$_POST['unixtime'];
			if(0==$status){//if status is planned
				if($publishing_time<time()){//if time is in past
					$publishing_time=0;//draft mode
					//$errors[]='&mdash; Publishing time must be in future for planned content';
				}
			}
			if(1==$status){//if status is published
				if($publishing_time<time()){//if time is in past
					$publishing_time=time();//set time to now
				}
			}
			$pin=0;
			if(isset($_POST['pin'])){
				if(1==$_POST['pin']){
					$pin=1;
				}
			}

			if(0==count($errors)){
				$db->sql("INSERT INTO `content` (`url`,`title`,
				`cover_url`,`thumbnail_cover_url`,
				`description`,`content`,`status`,`time`,`pin`) VALUES ('".$db->prepare($url)."','".$db->prepare($title)."',
				'".$db->prepare($cover_url)."','".$db->prepare($thumbnail_cover_url)."',
				'".$db->prepare($description)."','".$db->prepare($content)."','".$status."','".$publishing_time."','".$pin."')");

				$content_id=$db->last_id();

				//add files to database
				foreach($files as $file){
					$db->sql("INSERT INTO `files` SET `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$content_id."',`address`='".$auth['status_address_id']."'");
				}

				$db->sql("DELETE FROM `content_tags` WHERE `content`='".$db->prepare($content_id)."'");
				if(isset($_POST['tag'])){
					foreach($_POST['tag'] as $tag_id=>$tag_status){
						if('on'==$tag_status){
							$db->sql("INSERT INTO `content_tags` (`content`,`tag`) VALUES ('".$db->prepare($content_id)."','".$db->prepare($tag_id)."')");
						}
					}
				}

				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Content <a href="/admin/content/edit/'.$content_id.'/">"'.htmlspecialchars($url).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/admin/content/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

				//deleting temporarily uploaded files
				foreach($files as $file){
					unlink($file['path']);
				}
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
			print '<div class="max-w-fit">';
			$object_scheme=[//admin create content
				'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /content/<b>URL</b>/ page address bar.','required'=>true],
				'title'=>['type'=>'text','title'=>'Title','required'=>true],
				'description'=>['type'=>'textarea','title'=>'Description','placeholder'=>'Description (annotation)'],
				'cover'=>[
					'type'=>'image',
					'title'=>'🖼️ Cover image',
					'placeholder'=>'Cover',
					'object_type'=>'content',
					'file_path'=>'cover_url',
					'thumbnail_path'=>'thumbnail_cover_url',
					'descr'=>'The thumbnail will be created automatically with a target ratio of 1:1 (250x250)'
				],
				'content'=>['type'=>'textarea','title'=>'📄 Content','placeholder'=>'HTML content','wysiwyg'=>true],
				'sep1'=>['type'=>'hr'],
				'status'=>[
					'type'=>'select',
					'title'=>'Status',
					'options'=>$content_status_arr,
					'default_option'=>0,
					'classes'=>$content_status_style_arr,
				],
				'datetime'=>[
					'key'=>'time',
					'type'=>'datetime',
					'local'=>true,//-local
					'future'=>true,
					'title'=>'Publishing time (by your local time)',
					'value_title'=>'Publishing time',
					'timestamp'=>'unixtime',
					'value'=>0
				],
				'pin'=>['type'=>'checkbox','title'=>'Pinned content','value'=>1],
				'tag'=>[
					'type'=>'multi-checkbox',
					'title'=>'Tags',
					'options'=>$db->sql('SELECT * FROM `tags` ORDER BY `sort` ASC, `url` ASC'),
					'key'=>'id',
					'value'=>'caption',
				],
			];
			print build_form($object_scheme);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create content" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/content/">&larr; Back to content list</a>';
		print '<h2>Edit content</h2>';
		$content_id=(int)$path_array[4];
		$content_item=$db->sql_row("SELECT * FROM `content` WHERE `id`='".$content_id."'");
		if(3==$content_item['status']){
			print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content is banned</p>
			</div>';
			$content_item=null;
		}
		else
		if(null!=$content_item){
			if(isset($_POST['url'])){
				$cover_url=$content_item['cover_url'];
				$thumbnail_cover_url=$content_item['thumbnail_cover_url'];
				$errors=[];
				$files=[];
				$remove_files=[];

				//https://www.php.net/manual/ru/features.file-upload.errors.php
				$upload_max_size=ini_get('upload_max_filesize');
				foreach($_FILES as $file_key=>$file_data){
					switch($file_data['error']){
						case UPLOAD_ERR_INI_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
							break;
					}
				}

				$dir='/files/content/';
				if(isset($_FILES['cover']) && 0==$_FILES['cover']['error']){
					//check extensions
					$allowed_exts=['jpg','jpeg','png'];
					$ext=pathinfo($_FILES['cover']['name'],PATHINFO_EXTENSION);
					if(in_array($ext,$allowed_exts)){
						if($_FILES['cover']['size']<=1024*1024*2){//check size 2MB
							$cover=md5($_FILES['cover']['name']).'_'.time().'.'.$ext;
							if(!file_exists($root_dir.$dir)){
								mkdir($root_dir.$dir,0777,true);
							}
							$filename=$dir.$cover;
							move_uploaded_file($_FILES['cover']['tmp_name'],$root_dir.$filename);
							$cover_url=$filename;
							//make small thumbnail 250x250 from cover
							$thumbnail_cover=md5($_FILES['cover']['name']).'_'.time().'_thumbnail.'.$ext;
							$thumbnail_filename=$dir.$thumbnail_cover;
							$thumbnail_cover_url=$thumbnail_filename;
							$thumbnail_cover_path=$root_dir.$thumbnail_filename;
							$thumbnail_cover_obj=new Imagick($root_dir.$filename);
							$thumbnail_cover_obj->thumbnailImage(250,250,true);
							$thumbnail_cover_obj->writeImage($thumbnail_cover_path);

							$remove_files[]=$content_item['cover_url'];//remove old file
							$remove_files[]=$content_item['thumbnail_cover_url'];//remove old file
							$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>$_FILES['cover']['size'],'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>1];//target=content
							$files[]=['time'=>time(),'name'=>$_FILES['cover']['name'],'size'=>filesize($root_dir.$thumbnail_filename),'type'=>$_FILES['cover']['type'],'path'=>$root_dir.$thumbnail_filename,'relative_path'=>$thumbnail_filename,'target'=>1];//target=content
						}
						else{
							$errors[]='&mdash; Cover size must be less than 2MB';
						}
					}
					else{
						$errors[]='&mdash; Cover must be in JPG, JPEG or PNG format';
					}
				}

				$url=trim($_POST['url']);
				$url=mb_strtolower($url);
				$url=preg_replace('/\.+/','.',$url);
				if(''==$url){
					$errors[]='&mdash; URL is required';
				}
				//check if url is unique
				$check_url=$db->sql_row("SELECT * FROM `content` WHERE `url`='".$url."' AND `id`!='".$content_id."'");
				if(null!=$check_url){
					$errors[]='&mdash; URL is already in use';
				}
				if('index'!=$url){
					if(in_array($url,$reserved_urls)){
						$errors[]='&mdash; URL "'.htmlspecialchars($url).'" is reserved by platform';
					}
				}

				$title=trim($_POST['title']);
				if(''==$title){
					$errors[]='&mdash; Title is required';
				}
				$description=trim($_POST['description']);
				$content=trim($_POST['content']);

				$status=0;
				if($content_status_arr[$_POST['status']]){
					$status=(int)$_POST['status'];
				}
				$publishing_time=(int)$_POST['unixtime'];
				if(0==$status){//if status is planned
					if($publishing_time<time()){//if time is in past
						$publishing_time=0;
						//$errors[]='&mdash; Publishing time must be in future for planned content';
					}
				}
				if(1==$status){//if status is published
					if($publishing_time<time()){//if time is in past
						$publishing_time=time();//set time to now
					}
				}
				$pin=0;
				if(isset($_POST['pin'])){
					if(1==$_POST['pin']){
						$pin=1;
					}
				}

				if(0==count($errors)){
					$db->sql("UPDATE `content` SET `url`='".$db->prepare($url)."', `title`='".$db->prepare($title)."', `description`='".$db->prepare($description)."', `content`='".$db->prepare($content)."', `cover_url`='".$db->prepare($cover_url)."', `thumbnail_cover_url`='".$db->prepare($thumbnail_cover_url)."', `status`='".$status."', `time`='".$publishing_time."', `pin`='".$pin."' WHERE `id`='".$content_id."'");

					//deleting old files
					foreach($remove_files as $file){
						unlink($root_dir.$file);
						$db->sql("DELETE FROM `files` WHERE `path`='".$db->prepare($root_dir.$file)."'");
					}
					//add files to database
					foreach($files as $file){
						$db->sql("INSERT INTO `files` SET `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$content_id."',`address`='".$auth['status_address_id']."'");
					}

					$db->sql("DELETE FROM `content_tags` WHERE `content`='".$content_id."'");
					if(isset($_POST['tag'])){
						foreach($_POST['tag'] as $tag_id=>$tag_status){
							if('on'==$tag_status){
								$db->sql("INSERT INTO `content_tags` (`content`,`tag`) VALUES ('".$db->prepare($content_item['id'])."','".$db->prepare($tag_id)."')");
							}
						}
					}

					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Content <a href="/admin/content/edit/'.$content_item['id'].'/">"'.htmlspecialchars($url).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/admin/content/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';

					//deleting temporarily uploaded files
					foreach($files as $file){
						unlink($file['path']);
					}
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data" onsubmit="$(\'input[name=unixtime]\').val(Date.parse($(\'input[name=datetime]\').val())/1000);return true;">';
				print '<div class="max-w-fit">';
				$object_scheme=[//admin edit content
					'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /content/<b>URL</b>/ page address bar.','required'=>true],
					'title'=>['type'=>'text','title'=>'Title','required'=>true],
					'description'=>['type'=>'textarea','title'=>'Description','placeholder'=>'Description (annotation)'],
					'cover'=>[
						'type'=>'image',
						'title'=>'🖼️ Cover image',
						'placeholder'=>'Cover',
						'object_type'=>'content',
						'file_path'=>'cover_url',
						'thumbnail_path'=>'thumbnail_cover_url',
						'descr'=>'The thumbnail will be created automatically with a target ratio of 1:1 (250x250)'
					],
					'content'=>['type'=>'textarea','title'=>'📄 Content','placeholder'=>'HTML content','wysiwyg'=>true],
					'sep1'=>['type'=>'hr'],
					'status'=>[
						'type'=>'select',
						'title'=>'Status',
						'options'=>$content_status_arr,
						'default_option'=>0,
						'classes'=>$content_status_style_arr,
					],
					'datetime'=>[
						'key'=>'time',
						'type'=>'datetime',
						'local'=>true,//-local
						'future'=>true,
						'title'=>'Publishing time (by your local time)',
						'value_title'=>'Publishing time',
						'timestamp'=>'unixtime',
						'value'=>0
					],
					'pin'=>['type'=>'checkbox','title'=>'Pinned content','value'=>1],
					'tag'=>[
						'type'=>'multi-checkbox',
						'title'=>'Tags',
						'options'=>$db->sql('SELECT * FROM `tags` ORDER BY `sort` ASC, `url` ASC'),
						'key'=>'id',
						'value'=>'caption',
						'value_function'=>false,
						'checked_function'=>function($option,$object){
							global $db,$organizer_id;
							return (0<$db->table_count('content_tags','WHERE `tag`='.$option['id'].' AND `content`='.$object['id']));
						},
					],
				];
				print build_form($object_scheme,$content_item);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Update content" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Content not found</p>
			</div>';
		}
	}
	else{
		print '<a class="action-btn" href="/admin/content/create/">Create content</a>';
		$filter_tag=false;
		if(isset($_GET['tag'])){
			if(''!==$_GET['tag']){
				$filter_tag=(int)$_GET['tag'];
				if(0==$db->table_count('tags',"WHERE `id`='".$filter_tag."'")){
					$filter_tag=false;
				}
			}
		}

		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($content_status_arr[$filter_status])){
					$filter_status=false;
				}
			}
		}

		$filter_query=false;
		if(isset($_GET['query'])){
			if(''!==$_GET['query']){
				$filter_query=trim($_GET['query']);
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="query" class="form-post-by-enter" placeholder="Search query" value="'.($filter_query?htmlspecialchars($filter_query):'').'">
			</div>
			<div>
				<select name="tag" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_tag?' selected':'').'>Filter by tag</option>';
					$tags=$db->sql('SELECT * FROM `tags` ORDER BY `sort` ASC, `url` ASC');
					foreach($tags as $tag){
						print '<option value="'.htmlspecialchars($tag['id']).'"'.($tag['id']==$filter_tag?' selected':'').'>Tag: '.htmlspecialchars($tag['caption']).'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
					foreach($content_status_arr as $content_status_id=>$content_status_caption){
						print '<option value="'.htmlspecialchars($content_status_id).'" class="'.$content_status_style_arr[$content_status_id].'"'.($content_status_id===$filter_status?' selected':'').'>Status: '.htmlspecialchars($content_status_caption).'</option>';
					}
					print '
				</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>URL</th>';
			print '<th>Cover</th>';
			print '<th>Title</th>';
			print '<th>Description</th>';
			print '<th>Content</th>';
			print '<th>Status</th>';
			print '<th>Time</th>';
			print '<th>Pinned</th>';
			print '<th>Tags</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		if(false!==$filter_status){
			$sql_addon[]='`content`.`status`='.$filter_status;
		}
		if(false!==$filter_query){
			$sql_addon[]='(`content`.`url` LIKE \'%'.$filter_query.'%\' OR `content`.`title` LIKE \'%'.$filter_query.'%\' OR `content`.`description` LIKE \'%'.$filter_query.'%\')';
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		if(false!==$filter_tag){
			$sql_addon_str=' RIGHT JOIN `content_tags` ON `content_tags`.`content`=`content`.`id` AND `content_tags`.`tag`='.$filter_tag.' '.$sql_addon_str;
		}
		$per_page=50;
		$count=$db->table_count('content',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$content=$db->sql("SELECT `content`.* FROM `content`".$sql_addon_str." ORDER BY `content`.`id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($content as $content_item){
			print '<tr>';
			print '<td><a href="/admin/content/delete/'.$content_item['id'].'/" class="red-btn">Delete</a></td>';
			print '
			<td class="whitespace-nowrap">
				<a href="/admin/content/edit/'.$content_item['id'].'/">'.htmlspecialchars($content_item['url']).'</a>
				<a href="/content/'.htmlspecialchars($content_item['url']).'/" target="_blank" class="text-blue-500 !no-underline">⧉</a>
			</td>';
			if($content_item['thumbnail_cover_url']){
				print '<td><img src="'.$content_item['thumbnail_cover_url'].'" class="cover"></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>'.htmlspecialchars($content_item['title']).'</td>';
			print '<td class="small-padding"><div class="json-addon-cell tinyscroll">'.htmlspecialchars($content_item['description']).'</div></td>';

			print '<td>'.((''!=$content_item['content'])||(''!=$content_item['embed_content'])?'✔️':'❌').'</td>';

			print '<td>';
			print '<span class="'.$content_status_style_arr[$content_item['status']].'">';
			print htmlspecialchars($content_status_arr[$content_item['status']]);
			print '</span>';
			print '</td>';
			if($content_item['time']){
				print '<td><span class="time" data-time="'.$content_item['time'].'">'.date('d.m.Y H:i:s',$content_item['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>'.(1==$content_item['pin']?'✔️':'&mdash;').'</td>';//not use ❌ as more negative than &mdash;
			print '<td>';
			$content_tags=$db->sql("SELECT `tag` FROM `content_tags` WHERE `content`=".$content_item['id']);
			foreach($content_tags as $content_tag){
				$tag_arr=$db->sql_row("SELECT * FROM `tags` WHERE `id`=".$content_tag['tag']);
				print ' <a href="/admin/content/?tag='.$tag_arr['id'].'">'.htmlspecialchars($tag_arr['caption']).'</a>';
			}
			print '</td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('tags'==$path_array[2]){
	$replace['title']='Tags | '.$replace['title'];
	if('delete'==$path_array[3]){
		$tag_id=intval($path_array[4]);
		$tag_arr=$db->sql_row("SELECT * FROM `tags` WHERE `id`='".$tag_id."'");
		if(null!=$tag_arr){
			if(isset($_POST['approve'])){
				$db->sql("DELETE FROM `content_tags`WHERE `tag`='".$tag_id."'");
				$db->sql("DELETE FROM `tags` WHERE `id`='".$tag_id."'");
				header('location:/admin/tags/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/tags/">&larr; Back to tags</a>';
				print '<h2 class="text-red-600">Remove tag #'.$tag_arr['id'].'</h2>';
				$affected_content_count=$db->table_count('content_tags',"WHERE `tag`='".$tag_arr['id']."'");
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Warning</p>
					<p>Tag "<a href="/admin/tags/edit/'.$tag_arr['id'].'/">'.htmlspecialchars($tag_arr['url']).'</a>" will be removed from the database.</p>';
				if($affected_content_count){
					print '<p>Tag will also be removed for '.$affected_content_count.' linked content.</p>';
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Remove tag" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/tags/">&larr; Back to tags</a>';
			print '<h2 class="text-red-600">Tag not found</h2>';
			print '<p>Return to platform tags and try again.</p>';
		}
	}
	elseif('create'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/tags/">&larr; Back to tags</a>';
		print '<h2>Create tag</h2>';
		if(isset($_POST['url'])){
			$errors=[];

			$url=trim($_POST['url']);
			$url=mb_strtolower($url);
			$url=preg_replace('/\.+/','.',$url);
			if(!$url){
				$errors[]='URL is required';
			}
			$tag_exist=$db->table_count('tags',"WHERE `url`='".$db->prepare($url)."'");
			if($tag_exist){
				$errors[]='Tag url '.htmlspecialchars($url).' already exists';
			}

			$caption=trim($_POST['caption']);
			if(!$caption){
				$errors[]='Caption is required';
			}
			$tag_exist=$db->table_count('tags',"WHERE `caption`='".$db->prepare($caption)."'");
			if($tag_exist){
				$errors[]='Tag caption '.htmlspecialchars($caption).' already exists';
			}

			$sort=intval($_POST['sort']);

			if(0==count($errors)){
				$db->sql("INSERT INTO `tags` (`url`,`caption`,`sort`) VALUES ('".$db->prepare($url)."','".$db->prepare($caption)."','".$sort."')");
				$tag_id=$db->last_id();
				print '
				<div class="success-box" role="alert">
					<p class="font-bold">Success</p>
					<p>Tag <a href="/admin/tags/edit/'.$tag_id.'/">"'.htmlspecialchars($url).'"</a> was created</p>
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/admin/tags/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card">';
			print '<div class="max-w-fit">';
			$object_scheme=[//admin content tag
				'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /content/?tag=<b>URL</b> filter page address bar.','required'=>true],
				'caption'=>['type'=>'text','title'=>'Caption','required'=>true],
				'sort'=>['type'=>'number','title'=>'Sort','placeholder'=>'Sort (number)','required'=>true],
			];
			print build_form($object_scheme);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" value="Create tag" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('edit'==$path_array[3]){
		print '<a class="reverse-btn default-button" href="/admin/tags/">&larr; Back to tags</a>';
		print '<h2>Edit tag</h2>';
		$tag_id=(int)$path_array[4];
		$tag_arr=$db->sql_row("SELECT * FROM `tags` WHERE `id`='".$tag_id."'");
		if(null!=$tag_arr){
			if(isset($_POST['url'])){
				$errors=[];

				$url=trim($_POST['url']);
				$url=mb_strtolower($url);
				$url=preg_replace('/\.+/','.',$url);
				if(!$url){
					$errors[]='URL is required';
				}
				$tag_exist=$db->table_count('tags',"WHERE `url`='".$db->prepare($url)."' AND `id`!='".$tag_id."'");
				if($tag_exist){
					$errors[]='Tag url '.htmlspecialchars($url).' already exists';
				}

				$caption=trim($_POST['caption']);
				if(!$caption){
					$errors[]='Caption is required';
				}
				$tag_exist=$db->table_count('tags',"WHERE `caption`='".$db->prepare($caption)."' AND `id`!='".$tag_id."'");
				if($tag_exist){
					$errors[]='Tag caption '.htmlspecialchars($caption).' already exists';
				}

				$sort=intval($_POST['sort']);

				if(0==count($errors)){
					$db->sql("UPDATE `tags` SET
						`url`='".$db->prepare($url)."',
						`caption`='".$db->prepare($caption)."',
						`sort`='".$sort."'
						WHERE `id`='".$tag_id."'");
					print '
					<div class="success-box" role="alert">
						<p class="font-bold">Success</p>
						<p>Tag <a href="/admin/tags/edit/'.$tag_id.'/">"'.htmlspecialchars($url).'"</a> was updated</p>
					</div>';
					print '<meta http-equiv="refresh" content="5; url=/admin/tags/">';
				}
				else{
					print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>'.implode('<br>',$errors).'</p>
					</div>';
					print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
				}
			}
			else{
				print '<form action="" method="POST" class="manage-card">';
				print '<div class="max-w-fit">';
				$object_scheme=[//admin content tag
					'url'=>['type'=>'text','title'=>'URL','class'=>'url-part','descr'=>'Used for /content/?tag=<b>URL</b> filter page address bar.','required'=>true],
					'caption'=>['type'=>'text','title'=>'Caption','required'=>true],
					'sort'=>['type'=>'number','title'=>'Sort','placeholder'=>'Sort (number)','required'=>true],
				];
				print build_form($object_scheme,$tag_arr);
				print '</div>';
				print '<div class="grid-wrapper">';
				print '<input type="submit" value="Save changes" class="action-btn big">';
				print '</div>';
				print '</form>';
			}
		}
		else{
			print '<h2 class="text-red-600">Tag not found</h2>';
			print '<p>Return to platform tags and try again.</p>';
		}
	}
	else{
		print '<a href="/admin/tags/create/" class="action-btn">Create tag</a>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Action</th>';
			print '<th>Sort</th>';
			print '<th>URL</th>';
			print '<th>Caption</th>';
			print '<th>Content count</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$tags=$db->sql("SELECT * FROM `tags` ORDER BY `sort` ASC");
		foreach($tags as $tag){
			print '<tr>';
			print '<td><a href="/admin/tags/delete/'.$tag['id'].'/" class="red-btn">Delete</a></td>';
			print '<td>'.$tag['sort'].'</td>';
			print '<td><a href="/admin/tags/edit/'.$tag['id'].'/">'.htmlspecialchars($tag['url']).'</a></td>';
			print '<td>'.htmlspecialchars($tag['caption']).'</td>';
			$content_count=$db->table_count('content_tags',"WHERE `tag`='".$tag['id']."'");
			print '<td><a href="/admin/content/?tag='.$tag['id'].'">'.$content_count.'</a></td>';
			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';
	}
}
elseif('requests'==$path_array[2]){
	//check requests table for new requests to approve or reject action (delete linked_platform for address)
	$replace['title']='Requests | '.$replace['title'];
	//targets scheme by target type
	$support_targets=[
		1=>[//request to delete from linked_platforms
			'name'=>'Linked platform',
			'table'=>'linked_platforms',
			'description'=>'Delete linked platform for address.',
			'value_function'=>function($item_arr){
				$result='';
				if(2==$item_arr['platform']){//email
					$result.='Email '.$item_arr['internal_username'].'';
				}
				if(1==$item_arr['platform']){//telegram
					$result.='Telegram';
					if($item_arr['internal_username']){
						$result.=' @'.$item_arr['internal_username'];
					}
					if($item_arr['internal_id']){
						$result.=' #'.$item_arr['internal_id'];
					}
				}
				return $result;
			},
			'approve_function'=>function($item_arr,$address=false){
				global $db;
				$db->sql("UPDATE `linked_platforms` SET `status`='4', `status_time`='".time()."' WHERE `id`='".$item_arr['id']."'");
				if(false!==$address){
					//send notification to address about approve (target type=1)
					add_notify($address,0,1,'request_1_was_approved');
				}
			},
			'reject_function'=>function($address=false){
				global $db;
				if(false!==$address){
					//send notification to address about reject (target type=1)
					add_notify($address,0,1,'request_1_was_rejected');
				}
			},
		],
	];
	if('approve'==$path_array[3]){
		$request_id=intval($path_array[4]);
		$request_arr=$db->sql_row("SELECT * FROM `requests` WHERE `id`='".$request_id."' AND `status`=0");//only waiting requests
		if(null!=$request_arr){
			if(isset($_POST['approve'])){
				if(isset($support_targets[$request_arr['target']]['approve_function'])){
					//call approve function
					//example: update target and send notification to address about it
					$item_arr=$db->sql_row("SELECT * FROM `".$support_targets[$request_arr['target']]['table']."` WHERE `id`='".$request_arr['target_id']."'");
					$support_targets[$request_arr['target']]['approve_function']($item_arr,$request_arr['address']);
				}

				$db->sql("UPDATE `requests` SET `status`=1,`status_time`='".time()."',`status_address`='".$auth['status_address_id']."' WHERE `id`='".$request_id."'");

				header('location:/admin/requests/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/requests/">&larr; Back to requests</a>';
				print '<h2 class="text-green-600">Approve request #'.$request_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Attention</p>
					<p>'.$support_targets[$request_arr['target']]['description'].'</p>';
				print '<p>Address: ';
				if($request_arr['address']){
					$address_arr=$db->sql_row("SELECT `address` FROM `addresses` WHERE `id`='".$request_arr['address']."' LIMIT 1");
					print '<a href="/admin/addresses/edit/'.$request_arr['address'].'/" target="_blank">'.$request_arr['address'].'</a> ('.htmlspecialchars($address_arr['address']).')';
				}

				if(isset($support_targets[$request_arr['target']]['value_function'])){
					$item_arr=$db->sql_row("SELECT * FROM `".$support_targets[$request_arr['target']]['table']."` WHERE `id`='".$request_arr['target_id']."' LIMIT 1");
					$target_value=htmlspecialchars($support_targets[$request_arr['target']]['value_function']($item_arr));
					if($target_value){
						print '<p>'.$target_value.'</p>';
					}
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Approve request" class="green-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/requests/">&larr; Back to requests</a>';
			print '<h2 class="text-red-600">Request not found</h2>';
			print '<p>Return to requests list and try again.</p>';
		}
	}
	elseif('reject'==$path_array[3]){
		$request_id=intval($path_array[4]);
		$request_arr=$db->sql_row("SELECT * FROM `requests` WHERE `id`='".$request_id."' AND `status`=0");//only waiting requests
		if(null!=$request_arr){
			if(isset($_POST['reject'])){
				if(isset($support_targets[$request_arr['target']]['reject_function'])){
					//call reject function
					//example: send notification to address about reject
					$support_targets[$request_arr['target']]['reject_function']($request_arr['address']);
				}

				$db->sql("UPDATE `requests` SET `status`=2,`status_time`='".time()."',`status_address`='".$auth['status_address_id']."' WHERE `id`='".$request_id."'");

				header('location:/admin/requests/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/requests/">&larr; Back to requests</a>';
				print '<h2 class="text-red-600">Reject request #'.$request_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Attention</p>
					<p>'.$support_targets[$request_arr['target']]['description'].'</p>';
				print '<p>Address: ';
				if($request_arr['address']){
					$address_arr=$db->sql_row("SELECT `address` FROM `addresses` WHERE `id`='".$request_arr['address']."' LIMIT 1");
					print '<a href="/admin/addresses/edit/'.$request_arr['address'].'/" target="_blank">'.$request_arr['address'].'</a> ('.htmlspecialchars($address_arr['address']).')';
				}

				if(isset($support_targets[$request_arr['target']]['value_function'])){
					$item_arr=$db->sql_row("SELECT * FROM `".$support_targets[$request_arr['target']]['table']."` WHERE `id`='".$request_arr['target_id']."' LIMIT 1");
					$target_value=htmlspecialchars($support_targets[$request_arr['target']]['value_function']($item_arr));
					if($target_value){
						print '<p>'.$target_value.'</p>';
					}
				}
				print '
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="reject" value="Reject request" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/requests/">&larr; Back to requests</a>';
			print '<h2 class="text-red-600">Request not found</h2>';
			print '<p>Return to requests list and try again.</p>';
		}
	}
	else
	if(''==$path_array[3]){
		$filter_target=false;
		if(isset($_GET['target'])){
			if(''!==$_GET['target']){
				$filter_target=(int)$_GET['target'];
				if(!isset($support_targets[$filter_target])){
					$filter_target=false;
				}
			}
		}

		$filter_status=false;
		if(isset($_GET['status'])){
			if(''!==$_GET['status']){
				$filter_status=(int)$_GET['status'];
				if(!isset($content_status_arr[$filter_status])){
					$filter_status=false;
				}
			}
		}
		$request_status_arr=['❔','✔️','❌'];//emoji
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<select name="target" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_target?' selected':'').'>Filter by target</option>';
					foreach($support_targets as $target_type=>$target_arr){
						print '<option value="'.$target_type.'"'.($target_type==$filter_target?' selected':'').'>Target: '.htmlspecialchars($target_arr['name']).'</option>';
					}
					print '
				</select>
			</div>
			<div>
				<select name="status" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_status?' selected':'').'>Filter by status</option>';
					foreach($requests_status_arr as $requests_status_id=>$requests_status_caption){
						print '<option value="'.$requests_status_id.'" class="'.$requests_status_style_arr[$requests_status_id].'"'.($requests_status_id===$filter_status?' selected':'').'>Status: '.htmlspecialchars($requests_status_caption).'</option>';
					}
					print '
				</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>Target type</th>';
			print '<th>Target</th>';
			print '<th>Request Time</th>';
			print '<th>Status</th>';
			print '<th>Status Time</th>';
			print '<th>Decision maker</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		if(false!==$filter_target){
			$sql_addon[]='`target`='.$filter_target;
		}
		if(false!==$filter_status){
			$sql_addon[]='`status`='.$filter_status;
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		$per_page=50;
		$count=$db->table_count('requests',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$requests=$db->sql("SELECT * FROM `requests`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($requests as $request_arr){
			print '<tr>';
			print '<td>';
			if(0==$request_arr['status']){//waiting for decision
				print '<a href="/admin/requests/approve/'.$request_arr['id'].'/" class="green-btn mr-4 mb-2">Approve</a>';
				print '<a href="/admin/requests/reject/'.$request_arr['id'].'/" class="red-btn">Reject</a>';
			}
			else{
				print '&mdash;';
			}
			print '</td>';

			print '<td>'.htmlspecialchars($support_targets[$request_arr['target']]['name']).'</td>';

			$target_value=$request_arr['target_id'];
			if(isset($support_targets[$request_arr['target']]['value_function'])){
				if(is_callable($support_targets[$request_arr['target']]['value_function'])){
					$item_arr=$db->sql_row("SELECT * FROM `".$support_targets[$request_arr['target']]['table']."` WHERE `id`='".$request_arr['target_id']."' LIMIT 1");
					$target_value=htmlspecialchars($support_targets[$request_arr['target']]['value_function']($item_arr));
				}
			}
			print '<td>'.$target_value.'</td>';

			if($request_arr['time']){
				print '<td><span class="time" data-time="'.$request_arr['time'].'">'.date('d.m.Y H:i:s',$request_arr['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}
			print '<td>'.$request_status_arr[$request_arr['status']].'</td>';

			if($request_arr['status_time']){
				print '<td><span class="time" data-time="'.$request_arr['status_time'].'">'.date('d.m.Y H:i:s',$request_arr['status_time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}

			$decision_maker='&mdash;';
			if($request_arr['status_address']){
				$decision_maker_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$request_arr['status_address']."'");
				if($decision_maker_arr){
					$decision_maker=htmlspecialchars($decision_maker_arr['address']);
				}
			}
			print '<td>';
			print $decision_maker;
			print '</td>';

			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('files'==$path_array[2]){
	$replace['title']='Files | '.$replace['title'];
	if('upload'==$path_array[3]){
		$upload_max_size=ini_get('upload_max_filesize');
		$replace['title']='Upload | '.$replace['title'];
		if(isset($_POST['custom_upload'])){
			$dir='/files/custom/';
			if(!file_exists($root_dir.$dir)){
				mkdir($root_dir.$dir,0777,true);
			}
			$errors=[];
			$files=[];
			$upload_name='upload';

			if(isset($_FILES[$upload_name]) && 0==$_FILES[$upload_name]['error']){
				//check extensions
				$ext=pathinfo($_FILES[$upload_name]['name'],PATHINFO_EXTENSION);
				if(in_array($ext,$custom_upload_allowed_extensions)){
					if($_FILES[$upload_name]['size']<=1024*1024*(intval($upload_max_size))){//check size $upload_max_size
						$upload=md5($_FILES[$upload_name]['name']).'_'.time().'.'.$ext;
						$filename=$dir.$upload;
						move_uploaded_file($_FILES[$upload_name]['tmp_name'],$root_dir.$filename);
						http_response_code(200);

						$files[]=['time'=>time(),'name'=>$_FILES[$upload_name]['name'],'size'=>$_FILES[$upload_name]['size'],'type'=>$_FILES[$upload_name]['type'],'path'=>$root_dir.$filename,'relative_path'=>$filename,'target'=>0,'target_id'=>0];//target=none
					}
					else{
						$errors[]='File size must be less than '.$upload_max_size;
					}
				}
				else{
					$errors[]='File must be allowed extension';
				}
			}
			if(0==count($_FILES)){
				$errors[]='File not found';
			}

			//https://www.php.net/manual/ru/features.file-upload.errors.php
			foreach($_FILES as $file_key=>$file_data){
				switch($file_data['error']){
					case UPLOAD_ERR_INI_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit ('.$upload_max_size.')';
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$errors[]='File '.$file_key.' exceeded filesize limit (html MAX_FILE_SIZE)';
						break;
				}
			}

			if(0==count($errors)){
				print '
				<div class="success-box" role="alert">
				<p class="font-bold">Success</p>';
				foreach($files as $file){
					$db->sql("INSERT INTO `files` SET `time`='".$file['time']."',`name`='".$db->prepare($file['name'])."',`size`='".$file['size']."',`type`='".$db->prepare($file['type'])."',`path`='".$db->prepare($file['path'])."',`relative_path`='".$db->prepare($file['relative_path'])."',`target`='".$file['target']."',`target_id`='".$file['target_id']."',`address`='".$auth['status_address_id']."'");
					print '<p>File ';
					print '<a href="'.htmlspecialchars($file['relative_path']).'" target="_blank" class="font-bold">';
						print htmlspecialchars(substr($file['relative_path'],strrpos($file['relative_path'],'/')+1));
						print '</a>';
					print ' uploaded successfully.</p>';
				}
				print '
				</div>';
				print '<meta http-equiv="refresh" content="5; url=/admin/files/">';
			}
			else{
				print '<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>'.implode('<br>',$errors).'</p>
				</div>';
				print '<a class="reverse-btn default-button" href="javascript:history.back()">&larr; Return back</a>';
			}
		}
		else{
			print '<form action="" method="POST" class="manage-card" enctype="multipart/form-data">';
			print '<div class="max-w-fit">';
			print '<p class="text-sm">Allowed extensions: '.implode(', ',$custom_upload_allowed_extensions).'.</p>';
			$object_scheme=[//admin custom upload
				'upload'=>[
					'type'=>'file',
					'title'=>'File',
					'descr'=>'Uploaded file will be public available, so you can share it with anyone<br>Max size: '.$upload_max_size.'',
				],
			];
			print build_form($object_scheme);
			print '</div>';
			print '<div class="grid-wrapper">';
			print '<input type="submit" name="custom_upload" value="Upload file" class="action-btn big">';
			print '</div>';
			print '</form>';
		}
	}
	elseif('delete'==$path_array[3]){
		$replace['title']='Delete | '.$replace['title'];
		$file_arr=$db->sql_row("SELECT * FROM `files` WHERE `id`='".(int)$path_array[4]."' LIMIT 1");
		if($file_arr){
			if(isset($_POST['approve'])){
				$db->sql("DELETE FROM `files` WHERE `id`='".$file_arr['id']."' LIMIT 1");
				@unlink($file_arr['path']);
				header('Location: /admin/files/');
				exit;
			}
			else{
				print '<a class="reverse-btn default-button" href="/admin/files/">&larr; Back to files</a>';
				print '<h2 class="text-red-600">Delete file #'.$file_arr['id'].'</h2>';
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Attention</p>
					<p>If you delete this file, it will be lost forever and can not be displayed by linked content.</p>
					<p>Are you sure you want to delete file ';
					print '<a href="'.htmlspecialchars($file_arr['relative_path']).'" target="_blank" class="font-bold">';
					print htmlspecialchars(substr($file_arr['relative_path'],strrpos($file_arr['relative_path'],'/')+1));
					print '</a>';
					print '?</p>
				</div>';
				print '<form action="" method="POST" class="manage-card">';
				print '<input type="submit" name="approve" value="Yes, delete" class="red-btn">';
				print '</form>';
			}
		}
		else{
			print '<a class="reverse-btn default-button" href="/admin/files/">&larr; Back to files</a>';
			print '<h2 class="text-red-600">File not found</h2>';
			print '<p>Return to files list and try again.</p>';

			header('Location: /admin/files/');
			exit;
		}
	}
	else{
		print '<a class="action-btn" href="/admin/files/upload/">Upload file</a>';
		print '<span class="text-sm">Summary size of admin files: '.human_readable_filesize($db->select_one('files','SUM(`size`)')).'</span>';
		$filter_target=false;
		if(isset($_GET['target'])){
			if(''!==$_GET['target']){
				$filter_target=(int)$_GET['target'];
				if(!isset($admin_files_target[$filter_target])){
					$filter_target=false;
				}
			}
		}

		$filter_address=false;
		$filter_address_id=false;
		if(isset($_GET['address'])){
			if(''!==$_GET['address']){
				$filter_address=$_GET['address'];
				$filter_address_id=$db->select_one('addresses','id',"WHERE `address`='".$db->prepare($_GET['address'])."'");
				if(!$filter_address_id){
					$filter_address_id=-1;
				}
			}
		}
		print '
		<form action="" method="GET">
		<div class="filters-wrapper">
			<div>
				<input name="address" class="form-post-by-enter" placeholder="Uploader address" value="'.($filter_address?htmlspecialchars($filter_address):'').'">
			</div>
			<div>
				<select name="target" onchange="$(this).closest(\'form\')[0].submit()">
					<option value=""'.(false===$filter_target?' selected':'').'>Filter by target</option>';
					foreach($admin_files_target as $target_type=>$target_caption){
						print '<option value="'.$target_type.'"'.($target_type===$filter_target?' selected':'').'>Target: '.htmlspecialchars($target_caption).'</option>';
					}
					print '
				</select>
			</div>
		</div>
		</form>';
		print '
		<div class="flex flex-col">
			<div class="table-wrapper">
			<div class="py-2 inline-block min-w-full">
			<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th>Actions</th>';
			print '<th>File</th>';
			print '<th>Name</th>';
			print '<th>Size</th>';
			print '<th>Target</th>';
			print '<th>Time</th>';
			print '<th>Uploader</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
		$sql_addon=[];
		if(false!==$filter_target){
			$sql_addon[]='`target`='.$filter_target;
		}
		if(false!==$filter_address_id){
			$sql_addon[]='`address`='.$filter_address_id;
		}
		$sql_addon_str='';
		if(count($sql_addon)>0){
			$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
		}
		$per_page=50;
		$count=$db->table_count('files',$sql_addon_str);
		$pages_count=ceil($count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$files=$db->sql("SELECT * FROM `files`".$sql_addon_str." ORDER BY `id` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		foreach($files as $file_arr){
			print '<tr>';
			print '<td>';
			print '<a href="/admin/files/delete/'.$file_arr['id'].'/" class="red-btn">Delete</a>';
			print '</td>';

			print '<td class="text-sm">';
			print '<a href="'.htmlspecialchars($file_arr['relative_path']).'" target="_blank">';
			print htmlspecialchars(substr($file_arr['relative_path'],strrpos($file_arr['relative_path'],'/')+1));
			print '</a>';
			print '</td>';

			print '<td class="text-sm">'.htmlspecialchars($file_arr['name']).'</td>';
			print '<td class="whitespace-nowrap">'.human_readable_filesize($file_arr['size']).'</td>';

			print '<td>';
			$target_link=false;
			if($file_arr['target_id']){
				if(1==$file_arr['target']){
					$target_link='/admin/content/edit/'.$file_arr['target_id'].'/';
				}
				if(2==$file_arr['target']){
					$target_link='/admin/events/edit/'.$file_arr['target_id'].'/';//will redirected to event edit page
				}
			}
			if($target_link){
				print '<a href="'.htmlspecialchars($target_link).'">';
			}
			print htmlspecialchars($admin_files_target[$file_arr['target']]);
			if($target_link){
				print '</a>';
			}
			print '</td>';


			if($file_arr['time']){
				print '<td><span class="time" data-time="'.$file_arr['time'].'">'.date('d.m.Y H:i:s',$file_arr['time']).' GMT</span></td>';
			}
			else{
				print '<td>&mdash;</td>';
			}

			$uploader='&mdash;';
			if($file_arr['address']){
				$uploader_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$file_arr['address']."'");
				if($uploader_arr){
					$uploader=htmlspecialchars($uploader_arr['address']);
				}
			}
			print '<td>';
			print $uploader;
			print '</td>';

			print '</tr>';
		}
		print '</tbody>';
		print '</table>';
		print '
					</div>
				</div>
			</div>
		</div>';

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';
	}
}
elseif('phpinfo'==$path_array[2]){
	phpinfo();
}
else{
	http_response_code(404);
	print '<h1>'.htmlspecialchars($path_array[2]).'</h1>';
	print '<p>Page not found.</p>';
}

$content=ob_get_contents();
ob_end_clean();