<?php
set_time_limit(0);
$lock_fp=fopen($root_dir.'/check_subscription.lock','w');
//check lock file
if(!flock($lock_fp,LOCK_EX|LOCK_NB)){
	exit;
}

$organizers=$db->sql("SELECT * FROM `organizers` WHERE `subscription_expiration`!=0 AND `subscription_expiration`<'".time()."'");
foreach($organizers as $organizer){//expired subscription
	//clear subscription plan
	$db->sql('UPDATE `organizers` SET `subscription_plan`=0, `subscription_expiration`=0, `max_upload_files_size`=0 WHERE `id`="'.$organizer['id'].'"');
	//get all administators and send them notification about approved organizer
	$org_administation=$db->sql("SELECT `address` FROM `organizer_addresses` WHERE `organizer`='".$organizer['id']."'");
	foreach($org_administation as $org_administator){
		add_notify($org_administator['address'],0,1,'org_expired_subscription',json_encode(['organizer_title'=>htmlspecialchars($organizer['title']),'organizer_url'=>htmlspecialchars($organizer['url'])]));
	}
}

//close lock file
flock($lock_fp,LOCK_UN);
fclose($lock_fp);
exit;