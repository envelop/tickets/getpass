<?php
header("Content-type:text/html; charset=UTF-8");
@ob_end_clean();
@ob_end_clean();
//check referer
if(!isset($_SERVER['HTTP_REFERER'])){
	http_response_code(403);
	exit;
}
else{
	//check referer is on the same domain
	if(!preg_match('/^https?:\/\/'.$_SERVER['HTTP_HOST'].'/',$_SERVER['HTTP_REFERER'])){
		http_response_code(403);
		exit;
	}
}
if('organizers-list'==$path_array[2]){
	$available_organizers=0;
	foreach($auth['organizers_list'] as $auth_organizer_id=>$auth_organizer_status){
		$auth_organizer_arr=$db->sql_row("SELECT `id` FROM `organizers` WHERE `id`='".$db->prepare($auth_organizer_id)."' AND `status`<2");//0 (waiting approve) or 1 (approved)
		if(null!=$auth_organizer_arr){
			$available_organizers++;
		}
	}
	if(0<$available_organizers){
		krsort($auth['organizers_list'],SORT_NUMERIC);
		foreach($auth['organizers_list'] as $auth_organizer_id=>$auth_organizer_status){
			$auth_organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($auth_organizer_id)."'");
			if(null!=$auth_organizer_arr){
				if(2>$auth_organizer_arr['status']){//0 (waiting approve) or 1 (approved)
					print '<li><a href="/@'.$auth_organizer_arr['url'].'/manage/events/create/">'.htmlspecialchars($auth_organizer_arr['title']).'</a></li>';
				}
			}
		}
	}
	else{
		print '<li>There is no available organizers</li>';
	}
}
if('user-badge'==$path_array[2]){
	if(isset($path_array[3])){
		if($path_array[3]){
			$event_id=(int)$path_array[3];
			$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_id."'");
			if(null!==$event_arr){
				//update user badge components
				$event_url=$event_arr['url'];
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");
				$organizer_url=$organizer_arr['url'];
				$replace['user_badge']=event_user_badge($event_id,$event_url,$organizer_url);
				$replace['notify_badge']=str_replace('href="/notifications/"','href="/@'.$organizer_url.'/'.$event_url.'/notifications/"',$replace['notify_badge']);

				//check user status and force update if not checked in
				if($auth['addresses']){
					$find=false;
					$allow_event_manage=false;
					foreach($auth_address_arr as $auth_address_item){
						$search_event_user=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_id."' AND `address`='".$auth_address_item['id']."'");
						if(null!=$search_event_user){
							if($search_event_user['admin']){
								$allow_event_manage=true;
								$find=false;//if previous user not manager, but current is - need update user badge
							}
							if($search_event_user['manager']){
								$allow_event_manage=true;
								$find=false;//if previous user not manager, but current is - need update user badge
							}
							if(!$find){
								$event_user=$search_event_user;
								$find=true;//fill user badge only once
							}
							if($allow_event_manage){
								break;//have permission to manage event
							}
						}
					}
					if(!$find){//user not found in event, try set to update
						//cropped part from events profile.php, set update flag for user addresses
						$address_update_timeout=(300)-1;//5 min
						if(isset($path_array[4])){
							if('lazy'==$path_array[4]){//lazy mode (bigger range) for secondary event pages
								$address_update_timeout=(600)-1;//10 min
							}
						}
						foreach($auth_address_arr as $address_arr){
							if(0==$address_arr['update']){//not setted to update
								if(time()>($address_arr['update_time']+$address_update_timeout)){
									//update=2 for forced update (ignore last update time)
									$db->sql("UPDATE `addresses` SET `update`='2' WHERE `id`='".$address_arr['id']."'");
								}
							}
						}
					}
				}
			}
		}
	}
	//hide loading badge
	//https://gitlab.com/envelop/tickets/getpass-land/-/issues/2#note_1325948673
	/*
	$loading_badge='<span class="loading"><img src="/loader.svg" class="rotate"></span>';
	print $loading_badge;
	*/

	print $replace['theme_mode_badge'].PHP_EOL;
	print $replace['attention_badge'].PHP_EOL;
	print $replace['notify_badge'].PHP_EOL;
	print $replace['user_badge'].PHP_EOL;
}
exit;