<?php
ob_start();
if($path_array[2]){
	$address_id=$path_array[2];
	//check auth address
	$check_auth_address=$db->sql_row("SELECT * FROM `auth_addresses` WHERE `auth`='".$auth['id']."' AND `address`='".$db->prepare($address_id)."'");
	if(null==$check_auth_address['id']){
		http_response_code(404);
		exit;
	}
	$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$db->prepare($address_id)."'");
	if(null==$address_arr['id']){
		http_response_code(404);
		exit;
	}

	$success_arr=[];
	$error_arr=[];

	$update_timeout=1800;//30 min
	$update_timeout_error=300;//5 min

	if(isset($_GET['update'])){
		$chain_id=0;
		if(isset($_GET['chain'])){
			$chain_id=(int)$_GET['chain'];
		}
		if($chain_id){
			$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$db->prepare($chain_id)."' AND `status`=0");
			if(null==$chain_arr['id']){
				http_response_code(404);
				exit;
			}
			$oracle_update_arr=$db->sql_row("SELECT * FROM `oracle_updates` WHERE `address`='".$address_arr['id']."' AND `chain`='".$chain_arr['id']."' ORDER BY `status_time` DESC LIMIT 1");
			if(null!=$oracle_update_arr){
				$update_time_offset=time()-$oracle_update_arr['status_time'];
				$update_status_timeout=$update_timeout;
				if(2==$oracle_update_arr['status']){
					$update_status_timeout=$update_timeout_error;
				}
				if($update_time_offset>$update_status_timeout){
					$db->sql("INSERT INTO `oracle_updates` (`address`,`chain`,`status_time`) VALUES ('".$db->prepare($address_id)."','".$db->prepare($chain_arr['id'])."','".time()."')");
					$success_arr[]='Update added to queue for chain: '.$chain_arr['name'];
				}
				else{
					$error_arr[]='Update already was added, please wait '.($update_timeout/60).' minutes to try again for chain: '.$chain_arr['name'];
				}
			}
			else{
				$db->sql("INSERT INTO `oracle_updates` (`address`,`chain`,`status_time`) VALUES ('".$db->prepare($address_id)."','".$db->prepare($chain_arr['id'])."','".time()."')");
				$success_arr[]='Update added to queue for chain: '.$chain_arr['name'];
			}
		}
		else{
			$chains=$db->sql("SELECT * FROM `chains` WHERE `type`='".$db->prepare($address_arr['type'])."' AND `status`=0");
			if(count($chains)){
				foreach($chains as $chain_arr){
					$oracle_update_arr=$db->sql_row("SELECT * FROM `oracle_updates` WHERE `address`='".$address_arr['id']."' AND `chain`='".$chain_arr['id']."' ORDER BY `status_time` DESC LIMIT 1");
					if(null!=$oracle_update_arr){
						$update_time_offset=time()-$oracle_update_arr['status_time'];
						$update_status_timeout=$update_timeout;
						if(2==$oracle_update_arr['status']){
							$update_status_timeout=$update_timeout_error;
						}
						if($update_time_offset>$update_status_timeout){
							$db->sql("INSERT INTO `oracle_updates` (`address`,`chain`,`status_time`) VALUES ('".$db->prepare($address_id)."','".$db->prepare($chain_arr['id'])."','".time()."')");
							$success_arr[]='Update added to queue for chain: '.$chain_arr['name'];
						}
						else{
							$error_arr[]='Update already was added, please wait '.($update_timeout/60).' minutes to try again for chain: '.$chain_arr['name'];
						}
					}
					else{
						$db->sql("INSERT INTO `oracle_updates` (`address`,`chain`,`status_time`) VALUES ('".$db->prepare($address_id)."','".$db->prepare($chain_arr['id'])."','".time()."')");
						$success_arr[]='Update added to queue for chain: '.$chain_arr['name'];
					}
				}
			}
		}
	}
	print '<h1 class="long">';
	print '<a href="#" class="avatar"><svg data-jdenticon-value="'.$address_arr['address'].'" width="80" height="80"  alt="avatar"></svg></a>';
	print $address_arr['address'].'</h1>';
	print '<hr>';
	if(0!=count($success_arr)){
		print '<div class="success-box" role="alert">
		<p class="font-bold">Success</p>';
		foreach($success_arr as $success_str){
			print '<p>'.$success_str.'</p>';
		}
		print '</div>';
	}
	if(0!=count($error_arr)){
		print '<div class="attention-box" role="alert">
		<p class="font-bold">Error</p>';
		foreach($error_arr as $error_str){
			print '<p>'.$error_str.'</p>';
		}
		print '</div>';
	}
	print '<div class="text-content mb-8">';
	//print '<p>Address type: '.$types_arr[$address_arr['type']]['name'].'</p>';
	if(1==$address_arr['status']){
		print '<p>Address status: <b>admin</b></p>';
	}
	print '<p>Address first signed: '.date('d.m.Y H:i',$address_arr['time']).'</p>';
	print '</div>';

	if(isset($auth_address_arr[$address_id]['platforms'])){
		if(0<$auth_address_arr[$address_id]['platforms']){
			$delete_caption='Request to delete';
			if(false==$config['link_platform_removal_moderation']){
				$delete_caption='Delete';
			}
			print '<h2 class="mt-4">Platforms</h2>';
			print '<hr>';
			print '<div class="text-content mb-8">';
			foreach($auth['platforms_list'] as $platform_id=>$platform_arr){
				if($address_id==$platform_arr['address']){
					print '<p>';
					if(1==$platform_arr['platform']){//Telegram
						print 'Telegram: ';
						if($platform_arr['internal_username']){
							print '@'.htmlspecialchars($platform_arr['internal_username']).'';
						}
						else{
							print '#'.htmlspecialchars($platform_arr['internal_id']).'';
						}
					}
					elseif(2==$platform_arr['platform']){//Email
						print 'Email';
						if($platform_arr['internal_username']){
							print ': '.htmlspecialchars($platform_arr['internal_username']).'';
						}
					}
					print ' - Status: ';
					print '<span class="'.$linked_platforms_status_style_arr[$platform_arr['status']].'">'.$linked_platforms_status_arr[$platform_arr['status']].'</span>';

					if($config['link_platform_removal_moderation']){
						$check_request_exist=$db->table_count('requests',"WHERE `target`='1' AND `target_id`='".$platform_arr['id']."' AND `address`='".$platform_arr['address']."' AND `status`='0'");
						if(0==$check_request_exist){
							print ' <a href="/link-platform/delete/'.$platform_arr['id'].'/?'.gen_csrf_param().'" class="ml-2 red-btn default-button">'.$delete_caption.'</a>';
						}
					}
					else{
						print ' <a href="/link-platform/delete/'.$platform_arr['id'].'/?'.gen_csrf_param().'" class="ml-2 red-btn default-button">'.$delete_caption.'</a>';
					}
					print '</p>';
				}
			}
			print '</div>';
		}
	}

	if(0<count($auth_address_arr[$address_id]['organizers'])){
		print '<h2 class="mt-4">Organizers</h2>';
		print '<hr>';
		print '<div class="text-content mb-8">';
		foreach($auth_address_arr[$address_id]['organizers'] as $organizer_id=>$organizer_status){
			$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($organizer_id)."'");
			if(null!=$organizer_arr){
				//Changes https://gitlab.com/envelop/tickets/getpass-land/-/issues/42
				print '<p class="mb-4'.(1!=$organizer_arr['status']?' opacity-50 hover:opacity-75':'').'"><a href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/">'.htmlspecialchars($organizer_arr['title']).'</a>';
				/*
				if($organizer_arr['description']){
					print ' &mdash; '.htmlspecialchars($organizer_arr['description']);
				}
				*/
				if(1!=$organizer_arr['status']){//for not approved organizers
					print ' <span class="'.$organizers_status_arr_class[$organizer_arr['status']].'">('.htmlspecialchars($organizers_status_arr[$organizer_arr['status']]).')</span>';
				}
				//print ' <a class="violet-btn" href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/">Manage organizer'.(1==$organizer_status?' (owner)':'').'</a>';
				print '</p>';
			}
		}
		print '</div>';

		$events_result='';
		$events_count=0;
		foreach($auth['organizers_list'] as $organizer_id=>$organizer_status){
			$events_arr=$db->sql("SELECT * FROM `events` WHERE `organizer`='".$db->prepare($organizer_id)."' AND `status`<'3' ORDER BY `id` DESC");//0 - planning/draft, 1 - active/published, 2 - archived, 3 - deleted/hidden
			foreach($events_arr as $event_arr){
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($event_arr['organizer'])."'");
				if(3!=$organizer_arr['status']){//hidden
					$events_count++;
					if($events_count<=3){
						$events_result.='<p class="mb-4">';
						$events_result.='<a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/manage/" target="_blank">'.htmlspecialchars($event_arr['title']).'</a>';
						/*
						//https://gitlab.com/envelop/tickets/getpass/-/issues/19#note_1390596817
						if(1!=$event_arr['status']){//for not active events
							$events_result.=' <span class="'.$events_status_style_arr[$event_arr['status']].'">('.htmlspecialchars($events_status_arr[$event_arr['status']]).')</span>';
						}
						if(1!=$event_arr['moderation']){//not approved
							$events_result.=' <span class="'.$events_moderation_style_arr[$event_arr['moderation']].'">('.htmlspecialchars($events_moderation_arr[$event_arr['moderation']]).')</span>';
						}
						*/
						$events_result.=' by ';
						$events_result.='<a href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/" target="_blank">'.htmlspecialchars($organizer_arr['title']).'</a>';
						if(1!=$organizer_arr['status']){//for not approved organizers
							$events_result.=' <span class="'.$organizers_status_arr_class[$organizer_arr['status']].'">('.htmlspecialchars($organizers_status_arr[$organizer_arr['status']]).')</span>';
						}
						$events_result.='</p>';
					}
				}
			}
			if($events_count>=3){
				break;
			}
		}
		if(''!=$events_result){
			print '<h2 class="mt-4">Events</h2>';
			print '<hr>';
			print '<div class="text-content mb-8">';
			print $events_result;
			if($events_count>3){
				print '<p><a href="/my-events/" class="text-sm action-btn navigate default-button">View all</a></p>';
			}
			print '</div>';
		}
	}
	print '<h2 class="mt-4">NFT list by chains</h2>';
	print '<hr>';
	print '<div class="text-content mb-8">';
	$chains=$db->sql("SELECT * FROM `chains` WHERE `type`='".$address_arr['type']."' AND `status`=0 ORDER BY `sort` ASC");
	foreach($chains as $chain){
		print '<h3>'.$chain['name'].'</h3>';
		$nft_list=$db->sql("SELECT * FROM `nft_list` WHERE `address`='".$address_arr['id']."' AND `chain`='".$chain['id']."'");
		if($nft_list->num_rows){
			print '
			<div class="flex flex-col">
				<div class="table-wrapper">
					<div class="py-2 inline-block min-w-full">
						<div class="overflow-hidden">';
			print '<table class="table-auto min-w-full">';
			print '<thead class="bg-white border-b">';
			print '<tr>';
			print '<th scope="col">NFT contract</th>';
			print '<th scope="col">Token ID</th>';
			print '<th scope="col">Update time</th>';
			print '</tr>';
			print '</thead>';
			print '<tbody>';
			foreach($nft_list as $nft){
				$nft_url='';
				if($chain['nft_url']){
					$nft_url=str_replace('{contract}',$nft['contract'],$chain['nft_url']);
					$nft_url=str_replace('{token_id}',$nft['token_id'],$nft_url);
				}
				print '<tr>';
				print '<td>'.$nft['contract'].'</td>';
				print '<td>';
				if($nft_url){
					print '<a href="'.$nft_url.'" target="_blank">'.$nft['token_id'].'</a>';
				}
				else{
					print $nft['token_id'];
				}
				print '</td>';
				print '<td>'.date('d.m.Y H:i',$nft['time']).'</td>';
				print '</tr>';
			}
			print '</tbody>';
			print '</table>';
			print '</div></div></div></div>';
			$oracle_update_arr=$db->sql_row("SELECT * FROM `oracle_updates` WHERE `address`='".$address_arr['id']."' AND `chain`='".$chain['id']."' ORDER BY `status_time` DESC LIMIT 1");
			if(null!=$oracle_update_arr){
				if(0==$oracle_update_arr['status']){
					print '<p class="text-green-600 text-sm mb-4">Update in progress...</p>';
				}
				else
				if(1==$oracle_update_arr['status']){
					print '<p class="text-gray-400 text-sm mb-4">Last update '.date('d.m.Y H:i',$oracle_update_arr['status_time']).'</p>';
					$update_time_offset=time()-$oracle_update_arr['status_time'];
					$update_status_timeout=$update_timeout;
					if(2==$oracle_update_arr['status']){
						$update_status_timeout=$update_timeout_error;
					}
					if($update_time_offset>$update_status_timeout){
						print '<div class="my-4"><a href="?update=1&chain='.$chain['id'].'" class="reverse-btn">Update NFT list</a></div>';
					}
				}
				else
				if(2==$oracle_update_arr['status']){
					print '<p class="text-red-500 text-sm mb-4">Last update was stopped with error '.date('d.m.Y H:i',$oracle_update_arr['status_time']).'</p>';
					print '<div class="my-4"><a href="?update=1&chain='.$chain['id'].'" class="reverse-btn">Update NFT list</a></div>';
				}
			}
			else{
				print '<div class="my-4"><a href="?update=1&chain='.$chain['id'].'" class="reverse-btn">Update NFT list</a></div>';
			}
		}
		else{
			print '<p class="mb-4">Empty</p>';
			$oracle_update_arr=$db->sql_row("SELECT * FROM `oracle_updates` WHERE `address`='".$address_arr['id']."' AND `chain`='".$chain['id']."' ORDER BY `status_time` DESC LIMIT 1");
			if(null!=$oracle_update_arr){
				if(0==$oracle_update_arr['status']){
					print '<p class="text-green-600 text-sm mb-4">Update in progress...</p>';
				}
				else
				if(1==$oracle_update_arr['status']){
					print '<p class="text-gray-400 text-sm mb-4">Last update '.date('d.m.Y H:i',$oracle_update_arr['status_time']).'</p>';
					$update_time_offset=time()-$oracle_update_arr['status_time'];
					$update_status_timeout=$update_timeout;
					if(2==$oracle_update_arr['status']){
						$update_status_timeout=$update_timeout_error;
					}
					if($update_time_offset>$update_status_timeout){
						print '<div class="my-4"><a href="?update=1&chain='.$chain['id'].'" class="reverse-btn">Update NFT list</a></div>';
					}
				}
				else
				if(2==$oracle_update_arr['status']){
					print '<p class="text-red-500 text-sm mb-4">Last update was stopped with error '.date('d.m.Y H:i',$oracle_update_arr['status_time']).'</p>';
					print '<div class="my-4"><a href="?update=1&chain='.$chain['id'].'" class="reverse-btn">Update NFT list</a></div>';
				}
			}
			else{
				print '<div class="my-4"><a href="?update=1&chain='.$chain['id'].'" class="reverse-btn">Update NFT list</a></div>';
			}
		}
	}
	print '</div>';
	print '<hr>';
	print '<div class="my-4">
	<a href="/profile/" class="reverse-btn">&larr; Back to profile</a>
	</div>';
}
else{
	$replace['title']='Profile | '.$replace['title'];
	print '<div class="admin-title-wrapper">';
	print '<h1>Profile</h1>';
	if($config['manual_profile_link']){
		print '<div class="button-wrapper">';
		print '<a href="'.$config['manual_profile_link'].'" target="_blank" class="action-btn">'.$ltmp['icons']['video'].' '.$config['manual_profile_caption'].'</a>';
		print '</div>';
	}
	print '</div>';
	print '<hr class="my-4">';
	print '<h2>Linked addresses</h2>';
	if(0==$auth['addresses']){
		print '<p>None was found in current session. Please use <a href="/login/?back_url=/profile/">Login page</a>.</p>';
	}
	else{
		print '<div class="text-content">';
		foreach($auth_address_arr as $auth_address){
			print '<div class="mb-4 address-item">';
			print '<a href="#" class="avatar"><svg data-jdenticon-value="'.$auth_address['address'].'" width="80" height="80"  alt="avatar"></svg></a>';
			print '<a class="'.($auth['default_address']==$auth_address['address']?' font-semibold':'').'" href="/profile/'.$auth_address['id'].'/" title="Profile address card: Platforms, Organizers, NFT collection, etc.">'.$auth_address['address'].'</a>';
			//hide until we have more types
			//print ' ('.$types_arr[$auth_address['type']]['name'].')';
			print '</div>';
		}
		print '</div>';
		if(0<$auth['platforms']){
			$delete_caption='Request to delete';
			if(false==$config['link_platform_removal_moderation']){
				$delete_caption='Delete';
			}
			print '<h2 class="mt-4">Platforms</h2>';
			print '<hr>';
			print '<div class="text-content mb-8">';
			foreach($auth['platforms_list'] as $platform_id=>$platform_arr){
				print '<p>';
				if(1==$platform_arr['platform']){//Telegram
					print 'Telegram: ';
					if($platform_arr['internal_username']){
						print '@'.htmlspecialchars($platform_arr['internal_username']).'';
					}
					else{
						print '#'.htmlspecialchars($platform_arr['internal_id']).'';
					}
				}
				elseif(2==$platform_arr['platform']){//Email
					print '<p>Email';
					if($platform_arr['internal_username']){
						print ': '.htmlspecialchars($platform_arr['internal_username']).'';
					}
				}
				print ' - Status: ';
				print '<span class="'.$linked_platforms_status_style_arr[$platform_arr['status']].'">'.$linked_platforms_status_arr[$platform_arr['status']].'</span>';
				if($config['link_platform_removal_moderation']){
					$check_request_exist=$db->table_count('requests',"WHERE `target`='1' AND `target_id`='".$platform_arr['id']."' AND `address`='".$platform_arr['address']."' AND `status`='0'");
					if(0==$check_request_exist){
						print ' <a href="/link-platform/delete/'.$platform_arr['id'].'/?'.gen_csrf_param().'" class="ml-2 red-btn default-button">'.$delete_caption.'</a>';
					}
				}
				else{
					print ' <a href="/link-platform/delete/'.$platform_arr['id'].'/?'.gen_csrf_param().'" class="ml-2 red-btn default-button">'.$delete_caption.'</a>';
				}
				print '</p>';
			}
			print '</div>';
		}
		//https://gitlab.com/envelop/tickets/getpass-land/-/issues/35#note_1352565079
		/*
		else{
			print '<h2 class="mt-4">Platforms</h2>';
			print '<hr class="my-4">';
			print '<div class="text-content mb-8">';
			print '<p>Please assign your email address. This will allow you to receive notifications from service, subscribe to organizers and events news.</p>';
			print '<form action="/link-platform/2/" method="POST" class="manage-card">';
			print '<div class="my-4"><input type="text" name="name" placeholder="Name" required></div>';
			print '<div class="my-4"><input type="text" name="email" placeholder="Email" required></div>';
			print gen_csrf_form();
			print '<input type="submit" value="Assign email" class="action-btn big">';
			if($auth['addresses']>1){
				print '<br><span class="text-sm text-gray-500">Email will be assigned to default address</span>';
			}
			print '</form>';
			print '</div>';
		}
		*/
		if(0<$auth['organizers']){
			print '<h2 class="mt-4">Organizers</h2>';
			print '<hr>';
			print '<div class="text-content mb-8">';
			foreach($auth['organizers_list'] as $organizer_id=>$organizer_status){
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($organizer_id)."'");
				if(null!=$organizer_arr){
					//Changes https://gitlab.com/envelop/tickets/getpass-land/-/issues/42
					print '<p class="mb-4'.(1!=$organizer_arr['status']?' opacity-50 hover:opacity-75':'').'"><a href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/">'.htmlspecialchars($organizer_arr['title']).'</a>';
					/*
					if($organizer_arr['description']){
						print ' &mdash; '.htmlspecialchars($organizer_arr['description']);
					}
					*/
					if(1!=$organizer_arr['status']){//for not approved organizers
						print ' <span class="'.$organizers_status_arr_class[$organizer_arr['status']].'">('.htmlspecialchars($organizers_status_arr[$organizer_arr['status']]).')</span>';
					}
					//print ' <a class="violet-btn" href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/">Manage organizer'.(1==$organizer_status?' (owner)':'').'</a>';
					print '</p>';
				}
			}
			print '</div>';

			$events_result='';
			$events_count=0;
			foreach($auth['organizers_list'] as $organizer_id=>$organizer_status){
				$events_arr=$db->sql("SELECT * FROM `events` WHERE `organizer`='".$db->prepare($organizer_id)."' AND `status`<'3' ORDER BY `id` DESC");//0 - planning/draft, 1 - active/published, 2 - archived, 3 - deleted/hidden
				foreach($events_arr as $event_arr){
					$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($event_arr['organizer'])."'");
					if(3!=$organizer_arr['status']){//hidden
						$events_count++;
						if($events_count<=3){
							$events_result.='<p class="mb-4">';
							$events_result.='<a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/manage/" target="_blank">'.htmlspecialchars($event_arr['title']).'</a>';
							/*
							//https://gitlab.com/envelop/tickets/getpass/-/issues/19#note_1390596817
							if(1!=$event_arr['status']){//for not active events
								$events_result.=' <span class="'.$events_status_style_arr[$event_arr['status']].'">('.htmlspecialchars($events_status_arr[$event_arr['status']]).')</span>';
							}
							if(1!=$event_arr['moderation']){//not approved
								$events_result.=' <span class="'.$events_moderation_style_arr[$event_arr['moderation']].'">('.htmlspecialchars($events_moderation_arr[$event_arr['moderation']]).')</span>';
							}
							*/
							$events_result.=' by ';
							$events_result.='<a href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/" target="_blank">'.htmlspecialchars($organizer_arr['title']).'</a>';
							if(1!=$organizer_arr['status']){//for not approved organizers
								$events_result.=' <span class="'.$organizers_status_arr_class[$organizer_arr['status']].'">('.htmlspecialchars($organizers_status_arr[$organizer_arr['status']]).')</span>';
							}
							$events_result.='</p>';
						}
					}
				}
				if($events_count>=3){
					break;
				}
			}
			if(''!=$events_result){
				print '<h2 class="mt-4">Events</h2>';
				print '<hr>';
				print '<div class="text-content mb-8">';
				print $events_result;
				if($events_count>3){
					print '<p><a href="/my-events/" class="text-sm action-btn navigate default-button">View all</a></p>';
				}
				print '</div>';
			}
		}

		$tickets_counter=0;
		$tickets_binded_counter=0;
		$tickets_invited_counter=0;
		$event_tickets_counter_arr=[];
		$event_tickets_arr=[];
		$event_tickets_sort_id_arr=[];//id
		$event_tickets_sort_time_arr=[];//start time
		foreach($auth_address_arr as $auth_address){
			$find_event_users=$db->sql("SELECT * FROM `event_users` WHERE `address`='".$db->prepare($auth_address['id'])."' AND (`status`='1' OR `status`=2)");//invited or binded
			foreach($find_event_users as $find_event_user){
				$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$db->prepare($find_event_user['event'])."' AND (`status`='1' OR `status`=2) AND `moderation`='1'");//approved and active/archive
				if(null!==$event_arr){
					//count tickets
					$tickets_counter++;
					if(1==$find_event_user['status']){
						$tickets_binded_counter++;
					}
					elseif(2==$find_event_user['status']){
						$tickets_invited_counter++;
					}
					if(!isset($event_tickets_counter_arr[$find_event_user['event']])){
						$event_tickets_counter_arr[$find_event_user['event']]=0;
					}
					$event_tickets_counter_arr[$find_event_user['event']]++;

					$event_tickets_sort_id_arr[]=$event_arr['id'];
					$event_tickets_sort_time_arr[]=$event_arr['time'];
					$event_tickets_arr[]=$event_arr;
				}
			}
		}
		if($tickets_counter){
			//sort events by time desc
			array_multisort($event_tickets_sort_time_arr, SORT_DESC, $event_tickets_sort_id_arr, SORT_DESC, $event_tickets_arr);
			print '<h2 class="mt-4">Event tickets</h2>';
			print '<hr>';
			print '<div class="text-content mb-8">';
			$limit=5;
			foreach($event_tickets_arr as $event_id=>$event_arr){
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($event_arr['organizer'])."'");
				print '<p class="mb-4'.(2==$event_arr['status']?' opacity-50 hover:opacity-75':'').'"><a href="/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/" target="_blank">'.htmlspecialchars($event_arr['title']).'</a> event by <a href="/@'.htmlspecialchars($organizer_arr['url']).'/" target="_blank">'.htmlspecialchars($organizer_arr['title']).'</a>'.(2==$event_arr['status']?' (archived)':'').'</p>';
				--$limit;
				if($limit<=0){
					print '<p><a href="/event-tickets/" class="text-sm action-btn navigate default-button">View all</a></p>';
					break;
				}
			}
			print '</div>';
		}

		print '<hr class="my-4">';
		print '<div class="links-list">';
		if(0<$auth['organizers']){
			print '<a href="/add-event/" class="action-btn default-button selected">Create event</a>';
		}
		print '
		<a href="/add-organizer/" class="action-btn default-button">Create organizer</a>
		<a href="/login/?back_url=/profile/" class="action-btn default-button">Add address to current session</a>
		<a href="/link-platform/" class="action-btn default-button">Link additional platform</a>
		<a href="/mailing-rules/" class="action-btn configure default-button">Configure mailing rules</a>
		<a href="/logout/?'.gen_csrf_param().'" class="action-btn negative /*default2-button*/">Logout</a>
		</div>';
	}
}
$content=ob_get_contents();
ob_end_clean();