<?php
set_time_limit(0);
$lock_fp=fopen($root_dir.'/clear_auth_cron.lock','w');
if(!flock($lock_fp,LOCK_EX|LOCK_NB)){
	exit;
}

//update auth addresses count
$auths=$db->sql("SELECT * FROM `auth`");
foreach($auths as $auth){
	$auth_addresses_count=$db->table_count('auth_addresses','WHERE `auth`="'.$auth['id'].'"');
	$db->sql("UPDATE `auth` SET `addresses`='".$auth_addresses_count."' WHERE `id`='".$auth['id']."'");
}

//remove auth without addresses more than 30 days
$auths=$db->sql("SELECT * FROM `auth` WHERE `addresses`='0' AND `active_time`<'".(time()-30*24*60*60)."'");
foreach($auths as $auth){
	$db->sql("DELETE FROM `auth` WHERE `id`='".$auth['id']."'");
}

//remove temp auth with active_time=time and older than 1 day (only created)
$auths=$db->sql("SELECT * FROM `auth` WHERE `active_time`<'".(time()-24*60*60)."' AND `active_time`=`time`");
foreach($auths as $auth){
	$db->sql("DELETE FROM `auth` WHERE `id`='".$auth['id']."'");
}
//close lock file
flock($lock_fp,LOCK_UN);
fclose($lock_fp);
exit;