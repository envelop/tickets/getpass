<?php
ob_start();
$replace['title']='Organizers | '.$replace['title'];
print '<h1>Organizers</h1>';
print '<hr class="my-4">';
if(0==$auth['addresses']){
	print '<p>None addresses was found in current session. Please use <a href="/login/?back_url=/organizers/">Login page</a>.</p>';
}
else{
	print '<div class="text-content">';
	if($auth['default_address_id']){
		print '
		<div class="mb-4">
			<a href="/add-organizer/" class="action-btn">Create organizer</a>
		</div>';
	}
	if(0<$auth['organizers']){
		foreach($auth['organizers_list'] as $organizer_id=>$organizer_status){
			$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$db->prepare($organizer_id)."'");
			if(null!=$organizer_arr){
				print '<p class="mb-4'.(1!=$organizer_arr['status']?' opacity-75 hover:opacity-100':'').'"><a href="/@'.htmlspecialchars($organizer_arr['url']).'/">'.htmlspecialchars($organizer_arr['title']).'</a>';
				if($organizer_arr['description']){
					print ' &mdash; '.htmlspecialchars($organizer_arr['description']);
				}
				if(1!=$organizer_arr['status']){//for not approved organizers
					print ' <span class="'.$organizers_status_arr_class[$organizer_arr['status']].'">('.htmlspecialchars($organizers_status_arr[$organizer_arr['status']]).')</span>';
				}
				print ' <a class="violet-btn" href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/">Manage organizer'.(1==$organizer_status?' (owner)':'').'</a>';
				print '</p>';
			}
		}
	}
	else{
		print '<p>None organizers was found in current session. Please use <a href="/add-organizer/">Add organizer page</a></p>';
	}
	print '<hr>';
	print '<div class="my-4">
	<a href="/profile/" class="reverse-btn">&larr; Back to profile</a>
	</div>';
	print '</div>';
}
$content=ob_get_contents();
ob_end_clean();