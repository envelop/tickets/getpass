<?php
ob_start();
$replace['title']='Link platform'.' | '.$replace['title'];
if($auth['default_address_id']){
	$back_url='';
	if(isset($_GET['back_url'])){
		$back_url=urldecode($_GET['back_url']);
	}
	if(isset($_GET['back'])){
		$back_url=urldecode($_GET['back']);
	}
	if(isset($_POST['back_url'])){
		$back_url=urldecode($_POST['back_url']);
	}
	if(isset($_POST['back'])){
		$back_url=urldecode($_POST['back']);
	}
	$platform_id=0;

	$delete_caption='Request to delete';
	if(false==$config['link_platform_removal_moderation']){
		$delete_caption='Delete';
	}
	if('delete'==$path_array[2]){
		$linked_platform_id=0;
		if(isset($path_array[3])){
			$linked_platform_id=(int)$path_array[3];
		}
		if($linked_platform_id){
			$linked_platform_arr=$db->sql_row("SELECT * FROM `linked_platforms` WHERE `id`='".$linked_platform_id."'");
			if(null!=$linked_platform_arr){
				if(false==$config['link_platform_removal_moderation']){//if moderation is disabled (user can delete linked platform without moderation)
					if(check_csrf()){
						if(isset($auth_address_arr[$linked_platform_arr['address']])){
							$db->sql("UPDATE `linked_platforms` SET `status`='4', `status_time`='".time()."' WHERE `id`='".$linked_platform_arr['id']."'");
							print '<div class="success-box" role="alert">
							<p class="font-bold">Success</p>
							<p>Linked platform was deleted.</p>';
							if(2==$linked_platform_arr['platform']){//email
								print '<p>Email '.$linked_platform_arr['internal_username'].':</p>';
								print '<p>— '.$auth_address_arr[$linked_platform_arr['address']]['address'];
								//print ' ('.$types_arr[$auth_address_arr[$linked_platform_arr['address']]['type']]['name'].')';
								print '</p>';
							}
							if(1==$linked_platform_arr['platform']){//telegram
								print '<p>Telegram';
								if($linked_platform_arr['internal_username']){
									print ' @'.$linked_platform_arr['internal_username'];
								}
								if($linked_platform_arr['internal_id']){
									print ' #'.$linked_platform_arr['internal_id'];
								}
								print ':</p>';
								print '<p>— '.$auth_address_arr[$linked_platform_arr['address']]['address'];
								//print ' ('.$types_arr[$auth_address_arr[$linked_platform_arr['address']]['type']]['name'].')';
								print '</p>';
							}
							print '</div>';

							//add meta redirect to back_url if it is set
							if($back_url){
								print '<meta http-equiv="refresh" content="5; url='.htmlspecialchars($back_url).'">';
							}
						}
						else{
							print '<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>Address not found in current session for selected linked platform.</p>
							</div>';
						}
					}
					else{
						print '<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>CSRF token is invalid or expired. Please go back, reload the page and try again.</p>
						</div>';
					}
				}
				else{//need to send request to moderators
					$check_request_exist=$db->table_count('requests',"WHERE `target`='1' AND `target_id`='".$linked_platform_arr['id']."' AND `address`='".$linked_platform_arr['address']."' AND `status`='0'");
					if($check_request_exist){
						print '
						<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>Request to delete linked platform already exist.</p>
							<p>You will be redirected to the profile page in 5 seconds.</p>
						</div>';
						//redirect to profile page
						print '<meta http-equiv="refresh" content="5; url=/profile/">';
					}
					else{
						if(isset($_POST['approve'])){
							if(check_csrf(true)){
								print '<div class="success-box" role="alert">
								<p class="font-bold">Success request</p>
								<p>Linked platform was request to delete.</p>';
								if(2==$linked_platform_arr['platform']){//email
									print '<p>Email '.$linked_platform_arr['internal_username'].':</p>';
									print '<p>— '.$auth_address_arr[$linked_platform_arr['address']]['address'];
									//print ' ('.$types_arr[$auth_address_arr[$linked_platform_arr['address']]['type']]['name'].')';
									print '</p>';
								}
								if(1==$linked_platform_arr['platform']){//telegram
									print '<p>Telegram';
									if($linked_platform_arr['internal_username']){
										print ' @'.$linked_platform_arr['internal_username'];
									}
									if($linked_platform_arr['internal_id']){
										print ' #'.$linked_platform_arr['internal_id'];
									}
									print ':</p>';
									print '<p>— '.$auth_address_arr[$linked_platform_arr['address']]['address'];
									//print ' ('.$types_arr[$auth_address_arr[$linked_platform_arr['address']]['type']]['name'].')';
									print '</p>';
								}
								print '<p>The request has been sent to the moderator and will be processed within 24 hours.</p>';
								print '<p>You will be redirected to the profile page in 5 seconds.</p>';
								print '</div>';

								//add request to admins
								$db->sql("INSERT INTO `requests` SET `target`='1', `target_id`='".$linked_platform_arr['id']."', `time`='".time()."', `address`='".$linked_platform_arr['address']."'");

								//too often requests? (maybe need to add cron job for periodic sending with number of requests)
								//get all admins and send them notification about new request
								$platform_admins=$db->sql("SELECT `id` FROM `addresses` WHERE `status`='1'");
								foreach($platform_admins as $platform_admin){
									add_notify($platform_admin['id'],0,1,'admin_new_request_linked_platform_deletion',json_encode([
										'address'=>htmlspecialchars($auth_address_arr[$linked_platform_arr['address']]['address']),
									]));
								}

								//add meta redirect to back_url if it is set
								if($back_url){
									print '<meta http-equiv="refresh" content="5; url='.htmlspecialchars($back_url).'">';
								}
								else{
									print '<meta http-equiv="refresh" content="5; url=/profile/">';
								}
							}
							else{
								print '
								<div class="attention-box" role="alert">
									<p class="font-bold">Error</p>
									<p>CSRF is invalid, try again.</p>
								</div>';
							}
						}
						else{
							print '<div class="attention-box" role="alert">
							<p class="font-bold">Attention</p>
							<p>Please approve your request to delete linked platform:</p>';
							if(2==$linked_platform_arr['platform']){//email
								print '<p>Email '.$linked_platform_arr['internal_username'].':</p>';
								print '<p>— '.$auth_address_arr[$linked_platform_arr['address']]['address'];
								//print ' ('.$types_arr[$auth_address_arr[$linked_platform_arr['address']]['type']]['name'].')';
								print '</p>';
							}
							if(1==$linked_platform_arr['platform']){//telegram
								print '<p>Telegram';
								if($linked_platform_arr['internal_username']){
									print ' @'.$linked_platform_arr['internal_username'];
								}
								if($linked_platform_arr['internal_id']){
									print ' #'.$linked_platform_arr['internal_id'];
								}
								print ':</p>';
								print '<p>— '.$auth_address_arr[$linked_platform_arr['address']]['address'];
								//print ' ('.$types_arr[$auth_address_arr[$linked_platform_arr['address']]['type']]['name'].')';
								print '</p>';
							}
							print '<p>To prevent spam and malicious actions, the request will be sent to the moderator and will be processed within 24 hours.</p>';
							print '<form action="" method="POST" class="manage-card">';
							if($back_url){
								print '<input type="hidden" name="back_url" value="'.htmlspecialchars($back_url).'">';
							}
							print gen_csrf_form();
							print '<input type="submit" name="approve" value="Request remove" class="red-btn">';
							print '</form>';
							'</div>';
						}
					}
				}
			}
			else{
				print '<div class="attention-box" role="alert">
				<p class="font-bold">Error</p>
				<p>Linked platform not found.</p>
				</div>';
			}
		}
		else{
			print '<div class="attention-box" role="alert">
			<p class="font-bold">Error</p>
			<p>Linked platform not found.</p>
			</div>';
		}
	}
	else{
		if($path_array[2]){
			$platform_id=(int)$path_array[2];
		}
		if(0==$platform_id){
			print '<h1>Link platform</h1><hr>';
			print '<div class="text-content">';
			print '<p>Choose platform and assign it to address for updates:</p>';
			print '<p><ul class="text-xl">';
			$platforms=$db->sql("SELECT * FROM `platforms` WHERE `status`='1'");
			foreach($platforms as $platform_arr){
				print '<li><a href="/link-platform/'.$platform_arr['id'].'/'.($back_url?'?back='.urlencode($back_url):'').'">'.$platform_arr['name'].'</a></li>';
			}
			print '</ul></p>';
			print '<p class="italic text-gray-400 mt-4">This is optional, but highly recommended for important updates from organizers and events.</p>';
			print '</div>';
			if($back_url){
				print '<hr class="my-4"><div class="links-list"><a href="'.$back_url.'" class="reverse-btn">&larr; Return back</a></div>';
			}
			else{
				print '<hr class="my-4">';
				print '<div class="links-list">
				<a href="/profile/" class="action-btn reverse-btn /*navigate*/">&larr; Back to profile</a>
				</div>';
			}
		}
		else{
			$platform_arr=$db->sql_row("SELECT * FROM `platforms` WHERE `id`='".$platform_id."' AND `status`='1'");
			if($platform_arr){
				$replace['title']=htmlspecialchars($platform_arr['name']).' | '.$replace['title'];
				print '<h1><a href="/link-platform/">Link platform</a>: '.htmlspecialchars($platform_arr['name']).'</h1><hr>';
				print '<div class="text-content">';
				if(1==$platform_arr['id']){//telegram
					if(!isset($path_array[3])){
						$path_array[3]='';
					}
					if('confirm'==$path_array[3]){
						$confirmation_hash='';
						if(isset($path_array[4])){
							$confirmation_hash=$db->prepare($path_array['4']);
						}
						$check_hash=$_GET['hash'];
						$data_check_arr=[];
						foreach($_GET as $key=>$value){
							if($key!='hash'){
								$data_check_arr[]=$key.'='.$value;
							}
						}
						sort($data_check_arr);
						$data_check_string=implode("\n",$data_check_arr);
						$secret_key=hash('sha256',$config['telegram_api'],true);
						$hash=hash_hmac('sha256',$data_check_string,$secret_key);
						if(strcmp($hash,$check_hash)!==0) {
							print '<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>Data is not valid. <a href="/link-platform/1/">Try again.</a></p>
							</div>';
						}
						else{
							$internal_id=(int)$_GET['id'];
							$internal_username='';
							if(isset($_GET['username'])){
								if($_GET['username']){
									$internal_username=htmlspecialchars(urldecode($_GET['username']));
								}
							}
							$data=[];
							if(isset($_GET['first_name'])){
								if($_GET['first_name']){
									$data['first_name']=htmlspecialchars(urldecode($_GET['first_name']));
								}
							}
							if(isset($_GET['last_name'])){
								if($_GET['last_name']){
									$data['last_name']=htmlspecialchars(urldecode($_GET['last_name']));
								}
							}
							if(isset($_GET['photo_url'])){
								if($_GET['photo_url']){
									$data['photo_url']=htmlspecialchars(urldecode($_GET['photo_url']));
								}
							}
							$linked_platform_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `hash`='".$confirmation_hash."' AND `status`='1'");
							$success_links_arr=[];
							$redirect_back_url='';
							foreach($linked_platform_arr as $linked_platform){
								if(isset($auth_address_arr[$linked_platform['address']])){
									$db->sql("UPDATE `linked_platforms` SET `internal_id`='".$internal_id."', `internal_username`='".$db->prepare($internal_username)."', `data`='".$db->prepare(json_encode($data))."', `status`='2' WHERE `id`='".$linked_platform['id']."'");
									$linked_platform['internal_id']=$internal_id;
									$linked_platform['internal_username']=$internal_username;
									$success_links_arr[]=$linked_platform;
									if($linked_platform['back']){//set redirect url
										$redirect_back_url=$linked_platform['back'];
									}
								}
							}
							if(count($success_links_arr)>0){
								print '<div class="success-box" role="alert">
								<p class="font-bold">Success</p>';
								$unique_platform_arr=[];
								foreach($success_links_arr as $success_link){
									if(1==$success_link['platform']){//telegram
										if(!in_array($success_link['internal_username'],$unique_platform_arr)){
											print '<p>Telegram';
											if($success_link['internal_username']){
												print ' @'.htmlspecialchars($success_link['internal_username']);
											}
											else{
												print ' #'.htmlspecialchars($success_link['internal_id']);
											}
											print ':</p>';
											$unique_platform_arr[]=$success_link['internal_username'];
										}
									}
									print '<p>✔️ '.$auth_address_arr[$success_link['address']]['address'];
									//print ' ('.$types_arr[$auth_address_arr[$success_link['address']]['type']]['name'].')';
									print '</p>';
								}
								print '</div>';
								if($redirect_back_url){
									print '<p>You will redirect in 5 seconds <a href="'.htmlspecialchars($redirect_back_url).'">back to the page you came from</a>.</p>';
									$replace['head_addon'].='<meta http-equiv="refresh" content="5;url='.htmlspecialchars($redirect_back_url).'">';
								}
								else{
									print '<p>You can return to <a href="/profile/">profile page</a>.</p>';
								}
							}
							else{
								print '<div class="attention-box" role="alert">
								<p class="font-bold">Error</p>
								<p>None requests for linking was found.</p>
								</div>';
							}
						}
					}
					else{
						$find_platform_link=false;
						$addresses_unlinked=[];
						$addresses_linked=[];
						foreach($auth['platforms_list'] as $check_platform_link){
							if($check_platform_link['platform']==$platform_id){
								$find_platform_link=true;
								foreach($auth_address_arr as $auth_address){
									if($auth_address['id']==$check_platform_link['address']){
										print '<p class="text-gray-400">';
										if(0==count($addresses_linked)){
											print 'You already linked by:<br>';
										}
										$addresses_linked[]=$auth_address['id'];
										print ''.$auth_address['address'];
										//print ' ('.$types_arr[$auth_address['type']]['name'].')';
										if($check_platform_link['internal_username']){
											print ': @'.htmlspecialchars($check_platform_link['internal_username']).'';
										}
										else{
											if($check_platform_link['internal_id']){
												print ': #'.$check_platform_link['internal_id'].'';
											}
										}
										print ' - Status: ';
										print '<span class="'.$linked_platforms_status_style_arr[$check_platform_link['status']].'">'.$linked_platforms_status_arr[$check_platform_link['status']].'</span>';
										if($config['link_platform_removal_moderation']){
											$check_request_exist=$db->table_count('requests',"WHERE `target`='1' AND `target_id`='".$check_platform_link['id']."' AND `address`='".$check_platform_link['address']."' AND `status`='0'");
											if(0==$check_request_exist){
												print ' <a href="/link-platform/delete/'.$check_platform_link['id'].'/?'.gen_csrf_param().'" class="ml-2 red-btn">'.$delete_caption.'</a>';
											}
										}
										else{
											print ' <a href="/link-platform/delete/'.$check_platform_link['id'].'/?'.gen_csrf_param().'" class="ml-2 red-btn">'.$delete_caption.'</a>';
										}
										print '</p>';
									}
								}
							}
						}
						if($find_platform_link && $config['link_platform_removal_moderation']){
							print '
								<div class="attention-box" role="alert">
									<p class="font-bold">Error</p>
									<p>To create new link your previous link must be removed by moderator.</p>
									<p>You can request to remove your previous link by clicking on the "'.$delete_caption.'" button.</p>
								</div>';
						}
						else{
							foreach($auth_address_arr as $auth_address){
								if(!in_array($auth_address['id'],$addresses_linked)){
									$addresses_unlinked[]=$auth_address['id'];
								}
							}
							$sended_form=false;
							if(isset($_POST['csrf'])){
								if(check_csrf(true)){
									$errors=[];
									$addresses=[];
									if(isset($_POST['address'])){
										$find_address=false;
										foreach($auth_address_arr as $auth_address){
											if($auth_address['id']==$_POST['address']){
												$find_address=true;
											}
										}
										if(!$find_address){
											$errors[]='Address is invalid';
										}
										else{
											$addresses[]=(int)$_POST['address'];
										}
									}
									else{
										foreach($auth_address_arr as $auth_address){
											$addresses[]=$auth_address['id'];
										}
									}
									if(!$errors){
										$data=[];
										print '
										<div class="success-box" role="alert">
										<p class="font-bold">Please confirm the subscription</p>';
										print '<p>Click on widget below and confirm authorization.</p>';
										$confirmation_hash=hash('sha256',$ip.mt_rand(10000,99999).time().'telegram');
										$addresses_str_arr=[];
										$linked_platforms_ids=[];
										foreach($addresses as $address_id){
											/*
											//delete old linked platform
											$db->sql("DELETE FROM `linked_platforms` WHERE `address`='".$address_id."' AND `platform`='".$platform_id."'");
											*/
											//mark old linked platform as deleted
											$db->sql("UPDATE `linked_platforms` SET `status`='4',`status_time`='".time()."' WHERE `address`='".$address_id."' AND `platform`='".$platform_id."'");

											$db->sql("INSERT INTO `linked_platforms` (`address`,`platform`,`status`,`status_time`,`hash`,`flag1`,`flag2`,`flag3`,`flag4`,`flag5`,`data`,`back`) VALUES ('".$address_id."','".$platform_id."','1','".time()."','".$confirmation_hash."',1,1,1,1,1,'".$db->prepare(json_encode($data))."','".$db->prepare($back_url)."')");
											$linked_platforms_ids[]=$db->last_id();

											print '<p>'.$auth_address_arr[$address_id]['address'];
											//print ' ('.$types_arr[$auth_address_arr[$address_id]['type']]['name'].')';
											print '</p>';

											$addresses_str_arr[]=$auth_address_arr[$address_id]['address'];//.' ('.$types_arr[$auth_address_arr[$address_id]['type']]['name'].')';
										}
										print '</div>';
										print '<script async src="https://telegram.org/js/telegram-widget.js?19" data-telegram-login="getpass_is_bot" data-size="large" data-auth-url="'.$config['platform_url'].'/link-platform/1/confirm/'.$confirmation_hash.'/" data-request-access="write"></script>';
										$sended_form=true;
									}
									else{
										print '
										<div class="attention-box" role="alert">
											<p class="font-bold">Error</p>
											<p>'.implode('<br>',$errors).'</p>
										</div>';
									}
								}
								else{
									print '
									<div class="attention-box" role="alert">
										<p class="font-bold">Error</p>
										<p>CSRF is invalid, try again.</p>
									</div>';
								}
							}

							if(!$sended_form){
								print '<p>Please assign your telegram account using official Telegram bot for Web authentication. This will allow you to receive notifications from service bot, subscribe to organizers and events updates.</p>';
								print '<form action="/link-platform/'.$platform_id.'/" method="POST" class="manage-card">';
								if($back_url){
									print '<input type="hidden" name="back_url" value="'.htmlspecialchars($back_url).'">';
								}
								if($auth['addresses']){
									print '<div class="my-4">Address:<br><select name="address" class="w-fit">';
									$first_address=true;
									foreach($addresses_unlinked as $address_id){
										print '<option value="'.$address_id.'"'.($first_address?' selected':'').''.($address_id==$auth['default_address_id']?' class="font-bold"':'').'>'.$auth_address_arr[$address_id]['address'];
										//print ' ('.$types_arr[$auth_address_arr[$address_id]['type']]['name'].')';
										print '</option>';
										$first_address=false;
									}
									foreach($addresses_linked as $address_id){
										print '<option value="'.$address_id.'"'.($first_address?' selected':'').'>'.$auth_address_arr[$address_id]['address'];
										//print ' ('.$types_arr[$auth_address_arr[$address_id]['type']]['name'].')';
										print '</option>';
										$first_address=false;
									}
									print '</select></div>';
								}
								print gen_csrf_form();
								print '<input type="submit" value="Assign telegram" class="action-btn big">';
								if($auth['addresses']==1){
									print '<br><span class="text-sm text-gray-500">Telegram will be assigned to default address</span>';
								}
								print '</form>';
								print '</div>';

							}
						}
						if($back_url){
							print '<div class="mt-4"><a href="'.$back_url.'" class="reverse-btn">&larr; Return back</a></div>';
						}
					}
				}
				if(2==$platform_arr['id']){//email
					if(!isset($path_array[3])){
						$path_array[3]='';
					}
					if('confirm'==$path_array[3]){
						$hash=false;
						if(isset($path_array[4])){
							$hash=trim($path_array[4]);
						}

						$linked_platform_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `hash`='".$hash."' AND `status`='1'");
						$success_links_arr=[];
						foreach($linked_platform_arr as $linked_platform){
							if(isset($auth_address_arr[$linked_platform['address']])){
								$success_links_arr[]=$linked_platform;
								$db->sql("UPDATE `linked_platforms` SET `status`='2' WHERE `id`='".$linked_platform['id']."'");
							}
						}
						if(count($success_links_arr)>0){
							print '<div class="success-box" role="alert">
							<p class="font-bold">Success</p>';
							$unique_platform_arr=[];
							foreach($success_links_arr as $success_link){
								if(2==$success_link['platform']){//email
									if(!in_array($success_link['internal_username'],$unique_platform_arr)){
										print '<p>Email '.$success_link['internal_username'].':</p>';
										$unique_platform_arr[]=$success_link['internal_username'];
									}
								}
								print '<p>✔️ '.$auth_address_arr[$success_link['address']]['address'];
								//print ' ('.$types_arr[$auth_address_arr[$success_link['address']]['type']]['name'].')';
								print '</p>';
							}
							print '</div>';
							$replace['attention_badge']='';
						}
						else{
							print '<div class="attention-box" role="alert">
							<p class="font-bold">Error</p>
							<p>None requests for linking was found.</p>
							</div>';
						}
					}
					else{
						$sended_form=false;
						if(isset($_POST['csrf'])){
							if(check_csrf(true)){
								$errors=[];
								/*
								$name=trim($_POST['name']);
								if(''==$name){
									$errors[]='Name is empty';
								}
								*/
								$email=trim($_POST['email']);
								//check email by pattern
								if(!preg_match('~^[a-z0-9\.\-\_]+@[a-z0-9\.\-\_]+\.[a-z]{2,}$~iUs',$email)){
									$errors[]='Email is invalid';
								}
								$addresses=[];
								if(isset($_POST['address'])){
									$find_address=false;
									foreach($auth_address_arr as $auth_address){
										if($auth_address['id']==$_POST['address']){
											$find_address=true;
										}
									}
									if(!$find_address){
										$errors[]='Address is invalid';
									}
									else{
										$addresses[]=(int)$_POST['address'];
									}
								}
								else{
									foreach($auth_address_arr as $auth_address){
										$addresses[]=$auth_address['id'];
									}
								}
								if(!$errors){
									$data=[/*'name'=>$name*/];
									print '
									<div class="success-box" role="alert">
									<p class="font-bold">Please confirm the subscription</p>';
									print '<p>Open email '.htmlspecialchars($email).' and click on confirmation link.</p>';
									$confirmation_hash=hash('sha256',$ip.mt_rand(10000,99999).time().$email);
									$addresses_str_arr=[];
									$linked_platforms_ids=[];
									foreach($addresses as $address_id){
										/*
										//delete old linked platform
										$db->sql("DELETE FROM `linked_platforms` WHERE `address`='".$address_id."' AND `platform`='".$platform_id."'");
										*/
										//mark old linked platform as deleted
										$db->sql("UPDATE `linked_platforms` SET `status`='4',`status_time`='".time()."' WHERE `address`='".$address_id."' AND `platform`='".$platform_id."'");

										$db->sql("INSERT INTO `linked_platforms` (`address`,`platform`,`internal_username`,`status`,`status_time`,`hash`,`flag1`,`flag2`,`flag3`,`flag4`,`flag5`,`data`) VALUES ('".$address_id."','".$platform_id."','".$db->prepare($email)."','1','".time()."','".$confirmation_hash."',1,1,1,1,1,'".$db->prepare(json_encode($data))."')");
										$linked_platforms_ids[]=$db->last_id();

										print '<p>'.$auth_address_arr[$address_id]['address'];
										//print ' ('.$types_arr[$auth_address_arr[$address_id]['type']]['name'].')';
										print '</p>';

										$addresses_str_arr[]=$auth_address_arr[$address_id]['address'];//' ('.$types_arr[$auth_address_arr[$address_id]['type']]['name'].')';
									}
									print '</div>';

									new_mail($email,'','Confirm the subscription','<font style="color:#666;">Please ignore this message if you have not sent an appointment for the addresses listed below.</font><br><br>Confirmation link:<br><br><a href="'.$config['platform_url'].'/link-platform/2/confirm/'.$confirmation_hash.'/">Approve subscription for notifications, organizers and events updates</a><br><br>Assigned to addresses:<br>'.implode('<br>',$addresses_str_arr).'');

									if(''==$back_url){//if not set back url
										$back_url='/profile/';
									}
									if($back_url){
										print '<p>You will redirect in 5 seconds <a href="'.htmlspecialchars($back_url).'">back to the page you came from</a>.</p>';
										$replace['head_addon'].='<meta http-equiv="refresh" content="5;url='.htmlspecialchars($back_url).'">';
									}
									else{
										print '<p>You can return to <a href="/profile/">profile page</a>.</p>';
									}
									$sended_form=true;
								}
								else{
									print '
									<div class="attention-box" role="alert">
										<p class="font-bold">Error</p>
										<p>'.implode('<br>',$errors).'</p>
									</div>';
								}
							}
							else{
								print '
								<div class="attention-box" role="alert">
									<p class="font-bold">Error</p>
									<p>CSRF is invalid, try again.</p>
								</div>';
							}
						}

						if(!$sended_form){
							$find_platform_link=false;
							$addresses_unlinked=[];
							$addresses_linked=[];
							foreach($auth['platforms_list'] as $check_platform_link){
								if($check_platform_link['platform']==$platform_id){
									$find_platform_link=true;
									foreach($auth_address_arr as $auth_address){
										if($auth_address['id']==$check_platform_link['address']){
											print '<p class="text-gray-400">';
											if(0==count($addresses_linked)){
												print 'You already linked by:<br>';
											}
											$addresses_linked[]=$auth_address['id'];
											print ''.$auth_address['address'];
											//print ' ('.$types_arr[$auth_address['type']]['name'].')';
											if($check_platform_link['internal_username']){
												print ': '.htmlspecialchars($check_platform_link['internal_username']).'';
											}
											print ' - Status: ';
											print '<span class="'.$linked_platforms_status_style_arr[$check_platform_link['status']].'">'.$linked_platforms_status_arr[$check_platform_link['status']].'</span>';
											if($config['link_platform_removal_moderation']){
												$check_request_exist=$db->table_count('requests',"WHERE `target`='1' AND `target_id`='".$check_platform_link['id']."' AND `address`='".$check_platform_link['address']."' AND `status`='0'");
												if(0==$check_request_exist){
													print ' <a href="/link-platform/delete/'.$check_platform_link['id'].'/?'.gen_csrf_param().'" class="ml-2 red-btn">'.$delete_caption.'</a>';
												}
											}
											else{
												print ' <a href="/link-platform/delete/'.$check_platform_link['id'].'/?'.gen_csrf_param().'" class="ml-2 red-btn">'.$delete_caption.'</a>';
											}
											print '</p>';
										}
									}
								}
							}
							if($find_platform_link && $config['link_platform_removal_moderation']){
								print '
									<div class="attention-box" role="alert">
										<p class="font-bold">Error</p>
										<p>To create new link your previous link must be removed by moderator.</p>
										<p>You can request to remove your previous link by clicking on the "'.$delete_caption.'" button.</p>
									</div>';
							}
							else{
								foreach($auth_address_arr as $auth_address){
									if(!in_array($auth_address['id'],$addresses_linked)){
										$addresses_unlinked[]=$auth_address['id'];
									}
								}
								print '<p>Please assign your email address. This will allow you to receive notifications from service, subscribe to organizers and events updates.</p>';
								print '<form action="/link-platform/'.$platform_id.'/" method="POST" class="manage-card">';
								if($back_url){
									print '<input type="hidden" name="back_url" value="'.htmlspecialchars($back_url).'">';
								}
								if($auth['addresses']){
									print '<div class="my-4">Address:<br><select name="address" class="w-fit">';
									$first_address=true;
									foreach($addresses_unlinked as $address_id){
										print '<option value="'.$address_id.'"'.($first_address?' selected':'').''.($address_id==$auth['default_address_id']?' class="font-bold"':'').'>'.$auth_address_arr[$address_id]['address'];
										//print ' ('.$types_arr[$auth_address_arr[$address_id]['type']]['name'].')';
										print '</option>';
										$first_address=false;
									}
									foreach($addresses_linked as $address_id){
										print '<option value="'.$address_id.'"'.($first_address?' selected':'').'>'.$auth_address_arr[$address_id]['address'];
										//print ' ('.$types_arr[$auth_address_arr[$address_id]['type']]['name'].')';
										print '</option>';
										$first_address=false;
									}
									print '</select></div>';
								}
								/*
								//https://gitlab.com/envelop/tickets/getpass/-/issues/23
								print '<div class="my-4"><input type="text" name="name" placeholder="Name" required></div>';
								*/
								print '<div class="my-4"><input type="text" name="email" placeholder="Email" required></div>';
								print gen_csrf_form();
								print '<input type="submit" value="Assign email" class="action-btn big">';
								if($auth['addresses']==1){
									print '<br><span class="text-sm text-gray-500">Email will be assigned to default address</span>';
								}
								/*
								//https://gitlab.com/envelop/tickets/getpass/-/issues/37
								print '
								<div class="attention-box" role="alert">
									<p>Only one email can be assigned to unique address.</p>
								</div>';
								*/
								print '</form>';
								print '</div>';
							}
							if($back_url){
								print '<div class="mt-4"><a href="'.$back_url.'" class="reverse-btn">&larr; Return back</a></div>';
							}
						}
					}
				}
			}
			else{
				print '
				<div class="attention-box" role="alert">
					<p class="font-bold">Error</p>
					<p>Platform not found</p>
				</div>';
			}
		}
	}
}
else{
	print '<p>Please <a href="/login/?back_url=/link-platform/">sign-in</a>, platform linking assigned to wallet address.</p>';
}
$content=ob_get_contents();
ob_end_clean();