<?php
$app=$path_array[2];
if(!isset($apps_arr[$app])){
	http_response_code(403);
	exit;
}
//redirect to login if not auth
if(0==$auth_id){
	header('location:/login/?back=/fast-auth/'.$app.'/');
	exit;
}
//redirect to login if none addresses
if(0==$auth['addresses']){
	header('location:/login/?back=/fast-auth/'.$app.'/');
	exit;
}

$db->sql("INSERT INTO `fast_auth` (`app`,`auth_id`,`time`) VALUES ('".$app."','".$auth_id."','".time()."')");
$fast_auth_id=$db->last_id();

//generate key
$fast_auth_key=hash('sha256',$app.':'.$fast_auth_id.':'.$app_secret_arr[$app]);

//redirect to app
$redirect_url=str_replace(['%ID%','%KEY%'],[$fast_auth_id,$fast_auth_key],$apps_arr[$app]);
header('location:'.$redirect_url);
exit;
