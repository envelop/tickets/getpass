<?php
set_time_limit(0);
$lock_fp=fopen($root_dir.'/recalc_files_cron.lock','w');
//check lock file
if(!flock($lock_fp,LOCK_EX|LOCK_NB)){
	exit;
}

$time_offset=time()-$config['recalc_event_files'];
//recalc event files
$events=$db->sql("SELECT * FROM `events` WHERE (`status`=0 OR `status`=1) AND `recalc_files_size_time`<'".$time_offset."'");//planned or active
foreach($events as $event){
	$summary_files_size=0;
	$files=$db->sql("SELECT * FROM `event_files` WHERE `event`='".$event['id']."'");
	foreach($files as $file){
		$summary_files_size+=(int)$file['size'];
	}
	$db->sql('UPDATE `events` SET `summary_files_size`='.$summary_files_size.', `recalc_files_size_time`='.time().' WHERE `id`="'.$event['id'].'"');
}

$time_offset=time()-$config['recalc_organizer_files'];
//recalc organizers files
$organizers=$db->sql("SELECT * FROM `organizers` WHERE (`status`=0 OR `status`=1) AND `recalc_files_size_time`<'".$time_offset."'");//waiting or approved
foreach($organizers as $organizer){
	$summary_files_size=0;
	$files=$db->sql("SELECT * FROM `organizer_files` WHERE `organizer`='".$organizer['id']."'");
	foreach($files as $file){
		$summary_files_size+=(int)$file['size'];
	}
	$events=$db->sql("SELECT * FROM `events` WHERE `organizer`='".$organizer['id']."'");
	foreach($events as $event){
		$summary_files_size+=(int)$event['summary_files_size'];
	}
	$db->sql('UPDATE `organizers` SET `summary_files_size`='.$summary_files_size.', `recalc_files_size_time`='.time().' WHERE `id`="'.$organizer['id'].'"');
}

//close lock file
flock($lock_fp,LOCK_UN);
fclose($lock_fp);
exit;