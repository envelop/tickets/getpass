<?php
set_time_limit(0);
include($root_dir.'/class/oracle_api.php');

//script work time near 1 minute with sleep between cycles
$work_time=35;//60 minus 10 seconds
$sleep_time=15;//sleep time in seconds between cycles
$start_time=time();

$lock_fp=fopen($root_dir.'/oracle_cron.lock','w');
if(!flock($lock_fp,LOCK_EX|LOCK_NB)){
	exit;
}

while(true){
	//publish content on preparing status=0 to status=1 if time is up
	$db->sql("UPDATE `content` SET `status`=1 WHERE `status`=0 AND `time`!=0 AND `time`<".time());
	$db->sql("UPDATE `organizer_content` SET `status`=1 WHERE `status`=0 AND `time`!=0 AND `time`<".time());
	$db->sql("UPDATE `event_content` SET `status`=1 WHERE `status`=0 AND `time`!=0 AND `time`<".time());

	$auto_address_update=28800;//86400=1 day, 432000=5 days
	//turn on update flag for addresses that updated more than $auto_address_update
	$db->sql("UPDATE `addresses` SET `update`=1 WHERE `update_time`<".(time()-$auto_address_update));

	$update_timeout=1800;//3600=1 hour
	$update_timeout_error=300;//600=10 min
	//create update to addresses for each chains same type with update flag
	$addresses=$db->sql("SELECT * FROM `addresses` WHERE `update`!=0 LIMIT 1000");//1 for scheduled update, 2 for forced update
	foreach($addresses as $address_arr){
		$forced=false;
		if($address_arr['update']==2){
			$forced=true;
		}
		//search address in event_invites and create event user if founded
		$event_invites_arr=$db->sql("SELECT * FROM `event_invites` WHERE `type`='".$address_arr['type']."' AND `address`='".$address_arr['address']."' AND `status`=0");
		foreach($event_invites_arr as $event_invite_arr){//founded event invite
			//check event status
			$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$event_invite_arr['event']."'");
			if(null!=$event_arr){
				error_log('Event status = '.$event_arr['status']);
				if($event_arr['status']<2){//event still planning or active
					//get or create event user
					$event_users_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_invite_arr['event']."' AND `address`='".$address_arr['id']."'");
					$event_user_id=0;
					if(null==$event_users_arr){
						//event_users not finded, add it as invited (status=2)
						$db->sql("INSERT INTO `event_users` (`event`,`address`,`status`,`invited`) VALUES ('".$event_invite_arr['event']."','".$address_arr['id']."','2','".$event_invite_arr['id']."')");
						$event_user_id=$db->last_id();
					}
					else{
						$event_user_id=$event_users_arr['id'];
					}

					//update level and flags (admin, manager, speaker, sponsor) for event_users
					$db->sql("UPDATE `event_users` SET `level`='".$event_invite_arr['level']."', `admin`='".$event_invite_arr['admin']."', `manager`='".$event_invite_arr['manager']."', `speaker`='".$event_invite_arr['speaker']."', `sponsor`='".$event_invite_arr['sponsor']."' WHERE `id`='".$event_user_id."'");

					//update invite status
					$db->sql("UPDATE `event_invites` SET `status`=1, `time`='".time()."' WHERE `id`='".$event_invite_arr['id']."'");
				}
				else{
					//event is finished, set invite as expired
					$db->sql("UPDATE `event_invites` SET `status`=2, `time`='".time()."' WHERE `id`='".$event_invite_arr['id']."'");
				}
			}
			else{
				error_log('Addresses ID = '.$address_arr['id']);
				error_log('Invite ID = '.$event_invite_arr['id']);
				error_log('Event ID = '.$event_invite_arr['event'].' (not found)');
				//event not found, set invite as expired
				$db->sql("UPDATE `event_invites` SET `status`=2, `time`='".time()."' WHERE `id`='".$event_invite_arr['id']."'");
			}
		}

		//foreach chains and create update oracle task (in 1 hour)
		$chains=$db->sql("SELECT * FROM `chains` WHERE `type`='".$address_arr['type']."' AND `status`=0");
		foreach($chains as $chain_arr){
			$oracle_update_arr=$db->sql_row("SELECT * FROM `oracle_updates` WHERE `address`='".$address_arr['id']."' AND `chain`='".$chain_arr['id']."' ORDER BY `status_time` DESC LIMIT 1");
			if(null!=$oracle_update_arr){
				$update_time_offset=time()-$oracle_update_arr['status_time'];
				$update_status_timeout=$update_timeout;
				if(2==$oracle_update_arr['status']){
					$update_status_timeout=$update_timeout_error;
				}
				if($forced||($update_time_offset>$update_status_timeout)){
					$db->sql("INSERT INTO `oracle_updates` (`address`,`chain`,`status_time`) VALUES ('".$address_arr['id']."','".$chain_arr['id']."','".time()."')");
				}
			}
			else{
				$db->sql("INSERT INTO `oracle_updates` (`address`,`chain`,`status_time`) VALUES ('".$address_arr['id']."','".$chain_arr['id']."','".time()."')");
			}
		}

		$db->sql("UPDATE `addresses` SET `update`=0,`update_time`='".time()."' WHERE `id`='".$address_arr['id']."'");
	}

	//get oracle_updates with status 0
	$oracle_updates=$db->sql("SELECT * FROM `oracle_updates` WHERE `status`=0 LIMIT 1000");
	foreach($oracle_updates as $oracle_update_arr){
		$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$oracle_update_arr['chain']."'");
		$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$oracle_update_arr['address']."'");

		if(1==$chain_arr['type']){//EVM
			$nft_raw_arr=discover($oracle_api,$chain_arr['chain_id'],$address_arr['address']);
			$nft_arr=normalize_nft_arr($nft_raw_arr,$address_arr['address'],3);//type 3 for 721

			$nft_raw_arr2=discover($oracle_api,$chain_arr['chain_id'],$address_arr['address'],'1155');
			$nft_arr2=normalize_nft_arr($nft_raw_arr2,$address_arr['address'],4);//type 4 for 1155

			//merge 721 and 1155
			$full_nft_arr=array_merge($nft_arr,$nft_arr2);

			$processed_nft_list=[];
			//foreach nft and check if exists in db
			foreach($full_nft_arr as $nft_item){
				$find_nft=false;//for address
				if(isset($nft_item[3])){//is wnft known by oracle, need get original nft address and token_id
					$nft_item[0]=$nft_item[3];
					$nft_item[1]=$nft_item[4];
					$nft_item[2]=$nft_item[5];
				}
				$search_nft_arr=$db->sql("SELECT * FROM `nft_list` WHERE `type`='".$chain_arr['type']."' AND `chain`='".$chain_arr['id']."' AND `contract`='".$db->prepare($nft_item[0])."' AND `token_id`='".$db->prepare($nft_item[1])."'");
				foreach($search_nft_arr as $search_nft_item){
					if($search_nft_item['address']!=$address_arr['id']){//other holder address
						//set status to 2 (deactivation)
						$db->sql("UPDATE `nft_list` SET `status`='2', `status_time`='".time()."' WHERE `id`='".$search_nft_item['id']."'");
					}
					else{
						$processed_nft_list[]=$search_nft_item['id'];
						$find_nft=true;
					}
				}
				if(!$find_nft){
					//add new nft with status 1 (activation)
					$db->sql("INSERT INTO `nft_list` (`type`,`chain`,`address`,`contract`,`token_id`,`time`,`status`,`status_time`) VALUES ('".$chain_arr['type']."','".$chain_arr['id']."','".$address_arr['id']."','".$db->prepare($nft_item[0])."','".$db->prepare($nft_item[1])."','".time()."','1','".time()."')");
					$new_nft_item_id=$db->last_id();
					$processed_nft_list[]=$new_nft_item_id;
				}
			}

			//set status to 2 (deactivation) for all nft_list with address and type and chain and not in processed_nft_list
			$search_nft_arr=$db->sql("SELECT * FROM `nft_list` WHERE `type`='".$chain_arr['type']."' AND `chain`='".$chain_arr['id']."' AND `address`='".$oracle_update_arr['address']."'");
			foreach($search_nft_arr as $search_nft_item){
				if(!in_array($search_nft_item['id'],$processed_nft_list)){
					//set status to 2 (deactivation)
					$db->sql("UPDATE `nft_list` SET `status`='2', `status_time`='".time()."' WHERE `id`='".$search_nft_item['id']."'");
				}
			}

			$db->sql("UPDATE `oracle_updates` SET `status`=1,`status_time`='".time()."' WHERE `id`='".$oracle_update_arr['id']."'");
		}
	}

	//get nft_list with status 3 (update from oracle)
	$nft_list=$db->sql("SELECT * FROM `nft_list` WHERE `status`=3");
	foreach($nft_list as $nft_item){
		$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$nft_item['chain']."'");
		$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$nft_item['address']."'");

		if(1==$chain_arr['type']){//EVM
			$nft_raw_arr=discover_nft($oracle_api,$chain_arr['chain_id'],$nft_item['contract'],$nft_item['token_id'],'721');
			$nft_arr=normalize_nft_arr($nft_raw_arr,$address_arr['address'],3);//type 3 for 721

			$nft_raw_arr2=discover_nft($oracle_api,$chain_arr['chain_id'],$nft_item['contract'],$nft_item['token_id'],'1155');
			$nft_arr2=normalize_nft_arr($nft_raw_arr2,$address_arr['address'],4);//type 4 for 1155

			//merge 721 and 1155
			$full_nft_arr=array_merge($nft_arr,$nft_arr2);

			if(0==count($full_nft_arr)){//not found
				//set status to 2 (deactivation)
				$db->sql("UPDATE `nft_list` SET `status`='2', `status_time`='".time()."' WHERE `id`='".$nft_item['id']."'");
			}
			else{
				//set status to 0 (holded)
				$db->sql("UPDATE `nft_list` SET `status`='0', `status_time`='".time()."' WHERE `id`='".$nft_item['id']."'");
			}
		}
	}

	//get nft_list with status 2 (deactivation)
	$nft_list=$db->sql("SELECT * FROM `nft_list` WHERE `status`=2");
	foreach($nft_list as $nft_arr){
		//$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$nft_item['chain']."'");
		//$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$nft_item['address']."'");

		//check nft for binded_nft
		$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$nft_arr['id']."'");
		if(null==$binded_nft_arr){
			//binded not finded, already deactivated, ignore
		}
		else{
			$whitelist_arr=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `id`='".$binded_nft_arr['whitelist']."'");
			if(null!=$whitelist_arr){
				$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$whitelist_arr['event']."'");
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");

				//deactivate event_users
				$db->sql("UPDATE `event_users` SET `status`=0, `binded`=0, `level`=0 WHERE `event`='".$whitelist_arr['event']."' AND `address`='".$binded_nft_arr['address']."'");

				//deactivate event_invites for event_users
				$db->sql("UPDATE `event_invites` SET `status`=0, `time`=".time()." WHERE `event`='".$whitelist_arr['event']."' AND `address`='".$binded_nft_arr['address']."'");

				//deactivate event_whitelist
				$db->sql("UPDATE `event_whitelist` SET `binded_nft`=0, `time`='".time()."' WHERE `id`='".$binded_nft_arr['whitelist']."'");

				add_notify($binded_nft_arr['address'],0,1,'event_user_deactivation',json_encode([
					'organizer_title'=>htmlspecialchars($organizer_arr['title']),
					'organizer_url'=>htmlspecialchars($organizer_arr['url']),
					'event_title'=>htmlspecialchars($event_arr['title']),
					'event_url'=>htmlspecialchars($event_arr['url'])
				]));
			}
			/*
			//send notification to linked_platforms for user address
			$linked_platforms_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$binded_nft_arr['address']."'");
			foreach($linked_platforms_arr as $linked_platforms_item){
				//...send notification to platform_queue
			}
			*/
			//binded finded, remove it
			$db->sql("DELETE FROM `binded_nft` WHERE `id`='".$binded_nft_arr['id']."'");
		}
		//remove nft after deactivation
		$db->sql("DELETE FROM `nft_list` WHERE `id`='".$nft_arr['id']."'");
	}

	//get nft_list with status 1 (activation)
	$nft_list=$db->sql("SELECT * FROM `nft_list` WHERE `status`=1");
	foreach($nft_list as $nft_arr){
		//$chain_arr=$db->sql_row("SELECT * FROM `chains` WHERE `id`='".$nft_arr['chain']."'");
		$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `id`='".$nft_arr['address']."'");

		//check nft for binded_nft
		$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$nft_arr['id']."'");
		if(null==$binded_nft_arr){
			//primary is check nft in whitelist then check invites from event
			//binded not finded, check for event_whitelist
			$whitelist_arr=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `contract`='".$nft_arr['contract']."' AND `token_id`='".$nft_arr['token_id']."'");
			if(null!=$whitelist_arr){
				$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$whitelist_arr['event']."'");
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");

				//whitelisted finded, add binded_nft
				$db->sql("INSERT INTO `binded_nft` (`whitelist`,`address`,`nft`) VALUES ('".$whitelist_arr['id']."','".$nft_arr['address']."','".$nft_arr['id']."')");
				$db->sql("UPDATE `event_whitelist` SET `binded_nft`='".$nft_arr['id']."', `time`='".time()."' WHERE `id`='".$whitelist_arr['id']."'");

				//check event_users
				$event_user_id=0;
				$event_users_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$whitelist_arr['event']."' AND `address`='".$nft_arr['address']."'");
				if(null==$event_users_arr){
					//event_users not finded, add it
					$db->sql("INSERT INTO `event_users` (`event`,`address`,`status`,`binded`,`level`) VALUES ('".$whitelist_arr['event']."','".$nft_arr['address']."','1','".$nft_arr['id']."','".$whitelist_arr['level']."')");
					$event_user_id=$db->last_id();
				}
				else{
					$event_user_id=$event_users_arr['id'];
					//activate event_users from binded_nft and event_whitelist
					$db->sql("UPDATE `event_users` SET `status`=1, `binded`='".$nft_arr['id']."', `level`='".$whitelist_arr['level']."' WHERE `id`='".$event_users_arr['id']."'");
				}
				if($event_user_id){//if event_users finded or added (address in event users)
					//check event_invites for address (not activated)
					$event_invite_arr=$db->sql_row("SELECT * FROM `event_invites` WHERE `event`='".$whitelist_arr['event']."' AND `type`='".$address_arr['type']."' AND `address`='".$address_arr['address']."' AND `status`=0");
					if(null!=$event_invite_arr){
						//update level and flags (admin, manager, speaker, sponsor) for event_users
						$db->sql("UPDATE `event_users` SET `level`='".$event_invite_arr['level']."', `admin`='".$event_invite_arr['admin']."', `manager`='".$event_invite_arr['manager']."', `speaker`='".$event_invite_arr['speaker']."', `sponsor`='".$event_invite_arr['sponsor']."' WHERE `id`='".$event_user_id."'");

						//update invite status
						$db->sql("UPDATE `event_invites` SET `status`=1, `time`='".time()."' WHERE `id`='".$event_invite_arr['id']."'");
					}

					add_notify($nft_arr['address'],0,1,'event_user_activation',json_encode([
						'organizer_title'=>htmlspecialchars($organizer_arr['title']),
						'organizer_url'=>htmlspecialchars($organizer_arr['url']),
						'event_title'=>htmlspecialchars($event_arr['title']),
						'event_url'=>htmlspecialchars($event_arr['url'])
					]));
				}

				//send notification to linked_platforms for user address
				if($event_arr['check_in_message']){//if check_in_message not empty
					$clear_check_in_message=clear_html_tags($event_arr['check_in_message']);
					//check linked platforms for email (id=2) to send notification about nft activation
					$linked_platforms_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$nft_arr['address']."' AND `platform`=2 AND `status`=2 AND `flag3`=1");//flag3 - NFT-ticket activation
					foreach($linked_platforms_arr as $linked_platforms_item){
						$platform_data=json_decode($linked_platforms_item['data'],true);
						$address_str=htmlspecialchars($address_arr['address']);
						//$address_str.=' ('.$types_arr[$address_arr['type']]['name'].')';
						$username='%username%';
						if(isset($platform_data['name'])){
							$username=htmlspecialchars($platform_data['name']);
						}
						$subject=htmlspecialchars($event_arr['title']).': Successful check-in';
						$preheader='Activation of NFT-ticket from "'.htmlspecialchars($organizer_arr['title']).'"';
						$body=$clear_check_in_message;
						$body.='<br><br><font style="color:#666;">You have received this message because an NFT ticket for this event has been activated at the address below.<br><br>You can unsubscribe from event updates from <a href="'.$config['platform_url'].'/@'.htmlspecialchars($organizer_arr['url']).'/'.htmlspecialchars($event_arr['url']).'/content/">event blog page</a>.<br>Address: '.$address_str.'</font>';

						new_mail($linked_platforms_item['internal_username'],$username,$subject,$body,$preheader);
					}
					//check linked platforms for telegram (id=1) to send notification about nft activation
					$linked_platforms=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$nft_arr['address']."' AND `platform`=1 AND `status`=2 AND `flag3`=1");//flag3 - NFT-ticket activation
					foreach($linked_platforms as $linked_platform){
						telegram_queue($linked_platform['internal_id'],'sendMessage',['text'=>htmlspecialchars($event_arr['title']).': Successful check-in'.PHP_EOL.PHP_EOL.$clear_check_in_message,'parse_mode'=>'HTML']);
					}
				}
			}
			else{
				//secondary is check nft in whitelist WILDCARD then check invites from event
				//binded not finded, check for event_whitelist
				$whitelist_arr=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `contract`='".$nft_arr['contract']."' AND `token_id`='*'");
				if(null!=$whitelist_arr){
					//wildcard was found, create whitelist as copy of wildcard
					$db->sql("INSERT INTO `event_whitelist` (`event`,`chain`,`type`,`contract`,`token_id`,`binded_nft`,`level`,`time`) VALUES ('".$whitelist_arr['event']."','".$whitelist_arr['chain']."','".$whitelist_arr['type']."','".$nft_arr['contract']."','".$nft_arr['token_id']."','".$whitelist_arr['binded_nft']."','".$whitelist_arr['level']."','".time()."')");
					$whitelist_id=$db->last_id();
					$whitelist_arr=$db->sql_row("SELECT * FROM `event_whitelist` WHERE `id`='".$whitelist_id."'");
					if(null==$whitelist_arr){
						//error create whitelist
						break;
					}

					$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `id`='".$whitelist_arr['event']."'");
					$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."'");

					//whitelisted finded, add binded_nft
					$db->sql("INSERT INTO `binded_nft` (`whitelist`,`address`,`nft`) VALUES ('".$whitelist_arr['id']."','".$nft_arr['address']."','".$nft_arr['id']."')");
					$db->sql("UPDATE `event_whitelist` SET `binded_nft`='".$nft_arr['id']."', `time`='".time()."' WHERE `id`='".$whitelist_arr['id']."'");

					//check event_users
					$event_user_id=0;
					$event_users_arr=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$whitelist_arr['event']."' AND `address`='".$nft_arr['address']."'");
					if(null==$event_users_arr){
						//event_users not finded, add it
						$db->sql("INSERT INTO `event_users` (`event`,`address`,`status`,`binded`,`level`) VALUES ('".$whitelist_arr['event']."','".$nft_arr['address']."','1','".$nft_arr['id']."','".$whitelist_arr['level']."')");
						$event_user_id=$db->last_id();
					}
					else{
						$event_user_id=$event_users_arr['id'];
						//activate event_users from binded_nft and event_whitelist
						$db->sql("UPDATE `event_users` SET `status`=1, `binded`='".$nft_arr['id']."', `level`='".$whitelist_arr['level']."' WHERE `id`='".$event_users_arr['id']."'");
					}
					if($event_user_id){//if event_users finded or added (address in event users)
						//check event_invites for address (not activated)
						$event_invite_arr=$db->sql_row("SELECT * FROM `event_invites` WHERE `event`='".$whitelist_arr['event']."' AND `type`='".$address_arr['type']."' AND `address`='".$address_arr['address']."' AND `status`=0");
						if(null!=$event_invite_arr){
							//update level and flags (admin, manager, speaker, sponsor) for event_users
							$db->sql("UPDATE `event_users` SET `level`='".$event_invite_arr['level']."', `admin`='".$event_invite_arr['admin']."', `manager`='".$event_invite_arr['manager']."', `speaker`='".$event_invite_arr['speaker']."', `sponsor`='".$event_invite_arr['sponsor']."' WHERE `id`='".$event_user_id."'");

							//update invite status
							$db->sql("UPDATE `event_invites` SET `status`=1, `time`='".time()."' WHERE `id`='".$event_invite_arr['id']."'");
						}

						add_notify($nft_arr['address'],0,1,'event_user_activation',json_encode([
							'organizer_title'=>htmlspecialchars($organizer_arr['title']),
							'organizer_url'=>htmlspecialchars($organizer_arr['url']),
							'event_title'=>htmlspecialchars($event_arr['title']),
							'event_url'=>htmlspecialchars($event_arr['url'])
						]));
					}
					/*
					//send notification to linked_platforms for user address
					$linked_platforms_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$nft_arr['address']."'");
					foreach($linked_platforms_arr as $linked_platforms_item){
						//...send notification to platform_queue about
					}
					*/
				}
				else{
					//whitelisted not finded, ignore nft
					//we cant check on invites in this case, because we dont know event
					//need to check invites after address registered on platform
				}
			}
		}
		else{
			//binded finded, already activated, ignore
		}
		//change nft status to 0 (holded)
		$db->sql("UPDATE `nft_list` SET `status`=0, `status_time`='".time()."' WHERE `id`='".$nft_arr['id']."'");
	}

	//reverse matching for events
	//it can fix event whitelist if nft was added to whitelist by administration after oracle update address nft list
	$auto_event_update=432000;//5 days
	//turn on update flag for active events that updated more than $auto_event_update
	$db->sql("UPDATE `events` SET `update`=1 WHERE `status`=1 AND `update_time`<".(time()-$auto_event_update));
	//get events with update flag
	$events_arr=$db->sql("SELECT * FROM `events` WHERE `update`=1");
	foreach($events_arr as $event_arr){
		//get event whitelist
		$whitelist=$db->sql("SELECT * FROM `event_whitelist` WHERE `event`='".$event_arr['id']."' and `binded_nft`=0");
		foreach($whitelist as $whitelist_arr){
			if('*'!=$whitelist_arr['token_id']){//if token_id is NOT wildcard
				//lookup nft_list with strict and token_id
				$nft_list=$db->sql("SELECT * FROM `nft_list` WHERE `chain`='".$whitelist_arr['chain']."' AND `contract`='".$db->prepare($whitelist_arr['contract'])."' AND `token_id`='".$db->prepare($whitelist_arr['token_id'])."'");
				foreach($nft_list as $nft_arr){
					$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$nft_arr['id']."'");
					if(null==$binded_nft_arr){
						//binded not finded, need activation
						$db->sql("UPDATE `nft_list` SET `status`='1', `status_time`='".time()."' WHERE `id`='".$nft_arr['id']."'");
					}
				}
			}
			else{//wildcard in token_id
				//lookup nft_list with strict contract
				$nft_list=$db->sql("SELECT * FROM `nft_list` WHERE `chain`='".$whitelist_arr['chain']."' AND `contract`='".$db->prepare($whitelist_arr['contract'])."'");
				foreach($nft_list as $nft_arr){
					$binded_nft_arr=$db->sql_row("SELECT * FROM `binded_nft` WHERE `nft`='".$nft_arr['id']."'");
					if(null==$binded_nft_arr){
						//binded not finded, need activation
						$db->sql("UPDATE `nft_list` SET `status`='1', `status_time`='".time()."' WHERE `id`='".$nft_arr['id']."'");
					}
				}
			}
		}
		$db->sql("UPDATE `events` SET `update`=0 ,`update_time`='".time()."' WHERE `id`='".$event_arr['id']."'");
	}
	//check work time
	$now_time=time();
	if($work_time <= $now_time-$start_time){
		break;
	}
	sleep($sleep_time);
}
//close lock file
flock($lock_fp,LOCK_UN);
fclose($lock_fp);
exit;