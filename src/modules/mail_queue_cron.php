<?php
set_time_limit(0);
$replace['counters']='';
$replace['event_url']=$config['platform_url'];
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
//example for gmail smtp https://github.com/PHPMailer/PHPMailer/blob/master/examples/gmail.phps

//script work time near 1 minute with sleep between cycles
$work_time=50;//60 minus 10 seconds
$sleep_time=5;//sleep time in seconds between cycles
$start_time=time();

$lock_fp=fopen($root_dir.'/mail_queue_cron.lock','w');
if(!flock($lock_fp,LOCK_EX|LOCK_NB)){
	exit;
}
$mail=new PHPMailer(true);
$mail->CharSet = "utf-8";
//$mail->SMTPDebug = SMTP::DEBUG_SERVER;//Enable verbose debug output
$mail->SMTPDebug = SMTP::DEBUG_OFF;//Enable verbose debug output
$mail->isSMTP();//Send using SMTP
$mail->Host = $config['mail_smtp_host'];//Set the SMTP server to send through
$mail->SMTPAuth = true;//Enable SMTP authentication
$mail->Username = $config['mail_smtp_username'];//SMTP username
$mail->Password = $config['mail_smtp_password'];//SMTP password
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;//PHPMailer::ENCRYPTION_SMTPS;//Enable implicit TLS encryption
$mail->Port = $config['mail_smtp_port'];//465;//TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

$mail->setFrom('noreply@'.$config['platform_domain'],$config['mail_from_title']);
$mail->isHTML(true);//Set email format to HTML
while(true){
	//every 8 hours take addresses and form notifications from time range
	$new_notifications_time_range=8*60*60;
	$new_notifications_offset=time()-$new_notifications_time_range;
	$mailing_addresses=$db->sql("SELECT * FROM `addresses` WHERE `mailing`<'".($new_notifications_offset)."'");
	foreach($mailing_addresses as $address_item){
		$address_mailing_offset=$address_item['mailing'];
		$db->sql("UPDATE `addresses` SET `mailing`='".time()."' WHERE `id`='".$address_item['id']."'");
		$linked_platforms_count=$db->table_count('linked_platforms','WHERE `address`="'.$address_item['id'].'" AND `platform`=2 AND `status`=2');
		if(!$linked_platforms_count){//no linked platforms
			//error_log('$linked_platforms_count 0');
			continue;//ignore this address
		}
		$notifications_count=$db->table_count('notifications_queue','WHERE `address`="'.$address_item['id'].'" AND `time`>"'.($address_mailing_offset).'"');
		if(!$notifications_count){//no new notifications
			//error_log('$notifications_count 0');
			continue;//ignore this address
		}

		$address_str=htmlspecialchars($address_item['address']);//' ('.$types_arr[$address_item['type']]['name'].')';
		$username='';
		if(isset($platform_data['name'])){
			$username=htmlspecialchars($platform_data['name']);
		}

		$subject='You have '.$notifications_count.' new notifications';
		$body='';
		$altbody='';
		$notifications=$db->sql("SELECT * FROM `notifications_queue` WHERE `address`='".$address_item['id']."' AND `time`>'".($address_mailing_offset)."' ORDER BY `time` DESC");
		foreach($notifications as $notification){
			$render_preview=render_notify_preview($notification);
			$altbody.=$render_preview.PHP_EOL.($config['platform_url'].'/notifications/'.$notification['id'].'/').PHP_EOL.PHP_EOL;
			$body.='<p class="notify-item"><a href="'.($config['platform_url'].'/notifications/'.$notification['id'].'/').'">'.$render_preview.'</a></p>';
		}
		$body.='<p class="notify-item"><a href="'.($config['platform_url'].'/notifications/').'">View all notifications</a></p>';

		$linked_platforms_arr=$db->sql("SELECT * FROM `linked_platforms` WHERE `address`='".$address_item['id']."' AND `platform`=2 AND `status`=2");
		foreach($linked_platforms_arr as $linked_platform){
			if($linked_platform['flag2']){//flag2 - new notifications
				$platform_data=json_decode($linked_platform['data'],true);
				if(isset($platform_data['name'])){
					$username=htmlspecialchars($platform_data['name']);
				}
				$body_addon='<br><br><font style="color:#666;">You got it by active subscription at the address below.<br><br>You can unlink your email on the <a href="'.$config['platform_url'].'/link-platform/2/">configuration page</a>.<br>Address: '.$address_str.'</font>';

				new_mail($linked_platform['internal_username'],$username,$subject,$body.$body_addon);
			}
		}
	}

	//get mail from queue
	$mail_queue=$db->sql("SELECT * FROM `mail_queue` WHERE `status`='0' LIMIT 20");
	foreach($mail_queue as $mail_arr){
		$mail->addAddress($mail_arr['to'],($mail_arr['name']?$mail_arr['name']:false));
		$mail->Subject=$mail_arr['subject'];
		$mail->AltBody=strip_tags($mail_arr['text']);
		$t->open('mail.tpl','mail');
		$replace['preheader']='';
		if($mail_arr['preheader']){
			$replace['preheader']='
<!--[if !mso]><!-->
<div class="preheader" style="font-size:0px;color:transparent;opacity:0;">
'.htmlspecialchars($mail_arr['preheader']).'
</div>
<div class="preheader" style="font-size:0px;color:transparent;opacity:0;">
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;
</div>
<!--<![endif]-->';
		}
		foreach($replace as $name=>$value){
			$t->assign($name,$value,'mail');
		}
		$t->assign('body',$mail_arr['text'],'mail');
		$html_body=$t->get('mail');
		$mail->Body=$html_body;
		if($mail_arr['headers']){
			$headers=json_decode($mail_arr['headers'],true);
			if(is_array($headers)){
				foreach($headers as $header_name=>$header_value){
					$mail->addCustomHeader($header_name,$header_value);
				}
			}
		}
		//print $html_body;exit;//for important debug
		try{
			$mail->send();
			print 'Mailer Success: '.$mail_arr['id'].PHP_EOL;
			$db->sql("UPDATE `mail_queue` SET `status`=1, `status_time`='".time()."' WHERE `id`=".$mail_arr['id']);
		}
		catch(Exception $e){
			print 'Mailer Error: '.$mail->ErrorInfo.PHP_EOL;
			$mail->getSMTPInstance()->reset();
			$db->sql("UPDATE `mail_queue` SET `status`=2, `status_time`='".time()."' WHERE `id`=".$mail_arr['id']);
		}
		$mail->clearCustomHeaders();
		$mail->clearAddresses();
		$mail->clearAttachments();
	}
	//check work time
	$now_time=time();
	if($work_time <= $now_time-$start_time){
		break;
	}
	sleep($sleep_time);
}
//close lock file
flock($lock_fp,LOCK_UN);
fclose($lock_fp);
exit;