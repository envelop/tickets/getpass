<?php
use Elliptic\EC;
use kornrunner\Keccak;

ob_start();

$back_url='';
if(isset($_GET['back_url'])){
	$back_url=urldecode($_GET['back_url']);
}
if(isset($_GET['back'])){
	$back_url=urldecode($_GET['back']);
}
if(''==$back_url){
	//https://gitlab.com/envelop/tickets/getpass-land/-/issues/23
	$back_url='/profile/';
}

if(isset($_POST['type'])){
	if(!isset($auth['id'])){
		http_response_code(404);
		exit;
	}
	$type=(int)$_POST['type'];
	$type_arr=$db->sql_row("SELECT * FROM `types` WHERE `id`='".$type."' AND `signer`='1'");
	if($type_arr){
		if(1==$type_arr['id']){//Ethereum

			$ec=new EC('secp256k1');

			function pub_key_to_address($pubkey_hex) {
				return '0x'.substr(Keccak::hash(substr(hex2bin($pubkey_hex), 1), 256), 24);
			}

			function recover_from_signature($message,$signature) {
				global $ec;
				$hash=Keccak::hash("\x19Ethereum Signed Message:\n".strlen($message).$message, 256);
				$sign=[
					'r'=>substr($signature,2,64),
					's'=>substr($signature,66,64)
				];
				$recid = ord(hex2bin(substr($signature, 130, 2))) - 27;
				if($recid != ($recid & 1)){
					return false;
				}

				$pubkey = $ec->recoverPubKey($hash, $sign, $recid,'hex');
				return $pubkey->encode('hex');
			}

			$platform_str='Sign text proof for '.$config['platform_domain'];

			$result=[];
			$message=$_POST['message'];

			//$db->sql("INSERT `signer_history` (`auth`,`type`,`signature`,`nonce`,`time`,`status`) VALUES ('".$auth['id']."','".$type."','".$db->prepare($message.'==='.$_POST['signature'].'==='.PHP_EOL.var_export(strpos($message,$session_str).'==='.$session_str,true))."','0','0','4')");
			//preg match session:(id):hash
			preg_match('~session:([0-9]+):([0-9a-z]+)~is',$message,$session_arr);
			$session_id=(int)$session_arr[1];
			$session_hash=$session_arr[2];
			$check_session_arr=$db->sql_row("SELECT * FROM `auth` WHERE `id`='".$session_id."' AND md5(`hash`)='".$db->prepare($session_hash)."'");
			if($check_session_arr){
				$session_checked=true;
			}
			else{
				$session_checked=false;
			}

			//need domain proof in message
			if(false===strpos($message,$platform_str)){
				$session_checked=false;
			}

			if($session_checked){
				$signature=preg_replace('~[^0-9a-z]~iUs','',$_POST['signature']);
				$signature=strtolower($signature);

				preg_match('~nonce:([0-9]+)~is',$message,$nonce_arr);
				$nonce=(int)$nonce_arr[1];
				preg_match('~timestamp:([0-9]+)~is',$message,$timestamp_arr);
				$timestamp=(int)$timestamp_arr[1];

				$recover_pub_key=recover_from_signature($message,$signature);
				$recover_address=pub_key_to_address($recover_pub_key);

				if('0xdcc703c0e500b653ca82273b7bfad8045d85a470'==$recover_address){
					//empty recovered pub key (ledger bug)
					//https://github.com/MetaMask/metamask-extension/issues/5319
					//https://github.com/MetaMask/metamask-extension/issues/5523
					//https://github.com/omgnetwork/web-wallet/issues/51
					$recover_address=false;
				}
				if($recover_address){
					$result['result']=true;
					$address_arr=$db->sql_row("SELECT * FROM `addresses` WHERE `address`='".$db->prepare($recover_address)."'");
					$address_id=0;
					if(null==$address_arr){
						$address_status=0;
						if(in_array($recover_address,$config['admin_type_addresses'][$type])){
							$address_status=1;
						}
						$db->sql("INSERT INTO `addresses` (`type`,`address`,`time`,`status`,`mailing`,`mailing_platform`,`mailing_organizers`,`mailing_events`) VALUES ('".$type."','".$db->prepare($recover_address)."','".time()."','".$address_status."','".time()."','".time()."','".time()."','".time()."')");
						$address_id=$db->last_id();
					}
					else{
						$address_id=$address_arr['id'];
					}
					print json_encode($result);
					//clear old if exist
					$db->sql("DELETE FROM `auth_addresses` WHERE `auth`='".$auth['id']."' AND `address`='".$address_id."'");
					$db->sql("INSERT INTO `auth_addresses` (`auth`,`address`,`time`) VALUES ('".$auth['id']."','".$address_id."','".time()."')");

					//import last address notifications to new auth session from last week (7days)
					$notifications_arr=$db->sql("SELECT * FROM `notifications_queue` WHERE `address`='".$address_id."' AND `status`='1' AND `time`>'".(time()-(7*24*60*60))."' ORDER BY `id` ASC");
					foreach($notifications_arr as $notification_arr){
						if(0==$db->table_count('auth_notifications',"WHERE `auth`='".$auth['id']."' AND `notify`='".$notification_arr['id']."'")){
							$db->sql("INSERT INTO `auth_notifications` (`auth`,`notify`) VALUES ('".$auth['id']."','".$notification_arr['id']."')");
						}
					}

					//not working on nginx?
					/*
					ignore_user_abort(true);//Prevent echo, print, and flush from killing the script
					fastcgi_finish_request();//returns 200 to the user, and processing continues
					*/
					//start processing address check-in
					$db->sql("INSERT `signer_history` (`auth`,`type`,`signature`,`recovered_address`,`recovered_pubkey`,`nonce`,`time`,`status`) VALUES ('".$auth['id']."','".$type."','".$db->prepare($signature)."','".$db->prepare($recover_address)."','".$db->prepare($recover_pub_key)."','".$nonce."','".$timestamp."','1')");

					if($config['login_auto_create_organizer']){
						if(0==$db->table_count('organizer_addresses',"WHERE `address`='".$address_id."'")){
							//create organizer if not exist
							$organizer_id=0;

							$status=$config['organizer_moderation']?0:1;//0 - waiting, 1 - approved
							$db->sql("INSERT INTO `organizers` SET
								`url`='temp_".time()."',
								`title`='temp_".time()."',
								`description`='New organizer',
								`events_description`='',
								`status`='".$status."',
								`time`='".time()."',
								`update_time`='".time()."'
							");
							$organizer_id=$db->last_id();

							if(0!=$organizer_id){
								$db->sql("INSERT INTO `organizer_addresses` SET
									`organizer`='".$db->prepare($organizer_id)."',
									`address`='".$db->prepare($address_id)."',
									`status`='1'
								");

								$url_preset="org-".$organizer_id;
								$title_preset="title_org_".$organizer_id;

								$db->sql("UPDATE `organizers` SET
									`url`='".$db->prepare($url_preset)."',
									`title`='".$db->prepare($title_preset)."'
									WHERE `id`='".$organizer_id."'");

								//ignored for now, spam protection
								/*
								//send notification to all admin if need moderation
								if($config['organizer_moderation']){
									//get all administators and send them notification about new organizer
									$platform_admins=$db->sql("SELECT `id` FROM `addresses` WHERE `status`='1'");
									foreach($platform_admins as $platform_admin){
										add_notify($platform_admin['id'],0,1,'admin_new_org',json_encode(['organizer_title'=>htmlspecialchars($title_preset),'organizer_url'=>htmlspecialchars($url_preset)]));
									}
								}
								*/
							}
						}
					}
					http_response_code(200);
					exit;
				}
				else{
					$result['result']=false;
					print json_encode($result);
					ignore_user_abort(true);//Prevent echo, print, and flush from killing the script
					fastcgi_finish_request();//returns 200 to the user, and processing continues
					//start processing address check-in
					$db->sql("INSERT `signer_history` (`auth`,`type`,`signature`,`recovered_address`,`recovered_pubkey`,`nonce`,`time`,`status`) VALUES ('".$auth['id']."','".$type."','".$db->prepare($signature)."','".$db->prepare($recover_address)."','".$db->prepare($recover_pub_key)."','".$nonce."','".$timestamp."','2')");
				}
			}
			else{
				$result['result']=false;
				print json_encode($result);
				$db->sql("INSERT `signer_history` (`auth`,`type`,`signature`,`nonce`,`time`,`status`) VALUES ('".$auth['id']."','".$type."','".$db->prepare($signature)."','".$nonce."','".$timestamp."','3')");
			}
			exit;
		}
	}
	else{
		http_response_code(404);
		exit;
	}
}

$script_preset.='var auth_session=\''.$auth['id'].':'.md5($auth['hash']).'\';'.PHP_EOL;
$script_preset.='var platform_str=\'Sign text proof for '.$config['platform_domain'].'\';'.PHP_EOL;
print '<h1>Authorization on the platform</h1>';
print '<hr class="my-4">';
$types_arr=[];
$types=$db->sql("SELECT * FROM `types`");
foreach($types as $type){
	$types_arr[$type['id']]=$type;
}
foreach($auth_address_arr as $auth_address){
	print '<p class="text-gray-400">You already signed as '.$auth_address['address'];
	//print ' ('.$types_arr[$auth_address['type']]['name'].')';
	print '</p>';
}

if(false===strpos($back_url,'@')){//@ symbol is signal for organizer page (event back url redirect), this is participant, not new organizer
	print '<p>In 2022, we launched GetPass and are pleased to offer its full functionality for free: for anyone who registers as a user or event organizer between 01.09.2022 and 01.09.2023, the full functionality is available at 0 USD. Additionally, you can become a beta tester of our service and a participant in DAO Envelop at the same time. Please send your proposals to the <a href="mailto:info@envelop.is">administrator</a>.</p>';
}

//clear header from menu and user badge, user in login page
$replace['attention_badge']='';
$replace['notify_badge']='';
$replace['user_badge']='';
$replace['menu']='';
$replace['menu_button_wrapper_addon']=' hidden';

print '<p>Sign text proof with your wallet.</p>';
foreach($types_arr as $type_id=>$type_arr){
	if(1==$type_arr['signer']){
		print '<h2>Sign by '.$type_arr['name'].'</h2>';
		print '<div class="signer signer_'.$type_id.'">';
		print '<div class="signer-wrapper">';
		print '
			<div class="connect-buttons"></div>
			<div class="connect-status"></div>
			<div class="sign-status"></div>';
		print '</div>';
		print '</div>';
		$script_preset.='var back_url="'.htmlspecialchars($back_url).'";';
		//print '<script type="text/javascript">var back_url="'.htmlspecialchars($back_url).'";</script>';
		print '<script src="/js/walletconnect-sdk.min.js"></script>';
		print '<script src="/js/signer_'.$type_id.'.js?'.time().'"></script>';

	}
}

$content=ob_get_contents();
ob_end_clean();