<?php
ob_start();
if(!isset($event_arr)){
	http_response_code(404);
	exit;
}
$customize=[];
try{
	$customize=json_decode($event_arr['customize'],true);
}
catch(Exception $e){
	$customize=[];
}

//reset to default customize
if(!isset($customize['avatar_style'])){
	$customize['avatar_style']=0;
}
if(!isset($customize['link_platform_notice'])){
	$customize['link_platform_notice']=0;
}
//check event theme settings
if(!isset($customize['default_theme'])){
	$customize['default_theme']=0;//light
}
else{
	//check if theme exists
	if(!isset($config['platform_themes'][$customize['default_theme']])){
		$customize['default_theme']=0;//light
	}
}
if(!isset($customize['theme_changer'])){
	$customize['theme_changer']=true;
}
else{
	$customize['theme_changer']=($customize['theme_changer']?true:false);
}
if(!isset($customize['template_parts'])){
	$customize['template_parts']=$event_template_parts;
}
if(!isset($customize['template_parts_disabled'])){
	$customize['template_parts_disabled']=[];
}

$event_id=$event_arr['id'];
$script_preset.='var event_id='.$event_id.';';
$event_theme=$config['platform_themes'][$customize['default_theme']];

//set template theme
$replace['theme']=$event_theme;
//check user theme settings, rewrite template theme
if(isset($_COOKIE['theme_'.$event_id])){
	if(in_array($_COOKIE['theme_'.$event_id],$config['platform_themes'])){
		$replace['theme']=$_COOKIE['theme_'.$event_id];
	}
}
//script preset for changer and default theme
$script_preset.='var event_theme_changer='.($customize['theme_changer']?'true':'false').';';
$script_preset.='var event_default_theme=\''.($event_theme).'\';';

$allow_event_manage=false;//event admin, manager or organizer administration or platform admin
$allow_event_manage_address_id=0;//address id
if($auth['addresses']){
	$find=false;
	foreach($auth_address_arr as $auth_address_item){
		$search_event_user=$db->sql_row("SELECT * FROM `event_users` WHERE `event`='".$event_id."' AND `address`='".$auth_address_item['id']."'");
		if(null!=$search_event_user){
			if($search_event_user['admin']){
				$allow_event_manage=true;
				$allow_event_manage_address_id=$search_event_user['address'];
				$find=false;//if previous user not manager, but current is - need update user badge
			}
			if($search_event_user['manager']){
				$allow_event_manage=true;
				$allow_event_manage_address_id=$search_event_user['address'];
				$find=false;//if previous user not manager, but current is - need update user badge
			}
			if(!$find){
				$event_user=$search_event_user;

				$replace['notify_badge']=PHP_EOL.'<a class="notify-button'.(0!=$auth['notifications']?' new':'').'" href="/@'.$organizer_url.'/'.$event_url.'/notifications/" title="Notifications">'.$ltmp['icons']['notifications-bell'].'<span class="explainer">Notifications</span></a>';

				$find=true;//fill user badge only once
			}
			if($allow_event_manage){
				break;//have permission to manage event
			}
		}
	}
}

$replace['event_logo_url']='';
if('light'==$replace['theme']){
	$replace['event_logo_url']='/getpass-new-logo-blue-black.svg';
}
if('dark'==$replace['theme']){
	$replace['event_logo_url']='/getpass-new-logo-teal-white.svg';
}

$replace['original_attention_badge']=$replace['attention_badge'];
$replace['original_notify_badge']=$replace['notify_badge'];
$replace['original_user_badge']=$replace['user_badge'];

$replace['attention_badge']=str_replace('href="/link-platform/"','href="/link-platform/?back=/@'.$organizer_url.'/'.$event_url.'/"',$replace['attention_badge']);
$replace['notify_badge']=str_replace('href="/notifications/"','href="/@'.$organizer_url.'/'.$event_url.'/notifications/"',$replace['notify_badge']);
$replace['user_badge']=event_user_badge($event_id,$event_url,$organizer_url);

if(isset($auth['organizers_list'][$organizer_arr['id']])){
	$allow_event_manage=true;
	$allow_event_manage_address_id=$auth['organizers_list_address_id'][$organizer_arr['id']];
	$event_user['admin']=1;//allow to manage event as admin
}
if(1==$auth['status']){
	$allow_event_manage=true;//platform admin can manage as organizer administration
	$allow_event_manage_address_id=$auth['status_address_id'];
	$event_user['admin']=1;//allow to manage event as admin
}

$allow_event_view=false;
if(1==$event_arr['status']){//active
	$allow_event_view=true;
}
if(2==$event_arr['status']){//archive
	$allow_event_view=true;
}

//check moderation
if(0==$event_arr['moderation']){//waiting moderation
	$allow_event_view=false;
}
if(2==$event_arr['moderation']){//rejected
	$allow_event_view=false;
}

if($allow_event_manage){
	$allow_event_view=true;//even if moderation or status not allow to view - allow to managers
}
if($allow_event_view){
	if($allow_event_manage){
		if(1!=$event_arr['moderation']){//not approved
			$hide_moderation_notice=false;
			if('manage'==$path_array[3]){
				if('moderation'==$path_array[4]){
					$hide_moderation_notice=true;
				}
			}
			if(!$hide_moderation_notice){
				//show notice only if event is active and not moderated yet
				if(0==$event_arr['moderation']){//waiting moderation
					if(1==$event_arr['status']){//status active (on moderation now)
						print '
						<div class="attention-box" role="alert">
							<p class="font-bold">Notice</p>
							<p>The event is not moderated yet and is not available for public viewing.</p>
							<p>Send the finished event to moderation for publication.</p>
							<a href="/@'.$organizer_url.'/'.$event_url.'/manage/moderation/" class="action-btn default-button">Request moderation</a>
						</div>';
					}
				}
				if(2==$event_arr['moderation']){//rejected
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Notice</p>
						<p>This event is rejected by the administration and is not available for public viewing.</p>';
					if(''!=$event_arr['reject_comment']){
						print '<p><b>Comment from moderation:</b></p>';
						print '<p>'.nl2br($event_arr['reject_comment']).'</p>';
					}
					if($event_arr['reject_counter']>=$config['event_moderation_max_rejects']){
						print '<p><em>You can not request moderation for this event again.</em></p>';
					}
					else{
						print '<a href="/@'.$organizer_url.'/'.$event_url.'/manage/moderation/" class="action-btn default-button">Request moderation</a>';
					}
					print '</div>';
				}
			}
		}
	}
	if(0==$event_arr['status']){
		if($allow_event_manage){
			print '
			<div class="attention-box" role="alert">
				<p class="font-bold">Notice</p>
				<p>The event is in the planned status and is not available for public viewing.</p>
				<p>You can customize and edit information and fill out the content for the selected event in draft mode.</p>
			</div>';
		}
	}
	if(2==$event_arr['status']){
		print '
		<div class="attention-box" role="alert">
			<p class="font-bold">Notice</p>
			<p>This event has been moved to the archives.</p>
			<p>You can subscribe to <a href="/@'.$organizer_url.'/">the organizer</a> to get updates on new events.</p>
		</div>';
	}

	$replace['title']=$event_arr['title'].' | '.$replace['title'];
	$replace['description']=htmlspecialchars('Event "'.$event_arr['title'].'" by '.$organizer_arr['title'].' on GetPass.is');
	if($event_arr['short_description']){
		$replace['description']=htmlspecialchars(strip_tags($event_arr['short_description']));
	}
	$replace['event_url']='/@'.$organizer_url.'/'.$event_url.'/';//main event page

	$replace['head_addon'].='<meta property="og:title" content="'.htmlspecialchars($event_arr['title']).'">';
	$replace['head_addon'].='<meta property="og:description" content="'.htmlspecialchars($replace['description']).'">';
	if(''!=$event_arr['cover_url']){
		$replace['head_addon'].='<meta property="og:image" content="'.$config['platform_url'].$event_arr['cover_url'].'">';
		$replace['head_addon'].='<meta property="twitter:card" content="summary_large_image">';
	}
	$replace['head_addon'].='<meta property="og:url" content="'.$config['platform_url'].$replace['event_url'].'">';
	$replace['head_addon'].='<meta property="og:type" content="article">';
	$replace['head_addon'].='<meta property="og:site_name" content="'.$replace['title'].'">';
	$replace['head_addon'].='<meta property="article:published_time" content="'.date('c',$event_arr['time']).'">';
	$replace['head_addon'].='<meta property="article:modified_time" content="'.date('c',$event_arr['time']).'">';
	$replace['head_addon'].='<meta property="article:section" content="Event">';
	$replace['head_addon'].='<meta property="og:locale" content="en_US">';
	$replace['head_addon'].='<meta property="og:locale:alternate" content="ru_RU">';



	//https://gitlab.com/envelop/tickets/getpass/-/issues/33
	$tab_class='';//default3-button
	$replace['menu']='';//clear platform menu
	$replace['menu'].='<li><a href="/@'.$organizer_url.'/'.$event_url.'/" class="'.$tab_class.(''==$path_array[3]?' selected':'').'">Event</a></li>';
	$content_count=0;
	$content_count=$db->table_count('event_content','WHERE `event`='.$event_id.' AND status=1');
	if($content_count){
		$replace['menu'].='<li><a href="/@'.$organizer_url.'/'.$event_url.'/content/" class="'.$tab_class.('content'==$path_array[3]?' selected':'').'">Blog</a></li>';
	}

	$sessions_count=$db->table_count('event_sessions',"WHERE `event`='".$event_id."' AND `status`!=2");
	if($sessions_count){
		$replace['menu'].='<li><a href="/@'.$organizer_url.'/'.$event_url.'/agenda/" class="'.$tab_class.(('agenda'==$path_array[3]||'sessions'==$path_array[3])?' selected':'').'">Agenda</a></li>';
	}

	$speakers_count=$db->table_count('event_speakers',"WHERE `event`='".$event_id."' AND `status`!=2");
	if($speakers_count){
		$replace['menu'].='<li><a href="/@'.$organizer_url.'/'.$event_url.'/speakers/" class="'.$tab_class.('speakers'==$path_array[3]?' selected':'').'">Speakers</a></li>';
	}

	$partners_count=$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `status`!=2");
	if($partners_count){
		$replace['menu'].='<li><a href="/@'.$organizer_url.'/'.$event_url.'/partners/" class="'.$tab_class.('partners'==$path_array[3]?' selected':'').'">Partners</a></li>';
	}

	if($allow_event_manage){
		if(1==$event_user['admin']||1==$event_user['manager']){
			$replace['menu'].='<li><a href="/@'.$organizer_url.'/'.$event_url.'/manage/" class="'.$tab_class.('manage'==$path_array[3]?' selected':'').'">Manage</a></li>';
		}
	}

	if($event_arr['logo_url']){
		$replace['event_logo_url']=htmlspecialchars($event_arr['logo_url']);
		$replace['event_logo_addon']='';
		$replace['event_logo_class'].=' custom-logo';
		if(isset($customize['logo_style'])){
			$replace['event_logo_class'].=' '.$logo_style[$customize['logo_style']]['class_addon'];
		}
		if(isset($customize['logo_border'])){
			$replace['event_logo_class'].=' '.$logo_border[$customize['logo_border']]['class_addon'];
		}
	}
	$events_module=$path_array[3];
	if('load_content'==$events_module){
		$search_session_id=(int)$path_array[4];
		$session_item=$db->sql_row("SELECT * FROM `event_sessions` WHERE `event`='".$event_id."' AND `id`='".$search_session_id."' AND `status`!=2");
		if(null!==$session_item){
			ob_end_clean();
			ob_end_clean();
			$content='';
			ob_start();
			$is_allowed=false;
			if($allow_event_manage){
				$is_allowed=true;
			}
			if(isset($event_user['level'])){
				if($session_item['level']<=$event_user['level']){
					$is_allowed=true;
				}
			}
			else{
				if(0==$session_item['level']){//public for guests
					$is_allowed=true;
				}
			}
			if($is_allowed){
				$session_content='';
				$clear_content=clear_html_tags($session_item['content']);
				$clear_embed_content=clear_html_tags($session_item['embed_content']);
				$clear_embed_content='<figure class="media">'.$clear_embed_content.'</figure>';
				if($clear_content){
					$session_content.=$clear_content;
				}
				if($clear_embed_content){
					$session_content.=$clear_embed_content;
				}
				if(''!=$session_content){
					print $session_content;
				}
				else{
					print '<p>Session content is empty. Please, contact event organizer.</p>';
				}
			}
			else{
				print '<p>Access denied.</p>';
				if($event_arr['tickets_description']){
					$clear_tickets_description=clear_html_tags($event_arr['tickets_description']);
					print $clear_tickets_description;
				}
			}
		}
		exit;
	}
	else
	if($events_module){
		$events_module_file=$root_dir.'/modules/events/'.$events_module.'.php';
		if('manage'==$events_module){
			$manage_content='';
			include($root_dir.'/modules/manage/event.php');
			print $manage_content;
		}
		else{
			if(file_exists($events_module_file)){
				$event_module_content='';
				include($events_module_file);
				print $event_module_content;
				$script_preset.='var auto_update_user_badge=true;var auto_update_user_badge_lazy=true;';//load user badge with event id for auto check-in but lazy (not index event page)
			}
			else{
				header('HTTP/1.0 404 Not Found');
				print '<h1>404 Not Found</h1>';
				print '<p>The page that you have requested could not be found.</p>';
			}
		}
	}
	else{
		$script_preset.='var auto_update_user_badge=true;var auto_update_user_badge_lazy=false;';//load user badge with event id for auto check-in

		function build_event_template($part='main'){
			global $event_arr,$replace,$current_session,$db,$organizer_arr,$organizer_url,$event_id,$event_url,$allow_event_manage,$partners_count,$speakers_count,$build_event_template_counter,
			$config,
			$ltmp,
			$customize,
			$cover_background_color_overlay_options,
			$cover_text_styles_options,
			$cover_title_stroke_width_options,
			$cover_descr_text_opacity_options;
			$result='';
			if('main'==$part){
				if($event_arr['cover_url']){
					$cover_style_addon=isset($customize['cover_background_color'])?$cover_background_color_overlay_options[$customize['cover_background_color']]['style_addon'].',':'';

					$cover_title_text='';
					if(isset($customize['cover_title_text'])){
						$cover_title_text=$customize['cover_title_text'];
						$cover_title_text=str_replace("\n",'<br>',$cover_title_text);
					}
					$cover_title_style_addon='';

					$cover_title_styles=0;
					if(isset($customize['cover_title_styles'])){
						$cover_title_styles=$customize['cover_title_styles'];
					}
					$cover_title_style_addon.=$cover_text_styles_options[$cover_title_styles]['style_addon'];

					$cover_title_stroke_width=0;
					if(isset($customize['cover_title_stroke_width'])){
						$cover_title_stroke_width=$customize['cover_title_stroke_width'];
					}
					$cover_title_stroke_width_style=$cover_title_stroke_width_options[$cover_title_stroke_width]['style_addon'];
					if(1==$cover_title_styles){//black text with white stroke preset
						$cover_title_stroke_width_style=str_replace('rgb(0, 0, 0)','rgb(255, 255, 255)',$cover_title_stroke_width_style);
					}
					$cover_title_style_addon.=$cover_title_stroke_width_style;

					$cover_descr_text='';
					if(isset($customize['cover_descr_text'])){
						$cover_descr_text=$customize['cover_descr_text'];
						$cover_descr_text=str_replace("\n",'<br>',$cover_descr_text);
					}
					$cover_descr_style_addon='';

					$cover_descr_styles=0;
					if(isset($customize['cover_descr_styles'])){
						$cover_descr_styles=$customize['cover_descr_styles'];
					}
					$cover_descr_style_addon.=$cover_text_styles_options[$cover_descr_styles]['style_addon'];

					$cover_descr_text_opacity=0;
					if(isset($customize['cover_descr_text_opacity'])){
						$cover_descr_text_opacity=$customize['cover_descr_text_opacity'];
					}
					$cover_descr_style_addon.=$cover_descr_text_opacity_options[$cover_descr_text_opacity]['style_addon'];

					$generated_text='';
					if($cover_title_text){
						$generated_text.='<div class="cover-title-text" style="'.$cover_title_style_addon.'">'.$cover_title_text.'</div>';
					}
					if($cover_descr_text){
						$generated_text.='<div class="cover-descr-text" style="'.$cover_descr_style_addon.'">'.$cover_descr_text.'</div>';
					}
					if($generated_text){
						$generated_text='<div class="cover-text">'.$generated_text.'</div>';
					}
					//if first part make it by template var (prevent exploding for content part)
					if(0==$build_event_template_counter){
						$replace['event_cover']='<div class="event-cover" style="background-image:'.$cover_style_addon.'url(\''.htmlspecialchars($event_arr['cover_url']).'\')">'.$generated_text.'</div>';
					}
					else{
						$replace['main_container_addon']=' noshadow';
						$result.='</div></div>';
						$result.='<div class="event-cover" style="background-image:'.$cover_style_addon.'url(\''.htmlspecialchars($event_arr['cover_url']).'\')">'.$generated_text.'</div>';
						$result.='<div class="main-container noshadow"><div class="content">';
					}
				}

				$event_today=false;
				if($event_arr['time']){
					$event_date=date('Y-m-d',$event_arr['time']);
					$today_date=date('Y-m-d');
					if($event_date==$today_date){
						$event_today=true;
					}
				}

				if($event_today){
					//get actual and next session
					$next_session=false;
					$current_session=$db->sql_row('SELECT * FROM `event_sessions` WHERE `event`='.$event_id.' AND `time`<'.time().' AND `endtime`>'.time().' ORDER BY `time` DESC LIMIT 1');
					if(null==$current_session){
						$next_session=true;
						$current_session=$db->sql_row('SELECT * FROM `event_sessions` WHERE `event`='.$event_id.' AND `time`>'.time().' ORDER BY `time` ASC LIMIT 1');
					}
					if(null!==$current_session){
						if($current_session['time']>time()){
							$result.='<div class="content-wrapper" data-session="'.$current_session['id'].'" data-autoplay="yes">';
							$result.='<p class="font-bold">Next session: <a href="/@'.$organizer_url.'/'.$event_url.'/sessions/'.$current_session['id'].'/">'.htmlspecialchars($current_session['caption']).'</a> <span class="text-sm text-gray-500">('.$current_session['duration'].' Min)</p>';
							$result.='<p>'.htmlspecialchars($current_session['description']).'</p>';
							$result.='<div class="countdown" data-time="'.$current_session['time'].'">';
								$result.='<div class="circle">';
									$result.='<div class="countdown-value days"></div>';
									$result.='<div class="countdown-label">Days</div>';
								$result.='</div>';
								$result.='<div class="circle">';
									$result.='<div class="countdown-value hours"></div>';
									$result.='<div class="countdown-label">Hours</div>';
								$result.='</div>';
								$result.='<div class="circle">';
									$result.='<div class="countdown-value minutes"></div>';
									$result.='<div class="countdown-label">Minutes</div>';
								$result.='</div>';
								$result.='<div class="circle">';
									$result.='<div class="countdown-value seconds"></div>';
									$result.='<div class="countdown-label">Seconds</div>';
								$result.='</div>';
							$result.='</div>';
							$result.='</div>';
						}
						else{
							$result.='<div class="content-wrapper waiting" data-session="'.$current_session['id'].'" data-autoplay="yes"><p>Loading...</p></div>';
						}
					}
				}
				//removed h1 because org can sort parts and h1 is not always first
				//now h1 is available in wysiwyg editor
				$result.='<div class="content-card single">';
				$result.='<div class="content-wrapper">';
				$clear_description=clear_html_tags($event_arr['description']);
				$result.=$clear_description;
				$result.='</div>';
				$result.='</div>';
			}
			if('speakers'==$part){
				if($event_arr['speakers_cover_url']){
					$cover_style_addon=isset($customize['speakers_cover_background_color'])?$cover_background_color_overlay_options[$customize['speakers_cover_background_color']]['style_addon'].',':'';

					$cover_title_text='';
					if(isset($customize['speakers_cover_title_text'])){
						$cover_title_text=$customize['speakers_cover_title_text'];
						$cover_title_text=str_replace("\n",'<br>',$cover_title_text);
					}
					$cover_title_style_addon='';

					$cover_title_styles=0;
					if(isset($customize['speakers_cover_title_styles'])){
						$cover_title_styles=$customize['speakers_cover_title_styles'];
					}
					$cover_title_style_addon.=$cover_text_styles_options[$cover_title_styles]['style_addon'];

					$cover_title_stroke_width=0;
					if(isset($customize['speakers_cover_title_stroke_width'])){
						$cover_title_stroke_width=$customize['speakers_cover_title_stroke_width'];
					}
					$cover_title_stroke_width_style=$cover_title_stroke_width_options[$cover_title_stroke_width]['style_addon'];
					if(1==$cover_title_styles){//black text with white stroke preset
						$cover_title_stroke_width_style=str_replace('rgb(0, 0, 0)','rgb(255, 255, 255)',$cover_title_stroke_width_style);
					}
					$cover_title_style_addon.=$cover_title_stroke_width_style;

					$cover_descr_text='';
					if(isset($customize['speakers_cover_descr_text'])){
						$cover_descr_text=$customize['speakers_cover_descr_text'];
						$cover_descr_text=str_replace("\n",'<br>',$cover_descr_text);
					}
					$cover_descr_style_addon='';

					$cover_descr_styles=0;
					if(isset($customize['speakers_cover_descr_styles'])){
						$cover_descr_styles=$customize['speakers_cover_descr_styles'];
					}
					$cover_descr_style_addon.=$cover_text_styles_options[$cover_descr_styles]['style_addon'];

					$cover_descr_text_opacity=0;
					if(isset($customize['speakers_cover_descr_text_opacity'])){
						$cover_descr_text_opacity=$customize['speakers_cover_descr_text_opacity'];
					}
					$cover_descr_style_addon.=$cover_descr_text_opacity_options[$cover_descr_text_opacity]['style_addon'];

					$generated_text='';
					if($cover_title_text){
						$generated_text.='<div class="cover-title-text" style="'.$cover_title_style_addon.'">'.$cover_title_text.'</div>';
					}
					if($cover_descr_text){
						$generated_text.='<div class="cover-descr-text" style="'.$cover_descr_style_addon.'">'.$cover_descr_text.'</div>';
					}
					if($generated_text){
						$generated_text='<div class="cover-text">'.$generated_text.'</div>';
					}
					//if first part make it by template var (prevent exploding for content part)
					if(0==$build_event_template_counter){
						$replace['event_cover']='<div class="event-cover" style="background-image:'.$cover_style_addon.'url(\''.htmlspecialchars($event_arr['speakers_cover_url']).'\')">'.$generated_text.'</div>';
					}
					else{
						$replace['main_container_addon']=' noshadow';
						$result.='</div></div>';
						$result.='<div class="event-cover" style="background-image:'.$cover_style_addon.'url(\''.htmlspecialchars($event_arr['speakers_cover_url']).'\')">'.$generated_text.'</div>';
						$result.='<div class="main-container noshadow"><div class="content">';
					}
				}

				if($event_arr['speakers_description']){
					$result.='<div class="content-card single">';
					$result.='<div class="content-wrapper">';
					$clear_speakers_description=clear_html_tags($event_arr['speakers_description']);
					$result.=$clear_speakers_description;
					$result.='</div>';
					$result.='</div>';
				}

				if($speakers_count){
					$starred_speakers_count=$db->table_count('event_speakers',"WHERE `status`='1' AND `event`='".$event_arr['id']."'");
					if($starred_speakers_count){
						$result.='<div class="speakers-list center">';
						$sql_addon_str=" AND `event`='".$event_id."'";
						$speakers=$db->sql("SELECT * FROM `event_speakers` WHERE `status`=1".$sql_addon_str." ORDER BY `status` DESC, `sort` DESC, `public_name` ASC");
						while($speaker=$db->row($speakers)){
							$public_name_url=ru2lat($speaker['public_name']);
							$public_name_url=str_replace(' ','-',$public_name_url);
							$public_name_url=preg_replace('~[^a-z0-9\-_]+~iUs','',$public_name_url);
							$public_name_url=preg_replace('~\-+~iUs','-',$public_name_url);
							$public_name_url=preg_replace('~\-+$~iUs','',$public_name_url);
							$public_name_url=preg_replace('~^-+~iUs','',$public_name_url);
							$public_name_url=strtolower($public_name_url);

							$speaker_starred=true;

							$result.='<a class="speaker" href="/@'.$organizer_url.'/'.$event_url.'/speakers/'.$speaker['id'].'-'.$public_name_url.'/">';
							$result.='<div class="speaker-photo'.(isset($customize['avatar_style'])?' '.$avatar_style[$customize['avatar_style']]['class_addon']:'').'"><img src="'.$speaker['small_photo_url'].'" alt="speaker"></div>';
							$result.='<div class="speaker-public-name">'.htmlspecialchars($speaker['public_name']);
							if($speaker_starred){
								$result.='<span class="speaker-starred" title="Starred speaker">'.$ltmp['icons']['starred'].'</span>';
							}
							$result.='</div>';
							$result.='<div class="speaker-nick-name">'.htmlspecialchars($speaker['nick_name']).'</div>';
							$result.='<div class="speaker-title">'.htmlspecialchars($speaker['title']).'</div>';
							$result.='<div class="speaker-company">'.htmlspecialchars($speaker['company']).'</div>';
							$result.='</a>';
						}
						$result.='</div>';
					}
				}
			}
			if('location'==$part){
				if($event_arr['location_cover_url']){
					$cover_style_addon=isset($customize['location_cover_background_color'])?$cover_background_color_overlay_options[$customize['location_cover_background_color']]['style_addon'].',':'';

					$cover_title_text='';
					if(isset($customize['location_cover_title_text'])){
						$cover_title_text=$customize['location_cover_title_text'];
						$cover_title_text=str_replace("\n",'<br>',$cover_title_text);
					}
					$cover_title_style_addon='';

					$cover_title_styles=0;
					if(isset($customize['location_cover_title_styles'])){
						$cover_title_styles=$customize['location_cover_title_styles'];
					}
					$cover_title_style_addon.=$cover_text_styles_options[$cover_title_styles]['style_addon'];

					$cover_title_stroke_width=0;
					if(isset($customize['location_cover_title_stroke_width'])){
						$cover_title_stroke_width=$customize['location_cover_title_stroke_width'];
					}
					$cover_title_stroke_width_style=$cover_title_stroke_width_options[$cover_title_stroke_width]['style_addon'];
					if(1==$cover_title_styles){//black text with white stroke preset
						$cover_title_stroke_width_style=str_replace('rgb(0, 0, 0)','rgb(255, 255, 255)',$cover_title_stroke_width_style);
					}
					$cover_title_style_addon.=$cover_title_stroke_width_style;

					$cover_descr_text='';
					if(isset($customize['location_cover_descr_text'])){
						$cover_descr_text=$customize['location_cover_descr_text'];
						$cover_descr_text=str_replace("\n",'<br>',$cover_descr_text);
					}
					$cover_descr_style_addon='';

					$cover_descr_styles=0;
					if(isset($customize['location_cover_descr_styles'])){
						$cover_descr_styles=$customize['location_cover_descr_styles'];
					}
					$cover_descr_style_addon.=$cover_text_styles_options[$cover_descr_styles]['style_addon'];

					$cover_descr_text_opacity=0;
					if(isset($customize['location_cover_descr_text_opacity'])){
						$cover_descr_text_opacity=$customize['location_cover_descr_text_opacity'];
					}
					$cover_descr_style_addon.=$cover_descr_text_opacity_options[$cover_descr_text_opacity]['style_addon'];

					$generated_text='';
					if($cover_title_text){
						$generated_text.='<div class="cover-title-text" style="'.$cover_title_style_addon.'">'.$cover_title_text.'</div>';
					}
					if($cover_descr_text){
						$generated_text.='<div class="cover-descr-text" style="'.$cover_descr_style_addon.'">'.$cover_descr_text.'</div>';
					}
					if($generated_text){
						$generated_text='<div class="cover-text">'.$generated_text.'</div>';
					}
					//if first part make it by template var (prevent exploding for content part)
					if(0==$build_event_template_counter){
						$replace['event_cover']='<div class="event-cover" style="background-image:'.$cover_style_addon.'url(\''.htmlspecialchars($event_arr['location_cover_url']).'\')">'.$generated_text.'</div>';
					}
					else{
						$replace['main_container_addon']=' noshadow';
						$result.='</div></div>';
						$result.='<div class="event-cover" style="background-image:'.$cover_style_addon.'url(\''.htmlspecialchars($event_arr['location_cover_url']).'\')">'.$generated_text.'</div>';
						$result.='<div class="main-container noshadow"><div class="content">';
					}
				}
				if($event_arr['location_description']){
					$result.='<div class="content-card single">';
					$result.='<div class="content-wrapper">';
					$clear_location_description=clear_html_tags($event_arr['location_description']);
					$result.=$clear_location_description;
					$result.='</div>';
					$result.='</div>';
				}
			}
			if('tickets'==$part){
				if($event_arr['tickets_description']){
					$result.='<div class="content-card single">';
					$result.='<div class="content-wrapper">';
					$clear_tickets_description=clear_html_tags($event_arr['tickets_description']);
					$result.=$clear_tickets_description;
					$result.='</div>';
					$result.='</div>';
				}
			}
			if('timer'==$part){
				if($event_arr['time']){
					if(time()<$event_arr['time']){
						$result.='<div class="countdown" data-time="'.$event_arr['time'].'">';
						$result.='<div class="circle">';
						$result.='<div class="countdown-value days"></div>';
						$result.='<div class="countdown-label">Days</div>';
						$result.='</div>';
						$result.='<div class="circle">';
						$result.='<div class="countdown-value hours"></div>';
						$result.='<div class="countdown-label">Hours</div>';
						$result.='</div>';
						$result.='<div class="circle">';
						$result.='<div class="countdown-value minutes"></div>';
						$result.='<div class="countdown-label">Minutes</div>';
						$result.='</div>';
						$result.='<div class="circle">';
						$result.='<div class="countdown-value seconds"></div>';
						$result.='<div class="countdown-label">Seconds</div>';
						$result.='</div>';
						$result.='</div>';


						$replace['head_addon'].='<link rel="stylesheet" href="/css/atcb.css">';
						$replace['head_addon'].='<script src="/js/atcb.js" async defer></script>';
						$string_description='';
						$string_description.='[url]'.$config['platform_url'].'/@'.$organizer_url.'/'.$event_url.'/[/url]\n';
						$string_description.=htmlspecialchars($event_arr['short_description']);
						$string_description=str_replace(array("\r\n","\n","\r"),'\n',$string_description);
						$string_description=str_replace('\n','<br>',$string_description);

						$endtime=$event_arr['time']+3600*8;//by default
						$last_session=$db->sql_row("SELECT * FROM `event_sessions` WHERE `event`='".$event_arr['id']."' ORDER BY `time` DESC LIMIT 1");
						if(null!==$last_session){
							$endtime=$last_session['endtime'];
						}
						$result.='<div class="countdown-buttons-wrapper"><a class="action atcb-action-button">'.$ltmp['icons']['calendar-add'].' Add to calendar</a></div>
						<script type="application/javascript">
						const atcb_config={
							"name":"'.htmlspecialchars($event_arr['title']).'",
							"description":"'.$string_description.'",
							"startDate":"'.date('Y-m-d',$event_arr['time']).'",
							"endDate":"'.date('Y-m-d',$endtime).'",
							"startTime":"'.date('H:i',$event_arr['time']).'",
							"endTime":"'.date('H:i',$endtime).'",
							"location":"'.htmlspecialchars($event_arr['short_location_description']).'",
							"options":[
								"Google",
								"Apple",
								"iCal",
								"Microsoft365",
								"MicrosoftTeams",
								"Outlook.com",
								"Yahoo"
							],
							"timeZone":"Etc/UTC",
							"iCalFileName":"Reminder-Event",
							"trigger":"click",
							"inline":true,"listStyle":"modal",
						};
						let atcb_button=$(".atcb-action-button");
						atcb_button.on("click",function(){
							atcb_action(atcb_config,atcb_button[0]);
						});
						</script>
						';
					}
				}
			}
			if('partners'==$part){
				if($partners_count){
					if($event_arr['partners_cover_url']){
						$cover_style_addon=isset($customize['partners_cover_background_color'])?$cover_background_color_overlay_options[$customize['partners_cover_background_color']]['style_addon'].',':'';

						$cover_title_text='';
						if(isset($customize['partners_cover_title_text'])){
							$cover_title_text=$customize['partners_cover_title_text'];
							$cover_title_text=str_replace("\n",'<br>',$cover_title_text);
						}
						$cover_title_style_addon='';

						$cover_title_styles=0;
						if(isset($customize['partners_cover_title_styles'])){
							$cover_title_styles=$customize['partners_cover_title_styles'];
						}
						$cover_title_style_addon.=$cover_text_styles_options[$cover_title_styles]['style_addon'];

						$cover_title_stroke_width=0;
						if(isset($customize['partners_cover_title_stroke_width'])){
							$cover_title_stroke_width=$customize['partners_cover_title_stroke_width'];
						}
						$cover_title_stroke_width_style=$cover_title_stroke_width_options[$cover_title_stroke_width]['style_addon'];
						if(1==$cover_title_styles){//black text with white stroke preset
							$cover_title_stroke_width_style=str_replace('rgb(0, 0, 0)','rgb(255, 255, 255)',$cover_title_stroke_width_style);
						}
						$cover_title_style_addon.=$cover_title_stroke_width_style;

						$cover_descr_text='';
						if(isset($customize['partners_cover_descr_text'])){
							$cover_descr_text=$customize['partners_cover_descr_text'];
							$cover_descr_text=str_replace("\n",'<br>',$cover_descr_text);
						}
						$cover_descr_style_addon='';

						$cover_descr_styles=0;
						if(isset($customize['partners_cover_descr_styles'])){
							$cover_descr_styles=$customize['partners_cover_descr_styles'];
						}
						$cover_descr_style_addon.=$cover_text_styles_options[$cover_descr_styles]['style_addon'];

						$cover_descr_text_opacity=0;
						if(isset($customize['partners_cover_descr_text_opacity'])){
							$cover_descr_text_opacity=$customize['partners_cover_descr_text_opacity'];
						}
						$cover_descr_style_addon.=$cover_descr_text_opacity_options[$cover_descr_text_opacity]['style_addon'];

						$generated_text='';
						if($cover_title_text){
							$generated_text.='<div class="cover-title-text" style="'.$cover_title_style_addon.'">'.$cover_title_text.'</div>';
						}
						if($cover_descr_text){
							$generated_text.='<div class="cover-descr-text" style="'.$cover_descr_style_addon.'">'.$cover_descr_text.'</div>';
						}
						if($generated_text){
							$generated_text='<div class="cover-text">'.$generated_text.'</div>';
						}
						//if first part make it by template var (prevent exploding for content part)
						if(0==$build_event_template_counter){
							$replace['event_cover']='<div class="event-cover" style="background-image:'.$cover_style_addon.'url(\''.htmlspecialchars($event_arr['partners_cover_url']).'\')">'.$generated_text.'</div>';
						}
						else{
							$replace['main_container_addon']=' noshadow';
							$result.='</div></div>';
							$result.='<div class="event-cover" style="background-image:'.$cover_style_addon.'url(\''.htmlspecialchars($event_arr['partners_cover_url']).'\')">'.$generated_text.'</div>';
							$result.='<div class="main-container noshadow"><div class="content">';
						}
					}
					$partners_cats=$db->sql("SELECT * FROM `event_partners_cat` WHERE `event`='".$event_id."' ORDER BY `sort` ASC");
					foreach($partners_cats as $partners_cat){
						$partners_cat_count=$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `status`!=2 AND `cat`='".$partners_cat['id']."'");
						if($partners_cat_count){
							$result.='<h2 class="partners-category">'.htmlspecialchars($partners_cat['caption']).'</h2>';
							$result.='<div class="partners-list">';
							$partners=$db->sql("SELECT * FROM `event_partners` WHERE `event`='".$event_id."' AND `cat`='".$partners_cat['id']."' AND `status`!=2 ORDER BY `sort` ASC");
							foreach($partners as $partner){
								$result.='<div class="partner-item">';
								$external=true;
								if($partner['url']){
									//$external=false;
									$partner['new_url']='/@'.$organizer_url.'/'.$event_url.'/partners/'.$partner['url'].'/';
								}
								else{
									$partner['new_url']=$partner['link'];
								}
								$result.='<a href="'.htmlspecialchars($partner['new_url']).'"'.($external?' target="_blank"':'').'>';
								if($partner['small_logo_url']){
									$result.='<img src="'.htmlspecialchars($partner['small_logo_url']).'" alt="'.htmlspecialchars($partner['name']).'">';
								}
								else{
									$result.=htmlspecialchars($partner['name']);
								}
								if($partner['caption']){
									$result.='<div class="partner-caption">'.htmlspecialchars($partner['caption']).'</div>';
								}
								$result.='</a>';
								$result.='</div>';
							}
							$result.='</div>';
						}
					}

					//check if any partner without category
					if(0<$db->table_count('event_partners',"WHERE `event`='".$event_id."' AND `status`!=2 AND `cat`='0'")){
						$result.='<hr class="my-4"><div class="partners-list">';
						$partners=$db->sql("SELECT * FROM `event_partners` WHERE `event`='".$event_id."' AND `cat`='0' AND `status`!=2 ORDER BY `sort` ASC");
						foreach($partners as $partner){
							$result.='<div class="partner-item">';
							$external=true;
								if($partner['url']){
									$external=false;
									$partner['new_url']='/@'.$organizer_url.'/'.$event_url.'/partners/'.$partner['url'].'/';
								}
								else{
									$partner['new_url']=$partner['link'];
								}
								$result.='<a href="'.htmlspecialchars($partner['new_url']).'"'.($external?' target="_blank"':'').'>';
							if($partner['small_logo_url']){
								$result.='<img src="'.htmlspecialchars($partner['small_logo_url']).'" alt="'.htmlspecialchars($partner['name']).'">';
							}
							else{
								$result.=htmlspecialchars($partner['name']);
							}
							if($partner['caption']){
								$result.='<div class="partner-caption">'.htmlspecialchars($partner['caption']).'</div>';
							}
							$result.='</a>';
							$result.='</div>';
						}
						$result.='</div>';
					}
				}
			}
			if('blog'==$part){
				$sql_addon=[];
				$sql_addon[]="`event_content`.`event`='".$event_id."'";
				if(true==$allow_event_manage){//event management
					$sql_addon[]="`event_content`.`status`<='1'";//only planned & published
				}
				else{
					$sql_addon[]="`event_content`.`status`='1'";//only published
				}

				$sql_addon_str='';
				if(count($sql_addon)>0){
					$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
				}
				$content_count=$db->table_count('event_content',$sql_addon_str);
				if($content_count){
					$result.='<h2>Event blog</h2>';
					$content=$db->sql("SELECT `event_content`.* FROM `event_content` ".$sql_addon_str." ORDER BY `event_content`.`time` DESC LIMIT 5");
					foreach($content as $content_item){
						$pinned=(1==$content_item['pin']);

						$tags=$db->sql("SELECT `tag` FROM `event_content_tags` WHERE `event`='".$event_id."' AND `content`='".$content_item['id']."'");
						$tags_str='';
						foreach($tags as $tag){
							$tag_arr=$db->sql_row("SELECT * FROM `event_tags` WHERE `event`='".$event_id."' AND `id`='".$tag['tag']."'");
							$tags_str.='<a class="badge bg-tag" href="/@'.$organizer_url.'/'.$event_url.'/content/?tag='.$tag_arr['url'].'">'.$tag_arr['caption'].'</a> ';
						}
						$tags_str=trim($tags_str);

						$result.='<div class="content-card'.(0==$content_item['status']?' draft':'').'">';
							if($content_item['cover_url']){
								$result.='<div class="content-cover mb-4 md:mr-6">';
									$result.='<a href="/@'.$organizer_url.'/'.$event_url.'/content/'.$content_item['url'].'/">';
									$result.='<img src="'.$content_item['thumbnail_cover_url'].'" alt="'.htmlspecialchars($content_item['title']).'">';
									$result.='</a>';
								$result.='</div>';
							}
							$result.='<div class="content-info">';
								$result.='<div class="content-data">';
								$result.='<div class="content-title">';
									$result.='<a href="/@'.$organizer_url.'/'.$event_url.'/content/'.$content_item['url'].'/">'.htmlspecialchars($content_item['title']).'</a>';
									if($pinned){
										$result.='<span class="content-pinned" title="Pinned content">'.$ltmp['icons']['starred'].'</span>';
									}
								$result.='</div>';
								$result.='<div class="content-description">'.htmlspecialchars($content_item['description']).'</div>';
							$result.='</div>';
							$result.='<div class="content-time" data-datetime="'.$content_item['time'].'">';
							$result.='
							<div class="content-global-time">
								<span class="content-datetime">'.date('d.m.Y H:i',$content_item['time']).' UTC</span>
							</div>';
							$result.='
							<div class="content-local-time">
								<span class="content-datetime"></span>
							</div>';
							$result.='<div class="content-tags">'.$tags_str.'</div>';
							$result.='</div>';
							$result.='</div>';
						$result.='</div>';
					}
				}
			}
			if(''!=$result){
				$build_event_template_counter++;
			}
			return $result;
		}

		//if user is have auth
		if($auth['addresses']){
			//if user select to hide notice
			if(isset($_COOKIE['link_platform_notice_dismiss_'.$event_id])){
				$customize['link_platform_notice']=0;
			}
			if(0==$auth['platforms']){//without any linked platform
				if(1==$customize['link_platform_notice']){
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Notice to link platform</p>
						<p>To ensure that you receive timely updates about upcoming events and stay in the loop, we kindly request you to connect either your Email or Telegram account with our platform.</p>
						<p>By doing so, you\'ll never miss out on important event announcements, latest news, and exclusive offers.</p>
						<p class="italic">Rest assured that your personal information will be securely stored and used only for the purpose of keeping you informed about our events.</p>
						<a class="action-btn negative link-platform-notice-dismiss-action">Dismiss</a>
						<a class="action-btn" href="/link-platform/2/?back=/@'.$organizer_url.'/'.$event_url.'/" target="_blank">Link Email</a>
						<a class="action-btn" href="/link-platform/1/?back=/@'.$organizer_url.'/'.$event_url.'/" target="_blank">Link Telegram</a>
					</div>';
				}
				if(2==$customize['link_platform_notice']){
					print '<div class="modal-bg active">';
					print '
					<div class="modal-wrapper">
						<div class="modal-box">
							<div class="modal-header">
								<h3>Notice to link platform</h3>';
					print '
							</div>
							<div class="modal-body">
								<p>To ensure that you receive timely updates about upcoming events and stay in the loop, we kindly request you to connect either your Email or Telegram account with our platform.</p>
								<p>By doing so, you\'ll never miss out on important event announcements, latest news, and exclusive offers.</p>
								<p class="italic">Rest assured that your personal information will be securely stored and used only for the purpose of keeping you informed about our events.</p>
							</div>
							<div class="modal-footer">
								<div class="grow-white-space">
									<a class="action-btn negative link-platform-notice-dismiss-action opacity-75">Dismiss</a>
								</div>
								<a class="action-btn" href="/link-platform/2/?back=/@'.$organizer_url.'/'.$event_url.'/" target="_blank">Link Email</a>
								<a class="action-btn" href="/link-platform/1/?back=/@'.$organizer_url.'/'.$event_url.'/" target="_blank">Link Telegram</a>
							</div>
						</div>
					</div>';
					print '</div>';
				}
			}
		}

		$build_event_template_counter=0;
		$replace['main_container_addon']='';//remove addon (footer going bottom with standart offset by content)
		foreach($customize['template_parts'] as $part){
			if(!in_array($part,$customize['template_parts_disabled'])){
				print PHP_EOL.'<!-- start '.$part.' part -->'.PHP_EOL;
				print build_event_template($part);
				print PHP_EOL.'<!-- end '.$part.' part -->'.PHP_EOL;
			}
		}
		if(0==$build_event_template_counter){
			$replace['main_container_addon']=' single';//no content? need move footer by css class
			print '<h2 class="text-red-600">Oops there is no content</h2>';
			print '<p>Organizer not add any content yet.</p>';
		}
	}
}
else{
	http_response_code(404);
	print '<h2 class="text-red-600">Event not available</h2>';
	print '<p class="text-content">Return to <a href="/@'.$organizer_url.'/">organizer page</a>.</p>';
}

$event_content=ob_get_contents();
ob_end_clean();