<?php
ob_start();
if('@'==$path_array[1][0]){
	$organizer_url=urldecode($path_array[1]);
	$organizer_url=substr($organizer_url,1);
	$organizer_url=htmlspecialchars($organizer_url);
	$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `url`='".$db->prepare($organizer_url)."'");
	if(null!==$organizer_arr){
		if($organizer_arr['url']!==$organizer_url){
			$path_array[1]='@'.$organizer_arr['url'];
			$path=implode('/',$path_array);
			$path=str_replace('//','/',$path);
			header('Location: '.$path.($query_string?'?'.$query_string:''));
			exit;
		}

		$organizer_id=$organizer_arr['id'];

		//check if organizer exceed upload limit and limit uploads
		$limit_upload=false;//if organizer exceed upload limit
		//get max upload files size from subscription plan
		$max_organizer_files_size=$config['subscription_plans'][$organizer_arr['subscription_plan']]['max_upload_files_size'];
		if(0!=$organizer_arr['max_upload_files_size']){//rewrited by platform admin
			$max_organizer_files_size=$organizer_arr['max_upload_files_size'];
		}
		//need to check organizer subscription plan (in future)
		$organizer_files_size=$organizer_arr['summary_files_size'];
		if($organizer_files_size>=$max_organizer_files_size){
			$limit_upload=true;
		}

		$script_preset.='var organizer_id='.$organizer_id.';';
		$allow_manage=false;//organizer administration or platform admin
		$allow_manage_address_id=0;//address id
		if(isset($auth['organizers_list'][$organizer_arr['id']])){
			$allow_manage=$auth['organizers_list'][$organizer_arr['id']];
			$allow_manage_address_id=$auth['organizers_list_address_id'][$organizer_arr['id']];
		}
		if(1==$auth['status']){
			$allow_manage=1;//platform admin can manage as organizer owner
			$allow_manage_address_id=$auth['status_address_id'];
		}

		$allow_organizer_view=false;
		if(1==$organizer_arr['status']){
			$allow_organizer_view=true;
		}
		if(1==$allow_manage){
			if(0==$organizer_arr['status']){
				$allow_organizer_view=true;
			}
		}
		if($allow_organizer_view){
			if(0==$organizer_arr['status']){
				if($allow_manage){
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Notice</p>
						<p>The organizer is awaiting approval and is not available for public viewing.</p>
						<p>While you are waiting for moderation, you can manage it and fill it with information and plan events.</p>
					</div>';
				}
			}

			if($allow_manage){
				if($limit_upload){
					print '
					<div class="attention-box" role="alert">
						<p>The organizer is <b>exceeding the limit of the total size of the files</b>. You can increase the limit by subscribing to a paid plan or delete unnecessary files.</p>
					</div>';
				}
			}

			$replace['title']=htmlspecialchars($organizer_arr['title']).' | '.$replace['title'];
			$replace['description']=htmlspecialchars($organizer_arr['description']);
			$event_url=false;
			if('manage'==$path_array[2]){
				if(false!==$allow_manage){
					$manage_content='';
					include($root_dir.'/modules/manage/organizer.php');
					print $manage_content;
				}
				else{//not allowed to manage
					http_response_code(403);
					print '<h2 class="text-red-600">Access denied</h2>';
					print '<p class="text-content">Return to <a href="/@'.$organizer_url.'/">organizer page</a>.</p>';
				}
			}
			else
			if('content'==$path_array[2]){
				$replace['title']='Blog'.' | '.$replace['title'];

				if($path_array[3]){
					if('subscribe'==$path_array[3]){
						if($auth['default_address_id']){
							if(check_csrf()){
								$mailing_rule_arr=$db->sql_row("SELECT * FROM `mailing_rules` WHERE `address`='".$auth['default_address_id']."' AND `type`='1' AND `target`='".$organizer_id."' LIMIT 1");
								if(null===$mailing_rule_arr){
									$db->sql("INSERT INTO `mailing_rules` (`type`,`target`,`address`,`rule`,`time`) VALUES ('1','".$organizer_id."','".$auth['default_address_id']."','1','".time()."')");
								}
								else{
									$db->sql("UPDATE `mailing_rules` SET `rule`='1', `time`='".time()."' WHERE `id`='".$mailing_rule_arr['id']."'");
								}
								header('Location: /@'.$organizer_url.'/content/?success=subscribe');
							}
							else{
								header('Location: /@'.$organizer_url.'/content/?error=csrf');
							}
						}
						else{
							header('Location: /@'.$organizer_url.'/content/');
						}
					}
					else
					if('unsubscribe'==$path_array[3]){
						if($auth['default_address_id']){
							if(check_csrf()){
								$mailing_rule_arr=$db->sql_row("SELECT * FROM `mailing_rules` WHERE `address`='".$auth['default_address_id']."' AND `type`='1' AND `target`='".$organizer_id."' LIMIT 1");
								if(null===$mailing_rule_arr){
									$db->sql("INSERT INTO `mailing_rules` (`type`,`target`,`address`,`rule`,`time`) VALUES ('1','".$organizer_id."','".$auth['default_address_id']."','2','".time()."')");
								}
								else{
									$db->sql("UPDATE `mailing_rules` SET `rule`='2', `time`='".time()."' WHERE `id`='".$mailing_rule_arr['id']."'");
								}
								header('Location: /@'.$organizer_url.'/content/?success=unsubscribe');
							}
							else{
								header('Location: /@'.$organizer_url.'/content/?error=csrf');
							}
						}
						else{
							header('Location: /@'.$organizer_url.'/content/');
						}
					}
					else{
						$content_url=urldecode($path_array[3]);
						$sql_addon_str='';
						if($allow_manage){//org administration
							$sql_addon_str=" AND `status`<='1'";//only planned & published
						}
						else{
							$sql_addon_str=" AND `status`='1'";//only published
						}

						$content_item=$db->sql_row("SELECT * FROM `organizer_content` WHERE `organizer`='".$organizer_id."' AND `url`='".$db->prepare($content_url)."'".$sql_addon_str);
						if(null!==$content_item){
							$clear_content=clear_html_tags($content_item['content']);
							$pinned=(1==$content_item['pin']);

							$tags=$db->sql("SELECT `tag` FROM `organizer_content_tags` WHERE `organizer`='".$organizer_id."' AND `content`='".$content_item['id']."'");
							$tags_str='';
							foreach($tags as $tag){
								$tag_arr=$db->sql_row("SELECT * FROM `organizer_tags` WHERE `organizer`='".$organizer_id."' AND `id`='".$tag['tag']."'");
								$tags_str.='<a class="badge bg-tag" href="/@'.$organizer_url.'/content/?tag='.$tag_arr['url'].'">'.$tag_arr['caption'].'</a> ';
							}
							$tags_str=trim($tags_str);

							if(0==$content_item['status']){
								print '
								<div class="attention-box" role="alert">
									<p class="font-bold">Notice</p>';
								if($content_item['time']){
									print '<p>This content is planned to be published at <span class="time" data-time="'.$content_item['time'].'">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>. <a href="/@'.$organizer_url.'/manage/content/edit/'.$content_item['id'].'/">Click to edit.</a></p>';
								}
								else{
									print '<p>This content is in drafts, you can schedule it to be published. <a href="/@'.$organizer_url.'/manage/content/edit/'.$content_item['id'].'/">Click to edit.</a></p>';
								}
								print '</div>';
							}

							$replace['title']=htmlspecialchars($content_item['title']).' | '.$replace['title'];
							if(''!=$content_item['description']){
								$replace['description']=$content_item['description'];
								$replace['description']=strip_tags($replace['description']);
								$replace['description']=str_replace(array("\r\n","\n","\r"),' ',$replace['description']);
								$replace['description']=preg_replace('/\s+/',' ', $replace['description']);
								$replace['description']=substr($replace['description'],0,160);
								$replace['description'].='...';
								$replace['description']=htmlspecialchars($replace['description']);
							}

							$replace['head_addon'].='<meta property="og:title" content="'.htmlspecialchars($content_item['title']).'">';
							$replace['head_addon'].='<meta property="og:description" content="'.htmlspecialchars($content_item['description']).'">';
							if(''!=$content_item['cover_url']){
								$replace['head_addon'].='<meta property="og:image" content="'.$config['platform_url'].$content_item['cover_url'].'">';
							}
							$replace['head_addon'].='<meta property="og:url" content="'.$config['platform_url'].'/@'.$organizer_url.'/content/'.$content_item['url'].'/">';
							$replace['head_addon'].='<meta property="og:type" content="article">';
							$replace['head_addon'].='<meta property="og:site_name" content="'.$replace['title'].'">';
							$replace['head_addon'].='<meta property="article:published_time" content="'.date('c',$content_item['time']).'">';
							$replace['head_addon'].='<meta property="article:modified_time" content="'.date('c',$content_item['time']).'">';
							$replace['head_addon'].='<meta property="article:section" content="Blog">';
							$replace['head_addon'].='<meta property="og:locale" content="en_US">';
							$replace['head_addon'].='<meta property="og:locale:alternate" content="ru_RU">';

							$replace['head_addon'].='<meta property="article:tag" content="">';

							print '<h1>'.htmlspecialchars($content_item['title']);
							if($pinned){
								print '<span class="content-pinned" title="Pinned content">'.$ltmp['icons']['starred'].'</span>';
							}
							print '</h1>';
							print '<div class="content-card article single">';
							print '<div class="content-wrapper">';
							print $clear_content;
							print '</div>';
							print '<div class="content-info">';
								print '<div class="content-time" data-datetime="'.$content_item['time'].'">';
								print '<div class="content-global-time">
								<span class="content-datetime">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>
								</div>';
								print '<div class="content-local-time">
								<span class="content-datetime"></span>
								</div>';
								print '<div class="content-tags">'.$tags_str.'</div>';
								print '</div>';
							print '</div>';
							print '</div>';
							print '<hr class="my-4">';
							print '<a class="reverse-btn" href="/@'.$organizer_url.'/content/">&larr; Back to blog</a>';
						}
						else{
							header("HTTP/1.0 404 Not Found");
							print '<h1>404 Not Found</h1>';
							print '<p>Content not found, please return to <a href="/@'.$organizer_url.'/content/">organizer blog</a>.</p>';
						}
					}
				}
				else{
					$filter_tag=false;
					$filter_tag_arr=null;
					if(isset($_GET['tag'])){
						if(''!==$_GET['tag']){
							$filter_tag=$_GET['tag'];
							$filter_tag_arr=$db->sql_row('SELECT * FROM `organizer_tags` WHERE `organizer`='.$organizer_id.' AND `url`=\''.$db->prepare($_GET['tag']).'\'');
							if(null==$filter_tag_arr){
								$filter_tag=false;
							}
							else{
								$filter_tag=$filter_tag_arr['id'];
							}
						}
					}

					print '<h1>'.htmlspecialchars($organizer_arr['title']).' blog'.(null!=$filter_tag_arr?' #'.htmlspecialchars($filter_tag_arr['url']).'':'').'</h1><hr class="mb-4">';

					print '<div class="mb-4">';
					print '<a href="/@'.$organizer_url.'/" class="small-btn">Events list</a>';
					print '<a href="/@'.$organizer_url.'/content/" class="ml-4 small-btn">Blog</a>';
					if($auth['default_address_id']){
						if(isset($auth['organizers_list'][$organizer_arr['id']])||$allow_manage){
							print '<a href="/@'.$organizer_url.'/manage/" class="ml-4 violet-btn">Manage organizer</a>';
						}
					}
					print '</div>';

					print '<hr class="my-4">';
					if(null!=$filter_tag_arr){
						print '<a class="reverse-btn" href="/@'.$organizer_url.'/content/">&larr; Back to blog</a>';
					}

					if($auth['default_address_id']){
						$mailing_rule=$db->select_one('mailing_rules','rule'," WHERE `address`='".$auth['default_address_id']."' AND `type`='1' AND `target`='".$organizer_id."'");
						if(is_null($mailing_rule)){
							$mailing_rule=0;
						}
						if(1!=$mailing_rule){
							print '<a class="action-btn subscribe-action-button" href="/@'.$organizer_url.'/content/subscribe/?'.gen_csrf_param().'">'.$ltmp['icons']['subscribe'].' Subscribe</a>';
						}
						if(1==$mailing_rule){
							print '<a class="action-btn redo unsubscribe-action-button" href="/@'.$organizer_url.'/content/unsubscribe/?'.gen_csrf_param().'">'.$ltmp['icons']['unsubscribe'].' Unsubscribe</a>';
						}
						if(isset($_GET['error'])){
							if('csrf'==$_GET['error']){
								print '
								<div class="attention-box" role="alert">
									<p>⚠️ CSRF is invalid, try again.</p>
								</div>';
							}
						}
						if(isset($_GET['success'])){
							if('subscribe'==$_GET['success']){
								print '
								<div class="success-box" role="alert">
									<p>✔️ You have successfully subscribed to this organizer.</p>
								</div>';
							}
							if('unsubscribe'==$_GET['success']){
								print '
								<div class="success-box" role="alert">
									<p>You have successfully unsubscribed from this organizer.</p>
								</div>';
							}
						}
					}

					$sql_addon=[];
					$sql_addon[]="`organizer_content`.`organizer`='".$organizer_id."'";
					if($allow_manage){//org administration
						$sql_addon[]="`organizer_content`.`status`<='1'";//only planned & published
					}
					else{
						$sql_addon[]="`organizer_content`.`status`='1'";//only published
					}

					$sql_addon_str='';
					if(count($sql_addon)>0){
						$sql_addon_str=' WHERE '.implode(' AND ',$sql_addon);
					}

					if(false!==$filter_tag){
						$sql_addon_str=' RIGHT JOIN `organizer_content_tags` ON `organizer_content_tags`.`content`=`organizer_content`.`id` AND `organizer_content_tags`.`tag`='.$filter_tag.' '.$sql_addon_str;
					}

					$per_page=10;
					$count=$db->table_count('organizer_content',$sql_addon_str);
					$pages_count=ceil($count/$per_page);
					$page=1;
					if(isset($_GET['page'])){
						$page=(int)$_GET['page'];
						if($page<1){
							$page=1;
						}
						elseif($page>$pages_count){
							$page=$pages_count;
						}
					}

					$content_counter=0;
					$content=$db->sql("SELECT `organizer_content`.* FROM `organizer_content` ".$sql_addon_str." ORDER BY `organizer_content`.`time` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
					foreach($content as $content_item){
						$content_counter++;

						$content_html=clear_html_tags($content_item['content']);
						$pinned=(1==$content_item['pin']);

						$tags=$db->sql("SELECT `tag` FROM `organizer_content_tags` WHERE `organizer`='".$organizer_id."' AND `content`='".$content_item['id']."'");
						$tags_str='';
						foreach($tags as $tag){
							$tag_arr=$db->sql_row("SELECT * FROM `organizer_tags` WHERE `organizer`='".$organizer_id."' AND `id`='".$tag['tag']."'");
							$tags_str.='<a class="badge bg-tag" href="/@'.$organizer_url.'/content/?tag='.$tag_arr['url'].'">'.$tag_arr['caption'].'</a> ';
						}
						$tags_str=trim($tags_str);

						print '<div class="content-card'.(0==$content_item['status']?' draft':'').'">';
							if($content_item['cover_url']){
								print '<div class="content-cover mb-4 md:mr-6">';
								print '<a href="/@'.$organizer_url.'/content/'.$content_item['url'].'/">';
								print '<img src="'.$content_item['thumbnail_cover_url'].'" alt="'.htmlspecialchars($content_item['title']).'">';
								print '</a>';
								print '</div>';
							}
							print '<div class="content-info">';
								print '<div class="content-data">';
								print '<div class="content-title">';
								print '<a href="/@'.$organizer_url.'/content/'.$content_item['url'].'/">'.htmlspecialchars($content_item['title']).'</a>';
									if($pinned){
										print '<span class="content-pinned" title="Pinned content">'.$ltmp['icons']['starred'].'</span>';
									}
								print '</div>';
								print '<div class="content-description">'.htmlspecialchars($content_item['description']).'</div>';
								print '</div>';
								print '<div class="content-time" data-datetime="'.$content_item['time'].'">';
								print '<div class="content-global-time">
								<span class="content-datetime">'.date('d.m.Y H:i',$content_item['time']).' GMT</span>
								</div>';
								print '<div class="content-local-time">
								<span class="content-datetime"></span>
								</div>';
								print '<div class="content-tags">'.$tags_str.'</div>';
								print '</div>';
							print '</div>';
						print '</div>';
					}
					if(0==$content_counter){
						print '<p>No content was found</p>';
					}

					print '<div class="pagination">';
					//get string with all GET params except page
					$get_params=[];
					foreach($_GET as $get_param_name=>$get_param_value){
						if('page'!==$get_param_name){
							$get_params[]=$get_param_name.'='.urlencode($get_param_value);
						}
					}
					$get_params_str='';
					if(count($get_params)){
						$get_params_str=implode('&',$get_params);
					}
					if($page>1){
						print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
					}
					for($i=1;$i<=$pages_count;$i++){
						print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
					}
					if($page<$pages_count){
						print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
					}
					print '</div>';
				}
			}
			else
			if($path_array[2]){
				$event_url=urldecode($path_array[2]);
				$event_url=htmlspecialchars($event_url);
				$event_arr=$db->sql_row("SELECT * FROM `events` WHERE `url`='".$db->prepare($event_url)."' AND `organizer`='".$organizer_arr['id']."'");
				if(null!==$event_arr){
					$event_content='';
					include($root_dir.'/modules/events.php');
					print $event_content;
				}
				else{
					http_response_code(404);
					print '<h2 class="text-red-600">Event not found</h2>';
					print '<p class="text-content">Return to <a href="/@'.$organizer_url.'/">organizer page</a> and try find it.</p>';
				}
			}
			else{
				print '<h1>'.htmlspecialchars($organizer_arr['title']).'</h1><hr class="mb-4">';

				print '<div class="mb-4">';
				print '<a href="/@'.$organizer_url.'/" class="small-btn">Events list</a>';
				print '<a href="/@'.$organizer_url.'/content/" class="ml-4 small-btn">Blog</a>';
				if($auth['default_address_id']){
					if(isset($auth['organizers_list'][$organizer_arr['id']])||$allow_manage){
						print '<a href="/@'.$organizer_url.'/manage/" class="ml-4 violet-btn">Manage organizer</a>';
					}
				}
				print '</div>';

				print '<p>'.htmlspecialchars($organizer_arr['description']).'</p>';
				if($organizer_arr['events_description']){
					print '<h2>Events</h2>';
					print '<p>'.htmlspecialchars($organizer_arr['events_description']).'</p>';
				}
				print '<h3>Active events</h3>';
				$active_events=$db->table_count('events','WHERE `organizer`='.$organizer_arr['id'].' AND `status`=1');
				if(0==$active_events){
					print '<p>No active events.</p>';
				}
				else{
					$events=$db->sql("SELECT * FROM `events` WHERE `organizer`='".$organizer_arr['id']."' AND `status`='1' AND `moderation`='1' ORDER BY `time` ASC");
					print '<div class="events-list">';
					foreach($events as $event_arr){
						print '<div class="event-card">';
						if($event_arr['logo_url']){
							print '<div class="event-logo mb-4 md:mr-6">';
							print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/">';
							print '<img src="'.$event_arr['logo_url'].'" alt="'.htmlspecialchars($event_arr['title']).'">';
							print '</a>';
							print '</div>';
						}
						print '<div class="event-info">';
						if($event_arr['time']){
							print '<div class="event-time" data-start-time="'.$event_arr['time'].'">';
								print '<div class="event-global-time">';
									print '<span class="event-date"><span class="bold">'.date('d.m.Y',$event_arr['time']).'</span> GMT</span>';
									print '<span class="event-start-time"><span class="bold">'.date('H:i',$event_arr['time']).'</span>';
								print '</div>';
								print '<div class="event-local-time">';
									print '
									<span class="event-date"></span>
									<span class="event-start-time"><span class="bold"></span></span>';
								print '</div>';
							print '</div>';
						}
							print '<div class="event-info">';
							print '<div class="event-caption">';
							print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/">'.htmlspecialchars($event_arr['title']).'</a>';
								print '</div>';
								if($event_arr['short_description']){
									print '<div class="event-description">'.htmlspecialchars($event_arr['short_description']).'</div>';
								}
								if($event_arr['short_location_description']){
									print '<div class="event-location">
									'.$ltmp['icons']['location'].'
									'.$event_arr['short_location_description'].'</div>';
								}
								if($allow_manage){
									print '<div>';
									print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/manage/" class="violet-btn">Edit</a>';
									print '</div>';
								}
							print '</div>';
						print '</div>';
						print '</div>';
					}
					print '</div>';
				}
				if($allow_manage){
					$planning_events=$db->table_count('events','WHERE `organizer`='.$organizer_arr['id'].' AND `status`=0');
					if(0!=$planning_events){
						print '<h3>Planning</h3>';
						//use waiting moderation flag for manager as possible planning events
						$events=$db->sql("SELECT * FROM `events` WHERE `organizer`='".$organizer_arr['id']."' AND (`status`='0' OR `moderation`='0') ORDER BY `time` ASC");
						print '<div class="events-list">';
						foreach($events as $event_arr){
							print '<div class="event-card">';
							if($event_arr['logo_url']){
								print '<div class="event-logo mb-4 md:mr-6">';
								print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/">';
								print '<img src="'.$event_arr['logo_url'].'" alt="'.htmlspecialchars($event_arr['title']).'">';
								print '</a>';
								print '</div>';
							}
							print '<div class="event-info">';
							if($event_arr['time']){
								print '<div class="event-time" data-start-time="'.$event_arr['time'].'">';
									print '<div class="event-global-time">';
										print '<span class="event-date"><span class="bold">'.date('d.m.Y',$event_arr['time']).'</span> GMT</span>';
										print '<span class="event-start-time"><span class="bold">'.date('H:i',$event_arr['time']).'</span>';
									print '</div>';
									print '<div class="event-local-time">';
										print '
										<span class="event-date"></span>
										<span class="event-start-time"><span class="bold"></span></span>';
									print '</div>';
									print '</div>';
							}
								print '<div class="event-info">';
								print '<div class="event-caption">';
								print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/">'.htmlspecialchars($event_arr['title']).'</a>';
									print '</div>';
								if($event_arr['short_description']){
									print '<div class="event-description">'.htmlspecialchars($event_arr['short_description']).'</div>';
								}
								if($event_arr['short_location_description']){
									print '<div class="event-location">
									'.$ltmp['icons']['location'].'
									'.$event_arr['short_location_description'].'</div>';
								}
								if($allow_manage){
									print '<div>';
									print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/manage/" class="violet-btn">Edit</a>';
									print '</div>';
								}
								print '</div>';
							print '</div>';
							print '</div>';
						}
						print '</div>';
					}
				}
				$archived_events=$db->table_count('events','WHERE `organizer`='.$organizer_arr['id'].' AND `status`=2');
				if(0!=$archived_events){
					print '<h3>Archive</h3>';
					$events=$db->sql("SELECT * FROM `events` WHERE `organizer`='".$organizer_arr['id']."' AND `status`='2' AND `moderation`=1 ORDER BY `time` ASC");
					print '<div class="events-list">';
					foreach($events as $event_arr){
						print '<div class="event-card">';
						if($event_arr['logo_url']){
							print '<div class="event-logo mb-4 md:mr-6">';
							print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/">';
							print '<img src="'.$event_arr['logo_url'].'" alt="'.htmlspecialchars($event_arr['title']).'">';
							print '</a>';
							print '</div>';
						}
						print '<div class="event-info">';
						if($event_arr['time']){
							print '<div class="event-time" data-start-time="'.$event_arr['time'].'">';
								print '<div class="event-global-time">';
									print '<span class="event-date"><span class="bold">'.date('d.m.Y',$event_arr['time']).'</span> GMT</span>';
									print '<span class="event-start-time"><span class="bold">'.date('H:i',$event_arr['time']).'</span>';
								print '</div>';
								print '<div class="event-local-time">';
									print '
									<span class="event-date"></span>
									<span class="event-start-time"><span class="bold"></span></span>';
								print '</div>';
							print '</div>';
						}
							print '<div class="event-info">';
							print '<div class="event-caption">';
							print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/">'.htmlspecialchars($event_arr['title']).'</a>';
								print '</div>';
								if($event_arr['short_description']){
									print '<div class="event-description">'.htmlspecialchars($event_arr['short_description']).'</div>';
								}
								if($event_arr['short_location_description']){
									print '<div class="event-location">
									'.$ltmp['icons']['location'].'
									'.$event_arr['short_location_description'].'</div>';
								}
								if($allow_manage){
									print '<div>';
									print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/manage/" class="violet-btn">Edit</a>';
									print '</div>';
								}
							print '</div>';
						print '</div>';
						print '</div>';
					}
					print '</div>';
				}
			}
		}
		else{
			http_response_code(404);
			if(3!=$organizer_arr['status']){
				if(0==$organizer_arr['status']){
					print '<h2 class="text-red-600">Organizer not approved</h2>';
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>Organizer waiting moderation, please wait...</p>
					</div>';
				}
				if(2==$organizer_arr['status']){
					print '<h2 class="text-red-600">Organizer not available</h2>';
					print '
					<div class="attention-box" role="alert">
						<p class="font-bold">Error</p>
						<p>Organizer was banned</p>
					</div>';
				}
				print '<p class="text-content">Return to <a href="/organizers/">organizers list</a>.</p>';
			}
			else{
				exit;
			}
		}
	}
	else{
		http_response_code(404);
		print '<h2 class="text-red-600">Organizer not found</h2>';
		print '<p class="text-content">Return to <a href="/organizers/">organizers list</a>.</p>';
	}
}
if('events_poster'==$path_array[1]){
	$replace['head_addon'].='
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@Envelop_project">
<meta name="twitter:creator" content="@Envelop_project">
<meta name="twitter:title" content="Events poster by GetPass.is">
<meta name="twitter:description" content="Discover new events on GetPass.is web3 platform">
<meta name="twitter:image" content="/meta-image.jpg">
';
	//changed from organizers to events_poster https://gitlab.com/envelop/tickets/getpass-land/-/issues/41
	$replace['title']='Events poster'.' | '.$replace['title'];
	print '<h1>Events poster</h1>';
	print '<div class="text-content"><hr class="mb-4">';
	//https://gitlab.com/envelop/tickets/getpass-land/-/issues/43
	$sql_addon_str=' WHERE `status`=1';
	$sql_addon_str.=' AND `moderation`=1';
	//no time limit now
	/*
	//if time limit is needed
	$events_max_time_offset=time()-(1*60*60);//additional hour range for active events
	$sql_addon_str.=' AND `time`>='.$events_max_time_offset;
	*/

	$filter_organizer=false;
	if(isset($_GET['organizer'])){
		if(''!==$_GET['organizer']){
			$filter_organizer=$db->prepare($_GET['organizer']);
		}
	}
	$filter_event=false;
	if(isset($_GET['event'])){
		if(''!==$_GET['event']){
			$filter_event=trim($_GET['event']);
		}
	}

	print '
	<form action="" method="GET">
	<div class="filters-wrapper">';
	print '
		<div>
			<input name="organizer" class="form-post-by-enter" placeholder="Search by organizer" value="'.($filter_organizer?htmlspecialchars($filter_organizer):'').'">
		</div>';
	print '
		<div>
			<input name="event" class="form-post-by-enter" placeholder="Search event" value="'.($filter_event?htmlspecialchars($filter_event):'').'">
		</div>';
	print '
	</div>
	</form>';

	$sql_addon=[];
	if($filter_organizer){
		$organizers_list=[];
		$organizers_list_arr=$db->sql("SELECT `id` FROM `organizers` WHERE `status`=1 AND (`title` LIKE '%".$db->prepare($filter_organizer)."%' OR `url` LIKE '%".$db->prepare($filter_organizer)."%' OR `description` LIKE '%".$db->prepare($filter_organizer)."%')");
		foreach($organizers_list_arr as $organizer_arr){
			$organizers_list[]=$organizer_arr['id'];
		}
		if(0==count($organizers_list)){//no organizers found
			$sql_addon[]="`organizer`=-1";
		}
		else{
			$sql_addon[]="`organizer` IN (".implode(',',$organizers_list).")";
		}
	}
	if($filter_event){
		$sql_addon[]="(`title` LIKE '%".$db->prepare($filter_event)."%' OR `url` LIKE '%".$db->prepare($filter_event)."%' OR `short_description` LIKE '%".$db->prepare($filter_event)."%')";
	}
	if(count($sql_addon)){
		$sql_addon_str.=' AND '.implode(' AND ',$sql_addon);
	}

	print '<hr class="my-4">';

	$events_count=$db->table_count('events',$sql_addon_str);
	if(0==$events_count){
		print '<p>No active events.</p>';
	}
	else{
		$per_page=10;
		$pages_count=ceil($events_count/$per_page);
		$page=1;
		if(isset($_GET['page'])){
			$page=(int)$_GET['page'];
			if($page<1){
				$page=1;
			}
			elseif($page>$pages_count){
				$page=$pages_count;
			}
		}

		$events=$db->sql("SELECT * FROM `events` ".$sql_addon_str." ORDER BY `time` DESC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
		print '<div class="events-list">';
		$organizers_arr=[];
		foreach($events as $event_arr){
			$organizer_arr=[];
			if(isset($organizers_arr[$event_arr['organizer']])){//check cache
				$organizer_arr=$organizers_arr[$event_arr['organizer']];
			}
			else{
				$organizer_arr=$db->sql_row("SELECT * FROM `organizers` WHERE `id`='".$event_arr['organizer']."' LIMIT 1");
				$organizers_arr[$event_arr['organizer']]=$organizer_arr;//cache
			}
			$organizer_url=htmlspecialchars($organizer_arr['url']);

			print '<div class="event-card">';
			if($event_arr['logo_url']){
				print '<div class="event-logo mb-4 md:mr-6">';
				print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/">';
				print '<img src="'.$event_arr['logo_url'].'" alt="'.htmlspecialchars($event_arr['title']).'">';
				print '</a>';
				print '</div>';
			}
			print '<div class="event-info">';
			if($event_arr['time']){
				print '<div class="event-time" data-start-time="'.$event_arr['time'].'">';
					print '<div class="event-global-time">';
						print '<span class="event-date"><span class="bold">'.date('d.m.Y',$event_arr['time']).'</span> GMT</span>';
						print '<span class="event-start-time"><span class="bold">'.date('H:i',$event_arr['time']).'</span>';
					print '</div>';
					print '<div class="event-local-time">';
						print '
						<span class="event-date"></span>
						<span class="event-start-time"><span class="bold"></span></span>';
					print '</div>';
				print '</div>';
			}
			print '<div class="event-info">';
			print '<div class="event-caption">';
			print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/">'.htmlspecialchars($event_arr['title']).'</a>';
				print '</div>';
				if($event_arr['short_description']){
					print '<div class="event-description">'.htmlspecialchars($event_arr['short_description']).'</div>';
				}
				if($event_arr['short_location_description']){
					print '<div class="event-location">
					'.$ltmp['icons']['location'].'
					'.$event_arr['short_location_description'].'</div>';
				}
			//if need to show manage button
			/*
			$allow_manage=false;//organizer administration or platform admin
			$allow_manage_address_id=0;//address id
			if(isset($auth['organizers_list'][$organizer_arr['id']])){
				$allow_manage=$auth['organizers_list'][$organizer_arr['id']];
				$allow_manage_address_id=$auth['organizers_list_address_id'][$organizer_arr['id']];
			}
			if(1==$auth['status']){
				$allow_manage=1;//platform admin can manage as organizer owner
				$allow_manage_address_id=$auth['status_address_id'];
			}
			if($allow_manage){
				print '<div class="my-4">';
				print '<a href="/@'.$organizer_url.'/'.$event_arr['url'].'/manage/" class="violet-btn default-button">Manage</a>';
				print '</div>';
			}
			*/
			print '</div>';
			print '</div>';
			print '</div>';
		}

		print '<div class="pagination">';
		//get string with all GET params except page
		$get_params=[];
		foreach($_GET as $get_param_name=>$get_param_value){
			if('page'!==$get_param_name){
				$get_params[]=$get_param_name.'='.urlencode($get_param_value);
			}
		}
		$get_params_str='';
		if(count($get_params)){
			$get_params_str=implode('&',$get_params);
		}
		if($page>1){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
		}
		for($i=1;$i<=$pages_count;$i++){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
		}
		if($page<$pages_count){
			print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
		}
		print '</div>';

		print '</div>';
	}
	//old organizers list
	/*
	if($auth['default_address_id']){
		print '
		<div class="mb-4">
			<a href="/add-organizer/" class="action-btn">Create organizer</a>
		</div>';
	}
	$per_page=50;
	$sql_addon_str=' WHERE `status`=1';
	$count=$db->table_count('organizers',$sql_addon_str);
	$pages_count=ceil($count/$per_page);
	$page=1;
	if(isset($_GET['page'])){
		$page=(int)$_GET['page'];
		if($page<1){
			$page=1;
		}
		elseif($page>$pages_count){
			$page=$pages_count;
		}
	}

	$organizers=$db->sql("SELECT * FROM `organizers`".$sql_addon_str." ORDER BY `title` ASC LIMIT ".$per_page." OFFSET ".(($page-1)*$per_page)."");
	foreach($organizers as $organizer_arr){
		print '<p><a href="/@'.htmlspecialchars($organizer_arr['url']).'/">'.htmlspecialchars($organizer_arr['title']).'</a>';
		if($organizer_arr['description']){
			print ' &mdash; '.htmlspecialchars($organizer_arr['description']);
		}
		if(isset($auth['organizers_list'][$organizer_arr['id']])){
			print ' <a class="violet-btn" href="/@'.htmlspecialchars($organizer_arr['url']).'/manage/">Manage organizer</a>';
		}
		print '</p>';
	}
	print '<div class="pagination">';
	//get string with all GET params except page
	$get_params=[];
	foreach($_GET as $get_param_name=>$get_param_value){
		if('page'!==$get_param_name){
			$get_params[]=$get_param_name.'='.urlencode($get_param_value);
		}
	}
	$get_params_str='';
	if(count($get_params)){
		$get_params_str=implode('&',$get_params);
	}
	if($page>1){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page-1).'">Prev</a>';
	}
	for($i=1;$i<=$pages_count;$i++){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.$i.'"'.($i===$page?' class="active"':'').'>'.$i.'</a>';
	}
	if($page<$pages_count){
		print '<a href="?'.htmlspecialchars($get_params_str).'&page='.($page+1).'">Next</a>';
	}
	print '</div>';
	*/
	print '</div>';
}
$content=ob_get_contents();
ob_end_clean();