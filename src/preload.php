<?php
include('config.php');
putenv('TZ='.$config['server_timezone']);
date_default_timezone_set($config['server_timezone']);
header('X-Frame-Options:SAMEORIGIN');

include($root_dir.'/class/autoloader.php');
include($root_dir.'/class/db.php');
include($root_dir.'/class/template.php');

$db=new DataManagerDatabase($config['db_host'],$config['db_login'],$config['db_password']);
$db->db($config['db_base'],'utf8mb4');
if(!$db->link){
	http_response_code(503);//Service Temporarily Unavailable
	exit;
}

$install=true;
if(file_exists($root_dir.'/install.lock')){
	$install=false;
}
if($install){
	$requered_tables=[
		'addresses',
		'auth',
		'auth_addresses',
		'auth_notifications',
		'binded_nft',
		'chains',
		'content',
		'content_tags',
		'error',
		'events',
		'event_content',
		'event_content_tags',
		'event_invites',
		'event_levels',
		'event_locations',
		'event_partners',
		'event_partners_cat',
		'event_sessions',
		'event_sessions_speakers',
		'event_speakers',
		'event_tags',
		'event_users',
		'event_whitelist',
		'linked_platforms',
		'mailing_rules',
		'mail_queue',
		'nft_list',
		'notifications_queue',
		'oracle_updates',
		'organizers',
		'organizer_addresses',
		'organizer_content',
		'organizer_content_tags',
		'organizer_tags',
		'platforms',
		'presets',
		'signer_history',
		'tags',
		'telegram_groups',
		'telegram_groups_history',
		'telegram_polls',
		'telegram_queue',
		'telegram_updates',
		'telegram_users',
		'types',
	];
	$tables=$db->sql('show tables');
	$tables_count=0;

	foreach($tables as $table){
		$table=$table['Tables_in_'.$config['db_base']];
		if(in_array($table,$requered_tables)){
			$tables_count++;
		}
	}

	if(count($requered_tables)==$tables_count){
		touch($root_dir.'/install.lock');
		print 'Install complete... Reloading page...';
		print '<meta http-equiv="refresh" content="3">';
	}
	else{
		if(0==$tables_count){//fresh install
			if(!file_exists($root_dir.'/install.sql')){
				print 'Install error: install.sql not found';
				exit;
			}
			$install_sql=file_get_contents($root_dir.'/install.sql');
			$install_sql=explode(';',$install_sql);
			foreach($install_sql as $sql){
				if(''!=$sql){
					print PHP_EOL.$sql;
					$db->sql($sql);
				}
			}
			print 'Execute SQL. Reloading page...';
			print '<meta http-equiv="refresh" content="3">';
		}
		else{
			print 'Install error: tables count mismatch';
		}
	}
	exit;
}

$t=new DataManagerTemplate($root_dir.'/templates/');

$script_change_time=filemtime($root_dir.'/app.js');
$css_change_time=filemtime($root_dir.'/app.css');

$api=new VIZ\JsonRPC();

function telegram_method($method,$data=array(),$debug=false,$silence=false){
	global $api,$options;
	if(!$silence){
		print '['.date('d.m.Y H:i:s').'] Tg method: '.$method.PHP_EOL;
	}
	$result=$api->get_url($options['gate'].$method,json_encode($data),$debug);
	if(false!==$result){
		list($header,$result)=$api->parse_web_result($result);
		if($debug){
			print_r($header);
			print_r($result);
		}
		return $result;
	}
	return false;
}

function telegram_method_with_status($method,$data=array(),$debug=false,$silence=false){
	global $api,$options;
	if(!$silence){
		print '['.date('d.m.Y H:i:s').'] Tg method: '.$method.PHP_EOL;
	}
	$result=$api->get_url($options['gate'].$method,json_encode($data),$debug);
	if(false!==$result){
		list($header,$result)=$api->parse_web_result($result);
		$status_line=substr($header,0,strpos($header,"\n"));
		$status_arr=explode(' ',$status_line,3);
		$status=(int)$status_arr[1];
		if($debug){
			print_r($header);
			print_r($result);
		}
		return [$status,$result];
	}
	return [false,false];
}

function telegram_queue($chat_id,$method,$data,$inline_keyboard=false,$keyboard=false,$pin=0){
	global $db;
	if(isset($data['text'])){
		if(isset($data['parse_mode'])){
			if($data['parse_mode']=='HTML'){
				$data['text']=strip_tags($data['text'],'<b><strong><i><em><code><s><strike><del><u><pre><tg-spoiler><a>');
			}
		}
		//change \n\n\n to \n continuiously
		$data['text']=str_replace("\r",'',$data['text']);
		$data['text']=str_replace("\t",' ',$data['text']);
		while(false!==strpos($data['text'],"\n\n\n")){
			$data['text']=str_replace("\n\n\n","\n\n",$data['text']);
		}
		while(false!==strpos($data['text'],'  ')){
			$data['text']=str_replace('  ',' ',$data['text']);
		}
		while(false!==strpos($data['text'],"\n ")){
			$data['text']=str_replace("\n ","\n",$data['text']);
		}
		$data['text']=trim($data['text'],"\n\t ");
	}
	$db->sql("INSERT INTO `telegram_queue` (`chat_id`,`method`,`data`,`inline_keyboard`,`keyboard`,`pin`) VALUES ('".$chat_id."','".$method."','".$db->prepare(serialize($data))."','".(false!==$inline_keyboard?$db->prepare(serialize($inline_keyboard)):'')."','".(false!==$keyboard?$db->prepare(serialize($keyboard)):'')."','".(int)$pin."')");
}

function sig_handler($sig){
	global $pname,$work;
	print PHP_EOL.PHP_EOL.'['.date('d.m.Y H:i:s').'] '.$pname.' sig_handler >>>==='.PHP_EOL;
	print $sig;
	print PHP_EOL.'<<<==='.PHP_EOL.PHP_EOL;
	$work=false;
}

function fatal_handler(){
	global $pname,$pid_file,$work;
	print PHP_EOL.PHP_EOL.'['.date('d.m.Y H:i:s').'] '.$pname.' fatal_handler >>>==='.PHP_EOL;
	if($work){
		$error=error_get_last();
		var_dump($error);
	}
	else{
		print 'Regular shutdown'.PHP_EOL;
	}
	print PHP_EOL.'<<<==='.PHP_EOL.PHP_EOL;
	if(file_exists($pid_file)){
		print '['.date('d.m.Y H:i:s').'] SHUTDOWN pid file: '.$pid_file.PHP_EOL;
		unlink($pid_file);
	}
}

$ltmp_arr=[];
function ltmp($ltmp_str,$ltmp_args=[]){
	global $ltmp_arr;
	preg_match_all('~%%([a-zA-Z_0-9]*)%%~iUs',$ltmp_str,$ltmp_includes);
	foreach($ltmp_includes[1] as $var_name){
		$ltmp_str=str_replace($var_name,ltmp($ltmp_arr[$var_name]),$ltmp_str);
	}
	foreach($ltmp_args as $k=>$v){
		$ltmp_str=str_replace('{'.$k.'}',$v,$ltmp_str);
	}
	//remove empty args
	preg_match_all('~\{[a-z0-9_\-]*\}~iUs',$ltmp_str,$ltmp_prop_arr);
	foreach($ltmp_prop_arr[0] as $var_name){
		$ltmp_str=str_replace($var_name,'',$ltmp_str);
	}
	return $ltmp_str;
}

function mb_str_replace($search,$replace,$string){
	$charset=mb_detect_encoding($string);
	$unicodeString=iconv($charset,'UTF-8',$string);
	return str_replace($search, $replace, $unicodeString);
}

function mb_strtr($str,$from,$to){
	return str_replace(mb_str_split($from), mb_str_split($to), $str);
}

function ru2lat($string){
	$rus=array('ё','ж','ц','ч','ш','щ','ю','я','Ё','Ж','Ц','Ч','Ш','Щ','Ю','Я');
	$lat=array('yo','zh','tc','ch','sh','sh','yu','ya','YO','ZH','TC','CH','SH','SH','YU','YA');
	$string=mb_str_replace($rus,$lat,$string);
	//$string=str_replace($rus,$lat,$string);
	$string=mb_strtr($string,
		"АБВГДЕЗИЙКЛМНОПРСТУФХЪЫЬЭабвгдезийклмнопрстуфхъыьэ",
		"ABVGDEZIJKLMNOPRSTUFH_I_Eabvgdezijklmnoprstufh_i_e");
	return($string);
}